import io.pvdnc.idl.parser.IDLParserProvider;
import io.pvdnc.idl.pvd.ProvidenceParserProvider;

module io.pvdnc.idl.pvd {
    exports io.pvdnc.idl.pvd;

    requires transitive io.pvdnc.core;
    requires transitive io.pvdnc.idl.parser;

    requires net.morimekta.collect;
    requires net.morimekta.strings;
    requires net.morimekta.lexer;
    requires net.morimekta.file;

    provides IDLParserProvider with ProvidenceParserProvider;
}