/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.pvd.util;

import net.morimekta.collect.UnmodifiableSet;

import java.util.Set;

public final class ProvidenceConstants {
    private ProvidenceConstants() {}

    public static final Set<String> kProvidenceKeywords = UnmodifiableSet.<String>newBuilder()
            .add("include")
            .add("option")
            .add("bool")
            .add("byte")
            .add("short")
            .add("int")
            .add("long")
            .add("float")
            .add("double")
            .add("string")
            .add("binary")
            .add("list")
            .add("set")
            .add("map")
            .add("enum")
            .add("struct")
            .add("union")
            .add("exception")
            .add("interface")
            .add("of")
            .add("const")
            .add("typedef")
            .add("service")
            .add("extends")
            .add("implements")
            .build();

}
