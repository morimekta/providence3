/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.pvd.model;

import io.pvdnc.core.types.PMessageVariant;
import io.pvdnc.idl.parser.IDLToken;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * <pre>{@code
 * variant ::= 'struct' | 'union' | 'exception' | 'interface'
 * message ::= {variant} {name} (('implements' | 'of') {implementing})? '{' {field}* '}' {annotations}?
 * }</pre>
 */
public class MessageDeclaration extends Declaration {
    private final IDLToken               variantToken;
    private final IDLToken               extendsToken;
    private final List<IDLToken>         implementsTokens;
    private final List<FieldDeclaration> fields;

    public MessageDeclaration(String documentation,
                              IDLToken variantToken,
                              IDLToken nameToken,
                              IDLToken extendsToken,
                              List<IDLToken> implementsTokens,
                              List<FieldDeclaration> fields,
                              List<AnnotationDeclaration> annotations) {
        super(documentation, nameToken, annotations);
        this.variantToken = requireNonNull(variantToken, "variant == null");
        this.extendsToken = extendsToken;
        this.implementsTokens = requireNonNull(implementsTokens, "implements == null");
        this.fields = requireNonNull(fields);
    }

    public IDLToken getExtendsToken() {
        return extendsToken;
    }

    public List<IDLToken> getImplementsTokens() {
        return implementsTokens;
    }

    public List<FieldDeclaration> getFields() {
        return fields;
    }

    public PMessageVariant getVariant() {
        switch (variantToken.toString()) {
            case "union": return PMessageVariant.UNION;
            case "exception": return PMessageVariant.EXCEPTION;
        }
        return PMessageVariant.STRUCT;
    }

    public IDLToken getVariantToken() {
        return variantToken;
    }

    // --- Displayable

    @Override
    public String displayString() {
        StringBuilder builder = new StringBuilder(getVariantToken() + " " + getName());
        if (getExtendsToken() != null) {
            builder.append("\n        extends ").append(getExtendsToken());
        }
        if (!getImplementsTokens().isEmpty()) {
            boolean firstImpl = true;
            for (IDLToken impl : getImplementsTokens()) {
                if (firstImpl) {
                    builder.append("\n");
                    builder.append("        implements ");
                    firstImpl = false;
                } else {
                    builder.append(",\n");
                    builder.append("                   ");
                }
                builder.append(impl);
            }
        }
        builder.append(" {\n");
        for (FieldDeclaration field : fields) {
            builder.append("    ")
                   .append(field.displayString())
                   .append('\n');
        }
        builder.append("}");
        return builder.toString();
    }

    // --- Object

    @Override
    public String toString() {
        return "Message{" + fields.stream().map(Declaration::getName).collect(Collectors.joining(",")) + "}";
    }
}
