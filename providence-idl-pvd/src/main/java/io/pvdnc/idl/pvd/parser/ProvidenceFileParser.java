/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.pvd.parser;

import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.types.PMessageVariant;
import io.pvdnc.idl.parser.IDLException;
import io.pvdnc.idl.parser.IDLLexer;
import io.pvdnc.idl.parser.IDLToken;
import io.pvdnc.idl.parser.IDLTokenType;
import io.pvdnc.idl.parser.IDLWarningHandler;
import io.pvdnc.idl.pvd.ProvidenceParserProperties;
import io.pvdnc.idl.pvd.model.AnnotationDeclaration;
import io.pvdnc.idl.pvd.model.ConstDeclaration;
import io.pvdnc.idl.pvd.model.Declaration;
import io.pvdnc.idl.pvd.model.EnumDeclaration;
import io.pvdnc.idl.pvd.model.EnumValueDeclaration;
import io.pvdnc.idl.pvd.model.FieldDeclaration;
import io.pvdnc.idl.pvd.model.FileDeclaration;
import io.pvdnc.idl.pvd.model.IncludeDeclaration;
import io.pvdnc.idl.pvd.model.MessageDeclaration;
import io.pvdnc.idl.pvd.model.MethodDeclaration;
import io.pvdnc.idl.pvd.model.OptionDeclaration;
import io.pvdnc.idl.pvd.model.ServiceDeclaration;
import io.pvdnc.idl.pvd.model.TypedefDeclaration;
import io.pvdnc.idl.pvd.util.ProvidenceConstants;
import net.morimekta.strings.NamingUtil;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntSupplier;

import static java.util.Locale.US;
import static java.util.Objects.requireNonNull;
import static net.morimekta.file.PathUtil.getFileName;
import static io.pvdnc.idl.parser.IDLToken.kEntrySep;
import static io.pvdnc.idl.parser.IDLToken.kFieldValueSep;
import static io.pvdnc.idl.parser.IDLToken.kKeyValueSep;
import static io.pvdnc.idl.parser.IDLToken.kLineSep;
import static io.pvdnc.idl.parser.IDLToken.kListEnd;
import static io.pvdnc.idl.parser.IDLToken.kListStart;
import static io.pvdnc.idl.parser.IDLToken.kMessageEnd;
import static io.pvdnc.idl.parser.IDLToken.kMessageStart;
import static io.pvdnc.idl.parser.IDLToken.kParamsEnd;
import static io.pvdnc.idl.parser.IDLToken.kParamsStart;
import static io.pvdnc.idl.pvd.util.ProvidenceFileUtil.namespaceFromFileName;
import static net.morimekta.strings.EscapeUtil.javaEscape;
import static net.morimekta.strings.NamingUtil.Format.CAMEL;

public class ProvidenceFileParser {
    private boolean allowThriftCompat;
    private boolean allowNoIdFields;
    private boolean allowNoIdEnums;

    public ProvidenceFileParser(PPropertyMap properties) {
        requireNonNull(properties, "properties == null");
        this.allowThriftCompat = properties.getPropertyFor(
                ProvidenceParserProperties.ALLOW_THRIFT_COMPAT,
                PPropertyMap.empty());
        this.allowNoIdFields = properties.getPropertyFor(
                ProvidenceParserProperties.ALLOW_NO_ID_FIELDS,
                PPropertyMap.empty());
        this.allowNoIdEnums = properties.getPropertyFor(
                ProvidenceParserProperties.ALLOW_NO_ID_ENUMS,
                PPropertyMap.empty());
    }

    public boolean isAllowThriftCompat() {
        return allowThriftCompat;
    }

    public FileDeclaration parse(InputStream in,
                                 String fileName,
                                 IDLWarningHandler warningHandler) throws IOException {
        requireNonNull(in, "in == null");
        requireNonNull(fileName, "fileName == null");
        try {
            return parseInternal(in, fileName, warningHandler);
        } catch (IDLException e) {
            if (e.getFile() == null) {
                e.setFile(fileName);
            }
            throw e;
        }
    }

    private FileDeclaration parseInternal(InputStream in, String fileName,
                                          IDLWarningHandler warningHandler) throws IOException {
        String                   namespaceDocumentation = null;
        List<IncludeDeclaration> includes               = new ArrayList<>();
        List<OptionDeclaration>  namespaces             = new ArrayList<>();
        List<Declaration>        declarations           = new ArrayList<>();

        String      programName      = namespaceFromFileName(fileName);
        Set<String> knownNamespaces = new HashSet<>();
        IDLLexer    lexer            = new IDLLexer(new ProvidenceTokenizer(in));

        boolean hasDeclaration = false;

        String   documentation = null;
        IDLToken token;
        while ((token = lexer.next()) != null) {
            if (token.type() == IDLTokenType.DOCUMENTATION) {
                if (documentation != null && !hasDeclaration) {
                    namespaceDocumentation = documentation;
                }
                documentation = token.parseDocumentation();
                continue;
            }

            switch (token.toString()) {
                case ProvidenceTokenizer.kOption:
                    if (hasDeclaration) {
                        throw lexer.failure(token, "Unexpected token 'option', expected type declaration");
                    }
                    parseOption(lexer, token, namespaces);
                    break;
                case ProvidenceTokenizer.kNamespace:
                    if (hasDeclaration) {
                        throw lexer.failure(token, "Unexpected token 'namespace', expected type declaration");
                    }
                    parseNamespace(lexer, token, namespaces, warningHandler);
                    break;
                case ProvidenceTokenizer.kInclude:
                    if (hasDeclaration) {
                        throw lexer.failure(token, "Unexpected token 'include', expected type declaration");
                    }
                    includes.add(parseInclude(lexer, token, knownNamespaces));
                    break;
                case ProvidenceTokenizer.kEnum:
                    hasDeclaration = true;
                    declarations.add(parseEnum(lexer, token, documentation));
                    documentation = null;
                    break;
                case ProvidenceTokenizer.kTypeDef:
                    hasDeclaration = true;
                    declarations.add(parseTypeDef(knownNamespaces, lexer, token, documentation));
                    documentation = null;
                    break;
                case ProvidenceTokenizer.kStruct:
                case ProvidenceTokenizer.kUnion:
                case ProvidenceTokenizer.kException:
                case ProvidenceTokenizer.kInterface:
                    hasDeclaration = true;
                    declarations.add(parseMessage(knownNamespaces, lexer, token, documentation, warningHandler));
                    documentation = null;
                    break;
                case ProvidenceTokenizer.kService:
                    hasDeclaration = true;
                    declarations.add(parseService(lexer, token, documentation));
                    documentation = null;
                    break;
                case ProvidenceTokenizer.kConst:
                    hasDeclaration = true;
                    declarations.add(parseConst(knownNamespaces, lexer, token, documentation));
                    documentation = null;
                    break;
                default:
                    throw lexer.failure(token, "Unexpected token '%s'", javaEscape(token.toString()));
            }
        }

        return new FileDeclaration(
                namespaceDocumentation,
                programName,
                includes,
                namespaces,
                declarations);
    }

    /**
     * <pre>{@code
     * typedef ::= 'typedef' {name} {type}
     * }</pre>
     *
     *
     * @param knownNamespaces Included namespaces.
     * @param lexer The tokenizer.
     * @param documentation The documentation.
     * @return The typedef declaration.
     * @throws IOException If unable to parse typedef.
     */
    private Declaration parseTypeDef(Set<String> knownNamespaces,
                                     IDLLexer lexer,
                                     IDLToken token,
                                     String documentation) throws IOException {
        List<IDLToken> type = parseType(knownNamespaces, lexer, lexer.expect("typename"));
        IDLToken       name = lexer.expect("typedef identifier", IDLToken::isIdentifier);
        if (forbiddenNameIdentifier(name.toString())) {
            throw lexer.failure(name, "Typedef with reserved name: " + name);
        }
        List<AnnotationDeclaration> annotations = getAnnotationDeclarations(lexer);

        return new TypedefDeclaration(documentation, token, name, type, annotations);
    }

    /**
     * <pre>{@code
     * service ::= 'service' {name} ('extends' {extending})? '{' {method}* '}' {annotations}?
     * }</pre>
     *
     * @param lexer The tokenizer.
     * @param serviceToken The 'service' token.
     * @param documentation The service documentation string.
     * @return The service declaration.
     * @throws IOException If unable to parse the service.
     */
    private ServiceDeclaration parseService(
            IDLLexer lexer,
            IDLToken serviceToken,
            String documentation) throws IOException {
        IDLToken name = lexer.expect("service name", IDLToken::isIdentifier);
        if (forbiddenNameIdentifier(name.toString())) {
            throw lexer.failure(name, "Service with reserved name: " + name);
        }

        List<IDLToken> extending = new ArrayList<>();
        IDLToken next      = lexer.expect("service start or extends");
        if (next.toString().equals(ProvidenceTokenizer.kExtends)) {
            extending.add(lexer.expect("extending type", IDLToken::isReferenceIdentifier));
            while (lexer.expectSymbol("service start", kMessageStart, '&').isSymbol('&')) {
                extending.add(lexer.expect("extending type", IDLToken::isReferenceIdentifier));
            }
        }

        String                  doc_string = null;
        List<MethodDeclaration> functions  = new ArrayList<>();
        next = lexer.expect("method or service end");
        while (!next.isSymbol(kMessageEnd)) {
            if (next.type() == IDLTokenType.DOCUMENTATION) {
                doc_string = next.parseDocumentation();
                next = lexer.expect("function or service end");
                continue;
            }

            functions.add(parseMethod(lexer, next, doc_string));
            next = lexer.expect("method or service end");
        }

        List<AnnotationDeclaration> annotations = getAnnotationDeclarations(lexer);

        return new ServiceDeclaration(documentation, serviceToken, name, extending, functions, annotations);
    }

    /**
     * <pre>{@code
     * method ::= {response} {name} '(' {request} ')' {annotations}? ';'
     * }</pre>
     *
     * @param lexer The tokenizer.
     * @param responseType The first token of the method declaration.
     * @param documentation The method documentation.
     * @return The method declaration.
     * @throws IOException If unable to parse the method.
     */
    private MethodDeclaration parseMethod(IDLLexer lexer,
                                          IDLToken responseType,
                                          String documentation)
            throws IOException {
        IDLToken name         = lexer.expect("method name", IDLToken::isIdentifier);
        lexer.expectSymbol("params start", kParamsStart);
        IDLToken requestType = lexer.expect("request type", IDLToken::isReferenceIdentifier);
        lexer.expectSymbol("params end", kParamsEnd);
        List<AnnotationDeclaration> annotations = null;
        if (lexer.peek("annotation or service end").isSymbol(kParamsStart)) {
            lexer.next();
            annotations = parseAnnotations(lexer);
        }
        lexer.expectSymbol("service method end", kLineSep);
        return new MethodDeclaration(documentation, responseType, name, requestType, annotations);
    }

    /**
     * <pre>{@code
     * option ::= 'option' {name} '=' '"' {value} '"' ';'
     * }</pre>
     *
     * @param lexer The tokenizer.
     * @param optionToken The namespace token.
     * @param options List of already declared namespaces.
     * @throws IOException If unable to parse namespace.
     */
    private void parseOption(IDLLexer lexer,
                             IDLToken optionToken,
                             List<OptionDeclaration> options)
            throws IOException {
        IDLToken name = lexer.expect("option name",
                                            IDLToken::isReferenceIdentifier);
        if (options.stream().anyMatch(ns -> ns.getName().equals(name.toString()))) {
            throw lexer.failure(name,
                                    "Option %s already defined.",
                                    name.toString());
        }
        lexer.expectSymbol("option-value sep", kFieldValueSep);

        IDLToken value = lexer.expect(
                "option value",
                IDLTokenType.STRING);
        if (name.toString().equals(ProvidenceParserProperties.ALLOW_THRIFT_COMPAT.getName())) {
            allowThriftCompat = Boolean.parseBoolean(value.decodeString(false));
        }
        if (name.toString().equals(ProvidenceParserProperties.ALLOW_NO_ID_FIELDS.getName())) {
            allowNoIdFields = Boolean.parseBoolean(value.decodeString(false));
        }
        if (name.toString().equals(ProvidenceParserProperties.ALLOW_NO_ID_ENUMS.getName())) {
            allowNoIdEnums = Boolean.parseBoolean(value.decodeString(false));
        }

        if (lexer.hasNext() &&
            lexer.peek("next").isSymbol(kLineSep)) {
            lexer.next();
        } else {
            throw lexer.failure(value, "No ';' after option value.");
        }

        options.add(new OptionDeclaration(optionToken, name, value));
    }


    /**
     * <pre>{@code
     * option ::= 'option' {name} '=' '"' {value} '"' ';'
     * }</pre>
     *
     * @param lexer The tokenizer.
     * @param optionToken The namespace token.
     * @param options List of already declared namespaces.
     * @param warningHandler Warning handler for IDL warnings.
     * @throws IOException If unable to parse namespace.
     */
    private void parseNamespace(IDLLexer lexer,
                                IDLToken optionToken,
                                List<OptionDeclaration> options,
                                IDLWarningHandler warningHandler)
            throws IOException {
        IDLToken name = lexer.expect("language",
                                     IDLToken::isIdentifier);
        if (options.stream().anyMatch(ns -> ns.getName().equals(name.toString() + ".namespace"))) {
            throw lexer.failure(name,
                                "Namespace %s already defined.",
                                name.toString());
        }

        IDLToken namespace = lexer.expect(
                "option value",
                IDLTokenType.IDENTIFIER);
        if (!allowThriftCompat) {
            throw new IDLException(
                    optionToken, "Namespaces should be declared as options: 'option %s.namespace \"%s\";'",
                    name, namespace);
        }
        warningHandler.handleWarning(new IDLException(
                optionToken, "Namespaces should be declared as options: 'option %s.namespace \"%s\";'",
                name, namespace));

        if (lexer.hasNext() &&
            lexer.peek("next").isSymbol(kLineSep)) {
            lexer.next();
        } else {
            warningHandler.handleWarning(new IDLException(
                    namespace, "No ';' after namespace."));
        }
        options.add(new OptionDeclaration(optionToken, name, namespace));
    }

    /**
     * <pre>{@code
     * include ::= 'include' '"' {file} '"' ';'
     * }</pre>
     *
     * @param lexer The tokenizer.
     * @param includeToken The include token.
     * @param includedPrograms Set of included program names.
     * @return The include declaration.
     * @throws IOException If unable to parse the declaration.
     */
    private IncludeDeclaration parseInclude(IDLLexer lexer,
                                            IDLToken includeToken,
                                            Set<String> includedPrograms) throws IOException {
        IDLToken include  = lexer.expect("include file", IDLTokenType.STRING);
        String          fileName = getFileName(Path.of(include.decodeString(true)));
        IDLToken namespaceToken = null;
        String includedProgram;
        if (lexer.peek("end include or 'as'").toString().equals(ProvidenceTokenizer.kAs)) {
            lexer.next();
            namespaceToken = lexer.expect("namespace name", IDLTokenType.IDENTIFIER);
            includedProgram = namespaceToken.toString();
        } else {
            includedProgram = namespaceFromFileName(fileName);
        }
        if (includedPrograms.contains(includedProgram)) {
            throw lexer.failure(include, "namespace '" + includedProgram + "' already included");
        }
        includedPrograms.add(includedProgram);
        lexer.expectSymbol("end include", kLineSep);

        return new IncludeDeclaration(includeToken, include, namespaceToken);
    }

    /**
     * <pre>{@code
     * enum ::= 'enum' {name} '{' {enum_value}* '}'
     * }</pre>
     *
     * @param lexer Tokenizer to parse the enum.
     * @param enumToken The enum starting token.
     * @param documentation The enum documentation.
     * @return The enum declaration.
     * @throws IOException If unable to parse the enum.
     */
    private EnumDeclaration parseEnum(IDLLexer lexer,
                                      IDLToken enumToken,
                                      String documentation) throws IOException {
        IDLToken name = lexer.expect("enum name", IDLToken::isIdentifier);
        if (forbiddenNameIdentifier(name.toString())) {
            throw lexer.failure(name, "Enum with reserved name: %s", name);
        }
        lexer.expectSymbol("enum start", kMessageStart);

        List<EnumValueDeclaration> values = new ArrayList<>();

        String doc_string = null;
        IDLToken next = lexer.expect("enum value or end");
        while (!next.isSymbol(kMessageEnd)) {
            if (next.type() == IDLTokenType.DOCUMENTATION) {
                doc_string = next.parseDocumentation();
                next = lexer.expect("enum value or end");
                continue;
            }

            IDLToken valueName = next;
            IDLToken valueId   = null;
            next = lexer.expect("enum value sep or end");
            Integer id = null;
            if (next.isSymbol(kFieldValueSep)) {
                valueId = lexer.expect("enum value id", IDLToken::isNonNegativeID);
                next = lexer.expect("enum value or end");
            }
            if (!allowNoIdEnums && valueId == null) {
                throw lexer.failure(valueName, "Explicit enum value required, but missing");
            } else if (valueId != null) {
                id = valueId.parseInteger();
            }

            List<AnnotationDeclaration> annotations = null;
            if (next.isSymbol(kParamsStart)) {
                annotations = parseAnnotations(lexer);
                next = lexer.expect("enum value or end");
            }

            if (next.isSymbol(kEntrySep) ||
                next.isSymbol(kLineSep)) {
                next = lexer.expect("enum value or end");
            }

            values.add(new EnumValueDeclaration(doc_string, valueName, valueId, id, annotations));
            doc_string = null;
        }

        List<AnnotationDeclaration> annotations = getAnnotationDeclarations(lexer);

        return new EnumDeclaration(documentation, enumToken, name, values, annotations);
    }

    private List<AnnotationDeclaration> getAnnotationDeclarations(IDLLexer lexer) throws IOException {
        List<AnnotationDeclaration> annotations = null;
        if (lexer.hasNext() && lexer.peek("next").isSymbol(kParamsStart)) {
            lexer.next();
            annotations = parseAnnotations(lexer);
        }

        if (lexer.hasNext() && lexer.peek("next").isSymbol(kLineSep)) {
            lexer.next();
        }
        return annotations;
    }

    /**
     * <pre>{@code
     * message ::= {variant} {name} (('of' | 'implements') {implementing})? '{' {field}* '}'
     * }</pre>
     *
     *
     * @param includedNamespaces Set of included namespaces.
     * @param lexer Tokenizer.
     * @param messageVariantToken The message variant.
     * @param documentation The message documentation.
     * @param warningHandler The IDL warning handler.
     * @return The parsed message declaration.
     * @throws IOException If unable to parse the message.
     */
    private MessageDeclaration parseMessage(Set<String> includedNamespaces,
                                            IDLLexer lexer,
                                            IDLToken messageVariantToken,
                                            String documentation,
                                            IDLWarningHandler warningHandler)
            throws IOException {
        PMessageVariant messageVariant = PMessageVariant.variantForName(messageVariantToken.toString());
        List<FieldDeclaration> fields = new ArrayList<>();

        IDLToken name = lexer.expect("message name identifier", IDLToken::isIdentifier);
        if (forbiddenNameIdentifier(name.toString())) {
            throw lexer.failure(name, "Message with reserved name: %s", name);
        }

        IDLToken extendsToken = null;
        List<IDLToken> implementsTokens = new ArrayList<>();
        IntSupplier autoIdSupplier = allowNoIdFields ? new AtomicInteger()::decrementAndGet : null;
        if (messageVariant == PMessageVariant.UNION) {
            if (lexer.peek("message start of union-of").toString().equals(ProvidenceTokenizer.kOf)) {
                implementsTokens.add(lexer.expect("implements ref", IDLToken::isReferenceIdentifier));
            }
        } else if (messageVariant == PMessageVariant.INTERFACE) {
            if (lexer.peek("message start or extends").toString().equals(ProvidenceTokenizer.kExtends)) {
                do {
                    lexer.next();
                    implementsTokens.add(lexer.expect("implements ref", IDLToken::isReferenceIdentifier));
                } while (lexer.peek("message start or more").isSymbol('&'));
            }
            autoIdSupplier = new AtomicInteger()::getAndIncrement;
        } else {
            if (lexer.peek("message start or extends").toString().equals(ProvidenceTokenizer.kExtends)) {
                lexer.next();
                extendsToken = lexer.expect("extends ref", IDLToken::isReferenceIdentifier);
            }

            if (lexer.peek("message start or extends").toString().equals(ProvidenceTokenizer.kImplements)) {
                do {
                    lexer.next();
                    implementsTokens.add(lexer.expect("implements ref", IDLToken::isReferenceIdentifier));
                } while (lexer.peek("message start or more").isSymbol('&'));
            }
        }

        lexer.expectSymbol("message start", kMessageStart);

        Map<String, IDLToken> normalizedNames = new HashMap<>();
        Map<String, IDLToken> exactNames = new HashMap<>();
        Map<Integer, IDLToken> fieldIds = new HashMap<>();

        String docString = null;
        IDLToken next = lexer.expect("field def or message end");
        AnnotationDeclaration oneOf = null;

        while (!next.isSymbol(kMessageEnd)) {
            if (next.type() == IDLTokenType.DOCUMENTATION) {
                docString = next.parseDocumentation();
                next = lexer.expect("field def or message end");
                continue;
            }
            if (ProvidenceTokenizer.kOneOf.equals(next.toString())) {
                if (messageVariant == PMessageVariant.UNION) {
                    throw new IDLException(next, "one_of not allowed in unions, it is already a one_of by itself.");
                }
                if (oneOf != null) {
                    throw new IDLException(next, "Declaring one_of inside one_of " + oneOf);
                }
                IDLToken oneOfToken = next;
                IDLToken oneOfNameToken = lexer.expect("one_of name", IDLTokenType.IDENTIFIER);
                String of = oneOfNameToken.toString();
                if ("get".equals(of) || "set".equals(of) || "is".equals(of) || "has".equals(of)
                        || ProvidenceConstants.kProvidenceKeywords.contains(of)) {
                    throw new IDLException(of, "Invalid one_of name");
                }
                oneOf = new AnnotationDeclaration(oneOfToken, oneOfNameToken);
                lexer.expectSymbol("one of start", kMessageStart);
                next = lexer.expect("first one_of field");
            }

            FieldDeclaration fieldDeclaration = parseField(
                    messageVariant,
                    includedNamespaces,
                    lexer,
                    next,
                    autoIdSupplier,
                    warningHandler,
                    oneOf,
                    docString);

            if (fieldIds.containsKey(fieldDeclaration.getId())) {
                IDLToken other = fieldIds.get(fieldDeclaration.getId());
                throw new IDLException(fieldDeclaration.getIdToken(), "Field with id " + fieldDeclaration.getId() + " already declared on line %d.", other.lineNo());
            }
            fieldIds.put(fieldDeclaration.getId(), fieldDeclaration.getIdToken());

            if (exactNames.containsKey(fieldDeclaration.getName())) {
                IDLToken other = exactNames.get(fieldDeclaration.getName());
                throw new IDLException(fieldDeclaration.getNameToken(), "Field with name " + fieldDeclaration.getName() + " already declared on line %d.", other.lineNo());
            }
            exactNames.put(fieldDeclaration.getName(), fieldDeclaration.getNameToken());

            String normalizedName = NamingUtil.format(fieldDeclaration.getName(), CAMEL);
            if (normalizedNames.containsKey(normalizedName)) {
                IDLToken other = normalizedNames.get(normalizedName);
                throw new IDLException(fieldDeclaration.getNameToken(), "Field with conflicting name " + other.toString() + " already declared on line %d.", other.lineNo());
            }
            normalizedNames.put(normalizedName, fieldDeclaration.getNameToken());

            fields.add(fieldDeclaration);

            docString = null;
            next = lexer.expect("field def or message end");
            if (oneOf != null && next.isSymbol(kMessageEnd)) {
                oneOf = null;
                next = lexer.expect("field def or message end");
            }
        }

        List<AnnotationDeclaration> annotations = getAnnotationDeclarations(lexer);
        return new MessageDeclaration(documentation, messageVariantToken, name, extendsToken, implementsTokens, fields, annotations);
    }

    /**
     * <pre>{@code
     * field ::= ({id} ':')? {requirement}? {type} {name} ('=' {defaultValue})? {annotations}?
     * }</pre>
     *
     * @param variant The field variant to parse field for.
     * @param includedNamespaces Included program names.
     * @param lexer The tokenizer.
     * @param oneOf One of annotation  for the field.
     * @param documentation The field documentation.
     * @return The field declaration.
     * @throws IOException If unable to read the declaration.
     */
    private FieldDeclaration parseField(PMessageVariant variant,
                                        Set<String> includedNamespaces,
                                        IDLLexer lexer,
                                        IDLToken next,
                                        IntSupplier autoIdSupplier,
                                        IDLWarningHandler warningHandler,
                                        AnnotationDeclaration oneOf,
                                        String documentation) throws IOException {
        int             fieldId;
        IDLToken idToken = null;
        if (next.isNonNegativeID()) {
            if (variant == PMessageVariant.INTERFACE) {
                throw lexer.failure(next, "Field ID's not allowed on interfaces.");
            }
            idToken = next;
            fieldId = idToken.parseInteger();
            lexer.expectSymbol("field id sep", kKeyValueSep);
            next = lexer.expect("field type");
        } else if (autoIdSupplier == null) {
            throw lexer.failure(next, "Field ID missing");
        } else {
            fieldId = autoIdSupplier.getAsInt();
        }
        IDLToken requirement = null;
        if (allowThriftCompat && "optional".equals(next.toString())) {
            requirement = next;
            next = lexer.next();
            warningHandler.handleWarning(new IDLException(requirement, "Optional values is the default and deprecated in declaration, ignored"));
        } else if (allowThriftCompat && "required".equals(next.toString())) {
            requirement = next;
            next = lexer.next();
            warningHandler.handleWarning(new IDLException(requirement, "Required field values are deprecated, use 'field.requirement' annotation; setting to 'default_out'."));
        }

        List<IDLToken>       type         = parseType(includedNamespaces, lexer, next);
        IDLToken             name         = lexer.expect("field name", IDLToken::isIdentifier);
        List<IDLToken>       defaultValue = null;
        List<AnnotationDeclaration> annotations  = null;

        if (lexer.peek("field default value").isSymbol(kFieldValueSep)) {
            lexer.next();
            defaultValue = parseValue(lexer);
        }
        if (lexer.peek("field annotation").isSymbol(kParamsStart)) {
            lexer.next();
            annotations = parseAnnotations(lexer);
        }
        if (oneOf != null) {
            if (annotations == null) annotations = new ArrayList<>();
            annotations.add(oneOf);
        }

        if (!lexer.peek("field line sep").isSymbol(kLineSep)) {
            if (allowThriftCompat) {
                if (lexer.peek("field line sep").isSymbol(kEntrySep)) {
                    warningHandler.handleWarning(new IDLException(lexer.next(), "Deprecated ',' separator after field declaration, use ';'"));
                } else {
                    warningHandler.handleWarning(new IDLException(lexer.last(), "Missing ';' after field declaration"));
                }
            } else {
                throw new IDLException(lexer.last(), "Missing ';' after field declaration");
            }
        } else {
            lexer.next();
        }

        return new FieldDeclaration(documentation, idToken, requirement, fieldId, name, type, defaultValue, annotations);
    }

    /**
     * <pre>{@code
     * const type kName = value;
     * }</pre>
     *
     *
     * @param includedNamespaces Set of included namespaces.
     * @param lexer The tokenizer.
     * @return The parsed const declaration.
     * @throws IOException If parsing failed.
     */
    private ConstDeclaration parseConst(Set<String> includedNamespaces,
                                        IDLLexer lexer,
                                        IDLToken constToken,
                                        String documentation) throws IOException {
        requireNonNull(lexer);
        requireNonNull(constToken);

        List<IDLToken> type = parseType(includedNamespaces, lexer, lexer.expect("const type"));
        IDLToken       name = lexer.expect("const name", IDLToken::isIdentifier);
        if (forbiddenNameIdentifier(name.toString())) {
            throw lexer.failure(name, "Const with reserved name: " + name.toString());
        }

        lexer.expectSymbol("const value sep", kFieldValueSep);

        List<IDLToken> value = parseValue(lexer);

        List<AnnotationDeclaration> annotations = null;
        if (lexer.expectSymbol("end of const or annotations", kParamsStart, kLineSep).isSymbol(kParamsStart)) {
            annotations = parseAnnotations(lexer);
            lexer.expectSymbol("end of const", kLineSep);
        }
        return new ConstDeclaration(documentation, constToken, name, type, value, annotations);
    }

    /**
     * Parse annotations, not including the initial '('.
     *
     * <pre>{@code
     * annotation  ::= key ('=' value)?
     * annotations ::= '(' annotation (',' annotation)* ')'
     * }</pre>
     *
     * @param tokenizer The tokenizer to get tokens from.
     * @return The list of annotations.
     */
    private List<AnnotationDeclaration> parseAnnotations(IDLLexer tokenizer)
            throws IOException {
        requireNonNull(tokenizer, "tokenizer == null");

        List<AnnotationDeclaration> annotations = new ArrayList<>();
        IDLToken             next        = tokenizer.expect("annotation key");
        while (!next.isSymbol(kParamsEnd)) {
            if (!next.isReferenceIdentifier()) {
                throw tokenizer.failure(next, "annotation key must be identifier-like");
            }
            IDLToken key   = next;
            IDLToken value = null;
            next = tokenizer.expect("annotation end or sep");
            if (next.isSymbol(kFieldValueSep)) {
                value = tokenizer.expect("annotation value", IDLTokenType.STRING);
                next = tokenizer.expect("annotation end or sep");
            }
            annotations.add(new AnnotationDeclaration(key, value));
            if (next.isSymbol(kEntrySep)) {
                next = tokenizer.expect("annotation key", t -> !t.isSymbol(kParamsEnd));
            }
        }
        return annotations;
    }

    /**
     * @param lexer Tokenizer to parse value.
     * @return The list of tokens part of the value.
     * @throws IOException If unable to parse the type.
     */
    private List<IDLToken> parseValue(IDLLexer lexer) throws IOException {
        return parseValue(lexer, lexer.expect("value"));
    }

    /**
     * @param lexer Tokenizer to parse value.
     * @param token First token of the value.
     * @return The list of tokens part of the value.
     * @throws IOException If unable to parse the type.
     */
    private List<IDLToken> parseValue(IDLLexer lexer,
                                             IDLToken token) throws IOException {
        // Ignore these comments.
        while (token.type() == IDLTokenType.DOCUMENTATION) {
            token = lexer.expect("value");
        }

        List<IDLToken> tokens = new ArrayList<>();
        tokens.add(token);

        if (token.isSymbol(kListStart)) {
            IDLToken next = lexer.expect("list value");
            while (!next.isSymbol(kListEnd)) {
                tokens.addAll(parseValue(lexer, next));
                next = lexer.expect("list value, sep or end");
                if (next.isSymbol(kEntrySep) || next.isSymbol(kLineSep)) {
                    tokens.add(next);
                    next = lexer.expect("list value or end");
                }
            }
            tokens.add(next);
        } else if (token.isSymbol(kMessageStart)) {
            IDLToken next = lexer.expect("map key");
            while (!next.isSymbol(kMessageEnd)) {
                tokens.addAll(parseValue(lexer, next));
                tokens.add(lexer.expectSymbol("key value sep", kKeyValueSep));
                tokens.addAll(parseValue(lexer, lexer.expect("map value")));

                next = lexer.expect("map sep or end");
                if (next.isSymbol(kEntrySep) || next.isSymbol(kLineSep)) {
                    tokens.add(next);
                    next = lexer.expect("map sep or end");
                }
            }
            tokens.add(next);
        } else if (!token.isReferenceIdentifier() &&
                   token.type() != IDLTokenType.NUMBER &&
                   token.type() != IDLTokenType.STRING) {
            throw lexer.failure(token, "not a value type: '%s'", token.toString());
        }
        return tokens;
    }

    /**
     * @param lexer The tokenizer.
     * @param token The first token of the type.
     * @return List of tokens part of the type string.
     * @throws IOException If unable to parse the type.
     */
    private List<IDLToken> parseType(Set<String> includedNamespaces,
                                     IDLLexer lexer,
                                     IDLToken token) throws IOException {
        if (!token.isQualifiedIdentifier() &&
            !token.isIdentifier()) {
            throw lexer.failure(token, "Expected type identifier but found " + token);
        }
        if (token.isQualifiedIdentifier()) {
            String[] parts = token.toString().split("[.]", 2);
            if (!includedNamespaces.contains(parts[0])) {
                throw new IDLException(token, "No such include " + parts[0] + " for type " + token);
            }
        }

        List<IDLToken> tokens = new ArrayList<>();
        tokens.add(token);

        String type = token.toString();
        switch (type) {
            case "list":
            case "set": {
                tokens.add(lexer.expect(type + " generic start", t -> t.isSymbol(IDLToken.kGenericStart)));
                tokens.addAll(parseType(includedNamespaces, lexer, lexer.expect(type + " item type")));
                tokens.add(lexer.expect(type + " generic end", t -> t.isSymbol(IDLToken.kGenericEnd)));
                break;
            }
            case "map": {
                tokens.add(lexer.expect(type + " generic start", t -> t.isSymbol(IDLToken.kGenericStart)));
                tokens.addAll(parseType(includedNamespaces, lexer, lexer.expect(type + " key type")));
                tokens.add(lexer.expect(type + " generic sep", t -> t.isSymbol(kEntrySep)));
                tokens.addAll(parseType(includedNamespaces, lexer, lexer.expect(type + " item type")));
                tokens.add(lexer.expect(type + " generic end", t -> t.isSymbol(IDLToken.kGenericEnd)));
                break;
            }
            default: {
                break;
            }
        }
        return tokens;
    }

    private boolean forbiddenNameIdentifier(String name) {
        return ProvidenceConstants.kProvidenceKeywords.contains(name.toLowerCase(US));
    }
}
