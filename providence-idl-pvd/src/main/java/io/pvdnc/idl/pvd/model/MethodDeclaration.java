/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.pvd.model;

import io.pvdnc.idl.parser.IDLToken;

import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * <pre>{@code
 * function ::= 'oneway'? {type} {name} '(' {field}* ')' ('throws' '(' {field}* ')')? {annotations}?
 * }</pre>
 */
public class MethodDeclaration extends Declaration {
    private final IDLToken responseTypeToken;
    private final IDLToken requestTypeToken;

    public MethodDeclaration(String documentation,
                             IDLToken responseTypeToken,
                             IDLToken name,
                             IDLToken requestTypeToken,
                             List<AnnotationDeclaration> annotations) {
        super(documentation, name, annotations);
        this.responseTypeToken = requireNonNull(responseTypeToken, "request == null");
        this.requestTypeToken = requireNonNull(requestTypeToken, "response == null");
    }

    public IDLToken getResponseTypeToken() {
        return responseTypeToken;
    }

    public IDLToken getRequestTypeToken() {
        return requestTypeToken;
    }

    // --- Displayable

    @Override
    public String displayString() {
        return getResponseTypeToken().toString() + ' ' +
               getNameToken().toString() +
               '(' +
               getRequestTypeToken().toString() +
               ");";
    }

    // --- Object

    @Override
    public String toString() {
        return "Method{" + getName() + "}";
    }
}
