/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.pvd.model;

import io.pvdnc.idl.parser.IDLToken;
import net.morimekta.strings.Displayable;

import static java.util.Objects.requireNonNull;
import static io.pvdnc.idl.pvd.util.ProvidenceFileUtil.namespaceFromFileName;


/**
 * <pre>{@code
 * include ::= 'include' {file_path} ('as' {programName})?
 * }</pre>
 */
public class IncludeDeclaration implements Displayable {
    private final IDLToken includeToken;
    private final IDLToken filePath;
    private final IDLToken namespaceToken;

    public IncludeDeclaration(IDLToken includeToken,
                              IDLToken filePath,
                              IDLToken namespaceToken) {
        this.includeToken = requireNonNull(includeToken, "includeToken == null");
        this.filePath = requireNonNull(filePath, "filePath == null");
        this.namespaceToken = namespaceToken;
    }

    public IDLToken getIncludeToken() {
        return includeToken;
    }

    public String getFilePath() {
        return filePath.decodeString(false);
    }

    public IDLToken getFilePathToken() {
        return filePath;
    }

    public String getProgramName() {
        if (namespaceToken == null) {
            return namespaceFromFileName(filePath.decodeString(false));
        }
        return namespaceToken.toString();
    }

    // --- Displayable

    @Override
    public String displayString() {
        return "include \"" + getFilePath() + "\" as " + getProgramName() + ";";
    }

    @Override
    public String toString() {
        return "Include{" + getFilePath() + "}";
    }
}
