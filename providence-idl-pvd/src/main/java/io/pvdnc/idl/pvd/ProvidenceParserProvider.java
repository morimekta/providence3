/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.pvd;

import io.pvdnc.core.property.PProperty;
import io.pvdnc.idl.parser.IDLParser;
import io.pvdnc.idl.parser.IDLParserOptions;
import io.pvdnc.idl.parser.IDLParserProvider;
import io.pvdnc.idl.pvd.util.ProvidenceFileUtil;

import java.nio.file.Path;
import java.util.List;

import static net.morimekta.file.PathUtil.getFileName;

public class ProvidenceParserProvider implements IDLParserProvider {
    public static final String NAME = "providence";

    @Override
    public boolean willHandleFile(Path filePath) {
        return ProvidenceFileUtil.isProvidenceFileName(getFileName(filePath));
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public List<PProperty<?>> getOptions() {
        return List.of();
    }

    @Override
    public IDLParser getParser(IDLParserOptions options) {
        return new ProvidenceParser(options);
    }
}
