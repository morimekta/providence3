/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.pvd.model;

import io.pvdnc.idl.parser.IDLToken;
import io.pvdnc.idl.pvd.parser.ProvidenceTokenizer;
import net.morimekta.strings.Displayable;

import static java.util.Objects.requireNonNull;
import static net.morimekta.strings.EscapeUtil.javaEscape;

/**
 * Options work as annotations, but on the level of where it's contained.
 *
 * <pre>{@code
 * option :== 'option' {key} '=' '"' {value} '"' ';'
 * }</pre>
 */
public class OptionDeclaration implements Displayable {
    private final IDLToken optionToken;
    private final IDLToken nameToken;
    private final IDLToken valueToken;
    private final boolean isNamespace;

    public OptionDeclaration(IDLToken optionToken,
                             IDLToken nameToken,
                             IDLToken valueToken) {
        this.optionToken = requireNonNull(optionToken, "optionToken == null");
        this.nameToken = requireNonNull(nameToken, "keyToken == null");
        this.valueToken = requireNonNull(valueToken, "valueToken == null");
        this.isNamespace = getOptionToken().toString().equals(ProvidenceTokenizer.kNamespace);
    }

    public IDLToken getOptionToken() {
        return optionToken;
    }

    public String getName() {
        if (isNamespace) {
            return getNameToken().toString() + ".namespace";
        }
        return getNameToken().toString();
    }

    public IDLToken getNameToken() {
        return nameToken;
    }

    public String getValue() {
        if (isNamespace) {
            return getValueToken().toString();
        }
        return getValueToken().decodeString(true);
    }

    public IDLToken getValueToken() {
        return valueToken;
    }

    // --- Displayable

    @Override
    public String displayString() {
        return "option " + getName() + " = \"" + javaEscape(getValue()) + "\";";
    }

    // --- Object


    @Override
    public String toString() {
        return "Option{" + getName() + '}';
    }
}
