/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.pvd.model;

import io.pvdnc.idl.parser.IDLToken;
import io.pvdnc.idl.pvd.util.DeclarationUtil;

import java.util.List;

import static java.util.Objects.requireNonNull;
import static io.pvdnc.idl.pvd.util.DeclarationUtil.toTypeString;

/**
 * Declaration of a const value.
 *
 * <pre>{@code
 * const ::= 'const' {type} {name} '=' {value} {annotations}?;
 * }</pre>
 */
public class ConstDeclaration extends Declaration {
    private final List<IDLToken> typeTokens;
    private final List<IDLToken> valueTokens;
    private final IDLToken       constToken;

    public ConstDeclaration(String documentation,
                            IDLToken constToken,
                            IDLToken name,
                            List<IDLToken> typeTokens,
                            List<IDLToken> valueTokens,
                            List<AnnotationDeclaration> annotations) {
        super(documentation, name, annotations);
        this.constToken = requireNonNull(constToken, "constToken == null");
        this.typeTokens = requireNonNull(typeTokens, "typeTokens == null");
        this.valueTokens = requireNonNull(valueTokens, "valueTokens == null");
    }

    public IDLToken getConstToken() {
        return constToken;
    }

    public String getType() {
        return toTypeString(typeTokens);
    }

    public List<IDLToken> getTypeTokens() {
        return typeTokens;
    }

    public String getValue() {
        return DeclarationUtil.toValueString(valueTokens);
    }

    public List<IDLToken> getValueTokens() {
        return valueTokens;
    }

    // --- Displayable

    @Override
    public String displayString() {
        return "const " + toTypeString(typeTokens) + " " +
               getName() + " = " + getValue() + ";";
    }

    // --- Object


    @Override
    public String toString() {
        return "Const{" + getName() + "}";
    }
}
