/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.pvd;

import net.morimekta.file.FileUtil;
import net.morimekta.file.PathUtil;
import io.pvdnc.core.registry.PGlobalFileTypeRegistry;
import io.pvdnc.core.registry.PNamespaceFileTypeRegistry;
import io.pvdnc.idl.parser.IDLException;
import io.pvdnc.idl.parser.IDLParser;
import io.pvdnc.idl.parser.IDLParserOptions;
import io.pvdnc.idl.parser.IDLParserProvider;
import io.pvdnc.idl.parser.IDLWarningHandler;
import io.pvdnc.idl.pvd.model.FileDeclaration;
import io.pvdnc.idl.pvd.model.IncludeDeclaration;
import io.pvdnc.idl.pvd.parser.ProvidenceFileParser;
import io.pvdnc.idl.pvd.parser.ProvidenceModelConverter;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static net.morimekta.file.PathUtil.getFileName;
import static io.pvdnc.idl.pvd.util.ProvidenceFileUtil.isProvidenceFileName;
import static io.pvdnc.idl.pvd.util.ProvidenceFileUtil.namespaceFromFileName;

public class ProvidenceParser implements IDLParser {
    private final ProvidenceModelConverter modelConverter;
    private final IDLParserOptions         parserOptions;
    private final List<IDLParserProvider>  providers;

    public ProvidenceParser(IDLParserOptions parserOptions) {
        this(parserOptions, IDLParserProvider.getParserProviders());
    }

    public ProvidenceParser(IDLParserOptions parserOptions, List<IDLParserProvider> providers) {
        this.parserOptions = parserOptions;
        this.modelConverter = new ProvidenceModelConverter();
        this.providers = providers;
    }

    private IDLParserProvider providerFor(Path file, IncludeDeclaration declaration) throws IDLException {
        for (IDLParserProvider provider : providers) {
            if (provider.willHandleFile(file)) {
                return provider;
            }
        }
        throw new IDLException(declaration.getFilePathToken(), "No IDL handler for file: " + PathUtil.getFileName(file));
    }

    @Override
    public PNamespaceFileTypeRegistry parseFile(Path filePath, PGlobalFileTypeRegistry registry, IDLWarningHandler warningHandler) throws IOException {
        IDLWarningHandler fileWarningHandler = ex -> {
            ex.setFile(getFileName(filePath));
            warningHandler.handleWarning(ex);
        };
        Path parent = filePath.getParent();
        if (parent == null) {
            throw new IllegalStateException("Invalid file path: " + filePath);
        }

        PNamespaceFileTypeRegistry programRegistry = registry.getRegistryForPath(filePath).orElse(null);
        if (programRegistry != null) {
            return programRegistry;
        }

        FileDeclaration program;
        ProvidenceFileParser programParser = new ProvidenceFileParser(parserOptions.getParserOptions(ProvidenceParserProvider.NAME));
        try (InputStream in = Files.newInputStream(filePath)) {
            program = programParser.parse(in, PathUtil.getFileName(filePath), fileWarningHandler);
        }

        programRegistry = registry.computeRegistryForPath(filePath, namespaceFromFileName(getFileName(filePath)));
        for (IncludeDeclaration include : program.getIncludes()) {
            Path includePath = FileUtil.readCanonicalPath(parent.resolve(include.getFilePath()));
            if (!Files.exists(includePath)) {
                throw new IDLException(include.getFilePathToken(), "No such file to be included")
                        .setFile(getFileName(filePath));
            }

            PNamespaceFileTypeRegistry reg = registry.getRegistryForPath(includePath).orElse(null);
            if (reg == null) {
                if (isProvidenceFileName(includePath.toString())) {
                    reg = parseFile(includePath, registry, warningHandler);
                } else {
                    IDLParserProvider provider = providerFor(includePath, include);
                    IDLParser         parser   = provider.getParser(parserOptions);
                    reg = parser.parseFile(includePath, registry, warningHandler);
                }
            }
            if (reg.isBuilding()) {
                throw new IDLException(include.getFilePathToken(), "Circular dependency between " +
                                          getFileName(filePath) + " and " + getFileName(reg.getPath()))
                        .setFile(getFileName(filePath));
            }
            programRegistry.putInclude(include.getProgramName(), reg);
        }
        programRegistry.complete();
        modelConverter.convert(
                program,
                programRegistry,
                programParser.isAllowThriftCompat(),
                fileWarningHandler);
        return programRegistry;
    }
}
