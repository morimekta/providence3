/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.pvd;

import io.pvdnc.core.property.PPersistedProperty;
import io.pvdnc.core.property.PProperty;

public class ProvidenceParserProperties {
    /**
     * allow thrift compatibility features. If this is false (default) using thrift
     * compat features in .pvd files will cause parse errors, if true, it will cause
     * warnings to be logged.
     */
    public static final PProperty<Boolean> ALLOW_THRIFT_COMPAT;
    /**
     * Allow fields with out the field id declared. Assigned ID's will behave as
     * with thrift in that case. This is not considered a thrift 'compat' property
     * fir a consistency property.
     */
    public static final PProperty<Boolean> ALLOW_NO_ID_FIELDS;
    /**
     * Allow enum values without explicit ID value. Will fill in values starting at
     * the highest declared value + 1, or 0 if no value was declared.
     */
    public static final PProperty<Boolean> ALLOW_NO_ID_ENUMS;

    static {
        ALLOW_THRIFT_COMPAT = new PPersistedProperty<>("allowThriftCompat", Boolean.class, false);
        ALLOW_NO_ID_FIELDS = new PPersistedProperty<>("allowNoIdFields", Boolean.class, false);
        ALLOW_NO_ID_ENUMS = new PPersistedProperty<>("allowNoIdEnums", Boolean.class, false);
    }
}
