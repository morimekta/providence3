/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.pvd.parser;

import io.pvdnc.idl.pvd.model.AnnotationDeclaration;
import io.pvdnc.idl.pvd.model.ConstDeclaration;
import io.pvdnc.idl.pvd.model.Declaration;
import io.pvdnc.idl.pvd.model.EnumDeclaration;
import io.pvdnc.idl.pvd.model.EnumValueDeclaration;
import io.pvdnc.idl.pvd.model.FieldDeclaration;
import io.pvdnc.idl.pvd.model.FileDeclaration;
import io.pvdnc.idl.pvd.model.MessageDeclaration;
import io.pvdnc.idl.pvd.model.MethodDeclaration;
import io.pvdnc.idl.pvd.model.OptionDeclaration;
import io.pvdnc.idl.pvd.model.ServiceDeclaration;
import io.pvdnc.idl.pvd.model.TypedefDeclaration;
import io.pvdnc.idl.pvd.util.DeclarationUtil;
import net.morimekta.lexer.LexerException;
import net.morimekta.lexer.UncheckedLexerException;
import io.pvdnc.core.property.PDefaultProperties;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.reflect.CEnumDescriptor;
import io.pvdnc.core.reflect.CMessageDescriptor;
import io.pvdnc.core.reflect.CServiceDescriptor;
import io.pvdnc.core.registry.PNamespaceFileTypeRegistry;
import io.pvdnc.core.registry.PTypeReference;
import io.pvdnc.core.types.PConstDescriptor;
import io.pvdnc.core.types.PDeclarationType;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PServiceDescriptor;
import io.pvdnc.core.types.PServiceMethod;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.types.PType;
import io.pvdnc.core.types.PTypeDefDescriptor;
import io.pvdnc.idl.parser.IDLException;
import io.pvdnc.idl.parser.IDLToken;
import io.pvdnc.idl.parser.IDLWarningHandler;
import net.morimekta.strings.NamingUtil;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;
import static net.morimekta.collect.UnmodifiableMap.toMap;
import static io.pvdnc.core.property.PDefaultProperties.DOCUMENTATION;
import static io.pvdnc.core.property.PDefaultProperties.FIELD_REQUIREMENT;
import static io.pvdnc.core.property.PDefaultProperties.MESSAGE_VARIANT;
import static io.pvdnc.core.property.PDefaultProperties.REF_ARGUMENTS;
import static io.pvdnc.core.property.PDefaultProperties.REF_ENUM;
import static io.pvdnc.core.property.PDefaultProperties.SORTED;
import static io.pvdnc.core.registry.PTypeReference.ref;
import static io.pvdnc.core.types.PFieldRequirement.DEFAULT_OUT;
import static io.pvdnc.idl.parser.IDLUtil.toSingleToken;
import static net.morimekta.strings.NamingUtil.format;

public class ProvidenceModelConverter {
    public void convert(FileDeclaration program,
                        PNamespaceFileTypeRegistry registry,
                        boolean allowThriftCompat,
                        IDLWarningHandler warningHandler) throws IDLException {
        PPropertyMap.Builder properties = PPropertyMap.newBuilder();
        for (OptionDeclaration option : program.getOptions()) {
            properties.putUnsafe(option.getName(), option.getValue());
        }
        if (program.getDocumentation() != null) {
            properties.put(DOCUMENTATION, program.getDocumentation());
        }
        registry.setProperties(properties.build());

        // 1: list types that may be referenced
        Map<String, PDeclarationType> localTypes = new HashMap<>();
        for (Declaration declaration : program.getDeclarationList()) {
            if (declaration instanceof EnumDeclaration) {
                localTypes.put(declaration.getName(), PDeclarationType.ENUM);
            } else if (declaration instanceof MessageDeclaration) {
                localTypes.put(declaration.getName(), PDeclarationType.MESSAGE);
            } else if (declaration instanceof ServiceDeclaration) {
                localTypes.put(declaration.getName(), PDeclarationType.SERVICE);
            } else if (declaration instanceof ConstDeclaration) {
                localTypes.put(declaration.getName(), PDeclarationType.CONST);
            } else if (declaration instanceof TypedefDeclaration) {
                localTypes.put(declaration.getName(), PDeclarationType.TYPEDEF);
            }
        }

        // 2: register everything
        for (Declaration declaration : program.getDeclarationList()) {
            if (declaration instanceof EnumDeclaration) {
                registerEnum(program, (EnumDeclaration) declaration, registry);
            } else if (declaration instanceof MessageDeclaration) {
                registerMessage(localTypes, program, (MessageDeclaration) declaration, registry, allowThriftCompat, warningHandler);
            } else if (declaration instanceof ServiceDeclaration) {
                registerService(localTypes, program, (ServiceDeclaration) declaration, registry, allowThriftCompat, warningHandler);
            } else if (declaration instanceof ConstDeclaration) {
                registerConst(localTypes, (ConstDeclaration) declaration, registry, allowThriftCompat, warningHandler);
            } else if (declaration instanceof TypedefDeclaration) {
                registerTypedef(localTypes, (TypedefDeclaration) declaration, registry, allowThriftCompat, warningHandler);
            }
        }

        // 3: validate everything
        for (Declaration declaration : program.getDeclarationList()) {
            if (declaration instanceof EnumDeclaration) {
                validateEnum(program, (EnumDeclaration) declaration, registry);
            } else if (declaration instanceof MessageDeclaration) {
                validateMessage((MessageDeclaration) declaration, registry, warningHandler);
            } else if (declaration instanceof ServiceDeclaration) {
                validateService((ServiceDeclaration) declaration, registry);
            } else if (declaration instanceof ConstDeclaration) {
                validateConst((ConstDeclaration) declaration, registry);
            } else if (declaration instanceof TypedefDeclaration) {
                validateTypedef((TypedefDeclaration) declaration, registry);
            }
        }
    }

    private static void registerEnum(FileDeclaration program,
                                     EnumDeclaration declaration,
                                     PNamespaceFileTypeRegistry registry) {
        CEnumDescriptor.Builder builder = CEnumDescriptor.builder(
                program.getNamespace(), declaration.getName());
        for (EnumValueDeclaration valueDeclaration : declaration.getValues()) {
            PPropertyMap ppm = makeProperties(valueDeclaration.getAnnotations(), valueDeclaration.getDocumentation());
            if (valueDeclaration.getId() != null) {
                builder.value(valueDeclaration.getName(), valueDeclaration.getId(), ppm);
            }
        }
        for (EnumValueDeclaration valueDeclaration : declaration.getValues()) {
            PPropertyMap ppm = makeProperties(valueDeclaration.getAnnotations(), valueDeclaration.getDocumentation());
            if (valueDeclaration.getId() == null) {
                builder.value(valueDeclaration.getName(), ppm);
            }
        }
        builder.properties(makeProperties(declaration.getAnnotations(), declaration.getDocumentation()));
        registry.register(builder.build());
    }

    private static void validateEnum(FileDeclaration program,
                                     EnumDeclaration declaration,
                                     PNamespaceFileTypeRegistry registry) {
        // Nothing to do.
    }

    private static void registerMessage(Map<String, PDeclarationType> localTypes,
                                        FileDeclaration program,
                                        MessageDeclaration declaration,
                                        PNamespaceFileTypeRegistry registry,
                                        boolean allowThriftCompat,
                                        IDLWarningHandler warningHandler) throws IDLException {
        PPropertyMap messageProps = makeProperties(declaration.getAnnotations(), PPropertyMap.of(
                MESSAGE_VARIANT, declaration.getVariant()), declaration.getDocumentation());
        CMessageDescriptor.Builder builder = CMessageDescriptor.builder(program.getNamespace(),
                                                                        declaration.getName())
                                                               .properties(messageProps);
        if (declaration.getExtendsToken() != null) {
            builder.extending(DeclarationUtil.makeMessageTypeProvider(localTypes, registry, declaration.getExtendsToken()));
        }
        for (IDLToken impl : declaration.getImplementsTokens()) {
            builder.implementing(DeclarationUtil.makeMessageTypeProvider(localTypes, registry, impl));
        }
        for (FieldDeclaration field : declaration.getFields()) {
            PPropertyMap fieldProps = makeProperties(field.getAnnotations(),
                                                     PPropertyMap.of(),
                                                     field.getDocumentation());
            if (field.getRequirement() != null &&
                "required".equals(field.getRequirement().toString())) {
                fieldProps = PPropertyMap.newBuilder()
                                         .putAll(fieldProps)
                                         // 'required' is reduced to 'default out' for basic compatibility.
                                         .put(FIELD_REQUIREMENT, DEFAULT_OUT)
                                         .build();
            }
            Supplier<? extends PDescriptor> provider = DeclarationUtil.makeTypeProvider(
                    localTypes, registry, field.getTypeTokens(), fieldProps, allowThriftCompat, warningHandler);
            builder.field(field.getName(), provider)
                   .id(field.getId())
                   .properties(fieldProps);
        }
        registry.register(builder.build());
    }

    private static void validateMessage(MessageDeclaration declaration,
                                        PNamespaceFileTypeRegistry registry,
                                        IDLWarningHandler warningHandler) throws IDLException {
        PTypeReference reference  = ref(declaration.getName());
        PDescriptor    descriptor = registry.resolve(reference);
        if (!(descriptor instanceof PMessageDescriptor)) {
            throw new IDLException(declaration.getVariantToken(), "Unable to parse message");
        }
        PMessageDescriptor<?> messageDescriptor = (PMessageDescriptor<?>) descriptor;
        Map<String, PField> extendedFields = messageDescriptor.getExtending()
                                                              .stream()
                                                              .flatMap(d -> d.allFields().stream())
                                                              .collect(toMap(f -> format(f.getName(), NamingUtil.Format.PASCAL)));

        for (PField field : messageDescriptor.declaredFields()) {
            FieldDeclaration fieldDeclaration = findFieldByName(declaration.getFields(), field.getName());
            PField extendedByName = extendedFields.get(format(field.getName(), NamingUtil.Format.PASCAL));
            if (extendedByName != null){
                throw new IDLException("Field " + field.getName() + " with name conflict in " +
                                       extendedByName.getOnMessageType().getTypeName());
            }
            PField extendedById = messageDescriptor.getExtending().map(e -> e.findFieldById(field.getId())).orElse(null);
            if (extendedById != null) {
                throw new IDLException("Field " + field.getName() + " with ID conflict (" + field.getId() + ") in " +
                                       extendedById.getOnMessageType().getTypeName());
            }
            validateField(fieldDeclaration, field, registry, warningHandler);
        }
        for (PMessageDescriptor<?> impl : messageDescriptor.getImplementing()) {
            for (PField iField : impl.allFields()) {
                PField field = messageDescriptor.findFieldByName(iField.getName());
                if (field == null) {
                    throw new IDLException("Declaration for " +
                                           iField.getResolvedDescriptor().toString() + " " + iField.getName() +
                                           " from interface " + impl.getTypeName() + " is missing.");
                }
            }
        }
    }

    private static void validateField(FieldDeclaration declaration,
                                      PField descriptor,
                                      PNamespaceFileTypeRegistry registry,
                                      IDLWarningHandler warningHandler) throws IDLException {
        try {
            PDescriptor type = requireNonNull(descriptor.getResolvedDescriptor(), "field type == null");
            if (descriptor.hasProperty(SORTED) && descriptor.getProperty(SORTED)) {
                if (!(type instanceof PSetDescriptor) &&
                    !(type instanceof PMapDescriptor)) {
                    AnnotationDeclaration annotation = findAnnotationByName(declaration.getAnnotations(), SORTED.getName());
                    throw new IDLException(annotation.getValueToken(), "Field type not applicable for sorted annotation: " + type.getTypeName());
                }
            }
        } catch (IllegalStateException | IllegalArgumentException e) {
            throw new IDLException(toSingleToken(declaration.getTypeTokens()), "Invalid field type: " + e.getMessage())
                    .initCause(e);
        }
        try {
            descriptor.getDefaultValue();
        } catch (IllegalArgumentException | IllegalStateException e) {
            throw new IDLException(toSingleToken(declaration.getDefaultValueTokens()), "Invalid default value: " + e.getMessage())
                    .initCause(e);
        }
        if (descriptor.hasProperty(REF_ENUM)) {
            String                refEnum    = descriptor.getProperty(REF_ENUM);
            AnnotationDeclaration annotation = findAnnotationByName(declaration.getAnnotations(), REF_ENUM.getName());

            if (descriptor.getType() != PType.BYTE &&
                descriptor.getType() != PType.SHORT &&
                descriptor.getType() != PType.INT) {
                if (descriptor.getType() == PType.LIST ||
                    descriptor.getType() == PType.SET) {
                    // Warn, but allow. This should be possible to implement.
                    warningHandler.handleWarning(new IDLException(annotation.getTagToken(), "ref.enum on list and set not supported yet."));
                } else {
                    throw new IDLException(annotation.getTagToken(), "ref.enum not compatible with field type " + descriptor.getType().toString());
                }
            }
            try {
                PTypeReference reference         = DeclarationUtil.parseType(refEnum);
                PDescriptor    refEnumDescriptor = registry.resolve(reference);
                if (!(refEnumDescriptor instanceof PEnumDescriptor)) {
                    throw new IDLException(annotation.getValueToken(), "Not an enum type for ref.enum: " + refEnum);
                }
            } catch (IllegalArgumentException e) {
                throw new IDLException(annotation.getValueToken(), "Invalid ref.enum type: " + refEnum)
                        .initCause(e);
            }
        }
        if (descriptor.hasProperty(REF_ARGUMENTS)) {
            String                refArguments    = descriptor.getProperty(REF_ARGUMENTS);
            AnnotationDeclaration annotation = findAnnotationByName(declaration.getAnnotations(), REF_ARGUMENTS.getName());

            try {
                PTypeReference reference         = DeclarationUtil.parseType(refArguments);
                PDescriptor    refArgsDescriptor = registry.resolve(reference);
                if (!(refArgsDescriptor instanceof PMessageDescriptor)) {
                    throw new IDLException(annotation.getValueToken(), "Not an message type for ref.arguments: " + refArguments);
                }
            } catch (IllegalArgumentException e) {
                throw new IDLException(annotation.getValueToken(), "Invalid ref.arguments type: " + refArguments)
                        .initCause(e);
            }
        }
    }

    private static void registerService(Map<String, PDeclarationType> localTypes,
                                        FileDeclaration program,
                                        ServiceDeclaration declaration,
                                        PNamespaceFileTypeRegistry registry,
                                        boolean allowThriftCompat,
                                        IDLWarningHandler warningHandler) throws IDLException {
        CServiceDescriptor.Builder builder = CServiceDescriptor.newBuilder(program.getNamespace(), declaration.getName());
        PPropertyMap serviceProps = makeProperties(
                declaration.getAnnotations(),
                PPropertyMap.of(),
                declaration.getDocumentation());
        builder.properties(serviceProps);
        for (IDLToken extending : declaration.getExtendingTokens()) {
            builder.extending(DeclarationUtil.makeServiceProvider(localTypes, extending, registry));
        }
        for (MethodDeclaration method : declaration.getMethods()) {
            PPropertyMap.Builder methodProps = PPropertyMap.newBuilder();
            for (AnnotationDeclaration anno : declaration.getAnnotations()) {
                methodProps.putUnsafe(anno.getTag(), anno.getValue());
            }
            Supplier<PMessageDescriptor<?>> responseType = DeclarationUtil.makeMessageTypeProvider(localTypes, registry, method.getResponseTypeToken());
            Supplier<PMessageDescriptor<?>> requestType = DeclarationUtil.makeMessageTypeProvider(localTypes, registry, method.getRequestTypeToken());

            builder.method(method.getName(),
                           responseType,
                           requestType,
                           methodProps.build());
        }
        registry.register(builder.build());
    }

    private static void validateService(ServiceDeclaration declaration,
                                        PNamespaceFileTypeRegistry registry) throws IDLException {

        PDescriptor descriptor;
        try {
            descriptor = registry.descriptor(ref(declaration.getName()));
        } catch (IllegalArgumentException e) {
            throw new IDLException(declaration.getServiceToken(), "Unable to parse service");
        }
        if (!(descriptor instanceof PServiceDescriptor)) {
            throw new IDLException(declaration.getServiceToken(), "Unable to parse service");
        }
        PServiceDescriptor service = (PServiceDescriptor) descriptor;
        try {
            service.extending();
        } catch (IllegalArgumentException e) {
            throw new IDLException(toSingleToken(declaration.getExtendingTokens()),
                                   "Unknown extending service: " + e.getMessage());
        }

        Set<String> extendedMethods = service.allMethods()
                                             .stream()
                                             .filter(m -> !m.getOnServiceType().equals(service))
                                             .map(PServiceMethod::getName)
                                             .collect(Collectors.toSet());
        for (PServiceMethod method : service.declaredMethods()) {
            MethodDeclaration md = findMethodByName(declaration.getMethods(), method.getName());
            if (extendedMethods.contains(method.getName())) {
                throw new IDLException(md.getNameToken(),
                                       "Method already declared in " + declaration.getExtendingTokens());
            }
            try {
                method.getResponseType();
            } catch (IllegalArgumentException e) {
                throw new IDLException(md.getResponseTypeToken(), "Bad response type: " + e.getMessage())
                        .initCause(e);
            }
            try {
                method.getRequestType();
            } catch (IllegalArgumentException e) {
                throw new IDLException(md.getResponseTypeToken(), "Bad request type: " + e.getMessage())
                        .initCause(e);
            }
        }
    }

    private static void registerConst(Map<String, PDeclarationType> localTypes,
                                      ConstDeclaration declaration,
                                      PNamespaceFileTypeRegistry registry,
                                      boolean allowThriftCompat,
                                      IDLWarningHandler warningHandler) throws IDLException {
        Supplier<? extends PDescriptor> provider = DeclarationUtil.makeTypeProvider(
                localTypes,
                registry,
                declaration.getTypeTokens(),
                PPropertyMap.empty(),
                allowThriftCompat,
                warningHandler);
        Supplier<Object> constParser = new ProvidenceConstParser(registry, warningHandler, provider, declaration.getValueTokens());
        registry.register(new PConstDescriptor(registry.getNamespace(), declaration.getName(), provider, constParser));
    }

    private static void validateConst(ConstDeclaration declaration,
                                      PNamespaceFileTypeRegistry registry) throws IDLException {
        PDescriptor descriptor;
        try {
            descriptor = registry.descriptor(ref(declaration.getName()));
        } catch (IllegalArgumentException e) {
            throw new IDLException(declaration.getConstToken(), "Unable to parse const");
        }
        if (!(descriptor instanceof PConstDescriptor)) {
            throw new IDLException(declaration.getConstToken(), "Unable to parse const");
        }
        PConstDescriptor constDescriptor = (PConstDescriptor) descriptor;
        try {
            constDescriptor.getDescriptor();
            Object value = constDescriptor.getValue();
            if (value == null) {
                throw new IDLException(toSingleToken(declaration.getValueTokens()), "Null const value");
            }
        } catch (IllegalArgumentException e) {
            throw new IDLException(toSingleToken(declaration.getTypeTokens()), e.getMessage());
        } catch (UncheckedLexerException e) {
            if (e.getCause() instanceof IDLException) {
                throw (IDLException) e.getCause();
            }
            // NOTE: Should not be possible?
            LexerException cause = e.getCause();
            throw new IDLException(cause.getLine(),
                                      cause.getLineNo(),
                                      cause.getLinePos(),
                                      cause.getLength(),
                                      cause.getMessage());
        }
    }

    private static void registerTypedef(
            Map<String, PDeclarationType> localTypes,
            TypedefDeclaration declaration,
            PNamespaceFileTypeRegistry registry,
            boolean allowThriftCompat,
            IDLWarningHandler warningHandler) throws IDLException {
        PPropertyMap propertyMap = makeProperties(declaration.getAnnotations(), declaration.getDocumentation());
        Supplier<? extends PDescriptor> provider =
                DeclarationUtil.makeTypeProvider(localTypes, registry, declaration.getTypeTokens(), propertyMap, allowThriftCompat, warningHandler);
        registry.register(new PTypeDefDescriptor(registry.getNamespace(), declaration.getName(), propertyMap, provider));
    }

    private static void validateTypedef(TypedefDeclaration declaration,
                                        PNamespaceFileTypeRegistry registry) throws IDLException {
        PTypeReference reference  = ref(declaration.getName());
        PDescriptor    descriptor = registry.descriptor(reference);
        if (!(descriptor instanceof PTypeDefDescriptor)) {
            throw new IDLException(declaration.getTypedefToken(), "Unable to parse typedef");
        }
        PTypeDefDescriptor typeDefinition = (PTypeDefDescriptor) descriptor;
        try {
            typeDefinition.getDescriptor();
        } catch (IllegalStateException | IllegalArgumentException e) {
            throw new IDLException(toSingleToken(declaration.getTypeTokens()),
                                   "Invalid field type: " + e.getMessage());
        }
    }


    private static AnnotationDeclaration findAnnotationByName(List<AnnotationDeclaration> annotations, String name) {
        for (AnnotationDeclaration annotation : annotations) {
            if (annotation.getTag().equals(name)) {
                return annotation;
            }
        }
        throw new IllegalArgumentException("No such annotation: " + name);
    }

    private static FieldDeclaration findFieldByName(List<FieldDeclaration> fieldList, String fieldName) {
        for (FieldDeclaration field : fieldList) {
            if (field.getName().equals(fieldName)) {
                return field;
            }
        }
        throw new IllegalArgumentException("No such field: " + fieldName);
    }

    private static MethodDeclaration findMethodByName(List<MethodDeclaration> methodList, String fieldName) {
        for (MethodDeclaration field : methodList) {
            if (field.getName().equals(fieldName)) {
                return field;
            }
        }
        throw new IllegalArgumentException("No such method: " + fieldName);
    }

    private static PPropertyMap makeProperties(Collection<AnnotationDeclaration> annotations, String documentation) {
        return makeProperties(annotations, PPropertyMap.empty(), documentation);
    }

    private static PPropertyMap makeProperties(Collection<AnnotationDeclaration> annotations, PPropertyMap overrides,
                                               String documentation) {
        PPropertyMap.Builder builder = PPropertyMap.newBuilder();
        for (AnnotationDeclaration annotation : annotations) {
            builder.putUnsafe(annotation.getTag(), annotation.getValue());
        }
        if (documentation != null) {
            builder.put(PDefaultProperties.DOCUMENTATION, documentation);
        }
        builder.putAll(overrides);
        return builder.build();
    }
}
