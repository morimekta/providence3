/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.pvd.model;

import io.pvdnc.core.property.PDefaultProperties;
import io.pvdnc.idl.parser.IDLToken;
import net.morimekta.strings.Displayable;

import static java.util.Objects.requireNonNull;
import static io.pvdnc.idl.pvd.parser.ProvidenceTokenizer.kOneOf;
import static net.morimekta.strings.EscapeUtil.javaEscape;

/**
 * A single annotation declaration, as part of the annotation list.
 *
 * <pre>{@code
 * annotation  :== {tag} ('=' '"' {value} '"')?
 * annotations :== '(' {annotation} (',' {annotation})* ')'
 * }</pre>
 */
public class AnnotationDeclaration implements Displayable {
    private final IDLToken        tagToken;
    private final IDLToken valueToken;

    public AnnotationDeclaration(IDLToken tagToken,
                                 IDLToken valueToken) {
        this.tagToken = requireNonNull(tagToken, "tag == null");
        this.valueToken = valueToken;
    }

    public IDLToken getTagToken() {
        return tagToken;
    }

    public String getTag() {
        if (kOneOf.equals(tagToken.toString())) {
            return PDefaultProperties.ONE_OF_GROUP.getName();
        }
        return tagToken.toString();
    }

    public String getValue() {
        if (valueToken == null) return "";
        if (valueToken.isIdentifier()) {
            return valueToken.toString();
        }
        return valueToken.decodeString(false);
    }

    public IDLToken getValueToken() {
        return valueToken;
    }

    // --- Displayable

    @Override
    public String displayString() {
        StringBuilder builder = new StringBuilder(getTag());
        if (valueToken != null) {
            builder.append(" = \"").append(javaEscape(getValue())).append("\"");
        }
        return builder.toString();
    }

    @Override
    public String toString() {
        return "Annotation{" + getTag() + "}";
    }
}
