/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.pvd.util;


import io.pvdnc.core.property.PDefaultProperties;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.registry.PNamespaceFileTypeRegistry;
import io.pvdnc.core.registry.PTypeReference;
import io.pvdnc.core.registry.PTypeRegistry;
import io.pvdnc.core.types.PDeclarationType;
import io.pvdnc.core.types.PDeclaredDescriptor;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PServiceDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.types.PType;
import io.pvdnc.idl.parser.IDLException;
import io.pvdnc.idl.parser.IDLToken;
import io.pvdnc.idl.parser.IDLWarningHandler;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import static java.lang.Boolean.TRUE;
import static java.util.Objects.requireNonNull;
import static io.pvdnc.core.registry.PTypeReference.LOCAL_NAMESPACE;

public final class DeclarationUtil {
    public static String toTypeString(List<IDLToken> typeTokens) {
        StringBuilder builder = new StringBuilder();
        for (IDLToken token : typeTokens) {
            builder.append(token);
        }
        return builder.toString();
    }

    public static String toValueString(List<IDLToken> typeTokens) {
        StringBuilder builder = new StringBuilder();
        for (IDLToken token : typeTokens) {
            builder.append(token);
        }
        return builder.toString();
    }

    public static Supplier<PMessageDescriptor<?>> makeMessageTypeProvider(
            Map<String, PDeclarationType> localTypes,
            PNamespaceFileTypeRegistry registry,
            IDLToken referenceToken) throws IDLException {
        PTypeReference typeReference = parseType(referenceToken.toString());
        if (typeReference.isLocalType()) {
            if (!localTypes.containsKey(typeReference.getName())) {
                throw new IDLException(referenceToken, "Unknown message type: " + typeReference.getName());
            } else if (localTypes.get(typeReference.getName()) != PDeclarationType.MESSAGE) {
                throw new IDLException(referenceToken, "Not a message type: " + typeReference.getName());
            }
            return () -> (PMessageDescriptor<?>) registry.descriptor(typeReference);
        } else {
            PTypeRegistry include = registry.getInclude(typeReference.getNamespace())
                    .orElseThrow(() -> new IDLException(referenceToken, "Unknown namespace: " + typeReference.getNamespace() + " for " + typeReference));
            try {
                PDeclaredDescriptor descriptor = include.descriptor(typeReference.asLocalType());
                if (!(descriptor instanceof PMessageDescriptor)) {
                    throw new IDLException("Not a message type: " + typeReference.asLocalType() + " in " + include.toString());
                }
                return () -> (PMessageDescriptor<?>) descriptor;
            } catch (IllegalArgumentException e) {
                throw new IDLException("No type " + typeReference.asLocalType() + " in " + include.toString());
            }
        }
    }

    public static Supplier<? extends PDescriptor> makeTypeProvider(
            Map<String, PDeclarationType> localTypes,
            PNamespaceFileTypeRegistry registry,
            List<IDLToken> typeTokens,
            PPropertyMap propertyMap,
            boolean allowThriftCompat,
            IDLWarningHandler warningHandler) throws IDLException {
        LinkedList<IDLToken>     tokens   = new LinkedList<>(typeTokens);
        Supplier<? extends PDescriptor> provider = makeTypeProviderInternal(localTypes, registry, tokens, propertyMap, allowThriftCompat, warningHandler);
        if (!tokens.isEmpty()) {
            throw new IDLException(tokens.pollFirst(), "Unexpected type token");
        }
        return provider;
    }

    /**
     * Create a type reference from a global reference name. This must include
     * a program name and a type name, and nothing else.
     *
     * @param refString The global type reference string.
     * @return The type reference.
     */
    public static PTypeReference parseType(String refString) {
        if (refString.isEmpty()) {
            throw new IllegalArgumentException("Empty type reference");
        }
        try {
            String[] parts = refString.split("[.]", 2);
            if (parts.length == 1) {
                return PTypeReference.ref(LOCAL_NAMESPACE, refString);
            } else {
                return PTypeReference.ref(parts[0], parts[1]);
            }
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(e.getMessage() + " in reference '" + refString + "'", e);
        }
    }

    public static Supplier<PServiceDescriptor> makeServiceProvider(
            Map<String, PDeclarationType> localTypes,
            IDLToken serviceReferenceToken,
            PNamespaceFileTypeRegistry registry) throws IDLException {
        PTypeReference serviceReference = parseType(serviceReferenceToken.toString());
        if (serviceReference.isLocalType()) {
            PDeclarationType local = localTypes.get(serviceReference.getName());
            if (local == null) {
                throw new IDLException(serviceReferenceToken, "Unknown type name " + serviceReference.getName());
            }
            if (local != PDeclarationType.SERVICE) {
                throw new IDLException(serviceReferenceToken, "Not a service " + serviceReference.getName());
            }
            return () -> (PServiceDescriptor) registry.descriptor(serviceReference);
        }
        try {
            PDeclaredDescriptor other = registry
                    .getInclude(serviceReference.getNamespace())
                    .orElseThrow()
                    .descriptor(serviceReference.asLocalType());
            if (!(other instanceof PServiceDescriptor)) {
                throw new IDLException(serviceReferenceToken, "Not a service " + serviceReference.getName());
            }
            return () -> (PServiceDescriptor) other;
        } catch (IllegalArgumentException e) {
            throw new IDLException(serviceReferenceToken, "Unknown type name " + serviceReference.getName() + " in "+ serviceReference.getNamespace());
        }
    }

    private static Supplier<? extends PDescriptor> makeTypeProviderInternal(
            Map<String, PDeclarationType> localTypes,
            PNamespaceFileTypeRegistry registry,
            LinkedList<IDLToken> typeTokens,
            PPropertyMap propertyMap,
            boolean allowThriftCompat,
            IDLWarningHandler warningHandler) throws IDLException {
        IDLToken first = requireNonNull(typeTokens.pollFirst(), "no first token");
        switch (first.toString()) {
            case "void":
                return PType.VOID.provider();
            case "bool":
                return PType.BOOL.provider();
            case "i8":
                if (!allowThriftCompat) {
                    throw new IDLException(first, "Deprecated compatibility with thrift, use 'byte'");
                }
                warningHandler.handleWarning(new IDLException(
                        first, "Deprecated compatibility with thrift, use 'byte'"));
                return PType.BYTE.provider();
            case "byte":
                return PType.BYTE.provider();
            case "i16":
                if (!allowThriftCompat) {
                    throw new IDLException(first, "Deprecated compatibility with thrift, use 'short'");
                }
                warningHandler.handleWarning(new IDLException(
                        first, "Deprecated compatibility with thrift, use 'short'"));
                return PType.SHORT.provider();
            case "short":
                return PType.SHORT.provider();
            case "i32":
                if (!allowThriftCompat) {
                    throw new IDLException(first, "Deprecated compatibility with thrift, use 'int'");
                }
                warningHandler.handleWarning(new IDLException(
                        first, "Deprecated compatibility with thrift, use 'int'"));
                return PType.INT.provider();
            case "int":
                return PType.INT.provider();
            case "i64":
                if (!allowThriftCompat) {
                    throw new IDLException(first, "Deprecated compatibility with thrift, use 'long'");
                }
                warningHandler.handleWarning(new IDLException(
                        first, "Deprecated compatibility with thrift, use 'long'"));
                return PType.LONG.provider();
            case "long":
                return PType.LONG.provider();
            case "float":
                return PType.FLOAT.provider();
            case "double":
                return PType.DOUBLE.provider();
            case "string":
                return PType.STRING.provider();
            case "binary":
                return PType.BINARY.provider();
            case "any":
                return PType.ANY.provider();
            case "list": {
                verifyNextSymbol(typeTokens, '<');
                Supplier<? extends PDescriptor> itemType = makeTypeProviderInternal(
                        localTypes, registry, typeTokens, propertyMap, allowThriftCompat, warningHandler);
                verifyNextSymbol(typeTokens, '>');
                return PListDescriptor.provider(itemType);
            }
            case "set": {
                verifyNextSymbol(typeTokens, '<');
                Supplier<? extends PDescriptor> itemType = makeTypeProviderInternal(
                        localTypes, registry, typeTokens, propertyMap, allowThriftCompat, warningHandler);
                verifyNextSymbol(typeTokens, '>');
                if (TRUE.equals(propertyMap.getProperty(PDefaultProperties.SORTED))) {
                    return PSetDescriptor.sortedProvider(itemType);
                }
                return PSetDescriptor.provider(itemType);
            }
            case "map": {
                verifyNextSymbol(typeTokens, '<');
                Supplier<? extends PDescriptor> keyType = makeTypeProviderInternal(
                        localTypes, registry, typeTokens, propertyMap, allowThriftCompat, warningHandler);
                verifyNextSymbol(typeTokens, ',');
                Supplier<? extends PDescriptor> valueType = makeTypeProviderInternal(
                        localTypes, registry, typeTokens, propertyMap, allowThriftCompat, warningHandler);
                verifyNextSymbol(typeTokens, '>');
                if (TRUE.equals(propertyMap.getProperty(PDefaultProperties.SORTED))) {
                    return PMapDescriptor.sortedProvider(keyType, valueType);
                }
                return PMapDescriptor.provider(keyType, valueType);
            }
            default: {
                // enum, message or typedef
                PTypeReference ref = parseType(first.toString());
                if (!ref.isLocalType()) {
                    try {
                        PDescriptor descriptor = registry
                                .getInclude(ref.getNamespace())
                                .orElseThrow(() -> new IDLException(first, "No such include " + ref.getNamespace()))
                                .descriptor(ref.asLocalType());
                        if (descriptor instanceof PServiceDescriptor) {
                            throw new IDLException(first, "Not a referencable type " + ref.getName());
                        }
                        return () -> descriptor;
                    } catch (IllegalArgumentException e) {
                        throw new IDLException(first, e.getMessage()).initCause(e);
                    }
                }
                PDeclarationType local = localTypes.get(ref.getName());
                if (local == null) {
                    throw new IDLException(first, "Unknown type name " + ref.getName());
                }
                if (local != PDeclarationType.MESSAGE &&
                    local != PDeclarationType.ENUM &&
                    local != PDeclarationType.TYPEDEF) {
                    throw new IDLException(first, "Not a referencable type " + ref.getName());
                }
                // not always available yet.
                return () -> registry.descriptor(ref);
            }
        }
    }

    private static void verifyNextSymbol(LinkedList<IDLToken> typeTokens, char symbol) throws IDLException {
        IDLToken first = requireNonNull(typeTokens.pollFirst());
        if (!first.isSymbol(symbol)) {
            throw new IDLException(first, "Expected '" + symbol + "', found " + first);
        }
    }

}
