/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.pvd.parser;

import io.pvdnc.core.PMessageBuilder;
import net.morimekta.collect.ListBuilder;
import net.morimekta.collect.MapBuilder;
import net.morimekta.collect.SetBuilder;
import net.morimekta.collect.util.Binary;
import net.morimekta.lexer.TokenizerRepeater;
import net.morimekta.lexer.UncheckedLexerException;
import io.pvdnc.core.PEnum;
import io.pvdnc.core.registry.PNamespaceFileTypeRegistry;
import io.pvdnc.core.registry.PTypeReference;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.utils.ConversionUtil;
import io.pvdnc.idl.parser.IDLException;
import io.pvdnc.idl.parser.IDLLexer;
import io.pvdnc.idl.parser.IDLToken;
import io.pvdnc.idl.parser.IDLTokenType;
import io.pvdnc.idl.parser.IDLWarningHandler;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;
import static io.pvdnc.idl.parser.IDLToken.kEntrySep;
import static io.pvdnc.idl.parser.IDLToken.kFieldValueSep;
import static io.pvdnc.idl.parser.IDLToken.kKeyValueSep;
import static io.pvdnc.idl.parser.IDLToken.kLineSep;
import static io.pvdnc.idl.parser.IDLToken.kListEnd;
import static io.pvdnc.idl.parser.IDLToken.kListStart;
import static io.pvdnc.idl.parser.IDLToken.kMessageEnd;
import static io.pvdnc.idl.parser.IDLToken.kMessageStart;

public class ProvidenceConstParser implements Supplier<Object> {
    private final AtomicReference<Object>         value;
    private final PNamespaceFileTypeRegistry registry;
    private final IDLWarningHandler               warningHandler;
    private final Supplier<? extends PDescriptor> provider;
    private final List<IDLToken>                  valueTokens;

    public ProvidenceConstParser(PNamespaceFileTypeRegistry registry,
                                 IDLWarningHandler warningHandler,
                                 Supplier<? extends PDescriptor> provider,
                                 List<IDLToken> valueTokens) {
        this.registry = requireNonNull(registry, "registry == null");
        this.warningHandler = requireNonNull(warningHandler, "warningHandler == null");
        this.provider = requireNonNull(provider, "providence == null");
        this.valueTokens = requireNonNull(valueTokens, "valueToken == null");
        if (valueTokens.isEmpty()) {
            throw new IllegalArgumentException("No tokens in value tokens list");
        }
        this.value = new AtomicReference<>();
    }

    @Override
    public Object get() {
        return value.updateAndGet(value -> {
            if (value == null) {
                value = parse();
            }
            return value;
        });
    }

    private Object parse() {
        PDescriptor                               descriptor = provider.get();
        TokenizerRepeater<IDLTokenType, IDLToken> repeater   = new TokenizerRepeater<>(valueTokens);
        IDLLexer                                  lexer      = new IDLLexer(repeater);
        try {
            Object value = parseValue(descriptor, lexer);
            if (lexer.hasNext()) {
                throw new IDLException(lexer.next(), "Garbage token at end of const value");
            }
            return value;
        } catch (IDLException e) {
            throw new UncheckedLexerException(e);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @SuppressWarnings("unchecked")
    private Object parseValue(PDescriptor descriptor, IDLLexer lexer) throws IOException {
        IDLToken next = lexer.expect("value");
        if (next.isQualifiedIdentifier() ||
            next.isDoubleQualifiedIdentifier()) {
            // thrift compatibility, reference to enum values:
            return parseThriftEnumReference(descriptor, next);
        }

        switch (descriptor.getType()) {
            case BOOL: {
                switch (next.toString().toLowerCase(Locale.US)) {
                    case "1":
                    case "yes":
                    case "true":
                        return true;
                    case "0":
                    case "no":
                    case "false":
                        return false;
                    default:
                        throw new IDLException(next, "Unknown boolean value");
                }
            }
            case BYTE:
                if (next.type() != IDLTokenType.NUMBER) {
                    throw new IDLException(next, "Not a number: '" + next + "'");
                }
                try {
                    String str = next.toString();
                    if (str.startsWith("0")) {
                        if (str.startsWith("0x")) {
                            return Byte.parseByte(str.substring(2), 16);
                        }
                        return Byte.parseByte(str, 8);
                    }
                    return Byte.parseByte(str);
                } catch (NumberFormatException e) {
                    throw new IDLException(next, "Invalid Byte: " + e.getMessage());
                }
            case SHORT:
                if (next.type() != IDLTokenType.NUMBER) {
                    throw new IDLException(next, "Not a number: '" + next + "'");
                }
                try {
                    String str = next.toString();
                    if (str.startsWith("0")) {
                        if (str.startsWith("0x")) {
                            return Short.parseShort(str.substring(2), 16);
                        }
                        return Short.parseShort(str, 8);
                    }
                    return Short.parseShort(str);
                } catch (NumberFormatException e) {
                    throw new IDLException(next, "Invalid Short: " + e.getMessage()).initCause(e);
                }
            case INT:
                if (next.type() != IDLTokenType.NUMBER) {
                    throw new IDLException(next, "Not a number: '" + next + "'");
                }
                try {
                    String str = next.toString();
                    if (str.startsWith("0")) {
                        if (str.startsWith("0x")) {
                            return Integer.parseInt(str.substring(2), 16);
                        }
                        return Integer.parseInt(str, 8);
                    }
                    return Integer.parseInt(str);
                } catch (NumberFormatException e) {
                    throw new IDLException(next, "Invalid Integer: " + e.getMessage()).initCause(e);
                }
            case LONG:
                if (next.type() != IDLTokenType.NUMBER) {
                    throw new IDLException(next, "Not a number: '" + next + "'");
                }
                try {
                    String str = next.toString();
                    if (str.startsWith("0")) {
                        if (str.startsWith("0x")) {
                            return Long.parseLong(str.substring(2), 16);
                        }
                        return Long.parseLong(str, 8);
                    }
                    return Long.parseLong(str);
                } catch (NumberFormatException e) {
                    throw new IDLException(next, "Invalid Long: " + e.getMessage()).initCause(e);
                }
            case FLOAT:
                if (next.type() != IDLTokenType.NUMBER) {
                    throw new IDLException(next, "Not a number: '" + next + "'");
                }
                try {
                    return Float.parseFloat(next.toString());
                } catch (NumberFormatException e) {
                    throw new IDLException(next, "Invalid Float: " + e.getMessage()).initCause(e);
                }
            case DOUBLE:
                if (next.type() != IDLTokenType.NUMBER) {
                    throw new IDLException(next, "Not a number: '" + next + "'");
                }
                try {
                    return Double.parseDouble(next.toString());
                } catch (NumberFormatException e) {
                    throw new IDLException(next, "Invalid Double: " + e.getMessage()).initCause(e);
                }
            case STRING:
                return parseStringValue(next);
            case BINARY:
                return parseBinaryValue(next);
            case ENUM:
                return parseEnumValue((PEnumDescriptor) descriptor, next);
            case MESSAGE:
                return parseMessageValue((PMessageDescriptor<?>) descriptor, lexer, next);
            case LIST:
                return parseListValue((PListDescriptor<Object>) descriptor, lexer, next);
            case SET:
                return parseSetValue((PSetDescriptor<Object>) descriptor, lexer, next);
            case MAP:
                return parseMapValue((PMapDescriptor<Object, Object>) descriptor, lexer, next);
            default:
                throw new IDLException(next, descriptor.getTypeName() + " type not allowed in constants");
        }
    }

    private Object parseStringValue(IDLToken next) throws IDLException {
        if (next.type() != IDLTokenType.STRING) {
            throw new IDLException(next, "Not a string value: " + next);
        }
        try {
            return next.decodeString(true);
        } catch (IllegalStateException | IllegalArgumentException e) {
            throw new IDLException(next, "Bad string escaping: " + e.getMessage()).initCause(e);
        }
    }

    private Object parseBinaryValue(IDLToken next) throws IDLException {
        String str = next.toString();
        if (next.isInteger()) {
            if (next.toString().startsWith("0x")) {
                if (next.length() % 2 != 0) {
                    throw new IDLException(next, "Odd number of hex (" + (next.length() - 2) +
                                                 "), must be even for binary");
                }
                return Binary.fromHexString(str.substring(2));
            }
        } else if (next.type() == IDLTokenType.STRING) {
            try {
                return Binary.fromBase64(str.substring(1, str.length() - 1));
            } catch (IllegalArgumentException e) {
                throw new IDLException(next, e.getMessage()).initCause(e);
            }
        }
        throw new IDLException(next, "Not a binary value: '" + next + "'");
    }

    private Object parseEnumValue(PEnumDescriptor descriptor, IDLToken next) throws IDLException {
        PEnum value;
        if (next.isInteger()) {
            value = descriptor.findById(Integer.parseInt(next.toString()));
        } else if (next.isIdentifier()) {
            value = descriptor.findByName(next.toString());
        } else {
            throw new IDLException(next, "Bad enum value '" + next + "'");
        }
        if (value == null) {
            throw new IDLException(next, "Unknown " + descriptor.getTypeName() + " value " + next);
        }
        return value;
    }

    private Object parseListValue(PListDescriptor<Object> descriptor, IDLLexer lexer, IDLToken next) throws
                                                                                                     IOException {
        ListBuilder<Object> builder = descriptor.builder(4);
        if (!next.isSymbol(kListStart)) {
            throw new IDLException(next, "Invalid list start " + next);
        }
        while (!lexer.peek("end").isSymbol(kListEnd)) {
            builder.add(parseValue(descriptor.getItemType(), lexer));
            if (lexer.peek("sep").isSymbol(kEntrySep)) {
                lexer.next();
            }
        }
        lexer.next();  // skip the ending ']'
        return builder.build();
    }

    private Object parseSetValue(PSetDescriptor<Object> descriptor, IDLLexer lexer, IDLToken next) throws
                                                                                                   IOException {
        SetBuilder<Object>     builder = descriptor.builder(4);
        if (!next.isSymbol(kListStart)) {
            throw new IDLException(next, "Invalid set start " + next);
        }
        while (!lexer.peek("end").isSymbol(kListEnd)) {
            builder.add(parseValue(descriptor.getItemType(), lexer));
            if (lexer.peek("sep").isSymbol(kEntrySep)) {
                lexer.next();
            }
        }
        lexer.next();  // skip the ending ']'
        return builder.build();
    }

    private Object parseMapValue(PMapDescriptor<Object, Object> descriptor, IDLLexer lexer, IDLToken next) throws
                                                                                                           IOException {
        MapBuilder<Object, Object> builder = descriptor.builder(4);
        if (!next.isSymbol(kMessageStart)) {
            throw new IDLException(next, "Invalid map start " + next);
        }
        while (!lexer.peek("end").isSymbol(kMessageEnd)) {
            Object key = parseValue(descriptor.getKeyType(), lexer);
            lexer.expectSymbol("key value sep", kKeyValueSep);
            Object value = parseValue(descriptor.getValueType(), lexer);
            builder.put(key, value);

            if (lexer.peek("sep").isSymbol(kEntrySep)) {
                lexer.next();
            }
        }
        lexer.next();  // skip the ending ']'
        return builder.build();
    }

    private Object parseMessageValue(PMessageDescriptor<?> descriptor, IDLLexer lexer, IDLToken next)
            throws IOException {
        PMessageBuilder builder = descriptor.newBuilder();

        if (!next.isSymbol(kMessageStart)) {
            throw new IDLException(next, "Invalid message start " + next + ": Must be '{'");
        }
        next = lexer.expect("field or end");
        boolean warnForThriftMessageSyntax = true;
        boolean warnForThriftLineSeparator = true;
        while (!next.isSymbol(kMessageEnd)) {
            PField field;
            if (next.type() == IDLTokenType.STRING) {
                if (warnForThriftMessageSyntax) {
                    warningHandler.handleWarning(
                            new IDLException(next, "Using thrift json-like message syntax is deprecated. Use unquoted field 'name = ' instead."));
                    warnForThriftMessageSyntax = false;
                }
                // special handling for compatibility with thrift.
                field = descriptor.findFieldByName(next.decodeString(false));
                lexer.expectSymbol("key value sep", kKeyValueSep);
            } else if (next.type() != IDLTokenType.IDENTIFIER) {
                throw new IDLException(next, "Invalid field name " + next + ": Must be identifier");
            } else {
                field = descriptor.findFieldByName(next.toString());
                lexer.expectSymbol("field value sep", kFieldValueSep);
            }
            if (field == null) {
                throw new IDLException(next,
                                       "No field on " + descriptor.getTypeName() + " named '" + next.toString() +
                                       "'");
            }
            Object value = parseValue(field.getResolvedDescriptor(), lexer);
            builder.set(field.getId(), value);

            if (lexer.peek("sep").isSymbol(kLineSep)) {
                lexer.next();
            } else if (lexer.peek("sep").isSymbol(kEntrySep)) {
                next = lexer.next();
                if (warnForThriftLineSeparator) {
                    warningHandler.handleWarning(
                            new IDLException(next, "Deprecated line separator for message fields, use ';' or none"));
                    warnForThriftLineSeparator = false;
                }
            }
            next = lexer.expect("field or end");
        }
        return builder.build();
    }

    private Object parseThriftEnumReference(PDescriptor descriptor, IDLToken next) throws IDLException {
        String typeRef = next.toString().replaceAll("\\.[^.]+$", "");
        String typeName = typeRef;
        String namespace = PTypeReference.LOCAL_NAMESPACE;
        String valRef  = next.toString().replaceAll("^.*\\.", "");

        if (typeRef.contains(".")) {
            namespace = typeRef.replaceAll("\\..*", "");
            typeName = typeRef.replaceAll(".*\\.", "");
        }
        PTypeReference  ref = PTypeReference.ref(namespace, typeName);
        PEnumDescriptor rd;
        try {
            rd = (PEnumDescriptor) registry.resolve(ref);
        } catch (IllegalArgumentException e) {
            throw new IDLException(next, "No such enum type %s", typeRef).initCause(e);
        } catch (ClassCastException e) {
            throw new IDLException(next, "%s is not an enum", typeRef).initCause(e);
        }
        PEnum val = rd.findByName(valRef);
        if (val == null) {
            throw new IDLException(next, "No such value %s in %s", valRef, rd.getTypeName());
        }

        switch (descriptor.getType()) {
            case ENUM: {
                if (!rd.equals(descriptor)) {
                    throw new IDLException(next, "Incompatible enum type %s for %s", typeRef, descriptor.getTypeName());
                }
                warningHandler.handleWarning(
                        new IDLException(next, "Deprecated use of qualified enum reference in const. Use only value name."));
                return val;
            }
            case BYTE:
            case SHORT:
            case INT:
            case LONG:
                warningHandler.handleWarning(
                        new IDLException(next, "Using enum reference for value in const, deprecated thrift const feature."));
                try {
                    return ConversionUtil.coerceStrict(descriptor, val);
                } catch (IllegalArgumentException e) {
                    throw new IDLException(next, e.getMessage());
                }
            default:
                throw new IDLException(next,
                                       "Enum reference assignment not allowed for %s values.",
                                       descriptor.getType());
        }
    }
}
