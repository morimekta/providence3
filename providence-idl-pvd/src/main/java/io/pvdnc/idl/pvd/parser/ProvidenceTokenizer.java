/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.pvd.parser;

import io.pvdnc.idl.parser.IDLTokenizer;
import net.morimekta.strings.io.Utf8StreamReader;

import java.io.InputStream;
import java.io.Reader;

/**
 * Specialization of the 'pretty' tokenizer to make it handle some
 * special cases only applicable when parsing thrift files, but not
 * allowed in pretty format or config files.
 */
public class ProvidenceTokenizer extends IDLTokenizer {
    // Various thrift keywords.
    public static final String kOption    = "option";
    public static final String kNamespace = "namespace";
    public static final String kInclude   = "include";

    public static final String kEnum      = "enum";
    public static final String kStruct    = "struct";
    public static final String kTypeDef   = "typedef";
    public static final String kUnion     = "union";
    public static final String kException = "exception";
    public static final String kInterface = "interface";
    public static final String kConst     = "const";
    public static final String kService   = "service";

    // for struct X extends Y
    // for service X extends Y & Z
    public static final String kExtends    = "extends";
    // for struct X implements Y & Z
    public static final String kImplements = "implements";
    // for struct X { one_of name { ... } }
    public static final String kOneOf      = "one_of";
    // for union X of Y {}
    public static final String kOf         = "of";
    public static final String kAs         = "as";

    // Part of constant value
    public static final String kNull = "null";

    public ProvidenceTokenizer(InputStream in) {
        this(new Utf8StreamReader(in));
    }

    public ProvidenceTokenizer(Reader reader) {
        super(reader);
    }

    @Override
    protected boolean startString() {
        return lastChar == '\"' || lastChar == '\'';
    }

    @Override
    protected boolean parseJavaStyleComments() {
        return true;
    }
}
