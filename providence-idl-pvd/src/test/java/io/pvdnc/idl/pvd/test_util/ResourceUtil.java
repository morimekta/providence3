package io.pvdnc.idl.pvd.test_util;

import io.pvdnc.idl.pvd.ProvidenceParserTest;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.requireNonNull;

public class ResourceUtil {
    public static Path fromResource(Path target, String resource) {
        try {
            Files.createDirectories(target.getParent());
            try (InputStream in = requireNonNull(ProvidenceParserTest.class.getResourceAsStream(resource),
                                                 "no resource " + resource);
                 BufferedInputStream bin = new BufferedInputStream(in)) {
                Files.write(target, bin.readAllBytes(), StandardOpenOption.CREATE_NEW);
                return target;
            }
        } catch (IOException e) {
            throw new AssertionError(e.getMessage());
        }
    }

    public static String resourceAsString(String resource) {
        try (InputStream in = requireNonNull(ProvidenceParserTest.class.getResourceAsStream(resource),
                                             "no resource " + resource);
             BufferedInputStream bin = new BufferedInputStream(in)) {
            return new String(bin.readAllBytes(), UTF_8);
        } catch (IOException e) {
            throw new AssertionError(e.getMessage());
        }
    }
}
