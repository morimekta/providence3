package io.pvdnc.idl.pvd;

import io.pvdnc.core.registry.PGlobalFileTypeRegistry;
import io.pvdnc.core.registry.PNamespaceFileTypeRegistry;
import io.pvdnc.idl.parser.IDLException;
import io.pvdnc.idl.parser.IDLParserOptions;
import io.pvdnc.idl.parser.IDLWarningHandler;
import io.pvdnc.idl.pvd.test_util.ResourceUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class ProvidenceParserTest {
    @TempDir
    public File tempDir;
    private Path temp;

    @BeforeEach
    public void setUp() throws IOException {
        temp = tempDir.getAbsoluteFile().getCanonicalFile().toPath();
    }

    public static Stream<Arguments> testParse_ParseFailure() {
        return Stream.of(arguments("conflicting_field_name.providence",
                                   "Error in conflicting_field_name.providence on line 3 row 10-22: Field with conflicting name separated_name already declared on line 2.\n" +
                                   "  2: int separatedName;\n" +
                                   "---------^^^^^^^^^^^^^"),
                         arguments("duplicate_field_id.providence",
                                   "Error in duplicate_field_id.providence on line 3 row 3: Field with id 1 already declared on line 2.\n" +
                                   "  1: int second;\n" +
                                   "--^"),
                         arguments("duplicate_field_name.providence",
                                   "Error in duplicate_field_name.providence on line 3 row 10-14: Field with name first already declared on line 2.\n" +
                                   "  2: int first;\n" +
                                   "---------^^^^^"),
                         arguments("missing_field_id.providence",
                                   "Error in missing_field_id.providence on line 3 row 3-5: Field ID missing\n" +
                                   "  int second;\n" +
                                   "--^^^"),
                         arguments("invalid_include.providence",
                                   "Error in invalid_include.providence on line 5 row 1-7: Unexpected token 'include', expected type declaration\n" +
                                   "include \"valid_reference.providence\"\n" +
                                   "^^^^^^^"),
                         arguments("unknown_program.providence",
                                   "Error in unknown_program.providence on line 2 row 6-28: No such include valid_reference for type valid_reference.Message\n" +
                                   "  1: valid_reference.Message message;\n" +
                                   "-----^^^^^^^^^^^^^^^^^^^^^^^"));
    }

    @MethodSource("testParse_ParseFailure")
    @ParameterizedTest
    public void testParse_ParseFailure(String name,
                                       String displayString) throws IOException {
        fromResource("failure", "valid_reference.providence");
        try {
            ProvidenceParser    parser         = new ProvidenceParser(new IDLParserOptions());
            PGlobalFileTypeRegistry registry       = new PGlobalFileTypeRegistry();
            IDLWarningHandler   warningHandler = ex -> {};
            parser.parseFile(fromResource("failure", name), registry, warningHandler);
            fail("no exception");
        } catch (IDLException e) {
            try {
                assertThat(e.displayString(),
                           is(displayString));
            } catch (AssertionError a) {
                e.printStackTrace();
                throw a;
            }
        }
    }

    public static Stream<Arguments> testParse_ConvertFailure() {
        return Stream.of(arguments("unknown_include.pvd",
                                   "Error in unknown_include.pvd on line 1 row 9-29: No such file to be included\n" +
                                   "include \"no_such_file.thrift\";\n" +
                                   "--------^^^^^^^^^^^^^^^^^^^^^"),
                         arguments("unknown_include_type.pvd",
                                   "Error on line 4 row 6-28: No such type 'Unknown' in valid_reference.providence\n" +
                                   "  1: valid_reference.Unknown message;\n" +
                                   "-----^^^^^^^^^^^^^^^^^^^^^^^"),
                         arguments("unknown_type.pvd",
                                   "Error on line 2 row 6-14: Unknown type name OtherType\n" +
                                   "  1: OtherType message;\n" +
                                   "-----^^^^^^^^^"));
    }

    @MethodSource("testParse_ConvertFailure")
    @ParameterizedTest
    public void testParse_ConvertFailure(String name,
                                         String displayString) throws IOException {
        fromResource("failure_convert", "valid_reference.providence");
        try {
            ProvidenceParser    parser         = new ProvidenceParser(new IDLParserOptions());
            PGlobalFileTypeRegistry registry       = new PGlobalFileTypeRegistry();
            IDLWarningHandler   warningHandler = ex -> {};
            parser.parseFile(fromResource("failure_convert", name), registry, warningHandler);
            fail("no exception");
        } catch (IDLException e) {
            try {
                assertThat(e.displayString(),
                           is(displayString));
            } catch (AssertionError a) {
                e.printStackTrace();
                throw a;
            }
        }
    }

    public static Stream<Arguments> testParse_Regression() {
        return Stream.of(
                arguments(listOf("model.pvd", "included.thrift")),
                arguments(listOf("missing_field_id_allowed.providence")));
    }

    @MethodSource("testParse_Regression")
    @ParameterizedTest
    public void testParse_Regression(List<String> resources)
            throws IOException {
        try {
            List<Path> files = resources.stream()
                    .map(name -> fromResource("regression", name))
                    .collect(Collectors.toList());
            ProvidenceParser parser = new ProvidenceParser(new IDLParserOptions());
            PGlobalFileTypeRegistry registry = new PGlobalFileTypeRegistry();
            IDLWarningHandler warningHandler = ex -> {};
            PNamespaceFileTypeRegistry rtr = parser.parseFile(files.get(0), registry, warningHandler);
            assertThat(rtr, is(notNullValue()));
        } catch (IDLException e) {
            System.err.println(e.displayString());
            throw e;
        }
    }

    public static Stream<Arguments> testParse_CompatRegression() {
        return Stream.of(
                arguments(listOf("model.pvd", "included.thrift")),
                arguments(listOf("thrift-compat.pvd", "included.thrift")));
    }

    @MethodSource("testParse_CompatRegression")
    @ParameterizedTest
    public void testParse_CompatRegression(List<String> resources)
            throws IOException {
        try {
            List<Path> files = resources.stream()
                    .map(name -> fromResource("regression", name))
                    .collect(Collectors.toList());
            ProvidenceParser parser = new ProvidenceParser(new IDLParserOptions()
                    .parseOptions("providence:allowThriftCompat"));
            PGlobalFileTypeRegistry registry = new PGlobalFileTypeRegistry();
            IDLWarningHandler warningHandler = ex -> {};
            PNamespaceFileTypeRegistry rtr = parser.parseFile(files.get(0), registry, warningHandler);
            assertThat(rtr, is(notNullValue()));
        } catch (IDLException e) {
            System.err.println(e.displayString());
            throw e;
        }
    }

    private Path fromResource(String group, String name) {
        return ResourceUtil.fromResource(temp.resolve(name),
                                         "/io/pvdnc/idl/pvd/parser/" + group + "/" + name);
    }
}
