package io.pvdnc.idl.pvd.parser;

import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.idl.parser.IDLWarningHandler;
import io.pvdnc.idl.pvd.test_util.ResourceUtil;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.io.InputStream;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class ProvidenceFileParserTest {
    private static final String REGRESSION = "/io/pvdnc/idl/pvd/parser/regression/";

    public static Stream<Arguments> testParse_Regression() {
        return Stream.of(arguments("model.pvd", "model-parsed.pvd"));
    }

    @MethodSource("testParse_Regression")
    @ParameterizedTest
    public void testParse_Regression(String resource, String golden)
            throws IOException {
        ProvidenceFileParser parser = new ProvidenceFileParser(PPropertyMap.empty());
        IDLWarningHandler warningHandler = ex -> System.out.println(ex.displayString());
        try (InputStream in = getClass().getResourceAsStream(REGRESSION + resource)) {
            assertThat(parser.parse(in, resource, warningHandler).displayString(), CoreMatchers.is(ResourceUtil.resourceAsString(REGRESSION + golden)));
        }
    }
}
