package io.pvdnc.idl.pvd.parser;

import io.pvdnc.core.PMessageBuilder;
import net.morimekta.collect.util.Binary;
import net.morimekta.lexer.UncheckedLexerException;
import io.pvdnc.core.PMessage;
import io.pvdnc.core.reflect.CEnumDescriptor;
import io.pvdnc.core.reflect.CMessageDescriptor;
import io.pvdnc.core.registry.PNamespaceFileTypeRegistry;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.types.PType;
import io.pvdnc.idl.parser.IDLException;
import io.pvdnc.idl.parser.IDLToken;
import io.pvdnc.idl.parser.IDLTokenizer;
import io.pvdnc.idl.parser.IDLWarningHandler;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.mockito.Mockito.mock;

public class ProvidenceConstParserTest {
    public static CEnumDescriptor enumType;
    public static CMessageDescriptor messageType;
    public static PNamespaceFileTypeRegistry registry;

    @BeforeAll
    public static void setUp() {
        registry = new PNamespaceFileTypeRegistry(Path.of("foo.pvd"), "foo");

        CEnumDescriptor.Builder eb = CEnumDescriptor.builder("foo", "MyEnum");
        eb.value("FOO", 1).value("BAR", 2);
        enumType = eb.build();
        registry.register(enumType);
        CMessageDescriptor.Builder mb = CMessageDescriptor.builder("foo", "MyMessage");
        mb.field("b", PType.BOOL).id(1);
        mb.field("i", PType.INT).id(2);
        messageType = mb.build();
        registry.register(messageType);
    }

    @Test
    public void testBadArgs() {
        IDLWarningHandler warningHandler = mock(IDLWarningHandler.class);
        try {
            new ProvidenceConstParser(registry, warningHandler, () -> PType.STRING, listOf());
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No tokens in value tokens list"));
        }
    }

    public static Stream<Arguments> testParseConstData() {
        PMessageBuilder mb = messageType.newBuilder();
        mb.set(1, true);
        mb.set(2, 42);
        PMessage message = mb.build();

        return Stream.of(arguments("true", PType.BOOL, true),
                         arguments("false", PType.BOOL, false),
                         arguments("42", PType.BYTE, (byte) 42),
                         arguments("042", PType.BYTE, (byte) 34),
                         arguments("0x42", PType.BYTE, (byte) 66),
                         arguments("4200", PType.SHORT, (short) 4200),
                         arguments("04200", PType.SHORT, (short) 2176),
                         arguments("0x4200", PType.SHORT, (short) 16896),
                         arguments("42000000", PType.INT, 42000000),
                         arguments("042000000", PType.INT, 8912896),
                         arguments("0x42000000", PType.INT, 1107296256),
                         arguments("42000000000000", PType.LONG, 42000000000000L),
                         arguments("042000000000000", PType.LONG, 2336462209024L),
                         arguments("0x42000000000000", PType.LONG, 18577348462903296L),
                         arguments("42.42424", PType.FLOAT, 42.42424f),
                         arguments("42.4242424242", PType.DOUBLE, 42.4242424242),
                         arguments("'foo'", PType.STRING, "foo"),
                         arguments("\"foo\"", PType.STRING, "foo"),
                         arguments("\"base64\"", PType.BINARY, Binary.fromBase64("base64")),
                         arguments("0x12345678", PType.BINARY, Binary.fromHexString("12345678")),
                         arguments("[42,66]", PListDescriptor.provider(PType.INT.provider()).get(), listOf(42,66)),
                         arguments("[42,66]", PSetDescriptor.provider(PType.INT.provider()).get(), setOf(42,66)),
                         arguments("{42:66,72:21}", PMapDescriptor.provider(PType.INT.provider(), PType.INT.provider()).get(),
                                   mapOf(42,66,72,21)),
                         arguments("{\n" +
                                   "  42: 66\n" +
                                   "  72: 21\n" +
                                   "}", PMapDescriptor.provider(PType.INT.provider(), PType.INT.provider()).get(),
                                   mapOf(42,66,72,21)),
                         arguments("2", enumType, enumType.valueForName("BAR")),
                         arguments("FOO", enumType, enumType.valueForName("FOO")),
                         arguments("{b=true;i=42;}", messageType, message),
                         arguments("{b=true;i=42}", messageType, message),
                         arguments("{\n" +
                                   "  b = true\n" +
                                   "  i = 42\n" +
                                   "}", messageType, message));
    }

    @ParameterizedTest
    @MethodSource("testParseConstData")
    public void testParseConst(String content, PDescriptor descriptor, Object expected) throws IOException {
        IDLTokenizer   tokenizer = new ProvidenceTokenizer(new StringReader(content));
        List<IDLToken> tokens    = new ArrayList<>();
        IDLToken       next      = tokenizer.parseNextToken();
        while (next != null) {
            tokens.add(next);
            next = tokenizer.parseNextToken();
        }
        IDLWarningHandler warningHandler = mock(IDLWarningHandler.class);
        ProvidenceConstParser parser = new ProvidenceConstParser(registry, warningHandler, () -> descriptor, tokens);
        assertThat(parser.get(), is(expected));
        assertThat(parser.get(), is(sameInstance(parser.get())));
    }

    public static Stream<Arguments> testParseConstFailureData() {
        return Stream.of(arguments("true", PType.VOID,
                                   "Error on line 1 row 1-4: void type not allowed in constants\n" +
                                   "true\n" +
                                   "^^^^"),
                         arguments("42\n" +
                                   "{", PType.INT,
                                   "Error on line 2 row 1: Garbage token at end of const value\n" +
                                   "{\n" +
                                   "^"),
                         arguments("42", PType.BOOL,
                                   "Error on line 1 row 1-2: Unknown boolean value\n" +
                                   "42\n" +
                                   "^^"),
                         arguments("420", PType.BYTE,
                                   "Error on line 1 row 1-3: Invalid Byte: Value out of range. Value:\"420\" Radix:10\n" +
                                   "420\n" +
                                   "^^^"),
                         arguments("foo", PType.BYTE,
                                   "Error on line 1 row 1-3: Not a number: 'foo'\n" +
                                   "foo\n" +
                                   "^^^"),
                         arguments("42000", PType.SHORT,
                                   "Error on line 1 row 1-5: Invalid Short: Value out of range. Value:\"42000\" Radix:10\n" +
                                   "42000\n" +
                                   "^^^^^"),
                         arguments("foo", PType.SHORT,
                                   "Error on line 1 row 1-3: Not a number: 'foo'\n" +
                                   "foo\n" +
                                   "^^^"),
                         arguments("4200000000", PType.INT,
                                   "Error on line 1 row 1-10: Invalid Integer: For input string: \"4200000000\"\n" +
                                   "4200000000\n" +
                                   "^^^^^^^^^^"),
                         arguments("foo", PType.INT,
                                   "Error on line 1 row 1-3: Not a number: 'foo'\n" +
                                   "foo\n" +
                                   "^^^"),
                         arguments("42.4242", PType.LONG,
                                   "Error on line 1 row 1-7: Invalid Long: For input string: \"42.4242\"\n" +
                                   "42.4242\n" +
                                   "^^^^^^^"),
                         arguments("foo", PType.LONG,
                                   "Error on line 1 row 1-3: Not a number: 'foo'\n" +
                                   "foo\n" +
                                   "^^^"),
                         arguments("0xff", PType.FLOAT,
                                   "Error on line 1 row 1-4: Invalid Float: For input string: \"0xff\"\n" +
                                   "0xff\n" +
                                   "^^^^"),
                         arguments("foo", PType.FLOAT,
                                   "Error on line 1 row 1-3: Not a number: 'foo'\n" +
                                   "foo\n" +
                                   "^^^"),
                         arguments("0xff", PType.DOUBLE,
                                   "Error on line 1 row 1-4: Invalid Double: For input string: \"0xff\"\n" +
                                   "0xff\n" +
                                   "^^^^"),
                         arguments("foo", PType.DOUBLE,
                                   "Error on line 1 row 1-3: Not a number: 'foo'\n" +
                                   "foo\n" +
                                   "^^^"),
                         arguments("foo", PType.STRING,
                                   "Error on line 1 row 1-3: Not a string value: foo\n" +
                                   "foo\n" +
                                   "^^^"),
                         arguments("\"foo\\l\"", PType.STRING,
                                   "Error on line 1 row 1-7: Bad string escaping: Invalid escaped char: '\\l'\n" +
                                   "\"foo\\l\"\n" +
                                   "^^^^^^^"),
                         arguments("42", PType.BINARY,
                                   "Error on line 1 row 1-2: Not a binary value: '42'\n" +
                                   "42\n" +
                                   "^^"),
                         arguments("\"blæ\"", PType.BINARY,
                                   "Error on line 1 row 1-5: Illegal base64 character -1a\n" +
                                   "\"blæ\"\n" +
                                   "^^^^^"),
                         arguments("foo", PType.BINARY,
                                   "Error on line 1 row 1-3: Not a binary value: 'foo'\n" +
                                   "foo\n" +
                                   "^^^"),
                         arguments("0xfff", PType.BINARY,
                                   "Error on line 1 row 1-5: Odd number of hex (3), must be even for binary\n" +
                                   "0xfff\n" +
                                   "^^^^^"),
                         arguments("42", PListDescriptor.provider(PType.INT.provider()).get(),
                                   "Error on line 1 row 1-2: Invalid list start 42\n" +
                                   "42\n" +
                                   "^^"),
                         arguments("42", PSetDescriptor.provider(PType.INT.provider()).get(),
                                   "Error on line 1 row 1-2: Invalid set start 42\n" +
                                   "42\n" +
                                   "^^"),
                         arguments("42", PMapDescriptor.provider(PType.INT.provider(), PType.INT.provider()).get(),
                                   "Error on line 1 row 1-2: Invalid map start 42\n" +
                                   "42\n" +
                                   "^^"),
                         arguments("\"FOO\"", enumType,
                                   "Error on line 1 row 1-5: Bad enum value '\"FOO\"'\n" +
                                   "\"FOO\"\n" +
                                   "^^^^^"),
                         arguments("BAZ", enumType,
                                   "Error on line 1 row 1-3: Unknown foo.MyEnum value BAZ\n" +
                                   "BAZ\n" +
                                   "^^^"),
                         arguments("foo", messageType,
                                   "Error on line 1 row 1-3: Invalid message start foo: Must be '{'\n" +
                                   "foo\n" +
                                   "^^^"),
                         arguments("{", messageType,
                                   "Error on line 1 row 2: Expected field or end, but got end of file\n" +
                                   "{\n" +
                                   "-^"),
                         arguments("{b}", messageType,
                                   "Error on line 1 row 3: Expected field value sep, but got '}'\n" +
                                   "{b}\n" +
                                   "--^"),
                         arguments("{b=}", messageType,
                                   "Error on line 1 row 4: Unknown boolean value\n" +
                                   "{b=}\n" +
                                   "---^"),
                         arguments("{c=42}", messageType,
                                   "Error on line 1 row 2: No field on foo.MyMessage named 'c'\n" +
                                   "{c=42}\n" +
                                   "-^"));
    }

    @ParameterizedTest
    @MethodSource("testParseConstFailureData")
    public void testParseConstFailure(String content,
                                      PDescriptor descriptor,
                                      String displayString) throws IOException {
        IDLTokenizer   tokenizer = new ProvidenceTokenizer(new StringReader(content));
        List<IDLToken> tokens    = new ArrayList<>();
        IDLToken       next      = tokenizer.parseNextToken();
        while (next != null) {
            tokens.add(next);
            next = tokenizer.parseNextToken();
        }
        try {
            IDLWarningHandler warningHandler = mock(IDLWarningHandler.class);
            ProvidenceConstParser parser = new ProvidenceConstParser(registry,
                                                                     warningHandler,
                                                                     () -> descriptor,
                                                                     tokens);
            parser.get();
            fail("no exception");
        } catch (UncheckedLexerException e) {
            try {
                assertThat(e.getCause(), is(instanceOf(IDLException.class)));
                assertThat(e.displayString(), is(displayString));
            } catch (AssertionError ae) {
                e.printStackTrace();
                throw ae;
            }
        }
    }

    public static Stream<Arguments> testParseConstWarningsData() {
        PMessageBuilder mb = messageType.newBuilder();
        mb.set(1, true);
        mb.set(2, 1);
        PMessage message = mb.build();
        return Stream.of(arguments("{\"b\":true,\"i\":1}",
                                   messageType,
                                   message,
                                   "Error on line 1 row 2-4: Using thrift json-like message syntax is deprecated. Use unquoted field 'name = ' instead.\n" +
                                   "{\"b\":true,\"i\":1}\n" +
                                   "-^^^"),
                         arguments("{\n" +
                                   "  b = true\n" +
                                   "  i = MyEnum.FOO\n" +
                                   "}",
                                   messageType,
                                   message,
                                   "Error on line 3 row 7-16: Using enum reference for value in const, deprecated thrift const feature.\n" +
                                   "  i = MyEnum.FOO\n" +
                                   "------^^^^^^^^^^"));
    }

    @ParameterizedTest
    @MethodSource("testParseConstWarningsData")
    public void testParseConstWarnings(String content,
                                       PDescriptor descriptor,
                                       Object value,
                                       String displayString) throws IOException {
        List<IDLException> warnings  = new ArrayList<>();
        IDLTokenizer       tokenizer = new ProvidenceTokenizer(new StringReader(content));
        List<IDLToken>     tokens    = new ArrayList<>();
        IDLToken           next      = tokenizer.parseNextToken();
        while (next != null) {
            tokens.add(next);
            next = tokenizer.parseNextToken();
        }
        IDLWarningHandler warningHandler = warnings::add;
        ProvidenceConstParser parser = new ProvidenceConstParser(registry,
                                                                 warningHandler,
                                                                 () -> descriptor,
                                                                 tokens);
        assertThat(parser.get(), is(value));
        assertThat(warnings, hasSize(greaterThan(0)));
        assertThat(warnings.get(0).displayString(), is(displayString));
    }

}
