package io.pvdnc.idl.pvd;

import io.pvdnc.idl.parser.IDLParser;
import io.pvdnc.idl.parser.IDLParserOptions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Path;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class ProvidenceParserProviderTest {
    @TempDir
    public Path tempDir;

    @Test
    public void testProvider() {
        ProvidenceParserProvider provider = new ProvidenceParserProvider();
        assertThat(provider.willHandleFile(tempDir.resolve("foo.providence")), is(true));
        assertThat(provider.willHandleFile(tempDir.resolve("foo.pvd")), is(true));
        assertThat(provider.willHandleFile(tempDir.resolve("foo.thrift")), is(false));
        assertThat(provider.willHandleFile(tempDir.resolve("bar.json")), is(false));

        assertThat(provider.getOptions(), is(List.of()));

        IDLParser parser = provider.getParser(new IDLParserOptions());
        assertThat(parser, is(notNullValue()));
    }
}
