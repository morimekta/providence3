/**
 * This package contains a semi-type-safe map that is built to
 * hold configurable "properties" for providence types.
 */
package io.pvdnc.core.property;
