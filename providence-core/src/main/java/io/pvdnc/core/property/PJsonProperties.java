/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.property;

import io.pvdnc.core.PEnum;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMessageDescriptor;
import net.morimekta.collect.UnmodifiableMap;
import io.pvdnc.core.types.PEnumValue;

import java.util.Map;

public class PJsonProperties {
    /**
     * For json structs which certain limitations, can be serialized as an array of values instead of
     * as a JSON object. It will use the fields in declared order.
     * <p>
     * The resulting JSON array will have one entry for each field until the last that had any value
     * (non-null). So the array could contain null entries, meaning the field is not set.
     */
    public static final PProperty<Boolean> JSON_COMPACT;

    /**
     * For json structs and enums this property can control the name used to serialize (and can be
     * parsed for that field or value).
     */
    public static final PProperty<String> JSON_NAME;

    // --- Virtual Properties ---

    /**
     * Map of JSON field names to field. This is based on the 'json.name' property on the
     * field, and this property is on the struct containing the fields. If no field has the
     * 'json.name' property, this should be empty.
     */
    public static final PProperty<Map<String, PField>> JSON_FIELD_MAP;
    /**
     * Map of JSON enum value names to enum value. This is is based on the 'json.name' property
     * on the enum value, and this property is on the enum containing the value. If no value
     * has the 'json.name' property, this should be empty.
     */
    public static final PProperty<Map<String, PEnum>>  JSON_ENUM_MAP;

    private PJsonProperties() {}

    static {
        JSON_COMPACT = new PPersistedProperty<>("json.compact",
                                                Boolean.class,
                                                Boolean.FALSE,
                                                PPropertyTarget.MESSAGE);
        JSON_NAME = new PPersistedProperty<>("json.name",
                                             String.class,
                                             PPropertyTarget.MESSAGE_FIELD,
                                             PPropertyTarget.ENUM_VALUE) {
            @Override
            public String defaultValueFor(PPropertyHolder instance) {
                if (instance instanceof PEnumValue) {
                    return ((PEnumValue) instance).getName();
                } else if (instance instanceof PField) {
                    return ((PField) instance).getName();
                }
                return super.defaultValueFor(instance);
            }
        };
        JSON_FIELD_MAP = new PVirtualMapProperty<>("json.field.map",
                                                   PPropertyTarget.MESSAGE) {
            @Override
            public Map<String, PField> makeValueFor(PPropertyHolder instance) {
                if (!(instance instanceof PMessageDescriptor)) {
                    throw new IllegalArgumentException(
                            "not a message descriptor: " + instance.getClass().getSimpleName());
                }
                UnmodifiableMap.Builder<String, PField> builder    = UnmodifiableMap.newBuilder();
                PMessageDescriptor<?>                   descriptor = (PMessageDescriptor<?>) instance;
                for (PField field : descriptor.allFields()) {
                    if (field.hasProperty(JSON_NAME)) {
                        builder.put(field.getProperty(JSON_NAME), field);
                    }
                }
                return builder.build();
            }
        };

        JSON_ENUM_MAP = new PVirtualMapProperty<>("json.enum.map",
                                               PPropertyTarget.ENUM) {
            @Override
            public Map<String, PEnum> makeValueFor(PPropertyHolder instance) {
                if (!(instance instanceof PEnumDescriptor)) {
                    throw new IllegalArgumentException(
                            "not an enum descriptor: " + instance.getClass().getSimpleName());
                }
                UnmodifiableMap.Builder<String, PEnum> builder    = UnmodifiableMap.newBuilder();
                PEnumDescriptor                        descriptor = (PEnumDescriptor) instance;
                for (PEnum value : descriptor.allValues()) {
                    if (value.$meta().hasProperty(JSON_NAME)) {
                        builder.put(value.$meta().getProperty(JSON_NAME), value);
                    }
                }
                return builder.build();
            }
        };

        PProperty.registerProperties(JSON_COMPACT, JSON_NAME);
    }
}
