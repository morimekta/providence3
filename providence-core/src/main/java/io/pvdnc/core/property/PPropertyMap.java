/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.property;

import net.morimekta.collect.MapBuilder;
import net.morimekta.collect.UnmodifiableMap;
import net.morimekta.collect.UnmodifiableSortedMap;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import static java.lang.System.Logger.Level.ERROR;
import static java.util.Objects.requireNonNull;
import static net.morimekta.collect.UnmodifiableMap.asMap;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.collect.UnmodifiableSortedMap.asSortedMap;
import static net.morimekta.collect.UnmodifiableSortedMap.sortedMapOf;
import static net.morimekta.strings.EscapeUtil.javaEscape;

/**
 * an immutable and thread-safe class containing a set of key-value
 * pairs with getters for handling value-safe access. The parsed values
 * are all assumed to be immutable themselves, e.g. String, Numbers, enum
 * etc.
 */
public final class PPropertyMap implements PPropertyHolder {
    private static final System.Logger LOGGER = System.getLogger(PPropertyMap.class.getName());

    /**
     * @return An empty property map instance.
     */
    public static PPropertyMap empty() {
        return new PPropertyMap();
    }

    /**
     * Make a property map using each odd (1, 3, 5...) value as key
     * and the following as the associated value. Requires an even
     * number of values.
     *
     * @param values The keys and values.
     * @return The final property map.
     */
    public static PPropertyMap of(String... values) {
        if (values.length == 0) return empty();
        if (values.length % 2 != 0) {
            throw new IllegalArgumentException("Not an even number of arguments");
        }
        PPropertyMap.Builder builder = PPropertyMap.newBuilder();
        for (AtomicInteger i = new AtomicInteger(0);
             i.get() < values.length;
             i.addAndGet(2)) {
            String key = values[i.get()];
            String value = values[i.get() + 1];
            try {
                builder.putUnsafe(requireNonNull(key, () -> "param[" + (i.get() / 2) + "]"),
                                  requireNonNull(value, () -> "value[" + (i.get() / 2) + "]"));
            } catch (Exception e) {
                LOGGER.log(ERROR, () -> "Unknown property: " + key + " = \"" + javaEscape(value) + "\"", e);
            }
        }
        return builder.build();
    }

    public static <A> PPropertyMap of(PProperty<A> pa, A va) {
        PPropertyMap.Builder builder = PPropertyMap.newBuilder();
        builder.put(requireNonNull(pa, "pa == null"), requireNonNull(va, "va == null"));
        return builder.build();
    }

    public static <A, B> PPropertyMap of(PProperty<A> pa, A va,
                                         PProperty<B> pb, B vb) {
        PPropertyMap.Builder builder = PPropertyMap.newBuilder();
        builder.put(requireNonNull(pa, "pa == null"), requireNonNull(va, "va == null"));
        builder.put(requireNonNull(pb, "pb == null"), requireNonNull(vb, "vb == null"));
        return builder.build();
    }

    public static <A, B, C> PPropertyMap of(PProperty<A> pa, A va,
                                            PProperty<B> pb, B vb,
                                            PProperty<C> pc, C vc) {
        PPropertyMap.Builder builder = PPropertyMap.newBuilder();
        builder.put(requireNonNull(pa, "pa == null"), requireNonNull(va, "va == null"));
        builder.put(requireNonNull(pb, "pb == null"), requireNonNull(vb, "vb == null"));
        builder.put(requireNonNull(pc, "pc == null"), requireNonNull(vc, "vc == null"));
        return builder.build();
    }

    public static <A, B, C, D> PPropertyMap of(PProperty<A> pa, A va,
                                               PProperty<B> pb, B vb,
                                               PProperty<C> pc, C vc,
                                               PProperty<D> pd, D vd) {
        PPropertyMap.Builder builder = PPropertyMap.newBuilder();
        builder.put(requireNonNull(pa, "pa == null"), requireNonNull(va, "va == null"));
        builder.put(requireNonNull(pb, "pb == null"), requireNonNull(vb, "vb == null"));
        builder.put(requireNonNull(pc, "pc == null"), requireNonNull(vc, "vc == null"));
        builder.put(requireNonNull(pd, "pd == null"), requireNonNull(vd, "vd == null"));
        return builder.build();
    }

    public <T> T getPropertyFor(PProperty<T> property, PPropertyHolder holder) {
        T value = getProperty(property);
        requireNonNull(holder, "holder == null");
        if (value == null) {
            if (property instanceof PPersistedProperty) {
                String stringValue = getUnsafeProperty(property.getName());
                if (stringValue != null) {
                    value = ((PPersistedProperty<T>) property).parseString(stringValue);
                } else {
                    value = ((PPersistedProperty<T>) property).defaultValueFor(holder);
                }
                if (value != null) {
                    updateBackingMap(property, value);
                }
            } else if (property instanceof PVirtualProperty) {
                value = ((PVirtualProperty<T>) property).makeValueFor(holder);
                updateBackingMap(property, value);
            } else {
                throw new IllegalArgumentException("Unhandled property class: " + property.getClass().getSimpleName());
            }
        }
        return value;
    }

    // --- PPropertyHolder ---

    @Override
    public boolean hasProperty(PProperty<?> property) {
        requireNonNull(property, "property == null");
        return backingMap.get().get(property) != null ||
               backingUnsafeMap.containsKey(property.getName());
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T getProperty(PProperty<T> property) {
        requireNonNull(property, "property == null");
        T value = (T) backingMap.get().get(property);
        if (value == null) {
            if (property instanceof PPersistedProperty) {
                String stringValue = backingUnsafeMap.get(property.getName());
                if (stringValue != null) {
                    value = ((PPersistedProperty<T>) property).parseString(stringValue);
                    updateBackingMap(property, value);
                }
            }
        }
        return value;
    }

    @Override
    public Map<String, String> getUncheckedProperties() {
        return backingUnsafeMap;
    }

    // --- Object ---

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PPropertyMap that = (PPropertyMap) o;
        return backingUnsafeMap.equals(that.backingUnsafeMap);
    }

    @Override
    public int hashCode() {
        return Objects.hash(backingMap.get(), backingUnsafeMap);
    }

    @Override
    public String toString() {
        return "PPropertyMap" + backingUnsafeMap;
    }

    // --- Protected ---

    String getUnsafeProperty(String name) {
        requireNonNull(name, "name == null");
        return backingUnsafeMap.get(name);
    }

    // --- Private ---

    private final AtomicReference<UnmodifiableMap<PProperty<?>, Object>> backingMap;
    private final Map<String, String>                                    backingUnsafeMap;

    private PPropertyMap() {
        backingMap = new AtomicReference<>(mapOf());
        backingUnsafeMap = sortedMapOf();
    }

    private PPropertyMap(Map<PProperty<?>, Object> backingMap,
                         Map<String, String> backingUnsafeMap) {
        this.backingMap = new AtomicReference<>(asMap(backingMap));
        this.backingUnsafeMap = asSortedMap(backingUnsafeMap);
    }

    private <T> void updateBackingMap(PProperty<T> property, T finalValue) {
        backingMap.updateAndGet(map -> map.withEntry(property, finalValue));
    }

    // --- Static Utilities ---

    public static Builder newBuilder() {
        return new Builder();
    }

    public final static class Builder {
        private final MapBuilder<PProperty<?>, Object> backingMap;
        private final MapBuilder<String, String>       backingUnsafeMap;

        private Builder() {
            this.backingMap = UnmodifiableMap.newBuilder();
            this.backingUnsafeMap = UnmodifiableSortedMap.newBuilderNaturalOrder();
        }

        public <T> Builder put(PProperty<T> property, T value) {
            requireNonNull(property, "property == null");
            requireNonNull(value, "value == null");
            backingMap.put(property, value);
            if (property instanceof PPersistedProperty) {
                backingUnsafeMap.put(
                        property.getName(),
                        ((PPersistedProperty<T>) property).stringify(value));
            }
            return this;
        }

        public Builder putUnsafe(String propertyName, String value) {
            requireNonNull(propertyName, "propertyName == null");
            requireNonNull(value, "value == null");
            backingUnsafeMap.put(propertyName, value);
            PProperty.findProperty(propertyName).ifPresent(property -> {
                if (property instanceof PPersistedProperty) {
                    backingMap.put(property, ((PPersistedProperty<?>) property).parseString(value));
                }
            });
            return this;
        }

        public Builder putAll(PPropertyMap other) {
            requireNonNull(other, "other == null");
            backingMap.putAll(other.backingMap.get());
            backingUnsafeMap.putAll(other.backingUnsafeMap);
            return this;
        }

        public PPropertyMap build() {
            return new PPropertyMap(
                    backingMap.build(),
                    backingUnsafeMap.build());
        }
    }
}
