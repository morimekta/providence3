/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.property;

import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PFieldRequirement;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PMessageVariant;
import io.pvdnc.core.types.PType;

public final class PDefaultProperties {
    /**
     * Documentation for the type, field, value etc described.
     */
    public static final PProperty<String> DOCUMENTATION;
    /**
     * If the type, field, value etc have been deprecated. The string value is the deprecation message.
     * Any non-null values means it's deprecated.
     */
    public static final PProperty<String> DEPRECATED;
    /**
     * Only applies to set and map types. If set to true will use a sorted container for
     * the field value instead of the default order-preserving hashed container.
     */
    public static final PProperty<Boolean> SORTED;
    /**
     * Property that references an enum on a non-enum field. Can be used to have a numeric or
     * string field implicitly reference an enum.
     */
    public static final PProperty<String> REF_ENUM;
    /**
     * Property that references an 'arguments' message from any field. This can be used to
     * parameterize filling in of that field. E.g. used in <code>graphql</code>.
     */
    public static final PProperty<String> REF_ARGUMENTS;
    /**
     * Property that defined named aliases for each enum value. The aliases must be entered
     * as comma-separated list of alias names. Each alias must still be unique according to
     * the providence enum naming requirements.
     * <p>
     * Note that that alias name is <b>ONLY</b> used when de-serializing and looking up the
     * enum, never when serializing and stringifying the value.
     */
    public static final PProperty<String> ENUM_ALIASES;
    /**
     * Name a enum value to be the default for the enum. Set on the enum itself, and should
     * contain the name of the enum value.
     */
    public static final PProperty<String> ENUM_DEFAULT;
    /**
     * For fields in a one-of group, this is the name of the group. All fields in 'union' message
     * variants are in the 'union' one_of group.
     */
    public static final PProperty<String> ONE_OF_GROUP;
    /**
     * Type of message to be generated for the descriptor. This is mainly only relevant for
     * the code generator, but can influence from serialization formats.
     */
    public static final PProperty<PFieldRequirement> FIELD_REQUIREMENT;
    /**
     * Type of message to be generated for the descriptor. This is mainly only relevant for
     * the code generator, but can influence from serialization formats.
     */
    public static final PProperty<PMessageVariant> MESSAGE_VARIANT;
    /**
     * For exception messages (see 'message.variant'), use the field with this name as
     * the exception message.
     */
    public static final PProperty<String> EXCEPTION_MESSAGE;

    private PDefaultProperties() {}

    static {
        DOCUMENTATION =
                new PPersistedProperty<>("documentation", String.class, PPropertyTarget.values());
        DEPRECATED =
                new PPersistedProperty<>("deprecated", String.class, PPropertyTarget.values());
        SORTED =
                new PPersistedProperty<>("sorted", Boolean.class, Boolean.FALSE, true, PPropertyTarget.MESSAGE_FIELD);
        REF_ENUM =
                new PPersistedProperty<>("ref.enum", String.class, PPropertyTarget.MESSAGE_FIELD);
        REF_ARGUMENTS =
                new PPersistedProperty<>("ref.arguments", String.class, PPropertyTarget.MESSAGE_FIELD);
        ENUM_ALIASES =
                new PPersistedProperty<>("enum.aliases", String.class, PPropertyTarget.ENUM_VALUE);
        ENUM_DEFAULT =
                new PPersistedProperty<>("enum.default", String.class, null, true, PPropertyTarget.ENUM);
        ONE_OF_GROUP =
                new PPersistedProperty<>("one.of", String.class, PPropertyTarget.MESSAGE_FIELD);
        FIELD_REQUIREMENT =
                new PPersistedProperty<>("field.requirement", PFieldRequirement.class, PFieldRequirement.OPTIONAL, PPropertyTarget.MESSAGE_FIELD);
        MESSAGE_VARIANT =
                new PPersistedProperty<>("message.variant", PMessageVariant.class, PMessageVariant.STRUCT, PPropertyTarget.MESSAGE);
        EXCEPTION_MESSAGE =
                new PPersistedProperty<>("exception.message", String.class, PPropertyTarget.MESSAGE) {
                    @Override
                    public String defaultValueFor(PPropertyHolder o) {
                        if (o instanceof PMessageDescriptor) {
                            for (PField field : ((PMessageDescriptor<?>) o).allFields()) {
                                if (field.getType() == PType.STRING) {
                                    return field.getName();
                                }
                            }
                        }
                        return super.defaultValueFor(o);
                    }
                };

        PPersistedProperty.registerProperties(
                DOCUMENTATION,
                DEPRECATED,
                SORTED,
                REF_ENUM,
                REF_ARGUMENTS,
                ENUM_ALIASES,
                ENUM_DEFAULT,
                ONE_OF_GROUP,
                FIELD_REQUIREMENT,
                MESSAGE_VARIANT,
                EXCEPTION_MESSAGE);
    }
}
