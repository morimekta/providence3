/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.property;

import java.util.Map;

/**
 * Interface for types, fields etc that can be assigned
 * custom properties.
 */
public interface PPropertyHolder {
    /**
     * If the holder has the given property.
     * @param property The property to test.
     * @return If the property is present.
     */
    boolean hasProperty(PProperty<?> property);

    /**
     * @param property The property to get.
     * @param <T> The property value type.
     * @return The property value. Returns property default value if not present. May be null.
     *         May throw exception if property is not valid for the holder type. See {@link PPropertyTarget}
     *         for details.
     */
    <T> T getProperty(PProperty<T> property);

    /**
     * Get the full map of unchecked persistent properties.
     * @return The unchecked persistent property map.
     */
    Map<String, String> getUncheckedProperties();
}
