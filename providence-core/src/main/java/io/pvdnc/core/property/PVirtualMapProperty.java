/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.property;

import java.util.Map;

/**
 * Extension to the virtual property for holding a typed map. This is just
 * for convenience, and does not change any behavior.
 *
 * @param <K> The key type.
 * @param <V> The value type.
 */
public abstract class PVirtualMapProperty<K, V> extends PVirtualProperty<Map<K, V>> {
    @SuppressWarnings("unchecked,rawtypes")
    public PVirtualMapProperty(String name,
                               PPropertyTarget... targets) {
        super(name, (Class<Map<K, V>>) (Class) Map.class, targets);
    }

    public abstract Map<K, V> makeValueFor(PPropertyHolder instance);
}
