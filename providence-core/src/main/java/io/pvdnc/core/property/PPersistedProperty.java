/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.property;

import io.pvdnc.core.PEnum;
import io.pvdnc.core.types.PDeclaredDescriptor;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PServiceMethod;
import io.pvdnc.core.types.PTypeDefDescriptor;

import java.util.EnumSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Objects.requireNonNull;
import static net.morimekta.collect.UnmodifiableSet.asSet;

public class PPersistedProperty<T> implements PProperty<T> {
    private final String               name;
    private final Class<T>             type;
    private final T                    defaultValue;
    private final boolean              resolve;
    private final Set<PPropertyTarget> targets;

    public PPersistedProperty(String name,
                              Class<T> type,
                              PPropertyTarget... targets) {
        this(name, type, null, false, targets);
    }

    public PPersistedProperty(String name,
                              Class<T> type,
                              T defaultValue,
                              PPropertyTarget... targets) {
        this(name, type, defaultValue, false, targets);
    }

    public PPersistedProperty(String name,
                              Class<T> type,
                              T defaultValue,
                              boolean resolve,
                              PPropertyTarget... targets) {
        this.name = requireNonNull(name);
        this.type = requireNonNull(type);
        this.defaultValue = defaultValue;
        this.resolve = resolve;
        this.targets = asSet(targets);
    }

    public final String getName() {
        return name;
    }

    public final Class<T> getType() {
        return type;
    }

    public final Set<PPropertyTarget> getTargets() {
        return targets;
    }

    /**
     * @return Get the property global default value.
     */
    public final T getDefaultValue() {
        return defaultValue;
    }

    /**
     * Get the default value for the property.
     * <p>
     * If the property is marked with {@code resolve = true} the default property
     * value should be resolved so that:
     * <ul>
     *     <li>
     *         Property on field checks if field is a declared type, and then checks
     *         value for that type.
     *     </li>
     *     <li>
     *         Property on typedef of declared type checks the redefined type.
     *     </li>
     *     <li>
     *         Property on message that is extending another message checks
     *         the extended message type.
     *     </li>
     *     <li>
     *         Property on a service method checks on the service itself.
     *     </li>
     * </ul>
     * Note that services does <b>NOT</b> check on extended services or messages
     * interfaces. Both of these may extend multiple other declaration, so the
     * resolution becomes messy.
     *
     * @param instance The property holder instance to get default value for.
     * @return The resolved default value.
     */
    public T defaultValueFor(PPropertyHolder instance) {
        requireNonNull(instance, "instance == null");
        if (instance instanceof PTypeDefDescriptor) {
            if (((PTypeDefDescriptor) instance).getDescriptor() instanceof PDeclaredDescriptor) {
                return ((PDeclaredDescriptor) ((PTypeDefDescriptor) instance).getDescriptor()).getProperty(this);
            }
        }
        if (resolve) {
            if (instance instanceof PField) {
                if (((PField) instance).getDeclaredDescriptor() instanceof PDeclaredDescriptor) {
                    T value = ((PDeclaredDescriptor) ((PField) instance).getDeclaredDescriptor()).getProperty(this);
                    if (value != null) return value;
                }
            }
            if (instance instanceof PMessageDescriptor) {
                if (((PMessageDescriptor<?>) instance).getExtending().isPresent()) {
                    return ((PMessageDescriptor<?>) instance).getExtending().get().getProperty(this);
                }
            }
            if (instance instanceof PServiceMethod) {
                return ((PServiceMethod) instance).getOnServiceType().getProperty(this);
            }
        }
        return getDefaultValue();
    }

    /**
     * Parse a string value to the property type.
     *
     * @param value The string value to be parsed.
     * @return The parsed value.
     * @throws NumberFormatException If numeric field in not valid.
     * @throws IllegalArgumentException If otherwise unable to parse the value.
     * @throws IllegalStateException If no parse method is provided for the type.
     */
    @SuppressWarnings("unchecked,rawtypes")
    public T parseString(String value) {
        requireNonNull(value, "value == null");
        if (type.equals(String.class)) {
            return cast(value);
        }
        if (type.equals(Boolean.class) || type.equals(Boolean.TYPE)) {
            // a boolean "" should return true.
            return cast(value.isEmpty() || Boolean.parseBoolean(value));
        }
        if (type.equals(Long.class) || type.equals(Long.TYPE)) {
            return cast(Long.parseLong(value));
        }
        if (type.equals(Integer.class) || type.equals(Integer.TYPE)) {
            return cast(Integer.parseInt(value));
        }
        if (type.equals(Double.class) || type.equals(Double.TYPE)) {
            return cast(Double.parseDouble(value));
        }
        if (PEnum.class.isAssignableFrom(type)) {
            return cast(findPEnumValue(type, value));
        }
        if (Enum.class.isAssignableFrom(type)) {
            return cast(findEnumValue((Class) type, value));
        }
        throw new IllegalStateException("No parsing implemented for property '" + getName() + "' as " +
                                        type.getTypeName());
    }

    /**
     * Stringify the value.
     *
     * @param value The value to be a string.
     * @return The string value.
     */
    public String stringify(T value) {
        requireNonNull(value);
        if (value instanceof PEnum) {
            return ((PEnum) value).getName();
        }
        if (value instanceof Enum) {
            return ((Enum<?>) value).name();
        }
        return String.valueOf(value);
    }

    // --- Object

    @Override
    public String toString() {
        return "PProperty{" + name + "=" +
               (defaultValue == null ? "(" + type.getSimpleName() + ")" : String.valueOf(defaultValue)) +
               ": " + targets + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PPersistedProperty<?> other = (PPersistedProperty<?>) o;
        return name.equals(other.name) &&
               type.equals(other.type) &&
               Objects.equals(defaultValue, other.defaultValue) &&
               targets.equals(other.targets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type, defaultValue, targets);
    }

    // --- Private

    @SuppressWarnings("unchecked")
    private T cast(Object o) {
        return (T) o;
    }

    // ------------------------
    // --- Static Utilities ---
    // ------------------------

    protected static Optional<PProperty<?>> findProperty(String name) {
        return Optional.ofNullable(propertyRegister.get(name));
    }

    protected static void registerProperties(PProperty<?>... properties) {
        for (PProperty<?> property : properties) {
            propertyRegister.put(property.getName(), property);
        }
    }

    // --- Private

    private static final Map<String, PProperty<?>> propertyRegister = new ConcurrentHashMap<>();

    private static Object findPEnumValue(Class<?> type, String name) {
        PEnumDescriptor descriptor = PEnumDescriptor.getEnumDescriptor(type);
        return descriptor.valueForName(name);
    }

    private static <E extends Enum<E>> E findEnumValue(Class<E> type, String name) {
        EnumSet<E> all = EnumSet.allOf(type);
        for (E value : all) {
            if (value.name().equals(name)) {
                return value;
            }
        }
        throw new IllegalArgumentException("No value '" + name + "' for enum: " + type.getTypeName());
    }

}
