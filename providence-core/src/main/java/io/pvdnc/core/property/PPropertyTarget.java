/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.property;

/**
 * Possible targets for property.
 */
public enum PPropertyTarget {
    /**
     * Property targeting a file of definitions. These properties are
     * commonly set on the {@code RecursiveTypeRegistry} in
     * {@code io.pvdnc.core.reflect.type}, which represents
     * a single file with loaded definitions. Since no class is usually generated
     * for the whole of a file, these properties are <b>only</b> available at
     * build time.
     */
    FILE,
    /**
     * Property on a type definition. This may affect the type it represents.
     */
    TYPE_DEF,
    /**
     * Property on an enum. This is for the enum type, not for each value. See
     * {@link #ENUM_VALUE}.
     */
    ENUM,
    /**
     * Property on an enum value (name, id pair).
     */
    ENUM_VALUE,
    /**
     * Property on any message (struct, union, exception) or interface.
     */
    MESSAGE,
    /**
     * Property on any message field. This may also apply to virtual message
     * fields. E.g. see services for {@code providence-id-thrift}, which generates
     * messages to wrap method calls.
     */
    MESSAGE_FIELD,
    /**
     * Property on a service.
     */
    SERVICE,
    /**
     * Property on a service method. This may apply to virtual messages generated
     * for some services. E.g. see services for {@code providence-id-thrift}, which generates
     * messages to wrap method calls.
     */
    SERVICE_METHOD,
    ;
}
