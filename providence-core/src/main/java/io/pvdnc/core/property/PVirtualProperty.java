/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.property;

import java.util.Objects;
import java.util.Set;

import static java.util.Objects.requireNonNull;
import static net.morimekta.collect.UnmodifiableSet.asSet;

/**
 * A property that only depends on context and other properties. It
 * has no stored (persistent) state of its own. Examples can be pre-
 * calculated helper structures for serialization etc.
 *
 * @param <T> The value type.
 */
public abstract class PVirtualProperty<T> implements PProperty<T> {
    private final String                       name;
    private final Class<T>                     type;
    private final Set<PPropertyTarget>         targets;

    public PVirtualProperty(String name,
                            Class<T> type,
                            PPropertyTarget... targets) {
        this.name = requireNonNull(name);
        this.type = requireNonNull(type);
        this.targets = asSet(targets);
    }

    public final String getName() {
        return name;
    }

    public final Class<T> getType() {
        return type;
    }

    public final Set<PPropertyTarget> getTargets() {
        return targets;
    }

    public abstract T makeValueFor(PPropertyHolder instance);

    // --- Object

    @Override
    public String toString() {
        return "PVProperty{" + name + "=(" +
               type.getSimpleName() + "): " +
               targets + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PVirtualProperty)) return false;
        PVirtualProperty<?> other = (PVirtualProperty<?>) o;
        return name.equals(other.name) &&
               type.equals(other.type) &&
               targets.equals(other.targets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type, targets);
    }
}
