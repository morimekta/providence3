/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.property;

import java.util.Optional;
import java.util.Set;

/**
 * Interface for designating a property.
 *
 * @param <T> The value type of the property.
 */
public interface PProperty<T> {
    /**
     * The name or 'tag' for the property. This is used as an identifier.
     *
     * @return The property name.
     */
    String getName();

    /**
     * @return Set of targets for the property.
     */
    Set<PPropertyTarget> getTargets();

    /**
     * Find property based on its name or 'tag'.
     *
     * @param name The property name.
     * @return Optional found property.
     */
    static Optional<PProperty<?>> findProperty(String name) {
        return PPersistedProperty.findProperty(name);
    }

    static void registerProperties(PProperty<?>... properties) {
        PPersistedProperty.registerProperties(properties);
    }
}
