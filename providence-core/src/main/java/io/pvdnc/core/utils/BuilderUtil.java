/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.utils;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.PMessageBuilder;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import static java.util.stream.Collectors.toMap;
import static net.morimekta.collect.UnmodifiableList.toList;
import static net.morimekta.collect.UnmodifiableSortedMap.toSortedMap;

/**
 * Convenience methods for handling providence messages.
 */
public final class BuilderUtil {
    /**
     * Build the message from builder if it is not null.
     *
     * @param mob The builder to build.
     * @param <M> Message type.
     * @return The message or null if null input.
     */
    @SuppressWarnings("unchecked")
    public static <M extends PMessage>
    M toImmutableIfNotNull(PMessage mob) {
        if (mob instanceof PMessageBuilder) {
            return (M) ((PMessageBuilder) mob).build();
        }
        return (M) mob;
    }

    /**
     * Mutate the message if it is not null.
     *
     * @param mob Message or builder to mutate.
     * @param <B> The message builder type.
     * @return The builder or null if null input.
     */
    @SuppressWarnings("unchecked")
    public static <B extends PMessageBuilder>
    B toBuilderIfNonNull(PMessage mob) {
        if (mob != null && !(mob instanceof PMessageBuilder)) {
            return (B) mob.mutate();
        }
        return (B) mob;
    }

    /**
     * Build all items of the collection containing message-or-builders. The list must not
     * contain any null items.
     *
     * @param builders List of message-or-builders.
     * @param <M> The message type.
     * @return List of messages or null if null input.
     */
    @SuppressWarnings("unchecked")
    public static <M extends PMessage>
    List<M> toImmutableAll(Collection<? extends PMessage> builders) {
        if (builders == null) {
            return null;
        }
        return builders.stream()
                       .map(m -> (M) toImmutableIfNotNull(m))
                       .collect(toList());
    }

    /**
     * Mutate all items of the collection containing messages. The list must not
     * contain any null items.
     *
     * @param messages List of messages.
     * @param <B> The builder type.
     * @return List of builders or null if null input.
     */
    @SuppressWarnings("unchecked")
    public static <B extends PMessageBuilder>
    List<B> toBuilderAll(Collection<? extends PMessage> messages) {
        if (messages == null) {
            return null;
        }
        return (List<B>) messages.stream()
                                 .map(PMessage::mutate)
                                 .collect(toList());
    }

    /**
     * Mutate all items of the collection containing messages. The list must not
     * contain any null items.
     *
     * @param messages List of messages
     * @param <K> The map key type.
     * @param <M> The message type.
     * @return Map with message values, or null on null input.
     */
    @SuppressWarnings("unchecked")
    public static <K, M extends PMessage>
    Map<K, M> toImmutableValues(Map<K, ? extends PMessage> messages) {
        if (messages == null) {
            return null;
        }
        if (messages instanceof SortedMap) {
            return messages.entrySet()
                           .stream()
                           .collect(toSortedMap(
                                   Map.Entry::getKey,
                                   e -> (M) toImmutableIfNotNull(e.getValue()),
                                   ((SortedMap<K, ? extends PMessage>) messages).comparator()));
        }
        return messages.entrySet()
                       .stream()
                       .collect(toMap(Map.Entry::getKey,
                                      e -> toImmutableIfNotNull(e.getValue())));
    }

    /**
     * Mutate all values of the map containing message-or-builder values. The map must not
     * contain any null items.
     *
     * @param messages Map with message-or-builder values.
     * @param <K> The map key type.
     * @param <B> The builder type.
     * @return Map with builder values or null on null input.
     */
    @SuppressWarnings("unchecked")
    public static <K, B extends PMessageBuilder>
    Map<K, B> toBuilderValues(Map<K, ? extends PMessage> messages) {
        if (messages == null) {
            return null;
        }
        if (messages instanceof SortedMap) {
            return messages.entrySet()
                           .stream()
                           .collect(toSortedMap(
                                   Map.Entry::getKey,
                                   e -> (B) e.getValue().mutate(),
                                   ((SortedMap<K, ? extends PMessage>) messages).comparator()));
        }
        return messages.entrySet()
                       .stream()
                       .collect(toMap(Map.Entry::getKey,
                                      e -> (B) e.getValue().mutate()));
    }

    private BuilderUtil() {}
}
