/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.utils;

import io.pvdnc.core.PEnum;
import io.pvdnc.core.PMessage;
import io.pvdnc.core.types.PField;
import net.morimekta.strings.Stringable;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import static net.morimekta.strings.EscapeUtil.javaEscape;

/**
 * Most of these are pretty trivial methods, but here to vastly simplify
 * generated code when making builder, mutable values or build a message.
 */
public final class ValueUtil {
    public static String asString(Object o) {
        if (o == null) return "null";
        if (o instanceof Optional) {
            Optional<?> opt = (Optional<?>) o;
            if (opt.isEmpty()) {
                return "null";
            }
            return asString(opt.get());
        }
        if (o instanceof PMessage) {
            PMessage message = (PMessage) o;

            AtomicBoolean val     = new AtomicBoolean();
            StringBuilder builder = new StringBuilder();

            builder.append('{');
            for (PField field : message.$descriptor().allFields()) {
                message.optional(field.getId()).ifPresent(value -> {
                    if (val.getAndSet(true)) {
                        builder.append(", ");
                    }
                    builder.append(field.getName()).append("=").append(asString(value));
                });
            }
            builder.append('}');

            return builder.toString();
        }
        if (o instanceof PEnum) {
            return ((PEnum) o).getName();
        }
        if (o instanceof Map) {
            Map<?,?> map = (Map<?,?>) o;

            AtomicBoolean val = new AtomicBoolean();
            StringBuilder builder = new StringBuilder();

            builder.append('{');
            for (Map.Entry<?,?> entry : map.entrySet()) {
                if (val.getAndSet(true)) {
                    builder.append(", ");
                }
                builder.append(asString(entry.getKey()))
                       .append(": ")
                       .append(asString(entry.getValue()));
            }
            builder.append('}');

            return builder.toString();
        }
        if (o instanceof Collection) {
            Collection<?> collection = (Collection<?>) o;

            AtomicBoolean val = new AtomicBoolean();
            StringBuilder builder = new StringBuilder();

            builder.append(collection instanceof Set ? '{' : '[');
            for (Object item : collection) {
                if (val.getAndSet(true)) {
                    builder.append(", ");
                }
                builder.append(asString(item));
            }
            builder.append(collection instanceof Set ? '}' : ']');

            return builder.toString();
        }
        if (o instanceof CharSequence) {
            return "'" + javaEscape((CharSequence) o) + "'";
        }
        return Stringable.asString(o);
    }

    // --- Private ---

    private ValueUtil() {}
}
