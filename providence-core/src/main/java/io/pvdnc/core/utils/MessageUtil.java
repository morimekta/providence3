/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.utils;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.PMessageBuilder;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PType;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;

import static java.util.Objects.requireNonNull;
import static net.morimekta.collect.UnmodifiableSet.toSet;
import static net.morimekta.collect.UnmodifiableSortedSet.toSortedSet;

public final class MessageUtil {
    public static Set<PField> modifiedFields(PMessageBuilder builder) {
        return builder.$descriptor().allFields()
                      .stream()
                      .filter(f -> builder.modified(f.getId()))
                      .collect(toSet());
    }

    public static Set<PField> presentFields(PMessage message) {
        return message.$descriptor().allFields()
                      .stream()
                      .filter(f -> message.has(f.getId()))
                      .collect(toSet());
    }

    public static SortedSet<String> presentFieldsNames(PMessage message) {
        return message.$descriptor().allFields()
                      .stream()
                      .filter(f -> message.has(f.getId()))
                      .map(PField::getName)
                      .collect(toSortedSet());
    }

    /**
     * Make a builder of the target message with all differences between
     * source and target marked as modifications.
     *
     * @param source The source message for changes.
     * @param target The message to apply said changes to.
     * @param <M> The message type.
     * @param <B> The builder result type.
     * @return Builder of target with marked modifications.
     */
    @SuppressWarnings("unchecked")
    public static <M extends PMessage, B extends PMessageBuilder>
    B getTargetModifications(M source, M target) {
        B builder = (B) source.mutate();
        for (PField field : source.$descriptor().allFields()) {
            final int id = field.getId();
            if (!Objects.equals(source.optional(id), target.optional(id))) {
                builder.set(id, target.optional(id).orElse(null));
            }
        }
        return builder;
    }

    /**
     * Look up a key in the message structure. If the key is not found, return the
     * default value for that field, and iterate over the fields until the last one.
     * <p>
     * This differs form {@link #optionalInMessage(PMessage, List)} by handling
     * the fields' default values.
     * <p>
     * <b>NOTE:</b> This method should <b>NOT</b> be used directly in code with
     * constant field enums, in that case you should use optional of the getter
     * and map until you have the last value, which should always return the
     * same, but is compile-time type safe. E.g.:
     *
     * <pre>{@code
     * Optional.ofNullable(message.getFirst())
     *         .map(First::getSecond)
     *         .map(Second::getThird)
     *         .orElse(myDefault);
     * }</pre>
     *
     * @param message The message to look up into.
     * @param fields  Field to get in order.
     * @param <T>     The expected leaf value type.
     * @return The value found or null.
     * @throws IllegalArgumentException When unable to get value from message.
     */
    public static <T> Optional<T> getInMessage(PMessage message, List<PField> fields) {
        requireNonNull(message, "message == null");
        requireNonNull(fields, "fields == null");
        if (fields.isEmpty()) {
            throw new IllegalArgumentException("No fields arguments");
        }
        PField field = fields.get(0);
        if (fields.size() > 1) {
            if (field.getType() != PType.MESSAGE) {
                throw new IllegalArgumentException("Intermediate field " + field.getName() + " is not a message");
            }
            return getInMessage((PMessage) Optional.of(message)
                                                   .map(m -> m.get(field.getId()))
                                                   .or(field::getDefaultValue)
                                                   .or(() -> field.getDeclaredDescriptor().getDefaultValue())
                                                   .orElseGet(() -> ((PMessageDescriptor<?>) field.getResolvedDescriptor()).newBuilder().build()),
                                fields.subList(1, fields.size()));
        } else {
            return Optional.ofNullable(message.get(field.getId()));
        }
    }

    /**
     * Get a field value from a message with optional chaining. If the field is
     * not set, or any message in the chain leading up to the last message is
     * missing, it will return an empty optional, otherwise the leaf field value.
     * Note that this will only check for MESSAGE type if the message is present
     * and needs to be looked up in.
     * <p>
     * This differs from {@link #getInMessage(PMessage, List)} by explicitly
     * <b>NOT</b> handling fields' default values.
     * <p>
     * <b>NOTE:</b> This method should <b>NOT</b> be used directly in code with
     * constant field enums, in that case you should use the optional getter
     * and flatMap until you have the last value, which should always return
     * the same, but is compile-time type safe. E.g.:
     *
     * <pre>{@code
     * message.optionalFirst()
     *        .flatMap(First::optionalSecond)
     *        .flatMap(Second::optionalThird)
     *        .orElse(myDefault);
     * }</pre>
     *
     * @param message The message to start looking up field values in.
     * @param fields  Fields to look up in the message.
     * @param <T>     The expected leaf value type.
     * @return Optional field value.
     */
    public static <T> Optional<T> optionalInMessage(PMessage message, List<PField> fields) {
        if (fields.isEmpty()) {
            throw new IllegalArgumentException("No fields arguments");
        }
        if (message == null) {
            return Optional.empty();
        }
        PField field = fields.get(0);
        if (!message.has(field.getId())) {
            return Optional.empty();
        }

        if (fields.size() > 1) {
            if (field.getType() != PType.MESSAGE) {
                throw new IllegalArgumentException("Intermediate field " + field.getName() + " is not a message");
            }
            // Required to preserve generic typing.
            return optionalInMessage(message.get(field.getId()), fields.subList(1, fields.size()));
        } else {
            return message.optional(field.getId());
        }
    }
}
