/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.utils;

import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PType;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

public final class FieldUtil {
    /**
     * Convert a key path to a list of consecutive fields for recursive lookup.
     *
     * @param rootDescriptor The root message descriptor.
     * @param path            The '.' joined field name key.
     * @return Array of fields.
     */
    public static List<PField> fieldPathToFields(PMessageDescriptor<?> rootDescriptor, String path) {
        requireNonNull(rootDescriptor, "rootDescriptor == null");
        requireNonNull(path, "path == null");
        if (path.isEmpty()) {
            throw new IllegalArgumentException("Empty path");
        }

        ArrayList<PField> fields = new ArrayList<>();
        String[]          parts  = path.split("\\.", Byte.MAX_VALUE);
        for (int i = 0; i < (parts.length - 1); ++i) {
            String name = parts[i];
            if (name.isEmpty()) {
                throw new IllegalArgumentException("Empty field name in '" + path + "'");
            }
            PField field = rootDescriptor.findFieldByName(name);
            if (field == null) {
                throw new IllegalArgumentException(
                        "Message " + rootDescriptor.getTypeName() + " has no field named " + name);
            }
            if (field.getType() != PType.MESSAGE) {
                throw new IllegalArgumentException(
                        "Field '" + name + "' is not of message type in " + rootDescriptor.getTypeName());
            }
            fields.add(field);
            rootDescriptor = (PMessageDescriptor<?>) field.getResolvedDescriptor();
        }

        String name = parts[parts.length - 1];
        if (name.isEmpty()) {
            throw new IllegalArgumentException("Empty field name in '" + path + "'");
        }
        PField field = rootDescriptor.findFieldByName(name);
        if (field == null) {
            throw new IllegalArgumentException(
                    "Message " + rootDescriptor.getTypeName() + " has no field named " + name);
        }
        fields.add(field);
        return fields;
    }

    /**
     * Append field to the given path.
     *
     * @param fields Fields to make key path of.
     * @return The new appended key path.
     */
    public static String fieldPath(List<PField> fields) {
        requireNonNull(fields, "fields == null");
        if (fields.isEmpty()) throw new IllegalArgumentException("No field arguments");
        return fields.stream().map(PField::getName).collect(Collectors.joining("."));
    }

    /**
     * Append field to the given path.
     *
     * @param path The path to be appended to.
     * @param field The field who's name should be appended.
     * @return The new appended key path.
     */
    public static String fieldPathAppend(String path, PField field) {
        if (path == null || path.isEmpty()) {
            return field.getName();
        }
        return path + "." + field.getName();
    }

}
