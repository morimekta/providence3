/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.utils;

import io.pvdnc.core.PEnum;
import io.pvdnc.core.PMessage;
import io.pvdnc.core.PMessageBuilder;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import net.morimekta.collect.ListBuilder;
import net.morimekta.collect.MapBuilder;
import net.morimekta.collect.SetBuilder;
import net.morimekta.collect.util.Binary;

import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;

import static io.pvdnc.core.utils.BuilderUtil.toImmutableIfNotNull;

public final class ConversionUtil {
    /**
     * Coerce value to match the given type descriptor.
     *
     * @param valueType The value type to coerce to.
     * @param value The value to be coerced.
     * @return The coerced value.
     */
    public static Optional<?> coerce(PDescriptor valueType, Object value) {
        return coerceInternal(valueType, value, false);
    }

    /**
     * Coerce value to match the given type descriptor using struct type
     * checking. This means some loose coercion transitions are not allowed.
     *
     * @param valueType The value type to coerce to.
     * @param value The value to be coerced.
     * @return The coerced value.
     */
    public static Optional<?> coerceStrict(PDescriptor valueType, Object value) {
        return coerceInternal(valueType, value, true);
    }

    // --------------------
    // ---   INTERNAL   ---
    // --------------------

    @SuppressWarnings("unchecked")
    private static Optional<?> coerceInternal(PDescriptor valueType, Object val, boolean strict) {
        if (val == null) {
            return Optional.empty();
        }
        if (val instanceof Optional) {
            Optional<?> opt = (Optional<?>) val;
            if (opt.isEmpty()) return opt;
            return coerceInternal(valueType, opt.get(), strict);
        }
        switch (valueType.getType()) {
            case VOID:
                // Void does'nt really care about the value.
                if (Boolean.TRUE.equals(val)) {
                    return Optional.of(Boolean.TRUE);
                } else if (Boolean.FALSE.equals(val)) {
                    throw new IllegalArgumentException("Invalid void value " + val.toString());
                }
                break;
            case BOOL:
                if (val instanceof Boolean) {
                    return Optional.of(val);
                } else if (val instanceof Number && !(val instanceof Float) && !(val instanceof Double)) {
                    return Optional.of(((Number) val).longValue() != 0L);
                } else if (val instanceof PEnum) {  // e.g. enum
                    return Optional.of(((PEnum) val).getValue() != 0);
                } else if (strict) {
                    break;
                } else if (val instanceof CharSequence) {
                    switch (val.toString().toLowerCase(Locale.US)) {
                        case "true":
                        case "t":
                        case "yes":
                        case "y":
                        case "1":
                            return Optional.of(Boolean.TRUE);
                        case "false":
                        case "f":
                        case "no":
                        case "n":
                        case "0":
                            return Optional.of(Boolean.FALSE);
                    }
                    throw new IllegalArgumentException("Unknown boolean value for string '" + val + "'");
                }
                break;
            case BYTE:
                if (val instanceof Number) {
                    return Optional.of((byte) asInteger(valueType, (Number) val, Byte.MIN_VALUE, Byte.MAX_VALUE));
                } else if (val instanceof Boolean) {
                    return Optional.of((Boolean) val ? (byte) 1 : (byte) 0);
                } else if (val instanceof PEnum) {  // e.g. enum
                    return Optional.of((byte) asInteger(valueType,
                                                        ((PEnum) val).getValue(),
                                                        Byte.MIN_VALUE,
                                                        Byte.MAX_VALUE));
                } else if (strict) {
                    break;
                } else if (val instanceof CharSequence) {
                    try {
                        CharSequence cs = (CharSequence) val;
                        if (HEX.matcher(cs).matches()) {
                            return Optional.of(Byte.parseByte(cs.subSequence(2, cs.length()).toString(), 16));
                        } else {
                            return Optional.of(Byte.parseByte(val.toString()));
                        }
                    } catch (NumberFormatException e) {
                        throw new IllegalArgumentException("Invalid string value '" + val + "' for type byte", e);
                    }
                }
                break;
            case SHORT:
                if (val instanceof Number) {
                    return Optional.of((short) asInteger(valueType, (Number) val, Short.MIN_VALUE, Short.MAX_VALUE));
                } else if (val instanceof Boolean) {
                    return Optional.of((Boolean) val ? (short) 1 : (short) 0);
                } else if (val instanceof PEnum) {  // e.g. enum
                    return Optional.of((short) asInteger(valueType,
                                                         ((PEnum) val).getValue(),
                                                         Short.MIN_VALUE,
                                                         Short.MAX_VALUE));
                } else if (strict) {
                    break;
                } else if (val instanceof CharSequence) {
                    try {
                        CharSequence cs = (CharSequence) val;
                        if (HEX.matcher(cs).matches()) {
                            return Optional.of(Short.parseShort(cs.subSequence(2, cs.length()).toString(), 16));
                        } else {
                            return Optional.of(Short.parseShort(val.toString()));
                        }
                    } catch (NumberFormatException e) {
                        throw new IllegalArgumentException("Invalid string value '" + val + "' for type short", e);
                    }
                }
                break;
            case INT:
                if (val instanceof Number) {
                    return Optional.of(asInteger(valueType, (Number) val, Integer.MIN_VALUE, Integer.MAX_VALUE));
                } else if (val instanceof Boolean) {
                    return Optional.of((Boolean) val ? 1 : 0);
                } else if (val instanceof PEnum) {  // e.g. enum
                    return Optional.of(((PEnum) val).getValue());
                } else if (strict) {
                    break;
                } else if (val instanceof CharSequence) {
                    try {
                        CharSequence cs = (CharSequence) val;
                        if (HEX.matcher(cs).matches()) {
                            return Optional.of(Integer.parseInt(cs.subSequence(2, cs.length()).toString(), 16));
                        } else {
                            return Optional.of(Integer.parseInt(val.toString()));
                        }
                    } catch (NumberFormatException e) {
                        throw new IllegalArgumentException("Invalid string value '" + val + "' for type int", e);
                    }
                }
                break;
            case LONG:
                if (val instanceof Float || val instanceof Double) {
                    long l = ((Number) val).longValue();
                    if ((double) l != ((Number) val).doubleValue()) {
                        throw new IllegalArgumentException("Truncating long decimals from " + val.toString());
                    }
                    return Optional.of(l);
                } else if (val instanceof Number) {
                    return Optional.of(((Number) val).longValue());
                } else if (val instanceof Boolean) {
                    return Optional.of((Boolean) val ? 1L : 0L);
                } else if (val instanceof PEnum) {  // e.g. enum
                    return Optional.of((long) ((PEnum) val).getValue());
                } else if (strict) {
                    break;
                } else if (val instanceof CharSequence) {
                    try {
                        CharSequence cs = (CharSequence) val;
                        if (HEX.matcher(cs).matches()) {
                            return Optional.of(Long.parseLong(cs.subSequence(2, cs.length()).toString(), 16));
                        } else {
                            return Optional.of(Long.parseLong(val.toString()));
                        }
                    } catch (NumberFormatException e) {
                        throw new IllegalArgumentException("Invalid string value '" + val + "' for type long", e);
                    }
                }
                break;
            case FLOAT:
                if (val instanceof Number) {
                    return Optional.of(((Number) val).floatValue());
                } else if (strict) {
                    break;
                } else if (val instanceof PEnum) {
                    return Optional.of((float) ((PEnum) val).getValue());
                }
                break;
            case DOUBLE:
                if (val instanceof Number) {
                    return Optional.of(((Number) val).doubleValue());
                } else if (strict) {
                    break;
                } else if (val instanceof PEnum) {
                    return Optional.of((double) ((PEnum) val).getValue());
                }
                break;
            case STRING:
                if (val instanceof CharSequence) {
                    return Optional.of(val.toString());
                } else if (strict) {
                    break;
                } else if (val instanceof PEnum) {
                    return Optional.of(((PEnum) val).getName());
                } else {
                    return Optional.of(val.toString());
                }
            case BINARY:
                if (val instanceof Binary) {
                    return Optional.of(val);
                } else if (strict) {
                    break;
                } else if (val instanceof CharSequence) {
                    return Optional.of(Binary.fromBase64(val.toString()));
                }
                break;
            case ENUM: {
                PEnumDescriptor ed = (PEnumDescriptor) valueType;
                if (val instanceof PEnum) {
                    PEnum verified = ed.findById(((PEnum) val).getValue());
                    if (val.equals(verified)) {
                        return Optional.of(verified);
                    }
                } else if (val instanceof Number && !(val instanceof Double) && !(val instanceof Float)) {
                    int        i  = ((Number) val).intValue();
                    PEnum ev = ed.findById(i);
                    if (ev != null) {
                        return Optional.of(ev);
                    }
                    throw new IllegalArgumentException("Unknown " + ed.getTypeName() +
                                                       " value for id " + val.toString());
                } else if (val instanceof CharSequence) {
                    CharSequence cs = (CharSequence) val;
                    if (!strict && UNSIGNED.matcher(cs).matches()) {
                        int        i  = Integer.parseInt(val.toString());
                        PEnum ev = ed.findById(i);
                        if (ev != null) {
                            return Optional.of(ev);
                        }
                    } else if (!strict && HEX.matcher(cs).matches()) {
                        int        i  = Integer.parseInt(cs.subSequence(2, cs.length()).toString(), 16);
                        PEnum ev = ed.findById(i);
                        if (ev != null) {
                            return Optional.of(ev);
                        }
                    } else {
                        PEnum ev = ed.findByName(val.toString());
                        if (ev != null) {
                            return Optional.of(ev);
                        }
                    }
                    throw new IllegalArgumentException("Unknown " + ed.getTypeName() +
                                                       " value for string '" + val.toString() + "'");
                }
                throw new IllegalArgumentException(
                        "Invalid value type " + val.getClass() + " for enum " + ed.getTypeName());
            }
            case MESSAGE: {
                if (val instanceof PMessage) {
                    if (valueType.equals(((PMessage) val).$descriptor())) {
                        return Optional.of(toImmutableIfNotNull((PMessage) val));
                    } else {
                        throw new IllegalArgumentException(
                                "Unable to cast message type " +
                                ((PMessage) val).$descriptor().getTypeName() + " to " +
                                valueType.getTypeName());
                    }
                } else if (!strict && val instanceof Map) {
                    PMessageDescriptor<?> md      = (PMessageDescriptor<?>) valueType;
                    PMessageBuilder builder = md.newBuilder();
                    for (Map.Entry<Object, Object> entry : ((Map<Object, Object>) val).entrySet()) {
                        if (!(entry.getKey() instanceof CharSequence)) {
                            throw new IllegalArgumentException("Invalid message map key: " + entry.getKey().toString());
                        }
                        PField field = Optional.ofNullable(md.findFieldByName(entry.getKey().toString())).orElseThrow(() ->
                                new IllegalArgumentException("No such field " + entry.getKey() + " in " + md.getTypeName()));
                        builder.set(field.getId(),
                                    coerceInternal(field.getResolvedDescriptor(), entry.getValue(), false).orElse(null));
                    }
                    return Optional.of(builder.build());
                }
                throw new IllegalArgumentException(
                        "Invalid value type " + val.getClass() + " for message " + valueType.toString());
            }
            case LIST: {
                if (val instanceof Collection) {
                    PListDescriptor<Object> pl      = (PListDescriptor<Object>) valueType;
                    ListBuilder<Object>     builder = pl.builder(((Collection<?>) val).size());
                    for (Object o : (Collection<?>) val) {
                        Object value = coerceInternal(pl.getItemType(), o, strict).orElse(null);
                        if (value != null) {
                            builder.add(value);
                        } else if (strict) {
                            throw new IllegalArgumentException("Null value in list");
                        }
                    }
                    return Optional.of(builder.build());
                }
                throw new IllegalArgumentException(
                        "Invalid value type " + val.getClass() + " for " + valueType.toString());
            }
            case SET:
                if (val instanceof Collection) {
                    PSetDescriptor<Object> pl      = (PSetDescriptor<Object>) valueType;
                    SetBuilder<Object>     builder = pl.builder(((Collection<?>) val).size());
                    for (Object o : (Collection<?>) val) {
                        Object value = coerceInternal(pl.getItemType(), o, strict).orElse(null);
                        if (value != null) {
                            builder.add(value);
                        } else if (strict) {
                            throw new IllegalArgumentException("Null value in set");
                        }
                    }
                    return Optional.of(builder.build());
                }
                throw new IllegalArgumentException(
                        "Invalid value type " + val.getClass() + " for " + valueType.toString());
            case MAP:
                if (val instanceof Map) {
                    PMapDescriptor<Object, Object> pl      = (PMapDescriptor<Object, Object>) valueType;
                    MapBuilder<Object, Object>     builder = pl.builder(((Map<?, ?>) val).size());
                    for (Map.Entry<Object, Object> entry : ((Map<Object, Object>) val).entrySet()) {
                        Object key   = coerceInternal(pl.getKeyType(), entry.getKey(), strict).orElse(null);
                        Object value = coerceInternal(pl.getValueType(), entry.getValue(), strict).orElse(null);
                        if (key != null && value != null) {
                            builder.put(key, value);
                        } else if (strict) {
                            throw new IllegalArgumentException("Null key or value in map");
                        }
                    }
                    return Optional.of(builder.build());
                }
                throw new IllegalArgumentException(
                        "Invalid value type " + val.getClass() + " for " + valueType.toString());
        }
        throw new IllegalArgumentException("Invalid value type " + val.getClass() + " for type " + valueType.getType());
    }

    private static int asInteger(PDescriptor descriptor, Number value, int min, int max) {
        if (value instanceof Float || value instanceof Double) {
            long l = value.longValue();
            if ((double) l != value.doubleValue()) {
                throw new IllegalArgumentException(
                        "Truncating " + descriptor.getTypeName() + " decimals from " + value.toString());
            }
            return validateInRange(descriptor.getTypeName(), l, min, max);
        } else {
            return validateInRange(descriptor.getTypeName(), value.longValue(), min, max);
        }
    }

    private static int validateInRange(String type, long l, int min, int max) throws IllegalArgumentException {
        if (l < min) {
            throw new IllegalArgumentException(type + " value outside of bounds: " + l + " < " + min);
        } else if (l > max) {
            throw new IllegalArgumentException(type + " value outside of bounds: " + l + " > " + max);
        }
        return (int) l;
    }

    private static final Pattern UNSIGNED = Pattern.compile("(0|[1-9][0-9]*)");
    private static final Pattern HEX      = Pattern.compile("0x[0-9a-fA-F]+");

    private ConversionUtil() {}
}
