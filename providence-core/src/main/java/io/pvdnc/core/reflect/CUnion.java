/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.reflect;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.PUnion;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.utils.ValueUtil;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public class CUnion implements PMessage, PUnion {
    private final CMessageDescriptor descriptor;
    private final int fieldId;
    private final Object value;

    CUnion(CMessageDescriptor descriptor,
           int fieldId,
           Object value) {
        this.descriptor = descriptor;
        this.fieldId = fieldId;
        this.value = value;
    }

    // --- PMessage

    @Override
    public boolean has(int field) {
        return fieldId == field;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T get(int field) {
        if (fieldId == field) {
            return (T) value;
        }
        PField fd = descriptor.findFieldById(field);
        if (fd == null) return null;
        return (T) fd.getDefaultValue()
                     .or(() -> fd.getDeclaredDescriptor().getDefaultValue())
                     .orElse(null);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> Optional<T> optional(int field) {
        if (field == fieldId) {
            return Optional.of((T) value);
        }
        return Optional.empty();
    }

    @Override
    public CMessageBuilder mutate() {
        return new CMessageBuilder(descriptor, unionFieldIsSet() ? Map.of(fieldId, value) : Map.of());
    }

    @Override
    public CMessageDescriptor $descriptor() {
        return descriptor;
    }

    // --- PUnion

    public int unionFieldId() {
        return fieldId;
    }

    // --- Object ---

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CUnion cMessage = (CUnion) o;
        return Objects.equals(descriptor, cMessage.descriptor) &&
               Objects.equals(fieldId, cMessage.fieldId) &&
               Objects.equals(value, cMessage.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(descriptor, fieldId, value);
    }

    @Override
    public String toString() {
        return $descriptor().getTypeName() + ValueUtil.asString(this);
    }
}
