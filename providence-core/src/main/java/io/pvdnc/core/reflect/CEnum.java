/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.reflect;

import io.pvdnc.core.PEnum;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PEnumValue;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * A contained enum value. This object represents the actual enum value.
 */
public class CEnum implements PEnum, Comparable<CEnum> {
    private final PEnumValue meta;

    /**
     * Make a contained enum value.
     * @param meta The enum value meta info.
     */
    public CEnum(PEnumValue meta) {
        requireNonNull(meta, "meta == null");
        this.meta = meta;
    }

    // --- PEnum

    @Override
    public PEnumValue $meta() {
        return meta;
    }

    @Override
    public PEnumDescriptor $descriptor() {
        return meta.descriptor();
    }

    // --- Comparable

    @Override
    public int compareTo(CEnum o) {
        return Integer.compare(getValue(), o.getValue());
    }

    // --- Object

    @Override
    public String toString() {
        return meta.getName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CEnum cEnum = (CEnum) o;
        return Objects.equals(meta, cEnum.meta);
    }

    @Override
    public int hashCode() {
        return meta.hashCode();
    }
}
