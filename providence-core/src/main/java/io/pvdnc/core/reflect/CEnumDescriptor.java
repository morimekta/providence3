/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.reflect;

import io.pvdnc.core.PEnum;
import io.pvdnc.core.property.PDefaultProperties;
import io.pvdnc.core.property.PProperty;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.types.PEnumDescriptor;
import net.morimekta.collect.UnmodifiableMap;
import io.pvdnc.core.types.PEnumValue;
import net.morimekta.strings.NamingUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static java.util.Objects.requireNonNull;
import static net.morimekta.collect.UnmodifiableList.asList;
import static net.morimekta.collect.UnmodifiableMap.toMap;
import static net.morimekta.strings.EscapeUtil.javaEscape;

/**
 * This class represents a contained enum type. It fulfills the role
 * of the generated enum descriptor, and can be used to generate dynamic
 * types on the fly, e.g. in testing.
 */
public class CEnumDescriptor extends PEnumDescriptor {
    private final List<CEnum> values;
    private final CEnum defaultValue;
    private final Map<String, CEnum>  nameToValue;
    private final Map<Integer, CEnum> idToValue;

    public CEnumDescriptor(String namespace,
                           String enumName,
                           CEnum defaultValue,
                           PPropertyMap properties,
                           List<CEnum> values) {
        super(namespace, enumName, properties);
        this.values = asList(values);
        this.defaultValue = defaultValue;

        UnmodifiableMap.Builder<String, CEnum> nameToValueBuilder = UnmodifiableMap.newBuilder();
        for (CEnum cEnum : values) {
            nameToValueBuilder.put(cEnum.getName(), cEnum);
            String aliases = cEnum.$meta().getProperty(PDefaultProperties.ENUM_ALIASES);
            if (aliases != null) {
                for (String alias : aliases.split(",")) {
                    nameToValueBuilder.put(alias.strip(), cEnum);
                }
            }
        }
        this.nameToValue = nameToValueBuilder.build();
        this.idToValue = values.stream().collect(toMap(CEnum::getValue));
    }

    @Override
    public List<CEnum> allValues() {
        return values;
    }

    @Override
    public CEnum findById(Integer id) {
        if (id == null) return null;
        return idToValue.get(id);
    }

    @Override
    public CEnum findByName(String name) {
        if (name == null) return null;
        return nameToValue.get(name);
    }

    @Override
    public CEnum valueForId(int id) {
        return (CEnum) super.valueForId(id);
    }

    @Override
    public CEnum valueForName(String name) {
        return (CEnum) super.valueForName(name);
    }

    @Override
    public Optional<CEnum> getDefaultValue() {
        return Optional.ofNullable(defaultValue);
    }

    /**
     * Create a new {@link CEnumDescriptor.Builder} instance. It will start
     * building a new enum type.
     *
     * @param namespace The self-recognized namespace of the enum.
     * @param enumName The name of the enum.
     * @return The enum descriptor builder.
     */
    public static Builder builder(String namespace, String enumName) {
        return new Builder(namespace, enumName);
    }

    public static class Builder {
        private final String               namespace;
        private final String               enumName;
        private final PPropertyMap.Builder properties;
        private final List<CEnum>          values;

        private final AtomicReference<CEnumDescriptor> descriptor = new AtomicReference<>();

        private Builder(String namespace, String enumName) {
            this.namespace = requireNonNull(namespace, "namespace == null");
            this.enumName = requireNonNull(enumName, "enumName == null");
            this.properties = PPropertyMap.newBuilder();
            this.values = new ArrayList<>();
        }

        public Builder value(String name) {
            return value(name, PPropertyMap.empty());
        }

        public Builder value(String name, PPropertyMap properties) {
            int value = values.stream()
                              .mapToInt(PEnum::getValue)
                              .max()
                              .orElse(0) + 1;
            return value(name, value, properties);
        }

        public Builder value(String name, int value) {
            return value(name, value, PPropertyMap.empty());
        }

        /**
         * Add a value entry to the enum.
         *
         * This uses a pretty lenient enum name checking here. This effectively becomes a global
         * requirements for all enum names regardless. These are meant to at least allow most
         * names that {@link net.morimekta.strings.NamingUtil#format(CharSequence, NamingUtil.Format)}
         * can handle.
         *
         * @param name The enum value name.
         * @param value The enum value number.
         * @param properties The enum properties.
         * @return The builder.
         */
        public Builder value(String name, int value, PPropertyMap properties) {
            requireNonNull(name, "name == null");
            requireNonNull(properties, "properties == null");
            if (name.isEmpty()) {
                throw new IllegalArgumentException("Empty enum value name");
            }
            if (!name.matches("^[-._a-zA-Z0-9]+$")) {
                throw new IllegalArgumentException("Invalid enum value name: '" + javaEscape(name) + "', only ASCII allowed");
            }
            String normalizedName = NamingUtil.format(name, NamingUtil.Format.SNAKE_UPPER);

            values.stream()
                  .filter(v -> v.getValue() == value)
                  .findAny()
                  .ifPresent(v -> {
                      throw new IllegalArgumentException(
                              "Enum value conflict, " + value + " already exists as " + v.getName() +
                              " when creating " + name);
                  });
            values.stream()
                  .filter(v -> v.getName().equals(name))
                  .findAny()
                  .ifPresent(v -> {
                      throw new IllegalArgumentException(
                              "Enum name conflict, value with name " + name + " already exists");
                  });
            values.stream()
                  .filter(v -> NamingUtil.format(v.getName(), NamingUtil.Format.SNAKE_UPPER).equals(normalizedName))
                  .findAny()
                  .ifPresent(v -> {
                      throw new IllegalArgumentException(
                              "Enum name conflict, normalized name " + normalizedName + " already exists as " + v.getName() +
                              " when creating " + name);
                  });

            values.add(new CEnum(new PEnumValue(name, value, properties, descriptor::get)));
            return this;
        }

        public Builder properties(PPropertyMap map) {
            properties.putAll(map);
            return this;
        }

        public <T> Builder property(PProperty<T> property, T value) {
            properties.put(property, value);
            return this;
        }

        public Builder property(String name, String value) {
            properties.putUnsafe(name, value);
            return this;
        }

        public CEnumDescriptor build() {
            PPropertyMap map = properties.build();

            CEnum def = null;
            if (map.hasProperty(PDefaultProperties.ENUM_DEFAULT)) {
                String defName = map.getProperty(PDefaultProperties.ENUM_DEFAULT);
                def = values.stream().filter(v -> v.getName().equals(defName)).findFirst().orElse(null);
            }

            CEnumDescriptor built = new CEnumDescriptor(
                    namespace,
                    enumName,
                    def,
                    map,
                    asList(values));
            descriptor.set(built);
            return built;
        }
    }
}
