/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.reflect;

import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PServiceDescriptor;
import io.pvdnc.core.types.PServiceMethod;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

public class CServiceDescriptor extends PServiceDescriptor {
    public CServiceDescriptor(String namespace,
                              String serviceName,
                              PPropertyMap properties,
                              List<Supplier<? extends PServiceDescriptor>> extending,
                              List<PServiceMethod> methods) {
        super(namespace, serviceName, properties, extending, methods);
    }

    public static Builder newBuilder(String programName, String name) {
        return new Builder(programName, name);
    }

    public static class Builder {
        private final String                                       namespace;
        private final String                                       serviceName;
        private final PPropertyMap.Builder                         properties;
        private final List<PServiceMethod>                         methods;
        private final List<Supplier<? extends PServiceDescriptor>> extending;
        private final AtomicReference<CServiceDescriptor>          onService;

        public Builder(String namespace, String serviceName) {
            this.namespace = namespace;
            this.serviceName = serviceName;
            this.properties = PPropertyMap.newBuilder();
            this.onService = new AtomicReference<>();
            this.methods = new ArrayList<>();
            this.extending = new ArrayList<>();
        }

        public Builder extending(PServiceDescriptor service) {
            return extending(() -> service);
        }

        public Builder extending(Supplier<PServiceDescriptor> service) {
            extending.add(service);
            return this;
        }

        public Builder properties(PPropertyMap properties) {
            this.properties.putAll(properties);
            return this;
        }

        public Builder method(String methodName,
                              PMessageDescriptor<?> responseType,
                              PMessageDescriptor<?> requestType) {
            return method(methodName, () -> responseType, () -> requestType, PPropertyMap.of());
        }

        public Builder method(String methodName,
                              PMessageDescriptor<?> responseType,
                              PMessageDescriptor<?> requestType,
                              PPropertyMap properties) {
            return method(methodName, () -> responseType, () -> requestType, properties);
        }

        public Builder method(String methodName,
                              Supplier<PMessageDescriptor<?>> responseType,
                              Supplier<PMessageDescriptor<?>> requestType,
                              PPropertyMap properties) {
            methods.add(new PServiceMethod(methodName, responseType, requestType, properties, onService::get));
            return this;
        }

        public CServiceDescriptor build() {
            CServiceDescriptor service = new CServiceDescriptor(namespace, serviceName, properties.build(), extending, methods);
            onService.set(service);
            return service;
        }
    }
}
