/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.reflect;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.types.PField;
import net.morimekta.collect.UnmodifiableMap;
import io.pvdnc.core.utils.ValueUtil;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public class CMessage implements PMessage {
    private final CMessageDescriptor descriptor;
    private final Map<Integer, Object> values;

    CMessage(CMessageDescriptor descriptor,
             Map<Integer, Object> values) {
        this.descriptor = descriptor;
        this.values = UnmodifiableMap.asMap(values);
    }

    // --- PMessage

    @Override
    public boolean has(int field) {
        return values.containsKey(field);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T get(int field) {
        PField fd = descriptor.findFieldById(field);
        if (fd == null) return null;
        return (T) optional(field)
                .or(fd::getDefaultValue)
                .or(() -> fd.getDeclaredDescriptor().getDefaultValue())
                .orElse(null);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> Optional<T> optional(int field) {
        return Optional.ofNullable((T) values.get(field));
    }

    @Override
    public CMessageBuilder mutate() {
        return new CMessageBuilder(descriptor, values);
    }

    @Override
    public CMessageDescriptor $descriptor() {
        return descriptor;
    }

    // --- Object ---

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CMessage cMessage = (CMessage) o;
        return Objects.equals(descriptor, cMessage.descriptor) &&
               Objects.equals(values, cMessage.values);
    }

    @Override
    public int hashCode() {
        return Objects.hash(descriptor, values);
    }

    @Override
    public String toString() {
        return $descriptor().getTypeName() + ValueUtil.asString(this);
    }
}
