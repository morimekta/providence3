/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.reflect;

import io.pvdnc.core.property.PDefaultProperties;
import io.pvdnc.core.property.PProperty;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PMessageVariant;
import net.morimekta.collect.UnmodifiableMap;
import net.morimekta.strings.NamingUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

import static net.morimekta.collect.UnmodifiableList.asList;
import static net.morimekta.collect.UnmodifiableMap.toMap;

public class CMessageDescriptor extends PMessageDescriptor<CMessage> {
    private final List<PField>                     fields;
    private final UnmodifiableMap<Integer, PField> fieldById;
    private final UnmodifiableMap<String, PField>  fieldByName;
    private final AtomicReference<List<PField>>    allFields;

    private CMessageDescriptor(String namespace,
                               String typeName,
                               PPropertyMap properties,
                               Supplier<? extends PMessageDescriptor<?>> extending,
                               List<Supplier<? extends PMessageDescriptor<?>>> implementing,
                               List<PField> fields) {
        super(namespace, typeName, extending, implementing, properties);
        this.fields = asList(fields);
        this.fieldById = fields.stream().collect(toMap(PField::getId));
        this.fieldByName = fields.stream().collect(toMap(PField::getName));
        this.allFields = new AtomicReference<>();
    }

    @Override
    public List<PField> declaredFields() {
        return fields;
    }

    @Override
    public List<PField> allFields() {
        return allFields.updateAndGet(af -> {
            if (af == null) {
                List<PField> tmp = new ArrayList<>();
                getExtending().ifPresent(ex -> tmp.addAll(ex.allFields()));
                tmp.addAll(fields);
                af = asList(tmp);
            }
            return af;
        });
    }

    @Override
    public PField findFieldById(int id) {
        PField field = fieldById.get(id);
        if (field == null) {
            return getExtending().map(ex -> ex.findFieldById(id)).orElse(null);
        }
        return field;
    }

    @Override
    public PField findFieldByName(String name) {
        if (name == null) return null;
        PField field = fieldByName.get(name);
        if (field == null) {
            return getExtending().map(ex -> ex.findFieldByName(name)).orElse(null);
        }
        return field;
    }

    @Override
    public CMessageBuilder newBuilder() {
        if (getProperty(PDefaultProperties.MESSAGE_VARIANT) == PMessageVariant.INTERFACE) {
            throw new IllegalStateException("Interfaces do not support builders");
        }
        return new CMessageBuilder(this, new HashMap<>());
    }

    public static Builder builder(String namespace, String name) {
        return new Builder(namespace, name);
    }

    public static class Builder {
        public static class Field {
            private final String name;
            private final Supplier<? extends PDescriptor> type;
            private final PPropertyMap.Builder properties;

            private Supplier<?> defaultValue;
            private Integer id;
            private Supplier<? extends PMessageDescriptor<?>> arguments;

            private Field(String name,
                          Supplier<? extends PDescriptor> type) {
                this.name = name;
                this.type = type;
                this.properties = PPropertyMap.newBuilder();
            }

            public Field id(int id) {
                this.id = id;
                return this;
            }

            public Field properties(PPropertyMap propertyMap) {
                properties.putAll(propertyMap);
                return this;
            }

            public Field property(String name, String value) {
                properties.putUnsafe(name, value);
                return this;
            }

            public <T> Field property(PProperty<T> name, T value) {
                properties.put(name, value);
                return this;
            }

            public Field arguments(Supplier<? extends PMessageDescriptor<?>> arguments) {
                this.arguments = arguments;
                return this;
            }

            public Field defaultValue(Supplier<?> supplier) {
                this.defaultValue = supplier;
                return this;
            }
        }

        private final String                                          namespace;
        private final String                                          name;
        private final PPropertyMap.Builder                            properties;
        private final List<Field>                                     fieldsBuilders;
        private final List<Supplier<? extends PMessageDescriptor<?>>> implementing;
        private       Supplier<? extends PMessageDescriptor<?>>       extending;

        private Builder(String namespace,
                        String name) {
            this.namespace = namespace;
            this.name = name;
            this.properties = PPropertyMap.newBuilder();
            this.fieldsBuilders = new ArrayList<>();
            this.implementing = new ArrayList<>();
        }

        public Builder extending(Supplier<PMessageDescriptor<?>> extending) {
            this.extending = extending;
            return this;
        }

        public Builder implementing(Supplier<PMessageDescriptor<?>> implementing) {
            this.implementing.add(implementing);
            return this;
        }

        public Builder properties(PPropertyMap map) {
            this.properties.putAll(map);
            return this;
        }

        public Builder property(String name, String value) {
            this.properties.putUnsafe(name, value);
            return this;
        }

        public <T> Builder property(PProperty<T> property, T value) {
            this.properties.put(property, value);
            return this;
        }

        public Field field(String name, PDescriptor type) {
            Field field = new Field(name, () -> type);
            fieldsBuilders.add(field);
            return field;
        }

        public Field field(String name, Supplier<? extends PDescriptor> type) {
            Field field = new Field(name, type);
            fieldsBuilders.add(field);
            return field;
        }

        public CMessageDescriptor build() {
            AtomicReference<CMessageDescriptor> typeRef       = new AtomicReference<>();
            AtomicInteger                       nextUnknownId = new AtomicInteger(-1);

            List<PField>         fields        = new ArrayList<>();
            Map<Integer, PField> fieldById     = new HashMap<>();
            Map<String, PField>  fieldByName   = new HashMap<>();
            Map<String, PField>  fieldNormName = new HashMap<>();

            for (Field field : this.fieldsBuilders) {
                int id = Optional.ofNullable(field.id).orElseGet(nextUnknownId::getAndDecrement);
                if (fieldById.containsKey(id)) {
                    throw new IllegalStateException("Duplicate field ID " + id);
                }
                if (fieldByName.containsKey(field.name)) {
                    throw new IllegalStateException("Duplicate field name " + field.name);
                }
                String normalizedName = normalizeFieldName(field.name);
                if (fieldNormName.containsKey(normalizedName)) {
                    PField other = fieldNormName.get(normalizedName);
                    throw new IllegalStateException("Conflicting field name " + normalizedName + ": " +
                                                    field.name + " <-> " + other.getName());
                }

                PField descriptor = new PField(
                        id,
                        field.type,
                        field.name,
                        field.defaultValue,
                        field.properties.build(),
                        field.arguments,
                        typeRef::get);
                fieldById.put(id, descriptor);
                fieldByName.put(field.name, descriptor);
                fieldNormName.put(normalizedName, descriptor);
                fields.add(descriptor);
            }
            typeRef.set(new CMessageDescriptor(namespace, name, properties.build(), extending, implementing, fields));
            return typeRef.get();
        }
    }

    private static String normalizeFieldName(String name) {
        return NamingUtil.format(name, NamingUtil.Format.CAMEL);
    }
}
