/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.reflect;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.PMessageBuilder;
import io.pvdnc.core.property.PDefaultProperties;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PMessageVariant;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.types.PType;
import io.pvdnc.core.utils.ConversionUtil;
import net.morimekta.collect.UnmodifiableList;
import net.morimekta.collect.UnmodifiableMap;
import net.morimekta.collect.UnmodifiableSet;
import net.morimekta.collect.UnmodifiableSortedMap;
import net.morimekta.collect.UnmodifiableSortedSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import static io.pvdnc.core.utils.ConversionUtil.coerce;
import static io.pvdnc.core.utils.ValueUtil.asString;

public class CMessageBuilder implements PMessageBuilder {
    private final CMessageDescriptor   descriptor;
    private final Set<Integer>         modified;
    private final Map<Integer, Object> values;
    private final PMessageVariant      variant;

    public CMessageBuilder(CMessageDescriptor descriptor, Map<Integer, Object> values) {
        this.descriptor = descriptor;
        this.values = new HashMap<>(values);
        this.modified = new HashSet<>();
        this.variant = descriptor.getProperty(PDefaultProperties.MESSAGE_VARIANT);
    }

    @Override
    public boolean has(int field) {
        return values.containsKey(field);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T get(int field) {
        PField fd = descriptor.findFieldById(field);
        if (fd == null) return null;
        return (T) Optional.ofNullable(values.get(field))
                .or(fd::getDefaultValue)
                .or(() -> fd.getDeclaredDescriptor().getDefaultValue())
                .orElse(null);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> Optional<T> optional(int field) {
        return Optional.ofNullable((T) values.get(field));
    }

    @Override
    public PMessage build() {
        switch (variant) {
            case EXCEPTION:
                return new CException(descriptor, values, makeMessage());
            case UNION:
                if (values.isEmpty()) return new CUnion(descriptor, Integer.MIN_VALUE, null);
                Map.Entry<Integer, Object> e = values.entrySet().iterator().next();
                return new CUnion(descriptor, e.getKey(), e.getValue());
            // case INTERFACE: // should not be able to get here at all.
            default:
                return new CMessage(descriptor, values);
        }
    }

    @Override
    public CMessageBuilder mutate() {
        return new CMessageBuilder(descriptor, values);
    }

    @Override
    public PMessageDescriptor<?> $descriptor() {
        return descriptor;
    }

    @Override
    public void set(int field, Object value) {
        PField fd = descriptor.findFieldById(field);
        if (fd == null) {
            throw new IllegalArgumentException("No field id " + field + " on " + descriptor.getTypeName());
        }
        if (value == null) {
            clear(field);
        } else {
            if (variant == PMessageVariant.UNION) {
                modified.addAll(values.keySet());
                values.clear();
            }
            modified.add(field);
            values.put(field, coerce(fd.getResolvedDescriptor(), value).orElse(null));
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T mutable(int field) {
        PField fd = descriptor.findFieldById(field);
        if (fd == null) {
            throw new IllegalArgumentException("No field id " + field + " on " + descriptor.getTypeName());
        }
        modified.add(field);
        switch (fd.getType()) {
            case MESSAGE: {
                PMessage value = (PMessage) values.computeIfAbsent(field, id -> ((PMessageDescriptor<?>) fd.getResolvedDescriptor()).newBuilder());
                if (!(value instanceof PMessageBuilder)) {
                    value = value.mutate();
                    values.put(field, value);
                }
                return (T) value;
            }
            case LIST: {
                List<?> value = (List<?>) values.computeIfAbsent(field, id -> new ArrayList<>());
                if (value instanceof UnmodifiableList) {
                    value = new ArrayList<>(value);
                    values.put(field, value);
                }
                return (T) value;
            }
            case SET: {
                PSetDescriptor<?> psd = (PSetDescriptor<?>) fd.getResolvedDescriptor();
                Set<?> value = (Set<?>) values.computeIfAbsent(field, id -> psd.isSorted() ? new TreeSet<>() : new HashSet<>());
                if (value instanceof UnmodifiableSortedSet) {
                    value = new TreeSet<>(value);
                    values.put(field, value);
                } else if (value instanceof UnmodifiableSet) {
                    value = new HashSet<>(value);
                    values.put(field, value);
                }
                return (T) value;
            }
            case MAP: {
                PMapDescriptor<?,?> pmd = (PMapDescriptor<?, ?>) fd.getResolvedDescriptor();
                Map<?,?> value = (Map<?,?>) values.computeIfAbsent(field, id -> pmd.isSorted() ? new TreeMap<>() : new HashMap<>());
                if (value instanceof UnmodifiableSortedMap) {
                    value = new TreeMap<>(value);
                    values.put(field, value);
                } else if (value instanceof UnmodifiableMap) {
                    value = new HashMap<>(value);
                    values.put(field, value);
                }
                return (T) value;
            }
            default: {
                return (T) values.computeIfAbsent(field, id -> fd.getDefaultValue());
            }
        }
    }

    @Override
    public void clear(int field) {
        PField fd = descriptor.findFieldById(field);
        if (fd == null) {
            throw new IllegalArgumentException("No field id " + field + " on " + descriptor.getTypeName());
        }
        if (values.remove(field) != null &&
                descriptor.findFieldById(field) != null) {
            modified.add(field);
        }
    }

    @Override
    public boolean modified(int field) {
        return modified.contains(field);
    }

    // ---- Object ----

    @Override
    public String toString() {
        return descriptor.getTypeName() + asString(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CMessageBuilder builder = (CMessageBuilder) o;
        return descriptor.equals(builder.descriptor) &&
                modified.equals(builder.modified) &&
                values.equals(builder.values);
    }

    @Override
    public int hashCode() {
        return Objects.hash(descriptor, modified, values);
    }

    // ---- Internal ----

    private String makeMessage() {
        String messageFieldName = descriptor.getProperty(PDefaultProperties.EXCEPTION_MESSAGE);
        if (messageFieldName != null) {
            PField messageField = descriptor.findFieldByName(messageFieldName);
            if (messageField != null) {
                return (String) ConversionUtil.coerce(PType.STRING, values.get(messageField.getId()))
                        .orElse(null);
            }
        }
        return null;
    }
}
