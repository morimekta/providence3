/*
 * Copyright 2016 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.rpc;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.types.PServiceMethod;

import java.io.IOException;

@FunctionalInterface
public interface PServiceCallHandler {
    /**
     * Candle a call to given method.
     *
     * @param method The method to be called.
     * @param request The request message.
     * @param <RQ> The request message type.
     * @param <RS> The response message type.
     * @return The response message.
     * @throws IOException If call failed.
     */
    <RQ extends PMessage, RS extends PMessage>
    RS handleCall(PServiceMethod method, RQ request) throws IOException;
}
