/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core;

import io.pvdnc.core.impl.AnyBinary;
import io.pvdnc.core.impl.AnyWrapper;
import io.pvdnc.core.types.PMessageDescriptor;
import net.morimekta.collect.util.Binary;
import io.pvdnc.core.io.PSerializer;
import io.pvdnc.core.registry.PSimpleTypeRegistry;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Optional;
import java.util.Set;

import static java.util.Objects.requireNonNull;
import static net.morimekta.collect.UnmodifiableSet.setOf;

/**
 * Interface for holding any message value.
 */
public interface PAny {
    /**
     * Get the message contained in the Any.
     *
     * @param <M> The message type.
     * @return The contained message.
     */
    <M extends PMessage> M getMessage();

    /**
     * @return True if the any contains a message. In case of the {@link AnyBinary} class
     *         it is based on the presence of enough info to determine if it *can* contain
     *         an actual message, not necessarily that it can be parsed.
     */
    boolean isPresent();

    /**
     * Check if the contained message is of given type.
     *
     * @param descriptor The message type descriptor to check for.
     * @return If the contained message is of the given type.
     */
    boolean isA(PMessageDescriptor<?> descriptor);

    /**
     * Get the message if is is of the requested type.
     *
     * @param descriptor The message descriptor to get message as.
     * @param <M> The message type.
     * @return Optional with the parsed message or empty if not matching the
     *         desired type.
     */
    default <M extends PMessage> Optional<M> optionalIfA(PMessageDescriptor<M> descriptor) {
        if (isA(descriptor)) {
            return Optional.of(getMessage());
        }
        return Optional.empty();
    }

    /**
     * Get the wrapped message packed using the given serializer. If this is
     * already a packed binary 'any' instance, it will try to re-encode
     * the message using the new
     *
     * @param serializer The serializer to pack with.
     * @return The packed any struct.
     */
    AnyBinary pack(PSerializer serializer);

    /**
     * Unpack the any struct and return the wrapped message.
     *
     * @return The wrapped unpacked message.
     */
    AnyWrapper unpack();

    /**
     * Wrap a message in any.
     *
     * @param message The message to be wrapped.
     * @return The wrapped any struct.
     */
    static AnyWrapper wrap(PMessage message) {
        return new AnyWrapper(message);
    }

    /**
     * @return Get the empty any struct.
     */
    static PAny empty() {
        return AnyWrapper.EMPTY;
    }

    /**
     * Pack message in a binary any holder. The packed any struct
     * is by itself serializable, and should be the type used when
     * serializing 'any' fields.
     *
     * @param message The message to pack.
     * @param serializer The serializer to be used.
     * @return The packed any struct.
     */
    static AnyBinary pack(PMessage message, PSerializer serializer) {
        return AnyBinary.newBuilder()
                .setTypeName(message.$descriptor().getTypeName())
                .setMediaType(serializer.mediaTypes().get(0))
                .setData(anyPack(message, serializer))
                .build();
    }

    Set<String> TYPE_NAME_FIELD = setOf("@type", "@", "__type", "__type__");

    // ---- Private ----

    private static Binary anyPack(PMessage message, PSerializer serializer) {
        requireNonNull(message, "message == null");
        requireNonNull(serializer, "serializer == null");

        PSimpleTypeRegistry.getInstance().register(message.$descriptor());
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            serializer.writeTo(message, out);
            return Binary.wrap(out.toByteArray());
        } catch (IOException e) {
            throw new UncheckedIOException(e.getMessage(), e);
        }
    }
}
