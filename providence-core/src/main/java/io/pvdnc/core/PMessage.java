/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core;

import io.pvdnc.core.types.PMessageDescriptor;

import java.util.Optional;

public interface PMessage {
    /**
     * Checks if the message has the field set.
     *
     * @param field The field ID to check.
     * @return If the field is present.
     */
    boolean has(int field);

    /**
     * Get the value of a field. This method will try to
     * return a non-null value for the field regardless of
     * whether the field is set or not. It will use both
     * field-set and value based defaults to resolve into
     * a value if possible.
     *
     * @param field The field ID to get value for.
     * @param <T> The value type of the field.
     * @return The result. If the field does not exist, the
     *         return value is undefined, usually null.
     */
    <T> T get(int field);

    /**
     * Get the specified value for the field.
     *
     * @param field The field ID to get value for.
     * @param <T> The value type of the field.
     * @return Optional with the value if set (see {@link #has(int)}),
     *         and empty optional is not set. Returns empty if
     *         the field does not exist.
     */
    <T> Optional<T> optional(int field);

    /**
     * Mutate the message and create a builder from the current
     * state. The current object will be untouched by the new builder
     * even it that also was a builder instance.
     *
     * @return The builder instance of the message.
     */
    PMessageBuilder mutate();

    /**
     * @return The message descriptor for the message type.
     */
    PMessageDescriptor<?> $descriptor();

}
