/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.io;

import net.morimekta.collect.util.LazyCachedSupplier;

import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

import static java.lang.System.Logger.Level.WARNING;
import static java.util.Optional.ofNullable;

public class PSerializerProvider {
    private static final Supplier<PSerializerProvider> INSTANCE = LazyCachedSupplier.lazyCache(() -> new PSerializerProvider().load());
    public static PSerializerProvider getInstance() {
        return INSTANCE.get();
    }

    private static final System.Logger LOGGER = System.getLogger(PSerializerProvider.class.getName());

    private final Map<String, PSerializer> serializerMap;
    private final AtomicReference<PSerializer> defaultSerializer;

    public PSerializerProvider() {
        serializerMap = new ConcurrentHashMap<>();
        defaultSerializer = new AtomicReference<>();
    }

    public PSerializerProvider load() {
        PSerializer.listSerializers().forEach(this::register);
        return this;
    }

    public PSerializerProvider setDefault(String mediaType) {
        PSerializer serializer = forMediaType(mediaType)
                .orElseThrow(() -> new IllegalArgumentException("No serializer for media type: " + mediaType));
        defaultSerializer.set(serializer);
        return this;
    }

    public PSerializerProvider registerDefault(PSerializer serializer) {
        Objects.requireNonNull(serializer, "serializer == null");
        this.defaultSerializer.set(serializer);
        this.serializerMap.put("*/*", serializer);
        register(serializer);
        return this;
    }

    public PSerializerProvider register(PSerializer serializer) {
        setDefaultIfMissing(serializer);
        for (String mediaType : serializer.mediaTypes()) {
            PSerializer old = serializerMap.put(normalizeMediaType(mediaType), serializer);
            if (old != null && !Objects.equals(old, serializer)) {
                LOGGER.log(WARNING, () -> String.format("Replacing serializer for %s: %s -> %s",
                                                        normalizeMediaType(mediaType),
                                                        old.toString(),
                                                        serializer.toString()));
            }
        }
        return this;
    }

    /**
     * Unregister a specific serializer from the provided set of serializer.
     * <p>
     * <b>PS: Only Visible For Testing!</b>
     *
     * @param serializer The serializer to unregister.
     */
    public void unregister(PSerializer serializer) {
        serializerMap.entrySet().removeIf(s -> s == serializer);
        if (defaultSerializer.get() == serializer) {
            defaultSerializer.set(null);
        }
    }

    public Optional<PSerializer> forMediaTypes(Collection<String> mediaTypes) {
        for (String mediaType : mediaTypes) {
            PSerializer serializer = serializerMap.get(normalizeMediaType(mediaType));
            if (serializer != null) {
                return Optional.of(serializer);
            }
        }
        return Optional.empty();
    }

    public Optional<PSerializer> forMediaType(String mediaType) {
        return ofNullable(serializerMap.get(normalizeMediaType(mediaType)));
    }

    public PSerializer defaultSerializer() {
        return Optional.ofNullable(defaultSerializer.get())
                .orElseThrow(() -> new IllegalStateException("No default serializer"));
    }

    private void setDefaultIfMissing(PSerializer serializer) {
        defaultSerializer.updateAndGet(d -> {
            if (d == null) {
                serializerMap.put("*/*", serializer);
                return serializer;
            }
            return d;
        });
    }

    private static String normalizeMediaType(String mediaType) {
        return mediaType.replaceAll(";.*", "").strip().toLowerCase(Locale.US);
    }
}
