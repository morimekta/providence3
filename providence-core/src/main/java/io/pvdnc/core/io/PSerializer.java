/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.io;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.types.PMessageDescriptor;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.ServiceLoader;

import static net.morimekta.collect.UnmodifiableList.asList;

public interface PSerializer {
    /**
     * List media types handled by the serializer.
     *
     * @return The media types handled by this serializer.
     */
    List<String> mediaTypes();

    /**
     * Read message from stream.
     * @param descriptor The message descriptor to be read.
     * @param in The input stream to read from.
     * @param <M> The message type.
     * @return The message read.
     * @throws IOException If reading the message failed.
     */
    <M extends PMessage> M readFrom(PMessageDescriptor<M> descriptor, InputStream in) throws IOException;

    /**
     * Write message to stream.
     *
     * @param message The message to be written.
     * @param out The stream to write to.
     * @throws IOException If writing the message failed.
     */
    void writeTo(PMessage message, OutputStream out) throws IOException;

    /**
     * @return If the serializer is handling messages strictly.
     */
    boolean isStrict();

    /**
     * Make a strict version of the serializer.
     * @return The strict serializer.
     */
    PSerializer strict();

    /**
     * @return List of available serializers.
     */
    static List<PSerializer> listSerializers() {
        return asList(kServiceLoader);
    }
    ServiceLoader<PSerializer> kServiceLoader = ServiceLoader.load(PSerializer.class);
}
