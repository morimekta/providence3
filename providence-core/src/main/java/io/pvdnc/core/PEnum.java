/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core;

import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PEnumValue;

/**
 * Base class for declared enums.
 */
public interface PEnum {
    /**
     * Get the meta struct describing this enum value.
     *
     * @return The enum value meta struct.
     */
    PEnumValue $meta();

    /**
     * Get the descriptor of the enum as a whole.
     *
     * @return The enum descriptor.
     */
    PEnumDescriptor $descriptor();

    // --- Defaults ---

    /**
     * @return Get the enum value number.
     */
    default int getValue() {
        return $meta().getValue();
    }

    /**
     * @return Get the enum value declared name.
     */
    default String getName() {
        return $meta().getName();
    }
}
