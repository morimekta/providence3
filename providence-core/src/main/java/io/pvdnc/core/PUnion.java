/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core;

import io.pvdnc.core.types.PField;

import java.util.Optional;

public interface PUnion extends PMessage {
    /**
     * Get the field ID of the union field if present. If
     * no field is set will return a number that is not a valid field,
     * usually {@link Integer#MIN_VALUE}.
     *
     * @return The union field ID. Or {@link Integer#MIN_VALUE} if not set.
     */
    int unionFieldId();

    /**
     * Check if the union field is set.
     *
     * @return True if the union field is set.
     */
    default boolean unionFieldIsSet() {
        return unionFieldId() != Integer.MIN_VALUE;
    }

    /**
     * Get the field set by the union. Throw exception if not set, or
     * set to an ID pointing to no known field.
     *
     * @return The set union field.
     */
    default PField unionField() {
        if (!unionFieldIsSet()) {
            throw new IllegalStateException("No union field set on " + $descriptor().getTypeName());
        }
        int id = unionFieldId();
        return Optional.ofNullable($descriptor().findFieldById(id)).orElseThrow(
                () -> new IllegalStateException("No field for union field id " + id + " on " + $descriptor().getTypeName()));
    }
}
