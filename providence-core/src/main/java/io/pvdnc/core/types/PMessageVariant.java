/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.types;

import java.util.Locale;

import static java.util.Objects.requireNonNull;

public enum PMessageVariant {
    /**
     * A 'normal' struct, also called 'message', 'object' or 'record'
     * in other systems.
     */
    STRUCT,
    /**
     * A union is a message that can only contain a single value. Some
     * systems can replace a union with it's value.
     */
    UNION,
    /**
     * Exceptions are messages that extend {@link Exception} class and
     * handles possible cause and stack-traces. In all other cases they
     * are identical to normal {@link #STRUCT}.
     */
    EXCEPTION,
    /**
     * Interface is a message without a real implementation.
     */
    INTERFACE,
    ;

    @Override
    public String toString() {
        return name().toLowerCase(Locale.US);
    }

    public static PMessageVariant variantForName(String name) {
        requireNonNull(name, "name == null");
        for (PMessageVariant variant : values()) {
            if (variant.toString().equals(name)) {
                return variant;
            }
        }
        throw new IllegalArgumentException("Unknown message variant '" + name + "'");
    }
}
