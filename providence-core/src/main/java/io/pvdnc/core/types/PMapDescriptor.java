/*
 * Copyright 2015-2016 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.types;

import net.morimekta.collect.MapBuilder;
import net.morimekta.collect.UnmodifiableMap;
import net.morimekta.collect.UnmodifiableSortedMap;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * Descriptor for a map with key and item type.
 */
public final class PMapDescriptor<Key, Value> implements PDescriptor {
    private final Supplier<? extends PDescriptor> keyType;
    private final Supplier<? extends PDescriptor> valueType;
    private final boolean sorted;

    public PMapDescriptor(Supplier<? extends PDescriptor> keyType,
                          Supplier<? extends PDescriptor> valueType,
                          boolean sorted) {
        this.keyType = keyType;
        this.valueType = valueType;
        this.sorted = sorted;
    }

    public PDescriptor getKeyType() {
        return keyType.get();
    }

    public PDescriptor getValueType() {
        return valueType.get();
    }

    public boolean isSorted() {
        return sorted;
    }

    @SuppressWarnings("unchecked")
    public MapBuilder<Key, Value> builder(int capacity) {
        if (sorted) {
            return (MapBuilder<Key, Value>) UnmodifiableSortedMap.newBuilderNaturalOrder(capacity);
        }
        return UnmodifiableMap.newBuilder(capacity);
    }

    // --- PDescriptor ---

    @Override
    public String getQualifiedName(String programContext) {
        return "map<" + getKeyType().getQualifiedName(programContext) + "," +
               getValueType().getQualifiedName(programContext) + ">";
    }

    @Override
    public PType getType() {
        return PType.MAP;
    }

    @Override
    public Optional<Map<Key, Value>> getDefaultValue() {
        if (isSorted()) {
            return Optional.of(UnmodifiableSortedMap.sortedMapOf());
        } else {
            return Optional.of(UnmodifiableMap.mapOf());
        }
    }

    // --- Type ---

    @Override
    public String getTypeName() {
        return "map<" + getKeyType().getTypeName() + "," + getValueType().getTypeName() + ">";
    }

    // --- Object ---

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof PMapDescriptor)) {
            return false;
        }
        PMapDescriptor<?, ?> other = (PMapDescriptor<?, ?>) o;
        return sorted == other.sorted &&
               getKeyType().equals(other.getKeyType()) &&
               getValueType().equals(other.getValueType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(PMapDescriptor.class, sorted, getKeyType(), getValueType());
    }

    @Override
    public String toString() {
        return getTypeName();
    }

    // --- Static Utilities ---

    public static <K, V> Supplier<PMapDescriptor<K, V>> provider(
            Supplier<? extends PDescriptor> keyType,
            Supplier<? extends PDescriptor> valueType) {
        PMapDescriptor<K, V> map = new PMapDescriptor<>(keyType, valueType, false);
        return () -> map;
    }

    public static <K extends Comparable<K>, V> Supplier<PMapDescriptor<K, V>> sortedProvider(
            Supplier<? extends PDescriptor> keyType,
            Supplier<? extends PDescriptor> valueType) {
        PMapDescriptor<K, V> map = new PMapDescriptor<>(keyType, valueType, true);
        return () -> map;
    }
}
