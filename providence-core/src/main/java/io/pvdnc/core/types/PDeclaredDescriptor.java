/*
 * Copyright 2016 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.types;

import io.pvdnc.core.property.PProperty;
import io.pvdnc.core.property.PPropertyHolder;
import io.pvdnc.core.property.PPropertyMap;

import java.util.Map;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Base class for the definition of anything explicitly declared in providence.
 */
public abstract class PDeclaredDescriptor implements PDescriptor, PPropertyHolder {
    public static final String DESCRIPTOR_FIELD_NAME = "kDescriptor";

    private final String namespace;
    private final String typeName;
    private final String fullName;
    private final PType type;
    private final PDeclarationType declarationType;
    private final PPropertyMap properties;

    public PDeclaredDescriptor(String namespace,
                               String typeName,
                               PType type,
                               PDeclarationType declarationType,
                               PPropertyMap properties) {
        this.namespace = requireNonNull(namespace, "namespace == null");
        this.typeName = requireNonNull(typeName, "typeName == null");
        this.type = requireNonNull(type, "type == null");
        this.declarationType = requireNonNull(declarationType, "declarationType == null");
        this.properties = requireNonNull(properties, "properties == null");
        this.fullName = namespace + "." + typeName;
    }

    /**
     * @return The declared type's namespace. Declared types always have
     *         a namespace.
     */
    public String getNamespace() {
        return namespace;
    }

    /**
     * @return The declared type's simple name without namespace.
     */
    public String getSimpleName() {
        return typeName;
    }

    /**
     * @return The declaration type of the definition.
     */
    public PDeclarationType getDeclarationType() {
        return declarationType;
    }

    // --- PPropertyHolder

    @Override
    public boolean hasProperty(PProperty<?> property) {
        return properties.hasProperty(property);
    }

    @Override
    public <T> T getProperty(PProperty<T> property) {
        return properties.getPropertyFor(property, this);
    }

    @Override
    public Map<String, String> getUncheckedProperties() {
        return properties.getUncheckedProperties();
    }

    // --- PDescriptor

    @Override
    public PType getType() {
        return type;
    }

    @Override
    public String getQualifiedName(String contextNamespace) {
        requireNonNull(contextNamespace, "contextNamespace == null");
        if (Objects.equals(namespace, contextNamespace)) {
            return typeName;
        }
        return fullName;
    }

    // --- Type ---

    @Override
    public String getTypeName() {
        return fullName;
    }

    // --- Object ---

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PDeclaredDescriptor that = (PDeclaredDescriptor) o;
        return namespace.equals(that.namespace) &&
               typeName.equals(that.typeName) &&
               type == that.type &&
               properties.equals(that.properties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(namespace, typeName, type, properties);
    }

    @Override
    public String toString() {
        return getTypeName();
    }
}
