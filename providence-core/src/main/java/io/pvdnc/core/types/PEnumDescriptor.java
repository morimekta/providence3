/*
 * Copyright 2016 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.types;

import io.pvdnc.core.PEnum;
import io.pvdnc.core.property.PPropertyHolder;
import io.pvdnc.core.property.PPropertyMap;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static net.morimekta.strings.EscapeUtil.javaEscape;

/**
 * The definition of a serializable enum.
 */
public abstract class PEnumDescriptor
        extends PDeclaredDescriptor implements PPropertyHolder {
    public PEnumDescriptor(String namespace,
                           String enumName,
                           PPropertyMap properties) {
        super(namespace, enumName, PType.ENUM, PDeclarationType.ENUM, properties);
    }

    /**
     * @return List of all values in declared order.
     */
    public abstract List<? extends PEnum> allValues();

    /**
     * @param id Value to look up enum from.
     * @return Enum if found, null otherwise.
     */
    public abstract PEnum findById(Integer id);

    /**
     * @param name Name to look up enum from.
     * @return Enum if found, null otherwise.
     */
    public abstract PEnum findByName(String name);

    /**
     * @param id Value to look up enum from.
     * @return The enum value.
     * @throws IllegalArgumentException If value not found.
     */
    public PEnum valueForId(int id) {
        PEnum value = findById(id);
        if (value == null) {
            throw new IllegalArgumentException("No " + getTypeName() + " value for id " + id);
        }
        return value;
    }

    /**
     * @param name Name to look up enum from.
     * @return The enum value.
     * @throws IllegalArgumentException If value not found.
     */
    public PEnum valueForName(String name) {
        PEnum value = findByName(name);
        if (value == null) {
            throw new IllegalArgumentException("No " + getTypeName() + " value for name '" + javaEscape(name) + "'");
        }
        return value;
    }

    // --- PDeclaredDescriptor ---

    @Override
    public abstract Optional<? extends PEnum> getDefaultValue();

    // --- Object ---

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!super.equals(o)) {
            return false;
        }
        PEnumDescriptor that = (PEnumDescriptor) o;
        return allValues().equals(that.allValues()) &&
               Objects.equals(getDefaultValue().orElse(null), that.getDefaultValue().orElse(null));
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), allValues(), getDefaultValue());
    }

    @Override
    public String toString() {
        return getTypeName() + "{" + allValues().stream().map(e -> e.getName() + "=" + e.getValue()).collect(
                Collectors.joining(",")) + "}";
    }

    // --- Static Utilities ---

    public static PEnumDescriptor getEnumDescriptor(Class<?> type) {
        if (PEnum.class.isAssignableFrom(type)) {
            if (PEnum.class.equals(type) ||
                PEnumValue.class.equals(type)) {
                throw new IllegalArgumentException("Base class of PEnum not a typed enum");
            }
            try {
                Field  field      = type.getDeclaredField(DESCRIPTOR_FIELD_NAME);
                Object descriptor = field.get(null);
                if (descriptor instanceof PEnumDescriptor) {
                    return (PEnumDescriptor) descriptor;
                }
                throw new IllegalArgumentException(
                        "Not an enum descriptor property: " + type.getSimpleName() + "." + DESCRIPTOR_FIELD_NAME);
            } catch (NoSuchFieldException | SecurityException | IllegalAccessException e) {
                throw new IllegalArgumentException("Not a typed enum: " + type.getSimpleName(), e);
            }
        }
        throw new IllegalArgumentException("not a typed enum: " + type.getSimpleName());
    }

}
