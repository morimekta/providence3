/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.types;

import io.pvdnc.core.property.PProperty;
import io.pvdnc.core.property.PPropertyHolder;
import io.pvdnc.core.property.PPropertyMap;

import java.util.Comparator;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

public class PEnumValue implements PPropertyHolder, Comparable<PEnumValue> {
    private final String       name;
    private final int          value;
    private final PPropertyMap valueProperties;

    private final Supplier<? extends PEnumDescriptor> descriptorSupplier;

    public PEnumValue(String name,
                      int value,
                      Supplier<? extends PEnumDescriptor> descriptorSupplier) {
        this(name, value, PPropertyMap.empty(), descriptorSupplier);
    }

    public PEnumValue(String name,
                      int value,
                      PPropertyMap valueProperties,
                      Supplier<? extends PEnumDescriptor> descriptorSupplier) {
        this.name = requireNonNull(name);
        this.value = value;
        this.valueProperties = requireNonNull(valueProperties);
        this.descriptorSupplier = requireNonNull(descriptorSupplier);
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public PEnumDescriptor descriptor() {
        return descriptorSupplier.get();
    }

    // --- PPropertyHolder ---

    @Override
    public boolean hasProperty(PProperty<?> property) {
        return valueProperties.hasProperty(property);
    }

    @Override
    public <T> T getProperty(PProperty<T> property) {
        return valueProperties.getPropertyFor(property, this);
    }

    @Override
    public Map<String, String> getUncheckedProperties() {
        return valueProperties.getUncheckedProperties();
    }

    // --- Comparable ---

    private static final Comparator<PEnumValue> COMPARATOR = Comparator.comparing(PEnumValue::getValue);

    @Override
    public int compareTo(PEnumValue o) {
        return COMPARATOR.compare(this, o);
    }

    // --- Object ---

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PEnumValue that = (PEnumValue) o;
        return value == that.value &&
               name.equals(that.name) &&
               valueProperties.equals(that.valueProperties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, value, valueProperties);
    }

    @Override
    public String toString() {
        return name + "{" + value + "}";
    }
}
