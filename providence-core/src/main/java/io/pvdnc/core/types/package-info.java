/**
 * This package contains descriptors and descriptor helpers used
 * to describe types in detail in providence, and utilities to
 * access these types.
 */
package io.pvdnc.core.types;