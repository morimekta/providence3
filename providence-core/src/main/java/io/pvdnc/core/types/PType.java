/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.types;

import io.pvdnc.core.PEnum;
import io.pvdnc.core.PMessage;
import io.pvdnc.core.PAny;
import net.morimekta.collect.UnmodifiableList;
import net.morimekta.collect.UnmodifiableMap;
import net.morimekta.collect.UnmodifiableSet;
import net.morimekta.collect.util.Binary;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;

/**
 * Enum with basic value types. The enum
 */
public enum PType implements PDescriptor {
    /**
     * Void type is used for non-value types, e.g. service and typedef, and
     * also for value-less fields that only has presence, e.g. success response
     * field on void method calls in thrift RPC.
     *
     * As the type is not really allowed to be declared all over, it is marked
     * as not simple.
     */
    VOID("void", Void.TYPE, Void.class, false, null),

    // --- Simple value types.

    /** Simple boolean value */
    BOOL("bool", Boolean.TYPE, Boolean.class, true, false),
    /** Simple byte value */
    BYTE("byte", Byte.TYPE, Byte.class, true, (byte) 0),
    /** Simple short value */
    SHORT("short", Short.TYPE, Short.class, true, (short) 0),
    /** Simple int value */
    INT("int", Integer.TYPE, Integer.class, true, 0),
    /** Simple long value */
    LONG("long", Long.TYPE, Long.class, true, 0L),
    /** Simple float value */
    FLOAT("float", Float.TYPE, Float.class, true, 0.0f),
    /** Simple double value */
    DOUBLE("double", Double.TYPE, Double.class, true, 0.0d),

    /** A UTF-8 string value */
    STRING("string", String.class, String.class, true, ""),
    /** A Binary value */
    BINARY("binary", Binary.class, Binary.class, true, Binary.empty()),
    /**
     * Hold any message. Is for some purposes same as MESSAGE, but must be handled
     * pretty differently regarding serialization and deserialization. Note that
     * each way of serialization may handle this pretty differently.
     */
    ANY("any", PAny.class, PAny.class, false, PAny.empty()),

    // --- All below here requires parametrization.

    /** A List of values */
    LIST("list", List.class, List.class, false, UnmodifiableList.listOf()),
    /** A Set of values */
    SET("set", Set.class, Set.class, false, UnmodifiableSet.setOf()),
    /** A Map of keys and values */
    MAP("map", Map.class, Map.class, false, UnmodifiableMap.mapOf()),

    /**
     * An enum with set allowed values. An enum must be explicitly
     * declared in order to be usable by other types.
     */
    ENUM("enum", PEnum.class, PEnum.class, false, null),
    /**
     * A message with defined fields with own values. A message must
     * be explicitly declared in order to be usable by other types.
     */
    MESSAGE("message", PMessage.class, PMessage.class, false, null),
    ;

    private final String                name;
    private final Class<?>              baseType;
    private final Class<?>              baseClass;
    private final Object                defaultValue;
    private final boolean               simple;
    private final Supplier<PDescriptor> provider;

    PType(String name, Class<?> baseType, Class<?> baseClass, boolean simple, Object defaultValue) {
        this.name = name;
        this.baseType = baseType;
        this.baseClass = baseClass;
        this.defaultValue = defaultValue;
        this.simple = simple;
        this.provider = () -> this;
    }

    public Class<?> getBaseType() {
        return baseType;
    }
    public Class<?> getBaseClass() {
        return baseClass;
    }

    public boolean isSimple() {
        return simple;
    }

    public boolean isOneOf(PType... alternatives) {
        for (PType alt : alternatives) {
            if (this == alt) return true;
        }
        return false;
    }

    public Supplier<? extends PDescriptor> provider() {
        if (ordinal() < LIST.ordinal()) {
            return provider;
        }
        throw new IllegalStateException("Not allowed to get default type provider for '" + toString() + "'");
    }

    // --- PDescriptor ---

    @Override
    public PType getType() {
        return this;
    }

    @Override
    public String getQualifiedName(String contextNamespace) {
        return name;
    }

    @Override
    public Optional<?> getDefaultValue() {
        return Optional.ofNullable(defaultValue);
    }

    // --- Type ---

    @Override
    public String getTypeName() {
        return name;
    }

    // --- Object ---

    @Override
    public String toString() {
        return name;
    }

    // --- Static Utilities ---

    public static PType forName(String name) {
        for (PType type : values()) {
            if (type.name.equals(name)) {
                return type;
            }
        }
        return VOID;
    }

    public static PType forClass(Class<?> toCheck) {
        for (PType type : values()) {
            if (type.baseClass.isAssignableFrom(toCheck) ||
                type.baseType.isAssignableFrom(toCheck)) {
                return type;
            }
        }
        return VOID;
    }
}
