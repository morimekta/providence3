/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.types;

import io.pvdnc.core.property.PProperty;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.registry.PNamespaceTypeRegistry;
import io.pvdnc.core.registry.PTypeReference;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static net.morimekta.collect.UnmodifiableList.toList;
import static net.morimekta.collect.UnmodifiableMap.asMap;
import static net.morimekta.collect.UnmodifiableMap.toMap;

public class PFileDescriptor implements PNamespaceTypeRegistry {
    private final String namespace;
    private final Map<String, PNamespaceTypeRegistry> includes;
    private final Map<String, PConstDescriptor> constants;
    private final Map<String, PDeclaredDescriptor> descriptors;
    private final PPropertyMap propertyMap;

    protected PFileDescriptor(String namespace,
                              PPropertyMap propertyMap,
                              Map<String, PNamespaceTypeRegistry> includes,
                              Collection<? extends PDeclaredDescriptor> descriptors) {
        this.propertyMap = propertyMap;
        this.namespace = namespace;
        this.includes = asMap(includes);
        this.constants = descriptors.stream()
                .filter(t -> t.getDeclarationType().equals(PDeclarationType.CONST))
                .map(t -> (PConstDescriptor) t)
                .collect(toMap(PConstDescriptor::getSimpleName));
        this.descriptors = descriptors.stream().collect(toMap(PDeclaredDescriptor::getSimpleName));
    }

    // ---- PPropertyHolder ----

    @Override
    public boolean hasProperty(PProperty<?> property) {
        return propertyMap.hasProperty(property);
    }

    @Override
    public <T> T getProperty(PProperty<T> property) {
        return propertyMap.getPropertyFor(property, this);
    }

    @Override
    public Map<String, String> getUncheckedProperties() {
        return propertyMap.getUncheckedProperties();
    }

    // ---- PNamespaceTypeRegistry ----

    @Override
    public String getNamespace() {
        return namespace;
    }

    @Override
    public Map<String, PNamespaceTypeRegistry> getIncludes() {
        return includes;
    }

    @Override
    public Optional<PNamespaceTypeRegistry> getInclude(String namespace) {
        return Optional.ofNullable(includes.get(namespace));
    }

    // ---- PTypeRegistry ----

    @Override
    public PDeclaredDescriptor descriptor(PTypeReference reference) {
        Objects.requireNonNull(reference, "reference == null");
        if (reference.isLocalType() || reference.getNamespace().equals(namespace)) {
            PDeclaredDescriptor descriptor = descriptors.get(reference.getName());
            if (descriptor == null) {
                throw new IllegalArgumentException("No such type " + reference.getName() + " in " + namespace);
            }
            return descriptor;
        }
        return getInclude(reference.getNamespace())
                .orElseThrow(() -> new IllegalArgumentException("No include for namespace: " + reference))
                .descriptor(reference.asLocalType());
    }

    @Override
    public Object constant(PTypeReference reference) {
        Objects.requireNonNull(reference, "reference == null");
        if (reference.isLocalType()) {
            return Optional.ofNullable(constants.get(reference.getName()))
                    .orElseThrow(() -> new IllegalArgumentException("No constant with name " + reference.getName() + " in " + namespace))
                    .getValue();
        }
        return getInclude(reference.getNamespace())
                .orElseThrow(() -> new IllegalArgumentException("No include for namespace " + reference.getNamespace() + " for const " + reference))
                .constant(reference.asLocalType());
    }

    @Override
    public Collection<PTypeReference> declaredTypeReferences() {
        return descriptors.values()
                .stream()
                .map(t -> PTypeReference.ref(t.getNamespace(), t.getSimpleName()))
                .collect(toList());
    }

    @Override
    public Collection<PTypeReference> declaredTypeReferences(PDeclarationType ofType) {
        Objects.requireNonNull(ofType, "ofType == null");
        return descriptors.values()
                .stream()
                .filter(t -> ofType.equals(t.getDeclarationType()))
                .map(t -> PTypeReference.ref(t.getNamespace(), t.getSimpleName()))
                .collect(toList());
    }

    @Override
    public Collection<PDeclaredDescriptor> declaredDescriptors() {
        return descriptors.values();
    }

    @Override
    public Collection<PDeclaredDescriptor> declaredDescriptors(PDeclarationType ofType) {
        Objects.requireNonNull(ofType, "ofType == null");
        return descriptors.values()
                .stream()
                .filter(t -> ofType.equals(t.getDeclarationType()))
                .collect(toList());
    }

    // ---- Object ----

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PFileDescriptor that = (PFileDescriptor) o;
        return namespace.equals(that.namespace) &&
                includes.equals(that.includes) &&
                descriptors.equals(that.descriptors);
    }

    @Override
    public int hashCode() {
        return Objects.hash(namespace, includes, descriptors);
    }
}
