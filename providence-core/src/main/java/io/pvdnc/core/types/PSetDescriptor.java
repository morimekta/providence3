/*
 * Copyright 2015-2016 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.types;

import net.morimekta.collect.SetBuilder;
import net.morimekta.collect.UnmodifiableSet;
import net.morimekta.collect.UnmodifiableSortedSet;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;


/**
 * Descriptor for a set with item type.
 */
public final class PSetDescriptor<Item> implements PDescriptor {
    private final Supplier<? extends PDescriptor> itemType;
    private final boolean               sorted;

    public PSetDescriptor(Supplier<? extends PDescriptor> itemType, boolean sorted) {
        this.itemType = itemType;
        this.sorted = sorted;
    }

    public PDescriptor getItemType() {
        return itemType.get();
    }

    public boolean isSorted() {
        return sorted;
    }

    @SuppressWarnings("unchecked")
    public SetBuilder<Item> builder(int capacity) {
        if (sorted) {
            return (SetBuilder<Item>) UnmodifiableSortedSet.newBuilderNaturalOrder(capacity);
        }
        return UnmodifiableSet.newBuilder(capacity);
    }

    // --- PDescriptor ---

    @Override
    public String getQualifiedName(String programContext) {
        return "set<" + itemType.get().getQualifiedName(programContext) + ">";
    }

    @Override
    public PType getType() {
        return PType.SET;
    }

    @Override
    public Optional<Set<Item>> getDefaultValue() {
        if (isSorted()) {
            return Optional.of(UnmodifiableSortedSet.sortedSetOf());
        } else {
            return Optional.of(UnmodifiableSet.setOf());
        }
    }

    // --- Type ---

    @Override
    public String getTypeName() {
        return "set<" + itemType.get().getTypeName() + ">";
    }

    // --- Object ---

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof PSetDescriptor)) {
            return false;
        }
        PSetDescriptor<?> other = (PSetDescriptor<?>) o;
        return sorted == other.sorted &&
               getItemType().equals(other.getItemType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(PSetDescriptor.class, sorted, getItemType());
    }

    @Override
    public String toString() {
        return getTypeName();
    }

    // --- Static Utilities ---

    public static <I> Supplier<PSetDescriptor<I>> provider(Supplier<? extends PDescriptor> itemType) {
        PSetDescriptor<I> set = new PSetDescriptor<>(itemType, false);
        return () -> set;
    }

    public static <I> Supplier<PSetDescriptor<I>> sortedProvider(Supplier<? extends PDescriptor> itemType) {
        PSetDescriptor<I> set = new PSetDescriptor<>(itemType, true);
        return () -> set;
    }
}
