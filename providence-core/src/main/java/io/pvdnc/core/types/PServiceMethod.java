/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.types;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.property.PProperty;
import io.pvdnc.core.property.PPropertyHolder;
import io.pvdnc.core.property.PPropertyMap;

import java.util.Map;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

public class PServiceMethod implements PPropertyHolder {
    private final String                                    name;
    private final Supplier<? extends PMessageDescriptor<?>> requestType;
    private final Supplier<? extends PMessageDescriptor<?>> responseType;
    private final PPropertyMap                              properties;
    private final Supplier<? extends PServiceDescriptor>    onServiceType;

    public PServiceMethod(String name,
                          PMessageDescriptor<?> responseType,
                          PMessageDescriptor<?> requestType,
                          PPropertyMap properties,
                          Supplier<? extends PServiceDescriptor> onServiceType) {
        this(name, () -> responseType, () -> requestType, properties, onServiceType);
        requireNonNull(requestType, "requestType == null");
        requireNonNull(responseType, "responseType == null");
    }

    public PServiceMethod(String name,
                          Supplier<? extends PMessageDescriptor<?>> responseType,
                          Supplier<? extends PMessageDescriptor<?>> requestType,
                          PPropertyMap properties,
                          Supplier<? extends PServiceDescriptor> onServiceType) {
        this.name = requireNonNull(name, "name == null");
        this.requestType = requireNonNull(requestType, "requestType == null");
        this.responseType = requireNonNull(responseType, "responseType == null");
        this.properties = requireNonNull(properties, "properties == null");
        this.onServiceType = requireNonNull(onServiceType, "onServiceType == null");
    }

    public String getName() {
        return name;
    }

    @SuppressWarnings("unchecked")
    public <M extends PMessage>
    PMessageDescriptor<M> getRequestType() {
        return (PMessageDescriptor<M>) requestType.get();
    }

    @SuppressWarnings("unchecked")
    public <M extends PMessage>
    PMessageDescriptor<M> getResponseType() {
        return (PMessageDescriptor<M>) responseType.get();
    }

    public PServiceDescriptor getOnServiceType() {
        return onServiceType.get();
    }

    // --- PPropertyHolder ---

    @Override
    public boolean hasProperty(PProperty<?> property) {
        return properties.hasProperty(property);
    }

    @Override
    public <T> T getProperty(PProperty<T> property) {
        return properties.getPropertyFor(property, this);
    }

    @Override
    public Map<String, String> getUncheckedProperties() {
        return properties.getUncheckedProperties();
    }
}
