/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.types;

import io.pvdnc.core.PService;
import io.pvdnc.core.property.PPropertyMap;
import net.morimekta.collect.UnmodifiableMap;
import net.morimekta.collect.util.LazyCachedSupplier;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import static net.morimekta.collect.UnmodifiableList.asList;
import static net.morimekta.collect.UnmodifiableList.toList;

public class PServiceDescriptor extends PDeclaredDescriptor {
    private final List<PServiceMethod>                  methods;
    private final Map<String, PServiceMethod>           methodByName;
    private final Supplier<List<PServiceDescriptor>>    lazyExtending;
    private final Supplier<Collection<PServiceMethod>>  allMethods;

    public PServiceDescriptor(String namespace,
                              String serviceName,
                              PPropertyMap properties,
                              List<Supplier<? extends PServiceDescriptor>> extending,
                              List<PServiceMethod> methods) {
        super(namespace, serviceName, PType.VOID, PDeclarationType.SERVICE, properties);
        List<Supplier<? extends PServiceDescriptor>> finalExtending = asList(extending);
        this.lazyExtending = LazyCachedSupplier.lazyCache(
                () -> finalExtending.stream().map(Supplier::get).collect(toList()));
        this.methods = asList(methods);
        this.methodByName = methods.stream().collect(UnmodifiableMap.toMap(PServiceMethod::getName));
        this.allMethods = LazyCachedSupplier.lazyCache(() -> {
            List<PServiceMethod> all = new ArrayList<>(declaredMethods());
            extending().forEach(s -> all.addAll(s.allMethods()));
            return all;
        });
    }

    public Collection<PServiceDescriptor> extending() {
        return lazyExtending.get();
    }

    public Collection<PServiceMethod> declaredMethods() {
        return methods;
    }

    public Collection<PServiceMethod> allMethods() {
        return allMethods.get();
    }

    public PServiceMethod findMethodForName(CharSequence methodName) {
        PServiceMethod method = methodByName.get(methodName.toString());
        if (method != null) return method;
        for (PServiceDescriptor ext : extending()) {
            method = ext.findMethodForName(methodName);
            if (method != null) return method;
        }
        return null;
    }

    @Override
    public Optional<PService> getDefaultValue() {
        return Optional.empty();
    }
}
