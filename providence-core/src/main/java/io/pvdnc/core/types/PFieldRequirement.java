/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.types;

import io.pvdnc.core.PMessage;

public enum PFieldRequirement {
    /**
     * This results in the default providence field value behavior when serializing.
     * The field is optional in both input and output.
     */
    OPTIONAL,
    /**
     * When serializing with thrift binary serializer, use the value from
     * {@link PMessage#get(int)} instead of from
     * {@link PMessage#optional(int)}. This means
     * inserting a default value into the serialized stream regardless if the
     * value is set or not.
     * <p>
     * When de-serializing, will behave as normal. This means a serialized, then
     * deserialized message may differ.
     * <p>
     * Note that this is the default for '.thrift' message fields. This means
     * that output fields as essentially non-nullable.
     */
    DEFAULT_OUT,
    /**
     * The field is required for serialization, but not for deserialization.
     * This is similar to {@link #DEFAULT_OUT}, but requires the field to be
     * set. If serializing with 'lenient' modes, this should behave as
     * {@link #DEFAULT_OUT}. If serializing in strict mode serializer should
     * throw exception if not set.
     */
    REQUIRED_OUT,
    /**
     * Adds to the behavior of {@link #REQUIRED_OUT}, with extra requirement when
     * de-serializing content in {@code strict} mode. When de-serializing and the
     * value of a required field not set from the stream, throw an exception. This
     * means that input fields are non-nullable.
     *
     * Will not change generated message class behavior.
     */
    REQUIRED,
    ;
}
