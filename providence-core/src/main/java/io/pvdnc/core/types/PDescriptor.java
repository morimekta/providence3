/*
 * Copyright 2016 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.types;

import java.lang.reflect.Type;
import java.util.Optional;

/**
 * Base class for the definition of anything in providence.
 */
public interface PDescriptor extends Type {
    /**
     * @return Get the described type.
     */
    PType getType();

    /**
     * @param contextNamespace The context namespace to get name for.
     * @return The qualified type name from context.
     */
    String getQualifiedName(String contextNamespace);

    /**
     * @return Optional default value for the type. If the type does not
     *         have an intrinsic default value, returns null.
     */
    Optional<?> getDefaultValue();
}
