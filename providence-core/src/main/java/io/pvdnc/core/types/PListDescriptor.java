/*
 * Copyright 2015-2016 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.types;

import net.morimekta.collect.ListBuilder;
import net.morimekta.collect.UnmodifiableList;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * Descriptor for a list with item type.
 */
public final class PListDescriptor<Item> implements PDescriptor {
    private final Supplier<? extends PDescriptor> itemType;

    public PListDescriptor(Supplier<? extends PDescriptor> itemType) {
        this.itemType = itemType;
    }

    public PDescriptor getItemType() {
        return itemType.get();
    }

    public ListBuilder<Item> builder(int capacity) {
        return UnmodifiableList.newBuilder(capacity);
    }

    // --- PDescriptor ---

    @Override
    public String getQualifiedName(String programContext) {
        return "list<" + itemType.get().getQualifiedName(programContext) + ">";
    }

    @Override
    public PType getType() {
        return PType.LIST;
    }

    @Override
    public Optional<List<Item>> getDefaultValue() {
        return Optional.of(UnmodifiableList.listOf());
    }

    // --- Type ---

    @Override
    public String getTypeName() {
        return "list<" + itemType.get().getTypeName() + ">";
    }

    // --- Object ---

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof PListDescriptor)) {
            return false;
        }
        PListDescriptor<?> other = (PListDescriptor<?>) o;
        return getItemType().equals(other.getItemType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(PListDescriptor.class, getItemType());
    }

    @Override
    public String toString() {
        return getTypeName();
    }

    // --- Static Utilities ---

    public static <I> Supplier<PListDescriptor<I>> provider(Supplier<? extends PDescriptor> itemDesc) {
        PListDescriptor<I> list = new PListDescriptor<>(itemDesc);
        return () -> list;
    }
}
