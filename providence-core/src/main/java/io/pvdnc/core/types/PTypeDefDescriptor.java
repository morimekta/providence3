/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.types;

import io.pvdnc.core.property.PPropertyMap;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

public class PTypeDefDescriptor extends PDeclaredDescriptor {
    private final Supplier<? extends PDescriptor> provider;
    public PTypeDefDescriptor(String namespace, String typeName,
                              PPropertyMap propertyMap,
                              Supplier<? extends PDescriptor> provider) {
        super(namespace, typeName, PType.VOID, PDeclarationType.TYPEDEF, propertyMap);
        this.provider = provider;
    }

    public PDescriptor getDescriptor() {
        return provider.get();
    }

    // --- PDescriptor

    @Override
    public Optional<?> getDefaultValue() {
        return getDescriptor().getDefaultValue();
    }

    // --- Object

    @Override
    public String toString() {
        return getTypeName() + '{' + provider.get() + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PTypeDefDescriptor that = (PTypeDefDescriptor) o;
        return provider.equals(that.provider);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), provider);
    }
}
