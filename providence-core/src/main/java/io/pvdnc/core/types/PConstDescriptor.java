/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.types;

import io.pvdnc.core.property.PPropertyMap;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

public class PConstDescriptor extends PDeclaredDescriptor {
    private final Supplier<? extends PDescriptor> descriptorSupplier;
    private final Supplier<Object>      valueSupplier;

    public PConstDescriptor(String namespace,
                            String typeName,
                            Supplier<? extends PDescriptor> descriptorSupplier,
                            Supplier<Object> valueSupplier) {
        this(namespace, typeName, descriptorSupplier, valueSupplier, PPropertyMap.empty());
    }

    public PConstDescriptor(String namespace,
                            String typeName,
                            Supplier<? extends PDescriptor> descriptorSupplier,
                            Supplier<Object> valueSupplier,
                            PPropertyMap properties) {
        super(namespace, typeName, PType.VOID, PDeclarationType.CONST, properties);
        this.descriptorSupplier = descriptorSupplier;
        this.valueSupplier = valueSupplier;
    }

    /**
     * @return The type descriptor of the const value.
     */
    public PDescriptor getDescriptor() {
        return descriptorSupplier.get();
    }

    /**
     * @return The constant value.
     */
    public Object getValue() {
        return valueSupplier.get();
    }

    // --- PDescriptor ---

    @Override
    public Optional<?> getDefaultValue() {
        return Optional.of(getValue());
    }

    // --- Object ---


    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (super.equals(o)) {
            PConstDescriptor that = (PConstDescriptor) o;
            return descriptorSupplier.get().equals(that.descriptorSupplier.get()) &&
                   valueSupplier.get().equals(that.valueSupplier.get());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), descriptorSupplier.get(), valueSupplier.get());
    }
}
