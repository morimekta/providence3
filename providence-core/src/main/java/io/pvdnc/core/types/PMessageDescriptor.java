/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.types;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.PMessageBuilder;
import io.pvdnc.core.property.PPropertyHolder;
import io.pvdnc.core.property.PPropertyMap;
import net.morimekta.collect.util.LazyCachedSupplier;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static java.util.Objects.requireNonNull;
import static net.morimekta.collect.UnmodifiableList.asList;
import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableList.toList;

/**
 * Descriptor for a message type. The message can be a normal message (struct), union, interface or exception.
 *
 * @param <M> The message interface type.
 */
public abstract class PMessageDescriptor<M extends PMessage>
        extends PDeclaredDescriptor
        implements PPropertyHolder {
    private final Supplier<Optional<PMessageDescriptor<?>>>   lazyExtending;
    private final Supplier<Collection<PMessageDescriptor<?>>> lazyImplementing;
    private final Supplier<M>                                 lazyDefault;

    public PMessageDescriptor(String namespace,
                              String typeName,
                              PPropertyMap properties) {
        this(namespace, typeName, null, listOf(), properties);
    }

    public PMessageDescriptor(String namespace,
                              String typeName,
                              Supplier<? extends PMessageDescriptor<?>> extending,
                              List<Supplier<? extends PMessageDescriptor<?>>> implementing,
                              PPropertyMap properties) {
        super(namespace, typeName, PType.MESSAGE, PDeclarationType.MESSAGE, properties);
        requireNonNull(implementing, "implementing == null");
        this.lazyExtending = LazyCachedSupplier.lazyCache(
                () -> extending == null ? Optional.empty() : Optional.of(extending.get()));
        List<Supplier<? extends PMessageDescriptor<?>>> finalImplementing = asList(implementing);
        this.lazyImplementing = LazyCachedSupplier.lazyCache(
                () -> finalImplementing.stream().map(Supplier::get).collect(toList()));
        this.lazyDefault = LazyCachedSupplier.lazyCache(() -> (M) newBuilder().build());
    }

    /**
     * Get list of declared fields on the message. This will only include those that was declared on the message type
     * itself, which can happen for extended message types.
     *
     * @return List of field declared on type itself.
     */
    public abstract List<PField> declaredFields();

    /**
     * Get list of fields in the message.
     *
     * @return The list of fields in declared order. If the message is extending another type, the extending fields
     * should come first.
     */
    public abstract List<PField> allFields();

    /**
     * Find field by ID, including searching through inherited message types.
     *
     * @param id The ID of the field.
     * @return The field or null if not found.
     */
    public abstract PField findFieldById(int id);

    /**
     * Get field by ID, including searching through inherited message types.
     *
     * @param id The id of the field.
     * @return The field.
     * @throws IllegalArgumentException If no such field.
     */
    public PField fieldForId(int id) {
        PField field = findFieldById(id);
        if (field == null) {
            throw new IllegalArgumentException("No field for id " + id + " in " + getTypeName());
        }
        return field;
    }

    /**
     * Find field by name, including searching through inherited message types.
     *
     * @param name The name of the field.
     * @return The field or null if not found.
     */
    public abstract PField findFieldByName(String name);

    /**
     * Get field by name, including searching through inherited message types.
     *
     * @param name The name of the field.
     * @return The field.
     * @throws IllegalArgumentException If no such field.
     */
    public PField fieldForName(String name) {
        PField field = findFieldByName(requireNonNull(name, "name == null"));
        if (field == null) {
            throw new IllegalArgumentException("No field for name '" + name + "' in " + getTypeName());
        }
        return field;
    }

    /**
     * @return A new message builder for the type.
     */
    public abstract PMessageBuilder newBuilder();

    /**
     * @return Optional message type this is extending.
     */
    public Optional<PMessageDescriptor<?>> getExtending() {
        return lazyExtending.get();
    }

    /**
     * @return List of message interfaces this message is implementing.
     */
    public Collection<PMessageDescriptor<?>> getImplementing() {
        return lazyImplementing.get();
    }

    /**
     * Check if this message type contains the diven other message descriptor's fields.
     *
     * @param descriptor The descriptor to check for containment.
     * @return If the provided descriptor is contains in this.
     */
    public boolean contains(PMessageDescriptor<?> descriptor) {
        if (this == descriptor) return true;
        return Stream.concat(getExtending().stream(), getImplementing().stream())
                     .anyMatch(ex -> ex.contains(descriptor));
    }

    // ---------------------------

    // --- PDeclaredDescriptor ---

    @Override
    public Optional<M> getDefaultValue() {
        return Optional.of(lazyDefault.get());
    }

    // --- PDescriptor ---

    // --- Object ---

    @Override
    public boolean equals(Object o) {
        if (!super.equals(o)) return false;
        PMessageDescriptor<?> that = (PMessageDescriptor<?>) o;
        return allFields().equals(that.allFields());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), allFields());
    }

    // --- Static Utilities ---

    public static PMessageDescriptor<?> getMessageDescriptor(Class<?> type) {
        if (PMessage.class.isAssignableFrom(type)) {
            if (PMessage.class.equals(type) ||
                PMessageBuilder.class.equals(type)) {
                throw new IllegalArgumentException("Base class of PMessage not a typed message");
            }
            try {
                Field field;
                try {
                    field = type.getDeclaredField(DESCRIPTOR_FIELD_NAME);
                } catch (NoSuchFieldException e) {
                    field = type.getField(DESCRIPTOR_FIELD_NAME);
                }
                if ((field.getModifiers() & (Modifier.STATIC | Modifier.PUBLIC)) != (Modifier.STATIC | Modifier.PUBLIC)) {
                    throw new IllegalArgumentException(
                            "Not a message descriptor property: " + type.getSimpleName() + "." + DESCRIPTOR_FIELD_NAME + ": not public static");
                }

                Object descriptor;
                if (!field.canAccess(null)) {
                    // TODO: This is needed to access message types in other modules. The message type
                    //       must be in an exported module at least.
                    field.setAccessible(true);
                    descriptor = field.get(null);
                    field.setAccessible(false);
                } else {
                    descriptor = field.get(null);
                }
                if (descriptor instanceof PMessageDescriptor) {
                    return (PMessageDescriptor<?>) descriptor;
                }
                throw new IllegalArgumentException(
                        "Not a message descriptor property: " + type.getSimpleName() + "." + DESCRIPTOR_FIELD_NAME);
            } catch (NoSuchFieldException | SecurityException | IllegalAccessException e) {
                throw new IllegalArgumentException("Not a typed message: " + type.getSimpleName(), e);
            }
        }
        throw new IllegalArgumentException("Not a typed message: " + type.getSimpleName());
    }
}
