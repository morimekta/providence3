/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.types;

import net.morimekta.collect.util.LazyCachedSupplier;
import io.pvdnc.core.property.PProperty;
import io.pvdnc.core.property.PPropertyHolder;
import io.pvdnc.core.property.PPropertyMap;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;
import static io.pvdnc.core.property.PDefaultProperties.FIELD_REQUIREMENT;
import static io.pvdnc.core.utils.ValueUtil.asString;

/**
 * Descriptor class for a single field.
 */
public class PField implements PPropertyHolder {
    private final int id;
    private final String name;
    private final PPropertyMap properties;
    private final PFieldRequirement requirement;

    private final Supplier<?> defaultValueSupplier;
    private final Supplier<? extends PDescriptor> declaredDescriptor;
    private final Supplier<? extends PMessageDescriptor<?>> onMessageSupplier;
    private final Supplier<? extends PMessageDescriptor<?>> argumentsType;
    private final Supplier<? extends PDescriptor> resolvedDescriptor;

    public PField(int id,
                  Supplier<? extends PDescriptor> descriptor,
                  String name,
                  Supplier<?> defaultValueSupplier,
                  PPropertyMap properties,
                  Supplier<? extends PMessageDescriptor<?>> argumentsType,
                  Supplier<? extends PMessageDescriptor<?>> onMessageSupplier) {
        this.id = id;
        this.properties = requireNonNull(properties, "properties == null");
        this.name = requireNonNull(name, "name == null");
        this.defaultValueSupplier = defaultValueSupplier;
        this.declaredDescriptor = requireNonNull(descriptor, "descriptor == null");
        this.onMessageSupplier = requireNonNull(onMessageSupplier, "onMessageSupplier == null");
        // Common Properties to avoid too heavy map checking.
        this.requirement = requireNonNull(properties.getPropertyFor(FIELD_REQUIREMENT, this));
        this.argumentsType = argumentsType;
        this.resolvedDescriptor = LazyCachedSupplier.lazyCache(() -> {
            PDescriptor d = declaredDescriptor.get();
            while (d instanceof PTypeDefDescriptor) {
                d = ((PTypeDefDescriptor) d).getDescriptor();
            }
            return d;
        });
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public PType getType() {
        return getResolvedDescriptor().getType();
    }

    public PFieldRequirement getRequirement() {
        return requirement;
    }

    public PDescriptor getResolvedDescriptor() {
        return requireNonNull(resolvedDescriptor.get(), "descriptor == null");
    }

    public PDescriptor getDeclaredDescriptor() {
        return requireNonNull(declaredDescriptor.get(), "descriptor == null");
    }

    public PMessageDescriptor<?> getOnMessageType() {
        return requireNonNull(onMessageSupplier.get(), "onMessageType == null");
    }

    public PMessageDescriptor<?> getArgumentsType() {
        return argumentsType == null ? null : argumentsType.get();
    }

    @SuppressWarnings("unchecked")
    public <T> Optional<T> getDefaultValue() {
        if (defaultValueSupplier == null) {
            return Optional.empty();
        }
        return (Optional<T>) Optional.of(requireNonNull(defaultValueSupplier.get(), "defaultValue == null"));
    }

    // --- PPropertyHolder ---

    @Override
    public boolean hasProperty(PProperty<?> property) {
        return properties.hasProperty(property);
    }

    @Override
    public <PV> PV getProperty(PProperty<PV> property) {
        return properties.getPropertyFor(property, this);
    }

    @Override
    public Map<String, String> getUncheckedProperties() {
        return properties.getUncheckedProperties();
    }

    // --- Object ---

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PField pField = (PField) o;
        return id == pField.id &&
               name.equals(pField.name) &&
               properties.equals(pField.properties) &&
               Objects.equals(getDefaultValue(), pField.getDefaultValue()) &&
               getDeclaredDescriptor().equals(pField.getDeclaredDescriptor());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id,
                            name,
                            properties,
                            getDefaultValue(),
                            getDeclaredDescriptor());
    }

    @Override
    public String toString() {
        return (id >= 0 ? id + ": " : "") + getDeclaredDescriptor().getTypeName() + " " + name +
               (defaultValueSupplier == null ? "" : " = " + asString(getDefaultValue()));
    }
}
