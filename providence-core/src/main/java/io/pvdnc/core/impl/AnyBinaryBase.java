/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.impl;

import io.pvdnc.core.PAny;
import io.pvdnc.core.types.PDeclaredDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import net.morimekta.collect.util.Binary;
import io.pvdnc.core.PMessage;
import io.pvdnc.core.io.PSerializer;
import io.pvdnc.core.io.PSerializerProvider;
import io.pvdnc.core.registry.PSimpleTypeRegistry;
import io.pvdnc.core.registry.PTypeReference;
import io.pvdnc.core.registry.PTypeRegistry;

import java.io.IOException;
import java.io.UncheckedIOException;

import static java.util.Objects.requireNonNull;

public interface AnyBinaryBase extends PAny {
    // ---- AnyBinary ----

    /**
     * The type name of the message being serialized.
     *
     * @return The type_name value or default.
     */
    String getTypeName();

    /**
     * The media type of the serialization.
     *
     * @return The media_type value or default.
     */
    String getMediaType();

    /**
     * The raw data of the serialized content;
     *
     * @return The data value or default.
     */
    net.morimekta.collect.util.Binary getData();

    /**
     * @return If the data field is set.
     */
    boolean hasData();

    // ---- PAny ----

    @Override
    default boolean isA(PMessageDescriptor<?> descriptor) {
        requireNonNull(descriptor, "descriptor == null");
        return descriptor.getTypeName().equals(getTypeName());
    }

    @Override
    default boolean isPresent() {
        try {
            return !getTypeName().isEmpty() && !getMediaType().isEmpty() && hasData();
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    default <M extends PMessage> M getMessage() {
        return (M) anyUnpack(getData(), anyDescriptor(), anySerializer());
    }

    @Override
    default AnyBinary pack(PSerializer serializer) {
        if (serializer.mediaTypes().contains(getMediaType())) {
            return (AnyBinary) this;
        }
        return PAny.pack(getMessage(), serializer);
    }

    @Override
    default AnyWrapper unpack() {
        return PAny.wrap(anyUnpack(getData(), anyDescriptor(), anySerializer()));
    }

    // ---- AnyBinaryBase ----

    /**
     * Get the binary any deserialized using the specified serializer. This can be
     * done if the serializer is not a registered serializer.
     *
     * @param serializer The serializer to unpack with.
     * @return The wrapped message.
     */
    default AnyWrapper unpack(PSerializer serializer) {
        return unpack(anyDescriptor(), serializer);
    }

    /**
     * Get the binary any deserialized using the specified serializer. This can be
     * done if the serializer is not a registered serializer.
     *
     * @param descriptor The message descriptor un unpack as.
     * @param serializer The serializer to unpack with.
     * @return The wrapped message.
     */
    default AnyWrapper unpack(PMessageDescriptor<?> descriptor, PSerializer serializer) {
        return PAny.wrap(anyUnpack(getData(), descriptor, serializer));
    }

    // ---- Private ----

    private PSerializer anySerializer() {
        if (getMediaType().isEmpty()) {
            throw new IllegalStateException("No media type for Any{" + getTypeName() + "{...}}");
        };
        return PSerializerProvider.getInstance().forMediaType(getMediaType())
                .orElseThrow(() -> new IllegalArgumentException("No serializer for " + getMediaType()));
    }

    private PMessageDescriptor<?> anyDescriptor() {
        return anyDescriptor(PSimpleTypeRegistry.getInstance());
    }

    private PMessageDescriptor<?> anyDescriptor(PTypeRegistry registry) {
        if (getTypeName().isEmpty()) {
            throw new IllegalStateException("No type name for Any{}");
        }
        PTypeReference reference = PTypeReference.parseGlobalRef(getTypeName());
        PDeclaredDescriptor descriptor = registry.descriptor(reference);
        if (descriptor instanceof PMessageDescriptor) {
            return (PMessageDescriptor<?>) descriptor;
        }
        throw new IllegalStateException("Not a message type: " + getTypeName());
    }

    private static PMessage anyUnpack(Binary data, PMessageDescriptor<?> descriptor, PSerializer serializer) {
        try {
            return serializer.readFrom(descriptor, data.getInputStream());
        } catch (IOException e) {
            throw new UncheckedIOException(e.getMessage(), e);
        }
    }
}
