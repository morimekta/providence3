/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.impl;

import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.registry.PTypeReference;

import static java.util.Objects.requireNonNull;
import static io.pvdnc.core.registry.PTypeReference.parseGlobalRef;

/**
 * Used to parse and format type names for serialized formats of
 * the Any struct. Can be passed to some serializers to modify how
 * it parses and formats type names.
 */
public interface AnyTypeNameFormat {
    PTypeReference toReference(String typeName);
    String toTypeName(PMessageDescriptor<?> descriptor);

    AnyTypeNameFormat DEFAULT = new AnyTypeNameFormat() {
        @Override
        public PTypeReference toReference(String typeName) {
            return parseGlobalRef(requireNonNull(typeName, "typeName == null")
                    .replaceAll("^\\\"(.*)\\\"$", "$1")
                    .replaceAll("^'(.*)'$", "$1")
                    .replaceAll(".*/", ""));
        }

        @Override
        public String toTypeName(PMessageDescriptor<?> descriptor) {
            return descriptor.getTypeName();
        }
    };
}
