/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.impl;

import io.pvdnc.core.PAny;
import io.pvdnc.core.PMessage;
import io.pvdnc.core.io.PSerializer;
import io.pvdnc.core.types.PMessageDescriptor;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

public class AnyWrapper implements PAny {
    public static final AnyWrapper EMPTY = new AnyWrapper();

    private final PMessage message;

    public AnyWrapper(PMessage message) {
        this.message = requireNonNull(message, "message == null");
    }

    private AnyWrapper() {
        this.message = null;
    }

    // ---- Any ----

    @Override
    @SuppressWarnings("unchecked")
    public <M extends PMessage> M getMessage() {
        return (M) requireNonNull(message, "message == null");
    }

    @Override
    public boolean isPresent() {
        return message != null;
    }

    @Override
    public boolean isA(PMessageDescriptor<?> descriptor) {
        requireNonNull(descriptor, "descriptor == null");
        return message != null && message.$descriptor().equals(descriptor);
    }

    @Override
    public AnyBinary pack(PSerializer serializer) {
        requireNonNull(serializer, "serializer == null");
        requireNonNull(message, "message == null");
        return PAny.pack(message, serializer);
    }

    @Override
    public AnyWrapper unpack() {
        return this;
    }

    // ---- Object ----

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnyWrapper that = (AnyWrapper) o;
        return Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message);
    }

    @Override
    public String toString() {
        return "Any{message=" + message + '}';
    }
}
