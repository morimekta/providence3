/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core;

import io.pvdnc.core.types.PField;
import net.morimekta.collect.UnmodifiableList;
import net.morimekta.collect.UnmodifiableMap;
import net.morimekta.collect.UnmodifiableSet;
import net.morimekta.collect.UnmodifiableSortedMap;
import net.morimekta.collect.UnmodifiableSortedSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import static java.util.Objects.requireNonNull;

/**
 * Builder interface for messages. The builder is a version
 * of the same message interface, but with mutable field and
 * associated setters.
 */
public interface PMessageBuilder extends PMessage {
    /**
     * Set the field value.
     *
     * @param field The field ID to set the value for.
     * @param value The value to be set.
     */
    void set(int field, Object value);

    /**
     * Get a mutable instance of the value that reflects back to the
     * builder itself. This will only differentiate from {@link #get(int)}
     * if there exist a mutable variant of the value itself. E.g.:
     * map, list, set and message.
     *
     * @param field The field to get mutable value for.
     * @param <T>   The value type.
     * @return The mutable value.
     */
    @SuppressWarnings("unchecked")
    default <T> T mutable(int field) {
        T value = get(field);
        if (value != null && !(value instanceof PMessageBuilder)) {
            if (value instanceof PMessage) {
                value = (T) ((PMessage) value).mutate();
            } else if (value instanceof UnmodifiableList) {
                value = (T) new ArrayList<>((List<?>) value);
            } else if (value instanceof UnmodifiableSortedSet) {
                value = (T) new TreeSet<>((Set<?>) value);
            } else if (value instanceof UnmodifiableSet) {
                value = (T) new HashSet<>((Set<?>) value);
            } else if (value instanceof UnmodifiableSortedMap) {
                value = (T) new TreeMap<>((Map<?, ?>) value);
            } else if (value instanceof UnmodifiableMap) {
                value = (T) new HashMap<>((Map<?, ?>) value);
            }
            set(field, value);
        }
        return value;
    }

    /**
     * Clear the field value.
     *
     * @param field The field ID to clear the value for.
     */
    void clear(int field);

    /**
     * @param field The field to check.
     * @return If the field has been modified on this builder instance.
     */
    boolean modified(int field);

    /**
     * @return Build the message instance.
     */
    PMessage build();

    /**
     * Merge values from the given message. The provided message can be any
     * supertype of the current type descriptor.
     *
     * @param message The message to merge values from.
     * @throws IllegalArgumentException If the provided message is not a supertype.
     */
    default void mergeFrom(PMessage message) {
        requireNonNull(message, "message == null");
        if (!$descriptor().contains(message.$descriptor())) {
            throw new IllegalArgumentException(
                    message.$descriptor().getTypeName() + " is not a supertype of " + $descriptor().getTypeName());
        }
        for (PField field : message.$descriptor().allFields()) {
            message.optional(field.getId()).ifPresent(value -> set(field.getId(), value));
        }
    }
}
