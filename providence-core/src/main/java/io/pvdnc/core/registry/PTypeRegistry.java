/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.registry;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.types.PDeclarationType;
import io.pvdnc.core.types.PDeclaredDescriptor;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PType;
import io.pvdnc.core.types.PTypeDefDescriptor;

import java.util.Collection;

public interface PTypeRegistry {
    /**
     * Get the declared descriptor for the given reference. This will point at
     * the declared type as found, not the resulting actual type in case of
     * type definitions.
     *
     * @param reference The reference to resolve.
     * @return The descriptor.
     * @throws IllegalArgumentException If unable to find descriptor, or
     *         if the reference does not point to a descriptor.
     */
    PDeclaredDescriptor descriptor(PTypeReference reference);

    /**
     * Get a constant value for the given reference.
     *
     * @param reference The reference to resolve.
     * @return The constant value.
     * @throws IllegalArgumentException If the constant does not exist, or
     *         if the reference does not point to a constant.
     */
    Object constant(PTypeReference reference);

    /**
     * @return Collection of declared type references on this registry.
     */
    Collection<PTypeReference> declaredTypeReferences();

    /**
     * @param ofType Type of declarations to get from the registry.
     * @return Collection of requested declared type references on this registry.
     */
    Collection<PTypeReference> declaredTypeReferences(PDeclarationType ofType);

    /**
     * @return Collection of all declared type descriptors on this registry.
     */
    Collection<PDeclaredDescriptor> declaredDescriptors();

    /**
     * @param ofType Type of declarations to get from the registry.
     * @return Collection of requested declared type descriptors on this registry.
     */
    Collection<PDeclaredDescriptor> declaredDescriptors(PDeclarationType ofType);


    // --- Convenience

    /**
     * Resolve type down to its resulting type. This may be a non-declared type
     * in of itself, but defined though a type definition.
     *
     * @param reference The reference to resolve.
     * @return The resolved type.
     * @throws IllegalArgumentException If unable to find descriptor, or
     *         if the reference does not point to a descriptor.
     */
    default PDescriptor resolve(PTypeReference reference) {
        PDescriptor descriptor = descriptor(reference);
        while (descriptor instanceof PTypeDefDescriptor) {
            descriptor = ((PTypeDefDescriptor) descriptor).getDescriptor();
        }
        return descriptor;
    }

    /**
     * Get a descriptor from a type reference, requiring it to be of a
     * message type.
     *
     * @param reference The type reference to get descriptor for.
     * @param <M> The message type.
     * @return The message descriptor.
     * @throws IllegalArgumentException if not a message type, or unknown type.
     */
    @SuppressWarnings("unchecked")
    default <M extends PMessage> PMessageDescriptor<M> requireMessageDescriptor(PTypeReference reference) {
        PDescriptor descriptor = resolve(reference);
        if (descriptor.getType() != PType.MESSAGE) {
            throw new IllegalArgumentException("Not a message type: " + reference + " ( was " + descriptor.getType() + ")");
        }
        return (PMessageDescriptor<M>) descriptor;
    }

    /**
     * Get a descriptor from a type reference, requiring it to be of an
     * enum type.
     *
     * @param reference The type reference to get descriptor for.
     * @return The enum descriptor.
     * @throws IllegalArgumentException if not an enum type, or unknown type.
     */
    default PEnumDescriptor requireEnumDescriptor(PTypeReference reference) {
        PDescriptor descriptor = resolve(reference);
        if (descriptor.getType() != PType.ENUM) {
            throw new IllegalArgumentException("Not an enum type: " + reference + " ( was " + descriptor.getType() + ")");
        }
        return (PEnumDescriptor) descriptor;
    }
}
