/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.registry;

import java.lang.reflect.Type;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 *
 */
public class PTypeReference implements Comparable<PTypeReference>, Type {
    public static final String LOCAL_NAMESPACE = "";

    private final String namespace;
    private final String name;
    private final String typeName;

    protected PTypeReference(String namespace,
                             String name) {
        this.namespace = requireNonNull(namespace);
        this.name = requireNonNull(name);
        if (isLocalType()) {
            this.typeName = name;
        } else {
            this.typeName = namespace + "." + name;
        }
    }

    public String getNamespace() {
        return namespace;
    }

    public String getName() {
        return name;
    }

    public boolean isLocalType() {
        return LOCAL_NAMESPACE.equals(namespace);
    }

    public PTypeReference asLocalType() {
        return new PTypeReference(LOCAL_NAMESPACE, name);
    }

    // --- Type

    @Override
    public String getTypeName() {
        return typeName;
    }

    // --- Comparable

    @Override
    public int compareTo(PTypeReference o) {
        return typeName.compareTo(o.typeName);
    }

    // --- Object

    @Override
    public String toString() {
        return typeName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof PTypeReference)) return false;
        PTypeReference other = (PTypeReference) obj;
        return Objects.equals(namespace, other.namespace) &&
               Objects.equals(name, other.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getClass(), namespace, name);
    }

    // --- Static Utilities ---

    /**
     * Create a simple local type reference. This can only contain simple (program, type)
     * references. The type name can contain '.', but only for service request and response
     * types.
     *
     * @param typeName The type name.
     * @return The type reference.
     */
    public static PTypeReference ref(String typeName) {
        return new PTypeReference(LOCAL_NAMESPACE, typeName);
    }

    /**
     * Create a simple type reference. This can only contain simple (program, type)
     * references. The type name can contain '.', but only for service request and response
     * types.
     *
     * @param namespace The local program context.
     * @param typeName The type name.
     * @return The type reference.
     */
    public static PTypeReference ref(String namespace, String typeName) {
        return new PTypeReference(namespace, typeName);
    }

    /**
     * Parse a global reference to a type. Most likely to be used when looking up a type
     * in a simple type registry.
     *
     * @param typeName The type name to parse.
     * @return The parsed type reference.
     */
    public static PTypeReference parseGlobalRef(String typeName) {
        String namespace = typeName.replaceAll("\\.[^.]*", "");
        String name = typeName.replaceAll(".*\\.", "");
        return new PTypeReference(namespace, name);
    }
}
