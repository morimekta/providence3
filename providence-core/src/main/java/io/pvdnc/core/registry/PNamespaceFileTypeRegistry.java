/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.registry;

import io.pvdnc.core.types.PConstDescriptor;
import io.pvdnc.core.types.PDeclarationType;
import io.pvdnc.core.types.PDeclaredDescriptor;
import io.pvdnc.core.property.PProperty;
import io.pvdnc.core.property.PPropertyMap;

import java.nio.file.Path;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static java.util.Objects.requireNonNull;
import static net.morimekta.collect.UnmodifiableList.asList;
import static net.morimekta.collect.UnmodifiableList.toList;
import static net.morimekta.collect.UnmodifiableMap.asMap;
import static net.morimekta.collect.UnmodifiableSet.toSet;
import static net.morimekta.file.PathUtil.getFileName;
import static io.pvdnc.core.registry.PTypeReference.ref;

/**
 * Type registry for a single namespace with include reference to other registries based
 * on namespace. The type registry has an 'intrinsic' namespace, which must be non-empty.
 * That namespace should be reflected back onto the types registered here.
 */
public class PNamespaceFileTypeRegistry implements
        PNamespaceTypeRegistry,
        PWritableTypeRegistry {
    private final Path                             path;
    private final String                           namespace;
    private final Map<String, PNamespaceTypeRegistry> includes;
    private final Map<String, PDeclaredDescriptor> descriptorMap;
    private final AtomicReference<PPropertyMap>    properties;

    private boolean building;

    /**
     * Create a new un-finished namespace type registry. See {@link #isBuilding()} and
     * {@link #complete()} for namespace 'completeness'.
     *
     * @param path The IDL file path represented by the type registry.
     * @param namespace The intrinsic namespace of the registry.
     */
    public PNamespaceFileTypeRegistry(Path path, String namespace) {
        this.path = requireNonNull(path, "path == null");
        this.namespace = requireNonNull(namespace, "namespace == null");
        this.includes = new LinkedHashMap<>();
        this.descriptorMap = new LinkedHashMap<>();
        this.properties = new AtomicReference<>(PPropertyMap.empty());
        this.building = true;
    }

    /**
     * Check if the registry is in the building phase.
     * <p>
     * This is used to prevent circular dependencies. Meaning if when resolving
     * a type dependency a registry returning {@code true} from this method is
     * acquired, it means a circular dependency is happening.
     * <p>
     * Also see {@link #complete()}, which must be called when the parser is
     * done populating the registry.
     *
     * @return If the registry is in the building phase.
     */
    public boolean isBuilding() {
        return building;
    }

    /**
     * Mark the registry as complete. See {@link #isBuilding()} for details.
     */
    public void complete() {
        building = false;
    }

    /**
     * @return The file path represented by this type registry.
     */
    public Path getPath() {
        return path;
    }

    /**
     * Get the intrinsic namespace of the type registry. This is used for finding
     * types in {@code providence} IDL files, and in config files. It must be non-
     * empty for all providence declared types.
     *
     * @return The intrinsic namespace of the registry.
     */
    @Override
    public String getNamespace() {
        return namespace;
    }

    /**
     *
     * @param namespace Namespace of include.
     * @return Registry for included namespace.
     */
    @Override
    public Optional<PNamespaceTypeRegistry> getInclude(String namespace) {
        requireNonNull(namespace, "namespace == null");
        return Optional.ofNullable(includes.get(namespace));
    }

    /**
     * Put a register as an include from this registry. It can be used
     * for type lookup using it's namespace. The namespace of the include
     * does not need to match the included registry.
     *
     * @param namespace The namespace to put registry on.
     * @param registry The registry to put.
     */
    public void putInclude(String namespace, PNamespaceTypeRegistry registry) {
        requireNonNull(namespace, "namespace == null");
        requireNonNull(registry, "registry == null");
        if (registry == this) {
            throw new IllegalArgumentException("Not allowed to register to self");
        }
        includes.put(namespace, registry);
    }

    /**
     * @return A map of all includes. Namespace to registry.
     */
    @Override
    public Map<String, PNamespaceTypeRegistry> getIncludes() {
        return asMap(includes);
    }

    /**
     * Set properties on the namespace registry.
     *
     * @param properties The properties to set.
     */
    public void setProperties(PPropertyMap properties) {
        this.properties.set(properties);
    }

    // --- PWritableTypeRegistry ---

    @Override
    public void register(PDeclaredDescriptor descriptor) {
        requireNonNull(descriptor, "descriptor == null");
        descriptorMap.put(descriptor.getSimpleName(), descriptor);
    }

    // --- PTypeRegistry ---

    @Override
    public PDeclaredDescriptor descriptor(PTypeReference reference) {
        requireNonNull(reference, "reference == null");
        if (reference.isLocalType()) {
            return Optional.ofNullable(descriptorMap.get(reference.getName()))
                           .orElseThrow(() -> new IllegalArgumentException(
                                   "No such type '" + reference + "' in " + getFileName(path)));
        } else {
            if (includes.containsKey(reference.getNamespace())) {
                return getInclude(reference.getNamespace())
                        .orElseThrow()
                        .descriptor(reference.asLocalType());
            } else {
                throw new IllegalArgumentException(
                        "No such include '" + reference.getNamespace() + "' in " + getFileName(path));
            }
        }
    }

    @Override
    public Object constant(PTypeReference reference) {
        requireNonNull(reference, "reference == null");
        if (reference.isLocalType()) {
            if (descriptorMap.containsKey(reference.getName())) {
                PDeclaredDescriptor descriptor = descriptorMap.get(reference.getName());
                if (!(descriptor instanceof PConstDescriptor)) {
                    throw new IllegalArgumentException(
                            "The " + descriptor.getDeclarationType().name().toLowerCase(Locale.US) + " '" +
                            reference + "' is not a const in " + getFileName(path));
                }
                return ((PConstDescriptor) descriptor).getValue();
            } else {
                throw new IllegalArgumentException(
                        "No such const '" + reference + "' in " + getFileName(path));
            }
        } else {
            if (includes.containsKey(reference.getNamespace())) {
                return includes.get(reference.getNamespace()).constant(reference.asLocalType());
            } else {
                throw new IllegalArgumentException(
                        "No such include '" + reference.getNamespace() + "' in " + getFileName(path));
            }
        }
    }

    @Override
    public Collection<PTypeReference> declaredTypeReferences() {
        return descriptorMap.keySet()
                            .stream()
                            .map(PTypeReference::ref)
                            .collect(toSet());
    }

    @Override
    public Collection<PTypeReference> declaredTypeReferences(PDeclarationType ofType) {
        return descriptorMap.entrySet()
                            .stream()
                            .filter(e -> e.getValue().getDeclarationType() == ofType)
                            .map(e -> PTypeReference.ref(e.getKey()))
                            .collect(toSet());
    }

    @Override
    public Collection<PDeclaredDescriptor> declaredDescriptors() {
        return asList(descriptorMap.values());
    }

    @Override
    public Collection<PDeclaredDescriptor> declaredDescriptors(PDeclarationType ofType) {
        return descriptorMap.values()
                            .stream()
                            .filter(e -> e.getDeclarationType() == ofType)
                            .collect(toList());
    }

    // --- PPropertyHolder

    @Override
    public boolean hasProperty(PProperty<?> property) {
        return properties.get().hasProperty(property);
    }

    @Override
    public <T> T getProperty(PProperty<T> property) {
        return properties.get().getPropertyFor(property, this);
    }

    @Override
    public Map<String, String> getUncheckedProperties() {
        return properties.get().getUncheckedProperties();
    }

    // --- Object

    @Override
    public String toString() {
        return "PNamespaceTypeRegistry{" +
               "path=" + path +
               ", namespace='" + namespace + '\'' +
               '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PNamespaceFileTypeRegistry that = (PNamespaceFileTypeRegistry) o;
        return path.equals(that.path) &&
               namespace.equals(that.namespace) &&
               includes.keySet().equals(that.includes.keySet()) &&
               descriptorMap.keySet().equals(that.descriptorMap.keySet()) &&
               properties.get().equals(that.properties.get());
    }

    @Override
    public int hashCode() {
        return Objects.hash(path, namespace, includes, descriptorMap, properties);
    }
}
