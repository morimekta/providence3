/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.registry;

import io.pvdnc.core.types.PConstDescriptor;
import io.pvdnc.core.types.PDeclarationType;
import io.pvdnc.core.types.PDeclaredDescriptor;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PServiceDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.types.PTypeDefDescriptor;
import net.morimekta.collect.UnmodifiableList;
import net.morimekta.collect.UnmodifiableSet;
import net.morimekta.collect.util.LazyCachedSupplier;

import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import static net.morimekta.collect.UnmodifiableList.asList;
import static net.morimekta.collect.UnmodifiableSet.asSet;

/**
 * Simple type registry that requires namespace for all types. This will behave as
 * the {@link PGlobalFileTypeRegistry}, but does not need to manage files and namespaces
 * individually.
 */
public class PSimpleTypeRegistry implements PWritableTypeRegistry {
    private static final LazyCachedSupplier<PSimpleTypeRegistry> INSTANCE = LazyCachedSupplier.lazyCache(PSimpleTypeRegistry::new);
    public static PSimpleTypeRegistry getInstance() {
        return INSTANCE.get();
    }

    private final Set<String>                              namespaces;
    private final Map<PTypeReference, PDeclaredDescriptor> typeMap;

    public PSimpleTypeRegistry() {
        namespaces = new TreeSet<>();
        typeMap = new TreeMap<>();
    }

    public void registerRecursively(PDeclaredDescriptor descriptor) {
        registerRecursivelyInternal(descriptor);
    }

    public void registerRecursively(PTypeRegistry fileDescriptor) {
        for (PDeclaredDescriptor descriptor : fileDescriptor.declaredDescriptors()) {
            registerRecursively(descriptor);
        }
    }

    @Override
    public void register(PDeclaredDescriptor descriptor) {
        maybeRegister(descriptor);
    }

    @Override
    public PDeclaredDescriptor descriptor(PTypeReference reference) {
        if (reference.isLocalType()) {
            throw new IllegalArgumentException("Requesting local type " + reference + " in simple registry");
        }
        if (!namespaces.contains(reference.getNamespace())) {
            throw new IllegalArgumentException("Unknown namespace for type: " + reference);
        }
        if (typeMap.containsKey(reference)) {
            return typeMap.get(reference);
        }
        throw new IllegalArgumentException("No such type: " + reference);
    }

    public Object constant(PTypeReference reference) {
        if (reference.isLocalType()) {
            throw new IllegalArgumentException("Requesting local const " + reference + " in simple registry");
        }
        if (!namespaces.contains(reference.getNamespace())) {
            throw new IllegalArgumentException("Unknown namespace for const: " + reference);
        }
        PDeclaredDescriptor descriptor = typeMap.get(reference);
        if (descriptor == null) {
            throw new IllegalArgumentException("No such const: " + reference);
        }
        if (descriptor instanceof PConstDescriptor) {
            return ((PConstDescriptor) descriptor).getValue();
        }
        throw new IllegalArgumentException("The " + descriptor.getDeclarationType().name().toLowerCase(Locale.US) + " " + reference + " is not a constant.");
    }


    @Override
    public Collection<PTypeReference> declaredTypeReferences() {
        return asSet(typeMap.keySet());
    }

    @Override
    public Collection<PTypeReference> declaredTypeReferences(PDeclarationType ofType) {
        return typeMap.entrySet()
                      .stream()
                      .filter(e -> e.getValue().getDeclarationType() == ofType)
                      .map(Map.Entry::getKey)
                      .collect(UnmodifiableSet.toSet());
    }

    @Override
    public Collection<PDeclaredDescriptor> declaredDescriptors() {
        return asList(typeMap.values());
    }

    @Override
    public Collection<PDeclaredDescriptor> declaredDescriptors(PDeclarationType ofType) {
        return typeMap.values()
                      .stream()
                      .filter(e -> e.getDeclarationType() == ofType)
                      .collect(UnmodifiableList.toList());
    }

    // --- Private ---

    private void registerRecursivelyInternal(PDescriptor descriptor) {
        if (descriptor instanceof PDeclaredDescriptor) {
            if (!maybeRegister((PDeclaredDescriptor) descriptor)) {
                // already registered, nothing more to do.
                return;
            }
            switch (((PDeclaredDescriptor) descriptor).getDeclarationType()) {
                case TYPEDEF:
                    registerRecursivelyInternal(((PTypeDefDescriptor) descriptor).getDescriptor());
                    break;
                case CONST:
                    registerRecursivelyInternal(((PConstDescriptor) descriptor).getDescriptor());
                    break;
                case ENUM:
                    break;
                case MESSAGE:
                    for (PField field : ((PMessageDescriptor<?>) descriptor).allFields()) {
                        registerRecursivelyInternal(field.getDeclaredDescriptor());
                    }
                    ((PMessageDescriptor<?>) descriptor).getExtending().ifPresent(this::registerRecursivelyInternal);
                    ((PMessageDescriptor<?>) descriptor).getImplementing().forEach(this::registerRecursivelyInternal);
                    break;
                case SERVICE:
                    ((PServiceDescriptor) descriptor).declaredMethods().forEach(method -> {
                        registerRecursivelyInternal(method.getRequestType());
                        registerRecursivelyInternal(method.getResponseType());
                    });
                    break;
            }
        } else if (descriptor instanceof PListDescriptor) {
            registerRecursivelyInternal(((PListDescriptor<?>) descriptor).getItemType());
        } else if (descriptor instanceof PSetDescriptor) {
            registerRecursivelyInternal(((PSetDescriptor<?>) descriptor).getItemType());;
        } else if (descriptor instanceof PMapDescriptor) {
            registerRecursivelyInternal(((PMapDescriptor<?,?>) descriptor).getKeyType());
            registerRecursivelyInternal(((PMapDescriptor<?,?>) descriptor).getValueType());
        }
    }

    private boolean maybeRegister(PDeclaredDescriptor descriptor) {
        PTypeReference reference = PTypeReference.ref(descriptor.getNamespace(), descriptor.getSimpleName());
        if (typeMap.containsKey(reference)) return false;
        namespaces.add(descriptor.getNamespace());
        typeMap.put(reference, descriptor);
        return true;
    }
}
