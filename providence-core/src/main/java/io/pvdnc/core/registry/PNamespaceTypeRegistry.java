/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.registry;

import io.pvdnc.core.property.PPropertyHolder;

import java.util.Map;
import java.util.Optional;

/**
 * A type registry that represents a specific globally recognised namespace.
 * The registry itself may then have a number of includes, each to a namespace
 * registry, where the included-as namespace and recognized namespace of the
 * include does not have to match.
 */
public interface PNamespaceTypeRegistry extends PTypeRegistry, PPropertyHolder {
    /**
     * Get the globally recognized namespace for this registry.
     *
     * @return The global namespace for registry.
     */
    String getNamespace();

    /**
     * Get a map of all the includes, locally recognized namespace to actual registry.
     * Note that the namespace used in the include here does NOT need to reflect the
     * globally recognized namespace of the included registry.
     *
     * @return The included registry map.
     */
    Map<String, PNamespaceTypeRegistry> getIncludes();

    /**
     * Get include of the given locally recognized.
     *
     * @param namespace The included namespace to get registry for.
     * @return Optional included registry. Empty if no such include.
     */
    Optional<PNamespaceTypeRegistry> getInclude(String namespace);
}
