/**
 * This package contains type registry and variants of it used
 * around providence.
 */
package io.pvdnc.core.registry;