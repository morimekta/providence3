/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.core.registry;

import io.pvdnc.core.types.PDeclarationType;
import io.pvdnc.core.types.PDeclaredDescriptor;
import io.pvdnc.core.types.PDescriptor;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static net.morimekta.collect.UnmodifiableList.toList;
import static net.morimekta.collect.UnmodifiableSet.toSet;
import static io.pvdnc.core.registry.PTypeReference.ref;

/**
 * A collection of namespace registries used when parsing IDL and generating code. The
 * global type registry does not contain any types itself, but generally behaves as the
 * {@link PSimpleTypeRegistry}, requiring namespace on requested type references.
 */
public class PGlobalFileTypeRegistry implements PTypeRegistry {
    private final Map<Path, PNamespaceFileTypeRegistry>         typeRegistryMap;
    private final Map<String, List<PNamespaceFileTypeRegistry>> namespaceToRegistry;

    /**
     * Create a global type registry instance.
     */
    public PGlobalFileTypeRegistry() {
        this.typeRegistryMap = new LinkedHashMap<>();
        this.namespaceToRegistry = new LinkedHashMap<>();
    }

    /**
     * @param path The file path to get registry for.
     * @return Type registry that is registered on the file path. If no registry
     *         is registered on that path, returns empty.
     */
    public Optional<PNamespaceFileTypeRegistry> getRegistryForPath(Path path) {
        return Optional.ofNullable(typeRegistryMap.get(path));
    }

    /**
     * Get a list of type registries with the given namespace. Since namespaces
     * can be declared multiple times and places, this returns a list of all
     * type registries for the requested namespace.
     *
     * @param namespace The namespace string to get registries for.
     * @return List of registries.
     */
    public List<PNamespaceFileTypeRegistry> getRegistriesForNamespace(String namespace) {
        return namespaceToRegistry.getOrDefault(namespace, List.of());
    }

    /**
     * Get a registry for the file path. If it does not exist, it will be created with
     * the requested namespace. Note that if the file registry exists, the namespace will
     * not change even if not matching.
     *
     * @param path The file path to get registry for.
     * @param namespace The namespace to use for a new registry.
     * @return The type registry.
     */
    public PNamespaceFileTypeRegistry computeRegistryForPath(Path path, String namespace) {
        return typeRegistryMap.computeIfAbsent(path, (p) -> {
            PNamespaceFileTypeRegistry registry = new PNamespaceFileTypeRegistry(p, namespace);
            namespaceToRegistry.computeIfAbsent(namespace, n -> new ArrayList<>()).add(registry);
            return registry;
        });
    }

    // --- PTypeRegistry

    @Override
    public PDeclaredDescriptor descriptor(PTypeReference reference) {
        if (reference.isLocalType()) {
            throw new IllegalArgumentException("Requesting local type " + reference + " in global registry");
        }
        List<PNamespaceFileTypeRegistry> registries = getRegistriesForNamespace(reference.getNamespace());
        if (registries.isEmpty()) {
            throw new IllegalArgumentException("Unknown namespace '" + reference.getNamespace() + "' for type " + reference);
        }
        for (PNamespaceFileTypeRegistry registry : registries) {
            try {
                return registry.descriptor(reference.asLocalType());
            } catch (IllegalArgumentException e) {
                // ignore.
            }
        }
        throw new IllegalArgumentException("No type " + reference.getName() + " in namespace " + reference.getNamespace());
    }

    @Override
    public Object constant(PTypeReference reference) {
        if (reference.isLocalType()) {
            throw new IllegalArgumentException("Requesting local const " + reference + " in global registry");
        }
        List<PNamespaceFileTypeRegistry> registries = getRegistriesForNamespace(reference.getNamespace());
        if (registries.isEmpty()) {
            throw new IllegalArgumentException("Unknown namespace '" + reference.getNamespace() + "' for const " + reference);
        }
        for (PNamespaceFileTypeRegistry registry : registries) {
            try {
                return registry.constant(reference.asLocalType());
            } catch (IllegalArgumentException e) {
                // ignore.
            }
        }
        throw new IllegalArgumentException("No const " + reference.getName() + " in namespace " + reference.getNamespace());
    }

    @Override
    public Collection<PTypeReference> declaredTypeReferences() {
        return typeRegistryMap.values()
                              .stream()
                              .flatMap(registry -> registry.declaredTypeReferences()
                                                           .stream()
                                                           .map(ref -> ref(registry.getNamespace(), ref.getName())))
                              .collect(toSet());
    }

    @Override
    public Collection<PTypeReference> declaredTypeReferences(PDeclarationType ofType) {
        return typeRegistryMap.values()
                              .stream()
                              .flatMap(registry -> registry.declaredTypeReferences(ofType)
                                                           .stream()
                                                           .map(ref -> ref(registry.getNamespace(), ref.getName())))
                              .collect(toSet());
    }

    @Override
    public Collection<PDeclaredDescriptor> declaredDescriptors() {
        return typeRegistryMap.values()
                              .stream()
                              .flatMap(registry -> registry.declaredDescriptors().stream())
                              .collect(toList());
    }

    @Override
    public Collection<PDeclaredDescriptor> declaredDescriptors(PDeclarationType ofType) {
        return declaredDescriptors()
                .stream()
                .filter(d -> d.getDeclarationType() == ofType)
                .collect(toList());
    }

    @Override
    public PDescriptor resolve(PTypeReference reference) {
        if (reference.isLocalType()) {
            throw new IllegalArgumentException("Requesting local type " + reference + " in global registry");
        }
        List<PNamespaceFileTypeRegistry> registries = getRegistriesForNamespace(reference.getNamespace());
        if (registries.isEmpty()) {
            throw new IllegalArgumentException("Unknown namespace '" + reference.getNamespace() + "' for type " + reference);
        }
        for (PNamespaceFileTypeRegistry registry : registries) {
            try {
                return registry.resolve(reference.asLocalType());
            } catch (IllegalArgumentException e) {
                // ignore.
            }
        }
        throw new IllegalArgumentException("No type " + reference.getName() + " in namespace " + reference.getNamespace());
    }
}
