import io.pvdnc.core.io.PSerializer;

/**
 * Core library for providence.
 */
module io.pvdnc.core {
    requires transitive net.morimekta.collect;
    requires net.morimekta.strings;
    requires net.morimekta.file;

    exports io.pvdnc.core;
    exports io.pvdnc.core.impl;
    exports io.pvdnc.core.io;
    exports io.pvdnc.core.property;
    exports io.pvdnc.core.reflect;
    exports io.pvdnc.core.registry;
    exports io.pvdnc.core.rpc;
    exports io.pvdnc.core.types;
    exports io.pvdnc.core.utils;

    uses PSerializer;
}