// generated by providence-idl-generator:{version} @ {date}
package io.pvdnc.other.core.test;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.registry.PSimpleTypeRegistry;
import io.pvdnc.core.types.PFileDescriptor;
import net.morimekta.collect.UnmodifiableList;
import net.morimekta.collect.UnmodifiableMap;

@SuppressWarnings("unused")
public final class TestOn_FileDescriptor extends PFileDescriptor {
    public static final TestOn_FileDescriptor kDescriptor = new TestOn_FileDescriptor();

    private TestOn_FileDescriptor() {
        super("test_on",
                PPropertyMap.of(
                        "java.namespace", "io.pvdnc.other.core.test"),
                UnmodifiableMap.mapOf(),
                UnmodifiableList.listOf(
                        OtherEnum.kDescriptor,
                        OtherMessage.kDescriptor));
        PSimpleTypeRegistry.getInstance().registerRecursively(this);
    }
}
