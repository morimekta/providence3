// generated by providence-idl-generator:{version} @ {date}
package io.pvdnc.core.test;

import io.pvdnc.core.PEnum;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PEnumValue;
import net.morimekta.collect.UnmodifiableList;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

@SuppressWarnings("unused")
public enum TestEnum implements PEnum {
    // test = 1
    TEST($Descriptor.kValueTest),
    // other = 2
    OTHER($Descriptor.kValueOther),
    // NULL = 0
    NULL($Descriptor.kValueNull),
    ;

    private final PEnumValue meta;

    TestEnum(PEnumValue meta) {
        this.meta = meta;
    }

    // ---- PEnum ----

    @Override
    public PEnumValue $meta() {
        return meta;
    }

    @Override
    public $Descriptor $descriptor() {
        return kDescriptor;
    }

    public static final $Descriptor kDescriptor = new $Descriptor();

    public static Supplier<$Descriptor> provider() {
        return () -> kDescriptor;
    }

    public static TestEnum findById(Integer id) {
        if (id == null) return null;
        switch (id) {
            case 1: return TEST;
            case 2: return OTHER;
            case 0: return NULL;
            default: return null;
        }
    }

    public static TestEnum findByName(String name) {
        if (name == null) return null;
        switch (name) {
            case "test": return TEST;
            case "other": return OTHER;
            case "NULL": return NULL;
            default: return null;
        }
    }

    public static TestEnum valueForId(int id) {
        TestEnum value = findById(id);
        if (value == null) {
            throw new IllegalArgumentException("No test.TestEnum value for id " + id);
        }
        return value;
    }

    public static TestEnum valueForName(String name) {
        Objects.requireNonNull(name, "name == null");
        TestEnum value = findByName(name);
        if (value == null) {
            throw new IllegalArgumentException("No test.TestEnum value for name '" + name + "'");
        }
        return value;
    }

    public static class $Descriptor extends PEnumDescriptor {
        public static final PEnumValue kValueTest =
                new PEnumValue("test", 1, PPropertyMap.of(
                        ), () -> TestEnum.kDescriptor);
        public static final PEnumValue kValueOther =
                new PEnumValue("other", 2, PPropertyMap.of(
                        ), () -> TestEnum.kDescriptor);
        public static final PEnumValue kValueNull =
                new PEnumValue("NULL", 0, PPropertyMap.of(
                        ), () -> TestEnum.kDescriptor);

        private $Descriptor() {
            super("test", "TestEnum",
                  PPropertyMap.of());
        }

        // ---- PEnumDescriptor ----

        @Override
        public List<TestEnum> allValues() {
            return UnmodifiableList.asList(TestEnum.values());
        }

        @Override
        public TestEnum findById(Integer id) {
            return TestEnum.findById(id);
        }

        @Override
        public TestEnum findByName(String name) {
            return TestEnum.findByName(name);
        }

        // ---- PDescriptor ----

        @Override
        public Optional<TestEnum> getDefaultValue() {
            return Optional.empty();
        }

        // ---- Static ----
    }
}
