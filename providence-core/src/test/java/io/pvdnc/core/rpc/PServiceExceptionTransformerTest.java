package io.pvdnc.core.rpc;

import io.pvdnc.core.test.TestMessage;
import io.pvdnc.core.types.PServiceMethod;
import io.pvdnc.core.property.PPropertyMap;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;

public class PServiceExceptionTransformerTest {
    @Test
    public void testSimple() {
        PServiceMethod method = new PServiceMethod("foo",
                                                   TestMessage.kDescriptor,
                                                   TestMessage.kDescriptor,
                                                   PPropertyMap.empty(),
                                                   () -> null);
        PServiceExceptionTransformer simple = PServiceExceptionTransformer.SIMPLE;
        IOException io = new IOException("test");
        Exception other = new Exception("other");
        assertThat(simple.transform(method, io), is(sameInstance(io)));
        assertThat(simple.transform(method, other), is(instanceOf(IOException.class)));
        assertThat(simple.transform(method, other).getCause(), is(sameInstance(other)));
    }
}
