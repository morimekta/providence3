package io.pvdnc.core.impl;

import io.pvdnc.core.PAny;
import io.pvdnc.core.io.PSerializer;
import io.pvdnc.core.io.PSerializerProvider;
import io.pvdnc.core.test.TestMessage;
import io.pvdnc.core.io.TestStaticSerializer;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static io.pvdnc.core.registry.PTypeReference.ref;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.fail;

public class PAnyTest {
    private static final PSerializer TEST_SERIALIZER = new TestStaticSerializer("application/test-1");
    private static final PSerializer TEST_SERIALIZER2 = new TestStaticSerializer("application/test-2");

    @BeforeAll
    public static void registerSerializer() {
        PSerializerProvider.getInstance()
                .register(TEST_SERIALIZER).setDefault(TEST_SERIALIZER.mediaTypes().get(0));
    }

    @AfterAll
    public static void unregisterSerializer() {
        PSerializerProvider.getInstance()
                .unregister(TEST_SERIALIZER);
    }

    @Test
    public void testSimple() {
        TestMessage test = TestMessage.newBuilder().setIntField(42).build();
        AnyBinary any = PAny.pack(test, TEST_SERIALIZER);
        assertThat(any.getTypeName(), is(TestMessage.kDescriptor.getTypeName()));
        assertThat(any.getMediaType(), is(TEST_SERIALIZER.mediaTypes().get(0)));
        assertThat(any.getData().length(), is(4));
        MatcherAssert.assertThat(any.getMessage(), is(test));
        MatcherAssert.assertThat(any.optionalIfA(TestMessage.kDescriptor), is(Optional.of(test)));
        MatcherAssert.assertThat(any.optionalIfA(Empty.kDescriptor), is(Optional.empty()));
        MatcherAssert.assertThat(any.isPresent(), is(true));

        AnyWrapper unpacked = any.unpack();
        assertThat(unpacked.getMessage(), is(test));
        MatcherAssert.assertThat(unpacked.optionalIfA(TestMessage.kDescriptor), is(Optional.of(test)));
        MatcherAssert.assertThat(unpacked.optionalIfA(Empty.kDescriptor), is(Optional.empty()));
        assertThat(unpacked.isPresent(), is(true));

        AnyBinary anySame = PAny.pack(test, TEST_SERIALIZER);
        assertThat(anySame, is(any));
        assertThat(anySame, is(not(unpacked)));
        assertThat(anySame.hashCode(), is(anySame.hashCode()));
        assertThat(anySame.hashCode(), is(not(unpacked.hashCode())));

        AnyBinary reEncoded = any.pack(TEST_SERIALIZER2);
        assertThat(reEncoded.getMediaType(), is(TEST_SERIALIZER2.mediaTypes().get(0)));
        assertThat(reEncoded.getTypeName(), is(TestMessage.kDescriptor.getTypeName()));
        try {
            reEncoded.unpack();
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No serializer for application/test-2"));
        }
        MatcherAssert.assertThat(reEncoded.unpack(TEST_SERIALIZER2).getMessage(), is(test));
    }

    @Test
    public void testEmpty() {
        assertThat(PAny.empty(), is(PAny.empty()));
        assertThat(PAny.empty().isPresent(), is(false));
        assertThat(PAny.TYPE_NAME_FIELD, containsInAnyOrder("@", "@type", "__type", "__type__"));
        try {
            PAny tmp = PAny.wrap(null);
            fail("no exception");
            assertThat(tmp, is(nullValue()));
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("message == null"));
        }
    }

    @Test
    public void testAnyTypeNameFormat() {
        AnyTypeNameFormat format = AnyTypeNameFormat.DEFAULT;
        assertThat(format.toReference("foo.Bar"), is(ref("foo", "Bar")));
        assertThat(format.toReference("\"foo.Bar\""), is(ref("foo", "Bar")));
        assertThat(format.toReference("'foo.Bar'"), is(ref("foo", "Bar")));
        assertThat(format.toReference("'type.googleapis.com/foo.Bar'"), is(ref("foo", "Bar")));
        assertThat(format.toTypeName(AnyBinary.kDescriptor), is("providence.AnyBinary"));
    }
}
