package io.pvdnc.core.utils;

import io.pvdnc.core.reflect.TestReflectionUtil;
import io.pvdnc.core.test.TestEnum;
import io.pvdnc.core.test.TestMessage;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.types.PType;
import net.morimekta.collect.util.Binary;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Stream;

import static java.nio.charset.StandardCharsets.UTF_8;
import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.collect.UnmodifiableSortedMap.sortedMapOf;
import static net.morimekta.collect.UnmodifiableSortedSet.sortedSetOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class ConversionUtilTest {
    public static Stream<Arguments> coerceStrictArgs() {
        return Stream.of(
                // Void
                arguments(PType.VOID, null, null),
                arguments(PType.VOID, true, true),
                // Boolean
                arguments(PType.BOOL, null, null),
                arguments(PType.BOOL, true, true),
                arguments(PType.BOOL, false, false),
                arguments(PType.BOOL, 1, true),
                arguments(PType.BOOL, 0, false),
                arguments(PType.BOOL, TestEnum.TEST, true),
                arguments(PType.BOOL, TestEnum.NULL, false),
                // Byte
                arguments(PType.BYTE, null, null),
                arguments(PType.BYTE, 42, (byte) 42),
                arguments(PType.BYTE, 42.0, (byte) 42),
                arguments(PType.BYTE, true, (byte) 1),
                arguments(PType.BYTE, false, (byte) 0),
                arguments(PType.BYTE, TestEnum.OTHER, (byte) 2),
                // Short
                arguments(PType.SHORT, null, null),
                arguments(PType.SHORT, 42, (short) 42),
                arguments(PType.SHORT, 42.0, (short) 42),
                arguments(PType.SHORT, true, (short) 1),
                arguments(PType.SHORT, false, (short) 0),
                arguments(PType.SHORT, TestEnum.OTHER, (short) 2),
                // Integer
                arguments(PType.INT, null, null),
                arguments(PType.INT, 42L, 42),
                arguments(PType.INT, 42.0, 42),
                arguments(PType.INT, true, 1),
                arguments(PType.INT, false, 0),
                arguments(PType.INT, TestEnum.OTHER, 2),
                // Long
                arguments(PType.LONG, null, null),
                arguments(PType.LONG, 42, 42L),
                arguments(PType.LONG, 42.0, 42L),
                arguments(PType.LONG, true, 1L),
                arguments(PType.LONG, false, 0L),
                arguments(PType.LONG, TestEnum.OTHER, 2L),
                // Double
                arguments(PType.DOUBLE, null, null),
                arguments(PType.DOUBLE, 42, 42.0),
                arguments(PType.DOUBLE, 42.0, 42.0),
                arguments(PType.DOUBLE, 42.0f, 42.0),
                // String
                arguments(PType.STRING, null, null),
                arguments(PType.STRING, "foo", "foo"),
                // Binary
                arguments(PType.BINARY, null, null),
                arguments(PType.BINARY, Binary.wrap("foo".getBytes(UTF_8)), Binary.wrap("foo".getBytes(UTF_8))),
                // Enum
                arguments(TestEnum.kDescriptor, TestEnum.TEST, TestEnum.TEST),
                arguments(TestEnum.kDescriptor, 2, TestEnum.OTHER),
                arguments(TestEnum.kDescriptor, "other", TestEnum.OTHER),
                // Message
                // Arguments.arguments(Imaginary.kDescriptor,Imaginary.builder().setV(7).build(),Imaginary.builder().setV(7).build()),
                // List
                arguments(new PListDescriptor<Integer>(() -> PType.INT),
                             listOf(true, 2L, 3),
                             listOf(1, 2, 3)),
                arguments(new PListDescriptor<Integer>(() -> PType.INT),
                 sortedSetOf(3, 2, 1),
                 listOf(1, 2, 3)),
                // Map
                arguments(new PMapDescriptor<String,String>(() -> PType.STRING, () -> PType.STRING, false),
                             sortedMapOf("foo", "bar"),
                             mapOf("foo", "bar"))
                        );
    }

    @ParameterizedTest
    @MethodSource("coerceStrictArgs")
    public void testCoerceStrict(PDescriptor descriptor, Object in, Object out) {
        assertThat(ConversionUtil.coerceStrict(descriptor, in).orElse(null), is(out));
        assertThat(ConversionUtil.coerce(descriptor, in).orElse(null), is(out));
    }

    public static Stream<Arguments> coerceNotStrictData() {
        return Stream.of(
                // Boolean
                arguments(PType.BOOL, "1", Boolean.TRUE,
                          "Invalid value type class java.lang.String for type bool"),
                arguments(PType.BOOL, "0", Boolean.FALSE,
                          "Invalid value type class java.lang.String for type bool"),
                // Byte
                arguments(PType.BYTE, "42", (byte) 42,
                          "Invalid value type class java.lang.String for type byte"),
                arguments(PType.BYTE, "0x42", (byte) 66,
                          "Invalid value type class java.lang.String for type byte"),
                // Short
                arguments(PType.SHORT, "42", (short) 42,
                          "Invalid value type class java.lang.String for type short"),
                arguments(PType.SHORT, "0x42", (short) 66,
                          "Invalid value type class java.lang.String for type short"),
                // Integer
                arguments(PType.INT, "42", 42,
                          "Invalid value type class java.lang.String for type int"),
                arguments(PType.INT, "0x42", 66,
                          "Invalid value type class java.lang.String for type int"),
                // Long
                arguments(PType.LONG, "42", 42L,
                          "Invalid value type class java.lang.String for type long"),
                arguments(PType.LONG, "0x42", 66L,
                          "Invalid value type class java.lang.String for type long"),
                // Double
                arguments(PType.FLOAT, TestEnum.OTHER, 2.0f,
                          "Invalid value type class io.pvdnc.core.test.TestEnum for type float"),
                // Double
                arguments(PType.DOUBLE, TestEnum.OTHER, 2.0,
                          "Invalid value type class io.pvdnc.core.test.TestEnum for type double"),
                // String
                arguments(PType.STRING, TestEnum.OTHER, "other",
                          "Invalid value type class io.pvdnc.core.test.TestEnum for type string"),
                arguments(PType.STRING, 123, "123",
                          "Invalid value type class java.lang.Integer for type string"),
                // Binary
                arguments(PType.BINARY, "AAb=", Binary.fromBase64("AAb="),
                          "Invalid value type class java.lang.String for type binary"),
                // Enum
                arguments(TestEnum.kDescriptor, "2", TestEnum.OTHER,
                          "Unknown test.TestEnum value for string '2'"),
                arguments(TestEnum.kDescriptor, "0x2", TestEnum.OTHER,
                          "Unknown test.TestEnum value for string '0x2'"),
                // Message
                arguments(TestMessage.kDescriptor,
                          mapOf("int_field", 23, "string_field", "5.6"),
                          TestMessage.newBuilder().setIntField(23).setStringField("5.6").build(),
                          "Invalid value type class net.morimekta.collect.UnmodifiableMap for message test.TestMessage"),
                // List
                arguments(PListDescriptor.provider(() -> PType.INT).get(),
                          new ArrayList<Integer>() {{
                              add(2);
                              add(null);
                              add(5);
                          }},
                          listOf(2, 5),
                          "Null value in list"),
                // Set
                arguments(PSetDescriptor.sortedProvider(() -> PType.INT).get(),
                          new ArrayList<Integer>() {{
                              add(2);
                              add(null);
                              add(5);
                          }},
                          sortedSetOf(2, 5),
                          "Null value in set"),
                // Map
                arguments(PMapDescriptor.provider(PType.STRING.provider(), PType.INT.provider()).get(),
                          new HashMap<String, Integer>() {{
                              put("foo", null);
                              put("bar", 42);
                          }},
                          mapOf("bar", 42),
                          "Null key or value in map")
                        );
    }

    @ParameterizedTest
    @MethodSource("coerceNotStrictData")
    public void testCoerceNotStrict(PDescriptor descriptor,
                                    Object in,
                                    Object out,
                                    String messageOnStrict) {
        assertThat(ConversionUtil.coerce(descriptor, in).orElse(null), is(out));
        try {
            ConversionUtil.coerceStrict(descriptor, in);
            fail("no exception: " + messageOnStrict);
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is(messageOnStrict));
        }
    }

    public static Stream<Arguments> coerceFailData() {
        return Stream.of(
                // Void
                arguments(PType.VOID, false,
                          "Invalid void value false", null),
                arguments(PType.VOID, "",
                          "Invalid value type class java.lang.String for type void", null),
                // Boolean
                arguments(PType.BOOL, "3",
                          "Unknown boolean value for string '3'",
                          "Invalid value type class java.lang.String for type bool", null),
                arguments(PType.BOOL, 7.4,
                          "Invalid value type class java.lang.Double for type bool", null),
                // Byte
                arguments(PType.BYTE, 123.45,
                          "Truncating byte decimals from 123.45", null),
                arguments(PType.BYTE, 1234,
                          "byte value outside of bounds: 1234 > 127", null),
                arguments(PType.BYTE, -1234,
                          "byte value outside of bounds: -1234 < -128", null),
                arguments(PType.BYTE, Binary.empty(),
                          "Invalid value type class net.morimekta.collect.util.Binary for type byte", null),
                arguments(PType.BYTE, "0xfoo",
                          "Invalid string value '0xfoo' for type byte",
                          "Invalid value type class java.lang.String for type byte"),
                // Short
                arguments(PType.SHORT, 123.45,
                          "Truncating short decimals from 123.45", null),
                arguments(PType.SHORT, 123456,
                          "short value outside of bounds: 123456 > 32767", null),
                arguments(PType.SHORT, -123456,
                          "short value outside of bounds: -123456 < -32768", null),
                arguments(PType.SHORT, Binary.empty(),
                          "Invalid value type class net.morimekta.collect.util.Binary for type short", null),
                arguments(PType.SHORT, "0xfoo",
                          "Invalid string value '0xfoo' for type short",
                          "Invalid value type class java.lang.String for type short"),
                // Short
                arguments(PType.INT, 123.45,
                          "Truncating int decimals from 123.45", null),
                arguments(PType.INT, 12345678901234L,
                          "int value outside of bounds: 12345678901234 > 2147483647", null),
                arguments(PType.INT, -12345678901234L,
                          "int value outside of bounds: -12345678901234 < -2147483648", null),
                arguments(PType.INT, Binary.empty(),
                          "Invalid value type class net.morimekta.collect.util.Binary for type int", null),
                arguments(PType.INT, "0xfoo",
                          "Invalid string value '0xfoo' for type int",
                          "Invalid value type class java.lang.String for type int"),
                // Short
                arguments(PType.LONG, 123.45,
                          "Truncating long decimals from 123.45", null),
                arguments(PType.LONG, Binary.empty(),
                          "Invalid value type class net.morimekta.collect.util.Binary for type long", null),
                arguments(PType.LONG, "0xfoo",
                          "Invalid string value '0xfoo' for type long",
                          "Invalid value type class java.lang.String for type long"),
                // Double
                arguments(PType.DOUBLE, "0xfoo",
                          "Invalid value type class java.lang.String for type double", null),
                // Binary
                arguments(PType.BINARY, 1234,
                          "Invalid value type class java.lang.Integer for type binary", null),
                // Enum
                arguments(TestEnum.kDescriptor, 123.45,
                          "Invalid value type class java.lang.Double for enum test.TestEnum",
                          "Invalid value type class java.lang.Double for enum test.TestEnum"),
                arguments(TestEnum.kDescriptor, 4,
                          "Unknown test.TestEnum value for id 4",
                          "Unknown test.TestEnum value for id 4"),
                arguments(TestEnum.kDescriptor,
                          TestReflectionUtil.kFibonacci.findById(1),
                          "Invalid value type class io.pvdnc.core.reflect.CEnum for enum test.TestEnum",
                          "Invalid value type class io.pvdnc.core.reflect.CEnum for enum test.TestEnum"),
                arguments(TestEnum.kDescriptor, "7",
                          "Unknown test.TestEnum value for string '7'",
                          "Unknown test.TestEnum value for string '7'"),
                arguments(TestEnum.kDescriptor, "0x7",
                          "Unknown test.TestEnum value for string '0x7'",
                          "Unknown test.TestEnum value for string '0x7'"),
                // Message
                arguments(TestMessage.kDescriptor, TestReflectionUtil.kFieldTypes.newBuilder().build(),
                          "Unable to cast message type providence.FieldTypes to test.TestMessage",
                          "Unable to cast message type providence.FieldTypes to test.TestMessage"),
                arguments(TestMessage.kDescriptor,
                          mapOf(2, 23),
                          "Invalid message map key: 2",
                          "Invalid value type class net.morimekta.collect.UnmodifiableMap for message test.TestMessage"),
                arguments(TestMessage.kDescriptor,
                          mapOf("boo", 23),
                          "No such field boo in test.TestMessage",
                          "Invalid value type class net.morimekta.collect.UnmodifiableMap for message test.TestMessage"),
                // List
                arguments(PListDescriptor.provider(PType.INT.provider()).get(), 123,
                          "Invalid value type class java.lang.Integer for list<int>", null),
                // Set
                arguments(PSetDescriptor.provider(PType.BYTE.provider()).get(), 123,
                          "Invalid value type class java.lang.Integer for set<byte>", null),
                arguments(PSetDescriptor.sortedProvider(PType.BYTE.provider()).get(), 123,
                          "Invalid value type class java.lang.Integer for set<byte>", null),
                // Map
                arguments(PMapDescriptor.provider(PType.STRING.provider(), PType.STRING.provider()).get(), 123,
                          "Invalid value type class java.lang.Integer for map<string,string>", null),
                arguments(PMapDescriptor.sortedProvider(PType.STRING.provider(), PType.STRING.provider()).get(), 123,
                          "Invalid value type class java.lang.Integer for map<string,string>", null));
    }

    @ParameterizedTest
    @MethodSource("coerceFailData")
    public void testCoerceFail(PDescriptor descriptor,
                               Object in,
                               String message,
                               String messageOnStrict) {
        try {
            ConversionUtil.coerce(descriptor, in);
            fail("no exception: " + message);
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is(message));
        }
        try {
            ConversionUtil.coerceStrict(descriptor, in);
            fail("no exception: " + message);
        } catch (IllegalArgumentException e) {
            if (messageOnStrict != null) {
                assertThat(e.getMessage(), is(messageOnStrict));
            } else {
                assertThat(e.getMessage(), is(message));
            }
        }
    }
}
