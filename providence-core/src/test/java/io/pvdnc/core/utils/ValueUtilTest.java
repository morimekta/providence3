package io.pvdnc.core.utils;

import io.pvdnc.core.test.TestEnum;
import io.pvdnc.core.test.TestMessage;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static io.pvdnc.core.utils.ValueUtil.asString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ValueUtilTest {
    @Test
    public void testAsString() {
        assertThat(asString(null), is("null"));
        assertThat(asString(Optional.empty()), is("null"));
        assertThat(asString("foo"), is("'foo'"));
        assertThat(asString(Optional.of("foo")), is("'foo'"));
        assertThat(asString(TestMessage.newBuilder()
                                       .setStringField("foo")
                                       .build()), is("{string_field='foo'}"));
        assertThat(asString(TestEnum.OTHER), is("other"));
        assertThat(asString(mapOf(TestEnum.OTHER, "foo", TestEnum.NULL, "bar")),
                   is("{other: 'foo', NULL: 'bar'}"));
        assertThat(asString(listOf(TestEnum.OTHER, TestEnum.TEST)),
                   is("[other, test]"));
        assertThat(asString(setOf(TestEnum.OTHER, TestEnum.TEST)),
                   is("{other, test}"));
    }
}
