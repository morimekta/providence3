package io.pvdnc.core.utils;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.PMessageBuilder;
import io.pvdnc.core.reflect.TestReflectionUtil;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static io.pvdnc.core.utils.BuilderUtil.toBuilderAll;
import static io.pvdnc.core.utils.BuilderUtil.toBuilderIfNonNull;
import static io.pvdnc.core.utils.BuilderUtil.toBuilderValues;
import static io.pvdnc.core.utils.BuilderUtil.toImmutableAll;
import static io.pvdnc.core.utils.BuilderUtil.toImmutableIfNotNull;
import static io.pvdnc.core.utils.BuilderUtil.toImmutableValues;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;

public class BuilderUtilTest {
    @Test
    public void testToImmutableIfNotNull() {
        assertThat(toBuilderIfNonNull(null), is(nullValue()));
        assertThat(toImmutableIfNotNull(null), is(nullValue()));

        PMessageBuilder builder = TestReflectionUtil.kFieldTypes.newBuilder();
        assertThat(toImmutableIfNotNull(builder), is(instanceOf(PMessage.class)));
        assertThat(toImmutableIfNotNull(builder), is(not(instanceOf(PMessageBuilder.class))));

        PMessage message = builder.build();
        assertThat(message, is(not(instanceOf(PMessageBuilder.class))));
        assertThat(toImmutableIfNotNull(message), is(sameInstance(message)));
        assertThat(toBuilderIfNonNull(message), is(instanceOf(PMessageBuilder.class)));
    }

    @Test
    public void testToImmutableAll() {
        assertThat(toBuilderAll(null), is(nullValue()));
        assertThat(toImmutableAll(null), is(nullValue()));

        PMessageBuilder a = TestReflectionUtil.kFieldTypes.newBuilder();
        PMessageBuilder b = TestReflectionUtil.kFieldTypes.newBuilder();
        List<PMessage> all = toImmutableAll(List.of(a, b));
        assertThat(all.size(), is(2));
        assertThat(all.get(0), is(instanceOf(PMessage.class)));
        assertThat(all.get(1), is(instanceOf(PMessage.class)));
        assertThat(all.get(0), is(not(instanceOf(PMessageBuilder.class))));
        assertThat(all.get(1), is(not(instanceOf(PMessageBuilder.class))));

        List<PMessageBuilder> builders = toBuilderAll(all);
        assertThat(builders.size(), is(2));
        assertThat(builders.get(0), is(instanceOf(PMessageBuilder.class)));
        assertThat(builders.get(1), is(instanceOf(PMessageBuilder.class)));
    }

    @Test
    public void testToImmutableValues() {
        assertThat(toBuilderValues(null), is(nullValue()));
        assertThat(toImmutableValues(null), is(nullValue()));

        PMessageBuilder a = TestReflectionUtil.kFieldTypes.newBuilder();
        PMessageBuilder b = TestReflectionUtil.kFieldTypes.newBuilder();

        Map<String, PMessageBuilder> builders = mapOf("a", a, "b", b);
        Map<String, PMessage> all = toImmutableValues(builders);
        Map<String, PMessage> sorted = toImmutableValues(new TreeMap<>(builders));

        assertThat(all.size(), is(2));
        assertThat(all.get("a"), is(instanceOf(PMessage.class)));
        assertThat(all.get("b"), is(instanceOf(PMessage.class)));
        assertThat(all.get("a"), is(not(instanceOf(PMessageBuilder.class))));
        assertThat(all.get("b"), is(not(instanceOf(PMessageBuilder.class))));

        assertThat(sorted, is(instanceOf(SortedMap.class)));
        assertThat(sorted, is(all));

        builders = toBuilderValues(all);
        assertThat(builders.get("a"), is(instanceOf(PMessageBuilder.class)));
        assertThat(builders.get("b"), is(instanceOf(PMessageBuilder.class)));

        assertThat(toBuilderValues(sorted), is(builders));
        assertThat(toBuilderValues(sorted), is(instanceOf(SortedMap.class)));
    }
}
