package io.pvdnc.core.utils;

import io.pvdnc.core.reflect.TestReflectionUtil;
import io.pvdnc.core.test.TestMessage;
import org.junit.jupiter.api.Test;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static io.pvdnc.core.utils.FieldUtil.fieldPath;
import static io.pvdnc.core.utils.FieldUtil.fieldPathAppend;
import static io.pvdnc.core.utils.FieldUtil.fieldPathToFields;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class FieldUtilTest {
    @Test
    public void testFieldPath() {
        assertThat(fieldPathToFields(TestMessage.kDescriptor, "int_field"), is(listOf(TestMessage.$Descriptor.kFieldIntField)));
        assertThat(fieldPath(listOf(TestMessage.$Descriptor.kFieldEnumField)), is("enum_field"));
        assertThat(fieldPathAppend("", TestMessage.$Descriptor.kFieldEnumField), is("enum_field"));
        assertThat(fieldPathAppend("foo", TestMessage.$Descriptor.kFieldStringField), is("foo.string_field"));
    }

    @Test
    public void testFieldPathToFields_badArgs() {
        try {
            fieldPathToFields(null, "foo");
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("rootDescriptor == null"));
        }
        try {
            fieldPathToFields(TestMessage.kDescriptor, null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("path == null"));
        }
        try {
            fieldPathToFields(TestMessage.kDescriptor, "");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty path"));
        }
        try {
            fieldPathToFields(TestMessage.kDescriptor, "null");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Message test.TestMessage has no field named null"));
        }
        try {
            fieldPathToFields(TestMessage.kDescriptor, ".foo");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty field name in '.foo'"));
        }
        try {
            fieldPathToFields(TestMessage.kDescriptor, "foo.bar");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Message test.TestMessage has no field named foo"));
        }
        try {
            fieldPathToFields(TestMessage.kDescriptor, "int_field.bar");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Field 'int_field' is not of message type in test.TestMessage"));
        }
        try {
            fieldPathToFields(TestReflectionUtil.kFieldTypes, "msg.");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty field name in 'msg.'"));
        }
    }
}
