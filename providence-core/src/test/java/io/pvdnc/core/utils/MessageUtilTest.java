package io.pvdnc.core.utils;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.PMessageBuilder;
import io.pvdnc.core.test.TestEnum;
import io.pvdnc.core.test.TestMessage;
import io.pvdnc.core.types.PField;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static io.pvdnc.core.utils.MessageUtil.getInMessage;
import static io.pvdnc.core.utils.MessageUtil.getTargetModifications;
import static io.pvdnc.core.utils.MessageUtil.optionalInMessage;
import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class MessageUtilTest {
    @Test
    public void testModifiedFields() {
        TestMessage message = TestMessage.newBuilder()
                                         .setEnumField(TestEnum.NULL)
                                         .build();
        TestMessage.Builder builder = message.mutate().setIntField(42);

        assertThat(MessageUtil.modifiedFields(builder), is(setOf(TestMessage.$Descriptor.kFieldIntField)));
    }

    @Test
    public void testPresentFields() {
        TestMessage message = TestMessage.newBuilder()
                                         .setEnumField(TestEnum.NULL)
                                         .build();
        TestMessage.Builder builder = message.mutate().setIntField(42);

        assertThat(MessageUtil.presentFields(message), is(setOf(TestMessage.$Descriptor.kFieldEnumField)));
        assertThat(MessageUtil.presentFields(builder), is(setOf(TestMessage.$Descriptor.kFieldEnumField, TestMessage.$Descriptor.kFieldIntField)));
    }

    @Test
    public void testPresentFieldsNames() {
        TestMessage message = TestMessage.newBuilder()
                                         .setEnumField(TestEnum.NULL)
                                         .build();
        TestMessage.Builder builder = message.mutate().setIntField(42);

        assertThat(MessageUtil.presentFieldsNames(message), is(setOf("enum_field")));
        assertThat(MessageUtil.presentFieldsNames(builder), is(setOf("enum_field", "int_field")));
    }

    public static Stream<Arguments> testGetTargetModifications() {

        return Stream.of(arguments(TestMessage.newBuilder().build(),
                                   TestMessage.newBuilder().build(),
                                   setOf()));
    }

    @ParameterizedTest
    @MethodSource("testGetTargetModifications")
    public void testGetTargetModifications(PMessage a, PMessage b, Set<PField> modifiedFields) {
        PMessageBuilder mods = getTargetModifications(a, b);
        assertThat(MessageUtil.modifiedFields(mods), is(modifiedFields));
    }

    @Test
    public void testGetInMessage() {
        TestMessage message = TestMessage.newBuilder()
                                         .setIntField(TestEnum.OTHER.getValue())
                                         .build();
        assertThat(getInMessage(message, listOf(TestMessage.$Descriptor.kFieldEnumField)), is(Optional.empty()));
        assertThat(getInMessage(message, listOf(TestMessage.$Descriptor.kFieldIntField)), is(Optional.of(2)));
        assertThat(getInMessage(message, listOf(TestMessage.$Descriptor.kFieldStringField)), is(Optional.of("")));
    }

    @Test
    public void testOptionalInMessage() {
        TestMessage message = TestMessage.newBuilder()
                                         .setIntField(TestEnum.OTHER.getValue())
                                         .build();
        assertThat(optionalInMessage(message, listOf(TestMessage.$Descriptor.kFieldEnumField)), is(Optional.empty()));
        assertThat(optionalInMessage(message, listOf(TestMessage.$Descriptor.kFieldIntField)), is(Optional.of(2)));
        assertThat(optionalInMessage(message, listOf(TestMessage.$Descriptor.kFieldStringField)), is(Optional.empty()));
    }
}
