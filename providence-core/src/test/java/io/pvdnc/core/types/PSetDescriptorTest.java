package io.pvdnc.core.types;

import net.morimekta.collect.SetBuilder;
import net.morimekta.collect.UnmodifiableSet;
import net.morimekta.collect.UnmodifiableSortedSet;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static net.morimekta.collect.UnmodifiableSet.setOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class PSetDescriptorTest {
    @Test
    public void testSet() {
        PSetDescriptor<?> set = PSetDescriptor.provider(PType.STRING.provider()).get();
        assertThat(set.getItemType(), is(PType.STRING));
        assertThat(set.isSorted(), is(false));
        assertThat(set.builder(5), is(instanceOf(SetBuilder.class)));
        assertThat(set.builder(5), is(instanceOf(UnmodifiableSet.Builder.class)));
        assertThat(set.getType(), is(PType.SET));
        assertThat(set.getDefaultValue(), is(Optional.of(setOf())));
        assertThat(set.getQualifiedName("foo"), is("set<string>"));
        assertThat(set.getTypeName(), is("set<string>"));

        PSetDescriptor<?> s2 = PSetDescriptor.provider(PType.STRING.provider()).get();
        PSetDescriptor<?> s3 = PSetDescriptor.provider(PType.INT.provider()).get();

        assertThat(set.hashCode(), is(set.hashCode()));
        assertThat(set.hashCode(), is(s2.hashCode()));
        assertThat(set.hashCode(), is(not(s3.hashCode())));

        assertThat(set, is(set));
        assertThat(set, is(s2));
        assertThat(set, is(not(s3)));
        assertThat(set, is(not(new Object())));
    }

    @Test
    public void testSortedSet() {
        PSetDescriptor<?> set = PSetDescriptor.sortedProvider(PType.STRING.provider()).get();

        assertThat(set.getItemType(), is(PType.STRING));
        assertThat(set.isSorted(), is(true));
        assertThat(set.builder(5), is(instanceOf(SetBuilder.class)));
        assertThat(set.builder(5), is(instanceOf(UnmodifiableSortedSet.Builder.class)));
        assertThat(set.getType(), is(PType.SET));
        assertThat(set.getDefaultValue(), is(Optional.of(setOf())));
        assertThat(set.getQualifiedName("foo"), is("set<string>"));
        assertThat(set.getTypeName(), is("set<string>"));

        PSetDescriptor<?> s2 = PSetDescriptor.sortedProvider(PType.STRING.provider()).get();
        PSetDescriptor<?> s3 = PSetDescriptor.sortedProvider(PType.INT.provider()).get();

        assertThat(set.hashCode(), is(set.hashCode()));
        assertThat(set.hashCode(), is(s2.hashCode()));
        assertThat(set.hashCode(), is(not(s3.hashCode())));

        assertThat(set, is(set));
        assertThat(set, is(s2));
        assertThat(set, is(not(s3)));

        PSetDescriptor<?> s4 = PSetDescriptor.provider(PType.STRING.provider()).get();
        assertThat(set, is(not(s4)));
        assertThat(set.hashCode(), is(not(s4.hashCode())));

    }
}
