package io.pvdnc.core.types;

import io.pvdnc.core.property.PPropertyMap;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class PTypeDefDescriptorTest {
    @Test
    public void testTypeDef() {
        PTypeDefDescriptor typeDef = new PTypeDefDescriptor("test", "Type",
                                                            PPropertyMap.empty(),
                                                            PListDescriptor.provider(PType.BOOL.provider()));

        assertThat(typeDef.getDescriptor(), is(instanceOf(PListDescriptor.class)));
        assertThat(typeDef.getDefaultValue(), is(Optional.of(List.of())));
        assertThat(typeDef.toString(), is("test.Type{list<bool>}"));
    }
}
