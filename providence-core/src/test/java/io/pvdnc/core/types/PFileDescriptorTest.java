package io.pvdnc.core.types;

import io.pvdnc.core.impl.AnyBinary;
import io.pvdnc.core.impl.Empty;
import io.pvdnc.core.impl.Providence_FileDescriptor;
import io.pvdnc.core.property.PDefaultProperties;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static io.pvdnc.core.registry.PTypeReference.ref;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;

public class PFileDescriptorTest {
    @Test
    public void testSimple() {
        PFileDescriptor sut = Providence_FileDescriptor.kDescriptor;

        assertThat(sut.declaredDescriptors(), is(listOf(Empty.kDescriptor, AnyBinary.kDescriptor)));
        assertThat(sut.declaredDescriptors(PDeclarationType.ENUM), is(listOf()));
        assertThat(sut.declaredTypeReferences(), is(listOf(
                ref("providence", "Empty"),
                ref("providence", "AnyBinary"))));
        assertThat(sut.declaredTypeReferences(PDeclarationType.MESSAGE), is(listOf(
                ref("providence", "Empty"),
                ref("providence", "AnyBinary"))));

        assertThat(sut.hasProperty(PDefaultProperties.EXCEPTION_MESSAGE), is(false));
        assertThat(sut.getProperty(PDefaultProperties.DEPRECATED), is(nullValue()));
        assertThat(sut.getUncheckedProperties(), is(mapOf("java.namespace", "io.pvdnc.core.impl")));

        assertThat(sut.getNamespace(), is("providence"));
        assertThat(sut.getIncludes(), is(mapOf()));
        assertThat(sut.getInclude("foo"), is(Optional.empty()));

        assertThat(sut.descriptor(ref("Empty")), is(sameInstance(Empty.kDescriptor)));
        assertThat(sut.descriptor(ref("providence", "Empty")), is(sameInstance(Empty.kDescriptor)));
    }
}
