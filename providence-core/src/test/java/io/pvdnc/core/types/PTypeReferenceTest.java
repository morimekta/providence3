package io.pvdnc.core.types;

import io.pvdnc.core.registry.PTypeReference;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class PTypeReferenceTest {
    @Test
    public void testSimpleRef() {
        PTypeReference ref = PTypeReference.ref("foo", "Bar");
        assertThat(ref.getNamespace(), is("foo"));
        assertThat(ref.getName(), is("Bar"));
        assertThat(ref.toString(), is("foo.Bar"));
        assertThat(ref.getTypeName(), is("foo.Bar"));
    }
}
