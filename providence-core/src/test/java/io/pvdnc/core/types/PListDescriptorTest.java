package io.pvdnc.core.types;

import net.morimekta.collect.ListBuilder;
import net.morimekta.collect.UnmodifiableList;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class PListDescriptorTest {
    @Test
    public void testList() {
        PListDescriptor<?> list = PListDescriptor.provider(PType.STRING.provider()).get();
        assertThat(list.getItemType(), is(PType.STRING));
        assertThat(list.builder(5), is(instanceOf(ListBuilder.class)));
        assertThat(list.builder(5), is(instanceOf(UnmodifiableList.Builder.class)));
        assertThat(list.getType(), is(PType.LIST));
        assertThat(list.getDefaultValue(), is(Optional.of(listOf())));
        assertThat(list.getQualifiedName("foo"), is("list<string>"));
        assertThat(list.getTypeName(), is("list<string>"));

        PListDescriptor<?> l2 = PListDescriptor.provider(PType.STRING.provider()).get();
        PListDescriptor<?> l3 = PListDescriptor.provider(PType.INT.provider()).get();

        assertThat(list.hashCode(), is(list.hashCode()));
        assertThat(list.hashCode(), is(l2.hashCode()));
        assertThat(list.hashCode(), is(not(l3.hashCode())));

        assertThat(list, is(list));
        assertThat(list, is(l2));
        assertThat(list, is(not(l3)));
        assertThat(list, is(not(new Object())));
    }
}
