package io.pvdnc.core.types;

import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class PTypeTest {
    @Test
    public void testTypes() { ;
        assertThat(PType.VOID.isSimple(), is(false));
        assertThat(PType.BOOL.getType(), is(PType.BOOL));
        assertThat(PType.BYTE.getBaseType(), is(Byte.TYPE));
        assertThat(PType.SHORT.getBaseClass(), is(Short.class));
        assertThat(PType.INT.isSimple(), is(true));
        assertThat(PType.LONG.provider().get(), is(PType.LONG));
        assertThat(PType.FLOAT.getType(), is(PType.FLOAT));
        assertThat(PType.DOUBLE.getQualifiedName("foo"), is("double"));
        assertThat(PType.STRING.getDefaultValue(), is(Optional.of("")));
        assertThat(PType.BINARY.getTypeName(), is("binary"));
        assertThat(PType.LIST.toString(), is("list"));

        assertThat(PType.LIST.isOneOf(), is(false));
        assertThat(PType.LIST.isOneOf(PType.INT, PType.LONG), is(false));
        assertThat(PType.BOOL.isOneOf(PType.BOOL, PType.BYTE, PType.SHORT), is(true));

        assertThat(PType.forName("set"), is(PType.SET));
        assertThat(PType.forName("foo"), is(PType.VOID));
        assertThat(PType.forClass(Map.class), is(PType.MAP));
        assertThat(PType.forClass(Short.TYPE), is(PType.SHORT));
        assertThat(PType.forClass(Object.class), is(PType.VOID));

        try {
            PType.MESSAGE.provider();
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Not allowed to get default type provider for 'message'"));
        }
    }
}
