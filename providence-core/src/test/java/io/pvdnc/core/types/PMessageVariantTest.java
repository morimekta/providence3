package io.pvdnc.core.types;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class PMessageVariantTest {
    @Test
    public void testVariant() {
        assertThat(PMessageVariant.EXCEPTION.toString(), is("exception"));
        assertThat(PMessageVariant.variantForName("struct"), is(PMessageVariant.STRUCT));
        try {
            PMessageVariant.variantForName("foo");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Unknown message variant 'foo'"));
        }
    }
}
