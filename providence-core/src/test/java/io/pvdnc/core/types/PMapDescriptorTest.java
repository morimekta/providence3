package io.pvdnc.core.types;

import net.morimekta.collect.MapBuilder;
import net.morimekta.collect.UnmodifiableMap;
import net.morimekta.collect.UnmodifiableSortedMap;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class PMapDescriptorTest {
    @Test
    public void testSet() {
        PMapDescriptor<?,?> map = PMapDescriptor.provider(PType.STRING.provider(), PType.INT.provider()).get();
        assertThat(map.getKeyType(), is(PType.STRING));
        assertThat(map.getValueType(), is(PType.INT));
        assertThat(map.isSorted(), is(false));
        assertThat(map.builder(5), is(instanceOf(MapBuilder.class)));
        assertThat(map.builder(5), is(instanceOf(UnmodifiableMap.Builder.class)));
        assertThat(map.getType(), is(PType.MAP));
        assertThat(map.getDefaultValue(), is(Optional.of(mapOf())));
        assertThat(map.getQualifiedName("foo"), is("map<string,int>"));
        assertThat(map.getTypeName(), is("map<string,int>"));

        PMapDescriptor<?,?> m2 = PMapDescriptor.provider(PType.STRING.provider(), PType.INT.provider()).get();
        PMapDescriptor<?,?> m3 = PMapDescriptor.provider(PType.INT.provider(), PType.INT.provider()).get();
        PMapDescriptor<?,?> m4 = PMapDescriptor.provider(PType.STRING.provider(), PType.STRING.provider()).get();

        assertThat(map.hashCode(), is(map.hashCode()));
        assertThat(map.hashCode(), is(m2.hashCode()));
        assertThat(map.hashCode(), is(not(m3.hashCode())));
        assertThat(map.hashCode(), is(not(m4.hashCode())));

        assertThat(map, is(map));
        assertThat(map, is(m2));
        assertThat(map, is(not(m3)));
        assertThat(map, is(not(m4)));
        assertThat(map, is(not(new Object())));
    }

    @Test
    public void testSortedMap() {
        PMapDescriptor<?,?> map = PMapDescriptor.sortedProvider(PType.STRING.provider(), PType.INT.provider()).get();
        assertThat(map.getKeyType(), is(PType.STRING));
        assertThat(map.getValueType(), is(PType.INT));
        assertThat(map.isSorted(), is(true));
        assertThat(map.builder(5), is(instanceOf(MapBuilder.class)));
        assertThat(map.builder(5), is(instanceOf(UnmodifiableSortedMap.Builder.class)));
        assertThat(map.getType(), is(PType.MAP));
        assertThat(map.getDefaultValue(), is(Optional.of(mapOf())));
        assertThat(map.getQualifiedName("foo"), is("map<string,int>"));
        assertThat(map.getTypeName(), is("map<string,int>"));

        PMapDescriptor<?,?> m2 = PMapDescriptor.sortedProvider(PType.STRING.provider(), PType.INT.provider()).get();
        PMapDescriptor<?,?> m3 = PMapDescriptor.sortedProvider(PType.INT.provider(), PType.INT.provider()).get();
        PMapDescriptor<?,?> m4 = PMapDescriptor.sortedProvider(PType.STRING.provider(), PType.STRING.provider()).get();

        assertThat(map.hashCode(), is(map.hashCode()));
        assertThat(map.hashCode(), is(m2.hashCode()));
        assertThat(map.hashCode(), is(not(m3.hashCode())));
        assertThat(map.hashCode(), is(not(m4.hashCode())));

        assertThat(map, is(map));
        assertThat(map, is(m2));
        assertThat(map, is(not(m3)));
        assertThat(map, is(not(m4)));
        assertThat(map, is(not(new Object())));

        PMapDescriptor<?,?> m5 = PMapDescriptor.provider(PType.STRING.provider(), PType.INT.provider()).get();
        assertThat(map.hashCode(), is(not(m5.hashCode())));
        assertThat(map, is(not(m5)));
    }
}
