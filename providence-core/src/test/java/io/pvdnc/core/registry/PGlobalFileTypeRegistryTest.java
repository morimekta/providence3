package io.pvdnc.core.registry;

import io.pvdnc.core.types.PDeclarationType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Path;
import java.util.Optional;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static io.pvdnc.core.reflect.TestReflectionUtil.kConst;
import static io.pvdnc.core.reflect.TestReflectionUtil.kFibonacci;
import static io.pvdnc.core.reflect.TestReflectionUtil.kFieldTypes;
import static io.pvdnc.core.registry.PTypeReference.ref;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.fail;

public class PGlobalFileTypeRegistryTest {
    @TempDir
    public Path tempDir;

    @Test
    public void testGlobalRegistry() {
        PGlobalFileTypeRegistry registry = new PGlobalFileTypeRegistry();

        assertThat(registry.getRegistryForPath(tempDir.resolve("test.json")), is(Optional.empty()));
        assertThat(registry.getRegistriesForNamespace("test"), is(listOf()));

        PNamespaceFileTypeRegistry nsr1 = registry.computeRegistryForPath(tempDir.resolve("test.json"), "test");
        PNamespaceFileTypeRegistry nsr2 = registry.computeRegistryForPath(tempDir.resolve("foo/test.json"), "test");
        PNamespaceFileTypeRegistry pvd = registry.computeRegistryForPath(tempDir.resolve("providence.pvd"), "providence");

        assertThat(registry.getRegistryForPath(tempDir.resolve("test.json")).orElse(null), is(sameInstance(nsr1)));
        assertThat(registry.getRegistryForPath(tempDir.resolve("foo/test.json")).orElse(null), is(sameInstance(nsr2)));
        assertThat(registry.getRegistryForPath(tempDir.resolve("providence.pvd")).orElse(null), is(sameInstance(pvd)));

        assertThat(registry.getRegistriesForNamespace("test"), containsInAnyOrder(sameInstance(nsr1), sameInstance(nsr2)));
        assertThat(registry.getRegistriesForNamespace("providence"), containsInAnyOrder(sameInstance(pvd)));

        nsr2.register(kFieldTypes);
        nsr2.register(kFibonacci);
        nsr2.register(kConst);
        nsr2.complete();

        assertThat(registry.descriptor(ref("test", "FieldTypes")), is(kFieldTypes));
        assertThat(registry.constant(ref("test", "kConst")), is("test"));
        assertThat(registry.declaredTypeReferences(), is(setOf(ref("test", "FieldTypes"),
                                                               ref("test", "Fibonacci"),
                                                               ref("test", "kConst"))));
        assertThat(registry.declaredTypeReferences(PDeclarationType.CONST), is(setOf(ref("test", "kConst"))));
        assertThat(registry.declaredDescriptors(), containsInAnyOrder(kFibonacci, kFieldTypes, kConst));
        assertThat(registry.declaredDescriptors(PDeclarationType.ENUM), containsInAnyOrder(kFibonacci));
    }

    @Test
    public void testGlobalRegistry_BadArgs() {
        PGlobalFileTypeRegistry registry = new PGlobalFileTypeRegistry();

        try {
            registry.descriptor(ref("FieldTypes"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Requesting local type FieldTypes in global registry"));
        }
        try {
            registry.constant(ref("kConst"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Requesting local const kConst in global registry"));
        }
        try {
            registry.resolve(ref("FieldTypes"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Requesting local type FieldTypes in global registry"));
        }

        try {
            registry.descriptor(ref("test", "FieldTypes"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Unknown namespace 'test' for type test.FieldTypes"));
        }
        try {
            registry.constant(ref("test", "kConst"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Unknown namespace 'test' for const test.kConst"));
        }

        try {
            registry.resolve(ref("test", "FieldTypes"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Unknown namespace 'test' for type test.FieldTypes"));
        }

        registry.computeRegistryForPath(tempDir.resolve("test.pvd"), "test");

        try {
            registry.descriptor(ref("test", "FieldTypes"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No type FieldTypes in namespace test"));
        }
        try {
            registry.constant(ref("test", "kConst"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No const kConst in namespace test"));
        }
        try {
            registry.resolve(ref("test", "FieldTypes"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No type FieldTypes in namespace test"));
        }
    }
}
