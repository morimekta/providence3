package io.pvdnc.core.registry;

import io.pvdnc.core.test.TestMessage;
import io.pvdnc.core.types.PDeclarationType;
import io.pvdnc.core.property.PPropertyMap;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Path;

import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static io.pvdnc.core.property.PJsonProperties.JSON_NAME;
import static io.pvdnc.core.reflect.TestReflectionUtil.kConst;
import static io.pvdnc.core.reflect.TestReflectionUtil.kFibonacci;
import static io.pvdnc.core.reflect.TestReflectionUtil.kFieldTypes;
import static io.pvdnc.core.registry.PTypeReference.ref;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.fail;

public class PNamespaceTypeRegistryTest {
    @TempDir
    public Path tempDir;

    @Test
    public void testRegistry() {
        PNamespaceFileTypeRegistry nsr1 = new PNamespaceFileTypeRegistry(tempDir.resolve("foo/test.json"), "test");
        PNamespaceFileTypeRegistry nsr2 = new PNamespaceFileTypeRegistry(tempDir.resolve("foo/test.json"), "test");
        PNamespaceFileTypeRegistry pvd = new PNamespaceFileTypeRegistry(tempDir.resolve("providence.pvd"), "providence");

        assertThat(nsr1, CoreMatchers.is(nsr2));

        nsr2.register(kFieldTypes);
        nsr2.register(kFibonacci);
        nsr2.register(kConst);
        assertThat(nsr2.isBuilding(), is(true));
        assertThat(nsr2.getPath(), is(tempDir.resolve("foo/test.json")));
        assertThat(nsr2.getNamespace(), is("test"));
        nsr2.complete();
        assertThat(nsr2.isBuilding(), is(false));

        pvd.putInclude("test", nsr2);
        assertThat(pvd.hasProperty(JSON_NAME), is(false));
        pvd.setProperties(PPropertyMap.of(JSON_NAME, "nope"));
        assertThat(pvd.hasProperty(JSON_NAME), is(true));
        assertThat(pvd.getProperty(JSON_NAME), is("nope"));
        assertThat(pvd.getUncheckedProperties(), is(mapOf("json.name", "nope")));
        assertThat(pvd.toString(), is("PNamespaceTypeRegistry{path=" + tempDir + "/providence.pvd, namespace='providence'}"));
        assertThat(pvd, CoreMatchers.is(pvd));
        assertThat(pvd, is(CoreMatchers.not(nsr2)));
        assertThat(pvd, is(not(new Object())));

        assertThat(pvd.descriptor(ref("test", "FieldTypes")), is(kFieldTypes));
        assertThat(pvd.constant(ref("test", "kConst")), is("test"));

        assertThat(pvd.getIncludes(), is(mapOf("test", nsr2)));
        assertThat(pvd.declaredTypeReferences(), is(setOf()));
        assertThat(pvd.declaredTypeReferences(PDeclarationType.CONST), is(setOf()));

        assertThat(nsr2.declaredTypeReferences(), is(setOf(ref("FieldTypes"), ref("Fibonacci"), ref("kConst"))));
        assertThat(nsr2.declaredTypeReferences(PDeclarationType.CONST), is(setOf(ref("kConst"))));
        assertThat(nsr2.declaredDescriptors(), containsInAnyOrder(kFibonacci, kFieldTypes, kConst));
        assertThat(nsr2.declaredDescriptors(PDeclarationType.ENUM), containsInAnyOrder(kFibonacci));
    }

    @Test
    public void testRegistry_BadRequests() {
        PNamespaceFileTypeRegistry nsr = new PNamespaceFileTypeRegistry(tempDir.resolve("foo/test.json"), "test");
        PNamespaceFileTypeRegistry pvd = new PNamespaceFileTypeRegistry(tempDir.resolve("providence.pvd"), "providence");
        pvd.register(TestMessage.kDescriptor);

        try {
            pvd.descriptor(ref("test", "FieldTypes"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No such include 'test' in providence.pvd"));
        }
        try {
            pvd.constant(ref("test", "kConst"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No such include 'test' in providence.pvd"));
        }
        try {
            pvd.constant(ref("TestMessage"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("The message 'TestMessage' is not a const in providence.pvd"));
        }

        pvd.putInclude("test", nsr);
        try {
            pvd.descriptor(ref("test", "FieldTypes"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No such type 'FieldTypes' in test.json"));
        }
        try {
            pvd.constant(ref("test", "kConst"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No such const 'kConst' in test.json"));
        }
    }
}
