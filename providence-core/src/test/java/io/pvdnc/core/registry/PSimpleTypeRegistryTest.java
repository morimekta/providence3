package io.pvdnc.core.registry;

import io.pvdnc.core.impl.Empty;
import io.pvdnc.core.test.TestEnum;
import io.pvdnc.core.test.TestMessage;
import io.pvdnc.core.types.PConstDescriptor;
import io.pvdnc.core.types.PDeclarationType;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PType;
import io.pvdnc.core.types.PTypeDefDescriptor;
import io.pvdnc.core.reflect.CServiceDescriptor;
import io.pvdnc.core.reflect.TestReflectionUtil;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;

import static net.morimekta.collect.UnmodifiableSet.setOf;
import static io.pvdnc.core.reflect.TestReflectionUtil.kFibonacci;
import static io.pvdnc.core.reflect.TestReflectionUtil.kFieldTypes;
import static io.pvdnc.core.reflect.TestReflectionUtil.kTypeDef;
import static io.pvdnc.core.registry.PTypeReference.ref;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.fail;

public class PSimpleTypeRegistryTest {
    @Test
    public void testRegistry() {
        PConstDescriptor kTest = new PConstDescriptor("test2", "kTest", PType.STRING.provider(), () -> "test");
        CServiceDescriptor kService = new CServiceDescriptor.Builder("test2", "TestService")
                .method("test", kFieldTypes, kFieldTypes)
                .build();

        PSimpleTypeRegistry registry = new PSimpleTypeRegistry();
        registry.registerRecursively(TestMessage.kDescriptor);
        registry.registerRecursively(TestMessage.kDescriptor);
        registry.register(TestMessage.kDescriptor);
        registry.registerRecursively(kTest);
        assertThat(registry.descriptor(ref("test", "TestMessage")), is(TestMessage.kDescriptor));
        assertThat(registry.descriptor(ref("test", "TestEnum")), CoreMatchers.is(TestEnum.kDescriptor));
        assertThat(registry.constant(ref("test2", "kTest")), is("test"));

        assertThat(registry.declaredTypeReferences(), is(setOf(ref("test", "TestMessage"),
                                                               ref("test", "TestEnum"),
                                                               ref("test2", "kTest"))));
        assertThat(registry.declaredTypeReferences(PDeclarationType.CONST), is(setOf(ref("test2", "kTest"))));

        registry.registerRecursively(TestReflectionUtil.kTypeDef);
        registry.registerRecursively(TestReflectionUtil.kTypeDef);
        registry.registerRecursively(kService);
        assertThat(registry.descriptor(ref("providence", "FieldTypes")), is(TestReflectionUtil.kFieldTypes));
        assertThat(registry.descriptor(ref("providence", "Fibonacci")), is(TestReflectionUtil.kFibonacci));
        assertThat(registry.descriptor(ref("providence", "StringList")), is(instanceOf(PTypeDefDescriptor.class)));
        assertThat(registry.resolve(ref("providence", "StringList")), is(instanceOf(PListDescriptor.class)));
        assertThat(registry.declaredDescriptors(), containsInAnyOrder(kTypeDef, kFieldTypes, kFibonacci, kTest, kService,
                                                                      TestMessage.kDescriptor, TestEnum.kDescriptor, Empty.kDescriptor));
        assertThat(registry.declaredDescriptors(PDeclarationType.ENUM), containsInAnyOrder(kFibonacci, TestEnum.kDescriptor));
    }

    @Test
    public void testRegistry_BadArgs() {
        PSimpleTypeRegistry registry = new PSimpleTypeRegistry();
        registry.registerRecursively(TestMessage.kDescriptor);
        registry.registerRecursively(new PConstDescriptor("test2", "kTest", PType.STRING.provider(), () -> "test"));

        try {
            registry.constant(ref("kNameLess"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Requesting local const kNameLess in simple registry"));
        }
        try {
            registry.constant(ref("foo", "kNameLess"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Unknown namespace for const: foo.kNameLess"));
        }
        try {
            registry.constant(ref("test", "kNameLess"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No such const: test.kNameLess"));
        }
        try {
            registry.constant(ref("test", "TestMessage"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("The message test.TestMessage is not a constant."));
        }

        try {
            registry.descriptor(ref("TypeLess"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Requesting local type TypeLess in simple registry"));
        }
        try {
            registry.descriptor(ref("foo", "TypeLess"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Unknown namespace for type: foo.TypeLess"));
        }
        try {
            registry.descriptor(ref("test", "TypeLess"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No such type: test.TypeLess"));
        }
    }
}
