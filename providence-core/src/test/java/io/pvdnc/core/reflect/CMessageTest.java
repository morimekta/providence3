package io.pvdnc.core.reflect;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.PMessageBuilder;
import io.pvdnc.core.PUnion;
import io.pvdnc.core.impl.Empty;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.test.TestMessage;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PFieldRequirement;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PMessageVariant;
import io.pvdnc.core.types.PType;
import io.pvdnc.other.core.test.OtherMessage;
import net.morimekta.collect.util.Binary;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static io.pvdnc.core.property.PDefaultProperties.DEPRECATED;
import static io.pvdnc.core.property.PDefaultProperties.MESSAGE_VARIANT;
import static io.pvdnc.core.types.PMessageDescriptor.getMessageDescriptor;
import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.jupiter.api.Assertions.fail;

public class CMessageTest {
    @Test
    public void testCMessage_Empty() {
        PMessageBuilder builder = TestReflectionUtil.kFieldTypes.newBuilder();
        assertThat(builder.$descriptor(), is(TestReflectionUtil.kFieldTypes));

        assertThat(builder.has(1), is(false));
        assertThat(builder.has(2), is(false));
        assertThat(builder.has(-1), is(false));
        assertThat(builder.has(3), is(false));
        assertThat(builder.has(-2), is(false));
        assertThat(builder.has(-3), is(false));
        assertThat(builder.has(4), is(false));
        assertThat(builder.has(-4), is(false));
        assertThat(builder.has(-5), is(false));
        assertThat(builder.has(-6), is(false));
        assertThat(builder.has(5), is(false));
        assertThat(builder.has(-7), is(false));
        assertThat(builder.has(-8), is(false));
        assertThat(builder.has(-9), is(false));
        assertThat(builder.has(-10), is(false));

        assertThat(builder.get(1), is(nullValue()));
        assertThat(builder.get(2), is(false));
        assertThat(builder.get(-1), is((byte) 0));
        assertThat(builder.get(3), is((short) 0));
        assertThat(builder.get(-2), is(0));
        assertThat(builder.get(-3), is(0L));
        assertThat(builder.get(4), is(0.f));
        assertThat(builder.get(-4), is(0.0));
        assertThat(builder.get(-5), is(""));
        assertThat(builder.get(-6), is(Binary.fromBase64("")));
        assertThat(builder.get(5), is(nullValue()));
        assertThat(builder.get(-7), is(Empty.empty()));
        assertThat(builder.get(-8), is(listOf()));
        assertThat(builder.get(-9), is(setOf()));
        assertThat(builder.get(-10), is(mapOf()));

        PMessage message = builder.build();
        assertThat(message.$descriptor(), is(TestReflectionUtil.kFieldTypes));

        assertThat(message.has(1), is(false));
        assertThat(message.has(2), is(false));
        assertThat(message.has(-1), is(false));
        assertThat(message.has(3), is(false));
        assertThat(message.has(-2), is(false));
        assertThat(message.has(-3), is(false));
        assertThat(message.has(4), is(false));
        assertThat(message.has(-4), is(false));
        assertThat(message.has(-5), is(false));
        assertThat(message.has(-6), is(false));
        assertThat(message.has(5), is(false));
        assertThat(message.has(-7), is(false));
        assertThat(message.has(-8), is(false));
        assertThat(message.has(-9), is(false));
        assertThat(message.has(-10), is(false));

        assertThat(message.get(1), is(nullValue()));
        assertThat(message.get(2), is(false));
        assertThat(message.get(-1), is((byte) 0));
        assertThat(message.get(3), is((short) 0));
        assertThat(message.get(-2), is(0));
        assertThat(message.get(-3), is(0L));
        assertThat(message.get(4), is(0.f));
        assertThat(message.get(-4), is(0.0));
        assertThat(message.get(-5), is(""));
        assertThat(message.get(-6), is(Binary.fromBase64("")));
        assertThat(message.get(5), is(nullValue()));
        assertThat(message.get(-7), is(Empty.empty()));
        assertThat(message.get(-8), is(listOf()));
        assertThat(message.get(-9), is(setOf()));
        assertThat(message.get(-10), is(mapOf()));

        PMessageBuilder builder2 = message.$descriptor().newBuilder();
        builder2.mergeFrom(message);
        assertThat(builder2.build(), is(message));

        assertThat(message.toString(),
                   is("providence.FieldTypes{}"));
    }

    @Test
    public void testCMessage_Simple() {
        PMessageBuilder builder = TestReflectionUtil.kFieldTypes.newBuilder();
        assertThat(builder.$descriptor(), is(TestReflectionUtil.kFieldTypes));

        builder.set(1, true);
        builder.set(2, false);
        builder.set(-1, (byte) 42);
        builder.set(3, (short) 420);
        builder.set(-2, 4200);
        builder.set(-3, 42000L);
        builder.set(4, 42.42f);
        builder.set(-4, 42.42d);
        builder.set(-5, "foo");
        builder.set(-6, Binary.fromBase64("bar"));
        builder.set(5, TestReflectionUtil.kFibonacci.findById(55));
        builder.set(-7, Empty.empty());
        builder.set(-8, listOf());
        builder.set(-9, setOf());
        builder.set(-10, mapOf());

        assertThat(builder.has(1), is(true));
        assertThat(builder.has(2), is(true));
        assertThat(builder.has(-1), is(true));
        assertThat(builder.has(3), is(true));
        assertThat(builder.has(-2), is(true));
        assertThat(builder.has(-3), is(true));
        assertThat(builder.has(4), is(true));
        assertThat(builder.has(-4), is(true));
        assertThat(builder.has(-5), is(true));
        assertThat(builder.has(-6), is(true));
        assertThat(builder.has(5), is(true));
        assertThat(builder.has(-7), is(true));
        assertThat(builder.has(-8), is(true));
        assertThat(builder.has(-9), is(true));
        assertThat(builder.has(-10), is(true));

        assertThat(builder.get(1), is(true));
        assertThat(builder.get(2), is(false));
        assertThat(builder.get(-1), is((byte) 42));
        assertThat(builder.get(3), is((short) 420));
        assertThat(builder.get(-2), is(4200));
        assertThat(builder.get(-3), is(42000L));
        assertThat(builder.get(4), is(42.42f));
        assertThat(builder.get(-4), is(42.42d));
        assertThat(builder.get(-5), is("foo"));
        assertThat(builder.get(-6), is(Binary.fromBase64("bar")));
        assertThat(builder.get(5), is(TestReflectionUtil.kFibonacci.findByName("NINTH")));
        assertThat(builder.get(-7), is(Empty.empty()));
        assertThat(builder.get(-8), is(listOf()));
        assertThat(builder.get(-9), is(setOf()));
        assertThat(builder.get(-10), is(mapOf()));

        assertThat(builder.optional(1), is(Optional.of(true)));
        assertThat(builder.optional(2), is(Optional.of(false)));
        assertThat(builder.optional(-1), is(Optional.of((byte) 42)));
        assertThat(builder.optional(3), is(Optional.of((short) 420)));
        assertThat(builder.optional(-2), is(Optional.of(4200)));
        assertThat(builder.optional(-3), is(Optional.of(42000L)));
        assertThat(builder.optional(4), is(Optional.of(42.42f)));
        assertThat(builder.optional(-4), is(Optional.of(42.42d)));
        assertThat(builder.optional(-5), is(Optional.of("foo")));
        assertThat(builder.optional(-6), is(Optional.of(Binary.fromBase64("bar"))));
        assertThat(builder.optional(5), is(Optional.of(TestReflectionUtil.kFibonacci.findByName("NINTH"))));
        assertThat(builder.optional(-7), is(Optional.of(Empty.empty())));
        assertThat(builder.optional(-8), is(Optional.of(listOf())));
        assertThat(builder.optional(-9), is(Optional.of(setOf())));
        assertThat(builder.optional(-10), is(Optional.of(mapOf())));

        PMessage message = builder.build();
        assertThat(message.$descriptor(), is(TestReflectionUtil.kFieldTypes));

        assertThat(message.has(1), is(true));
        assertThat(message.has(2), is(true));
        assertThat(message.has(-1), is(true));
        assertThat(message.has(3), is(true));
        assertThat(message.has(-2), is(true));
        assertThat(message.has(-3), is(true));
        assertThat(message.has(4), is(true));
        assertThat(message.has(-4), is(true));
        assertThat(message.has(-5), is(true));
        assertThat(message.has(-6), is(true));
        assertThat(message.has(5), is(true));
        assertThat(message.has(-7), is(true));
        assertThat(message.has(-8), is(true));
        assertThat(message.has(-9), is(true));
        assertThat(message.has(-10), is(true));

        assertThat(message.get(1), is(true));
        assertThat(message.get(2), is(false));
        assertThat(message.get(-1), is((byte) 42));
        assertThat(message.get(3), is((short) 420));
        assertThat(message.get(-2), is(4200));
        assertThat(message.get(-3), is(42000L));
        assertThat(message.get(4), is(42.42f));
        assertThat(message.get(-4), is(42.42d));
        assertThat(message.get(-5), is("foo"));
        assertThat(message.get(-6), is(Binary.fromBase64("bar")));
        assertThat(message.get(5), is(TestReflectionUtil.kFibonacci.findByName("NINTH")));
        assertThat(message.get(-7), is(Empty.empty()));
        assertThat(message.get(-8), is(listOf()));
        assertThat(message.get(-9), is(setOf()));
        assertThat(message.get(-10), is(mapOf()));

        assertThat(message.optional(1), is(Optional.of(true)));
        assertThat(message.optional(2), is(Optional.of(false)));
        assertThat(message.optional(-1), is(Optional.of((byte) 42)));
        assertThat(message.optional(3), is(Optional.of((short) 420)));
        assertThat(message.optional(-2), is(Optional.of(4200)));
        assertThat(message.optional(-3), is(Optional.of(42000L)));
        assertThat(message.optional(4), is(Optional.of(42.42f)));
        assertThat(message.optional(-4), is(Optional.of(42.42d)));
        assertThat(message.optional(-5), is(Optional.of("foo")));
        assertThat(message.optional(-6), is(Optional.of(Binary.fromBase64("bar"))));
        assertThat(message.optional(5), is(Optional.of(TestReflectionUtil.kFibonacci.findByName("NINTH"))));
        assertThat(message.optional(-7), is(Optional.of(Empty.empty())));
        assertThat(message.optional(-8), is(Optional.of(listOf())));
        assertThat(message.optional(-9), is(Optional.of(setOf())));
        assertThat(message.optional(-10), is(Optional.of(mapOf())));
        assertThat(message.optional(22), is(Optional.empty()));

        builder = message.mutate();
        builder.clear(2);
        builder.set(-3, null);
        builder.set(-5, "bar");
        try {
            builder.set(22, "bar");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No field id 22 on providence.FieldTypes"));
        }
        try {
            builder.clear(22);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No field id 22 on providence.FieldTypes"));
        }

        assertThat(builder.mutate(), is(not(sameInstance(builder))));
        assertThat(builder.has(1), is(true));
        assertThat(builder.has(2), is(false));
        assertThat(builder.has(-3), is(false));
        assertThat(builder.has(-5), is(true));

        assertThat(builder.get(1), is(true));
        assertThat(builder.get(2), is(false));
        assertThat(builder.get(-3), is(0L));
        assertThat(builder.get(-5), is("bar"));
        assertThat(builder.get(22), is(nullValue()));

        assertThat(builder.mutable(-8), is(instanceOf(ArrayList.class)));
        ArrayList<Short> tmp = builder.mutable(-8);
        tmp.add((short) 42);
        assertThat(builder.get(-8), is(List.of((short) 42)));

        assertThat(builder.modified(1), is(false));
        assertThat(builder.modified(2), is(true));
        assertThat(builder.modified(-3), is(true));
        assertThat(builder.modified(-5), is(true));
        assertThat(builder.modified(22), is(false));

        PMessage other = builder.build();
        assertThat(message, is(message));
        assertThat(message, is(not(new Object())));
        assertThat(other, is(not(message)));
        assertThat(message.hashCode(), is(message.hashCode()));
        assertThat(other.hashCode(), is(not(message.hashCode())));

        PMessageBuilder builder2 = message.$descriptor().newBuilder();
        builder2.mergeFrom(message);
        assertThat(builder2.build(), is(message));
        try {
            builder2.mergeFrom(Empty.empty());
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("providence.Empty is not a supertype of providence.FieldTypes"));
        }

        assertThat(message.toString(),
                   is("providence.FieldTypes{" +
                      "v=true, b=false, bt=42, s=420, i=4200, l=42000, f=42.42, d=42.42, str='foo', " +
                      "bin=binary(6daa), e=NINTH, msg={}, lst=[], set={}, map={}}"));
    }

    @Test
    public void testUnion() {
        CMessageDescriptor.Builder unionB = CMessageDescriptor.builder("providence", "Union");
        unionB.field("fields", TestReflectionUtil.kFieldTypes).id(1);
        unionB.field("fib", TestReflectionUtil.kFibonacci).id(2);
        unionB.field("value", TestReflectionUtil.kValue).id(3);
        unionB.property("message.variant", "UNION");
        CMessageDescriptor unionD = unionB.build();

        assertThat(unionD.getProperty(MESSAGE_VARIANT), CoreMatchers.is(PMessageVariant.UNION));

        CMessageBuilder builder = unionD.newBuilder();
        builder.set(3, TestReflectionUtil.kValue.findById(1));
        builder.set(2, TestReflectionUtil.kFibonacci.findById(8));

        PMessage message = builder.build();
        assertThat(message, is(instanceOf(CUnion.class)));
        PUnion union = (CUnion) message;
        assertThat(union.unionFieldIsSet(), is(true));
        assertThat(union.unionFieldId(), is(2));
        assertThat(union.unionField(), is(unionD.fieldForId(2)));
        assertThat(union.hashCode(), is(union.hashCode()));
        assertThat(union, is(union));
        assertThat(union, is(not(new Object())));

        assertThat(union.has(1), is(false));
        assertThat(union.has(2), is(true));
        assertThat(union.get(2), is(TestReflectionUtil.kFibonacci.findById(8)));
        assertThat(union.get(3), is(TestReflectionUtil.kValue.findById(1)));
        assertThat(union.optional(1), is(Optional.empty()));
        assertThat(union.optional(2), is(Optional.of(TestReflectionUtil.kFibonacci.findById(8))));

        builder = (CMessageBuilder) union.mutate();
        assertThat(builder.$descriptor(), CoreMatchers.is(unionD));
        builder.set(2, TestReflectionUtil.kFibonacci.findById(55));

        message = builder.build();
        assertThat(message, is(not(union)));
        assertThat(message.toString(), is("providence.Union{fib=NINTH}"));

        PUnion empty = (PUnion) unionD.newBuilder().build();
        assertThat(empty.unionFieldIsSet(), is(false));
        assertThat(empty.unionFieldId(), is(Integer.MIN_VALUE));
        try {
            empty.unionField();
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("No union field set on providence.Union"));
        }

        PUnion bad = new CUnion(unionD, 55, "");
        try {
            bad.unionField();
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("No field for union field id 55 on providence.Union"));
        }
    }

    @Test
    public void testException() {
        CMessageDescriptor.Builder exB = CMessageDescriptor.builder("providence", "Exception");
        exB.field("fields", TestReflectionUtil.kFieldTypes).id(1);
        exB.field("fib", TestReflectionUtil.kFibonacci).id(2);
        exB.field("value", PType.STRING).id(3);  // should become the message.
        exB.property("message.variant", "EXCEPTION");
        CMessageDescriptor exD = exB.build();

        CMessageBuilder builder = exD.newBuilder();
        builder.set(3, "msg");
        builder.set(2, TestReflectionUtil.kFibonacci.findById(34));

        PMessage message = builder.build();
        assertThat(message, is(instanceOf(CException.class)));
        CException exception = (CException) message;
        assertThat(exception.$descriptor(), CoreMatchers.is(exD));
        assertThat(exception.getMessage(), is("msg"));
        assertThat(exception.initCause(new IOException("foo")), is(sameInstance(exception)));
        assertThat(exception.getCause(), is(instanceOf(IOException.class)));

        assertThat(exception.has(1), is(false));
        assertThat(exception.has(2), is(true));
        assertThat(exception.has(3), is(true));
        assertThat(exception.get(1), is(TestReflectionUtil.kFieldTypes.getDefaultValue().orElse(null)));
        assertThat(exception.get(2), is(TestReflectionUtil.kFibonacci.findByName("EIGHTH")));
        assertThat(exception.get(3), is("msg"));
        assertThat(exception.optional(1), is(Optional.empty()));
        assertThat(exception.optional(3), is(Optional.of("msg")));

        builder = exception.mutate();
        builder.clear(3);
        message = builder.build();

        assertThat(exception, CoreMatchers.is(exception));
        assertThat(exception, is(not(message)));
        assertThat(exception, is(not(new Object())));
        assertThat(exception.hashCode(), is(exception.hashCode()));
        assertThat(exception.hashCode(), is(not(message.hashCode())));

        assertThat(message.toString(), is("providence.Exception{fib=EIGHTH}"));
    }

    @Test
    public void testDescriptor() {
        assertThat(TestReflectionUtil.kFieldTypes.allFields(), hasSize(15));
        assertThat(TestReflectionUtil.kFieldTypes.declaredFields(), hasSize(15));
        assertThat(TestReflectionUtil.kFieldTypes.getNamespace(), is("providence"));
        assertThat(TestReflectionUtil.kFieldTypes.getSimpleName(), is("FieldTypes"));
        assertThat(TestReflectionUtil.kFieldTypes.getTypeName(), is("providence.FieldTypes"));

        CMessageDescriptor.Builder iFaceB = CMessageDescriptor.builder("test", "iFace");
        iFaceB.field("str", PType.STRING).properties(PPropertyMap.of("foo", "bar"));
        iFaceB.field("bar", PType.STRING).property("foo", "bar");
        iFaceB.property(MESSAGE_VARIANT, PMessageVariant.INTERFACE);
        CMessageDescriptor iFace = iFaceB.build();

        assertThat(iFace.fieldForId(-1).getUncheckedProperties(), is(iFace.fieldForId(-2).getUncheckedProperties()));

        CMessageDescriptor.Builder extendedB = CMessageDescriptor.builder("test", "Extended");
        extendedB.extending(() -> TestReflectionUtil.kFieldTypes);
        extendedB.implementing(() -> iFace);
        extendedB.field("foo", PType.STRING).id(20).property(DEPRECATED, "yes");
        extendedB.field("bar", PType.STRING).id(21).defaultValue(() -> "no");
        CMessageDescriptor extended = extendedB.build();

        assertThat(extended.allFields(), hasSize(17));
        assertThat(extended.declaredFields(), hasSize(2));
        assertThat(extended.getImplementing(), contains(iFace));
        assertThat(extended.getExtending(), is(Optional.of(TestReflectionUtil.kFieldTypes)));

        assertThat(extended.fieldForId(1), is(notNullValue()));
        assertThat(extended.fieldForId(20), is(notNullValue()));
        assertThat(extended.fieldForId(20).getProperty(DEPRECATED), is("yes"));
        assertThat(extended.findFieldById(40), is(nullValue()));
        assertThat(extended.fieldForName("foo"), is(notNullValue()));
        assertThat(extended.fieldForName("s"), is(notNullValue()));
        assertThat(extended.fieldForName("s").getDefaultValue(), is(Optional.empty()));
        assertThat(extended.fieldForName("bar"), is(notNullValue()));
        assertThat(extended.fieldForName("bar").getDefaultValue(), is(Optional.of("no")));
        assertThat(extended.findFieldByName("baz"), is(nullValue()));

        assertThat(extended.getDefaultValue().orElse(null), is(notNullValue()));
        assertThat(extended.hashCode(), is(not(iFace.hashCode())));
        assertThat(extended.hashCode(), is(extended.hashCode()));
        assertThat(extended.toString(), is("test.Extended"));

        assertThat(extended.hasProperty(DEPRECATED), is(false));
        assertThat(extended.getProperty(DEPRECATED), is(nullValue()));

        assertThat(extended.contains(iFace), is(true));
        assertThat(extended.contains(TestReflectionUtil.kFieldTypes), is(true));
        assertThat(extended.contains(TestReflectionUtil.kSimple), is(false));

        try {
            extended.fieldForId(123);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No field for id 123 in test.Extended"));
        }
        try {
            extended.fieldForName(null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("name == null"));
        }
        try {
            extended.fieldForName("baz");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No field for name 'baz' in test.Extended"));
        }

        PField field = extended.findFieldById(5);
        assertThat(field, is(notNullValue()));
        assertThat(field.getId(), is(5));
        assertThat(field.getName(), is("e"));
        assertThat(field.getType(), is(PType.ENUM));
        assertThat(field.getRequirement(), CoreMatchers.is(PFieldRequirement.OPTIONAL));
        assertThat(field.getResolvedDescriptor(), is(TestReflectionUtil.kFibonacci));
        assertThat(field.getDeclaredDescriptor(), is(TestReflectionUtil.kFibonacci));
        assertThat(field.getOnMessageType(), is(TestReflectionUtil.kFieldTypes));
        assertThat(field.hasProperty(DEPRECATED), is(false));
        assertThat(field.getProperty(DEPRECATED), is(nullValue()));

        assertThat(field, is(field));
        assertThat(field, is(new PField(5, () -> TestReflectionUtil.kFibonacci, "e", null, PPropertyMap.empty(), null, () -> TestReflectionUtil.kFieldTypes)));
        assertThat(field, is(not(extended.fieldForId(3))));
        assertThat(field, is(not(new Object())));

        assertThat(field.toString(), is("5: providence.Fibonacci e"));
    }

    @Test
    public void testInterface() {
        CMessageDescriptor.Builder iFaceB = CMessageDescriptor.builder("test", "iFace");
        iFaceB.field("str", PType.STRING).properties(PPropertyMap.of("foo", "bar"));
        iFaceB.field("bar", PType.STRING).property("foo", "bar");
        iFaceB.properties(PPropertyMap.of(MESSAGE_VARIANT, PMessageVariant.INTERFACE));
        CMessageDescriptor iFace = iFaceB.build();

        try {
            iFace.newBuilder();
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Interfaces do not support builders"));
        }
    }

    @Test
    public void testBuild_duplicateID() {
        CMessageDescriptor.Builder msgB = CMessageDescriptor.builder("test", "Msg");
        msgB.field("foo", PType.STRING).id(1);
        msgB.field("bar", PType.INT).id(1);
        try {
            msgB.build();
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Duplicate field ID 1"));;
        }
    }
    @Test
    public void testBuild_duplicateName() {
        CMessageDescriptor.Builder msgB = CMessageDescriptor.builder("test", "Msg");
        msgB.field("foo", PType.STRING);
        msgB.field("foo", PType.INT);
        try {
            msgB.build();
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Duplicate field name foo"));;
        }
    }
    @Test
    public void testBuild_conflictingName() {
        CMessageDescriptor.Builder msgB = CMessageDescriptor.builder("test", "Msg");
        msgB.field("foo", PType.STRING);
        msgB.field("Foo", PType.INT);
        try {
            msgB.build();
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Conflicting field name foo: Foo <-> foo"));;
        }
    }

    public static class NotAMessage implements PMessage {
        @Override
        public boolean has(int field) {
            return false;
        }

        @Override
        public <T> T get(int field) {
            return null;
        }

        @Override
        public <T> Optional<T> optional(int field) {
            return Optional.empty();
        }

        @Override
        public PMessageBuilder mutate() {
            return null;
        }

        @Override
        public PMessageDescriptor<?> $descriptor() {
            return null;
        }
    }
    public static class NotAMessage2 extends NotAMessage {
        @SuppressWarnings("unused")
        private static final PMessageDescriptor<?> kDescriptor = TestMessage.kDescriptor;
    }
    public static class NotAMessage3 extends NotAMessage {
        public static final PEnumDescriptor kDescriptor = TestReflectionUtil.kFibonacci;
    }

    @Test
    public void testGetMessageDescriptor() {
        assertThat(getMessageDescriptor(TestMessage.class), is(sameInstance(TestMessage.kDescriptor)));
        assertThat(getMessageDescriptor(OtherMessage.class), is(sameInstance(OtherMessage.kDescriptor)));
        try {
            getMessageDescriptor(PMessage.class);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Base class of PMessage not a typed message"));
        }
        try {
            getMessageDescriptor(PMessageBuilder.class);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Base class of PMessage not a typed message"));
        }
        try {
            getMessageDescriptor(NotAMessage.class);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a typed message: NotAMessage"));
        }
        try {
            getMessageDescriptor(NotAMessage2.class);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a message descriptor property: NotAMessage2.kDescriptor: not public static"));
        }
        try {
            getMessageDescriptor(NotAMessage3.class);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a message descriptor property: NotAMessage3.kDescriptor"));
        }
        try {
            getMessageDescriptor(Integer.class);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a typed message: Integer"));
        }
    }
}
