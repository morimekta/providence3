package io.pvdnc.core.reflect;

import io.pvdnc.core.impl.Empty;
import io.pvdnc.core.types.PServiceMethod;
import io.pvdnc.core.types.PType;
import io.pvdnc.core.property.PDefaultProperties;
import io.pvdnc.core.property.PPropertyMap;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;

public class CServiceTest {
    @Test
    public void testDescriptor() {
        CServiceDescriptor base = CServiceDescriptor.newBuilder("test", "BaseService")
                                                    .method("fields",
                                                            Empty.kDescriptor,
                                                            TestReflectionUtil.kFieldTypes,
                                                            PPropertyMap.empty())
                                                    .build();

        CServiceDescriptor service = CServiceDescriptor.newBuilder("test", "Service")
                                                       .extending(base)
                                                       .method("moreFields",
                                                               TestReflectionUtil.kFieldTypes,
                                                               TestReflectionUtil.kFieldTypes,
                                                               PPropertyMap.of("foo", "bar"))
                                                       .properties(PPropertyMap.of("foo", "bar")).build();

        assertThat(service.getNamespace(), is("test"));
        assertThat(service.getSimpleName(), is("Service"));
        assertThat(service.getTypeName(), is("test.Service"));
        assertThat(service.getType(), CoreMatchers.is(PType.VOID));
        assertThat(service.getDefaultValue(), is(Optional.empty()));
        assertThat(service.findMethodForName("missing"), is(nullValue()));
        assertThat(service.getUncheckedProperties(), is(mapOf("foo", "bar")));

        PServiceMethod fields = service.findMethodForName("fields");
        assertThat(fields, is(notNullValue()));
        assertThat(fields.getName(), is("fields"));
        assertThat(fields.getRequestType(), CoreMatchers.is(TestReflectionUtil.kFieldTypes));
        assertThat(fields.getResponseType(), is(Empty.kDescriptor));
        assertThat(fields.getOnServiceType(), is(sameInstance(base)));
        assertThat(fields.hasProperty(PDefaultProperties.DEPRECATED), is(false));
        assertThat(fields.getProperty(PDefaultProperties.DEPRECATED), is(nullValue()));

        PServiceMethod moreFields = service.findMethodForName("moreFields");
        assertThat(moreFields, is(notNullValue()));
        assertThat(moreFields.getName(), is("moreFields"));
        assertThat(moreFields.getUncheckedProperties(), is(mapOf("foo", "bar")));
        assertThat(moreFields.getOnServiceType(), is(sameInstance(service)));

        assertThat(service.allMethods(), containsInAnyOrder(fields, moreFields));
        assertThat(service.declaredMethods(), contains(moreFields));;
    }
}
