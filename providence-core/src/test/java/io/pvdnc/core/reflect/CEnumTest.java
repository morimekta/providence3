package io.pvdnc.core.reflect;

import io.pvdnc.core.PEnum;
import io.pvdnc.core.test.TestEnum;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PType;
import io.pvdnc.core.property.PDefaultProperties;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.types.PEnumValue;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Optional;

import static io.pvdnc.core.reflect.TestReflectionUtil.kFibonacci;
import static io.pvdnc.core.reflect.TestReflectionUtil.kFieldTypes;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.fail;

public class CEnumTest {
    @Test
    public void testEnum() {
        CEnum first = kFibonacci.valueForId(1);
        assertThat(first, is(notNullValue()));
        assertThat(first.getName(), is("FIRST"));
        assertThat(first.getValue(), is(1));
        assertThat(first.$meta(), is(notNullValue()));
        assertThat(first.$meta().getName(), is("FIRST"));
        assertThat(first.$descriptor(), is(sameInstance(kFibonacci)));

        CEnum second = kFibonacci.valueForName("SECOND");
        assertThat(second, is(notNullValue()));

        assertThat(first, CoreMatchers.is(first));
        assertThat(first, is(CoreMatchers.not(second)));
        assertThat(first, is(not(new Object())));

        assertThat(first.toString(), is("FIRST"));
        assertThat(first.hashCode(), is(first.$meta().hashCode()));
        assertThat(first.hashCode(), is(not(second.hashCode())));
        assertThat(first.compareTo(second), is(-1));

        assertThat(first.$meta().getName(), is("FIRST"));
        assertThat(first.$meta().getValue(), is(1));
        assertThat(first.$meta().hasProperty(PDefaultProperties.DEPRECATED), is(false));
        assertThat(first.$meta().getUncheckedProperties(), is(Map.of()));
        assertThat(first.$meta().toString(), is("FIRST{1}"));
        assertThat(first.$meta().compareTo(second.$meta()), is(-1));
    }
    @Test
    public void testEnumDescriptor() {
        PEnum first = kFibonacci.valueForId(1);
        PEnum second = kFibonacci.valueForName("SECOND");

        assertThat(kFibonacci.getNamespace(), is("providence"));
        assertThat(kFibonacci.getSimpleName(), is("Fibonacci"));
        assertThat(kFibonacci.getTypeName(), is("providence.Fibonacci"));
        assertThat(kFibonacci.getQualifiedName("foo"), is("providence.Fibonacci"));
        assertThat(kFibonacci.getQualifiedName("providence"), is("Fibonacci"));
        assertThat(kFibonacci.getType(), CoreMatchers.is(PType.ENUM));
        assertThat(kFibonacci.toString(), is("providence.Fibonacci{FIRST=1,SECOND=2,THIRD=3,FOURTH=5,FIFTH=8,SIXTH=13,SEVENTH=21,EIGHTH=34,NINTH=55}"));

        assertThat(kFibonacci.allValues(), hasSize(9));
        assertThat(kFibonacci.findById(2), is(sameInstance(second)));
        assertThat(kFibonacci.findByName("FIRST"), is(sameInstance(first)));
        assertThat(kFibonacci.findByName("OTHER"), is(sameInstance(second)));
        assertThat(kFibonacci.findById(4), is(nullValue()));
        assertThat(kFibonacci.findByName("FOO"), is(nullValue()));
        assertThat(kFibonacci.getDefaultValue(), is(Optional.empty()));

        assertThat(kFibonacci.valueForId(3).getName(), is("THIRD"));
        assertThat(kFibonacci.valueForId(5).getName(), is("FOURTH"));
        assertThat(kFibonacci.valueForId(8).getName(), is("FIFTH"));
        assertThat(kFibonacci.valueForId(13).getName(), is("SIXTH"));
        assertThat(kFibonacci.valueForId(21).getName(), is("SEVENTH"));
        assertThat(kFibonacci.valueForId(34).getName(), is("EIGHTH"));
        assertThat(kFibonacci.valueForId(55).getName(), is("NINTH"));

        assertThat(kFibonacci.getProperty(PDefaultProperties.DEPRECATED), is("not really"));

        assertThat(kFibonacci, is(kFibonacci));
        assertThat(kFibonacci.hashCode(), is(kFibonacci.hashCode()));
        assertThat(kFibonacci, is(CoreMatchers.not(TestEnum.kDescriptor)));
        assertThat(kFibonacci.hashCode(), CoreMatchers.is(CoreMatchers.not(TestEnum.kDescriptor.hashCode())));
        assertThat(kFibonacci, is(not(new Object())));

        CEnumDescriptor.Builder builder = CEnumDescriptor.builder("providence", "Fibonacci");
        builder.value("FIRST");
        builder.properties(PPropertyMap.of(PDefaultProperties.DEPRECATED, "not really"));
        CEnumDescriptor fib = builder.build();

        assertThat(kFibonacci, is(CoreMatchers.not(fib)));

        builder.value("SECOND", PPropertyMap.of("enum.aliases", "OTHER"));
        builder.value("THIRD");
        builder.value("FOURTH", 5);
        builder.value("FIFTH", 8);
        builder.value("SIXTH", 13);
        builder.value("SEVENTH", 21, PPropertyMap.of("foo", "bar"));
        builder.value("EIGHTH", 34);
        builder.value("NINTH", 55);
        fib = builder.build();

        assertThat(kFibonacci, CoreMatchers.is(fib));
    }

    @Test
    public void testEnum_badArgs() {
        try {
            kFibonacci.valueForId(4);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No providence.Fibonacci value for id 4"));
        }
        try {
            kFibonacci.valueForName("FOO");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No providence.Fibonacci value for name 'FOO'"));
        }
    }

    @Test
    public void testCEnumBuilder_badArgs() {
        try {
            CEnumDescriptor.builder(null, "foo");
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("namespace == null"));
        }
        try {
            CEnumDescriptor.builder("foo", null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("enumName == null"));
        }
        CEnumDescriptor.Builder builder = CEnumDescriptor.builder("test", "Name");
        try {
            builder.value(null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("name == null"));
        }
        try {
            builder.value("");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty enum value name"));
        }
        try {
            builder.value("Ålborg");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Invalid enum value name: 'Ålborg', only ASCII allowed"));
        }
        builder.value("foo", 5);
        try {
            builder.value("bar", 5);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Enum value conflict, 5 already exists as foo when creating bar"));
        }
        try {
            builder.value("foo", 3);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Enum name conflict, value with name foo already exists"));
        }
        try {
            builder.value("Foo", 3);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Enum name conflict, normalized name FOO already exists as foo when creating Foo"));
        }

    }

    public static class NotAnEnum implements PEnum {

        @Override
        public PEnumValue $meta() {
            return null;
        }
        @Override
        public PEnumDescriptor $descriptor() {
            return null;
        }

    }
    public static class AlsoNotAnEnum implements PEnum {

        @Override
        public PEnumValue $meta() {
            return null;
        }

        @Override
        public PEnumDescriptor $descriptor() {
            return null;
        }
        private static final PEnumDescriptor kDescriptor = kFibonacci;

    }
    public static class Also2NotAnEnum implements PEnum {

        @Override
        public PEnumValue $meta() {
            return null;
        }

        @Override
        public PEnumDescriptor $descriptor() {
            return null;
        }
        public static final PMessageDescriptor<?> kDescriptor = kFieldTypes;

    }

    @Test
    public void testGetEnumDescriptor() {
        PEnumDescriptor descriptor = PEnumDescriptor.getEnumDescriptor(TestEnum.class);
        assertThat(descriptor, is(notNullValue()));
        assertThat(descriptor.getTypeName(), is("test.TestEnum"));

        try {
            PEnumDescriptor.getEnumDescriptor(PEnum.class);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Base class of PEnum not a typed enum"));
        }
        try {
            PEnumDescriptor.getEnumDescriptor(NotAnEnum.class);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a typed enum: NotAnEnum"));
        }
        try {
            PEnumDescriptor.getEnumDescriptor(AlsoNotAnEnum.class);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a typed enum: AlsoNotAnEnum"));
        }
        try {
            PEnumDescriptor.getEnumDescriptor(Also2NotAnEnum.class);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not an enum descriptor property: Also2NotAnEnum.kDescriptor"));
        }
        try {
            PEnumDescriptor.getEnumDescriptor(String.class);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("not a typed enum: String"));
        }
    }
}
