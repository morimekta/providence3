package io.pvdnc.core.reflect;

import io.pvdnc.core.impl.Empty;
import io.pvdnc.core.property.PDefaultProperties;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.types.PConstDescriptor;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.types.PType;
import io.pvdnc.core.types.PTypeDefDescriptor;

import static io.pvdnc.core.property.PDefaultProperties.DEPRECATED;
import static io.pvdnc.core.property.PDefaultProperties.ENUM_ALIASES;

/**
 * Utility class to quickly create contained types in test code. It basically has
 * a set of sample contained types reflecting a broad spectre of features of providence.
 */
public class TestReflectionUtil {
    public static final CEnumDescriptor kFibonacci;
    public static final CEnumDescriptor kValue;
    public static final CMessageDescriptor kFieldTypes;
    public static final CMessageDescriptor kSimple;
    public static final PTypeDefDescriptor kTypeDef;
    public static final PConstDescriptor kConst;

    static {
        kTypeDef = new PTypeDefDescriptor("providence", "StringList",
                PPropertyMap.empty(), PListDescriptor.provider(PType.STRING.provider()));

        CEnumDescriptor.Builder bFibonacci = CEnumDescriptor.builder("providence", "Fibonacci");
        bFibonacci.value("FIRST", 1);
        bFibonacci.value("SECOND", PPropertyMap.of(ENUM_ALIASES, "OTHER"));
        bFibonacci.value("THIRD");
        bFibonacci.value("FOURTH", 5);
        bFibonacci.value("FIFTH", 8);
        bFibonacci.value("SIXTH", 13);
        bFibonacci.value("SEVENTH", 21, PPropertyMap.of("foo", "bar"));
        bFibonacci.value("EIGHTH", 34);
        bFibonacci.value("NINTH", 55);
        bFibonacci.properties(PPropertyMap.of(DEPRECATED, "not really"));
        kFibonacci = bFibonacci.build();

        CEnumDescriptor.Builder bValue = CEnumDescriptor.builder("providence", "Value");
        bValue.value("A");
        bValue.value("B");
        bValue.property(PDefaultProperties.ENUM_DEFAULT, "A");
        bValue.property("unknown", "property");
        kValue = bValue.build();

        CMessageDescriptor.Builder bFieldTypes = CMessageDescriptor.builder("providence", "FieldTypes");
        bFieldTypes.field("v", PType.VOID).id(1);  // it's theoretically allowed.
        bFieldTypes.field("b", PType.BOOL).id(2);
        bFieldTypes.field("bt", PType.BYTE);
        bFieldTypes.field("s", PType.SHORT).id(3);
        bFieldTypes.field("i", PType.INT);
        bFieldTypes.field("l", PType.LONG);
        bFieldTypes.field("f", PType.FLOAT).id(4);
        bFieldTypes.field("d", PType.DOUBLE);
        bFieldTypes.field("str", PType.STRING);
        bFieldTypes.field("bin", PType.BINARY);
        bFieldTypes.field("e", kFibonacci).id(5);
        bFieldTypes.field("msg", Empty.kDescriptor);
        bFieldTypes.field("lst", PListDescriptor.provider(PType.SHORT.provider()));
        bFieldTypes.field("set", PSetDescriptor.provider(PType.STRING.provider()));
        bFieldTypes.field("map", PMapDescriptor.provider(PType.INT.provider(), PType.DOUBLE.provider()));
        kFieldTypes = bFieldTypes.build();

        CMessageDescriptor.Builder bSimple = CMessageDescriptor.builder("providence", "Simple");
        bSimple.field("a", PType.INT).id(3);
        kSimple = bSimple.build();

        kConst = new PConstDescriptor("test", "kConst", PType.STRING.provider(), () -> "test");
    }
}
