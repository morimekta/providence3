package io.pvdnc.core.io;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PSerializerProviderTest {
    @Test
    public void testProviders() {
        PSerializer json = mock(PSerializer.class);
        when(json.mediaTypes()).thenReturn(List.of("application/json"));
        when(json.toString()).thenReturn("json");
        PSerializer thrift = mock(PSerializer.class);
        when(thrift.mediaTypes()).thenReturn(List.of("application/vnd.apache.thrift.binary", "application/x-thrift"));
        when(thrift.toString()).thenReturn("thrift");

        PSerializerProvider provider = new PSerializerProvider();
        provider.register(json);
        provider.register(thrift);

        assertThat(provider.forMediaType("application/json;utf-8"), is(Optional.of(json)));
        assertThat(provider.forMediaTypes(List.of("text/problem+json", "application/json")), is(Optional.of(json)));
        assertThat(provider.forMediaTypes(List.of("text/problem+json", "*/*")), is(Optional.of(json)));
        assertThat(provider.forMediaType("text/html"), is(Optional.empty()));
        assertThat(provider.forMediaTypes(List.of("text/html", "text/problem+json")), is(Optional.empty()));
        assertThat(provider.defaultSerializer(), is(json));

        try {
            new PSerializerProvider().defaultSerializer();
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("No default serializer"));
        }

        PSerializerProvider other = new PSerializerProvider();
        other.register(json);
        other.registerDefault(thrift);

        assertThat(other.defaultSerializer(), is(thrift));

        provider.setDefault("application/json");
        assertThat(provider.defaultSerializer(), is(json));
    }

    @Test
    public void testReRegister() {
        PSerializer json = mock(PSerializer.class, "json");
        when(json.mediaTypes()).thenReturn(List.of("application/json"));
        when(json.toString()).thenReturn("json");
        PSerializer thrift = mock(PSerializer.class, "thrift");
        when(thrift.mediaTypes()).thenReturn(List.of("application/json;x-thrift"));
        when(thrift.toString()).thenReturn("thrift");

        PSerializerProvider provider = new PSerializerProvider();
        provider.register(json);
        provider.register(json);
        provider.registerDefault(thrift);

        assertThat(provider.defaultSerializer(), is(thrift));
    }

    @Test
    public void testLoad() {
        new PSerializerProvider().load();

        try {
            new PSerializerProvider().setDefault("test/html");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No serializer for media type: test/html"));
        }

        try {
            // TODO: Change this test to find something actually loaded.
            new PSerializerProvider().defaultSerializer();
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("No default serializer"));
        }

    }
}
