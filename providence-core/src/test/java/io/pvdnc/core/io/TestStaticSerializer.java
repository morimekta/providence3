package io.pvdnc.core.io;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.types.PMessageDescriptor;
import net.morimekta.collect.util.Binary;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public class TestStaticSerializer implements PSerializer {
    private final Map<Binary, PMessage> contentToMessage = new HashMap<>();
    private final String mediaType;

    public TestStaticSerializer(String mediaType) {
        this.mediaType = mediaType;
    }

    @Override
    public List<String> mediaTypes() {
        return List.of(mediaType);
    }

    private Binary createContent(PMessage message) {
        int hash = Objects.hash(mediaType, message);
        return Binary.fromHexString(String.format("%08x", hash));
    }

    @Override
    @SuppressWarnings("unchecked")
    public <M extends PMessage> M readFrom(PMessageDescriptor<M> descriptor, InputStream in) throws IOException {
        return Optional
                .ofNullable((M) contentToMessage.get(Binary.read(in)))
                .orElseThrow(() -> new IOException("Unreadable message"));
    }

    @Override
    public void writeTo(PMessage message, OutputStream out) throws IOException {
        Binary content = createContent(message);
        contentToMessage.put(content, message);
        content.write(out);
    }

    @Override
    public boolean isStrict() {
        return false;
    }

    @Override
    public PSerializer strict() {
        return this;
    }
}
