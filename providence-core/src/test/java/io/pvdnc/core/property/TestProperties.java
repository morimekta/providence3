package io.pvdnc.core.property;

import static io.pvdnc.core.property.PPropertyTarget.MESSAGE_FIELD;

public final class TestProperties {
    public static final PProperty<Integer> MAX_LENGTH;

    private TestProperties() {}

    static {
        MAX_LENGTH = new PPersistedProperty<>("max.length", Integer.TYPE, MESSAGE_FIELD);
    }}
