package io.pvdnc.core.property;

import io.pvdnc.core.PEnum;
import io.pvdnc.core.test.TestEnum;
import io.pvdnc.core.test.TestMessage;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMessageVariant;
import io.pvdnc.core.types.PType;
import io.pvdnc.core.reflect.CEnum;
import io.pvdnc.core.reflect.CEnumDescriptor;
import io.pvdnc.core.reflect.CMessageDescriptor;
import io.pvdnc.core.types.PEnumValue;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static io.pvdnc.core.property.PDefaultProperties.DEPRECATED;
import static io.pvdnc.core.property.PJsonProperties.JSON_COMPACT;
import static io.pvdnc.core.property.PJsonProperties.JSON_NAME;
import static io.pvdnc.core.property.PProperty.findProperty;
import static io.pvdnc.core.property.PPropertyTarget.ENUM_VALUE;
import static io.pvdnc.core.property.PPropertyTarget.MESSAGE;
import static io.pvdnc.core.property.PPropertyTarget.MESSAGE_FIELD;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class PPropertyTest {
    @Test
    public void testProperty() {
        PEnumValue foo_bar = new PEnumValue(
                "FOO_BAR",
                3,
                PPropertyMap.of(JSON_NAME, "foo-bar",
                                JSON_COMPACT, true),
                () -> null);
        assertThat(foo_bar.getProperty(JSON_NAME), is("foo-bar"));
        assertThat(foo_bar.getProperty(JSON_COMPACT), is(true));

        PEnumValue bar_bar = new PEnumValue(
                "BAR_BAR",
                3,
                () -> null);
        assertThat(bar_bar.getProperty(JSON_NAME), is("BAR_BAR"));
        assertThat(bar_bar.getProperty(JSON_COMPACT), is(false));

        assertThat(findProperty("json.name").orElse(null),
                   is(sameInstance(JSON_NAME)));

        assertThat(JSON_NAME.getName(), is("json.name"));
        assertThat(((PPersistedProperty<?>) JSON_NAME).getType(), is(String.class));
        assertThat(((PPersistedProperty<?>) JSON_NAME).getDefaultValue(), is(nullValue()));
        assertThat(JSON_NAME.getTargets(), is(setOf(
                ENUM_VALUE,
                MESSAGE_FIELD)));

        assertThat(JSON_NAME, is(not(JSON_COMPACT)));
        assertThat(JSON_COMPACT, is(not(DEPRECATED)));

        assertThat(JSON_COMPACT, is(new PPersistedProperty<>("json.compact", Boolean.class, false, MESSAGE)));
        assertThat(JSON_COMPACT, is(not(new PPersistedProperty<>("json.compact", Boolean.class, MESSAGE))));
        assertThat(JSON_COMPACT.toString(), is("PProperty{json.compact=false: {MESSAGE}}"));
        assertThat(JSON_NAME.toString(), is("PProperty{json.name=(String): {MESSAGE_FIELD, ENUM_VALUE}}"));


        assertThat(((PPersistedProperty<?>) JSON_NAME).defaultValueFor(
                new PEnumValue("BAR_BAR", 1, PPropertyMap.empty(), () -> null)), is("BAR_BAR"));
        assertThat(((PPersistedProperty<?>) JSON_NAME).defaultValueFor(
                PPropertyMap.empty()), is(nullValue()));
        assertThat(((PPersistedProperty<?>) JSON_NAME).defaultValueFor(
                new PField(1, () -> PType.INT, "foo_bar", null, PPropertyMap.empty(), null, () -> null)),
                   is("foo_bar"));
    }

    @Test
    public void testParserString() {
        assertThat(parseString("foo", String.class), is("foo"));
        assertThat(parseString("false", Boolean.class), is(false));
        assertThat(parseString("true", Boolean.TYPE), is(true));
        assertThat(parseString("42", Long.class), is(42L));
        assertThat(parseString("420", Long.TYPE), is(420L));
        assertThat(parseString("42", Integer.class), is(42));
        assertThat(parseString("420", Integer.TYPE), is(420));
        assertThat(parseString("42.42", Double.class), is(42.42));
        assertThat(parseString("42.4242", Double.TYPE), is(42.4242));
        assertThat(parseString("test", TestEnum.class), is(TestEnum.TEST));
        assertThat(parseString("UNION", PMessageVariant.class), is(PMessageVariant.UNION));

        try {
            parseString("foo", TestMessage.class);
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("No parsing implemented for property 'test' " +
                                          "as io.pvdnc.core.test.TestMessage"));
        }
        try {
            parseString("foo", PMessageVariant.class);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No value 'foo' for enum: io.pvdnc.core.types.PMessageVariant"));
        }
    }

    private <T> T parseString(String str, Class<T> type) {
        return new PPersistedProperty<>("test", type).parseString(str);
    }

    @Test
    public void testVirtualProperty() {
        PVirtualProperty<Map<String, PEnum>>  JEM = (PVirtualMapProperty<String, PEnum>) PJsonProperties.JSON_ENUM_MAP;
        PVirtualProperty<Map<String, PField>> JMM = (PVirtualProperty<Map<String, PField>>) PJsonProperties.JSON_FIELD_MAP;
        CEnumDescriptor ed = CEnumDescriptor.builder("test", "Enum")
                                            .value("FOO")
                                            .value("BAR", PPropertyMap.of("json.name", "bar"))
                                            .build();
        CEnum bar = ed.valueForName("BAR");

        assertThat(JEM.getName(), is("json.enum.map"));
        assertThat(JEM.getType(), is(Map.class));
        assertThat(JEM.getTargets(), is(setOf(PPropertyTarget.ENUM)));
        assertThat(JEM.makeValueFor(ed), is(mapOf("bar", bar)));
        assertThat(JEM.toString(), is("PVProperty{json.enum.map=(Map): {ENUM}}"));
        assertThat(JEM.hashCode(), is(JEM.hashCode()));
        assertThat(JEM, CoreMatchers.is(JEM));
        assertThat(JEM, is(CoreMatchers.not(JMM)));
        assertThat(JEM, is(not(new Object())));

        CMessageDescriptor.Builder mb = CMessageDescriptor.builder("test", "Message");
        mb.field("foo", PType.BYTE).property(JSON_NAME, "foo-bar");
        CMessageDescriptor md = mb.build();
        PField foo = md.fieldForName("foo");

        assertThat(JMM.makeValueFor(md), is(mapOf("foo-bar", foo)));

        try {
            JEM.makeValueFor(md);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("not an enum descriptor: CMessageDescriptor"));
        }
        try {
            JMM.makeValueFor(ed);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("not a message descriptor: CEnumDescriptor"));
        }
    }
}
