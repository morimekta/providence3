package io.pvdnc.core.property;

import io.pvdnc.core.types.PMessageVariant;
import org.hamcrest.CoreMatchers;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class PPropertyMapTest {
    @Test
    public void testPropertyMap() {
        PPropertyMap map = PPropertyMap.empty();

        assertThat(map.hasProperty(PDefaultProperties.DEPRECATED), is(false));
        assertThat(map.getProperty(PDefaultProperties.DEPRECATED), is(nullValue()));

        PPropertyMap notEmpty = PPropertyMap
                .newBuilder()
                .putAll(map)
                .put(PDefaultProperties.DEPRECATED, "true")
                .putUnsafe("max.length", "42")
                .putUnsafe("sorted", "true")
                .build();

        assertThat(notEmpty.hasProperty(PDefaultProperties.MESSAGE_VARIANT), is(false));
        assertThat(notEmpty.getProperty(PDefaultProperties.MESSAGE_VARIANT), is(nullValue()));
        assertThat(notEmpty.getUnsafeProperty(PDefaultProperties.MESSAGE_VARIANT.getName()), is(nullValue()));

        assertThat(notEmpty.hasProperty(PDefaultProperties.DEPRECATED), is(true));
        assertThat(notEmpty.getProperty(PDefaultProperties.DEPRECATED), is("true"));
        assertThat(notEmpty.hasProperty(PDefaultProperties.SORTED), is(true));
        assertThat(notEmpty.getProperty(PDefaultProperties.SORTED), is(true));
        assertThat(notEmpty.getUnsafeProperty(PDefaultProperties.SORTED.getName()), is("true"));
        assertThat(notEmpty.hasProperty(TestProperties.MAX_LENGTH), is(true));
        assertThat(notEmpty.getProperty(TestProperties.MAX_LENGTH), is(42));
        assertThat(notEmpty.getUnsafeProperty(TestProperties.MAX_LENGTH.getName()), is("42"));

        assertThat(map, is(CoreMatchers.not(notEmpty)));
        assertThat(map, Is.is(map));
        assertThat(map.hashCode(), is(not(notEmpty.hashCode())));

        assertThat(map.toString(), is("PPropertyMap{}"));
        assertThat(notEmpty.toString(), is("PPropertyMap{deprecated: true, max.length: 42, sorted: true}"));
    }

    @Test
    public void testConstructor() {
        PPropertyMap a = PPropertyMap.of(PDefaultProperties.DEPRECATED, "foo");
        PPropertyMap b = PPropertyMap.of(PDefaultProperties.DEPRECATED, "foo",
                                         PDefaultProperties.MESSAGE_VARIANT, PMessageVariant.INTERFACE);
        PPropertyMap c = PPropertyMap.of(PDefaultProperties.DEPRECATED, "foo",
                                         PDefaultProperties.MESSAGE_VARIANT, PMessageVariant.INTERFACE,
                                         PDefaultProperties.ENUM_DEFAULT, "BAR");
        PPropertyMap d = PPropertyMap.of(PDefaultProperties.DEPRECATED, "foo",
                                         PDefaultProperties.MESSAGE_VARIANT, PMessageVariant.INTERFACE,
                                         PDefaultProperties.ENUM_DEFAULT, "BAR",
                                         PDefaultProperties.SORTED, true);

        assertThat(PPropertyMap.of("deprecated", "foo"), Is.is(a));
        assertThat(PPropertyMap.of("deprecated", "foo",
                                   "message.variant", "INTERFACE"), Is.is(b));
        assertThat(PPropertyMap.of("deprecated", "foo",
                                   "message.variant", "INTERFACE",
                                   "enum.default", "BAR"), Is.is(c));
        assertThat(PPropertyMap.of("deprecated", "foo",
                                   "message.variant", "INTERFACE",
                                   "enum.default", "BAR",
                                   "sorted", "true"), Is.is(d));
    }
}
