package io.pvdnc.sio.gson;

import io.pvdnc.core.PAny;
import io.pvdnc.core.PMessage;
import io.pvdnc.core.PMessageBuilder;
import io.pvdnc.core.reflect.CMessageDescriptor;
import io.pvdnc.core.registry.PSimpleTypeRegistry;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PType;
import net.morimekta.collect.util.Binary;
import org.junit.jupiter.api.BeforeAll;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.collect.UnmodifiableSet.setOf;

public class TestSerializerBase {
    protected static PMessageDescriptor<?> testTypesDescriptor;
    protected static PMessage              testTypes;
    protected static TestMessage           message1;
    protected static TestMessage           message2;
    protected static TestCompact           compact1;
    protected static TestWrapper           wrapper;

    @BeforeAll
    public static void setUp() {
        CMessageDescriptor.Builder messageB = CMessageDescriptor.builder("pvd", "ContainedMessage");
        messageB.field("v", PType.VOID).id(1);
        messageB.field("map_b", PMapDescriptor.provider(PType.BOOL.provider(), PType.STRING.provider())).id(2);
        messageB.field("map_bt", PMapDescriptor.provider(PType.BYTE.provider(), PType.STRING.provider())).id(3);
        messageB.field("map_s", PMapDescriptor.provider(PType.SHORT.provider(), PType.STRING.provider())).id(4);
        messageB.field("map_i", PMapDescriptor.provider(PType.INT.provider(), PType.STRING.provider())).id(5);
        messageB.field("map_l", PMapDescriptor.provider(PType.LONG.provider(), PType.STRING.provider())).id(6);
        messageB.field("map_f", PMapDescriptor.provider(PType.FLOAT.provider(), PType.STRING.provider())).id(7);
        messageB.field("map_d", PMapDescriptor.provider(PType.DOUBLE.provider(), PType.STRING.provider())).id(8);
        messageB.field("map_str", PMapDescriptor.provider(PType.STRING.provider(), PType.STRING.provider())).id(9);
        messageB.field("map_bin", PMapDescriptor.provider(PType.BINARY.provider(), PType.STRING.provider())).id(10);
        messageB.field("map_e", PMapDescriptor.provider(TestEnum.provider(), PType.STRING.provider())).id(11);
        testTypesDescriptor = messageB.build();
        PMessageBuilder typesB = testTypesDescriptor.newBuilder();
        typesB.set(1, true);
        typesB.set(2, mapOf(true, "true", false, "false"));
        typesB.set(3, mapOf((byte) 42, "meaning", (byte) -42, "less"));
        typesB.set(4, mapOf((short) 42, "meaning", (short) -42, "less"));
        typesB.set(5, mapOf(42, "meaning", -42, "less"));
        typesB.set(6, mapOf(42L, "meaning", -42L, "less"));
        typesB.set(7, mapOf(42.4242f, "meaning", -42.4242f, "less"));
        typesB.set(8, mapOf(42.424242, "meaning", -42.424242, "less"));
        typesB.set(9, mapOf("Douglas", "meaning", "Adams", "less"));
        typesB.set(10, mapOf(Binary.fromBase64("foo1"), "meaning", Binary.fromBase64("bari"), "less"));
        typesB.set(11, mapOf(TestEnum.FIRST, "meaning", TestEnum.SECOND, "less"));
        testTypes = typesB.build();

        message1 = TestMessage
                .newBuilder()
                .setB(true)
                .setBt((byte) 42)
                .setS((short) 4200)
                .setI(42000000)
                .setL(42000000000L)
                .setF(42.42424f)
                .setD(42.4242424242)
                .setE(TestEnum.SECOND)
                .setStr("foo")
                .setBin(Binary.fromBase64("bar"))
                .setBList(listOf((byte) 42))
                .setFSet(setOf(42.4242f))
                .setIsMap(mapOf(42, "bar"))
                .build();
        message2 = TestMessage
                .newBuilder()
                .setStr("Wıð ûñ¡côþ€")
                .setBList(listOf((byte) 42, (byte) -42))
                .setFSet(setOf(42.4242f, -42.4242f))
                .setIsMap(mapOf(42, "bar", -42, "foo"))
                .build();
        compact1 = TestCompact
                .newBuilder()
                .setBt((byte) -42)
                .setS((short) 4200)
                .build();
        compact1 = TestCompact
                .newBuilder()
                .setB(false)
                .setBt((byte) -42)
                .setL(4200_000_000_000_000_000L)
                .build();
        wrapper = TestWrapper
                .newBuilder()
                .setMsg(compact1)
                .setMsgList(listOf(message1, message2))
                .setMsgSet(setOf(compact1))
                .setMsgMap(mapOf("foo", message2, "bar", message1))
                .setAny(PAny.wrap(compact1))
                .build();

        PSimpleTypeRegistry.getInstance().registerRecursively(wrapper.$descriptor());
    }
}
