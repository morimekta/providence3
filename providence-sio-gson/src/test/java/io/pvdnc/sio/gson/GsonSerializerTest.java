package io.pvdnc.sio.gson;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.io.PSerializer;
import io.pvdnc.core.io.PSerializerProvider;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.stream.Stream;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class GsonSerializerTest extends TestSerializerBase {
    @Test
    public void testGson_Provided() {
        PSerializerProvider provider = new PSerializerProvider().load();
        assertThat(provider.forMediaType(GsonSerializer.MEDIA_TYPE).orElse(null),
                   is(instanceOf(GsonSerializer.class)));
        assertThat(provider.forMediaType(CompactGsonSerializer.MEDIA_TYPE).orElse(null),
                   is(instanceOf(CompactGsonSerializer.class)));
    }

    @Test
    public void testGson_Registered() {
        PSerializerProvider provider = new PSerializerProvider()
                .registerDefault(new GsonSerializer())
                .register(new CompactGsonSerializer());
        assertThat(provider.forMediaType(GsonSerializer.MEDIA_TYPE).orElse(null),
                   is(instanceOf(GsonSerializer.class)));
        assertThat(provider.forMediaType(CompactGsonSerializer.MEDIA_TYPE).orElse(null),
                   is(instanceOf(CompactGsonSerializer.class)));
    }

    @Test
    public void testConstructors() {
        GsonSerializer serializer = new GsonSerializer();
        assertThat(serializer.isPretty(), is(false));
        assertThat(serializer.isStrict(), is(false));

        GsonSerializer pretty = serializer.pretty();
        assertThat(pretty.isPretty(), is(true));
        assertThat(pretty.isStrict(), is(false));
        assertThat(pretty.pretty(), is(sameInstance(pretty)));

        GsonSerializer strict = serializer.strict();
        assertThat(strict.isPretty(), is(false));
        assertThat(strict.isStrict(), is(true));
        assertThat(strict.strict(), is(sameInstance(strict)));

        GsonSerializer both = strict.pretty();
        assertThat(both.isPretty(), is(true));
        assertThat(both.isStrict(), is(true));
        assertThat(both.pretty(), is(sameInstance(both)));
        assertThat(both.strict(), is(sameInstance(both)));
    }

    @Test
    public void testConstructors_compact() {
        CompactGsonSerializer serializer = new CompactGsonSerializer();
        assertThat(serializer.isStrict(), is(false));

        CompactGsonSerializer strict = serializer.strict();
        assertThat(strict.isStrict(), is(true));
        assertThat(strict.strict(), is(sameInstance(strict)));
    }

    public static Stream<Arguments> testPrettyData() {
        GsonSerializer gson = new GsonSerializer();
        GsonSerializer pretty = gson.pretty();
        CompactGsonSerializer compact = new CompactGsonSerializer();
        return Stream.of(arguments(pretty,
                                   message1,
                                   "{\n" +
                                   "  \"b\": true,\n" +
                                   "  \"bt\": 42,\n" +
                                   "  \"s\": 4200,\n" +
                                   "  \"i\": 42000000,\n" +
                                   "  \"l\": 42000000000,\n" +
                                   "  \"f\": 42.42424,\n" +
                                   "  \"d\": 42.4242424242,\n" +
                                   "  \"str\": \"foo\",\n" +
                                   "  \"bin\": \"bao\",\n" +
                                   "  \"e\": \"second\",\n" +
                                   "  \"lst\": [\n" +
                                   "    42\n" +
                                   "  ],\n" +
                                   "  \"set\": [\n" +
                                   "    42.4242\n" +
                                   "  ],\n" +
                                   "  \"map\": {\n" +
                                   "    \"42\": \"bar\"\n" +
                                   "  }\n" +
                                   "}"),
                         arguments(gson,
                                   message1,
                                   "{\"b\":true,\"bt\":42,\"s\":4200,\"i\":42000000,\"l\":42000000000,\"f\":42.42424,\"d\":42.4242424242,\"str\":\"foo\",\"bin\":\"bao\",\"e\":\"second\",\"lst\":[42],\"set\":[42.4242],\"map\":{\"42\":\"bar\"}}"),
                         arguments(compact,
                                   message1,
                                   "{\"1\":true,\"2\":42,\"3\":4200,\"4\":42000000,\"5\":42000000000,\"6\":42.42424,\"7\":42.4242424242,\"8\":\"foo\",\"9\":\"bao\",\"10\":2,\"11\":[42],\"12\":[42.4242],\"13\":{\"42\":\"bar\"}}"),
                         arguments(pretty,
                                   wrapper,
                                   "{\n" +
                                   "  \"msg\": [\n" +
                                   "    false,\n" +
                                   "    -42,\n" +
                                   "    null,\n" +
                                   "    null,\n" +
                                   "    4200000000000000000\n" +
                                   "  ],\n" +
                                   "  \"lst\": [\n" +
                                   "    {\n" +
                                   "      \"b\": true,\n" +
                                   "      \"bt\": 42,\n" +
                                   "      \"s\": 4200,\n" +
                                   "      \"i\": 42000000,\n" +
                                   "      \"l\": 42000000000,\n" +
                                   "      \"f\": 42.42424,\n" +
                                   "      \"d\": 42.4242424242,\n" +
                                   "      \"str\": \"foo\",\n" +
                                   "      \"bin\": \"bao\",\n" +
                                   "      \"e\": \"second\",\n" +
                                   "      \"lst\": [\n" +
                                   "        42\n" +
                                   "      ],\n" +
                                   "      \"set\": [\n" +
                                   "        42.4242\n" +
                                   "      ],\n" +
                                   "      \"map\": {\n" +
                                   "        \"42\": \"bar\"\n" +
                                   "      }\n" +
                                   "    },\n" +
                                   "    {\n" +
                                   "      \"str\": \"Wıð ûñ¡côþ€\",\n" +
                                   "      \"lst\": [\n" +
                                   "        42,\n" +
                                   "        -42\n" +
                                   "      ],\n" +
                                   "      \"set\": [\n" +
                                   "        42.4242,\n" +
                                   "        -42.4242\n" +
                                   "      ],\n" +
                                   "      \"map\": {\n" +
                                   "        \"42\": \"bar\",\n" +
                                   "        \"-42\": \"foo\"\n" +
                                   "      }\n" +
                                   "    }\n" +
                                   "  ],\n" +
                                   "  \"set\": [\n" +
                                   "    [\n" +
                                   "      false,\n" +
                                   "      -42,\n" +
                                   "      null,\n" +
                                   "      null,\n" +
                                   "      4200000000000000000\n" +
                                   "    ]\n" +
                                   "  ],\n" +
                                   "  \"map\": {\n" +
                                   "    \"bar\": {\n" +
                                   "      \"b\": true,\n" +
                                   "      \"bt\": 42,\n" +
                                   "      \"s\": 4200,\n" +
                                   "      \"i\": 42000000,\n" +
                                   "      \"l\": 42000000000,\n" +
                                   "      \"f\": 42.42424,\n" +
                                   "      \"d\": 42.4242424242,\n" +
                                   "      \"str\": \"foo\",\n" +
                                   "      \"bin\": \"bao\",\n" +
                                   "      \"e\": \"second\",\n" +
                                   "      \"lst\": [\n" +
                                   "        42\n" +
                                   "      ],\n" +
                                   "      \"set\": [\n" +
                                   "        42.4242\n" +
                                   "      ],\n" +
                                   "      \"map\": {\n" +
                                   "        \"42\": \"bar\"\n" +
                                   "      }\n" +
                                   "    },\n" +
                                   "    \"foo\": {\n" +
                                   "      \"str\": \"Wıð ûñ¡côþ€\",\n" +
                                   "      \"lst\": [\n" +
                                   "        42,\n" +
                                   "        -42\n" +
                                   "      ],\n" +
                                   "      \"set\": [\n" +
                                   "        42.4242,\n" +
                                   "        -42.4242\n" +
                                   "      ],\n" +
                                   "      \"map\": {\n" +
                                   "        \"42\": \"bar\",\n" +
                                   "        \"-42\": \"foo\"\n" +
                                   "      }\n" +
                                   "    }\n" +
                                   "  },\n" +
                                   "  \"any\": {\n" +
                                   "    \"@type\": \"test.TestCompact\",\n" +
                                   "    \"b\": false,\n" +
                                   "    \"bt\": -42,\n" +
                                   "    \"l\": 4200000000000000000\n" +
                                   "  }\n" +
                                   "}"),
                         arguments(gson,
                                   wrapper,
                                   "{\"msg\":[false,-42,null,null,4200000000000000000],\"lst\":[{\"b\":true,\"bt\":42,\"s\":4200,\"i\":42000000,\"l\":42000000000,\"f\":42.42424,\"d\":42.4242424242,\"str\":\"foo\",\"bin\":\"bao\",\"e\":\"second\",\"lst\":[42],\"set\":[42.4242],\"map\":{\"42\":\"bar\"}},{\"str\":\"Wıð ûñ¡côþ€\",\"lst\":[42,-42],\"set\":[42.4242,-42.4242],\"map\":{\"42\":\"bar\",\"-42\":\"foo\"}}],\"set\":[[false,-42,null,null,4200000000000000000]],\"map\":{\"bar\":{\"b\":true,\"bt\":42,\"s\":4200,\"i\":42000000,\"l\":42000000000,\"f\":42.42424,\"d\":42.4242424242,\"str\":\"foo\",\"bin\":\"bao\",\"e\":\"second\",\"lst\":[42],\"set\":[42.4242],\"map\":{\"42\":\"bar\"}},\"foo\":{\"str\":\"Wıð ûñ¡côþ€\",\"lst\":[42,-42],\"set\":[42.4242,-42.4242],\"map\":{\"42\":\"bar\",\"-42\":\"foo\"}}},\"any\":{\"@type\":\"test.TestCompact\",\"b\":false,\"bt\":-42,\"l\":4200000000000000000}}"),
                         arguments(compact,
                                   wrapper,
                                   "{\"1\":[false,-42,null,null,4200000000000000000],\"2\":[{\"1\":true,\"2\":42,\"3\":4200,\"4\":42000000,\"5\":42000000000,\"6\":42.42424,\"7\":42.4242424242,\"8\":\"foo\",\"9\":\"bao\",\"10\":2,\"11\":[42],\"12\":[42.4242],\"13\":{\"42\":\"bar\"}},{\"8\":\"Wıð ûñ¡côþ€\",\"11\":[42,-42],\"12\":[42.4242,-42.4242],\"13\":{\"42\":\"bar\",\"-42\":\"foo\"}}],\"3\":[[false,-42,null,null,4200000000000000000]],\"4\":{\"bar\":{\"1\":true,\"2\":42,\"3\":4200,\"4\":42000000,\"5\":42000000000,\"6\":42.42424,\"7\":42.4242424242,\"8\":\"foo\",\"9\":\"bao\",\"10\":2,\"11\":[42],\"12\":[42.4242],\"13\":{\"42\":\"bar\"}},\"foo\":{\"8\":\"Wıð ûñ¡côþ€\",\"11\":[42,-42],\"12\":[42.4242,-42.4242],\"13\":{\"42\":\"bar\",\"-42\":\"foo\"}}},\"5\":{\"@\":\"test.TestCompact\",\"1\":false,\"2\":-42,\"5\":4200000000000000000}}"),
                         arguments(pretty,
                                   testTypes,
                                   "{\n" +
                                   "  \"v\": true,\n" +
                                   "  \"map_b\": {\n" +
                                   "    \"true\": \"true\",\n" +
                                   "    \"false\": \"false\"\n" +
                                   "  },\n" +
                                   "  \"map_bt\": {\n" +
                                   "    \"42\": \"meaning\",\n" +
                                   "    \"-42\": \"less\"\n" +
                                   "  },\n" +
                                   "  \"map_s\": {\n" +
                                   "    \"42\": \"meaning\",\n" +
                                   "    \"-42\": \"less\"\n" +
                                   "  },\n" +
                                   "  \"map_i\": {\n" +
                                   "    \"42\": \"meaning\",\n" +
                                   "    \"-42\": \"less\"\n" +
                                   "  },\n" +
                                   "  \"map_l\": {\n" +
                                   "    \"42\": \"meaning\",\n" +
                                   "    \"-42\": \"less\"\n" +
                                   "  },\n" +
                                   "  \"map_f\": {\n" +
                                   "    \"42.4242\": \"meaning\",\n" +
                                   "    \"-42.4242\": \"less\"\n" +
                                   "  },\n" +
                                   "  \"map_d\": {\n" +
                                   "    \"42.424242\": \"meaning\",\n" +
                                   "    \"-42.424242\": \"less\"\n" +
                                   "  },\n" +
                                   "  \"map_str\": {\n" +
                                   "    \"Douglas\": \"meaning\",\n" +
                                   "    \"Adams\": \"less\"\n" +
                                   "  },\n" +
                                   "  \"map_bin\": {\n" +
                                   "    \"foo1\": \"meaning\",\n" +
                                   "    \"bari\": \"less\"\n" +
                                   "  },\n" +
                                   "  \"map_e\": {\n" +
                                   "    \"FIRST\": \"meaning\",\n" +
                                   "    \"second\": \"less\"\n" +
                                   "  }\n" +
                                   "}"),
                         arguments(compact,
                                   testTypes,
                                   "{\"1\":true,\"2\":{\"true\":\"true\",\"false\":\"false\"},\"3\":{\"42\":\"meaning\",\"-42\":\"less\"},\"4\":{\"42\":\"meaning\",\"-42\":\"less\"},\"5\":{\"42\":\"meaning\",\"-42\":\"less\"},\"6\":{\"42\":\"meaning\",\"-42\":\"less\"},\"7\":{\"42.4242\":\"meaning\",\"-42.4242\":\"less\"},\"8\":{\"42.424242\":\"meaning\",\"-42.424242\":\"less\"},\"9\":{\"Douglas\":\"meaning\",\"Adams\":\"less\"},\"10\":{\"foo1\":\"meaning\",\"bari\":\"less\"},\"11\":{\"1\":\"meaning\",\"2\":\"less\"}}"));
    }

    @ParameterizedTest
    @MethodSource("testPrettyData")
    public void testPretty(PSerializer serializer, PMessage message, String content) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        serializer.writeTo(message, out);
        PMessage parsed = serializer.readFrom(message.$descriptor(), new ByteArrayInputStream(out.toByteArray()));
        serializer.strict().readFrom(message.$descriptor(), new ByteArrayInputStream(out.toByteArray()));

        assertThat(out.toString(UTF_8), is(content));
        assertThat(parsed, is(message));
    }
}
