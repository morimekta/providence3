package io.pvdnc.sio.gson.adapter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import io.pvdnc.core.reflect.CEnum;
import io.pvdnc.sio.gson.TestEnum;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class PEnumTypeAdapterTest {
    public static class Simple {
        public TestEnum ev;
        public Map<TestEnum, TestEnum> map;
    }

    @Test
    public void testAdapter() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new PEnumTypeAdapterFactory());
        Gson gson = builder.create();

        Simple test = gson.fromJson("{\"ev\":\"FIRST\",\"map\":{\"second\":\"second\"}}", Simple.class);
        assertThat(test, is(notNullValue()));
        assertThat(test.ev, is(TestEnum.FIRST));
        assertThat(test.map, is(mapOf(TestEnum.SECOND, TestEnum.SECOND)));

        String serialized = gson.toJson(test);
        assertThat(serialized, is("{\"ev\":\"FIRST\",\"map\":{\"second\":\"second\"}}"));
    }

    @Test
    public void testFactory() {
        PEnumTypeAdapterFactory factory = new PEnumTypeAdapterFactory();
        Gson gson = new Gson();

        assertThat(factory.create(gson, TypeToken.get(String.class)), is(nullValue()));
        assertThat(factory.create(gson, TypeToken.get(CEnum.class)), is(nullValue()));
    }
}
