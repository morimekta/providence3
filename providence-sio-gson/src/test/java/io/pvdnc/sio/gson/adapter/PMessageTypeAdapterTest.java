package io.pvdnc.sio.gson.adapter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import io.pvdnc.core.PAny;
import io.pvdnc.core.impl.AnyBinary;
import io.pvdnc.core.reflect.CMessage;
import io.pvdnc.sio.gson.TestMessage;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class PMessageTypeAdapterTest {
    @Test
    public void testAdapter() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new PMessageTypeAdapterFactory());
        Gson gson = builder.create();

        TestMessage test = gson.fromJson("{\"str\":\"foo\"}", TestMessage.class);
        assertThat(test, is(notNullValue()));
        assertThat(test.hasI(), is(false));
        assertThat(test.hasE(), is(false));
        assertThat(test.getStr(), is("foo"));

        String serialized = gson.toJson(test);
        assertThat(serialized, is("{\"str\":\"foo\"}"));
    }

    @Test
    public void testAnyAdapter() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new PMessageTypeAdapterFactory());
        Gson gson = builder.create();

        PAny test = gson.fromJson("{\"@\":\"test.Unknown\",\"str\":\"foo\"}", PAny.class);
        assertThat(test, is(instanceOf(AnyBinary.class)));
        AnyBinary binary = (AnyBinary) test;
        assertThat(binary.isPresent(), is(true));
        assertThat(binary.getMediaType(), is("application/json"));
        assertThat(binary.getTypeName(), is("test.Unknown"));
        assertThat(binary.getData().decodeToString(StandardCharsets.UTF_8), is("{\"str\":\"foo\"}"));

        String serialized = gson.toJson(test);
        assertThat(serialized, is("{\"@type\":\"test.Unknown\",\"str\":\"foo\"}"));
    }

    @Test
    public void testFactory() {
        PMessageTypeAdapterFactory factory = new PMessageTypeAdapterFactory();
        Gson gson = new Gson();

        assertThat(factory.create(gson, TypeToken.get(String.class)), is(nullValue()));
        assertThat(factory.create(gson, TypeToken.get(CMessage.class)), is(nullValue()));
    }
}
