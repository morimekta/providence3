/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.gson.adapter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import net.morimekta.collect.MapBuilder;
import net.morimekta.collect.UnmodifiableMap;
import net.morimekta.collect.UnmodifiableSortedMap;
import io.pvdnc.core.PEnum;
import io.pvdnc.core.types.PEnumDescriptor;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Pattern;

import static java.util.Objects.requireNonNull;
import static io.pvdnc.core.property.PJsonProperties.JSON_ENUM_MAP;
import static io.pvdnc.core.property.PJsonProperties.JSON_NAME;

public class PEnumMapKeyTypeAdapter<Enum extends PEnum, V> extends TypeAdapter<Map<Enum, V>> {
    private final PEnumDescriptor            descriptor;
    private final TypeAdapter<V>             valueAdapter;
    private final Map<String, PEnum>         jsonNames;
    private final Class<? extends Map<?, ?>> mapClass;

    public PEnumMapKeyTypeAdapter(PEnumDescriptor descriptor, TypeAdapter<V> valueAdapter, Class<? extends Map<?,?>> mapClass) {
        this.descriptor = requireNonNull(descriptor, "descriptor == null");
        this.valueAdapter = requireNonNull(valueAdapter, "valueAdapter == null");
        this.mapClass = requireNonNull(mapClass, "mapClass == null");
        this.jsonNames = descriptor.getProperty(JSON_ENUM_MAP);
    }

    @Override
    public void write(JsonWriter out, Map<Enum, V> value) throws IOException {
        if (value == null) out.nullValue();
        else {
            out.beginObject();
            for (Map.Entry<Enum, V> entry : value.entrySet()) {
                out.name(entry.getKey().$meta().getProperty(JSON_NAME));
                valueAdapter.write(out, entry.getValue());
            }
            out.endObject();
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<Enum, V> read(JsonReader in) throws IOException {
        in.beginObject();
        MapBuilder<Enum, V> builder = UnmodifiableMap.newBuilder();
        while (in.peek() != JsonToken.END_OBJECT) {
            Enum key = (Enum) parseEnum(in.nextName());
            V value = valueAdapter.read(in);
            builder.put(key, value);
        }
        in.endObject();
        return mapWrapper(builder.build());
    }

    private Map<Enum, V> mapWrapper(Map<Enum, V> build) {
        if (UnmodifiableMap.class.isAssignableFrom(mapClass)) {
            return build;
        } else if (UnmodifiableSortedMap.class.isAssignableFrom(mapClass)) {
            return UnmodifiableSortedMap.asSortedMap(build);
        } else if (LinkedHashMap.class.isAssignableFrom(mapClass)) {
            return new LinkedHashMap<>(build);
        } else if (SortedMap.class.isAssignableFrom(mapClass)) {
            return new TreeMap<>(build);
        } else {
            return new HashMap<>(build);
        }
    }

    private PEnum parseEnum(String name) {
        PEnum value;
        if (ID.matcher(name).matches()) {
            value = descriptor.findById(Integer.parseInt(name));
        } else {
            value = descriptor.findByName(name);
            if (value == null) {
                value = jsonNames.get(name);
            }
        }
        return requireNonNull(value, () -> "value == null for '" + name + "'");
    }

    private static final Pattern ID = Pattern.compile("^(0|[1-9][0-9]*)$");
}
