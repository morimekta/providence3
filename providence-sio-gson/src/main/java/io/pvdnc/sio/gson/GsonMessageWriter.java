/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.gson;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.pvdnc.core.PAny;
import io.pvdnc.core.PEnum;
import io.pvdnc.core.PMessage;
import io.pvdnc.core.impl.AnyBinary;
import io.pvdnc.core.impl.AnyTypeNameFormat;
import io.pvdnc.core.io.PMessageWriter;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import net.morimekta.collect.util.Binary;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static io.pvdnc.core.property.PJsonProperties.JSON_COMPACT;
import static io.pvdnc.core.property.PJsonProperties.JSON_NAME;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.requireNonNull;
import static net.morimekta.strings.Stringable.asString;

/**
 * Message writer that uses a Gson for writing the messages to
 * stream.
 */
public class GsonMessageWriter implements PMessageWriter {
    protected final Writer rawWriter;
    protected final JsonWriter writer;
    protected final boolean namedFields;
    protected final boolean namedEnums;
    protected final boolean allowCompact;
    protected final String anyTypeNameField;
    protected final AnyTypeNameFormat anyTypeNameFormat;

    public GsonMessageWriter(JsonWriter writer) {
        this(null, writer, true, true, true, null, AnyTypeNameFormat.DEFAULT);
    }

    /**
     * Create a message writer that writes JSON to a writer as
     * a stream of messages.
     *
     * @param rawWriter The raw writer. If this is not null, it will write a newline after
     *                  each message, visually separating them in a stream file of messages.
     * @param writer The Gson Json writer.
     * @param namedFields If fields should be named. If false will use the numeric ID of the
     *                    field as the field name. Otherwise the 'json.name' or the name of
     *                    the field is used.
     * @param namedEnums If enum values should be named. If false will use the numeric value
     *                   of the enum. Otherwise the 'json.name' or the name of the enum is
     *                   used.
     * @param allowCompact If 'json.compact' messages should be written using compact syntax.
     *                     If false will write compact messages normally.
     * @param anyTypeNameField Field name to use for the serialized 'Any' message struct.
     * @param anyTypeNameFormat Formatting rule for serializing the 'Any' message type name.
     */
    public GsonMessageWriter(Writer rawWriter,
                             JsonWriter writer,
                             boolean namedFields,
                             boolean namedEnums,
                             boolean allowCompact,
                             String anyTypeNameField,
                             AnyTypeNameFormat anyTypeNameFormat) {
        this.rawWriter = rawWriter;
        this.writer = requireNonNull(writer, "writer == null");
        this.namedFields = namedFields;
        this.namedEnums = namedEnums;
        this.allowCompact = allowCompact;
        this.anyTypeNameField = Optional.ofNullable(anyTypeNameField).orElse("@type");
        this.anyTypeNameFormat = Optional.ofNullable(anyTypeNameFormat).orElse(AnyTypeNameFormat.DEFAULT);
    }

    @Override
    public void write(PMessage message) throws IOException {
        writeMessage(message);
        writer.flush();
        if (rawWriter != null) {
            rawWriter.write("\n");
            rawWriter.flush();
        }
    }

    private Map<String, Object> getAnyBinaryRawJson(AnyBinary binary) {
        try {
            if ((namedFields ? GsonSerializer.MEDIA_TYPE : CompactGsonSerializer.MEDIA_TYPE).equals(binary.getMediaType())
                    && binary.hasTypeName() && binary.hasData()) {
                JsonReader reader = new JsonReader(new InputStreamReader(binary.getData().getInputStream(), UTF_8));
                return GsonMessageReader.readObject(reader);
            }
            return null;
        } catch (IOException e) {
            return null;
        }
    }

    public void writeAny(PAny any) throws IOException {
        if (any instanceof AnyBinary) {
            Map<String, Object> values = getAnyBinaryRawJson((AnyBinary) any);
            if (values != null) {
                writer.beginObject();
                if (namedFields) {
                    writer.name(anyTypeNameField);
                } else {
                    writer.name("@");
                }
                writer.value(((AnyBinary) any).getTypeName());
                writeMessageMapNoBegin(values);
            } else {
                writeMessage((AnyBinary) any);
            }
        } else {
            PMessage message = any.getMessage();
            writer.beginObject();
            if (namedFields) {
                writer.name(anyTypeNameField);
            } else {
                writer.name("@");
            }
            writer.value(anyTypeNameFormat.toTypeName(message.$descriptor()));
            writeMessageFields(message);
            writer.endObject();
        }
    }

    private void writeMessageMapNoBegin(Map<String, ?> map) throws IOException {
        for (Map.Entry<String, ?> entry : map.entrySet()) {
            writer.name(entry.getKey());
            writeValue(entry.getValue());
        }
        writer.endObject();
    }

    @SuppressWarnings("unchecked")
    private void writeValue(Object value) throws IOException {
        if (value == null) {
            writer.nullValue();
        } else if (value instanceof Map) {
            writer.beginObject();
            writeMessageMapNoBegin((Map<String, ?>) value);
        } else if (value instanceof Collection) {
            writer.beginArray();
            for (Object o : (Collection<?>) value) {
                writeValue(o);
            }
            writer.endArray();
        } else if (value instanceof Double || value instanceof Float) {
            writer.value(((Number) value).doubleValue());
        } else if (value instanceof Number) {
            writer.value(((Number) value).longValue());
        } else if (value instanceof CharSequence) {
            writer.value(value.toString());
        } else if (value instanceof Boolean) {
            writer.value((Boolean) value);
        } else if (value instanceof Binary) {
            writer.value(((Binary) value).toBase64());
        } else {
            writer.value(value.toString());
        }
    }

    protected void writeMessage(PMessage message) throws IOException {
        if (allowCompact && message.$descriptor().getProperty(JSON_COMPACT)) {
            writer.beginArray();
            int skipped = 0;
            for (PField field : message.$descriptor().allFields()) {
                Optional<?> val = message.optional(field.getId());
                if (val.isPresent()) {
                    while (skipped > 0) {
                        writer.nullValue();
                        --skipped;
                    }
                    writeValue(val.get(), field.getResolvedDescriptor());
                } else {
                    ++skipped;
                }
            }
            writer.endArray();
        } else {
            writer.beginObject();
            writeMessageFields(message);
            writer.endObject();
        }
    }

    private void writeMessageFields(PMessage message) throws IOException {
        for (PField field : message.$descriptor().allFields()) {
            Optional<?> val = message.optional(field.getId());
            if (val.isPresent()) {
                if (namedFields) {
                    writer.name(field.getProperty(JSON_NAME));
                } else {
                    writer.name("" + field.getId());
                }
                writeValue(val.get(), field.getResolvedDescriptor());
            }
        }
    }

    private void writeValue(Object value, PDescriptor descriptor) throws IOException {
        switch (descriptor.getType()) {
            case VOID: {
                writer.value(true);
                break;
            }
            case BOOL: {
                writer.value((Boolean) value);
                break;
            }
            case BYTE:
            case SHORT:
            case INT:
            case LONG:
            case FLOAT:
            case DOUBLE: {
                writer.value((Number) value);
                break;
            }
            case STRING: {
                writer.value((String) value);
                break;
            }
            case BINARY: {
                writer.value(((Binary) value).toBase64());
                break;
            }
            case ANY: {
                writeAny((PAny) value);
                break;
            }
            case LIST: {
                PDescriptor itemType = ((PListDescriptor<?>) descriptor).getItemType();
                writer.beginArray();
                List<?> list = (List<?>) value;
                for (Object o : list) {
                    writeValue(o, itemType);
                }
                writer.endArray();
                break;
            }
            case SET: {
                writer.beginArray();
                PDescriptor itemType = ((PSetDescriptor<?>) descriptor).getItemType();
                Set<?> list = (Set<?>) value;
                for (Object o : list) {
                    writeValue(o, itemType);
                }
                writer.endArray();
                break;
            }
            case MAP: {
                writer.beginObject();
                PMapDescriptor<?,?> type    = (PMapDescriptor<?,?>) descriptor;
                PDescriptor         keyType = type.getKeyType();
                PDescriptor valueType = type.getValueType();
                Map<?,?> map = (Map<?,?>) value;
                for (Map.Entry<?,?> entry : map.entrySet()) {
                    writeMapKey(entry.getKey(), keyType);
                    writeValue(entry.getValue(), valueType);
                }
                writer.endObject();
                break;
            }
            case ENUM: {
                if (namedEnums) {
                    writer.value(((PEnum) value).$meta().getProperty(JSON_NAME));
                } else {
                    writer.value(((PEnum) value).getValue());
                }
                break;
            }
            case MESSAGE: {
                writeMessage((PMessage) value);
                break;
            }
            default: {
                throw new IOException("Unhandled value type: " + descriptor.getTypeName());
            }
        }
    }

    private void writeMapKey(Object value, PDescriptor descriptor) throws IOException {
        switch (descriptor.getType()) {
            case BOOL:
            case BYTE:
            case SHORT:
            case INT:
            case LONG:
            case DOUBLE:
            case STRING:
                writer.name(value.toString());
                break;
            case FLOAT:
                writer.name(asString((float) value));
                break;
            case BINARY:
                writer.name(((Binary) value).toBase64());
                break;
            case ENUM:
                if (namedEnums) {
                    writer.name(((PEnum) value).$meta().getProperty(JSON_NAME));
                } else {
                    writer.name("" + ((PEnum) value).getValue());
                }
                break;
            case VOID:
            case MESSAGE:
            case LIST:
            case SET:
            case MAP:
            default:
                throw new IOException("Value type incompatible with map key: " + descriptor.getTypeName());
        }
    }
}
