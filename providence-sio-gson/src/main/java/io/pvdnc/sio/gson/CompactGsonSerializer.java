/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.gson;

import com.google.gson.stream.JsonWriter;
import io.pvdnc.core.PMessage;
import io.pvdnc.core.impl.AnyTypeNameFormat;
import io.pvdnc.core.io.PSerializer;
import io.pvdnc.core.types.PMessageDescriptor;
import net.morimekta.strings.io.Utf8StreamReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;
import static net.morimekta.collect.UnmodifiableList.listOf;

public class CompactGsonSerializer implements PSerializer {
    public static final String MEDIA_TYPE = "application/vnd.morimekta.providence.json";

    private final boolean strict;

    public CompactGsonSerializer() {
        this.strict = false;
    }

    protected CompactGsonSerializer(boolean strict) {
        this.strict = strict;
    }

    @Override
    public List<String> mediaTypes() {
        return listOf(MEDIA_TYPE);
    }

    @Override
    public <M extends PMessage> M readFrom(PMessageDescriptor<M> descriptor, InputStream in) throws IOException {
        return new GsonMessageReader(new Utf8StreamReader(in), strict).read(descriptor);
    }

    @Override
    public void writeTo(PMessage message, OutputStream out) throws IOException {
        Writer     rawWriter = new OutputStreamWriter(out, UTF_8);
        JsonWriter writer    = new JsonWriter(rawWriter);
        new GsonMessageWriter(null, writer, false, false, true, "@", AnyTypeNameFormat.DEFAULT).write(message);
    }

    @Override
    public boolean isStrict() {
        return strict;
    }

    @Override
    public CompactGsonSerializer strict() {
        if (strict) return this;
        return new CompactGsonSerializer(true);
    }

    // --- Object

    @Override
    public String toString() {
        return "CompactGsonSerializer{" +
               "strict=" + strict +
               '}';
    }
}
