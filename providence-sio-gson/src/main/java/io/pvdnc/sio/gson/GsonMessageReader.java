/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.gson;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import io.pvdnc.core.PAny;
import io.pvdnc.core.PEnum;
import io.pvdnc.core.PMessage;
import io.pvdnc.core.PMessageBuilder;
import io.pvdnc.core.impl.AnyBinary;
import io.pvdnc.core.impl.AnyTypeNameFormat;
import io.pvdnc.core.io.PMessageReader;
import io.pvdnc.core.registry.PSimpleTypeRegistry;
import io.pvdnc.core.registry.PTypeReference;
import io.pvdnc.core.registry.PTypeRegistry;
import io.pvdnc.core.types.PDeclaredDescriptor;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import net.morimekta.collect.ListBuilder;
import net.morimekta.collect.MapBuilder;
import net.morimekta.collect.SetBuilder;
import net.morimekta.collect.util.Binary;
import net.morimekta.strings.io.LineBufferedReader;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static io.pvdnc.core.property.PJsonProperties.JSON_ENUM_MAP;
import static io.pvdnc.core.property.PJsonProperties.JSON_FIELD_MAP;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.requireNonNull;
import static net.morimekta.collect.UnmodifiableList.asList;
import static net.morimekta.strings.EscapeUtil.javaEscape;

public class GsonMessageReader implements PMessageReader {
    private final JsonReader reader;
    private final boolean strict;
    private final PTypeRegistry typeRegistry;
    private final AnyTypeNameFormat anyTypeNameFormat;

    public GsonMessageReader(Reader in, boolean strict) {
        this(new JsonReader(new LineBufferedReader(in)), strict);
    }

    public GsonMessageReader(JsonReader reader, boolean strict) {
        this(reader, strict, PSimpleTypeRegistry.getInstance(), AnyTypeNameFormat.DEFAULT);
    }

    public GsonMessageReader(JsonReader reader, boolean strict, PTypeRegistry typeRegistry, AnyTypeNameFormat anyTypeNameFormat) {
        this.reader = reader;
        this.strict = strict;
        this.typeRegistry = requireNonNull(typeRegistry, "typeRegistry == null");
        this.anyTypeNameFormat = requireNonNull(anyTypeNameFormat, "anyTypeNameFormat == null");
    }

    @Override
    public <M extends PMessage> M read(PMessageDescriptor<M> descriptor) throws IOException {
        return readInternal(descriptor);
    }

    public PAny readAny() throws IOException {
        reader.beginObject();
        if (reader.peek() == JsonToken.END_OBJECT) {
            reader.endObject();
            return PAny.empty();
        }

        String name = reader.nextName();

        if (!PAny.TYPE_NAME_FIELD.contains(name)) {
            // it's binary...
            AnyBinary.Builder builder = AnyBinary.newBuilder();
            while (name != null) {
                PField field;
                if (NUMERIC_ID.matcher(name).matches()) {
                    field = AnyBinary.kDescriptor.findFieldById(Integer.parseInt(name));
                } else {
                    field = AnyBinary.kDescriptor.findFieldByName(name);
                }
                if (field == null) {
                    reader.skipValue();
                } else {
                    builder.set(field.getId(), readValue(field.getResolvedDescriptor()));
                }
                name = reader.nextName();
            }
            return builder.build();
        }
        String typeSpec = reader.nextString();
        AnyTypeNameFormat typeNameFormat = AnyTypeNameFormat.DEFAULT;
        PTypeReference reference = typeNameFormat.toReference(typeSpec);
        PMessageDescriptor<?> descriptor;
        try {
            PDeclaredDescriptor pdd = typeRegistry.descriptor(reference);
            if (!(pdd instanceof PMessageDescriptor)) {
                throw new IllegalArgumentException();
            }
            descriptor = (PMessageDescriptor<?>) pdd;
        } catch (IllegalArgumentException e) {
            AnyBinary.Builder anyBuilder = AnyBinary.newBuilder();
            anyBuilder.setMediaType("application/json");
            anyBuilder.setTypeName(reference.getTypeName());
            Map<String, Object> object = readObjectMapNoBegin(reader);
            anyBuilder.setData(Binary.encodeFromString(new Gson().toJson(object), UTF_8));
            return anyBuilder.build();
        }
        PMessage message = readMessageFields(descriptor);
        return PAny.wrap(message);
    }

    static Map<String, Object> readObject(JsonReader reader) throws IOException {
        reader.beginObject();
        return readObjectMapNoBegin(reader);
    }

    private static Map<String, Object> readObjectMapNoBegin(JsonReader reader) throws IOException {
        Map<String, Object> object = new LinkedHashMap<>();
        while (reader.peek() != JsonToken.END_OBJECT) {
            object.put(reader.nextName(), readValue(reader));
        }
        reader.endObject();
        return object;
    }

    private static Object readValue(JsonReader reader) throws IOException {
        switch (reader.peek()) {
            case STRING:
                return reader.nextString();
            case NUMBER:
                String num = reader.nextString();
                try {
                    return Long.parseLong(num);
                } catch (NumberFormatException nfe) {
                    return Double.parseDouble(num);
                }
            case BOOLEAN:
                return reader.nextBoolean();
            case NULL:
                reader.skipValue();
                return null;
            case BEGIN_ARRAY:
                List<Object> list = new ArrayList<>();
                reader.beginArray();
                while (reader.peek() != JsonToken.END_ARRAY) {
                    list.add(readValue(reader));
                }
                reader.endArray();
                return list;
            case BEGIN_OBJECT:
                reader.beginObject();
                return readObjectMapNoBegin(reader);
            case NAME:
            case END_ARRAY:
            case END_OBJECT:
            case END_DOCUMENT:
            default:
                throw new IOException("Unknown value token: " + reader.peek());
        }
    }

    @SuppressWarnings("unchecked")
    public <M extends PMessage> M readInternal(PMessageDescriptor<M> descriptor) throws IOException {
        if (reader.peek() == JsonToken.BEGIN_ARRAY) {
            List<PField> fields = asList(descriptor.allFields());
            PMessageBuilder builder = descriptor.newBuilder();

            reader.beginArray();
            int idx = 0;
            while (reader.peek() != JsonToken.END_ARRAY) {
                if (idx >= fields.size()) {
                    if (strict) {
                        throw new IOException("Data after last field at " + reader.getPath());
                    }
                    reader.skipValue();
                } else if (reader.peek() == JsonToken.NULL) {
                    reader.nextNull();
                } else {
                    PField field = fields.get(idx);
                    builder.set(field.getId(), readValue(field.getResolvedDescriptor()));
                }
                idx++;
            }
            reader.endArray();
            return (M) builder.build();
        } else {
            reader.beginObject();
            return readMessageFields(descriptor);
        }
    }

    @SuppressWarnings("unchecked")
    private <M extends PMessage> M readMessageFields(PMessageDescriptor<M> descriptor) throws IOException {
        PMessageBuilder builder = descriptor.newBuilder();
        Map<String, PField> jsonFields = descriptor.getProperty(JSON_FIELD_MAP);
        while (reader.peek() != JsonToken.END_OBJECT) {
            String name = reader.nextName();
            PField field;
            if (NUMERIC_ID.matcher(name).matches()) {
                field = descriptor.findFieldById(Integer.parseInt(name));
            } else {
                field = descriptor.findFieldByName(name);
                if (field == null) {
                    field = jsonFields.get(name);
                }
            }
            if (field == null) {
                if (strict) {
                    throw new IOException("No field " + name + " at " + reader.getPath());
                }
                reader.skipValue();
            } else {
                Object o = readValue(field.getResolvedDescriptor());
                if (o != null) {
                    builder.set(field.getId(), o);
                } else if (strict) {
                    throw new IOException("Null value at " + reader.getPath());
                }
            }
        }
        reader.endObject();
        return (M) builder.build();
    }

    private Object readValue(PDescriptor descriptor) throws IOException {
        if (reader.peek() == JsonToken.NULL) {
            return null;
        }
        switch (descriptor.getType()) {
            case VOID: {
                // ignore the actual value.
                reader.nextBoolean();
                return Boolean.TRUE;
            }
            case BOOL: {
                return reader.nextBoolean();
            }
            case BYTE: {
                return (byte) reader.nextInt();
            }
            case SHORT: {
                return (short) reader.nextInt();
            }
            case INT: {
                return reader.nextInt();
            }
            case LONG: {
                return reader.nextLong();
            }
            case FLOAT: {
                return (float) reader.nextDouble();
            }
            case DOUBLE: {
                return reader.nextDouble();
            }
            case STRING: {
                return reader.nextString();
            }
            case BINARY: {
                return Binary.fromBase64(reader.nextString());
            }
            case ANY: {
                return readAny();
            }
            case LIST: {
                @SuppressWarnings("unchecked")
                PListDescriptor<Object> type = (PListDescriptor<Object>) descriptor;
                ListBuilder<Object> list = type.builder(10);
                reader.beginArray();
                while (reader.peek() != JsonToken.END_ARRAY) {
                    Object value = readValue(type.getItemType());
                    if (value != null) {
                        list.add(value);
                    } else if (strict) {
                        throw new IOException("No value at " + reader.getPath());
                    }
                }
                reader.endArray();
                return list.build();
            }
            case SET: {
                @SuppressWarnings("unchecked")
                PSetDescriptor<Object> type = (PSetDescriptor<Object>) descriptor;
                SetBuilder<Object> set = type.builder(10);
                reader.beginArray();
                while (reader.peek() != JsonToken.END_ARRAY) {
                    Object value = readValue(type.getItemType());
                    if (value != null) {
                        set.add(value);
                    } else if (strict) {
                        throw new IOException("No value at " + reader.getPath());
                    }
                }
                reader.endArray();
                return set.build();
            }
            case MAP: {
                @SuppressWarnings("unchecked")
                PMapDescriptor<Object, Object> type = (PMapDescriptor<Object, Object>) descriptor;
                MapBuilder<Object, Object> map = type.builder(10);
                reader.beginObject();
                while (reader.peek() != JsonToken.END_OBJECT) {
                    Object key = readKeyValue(type.getKeyType());
                    Object value = readValue(type.getValueType());
                    if (value != null) {
                        map.put(key, value);
                    } else if (strict) {
                        throw new IOException("No map value at " + reader.getPath());
                    }
                }
                reader.endObject();
                return map.build();
            }
            case ENUM: {
                PEnumDescriptor ed = (PEnumDescriptor) descriptor;
                if (reader.peek() == JsonToken.STRING) {
                    String name = reader.nextString();
                    PEnum tmp = ed.findByName(name);
                    if (tmp == null) {
                        Map<String, PEnum> jsonMap = ed.getProperty(JSON_ENUM_MAP);
                        return jsonMap.get(name);
                    }
                    return tmp;
                } else {
                    return ed.findById(reader.nextInt());
                }
            }
            case MESSAGE: {
                return readInternal((PMessageDescriptor<?>) descriptor);
            }
            default: {
                // Usually not testable.
                throw new IOException("Unhandled value type: " + descriptor.getTypeName());
            }
        }
    }

    private Object readKeyValue(PDescriptor descriptor) throws IOException {
        String name = reader.nextName();
        if (name == null) {
            throw new IOException("No name at " + reader.getPath());
        }
        try {
            switch (descriptor.getType()) {
                case BOOL:
                    return Boolean.parseBoolean(name);
                case BYTE:
                    return Byte.parseByte(name);
                case SHORT:
                    return Short.parseShort(name);
                case INT:
                    return Integer.parseInt(name);
                case LONG:
                    return Long.parseLong(name);
                case FLOAT:
                    return Float.parseFloat(name);
                case DOUBLE:
                    return Double.parseDouble(name);
                case STRING:
                    return name;
                case BINARY:
                    return Binary.fromBase64(name);
                case ENUM: {
                    PEnumDescriptor ed = (PEnumDescriptor) descriptor;
                    if (NUMERIC_ID.matcher(name).matches()) {
                        return ed.valueForId(Integer.parseInt(name));
                    } else {
                        PEnum tmp = ed.findByName(name);
                        if (tmp == null) {
                            Map<String, PEnum> jsonMap = ed.getProperty(JSON_ENUM_MAP);
                            tmp = jsonMap.get(name);
                        }
                        if (tmp == null) {
                            throw new IOException("No " + ed.getTypeName() + " value for name '" + javaEscape(name) + "'");
                        }
                        return tmp;
                    }
                }
                case VOID:
                case LIST:
                case SET:
                case MAP:
                case MESSAGE:
                default: {
                    throw new IOException("Not a valid map key type: " + descriptor.getTypeName());
                }
            }
        } catch (NumberFormatException e) {
            throw new IOException("Invalid numeric key: '" + javaEscape(name) + "'", e);
        } catch (IllegalArgumentException e) {
            throw new IOException(e.getMessage(), e);
        }
    }

    private static final Pattern NUMERIC_ID = Pattern.compile("^(0|-?[1-9][0-9]*)$");
}
