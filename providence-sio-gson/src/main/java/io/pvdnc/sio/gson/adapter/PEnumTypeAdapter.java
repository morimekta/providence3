/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.gson.adapter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import io.pvdnc.core.PEnum;
import io.pvdnc.core.types.PEnumDescriptor;

import java.io.IOException;
import java.util.Map;

import static java.util.Objects.requireNonNull;
import static io.pvdnc.core.property.PJsonProperties.JSON_ENUM_MAP;
import static io.pvdnc.core.property.PJsonProperties.JSON_NAME;

public class PEnumTypeAdapter<Enum extends PEnum> extends TypeAdapter<Enum> {
    private final PEnumDescriptor    descriptor;
    private final Map<String, PEnum> jsonNames;

    public PEnumTypeAdapter(PEnumDescriptor descriptor) {
        this.descriptor = requireNonNull(descriptor, "descriptor == null");
        this.jsonNames = descriptor.getProperty(JSON_ENUM_MAP);
    }

    @Override
    public void write(JsonWriter out, Enum value) throws IOException {
        out.value(value.$meta().getProperty(JSON_NAME));
    }

    @Override
    @SuppressWarnings("unchecked")
    public Enum read(JsonReader in) throws IOException {
        PEnum value;
        if (in.peek() == JsonToken.NUMBER) {
            value = descriptor.findById(in.nextInt());
        } else {
            String next = in.nextString();
            value = descriptor.findByName(next);
            if (value == null) {
                value = jsonNames.get(next);
            }
        }
        return (Enum) value;
    }
}
