/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.gson.adapter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.pvdnc.core.PMessage;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.sio.gson.GsonMessageWriter;
import io.pvdnc.sio.gson.GsonMessageReader;

import java.io.IOException;

import static java.util.Objects.requireNonNull;

public class PMessageTypeAdapter<M extends PMessage> extends TypeAdapter<M> {
    private final PMessageDescriptor<M> descriptor;

    public PMessageTypeAdapter(PMessageDescriptor<M> descriptor) {
        this.descriptor = requireNonNull(descriptor, "descriptor == null");
    }

    @Override
    public void write(JsonWriter out, M value) throws IOException {
        new GsonMessageWriter(out).write(value);
    }

    @Override
    public M read(JsonReader in) throws IOException {
        return new GsonMessageReader(in, false).read(descriptor);
    }
}
