/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.gson.adapter;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import io.pvdnc.core.PEnum;
import io.pvdnc.core.types.PEnumDescriptor;

import java.lang.reflect.ParameterizedType;
import java.util.Map;

public class PEnumTypeAdapterFactory implements TypeAdapterFactory {
    @Override
    @SuppressWarnings("unchecked")
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
        try {
            if (PEnum.class.isAssignableFrom(type.getRawType())) {
                PEnumDescriptor descriptor = PEnumDescriptor.getEnumDescriptor(type.getRawType());
                return (TypeAdapter<T>) new PEnumTypeAdapter<>(descriptor);
            } else if (Map.class.isAssignableFrom(type.getRawType())) {
                if (type.getType() instanceof ParameterizedType) {
                    ParameterizedType pt = (ParameterizedType) type.getType();
                    if (pt.getActualTypeArguments().length == 2) {
                        if (pt.getActualTypeArguments()[0] instanceof Class) {
                            Class<?> keyType = (Class<?>) pt.getActualTypeArguments()[0];
                            if (PEnum.class.isAssignableFrom(keyType)) {
                                PEnumDescriptor descriptor = PEnumDescriptor.getEnumDescriptor(keyType);
                                TypeAdapter<?> valueAdapter = gson.getAdapter((Class<?>) pt.getActualTypeArguments()[1]);
                                return (TypeAdapter<T>) new PEnumMapKeyTypeAdapter<>(
                                        descriptor, valueAdapter, (Class<? extends Map<?,?>>) type.getRawType());
                            }
                        }
                    }
                }
            }
        } catch (IllegalArgumentException ignore) {
            // ignore
        }
        return null;
    }
}
