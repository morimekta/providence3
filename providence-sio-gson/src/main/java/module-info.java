import io.pvdnc.core.io.PSerializer;
import io.pvdnc.sio.gson.CompactGsonSerializer;
import io.pvdnc.sio.gson.GsonSerializer;

module io.pvdnc.sio.gson {
    requires transitive io.pvdnc.core;
    requires net.morimekta.collect;
    requires net.morimekta.strings;
    requires com.google.gson;

    exports io.pvdnc.sio.gson;
    exports io.pvdnc.sio.gson.adapter;

    provides PSerializer with
            GsonSerializer,
            CompactGsonSerializer;
}