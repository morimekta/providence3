# VERSION := 1.7.1
VERSION := $(shell cat pom.xml | grep "^    <version>" | sed -e 's:.*<version>::' -e 's:</version>.*::')
THRIFT_VERSION := $(shell cat pom.xml | grep ".*<thrift.version>" | sed -e 's:.*<thrift.version>::' -e 's:</thrift.version>.*::')
USER_UG := $(shell echo `id -u`:`id -g`)

######################
## -- PROVIDENCE -- ##
######################

PVD_DONE      := $(patsubst %/.java-gen.marker,%/.java-gen.done, $(wildcard */src/main/providence/.java-gen.marker))
PVD_TEST_DONE := $(patsubst %/.java-gen.marker,%/.java-gen.done, $(wildcard */src/test/providence/.java-gen.marker))

%/src/main/providence/.java-gen.done: %/src/main/providence/.java-gen.marker
	@echo "-- $<"
	@cd $(patsubst %/src/main/providence/.java-gen.marker,%,$<) && \
        mvn io.pvdnc:providence-maven-plugin:$(VERSION):generate \
           -Dprovidence.gen.write_generator_version=false \
           -Dprovidence.gen.write_generated_date=false \
           -Dprovidence.main.output=src/main/java-gen
	@touch $@

%/src/test/providence/.java-gen.done: %/src/test/providence/.java-gen.marker
	@echo "-- $<"
	@cd $(patsubst %/src/test/providence/.java-gen.marker,%,$<) && \
        mvn io.pvdnc:providence-maven-plugin:$(VERSION):testGenerate \
            -Dprovidence.gen.write_generator_version=false \
            -Dprovidence.gen.write_generated_date=false \
            -Dprovidence.test.output=src/test/java-gen
	@touch $@

providence: $(PVD_DONE) $(PVD_TEST_DONE)

##################
## -- THRIFT -- ##
##################

%.thrift.done: %.thrift
	$(eval OUT=$(shell dirname $< | sed 's:/thrift:/java-gen:'))
	mkdir -p $(OUT)
	docker container run --user $(USER_UG) --rm -ti -v ${PWD}:${PWD} -w ${PWD} thrift:$(THRIFT_VERSION) \
	thrift --out $(OUT) --gen java:generated_annotations=suppress,private-members,fullcamel $<
	touch $@

TEST_THRIFT_FILES=$(wildcard */src/test/thrift/*.thrift)
TEST_THRIFT_LOCKS=$(patsubst %.thrift,%.thrift.done, $(TEST_THRIFT_FILES))

test-thrift: $(TEST_THRIFT_LOCKS)

clean:
	@rm -rf $(THRIFT_LOCKS) $(TEST_THRIFT_LOCKS)
	@rm -rf ..does-not-exist $(wildcard */src/*/providence/.java-gen.done)


.PHONY: clean test-thrift providence
