module io.pvdnc.rpc.http.servlet {
    exports io.pvdnc.rpc.http.servlet;

    requires io.pvdnc.core;
    requires jakarta.servlet;
}