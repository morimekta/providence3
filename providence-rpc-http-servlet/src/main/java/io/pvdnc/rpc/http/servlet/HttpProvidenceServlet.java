/*
 * Copyright 2021 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.rpc.http.servlet;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.io.PSerializer;
import io.pvdnc.core.io.PSerializerProvider;
import io.pvdnc.core.rpc.PServiceProcessor;
import io.pvdnc.core.types.PServiceMethod;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static java.lang.System.Logger.Level.DEBUG;
import static java.lang.System.Logger.Level.ERROR;
import static java.lang.System.Logger.Level.INFO;
import static java.lang.System.Logger.Level.WARNING;
import static java.util.Objects.requireNonNull;

/**
 * A basic javax servlet that
 */
public class HttpProvidenceServlet extends HttpServlet {
    private static final long serialVersionUID = 912005027126144857L;
    private static final System.Logger LOGGER = System.getLogger(HttpProvidenceServlet.class.getName());

    private transient final HttpProcessorProvider httpProcessorProvider;
    private transient final PSerializerProvider serializerProvider;
    private final boolean sizedOutput;

    public HttpProvidenceServlet(HttpProcessorProvider httpProcessorProvider) {
        this(httpProcessorProvider, PSerializerProvider.getInstance(), false);
    }

    public HttpProvidenceServlet(HttpProcessorProvider httpProcessorProvider, PSerializerProvider serializerProvider, boolean sizedOutput) {
        this.httpProcessorProvider = requireNonNull(httpProcessorProvider, "httpProcessorProvider == null");
        this.serializerProvider = requireNonNull(serializerProvider, "serializerProvider == null");
        this.sizedOutput = sizedOutput;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try (PServiceProcessor processor = httpProcessorProvider.processorForRequest(req)) {
            String requestPath = req.getPathInfo();
            if (requestPath.startsWith("/")) {
                requestPath = requestPath.substring(1);
            }
            if (requestPath.contains("?")) {
                requestPath = requestPath.substring(0, requestPath.indexOf('?'));
            }
            PServiceMethod method = processor.$descriptor().findMethodForName(requestPath);
            if (method == null) {
                LOGGER.log(DEBUG, "Unknown method for: " + req.getPathInfo());
                resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Unknown method for: " + req.getPathInfo());
                return;
            }

            PSerializer requestSerializer = serializerProvider.defaultSerializer();
            if (req.getContentType() != null) {
                try {
                    requestSerializer = serializerProvider.forMediaType(req.getContentType())
                            .orElseThrow(() -> new IllegalArgumentException(""));
                } catch (IllegalArgumentException e) {
                    LOGGER.log(INFO, "Unknown content type in request", e);
                    resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Unknown content-type: " + req.getContentType());
                    return;
                }
            } else {
                LOGGER.log(DEBUG, "Request is missing content type.");
            }

            PSerializer responseSerializer = requestSerializer;
            String acceptHeader = req.getHeader("Accept");
            if (acceptHeader != null) {
                String[] entries = acceptHeader.split(",");
                for (String entry : entries) {
                    entry = entry.trim();
                    if (entry.isEmpty()) {
                        continue;
                    }
                    if ("*/*".equals(entry)) {
                        // Then responding same as request is good.
                        break;
                    }

                    try {
                        responseSerializer = serializerProvider.forMediaType(entry)
                                .orElseThrow(() -> new IllegalArgumentException(""));
                        break;
                    } catch (IllegalArgumentException ignore) {
                        // Ignore. Bad header input is pretty common.
                    }
                }
            }

            PMessage request;
            try (InputStream in = req.getInputStream()) {
                request = requestSerializer.readFrom(method.getRequestType(), in);
                // TODO: Check?
                // requestSerializer.verifyEndOfContent(in);
            } catch (IOException e) {
                LOGGER.log(INFO, "Bad request content:" + e.getMessage(), e);
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Bad request content: " + e.getMessage());
                return;
            }

            PMessage response;
            try {
                response = processor.handleCall(method, request);
            } catch (Exception e) {
                LOGGER.log(WARNING, "Execution error calling " + method.getName(), e);
                resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Execution error");
                return;
            }

            if (response != null) {
                resp.setContentType(responseSerializer.mediaTypes().get(0));
                try (OutputStream out = new BufferedOutputStream(resp.getOutputStream())) {
                    if (sizedOutput) {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        responseSerializer.writeTo(response, baos);
                        resp.setContentLength(baos.size());
                        out.write(baos.toByteArray());
                    } else {
                        responseSerializer.writeTo(response, out);
                    }
                } catch (IOException e) {
                    if (!resp.isCommitted()) {
                        LOGGER.log(WARNING, "Error serializing " + response.$descriptor().getTypeName(), e);
                        resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Serialization error");
                    }
                }
            } else {
                resp.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
            resp.flushBuffer();
        } catch (Exception e) {
            LOGGER.log(ERROR, "Unhandled exception: " + e.getMessage(), e);
            if (!resp.isCommitted()) {
                resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Unhandled exception");
            }
        }
    }
}
