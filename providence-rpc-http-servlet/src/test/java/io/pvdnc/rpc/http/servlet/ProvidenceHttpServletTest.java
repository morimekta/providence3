package io.pvdnc.rpc.http.servlet;

import io.pvdnc.rpc.client.test.PvdCompact;
import io.pvdnc.rpc.client.test.PvdService;
import io.pvdnc.rpc.client.test.PvdUnion;
import io.pvdnc.rpc.client.test.PvdWrapper;
import io.pvdnc.rpc.http.client.HttpClientHandler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class ProvidenceHttpServletTest {
    private Server server;
    private int port;

    private final PvdService impl = Mockito.mock(PvdService.class);

    @BeforeEach
    public void setUp() throws Exception {
        server = new Server(0);

        ServletContextHandler handler = new ServletContextHandler();
        handler.addServlet(new ServletHolder(
                new HttpProvidenceServlet(rq -> new PvdService.$Processor(impl))), "/test/*");

        server.setHandler(handler);
        server.start();

        port = ((ServerConnector) server.getConnectors()[0]).getLocalPort();
    }

    @AfterEach
    public void tearDown() throws Exception {
        if (server != null) {
            server.stop();
            server.destroy();
        }
    }

    private PvdService newClient() {
        return PvdService.newClient(new HttpClientHandler("http://localhost:" + port + "/test"));
    }

    @Test
    public void testServlet() throws IOException {
        Mockito.when(impl.fullMethod(PvdCompact.newBuilder()
                .setI(42)
                .build())).thenReturn(PvdWrapper.newBuilder()
                .setMsgUnion(PvdUnion.withText("foo"))
                .build());

        PvdService client = newClient();
        PvdWrapper response = client.fullMethod(PvdCompact.newBuilder()
                .setI(42)
                .build());

        assertThat(response, is(notNullValue()));
        assertThat(response.hasMsgUnion(), is(true));
        assertThat(response.getMsgUnion().getText(), is("foo"));
    }
}
