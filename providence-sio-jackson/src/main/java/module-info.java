import io.pvdnc.sio.jackson.ProvidenceModule;

module io.pvdnc.sio.jackson {
    requires io.pvdnc.core;
    requires net.morimekta.collect;
    requires net.morimekta.strings;

    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.core;

    exports io.pvdnc.sio.jackson;

    opens io.pvdnc.sio.jackson to com.fasterxml.jackson.databind;

    provides com.fasterxml.jackson.databind.Module with
            ProvidenceModule;
}