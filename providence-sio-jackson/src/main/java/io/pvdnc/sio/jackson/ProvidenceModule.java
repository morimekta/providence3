/*
 * Copyright 2017 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.jackson;

import com.fasterxml.jackson.databind.module.SimpleModule;
import io.pvdnc.core.PAny;
import io.pvdnc.core.PEnum;
import io.pvdnc.core.PMessage;
import io.pvdnc.sio.jackson.adapter.AnyDeserializer;
import io.pvdnc.sio.jackson.adapter.AnySerializer;
import io.pvdnc.sio.jackson.adapter.BinaryDeserializer;
import io.pvdnc.sio.jackson.adapter.BinaryKeyDeserializer;
import io.pvdnc.sio.jackson.adapter.BinaryKeySerializer;
import io.pvdnc.sio.jackson.adapter.BinarySerializer;
import io.pvdnc.sio.jackson.adapter.PEnumKeySerializer;
import io.pvdnc.sio.jackson.adapter.PEnumSerializer;
import io.pvdnc.sio.jackson.adapter.PMessageSerializer;
import io.pvdnc.sio.jackson.adapter.ProvidenceDeserializers;
import net.morimekta.collect.util.Binary;

public class ProvidenceModule extends SimpleModule {
    public ProvidenceModule() {
        super("providence-sio-jackson");
        setDeserializers(new ProvidenceDeserializers());
        registerSubtypes(PEnum.class, PMessage.class, PAny.class);

        addSerializer(Binary.class, new BinarySerializer());
        addDeserializer(Binary.class, new BinaryDeserializer());
        addKeySerializer(Binary.class, new BinaryKeySerializer());
        addKeyDeserializer(Binary.class, new BinaryKeyDeserializer());

        addSerializer(PEnum.class, new PEnumSerializer());
        addKeySerializer(PEnum.class, new PEnumKeySerializer());

        addSerializer(PMessage.class, new PMessageSerializer());

        addSerializer(PAny.class, AnySerializer.INSTANCE);
        addDeserializer(PAny.class, AnyDeserializer.INSTANCE);
    }
}
