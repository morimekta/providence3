/*
 * Copyright 2017 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.jackson.adapter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.pvdnc.core.PAny;
import io.pvdnc.core.PMessageBuilder;
import io.pvdnc.core.impl.AnyBinary;
import io.pvdnc.core.impl.AnyTypeNameFormat;
import io.pvdnc.core.registry.PSimpleTypeRegistry;
import io.pvdnc.core.registry.PTypeReference;
import io.pvdnc.core.types.PDeclaredDescriptor;
import net.morimekta.collect.ListBuilder;
import net.morimekta.collect.MapBuilder;
import net.morimekta.collect.SetBuilder;
import net.morimekta.collect.util.Binary;
import io.pvdnc.core.PMessage;
import io.pvdnc.core.property.PJsonProperties;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PSetDescriptor;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import static com.fasterxml.jackson.core.JsonToken.END_ARRAY;
import static com.fasterxml.jackson.core.JsonToken.START_ARRAY;
import static com.fasterxml.jackson.core.JsonToken.START_OBJECT;
import static com.fasterxml.jackson.core.JsonToken.VALUE_FALSE;
import static com.fasterxml.jackson.core.JsonToken.VALUE_NULL;
import static com.fasterxml.jackson.core.JsonToken.VALUE_NUMBER_FLOAT;
import static com.fasterxml.jackson.core.JsonToken.VALUE_NUMBER_INT;
import static com.fasterxml.jackson.core.JsonToken.VALUE_STRING;
import static com.fasterxml.jackson.core.JsonToken.VALUE_TRUE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static net.morimekta.collect.UnmodifiableList.asList;

public class PMessageDeserializer<M extends PMessage> extends JsonDeserializer<M> {
    private static final Pattern NUMERIC_ID = Pattern.compile("(0|-?[1-9][0-9]*)");

    private final PMessageDescriptor<M> descriptor;

    public PMessageDeserializer(PMessageDescriptor<M> descriptor) {
        this.descriptor = descriptor;
    }

    @Override
    public M deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException {
        return parseMessage(jsonParser, jsonParser.currentToken(), descriptor);
    }

    @SuppressWarnings("unchecked")
    private static <M extends PMessage> M parseMessage(JsonParser jsonParser, JsonToken next, PMessageDescriptor<M> descriptor)
            throws IOException {
        if (isTokenType(jsonParser.currentToken(), VALUE_NULL)) {
            return null;
        } else if (isTokenType(next, JsonToken.START_OBJECT)) {
            return parseMessageFields(jsonParser, jsonParser.nextToken(), descriptor);
        } else if (isTokenType(next, START_ARRAY)) {
            PMessageBuilder builder = descriptor.newBuilder();
            if (!descriptor.getProperty(PJsonProperties.JSON_COMPACT)) {
                throw new IOException("Array notation not allowed for " + descriptor.getTypeName());
            }
            next = jsonParser.nextToken();
            int          idx    = -1;
            List<PField> fields = asList(descriptor.allFields());
            while (!isTokenType(next, END_ARRAY)) {
                ++idx;
                if (idx >= fields.size()) {
                    // just consume all remaining values
                    while (jsonParser.nextValue() != null) {
                        jsonParser.skipChildren();
                    }
                    break;
                }
                if (isTokenType(next, VALUE_NULL)) {
                    next = jsonParser.nextToken();
                    continue;
                }
                PField field = fields.get(idx);
                Object value = parseValue(jsonParser, next, field.getResolvedDescriptor());
                builder.set(field.getId(), value);
                next = jsonParser.nextToken();
            }
            return (M) builder.build();
        } else {
            throw new IOException("Unknown start of object for " + descriptor.getTypeName() + ": " + next);
        }
    }

    @SuppressWarnings("unchecked")
    protected static <M extends PMessage> M parseMessageFields(JsonParser jsonParser, JsonToken next, PMessageDescriptor<M> descriptor) throws IOException {
        PMessageBuilder builder = descriptor.newBuilder();
        while (!next.isStructEnd()) {
            String fieldName = jsonParser.currentName();
            PField field;
            if (NUMERIC_ID.matcher(fieldName).matches()) {
                field = descriptor.findFieldById(Integer.parseInt(fieldName));
            } else {
                field = descriptor.findFieldByName(fieldName);
                if (field == null) {
                    field = descriptor.getProperty(PJsonProperties.JSON_FIELD_MAP).get(fieldName);
                }
            }
            next = jsonParser.nextValue();
            if (field == null) {
                jsonParser.skipChildren();
                next = jsonParser.nextToken();
                continue;
            }
            Object value = parseValue(jsonParser, next, field.getResolvedDescriptor());
            builder.set(field.getId(), value);
            next = jsonParser.nextToken();
        }
        return (M) builder.build();
    }

    private static PAny parseAny(JsonParser jsonParser, JsonToken next) throws IOException {
        ObjectNode object = jsonParser.readValueAsTree();
        if (object.isEmpty()) {
            System.err.println(next);
            return PAny.empty();
        }
        String first = object.fieldNames().next();
        if (!PAny.TYPE_NAME_FIELD.contains(first)) {
            JsonParser parser = new ObjectMapper().treeAsTokens(object);
            return parseMessage(parser, parser.nextToken(), AnyBinary.kDescriptor);
        }

        PTypeReference ref = AnyTypeNameFormat.DEFAULT.toReference(String.valueOf(object.get(first)));
        try {
            PDeclaredDescriptor descriptor = PSimpleTypeRegistry.getInstance().descriptor(ref);
            JsonParser parser = new ObjectMapper().treeAsTokens(object);
            PMessage message = parseMessage(parser, parser.nextToken(), (PMessageDescriptor<? extends PMessage>) descriptor);
            return PAny.wrap(message);
        } catch (IOException | IllegalArgumentException e) {
            e.printStackTrace();
            String content = new ObjectMapper().writeValueAsString(object);
            return AnyBinary.newBuilder()
                    .setTypeName(ref.getTypeName())
                    .setMediaType("application/json")
                    .setData(Binary.encodeFromString(content, UTF_8))
                    .build();
        }
    }

    private static Object parseValue(JsonParser jsonParser, JsonToken next, PDescriptor descriptor) throws IOException {
        if (isTokenType(next, VALUE_NULL)) {
            return null;
        }
        switch (descriptor.getType()) {
            case VOID:
                validateTokenType(next, VALUE_TRUE);
                return jsonParser.getValueAsBoolean();
            case BOOL:
                validateTokenType(next, VALUE_TRUE, VALUE_FALSE);
                return jsonParser.getValueAsBoolean();
            case BYTE:
                validateTokenType(next, VALUE_NUMBER_INT);
                return (byte) jsonParser.getValueAsInt();
            case SHORT:
                validateTokenType(next, VALUE_NUMBER_INT);
                return (short) jsonParser.getValueAsInt();
            case INT:
                validateTokenType(next, VALUE_NUMBER_INT);
                return jsonParser.getValueAsInt();
            case LONG:
                validateTokenType(next, VALUE_NUMBER_INT);
                return jsonParser.getValueAsLong();
            case FLOAT:
                validateTokenType(next, VALUE_NUMBER_FLOAT, VALUE_NUMBER_INT);
                return (float) jsonParser.getValueAsDouble();
            case DOUBLE:
                validateTokenType(next, VALUE_NUMBER_FLOAT, VALUE_NUMBER_INT);
                return jsonParser.getValueAsDouble();
            case STRING:
                validateTokenType(next, VALUE_STRING);
                return jsonParser.getValueAsString();
            case BINARY:
                validateTokenType(next, VALUE_STRING);
                return Binary.fromBase64(jsonParser.getValueAsString());
            case ANY:
                validateTokenType(next, START_OBJECT);
                return parseAny(jsonParser, next);
            case LIST: {
                validateTokenType(next, START_ARRAY);
                @SuppressWarnings("unchecked")
                PListDescriptor<Object> list = (PListDescriptor<Object>) descriptor;
                ListBuilder<Object> builder = list.builder(4);
                next = jsonParser.nextToken();
                while (!isTokenType(next, END_ARRAY)) {
                    Object value = parseValue(jsonParser, next, list.getItemType());
                    if (value != null) {
                        builder.add(value);
                    }
                    next = jsonParser.nextToken();
                }
                return builder.build();
            }
            case SET: {
                validateTokenType(next, START_ARRAY);
                @SuppressWarnings("unchecked")
                PSetDescriptor<Object> set = (PSetDescriptor<Object>) descriptor;
                SetBuilder<Object> builder = set.builder(4);
                next = jsonParser.nextToken();
                while (!isTokenType(next, END_ARRAY)) {
                    Object value = parseValue(jsonParser, next, set.getItemType());
                    if (value != null) {
                        builder.add(value);
                    }
                    next = jsonParser.nextToken();
                }
                return builder.build();
            }
            case MAP: {
                validateTokenType(next, START_OBJECT);
                @SuppressWarnings("unchecked")
                PMapDescriptor<Object,Object> map = (PMapDescriptor<Object,Object>) descriptor;
                MapBuilder<Object,Object> builder = map.builder(4);
                String keyName = jsonParser.nextFieldName();
                while (keyName != null) {
                    Object key = parseMapKey(keyName, map.getKeyType());
                    Object value = parseValue(jsonParser, jsonParser.nextValue(), map.getValueType());
                    if (key != null && value != null) {
                        builder.put(key, value);
                    }
                    keyName = jsonParser.nextFieldName();
                }
                return builder.build();
            }
            case ENUM: {
                validateTokenType(next, VALUE_STRING, VALUE_NUMBER_INT);
                PEnumDescriptor ed = (PEnumDescriptor) descriptor;
                if (next.isNumeric()) {
                    return ed.findById(jsonParser.getValueAsInt());
                } else {
                    return ed.findByName(jsonParser.getValueAsString());
                }
            }
            case MESSAGE:
                return parseMessage(jsonParser, next, (PMessageDescriptor<?>) descriptor);
        }
        throw new IllegalStateException("Unhandled value type: " + descriptor.getType());
    }

    private static Object parseMapKey(String keyName, PDescriptor keyType) throws IOException {
        switch (keyType.getType()) {
            case BOOL:
                return Boolean.parseBoolean(keyName);
            case BYTE:
                return Byte.parseByte(keyName);
            case SHORT:
                return Short.parseShort(keyName);
            case INT:
                return Integer.parseInt(keyName);
            case LONG:
                return Long.parseLong(keyName);
            case FLOAT:
                return Float.parseFloat(keyName);
            case DOUBLE:
                return Double.parseDouble(keyName);
            case STRING:
                return keyName;
            case BINARY:
                return Binary.fromBase64(keyName);
            case ENUM: {
                PEnumDescriptor ed = (PEnumDescriptor) keyType;
                if (NUMERIC_ID.matcher(keyName).matches()) {
                    return ed.findById(Integer.parseInt(keyName));
                }
                return Optional.ofNullable(ed.findByName(keyName))
                               .orElseGet(() -> ed.getProperty(PJsonProperties.JSON_ENUM_MAP).get(keyName));
            }
        }
        throw new IOException("Not allowed in JSON key: " + keyType.getType());
    }

    public static boolean isTokenType(JsonToken token, JsonToken type) {
        return token.id() == type.id();
    }

    public static void validateTokenType(JsonToken token, JsonToken type) throws IOException {
        if (token.id() != type.id()) {
            throw new IOException("Expected " + type.toString() + " but got " + token);
        }
    }

    public static void validateTokenType(JsonToken token, JsonToken typeA, JsonToken typeB) throws IOException {
        if (token.id() != typeA.id() && token.id() != typeB.id()) {
            throw new IOException("Expected " + typeA.toString() + " or " + typeB.toString() + " but got " + token);
        }
    }
}
