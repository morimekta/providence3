/*
 * Copyright 2017 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.jackson.adapter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import io.pvdnc.core.PAny;
import io.pvdnc.core.PMessage;
import io.pvdnc.core.impl.AnyBinary;

import java.io.IOException;

import static io.pvdnc.sio.jackson.adapter.PMessageSerializer.writeMessageFields;
import static net.morimekta.collect.UnmodifiableList.asList;
import static net.morimekta.providence.serialization.jackson.ProvidenceStringFeature.ANY_TYPE_FIELD;


public class AnySerializer extends StdSerializer<PAny> {

    public AnySerializer() {
        super(PAny.class);
    }

    @Override
    public void serialize(PAny value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartObject();
        if (value instanceof AnyBinary) {
            AnyBinary binary = (AnyBinary) value;
            ObjectNode jsonContent = parseAsTree(binary);
            if (jsonContent != null) {
                gen.writeFieldName(ANY_TYPE_FIELD.get(serializers));
                gen.writeString(binary.getTypeName());
                for (String key : asList(jsonContent.fieldNames())) {
                    gen.writeFieldName(key);
                    gen.writeObject(jsonContent.get(key));
                }
            } else {
                writeMessageFields((AnyBinary) value, gen, serializers);
            }
        } else {
            PMessage message = value.getMessage();
            gen.writeFieldName(ANY_TYPE_FIELD.get(serializers));
            gen.writeString(message.$descriptor().getTypeName());
            writeMessageFields(message, gen, serializers);
        }
        gen.writeEndObject();
    }

    public static final AnySerializer INSTANCE = new AnySerializer();

    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static ObjectNode parseAsTree(AnyBinary binary) {
        if ("application/json".equals(binary.getMediaType()) && binary.hasData() && binary.hasTypeName()) {
            try {
                return MAPPER.createParser(binary.getData().getInputStream()).readValueAsTree();
            } catch (ClassCastException | IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }
}
