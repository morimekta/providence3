/*
 * Copyright 2017 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.jackson.adapter;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.pvdnc.core.PMessage;
import io.pvdnc.core.PAny;
import io.pvdnc.core.impl.AnyBinary;
import io.pvdnc.core.registry.PSimpleTypeRegistry;
import io.pvdnc.core.types.PDeclaredDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import net.morimekta.providence.serialization.jackson.ProvidenceStringFeature;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.fasterxml.jackson.core.JsonToken.VALUE_NULL;
import static io.pvdnc.core.registry.PTypeReference.parseGlobalRef;
import static io.pvdnc.sio.jackson.adapter.PMessageDeserializer.isTokenType;
import static io.pvdnc.sio.jackson.adapter.PMessageDeserializer.parseMessageFields;
import static net.morimekta.collect.util.Binary.encodeFromString;

public class AnyDeserializer extends JsonDeserializer<PAny> {
    public AnyDeserializer() {}

    @Override
    public PAny deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException {
        if (isTokenType(jsonParser.currentToken(), VALUE_NULL)) {
            return null;
        }
        String anyTypeNameField = ProvidenceStringFeature.ANY_TYPE_FIELD.get(deserializationContext);
        if (isTokenType(jsonParser.currentToken(), JsonToken.START_OBJECT)) {
            String typeNameField = jsonParser.nextFieldName();
            if (typeNameField == null) {
                return PAny.empty();
            } else if (PAny.TYPE_NAME_FIELD.contains(typeNameField) || anyTypeNameField.equals(typeNameField)) {
                String typeName = jsonParser.nextTextValue();
                PDeclaredDescriptor descriptor;
                try {
                    descriptor = PSimpleTypeRegistry.getInstance().descriptor(parseGlobalRef(typeName));
                } catch (IllegalArgumentException e) {
                    return readUnknownType(typeName, jsonParser);
                }
                if (!(descriptor instanceof PMessageDescriptor)) {
                    throw new IOException("Any type not a message: " + typeName);
                }
                PMessage message = parseMessageFields(jsonParser, jsonParser.nextToken(), (PMessageDescriptor<?>) descriptor);
                return PAny.wrap(message);
            } else {
                return parseMessageFields(jsonParser, jsonParser.nextToken(), AnyBinary.kDescriptor);
            }
        } else {
            throw new JsonParseException(jsonParser, "Invalid Any object start " + jsonParser.currentToken());
        }
    }

    private AnyBinary readUnknownType(String typeName, JsonParser jsonParser) throws IOException {
        Map<String, Object> content = new LinkedHashMap<>();
        String field = jsonParser.nextFieldName();
        while (field != null) {
            content.put(field, jsonParser.readValueAsTree());
            field = jsonParser.nextFieldName();
        }
        String compactValue = new ObjectMapper().writeValueAsString(content);
        return AnyBinary.newBuilder()
                .setTypeName(typeName)
                .setMediaType("application/json")
                .setData(encodeFromString(compactValue, StandardCharsets.UTF_8))
                .build();
    }

    public static final AnyDeserializer INSTANCE = new AnyDeserializer();
}
