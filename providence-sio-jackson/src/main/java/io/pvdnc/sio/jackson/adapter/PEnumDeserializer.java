/*
 * Copyright 2017 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.jackson.adapter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import io.pvdnc.core.PEnum;
import io.pvdnc.core.types.PEnumDescriptor;

import java.io.IOException;

import static java.util.Objects.requireNonNull;


public class PEnumDeserializer extends JsonDeserializer<PEnum> {
    private final PEnumDescriptor descriptor;

    public PEnumDeserializer(PEnumDescriptor descriptor) {
        this.descriptor = requireNonNull(descriptor);
    }

    @Override
    public PEnum deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException {
        if (jsonParser.currentToken().isNumeric()) {
            return descriptor.findById(Integer.parseInt(jsonParser.getValueAsString()));
        } else {
            return descriptor.findByName(jsonParser.getValueAsString());
        }
    }
}
