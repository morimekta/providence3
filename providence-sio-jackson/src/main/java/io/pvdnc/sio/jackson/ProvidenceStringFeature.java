/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.serialization.jackson;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.cfg.ContextAttributes;

/**
 * Providence specific features or attributes used to configure
 * serialization further. These are technically 'attributes' on
 * the serialization config, but is used are simple feature
 * flags.
 */
public enum ProvidenceStringFeature {
    /**
     * Allow using the <code>json.compact</code> notation. If set to false
     * will always use standard JSON object notation.
     * <p>
     * Defaults to <code>true</code>.
     */
    ANY_TYPE_FIELD("@type"),
    ;
    private final String defaultValue;

    ProvidenceStringFeature(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public void set(ObjectMapper mapper, String value) {
        mapper.setConfig(mapper.getSerializationConfig().withAttribute(this, value));
    }

    public String get(SerializerProvider sp) {
        Object o = sp.getAttribute(this);
        if (o == null) return defaultValue;
        return o.toString();
    }

    public String get(DeserializationContext sp) {
        Object o = sp.getAttribute(this);
        if (o == null) return defaultValue;
        return o.toString();
    }

    public String get(ContextAttributes sp) {
        Object o = sp.getAttribute(this);
        if (o == null) return defaultValue;
        return o.toString();
    }
}
