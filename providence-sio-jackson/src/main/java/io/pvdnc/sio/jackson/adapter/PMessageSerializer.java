/*
 * Copyright 2017 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.jackson.adapter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import io.pvdnc.core.PEnum;
import io.pvdnc.core.PMessage;
import io.pvdnc.core.PAny;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import net.morimekta.collect.util.Binary;
import net.morimekta.strings.Stringable;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import static io.pvdnc.core.property.PJsonProperties.JSON_COMPACT;
import static io.pvdnc.core.property.PJsonProperties.JSON_NAME;
import static io.pvdnc.sio.jackson.ProvidenceFeature.ALLOW_JSON_COMPACT;
import static io.pvdnc.sio.jackson.ProvidenceFeature.WRITE_FIELD_NAMES_AS_IDS;


public class PMessageSerializer extends StdSerializer<PMessage> {
    public PMessageSerializer() {
        super(PMessage.class);
    }

    @Override
    public void serialize(PMessage value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        serializeMessage(value, gen, serializers);
    }

    private static void serializeMessage(PMessage value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        if (ALLOW_JSON_COMPACT.isEnabled(serializers) && value.$descriptor().getProperty(JSON_COMPACT)) {
            gen.writeStartArray();
            int skipped = 0;
            for (PField field : value.$descriptor().allFields()) {
                Optional<Object> val = value.optional(field.getId());
                if (val.isPresent()) {
                    while (skipped > 0) {
                        gen.writeNull();
                        --skipped;
                    }
                    writeValue(val.get(), field.getResolvedDescriptor(), gen, serializers);
                } else {
                    ++skipped;
                }
            }
            gen.writeEndArray();
        } else {
            gen.writeStartObject();
            writeMessageFields(value, gen, serializers);
            gen.writeEndObject();
        }
    }

    protected static void writeMessageFields(PMessage value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        boolean fieldNamesAsIds = WRITE_FIELD_NAMES_AS_IDS.isEnabled(serializers);
        for (PField field : value.$descriptor().allFields()) {
            Optional<Object> val = value.optional(field.getId());
            if (val.isPresent()) {
                if (fieldNamesAsIds) {
                    gen.writeFieldId(field.getId());
                } else {
                    gen.writeFieldName(field.getProperty(JSON_NAME));
                }
                writeValue(val.get(), field.getResolvedDescriptor(), gen, serializers);
            }
        }
    }

    private static void writeValue(Object value, PDescriptor descriptor, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        switch (descriptor.getType()) {
            case VOID:
                gen.writeBoolean(true);
                break;
            case BOOL:
                gen.writeBoolean((Boolean) value);
                break;
            case BYTE:
                gen.writeNumber(((Number) value).byteValue());
                break;
            case SHORT:
                gen.writeNumber(((Number) value).shortValue());
                break;
            case INT:
                gen.writeNumber(((Number) value).intValue());
                break;
            case LONG:
                gen.writeNumber(((Number) value).longValue());
                break;
            case FLOAT:
                gen.writeNumber(((Number) value).floatValue());
                break;
            case DOUBLE:
                gen.writeNumber(((Number) value).doubleValue());
                break;
            case STRING:
                gen.writeString((String) value);
                break;
            case BINARY:
                gen.writeString(((Binary) value).toBase64());
                break;
            case LIST: {
                @SuppressWarnings("unchecked")
                PListDescriptor<Object> list = (PListDescriptor<Object>) descriptor;
                @SuppressWarnings("unchecked")
                Collection<Object> collection = (Collection<Object>) value;
                gen.writeStartArray();
                for (Object o : collection) {
                    writeValue(o, list.getItemType(), gen, serializers);
                }
                gen.writeEndArray();
                break;
            }
            case SET: {
                @SuppressWarnings("unchecked")
                PSetDescriptor<Object> set = (PSetDescriptor<Object>) descriptor;
                @SuppressWarnings("unchecked")
                Collection<Object> collection = (Collection<Object>) value;
                gen.writeStartArray();
                for (Object o : collection) {
                    writeValue(o, set.getItemType(), gen, serializers);
                }
                gen.writeEndArray();
                break;
            }
            case MAP: {
                @SuppressWarnings("unchecked")
                PMapDescriptor<Object, Object> map = (PMapDescriptor<Object, Object>) descriptor;
                @SuppressWarnings("unchecked")
                Map<Object, Object> objectMap = (Map<Object, Object>) value;
                gen.writeStartObject();
                for (Map.Entry<Object, Object> entry : objectMap.entrySet()) {
                    writeMapKey(entry.getKey(), map.getKeyType(), gen, serializers);
                    writeValue(entry.getValue(), map.getValueType(), gen, serializers);
                }
                gen.writeEndObject();
                break;
            }
            case ENUM: {
                if (serializers.getConfig().isEnabled(SerializationFeature.WRITE_ENUMS_USING_INDEX)) {
                    gen.writeNumber(((PEnum) value).getValue());
                } else {
                    gen.writeString(((PEnum) value).$meta().getProperty(JSON_NAME));
                }
                break;
            }
            case MESSAGE: {
                serializeMessage((PMessage) value, gen, serializers);
                break;
            }
            case ANY: {
                AnySerializer.INSTANCE.serialize((PAny) value, gen, serializers);
                break;
            }
        }
    }

    private static void writeMapKey(Object value, PDescriptor descriptor, JsonGenerator gen, SerializerProvider serializers)
            throws IOException {
        switch (descriptor.getType()) {
            case BOOL:
            case BYTE:
            case SHORT:
            case INT:
            case LONG:
            case DOUBLE:
            case STRING:
                gen.writeFieldName(value.toString());
                break;
            case FLOAT:
                gen.writeFieldName(Stringable.asString(value));
                break;
            case BINARY:
                gen.writeFieldName(((Binary) value).toBase64());
                break;
            case ENUM:
                if (serializers.getConfig().isEnabled(SerializationFeature.WRITE_ENUM_KEYS_USING_INDEX)) {
                    gen.writeFieldName("" + ((PEnum) value).getValue());
                } else {
                    gen.writeFieldName(((PEnum) value).$meta().getProperty(JSON_NAME));
                }
                break;
            case VOID:
            case LIST:
            case SET:
            case MAP:
            case MESSAGE:
            case ANY:
            default:
                throw new IOException("Not allowed in JSON key: " + descriptor.getType());
        }
    }
}
