/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.jackson.adapter;

import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.fasterxml.jackson.databind.jsontype.TypeDeserializer;
import com.fasterxml.jackson.databind.module.SimpleDeserializers;
import com.fasterxml.jackson.databind.type.MapType;
import io.pvdnc.core.PAny;
import io.pvdnc.core.PEnum;
import io.pvdnc.core.PMessage;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static io.pvdnc.core.types.PEnumDescriptor.getEnumDescriptor;
import static io.pvdnc.core.types.PMessageDescriptor.getMessageDescriptor;

public class ProvidenceDeserializers extends SimpleDeserializers {
    private final Map<Class<?>, JsonDeserializer<?>> classDeserializerMap;
    private final Map<Class<?>, KeyDeserializer> classKeyDeserializerMap;

    public ProvidenceDeserializers() {
        classDeserializerMap = new ConcurrentHashMap<>();
        classKeyDeserializerMap = new ConcurrentHashMap<>();
    }

    @Override
    public JsonDeserializer<?> findMapDeserializer(MapType type,
                                                   DeserializationConfig config,
                                                   BeanDescription beanDesc,
                                                   KeyDeserializer keyDeserializer,
                                                   TypeDeserializer elementTypeDeserializer,
                                                   JsonDeserializer<?> elementDeserializer)
            throws JsonMappingException {
        if (PEnum.class.isAssignableFrom(type.getKeyType().getRawClass())) {
            return new ProvidenceMapDeserializer<>(
                    type.getRawClass(),
                    classKeyDeserializerMap.computeIfAbsent(
                            type.getKeyType().getRawClass(), this::createEnumKeyDeserializer),
                    type.getContentType());
        }
        return super.findMapDeserializer(type,
                                         config,
                                         beanDesc,
                                         keyDeserializer,
                                         elementTypeDeserializer,
                                         elementDeserializer);
    }

    @Override
    public JsonDeserializer<?> findEnumDeserializer(Class<?> type,
                                                    DeserializationConfig config,
                                                    BeanDescription beanDesc) throws JsonMappingException {
        if (PEnum.class.isAssignableFrom(type)) {
            try {
                return classDeserializerMap.computeIfAbsent(type, this::createEnumDeserializer);
            } catch (IllegalArgumentException e) {
                throw new JsonMappingException(null, e.getMessage(), e);
            }
        }
        return super.findEnumDeserializer(type, config, beanDesc);
    }

    @Override
    public JsonDeserializer<?> findBeanDeserializer(JavaType type,
                                                    DeserializationConfig config,
                                                    BeanDescription beanDesc) throws JsonMappingException {
        if (PAny.class.isAssignableFrom(type.getRawClass())) {
            return classDeserializerMap.computeIfAbsent(type.getRawClass(), c -> new AnyDeserializer());
        } else if (PMessage.class.isAssignableFrom(type.getRawClass())) {
            try {
                return classDeserializerMap.computeIfAbsent(type.getRawClass(), this::createMessageDeserializer);
            } catch (IllegalArgumentException e) {
                throw new JsonMappingException(null, e.getMessage(), e);
            }
        } else if (PEnum.class.isAssignableFrom(type.getRawClass())) {
            try {
                return classDeserializerMap.computeIfAbsent(type.getRawClass(), this::createEnumDeserializer);
            } catch (IllegalArgumentException e) {
                throw new JsonMappingException(null, e.getMessage(), e);
            }
        }
        return super.findBeanDeserializer(type, config, beanDesc);
    }

    private JsonDeserializer<?> createMessageDeserializer(Class<?> type) {
        return new PMessageDeserializer<>(getMessageDescriptor(type));
    }

    private JsonDeserializer<?> createEnumDeserializer(Class<?> type) {
        return new PEnumDeserializer(getEnumDescriptor(type));
    }

    private KeyDeserializer createEnumKeyDeserializer(Class<?> type) {
        return new PEnumKeyDeserializer(getEnumDescriptor(type));
    }
}
