package io.pvdnc.sio.jackson;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import net.morimekta.collect.util.Binary;
import org.immutables.value.Value;

import javax.annotation.Nullable;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Value.Immutable
@JsonDeserialize(builder = JacksonMessage.Builder.class)
@JsonInclude(NON_NULL)
public interface JacksonMessage {
    @Nullable @JsonProperty("b") Boolean getB();
    @Nullable @JsonProperty("bt") Byte getBt();
    @Nullable @JsonProperty("s") Short getS();
    @Nullable @JsonProperty("i") Integer getI();
    @Nullable @JsonProperty("l") Long getL();
    @Nullable @JsonProperty("f") Float getF();
    @Nullable @JsonProperty("d") Double getD();
    @Nullable @JsonProperty("str") String getStr();
    @Nullable @JsonProperty("bin") Binary getBin();
    @Nullable @JsonProperty("e") JacksonEnum getE();

    static ImmutableJacksonMessage.Builder newBuilder() {
        return new JacksonMessage.Builder();
    }

    class Builder extends ImmutableJacksonMessage.Builder {}
}
