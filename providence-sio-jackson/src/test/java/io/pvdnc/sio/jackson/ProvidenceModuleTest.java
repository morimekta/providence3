package io.pvdnc.sio.jackson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.pvdnc.core.PAny;
import io.pvdnc.core.registry.PSimpleTypeRegistry;
import net.morimekta.collect.util.Binary;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ProvidenceModuleTest {
    private ObjectMapper mapper;

    @BeforeEach
    public void setUp() {
        mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
    }

    @Test
    public void testCompared() throws JsonProcessingException {
        JacksonMessage jck = JacksonMessage.newBuilder()
                                           .b(true)
                                           .bt((byte) 42)
                                           .s((short) 4200)
                                           .i(42000000)
                                           .l(42000000000L)
                                           .f(42.4242f)
                                           .d(42.42424242)
                                           .str("foo")
                                           .bin(Binary.fromBase64("bar"))
                                           .e(JacksonEnum.SECOND)
                                           .build();
        PvdMessage pvd = PvdMessage.newBuilder()
                                   .setB(true)
                                   .setBt((byte) 42)
                                   .setS((short) 4200)
                                   .setI(42000000)
                                   .setL(42000000000L)
                                   .setF(42.4242f)
                                   .setD(42.42424242)
                                   .setStr("foo")
                                   .setBin(Binary.fromBase64("bar"))
                                   .setE(PvdEnum.SECOND)
                                   .build();

        String jacksonStr = mapper.writeValueAsString(jck);
        assertThat(jacksonStr, is("{\"b\":true,\"bt\":42,\"s\":4200,\"i\":42000000,\"l\":42000000000,\"f\":42.4242,\"d\":42.42424242,\"str\":\"foo\",\"bin\":\"bao\",\"e\":\"SECOND\"}"));
        String pvdString = mapper.writeValueAsString(pvd);
        assertThat(pvdString, is(jacksonStr));

        assertThat(mapper.readValue(jacksonStr, PvdMessage.class), is(pvd));
        assertThat(mapper.readValue(pvdString, JacksonMessage.class), is(jck));
    }

    @Test
    public void testCompared_nulls() throws JsonProcessingException {
        JacksonMessage jck = JacksonMessage.newBuilder()
                                           .b(true)
                                           .e(JacksonEnum.FIRST)
                                           .build();
        PvdMessage pvd = PvdMessage.newBuilder()
                                   .setB(true)
                                   .setE(PvdEnum.FIRST)
                                   .build();

        String jacksonStr = mapper.writeValueAsString(jck);
        assertThat(jacksonStr, is("{\"b\":true,\"e\":\"FIRST\"}"));
        String pvdString = mapper.writeValueAsString(pvd);
        assertThat(pvdString, is(jacksonStr));

        assertThat(mapper.readValue(jacksonStr, PvdMessage.class), is(pvd));
        assertThat(mapper.readValue(pvdString, JacksonMessage.class), is(jck));
    }

    @Test
    public void testCompared_enumIndex() throws JsonProcessingException {
        mapper.setConfig(mapper.getSerializationConfig()
                               .withFeatures(SerializationFeature.WRITE_ENUMS_USING_INDEX,
                                             SerializationFeature.WRITE_ENUM_KEYS_USING_INDEX));

        JacksonMessage jck = JacksonMessage.newBuilder()
                                           .b(true)
                                           .bt((byte) 42)
                                           .s((short) 4200)
                                           .i(42000000)
                                           .l(42000000000L)
                                           .f(42.4242f)
                                           .d(42.42424242)
                                           .str("foo")
                                           .bin(Binary.fromBase64("bar"))
                                           .e(JacksonEnum.SECOND)
                                           .build();
        PvdMessage pvd = PvdMessage.newBuilder()
                                   .setB(true)
                                   .setBt((byte) 42)
                                   .setS((short) 4200)
                                   .setI(42000000)
                                   .setL(42000000000L)
                                   .setF(42.4242f)
                                   .setD(42.42424242)
                                   .setStr("foo")
                                   .setBin(Binary.fromBase64("bar"))
                                   .setE(PvdEnum.SECOND)
                                   .build();

        String jacksonStr = mapper.writeValueAsString(jck);
        assertThat(jacksonStr, is("{\"b\":true,\"bt\":42,\"s\":4200,\"i\":42000000,\"l\":42000000000,\"f\":42.4242,\"d\":42.42424242,\"str\":\"foo\",\"bin\":\"bao\",\"e\":1}"));
        String pvdString = mapper.writeValueAsString(pvd);
        assertThat(pvdString, is(jacksonStr));

        assertThat(mapper.readValue(jacksonStr, PvdMessage.class), is(pvd));
        assertThat(mapper.readValue(pvdString, JacksonMessage.class), is(jck));
    }

    @Test
    public void testWrapped() throws JsonProcessingException {
        PSimpleTypeRegistry.getInstance().registerRecursively(PvdCompact.kDescriptor);

        JacksonCompact jc = JacksonCompact.newBuilder().i(42).build();
        PvdCompact pc = PvdCompact.newBuilder().setI(42).build();

        JacksonWrapper jck = JacksonWrapper.newBuilder()
                                           .e(PvdEnum.FIRST)
                                           .bMap(mapOf(false, jc))
                                           .btMap(mapOf((byte) 42, jc))
                                           .sMap(mapOf((short) 4200, jc))
                                           .iMap(mapOf(42000000, jc))
                                           .lMap(mapOf(42000000000L, jc))
                                           .fMap(mapOf(42.4242f, jc))
                                           .dMap(mapOf(42.42424242, jc))
                                           .strMap(mapOf("foo", jc))
                                           .binMap(mapOf(Binary.fromBase64("bar"), jc))
                                           .eMap(mapOf(PvdEnum.THIRD, jc))
                                           .eList(listOf(PvdEnum.SECOND))
                                           .eSet(setOf(PvdEnum.FIRST))
                                           .vAny(PAny.wrap(pc))
                                           .build();
        PvdWrapper pvd = PvdWrapper.newBuilder()
                                   .setE(PvdEnum.FIRST)
                                   .setBMap(mapOf(false, pc))
                                   .setBtMap(mapOf((byte) 42, pc))
                                   .setSMap(mapOf((short) 4200, pc))
                                   .setIMap(mapOf(42000000, pc))
                                   .setLMap(mapOf(42000000000L, pc))
                                   .setFMap(mapOf(42.4242f, pc))
                                   .setDMap(mapOf(42.42424242, pc))
                                   .setStrMap(mapOf("foo", pc))
                                   .setBinMap(mapOf(Binary.fromBase64("bar"), pc))
                                   .setEMap(mapOf(PvdEnum.THIRD, pc))
                                   .setEList(listOf(PvdEnum.SECOND))
                                   .setESet(setOf(PvdEnum.FIRST))
                                   .setVAny(PAny.wrap(pc))
                                   .build();

        String pvdString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(pvd);
        assertThat(pvdString,
                   is("{\n" +
                      "  \"e\" : \"FIRST\",\n" +
                      "  \"b_map\" : {\n" +
                      "    \"false\" : {\n" +
                      "      \"i\" : 42\n" +
                      "    }\n" +
                      "  },\n" +
                      "  \"bt_map\" : {\n" +
                      "    \"42\" : {\n" +
                      "      \"i\" : 42\n" +
                      "    }\n" +
                      "  },\n" +
                      "  \"s_map\" : {\n" +
                      "    \"4200\" : {\n" +
                      "      \"i\" : 42\n" +
                      "    }\n" +
                      "  },\n" +
                      "  \"i_map\" : {\n" +
                      "    \"42000000\" : {\n" +
                      "      \"i\" : 42\n" +
                      "    }\n" +
                      "  },\n" +
                      "  \"l_map\" : {\n" +
                      "    \"42000000000\" : {\n" +
                      "      \"i\" : 42\n" +
                      "    }\n" +
                      "  },\n" +
                      "  \"f_map\" : {\n" +
                      "    \"42.4242\" : {\n" +
                      "      \"i\" : 42\n" +
                      "    }\n" +
                      "  },\n" +
                      "  \"d_map\" : {\n" +
                      "    \"42.42424242\" : {\n" +
                      "      \"i\" : 42\n" +
                      "    }\n" +
                      "  },\n" +
                      "  \"str_map\" : {\n" +
                      "    \"foo\" : {\n" +
                      "      \"i\" : 42\n" +
                      "    }\n" +
                      "  },\n" +
                      "  \"bin_map\" : {\n" +
                      "    \"bao\" : {\n" +
                      "      \"i\" : 42\n" +
                      "    }\n" +
                      "  },\n" +
                      "  \"e_map\" : {\n" +
                      "    \"THIRD\" : {\n" +
                      "      \"i\" : 42\n" +
                      "    }\n" +
                      "  },\n" +
                      "  \"e_list\" : [ \"SECOND\" ],\n" +
                      "  \"e_set\" : [ \"FIRST\" ],\n" +
                      "  \"v_any\" : {\n" +
                      "    \"@type\" : \"test.PvdCompact\",\n" +
                      "    \"i\" : 42\n" +
                      "  }\n" +
                      "}"));
        String jacksonStr = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jck);
        assertThat(jacksonStr,
                   is(pvdString));

        assertThat(mapper.readValue(jacksonStr, PvdWrapper.class), CoreMatchers.is(pvd));
        assertThat(mapper.readValue(pvdString, JacksonWrapper.class), is(jck));
    }


    @Test
    public void testWrapped_enumIndex() throws JsonProcessingException {
        mapper.setConfig(mapper.getSerializationConfig()
                               .withFeatures(SerializationFeature.WRITE_ENUMS_USING_INDEX,
                                             SerializationFeature.WRITE_ENUM_KEYS_USING_INDEX));

        JacksonCompact jc = JacksonCompact.newBuilder()
                                          .i(42)
                                          .build();
        JacksonWrapper jck = JacksonWrapper.newBuilder()
                                           .e(PvdEnum.FIRST)
                                           .eMap(mapOf(PvdEnum.THIRD, jc))
                                           .eList(listOf(PvdEnum.SECOND))
                                           .eSet(setOf(PvdEnum.FIRST))
                                           .build();
        PvdCompact pc = PvdCompact.newBuilder().setI(42).build();
        PvdWrapper pvd = PvdWrapper.newBuilder()
                                   .setE(PvdEnum.FIRST)
                                   .setEMap(mapOf(PvdEnum.THIRD, pc))
                                   .setEList(listOf(PvdEnum.SECOND))
                                   .setESet(setOf(PvdEnum.FIRST))
                                   .build();

        String pvdString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(pvd);
        assertThat(pvdString,
                   is("{\n" +
                      "  \"e\" : 0,\n" +
                      "  \"e_map\" : {\n" +
                      "    \"2\" : {\n" +
                      "      \"i\" : 42\n" +
                      "    }\n" +
                      "  },\n" +
                      "  \"e_list\" : [ 1 ],\n" +
                      "  \"e_set\" : [ 0 ]\n" +
                      "}"));
        String jacksonStr = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jck);
        assertThat(jacksonStr,
                   is(pvdString));

        assertThat(mapper.readValue(jacksonStr, PvdWrapper.class), CoreMatchers.is(pvd));
        assertThat(mapper.readValue(pvdString, JacksonWrapper.class), is(jck));
    }

    @Test
    public void testAllowCompact() throws JsonProcessingException {
        PvdReallyCompact compact = PvdReallyCompact.newBuilder()
                .setB(false)
                .setI(42)
                .build();
        assertThat(mapper.writer().writeValueAsString(compact),
                is("[false,null,null,42]"));
        ProvidenceFeature.ALLOW_JSON_COMPACT.set(mapper, false);
        assertThat(mapper.writer().writeValueAsString(compact),
                is("{\"b\":false,\"i\":42}"));
    }
}
