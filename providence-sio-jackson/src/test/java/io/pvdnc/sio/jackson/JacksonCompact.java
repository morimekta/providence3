package io.pvdnc.sio.jackson;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Value.Immutable
@JsonDeserialize(builder = JacksonCompact.Builder.class)
@JsonInclude(NON_NULL)
public interface JacksonCompact {
    @Nullable @JsonProperty("b") Boolean getB();
    @Nullable @JsonProperty("bt") Byte getBt();
    @Nullable @JsonProperty("s") Short getS();
    @Nullable @JsonProperty("i") Integer getI();
    @Nullable @JsonProperty("l") Long getL();

    static JacksonCompact.Builder newBuilder() {
        return new JacksonCompact.Builder();
    }

    class Builder extends ImmutableJacksonCompact.Builder {}
}
