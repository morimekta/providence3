package io.pvdnc.sio.jackson;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.pvdnc.core.PAny;
import net.morimekta.collect.util.Binary;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Value.Immutable
@JsonDeserialize(builder = JacksonWrapper.Builder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"e","b_map","bt_map","s_map","i_map","l_map","f_map","d_map","str_map","bin_map","e_map","e_list","e_set","v_any"})
public interface JacksonWrapper {
    @Nullable @JsonProperty("e") PvdEnum getE();
    @Nullable @JsonProperty("b_map") Map<Boolean, JacksonCompact> getBMap();
    @Nullable @JsonProperty("bt_map") Map<Byte, JacksonCompact> getBtMap();
    @Nullable @JsonProperty("s_map") Map<Short, JacksonCompact> getSMap();
    @Nullable @JsonProperty("i_map") Map<Integer, JacksonCompact> getIMap();
    @Nullable @JsonProperty("l_map") Map<Long, JacksonCompact> getLMap();
    @Nullable @JsonProperty("f_map") Map<Float, JacksonCompact> getFMap();
    @Nullable @JsonProperty("d_map") Map<Double, JacksonCompact> getDMap();
    @Nullable @JsonProperty("str_map") Map<String, JacksonCompact> getStrMap();
    @Nullable @JsonProperty("bin_map") Map<Binary, JacksonCompact> getBinMap();
    @Nullable @JsonProperty("e_map") Map<PvdEnum, JacksonCompact> getEMap();
    @Nullable @JsonProperty("e_list") List<PvdEnum> getEList();
    @Nullable @JsonProperty("e_set") Set<PvdEnum> getESet();
    @Nullable @JsonProperty("v_any")
    PAny getVAny();

    static JacksonWrapper.Builder newBuilder() {
        return new JacksonWrapper.Builder();
    }

    class Builder extends ImmutableJacksonWrapper.Builder {}
}
