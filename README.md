Providence 3 Messages
=====================

[![Morimekta](https://img.shields.io/static/v1?label=morimekta.net&logo=gitlab&message=providence3&color=informational)](https://gitlab.com/morimekta/providence3)
[![Pipeline](https://gitlab.com/morimekta/providence3/badges/master/pipeline.svg)](https://gitlab.com/morimekta/providence3/pipelines)
[![Coverage](https://gitlab.com/morimekta/providence3/badges/master/coverage.svg)](https://morimekta.net/providence3/jacoco-aggregate/)
[![License](https://img.shields.io/static/v1?label=license&message=apache%202.0&color=informational)](https://apache.org/licenses/LICENSE-2.0)

Providence 3 is a message handling system designed around a core of generated
code from written type definitions (called IDL), and utilities that use these
for serialization or other model transformations.

Providence 3 is the next major iteration of [providence](https://morimekta.net/providence).

# Releasing Providence

Run the maven versions plugin to see what has been updated of dependencies and
plugins. See if updates should be done. Usually it's better to depend on
newer versions, as you may drag in older versions into other projects that
misses features or has specific bugs.

```bash
mvn versions:display-dependency-updates
mvn versions:display-plugin-updates
```

## Making The Release.

```bash
# Do the maven release:
mvn release:prepare
mvn release:perform
mvn release:clean
git fetch origin
```

If the artifacts found at the
[Nexus Repository Manager](https://oss.sonatype.org/#stagingRepositories)
are correct, you're ready to make the release. If not a git hard rollback is
needed (to remove release version, tag and commits).