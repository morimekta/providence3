<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <name>Providence</name>
    <description>
        Providence is...
    </description>
    <url>http://www.morimekta.net/providence</url>
    <groupId>io.pvdnc</groupId>
    <artifactId>providence</artifactId>
    <version>3.0.0-SNAPSHOT</version>
    <packaging>pom</packaging>

    <inceptionYear>2020</inceptionYear>

    <licenses>
        <license>
            <name>Apache License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <distributionManagement>
        <!--
        <snapshotRepository>
            <id>sonatype</id>
            <url>https://oss.sonatype.org/content/repositories/snapshots</url>
        </snapshotRepository>
        -->
        <repository>
            <id>sonatype</id>
            <url>https://oss.sonatype.org/service/local/staging/deploy/maven2</url>
        </repository>
        <site>
            <id>morimekta</id>
            <name>Providence @ Morimekta</name>
            <url>https://www.morimekta.net/providence</url>
        </site>
    </distributionManagement>

    <organization>
        <name>morimekta.net</name>
        <url>https://www.morimekta.net/providence</url>
    </organization>

    <developers>
        <developer>
            <id>morimekta</id>
            <name>Stein Eldar Johnsen</name>
            <email>oss@morimekta.net</email>
            <timezone>Europe/Oslo</timezone>
            <roles>
                <role>Maintainer</role>
                <role>Developer</role>
            </roles>
        </developer>
    </developers>

    <scm>
        <url>https://gitlab.com/morimekta/providence</url>
        <developerConnection>scm:git:ssh://git@gitlab.com/morimekta/providence.git</developerConnection>
        <tag>HEAD</tag>
    </scm>

    <properties>
        <gpg.executable>gpg2</gpg.executable>

        <disable.distribution.packaging>false</disable.distribution.packaging>
        <dependency.locations.enabled>false</dependency.locations.enabled>

        <additionalparam>-Xdoclint:all</additionalparam>
        <skipTests>false</skipTests>
        <maven.javadoc.skip>false</maven.javadoc.skip>
        <!-- Arg lines not specified, set by jacoco plugin -->
        <!-- surefireArgLine -->
        <!-- failsafeArgLine -->

        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.javadoc.encoding>UTF-8</project.javadoc.encoding>
        <project.reporting.inputEncoding>UTF-8</project.reporting.inputEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <maven.compiler.source>11</maven.compiler.source>
        <maven.compiler.target>11</maven.compiler.target>
        <maven.sources.encoding>UTF-8</maven.sources.encoding>
        <maven.testing.args>-Xmx1024m -XX:+UseSerialGC -XX:+OmitStackTraceInFastThrow</maven.testing.args>

        <!-- PROTO -->
        <protobuf.version>3.12.2</protobuf.version>
        <thrift.version>0.12.0</thrift.version>
    </properties>

    <modules>
        <!-- Core Modules -->
        <module>providence-core</module>
        <module>providence-sio-gson</module>
        <module>providence-sio-thrift</module>
        <module>providence-sio-protobuf</module>
        <module>providence-sio-jackson</module>
        <module>providence-idl-generator</module>
        <module>providence-idl-parser</module>
        <module>providence-idl-thrift</module>
        <module>providence-idl-pvd</module>
        <module>providence-maven-plugin</module>
        <module>providence-rpc-http-client</module>
        <module>providence-rpc-http-servlet</module>
        <module>providence-reflect-protobuf</module>
    </modules>

    <dependencyManagement>
        <dependencies>
            <!-- ######## CORE ######## -->
            <dependency>
                <groupId>io.pvdnc</groupId>
                <artifactId>providence-core</artifactId>
                <version>${project.version}</version>
            </dependency>

            <!-- ######## SERIALIZATION & I/O ######## -->
            <dependency>
                <groupId>io.pvdnc</groupId>
                <artifactId>providence-sio-gson</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>io.pvdnc</groupId>
                <artifactId>providence-sio-thrift</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>io.pvdnc</groupId>
                <artifactId>providence-sio-jackson</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>io.pvdnc</groupId>
                <artifactId>providence-sio-protobuf</artifactId>
                <version>${project.version}</version>
            </dependency>

            <!-- ######## RPC ######## -->
            <dependency>
                <groupId>io.pvdnc</groupId>
                <artifactId>providence-rpc-http-client</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>io.pvdnc</groupId>
                <artifactId>providence-rpc-http-servlet</artifactId>
                <version>${project.version}</version>
            </dependency>

            <!-- ######## IDL ######## -->
            <dependency>
                <groupId>io.pvdnc</groupId>
                <artifactId>providence-idl-generator</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>io.pvdnc</groupId>
                <artifactId>providence-idl-parser</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>io.pvdnc</groupId>
                <artifactId>providence-idl-pvd</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>io.pvdnc</groupId>
                <artifactId>providence-idl-thrift</artifactId>
                <version>${project.version}</version>
            </dependency>

            <!-- Reflection -->
            <dependency>
                <groupId>io.pvdnc</groupId>
                <artifactId>providence-reflect-protobuf</artifactId>
                <version>${project.version}</version>
            </dependency>

            <!-- Utilities -->
            <dependency>
                <groupId>net.morimekta.utils</groupId>
                <artifactId>collect</artifactId>
                <version>4.3.1</version>
            </dependency>
            <dependency>
                <groupId>net.morimekta.utils</groupId>
                <artifactId>io</artifactId>
                <version>4.2.1</version>
            </dependency>
            <dependency>
                <groupId>net.morimekta.utils</groupId>
                <artifactId>file</artifactId>
                <version>4.1.0</version>
            </dependency>
            <dependency>
                <groupId>net.morimekta.utils</groupId>
                <artifactId>strings</artifactId>
                <version>4.2.0</version>
            </dependency>
            <dependency>
                <groupId>net.morimekta.utils</groupId>
                <artifactId>lexer</artifactId>
                <version>4.0.0</version>
            </dependency>
            <dependency>
                <groupId>com.google.code.gson</groupId>
                <artifactId>gson</artifactId>
                <version>2.8.6</version>
            </dependency>
            <dependency>
                <groupId>org.apache.thrift</groupId>
                <artifactId>libthrift</artifactId>
                <version>${thrift.version}</version>
            </dependency>
            <dependency>
                <groupId>jakarta.servlet</groupId>
                <artifactId>jakarta.servlet-api</artifactId>
                <version>5.0.0</version>
            </dependency>
            <dependency>
                <groupId>com.google.protobuf</groupId>
                <artifactId>protobuf-java</artifactId>
                <version>${protobuf.version}</version>
            </dependency>
            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>30.0-jre</version>
            </dependency>

            <!-- TESTING -->

            <dependency>
                <groupId>org.eclipse.jetty</groupId>
                <artifactId>jetty-servlet</artifactId>
                <version>11.0.0</version>
            </dependency>
            <dependency>
                <groupId>org.eclipse.jetty</groupId>
                <artifactId>jetty-server</artifactId>
                <version>11.0.0</version>
            </dependency>
            <dependency>
                <groupId>org.junit.jupiter</groupId>
                <artifactId>junit-jupiter</artifactId>
                <version>5.7.0</version>
            </dependency>
            <dependency>
                <groupId>org.junit.jupiter</groupId>
                <artifactId>junit-jupiter-params</artifactId>
                <version>5.7.0</version>
            </dependency>
            <dependency>
                <groupId>org.hamcrest</groupId>
                <artifactId>hamcrest-core</artifactId>
                <version>2.2</version>
            </dependency>
            <dependency>
                <groupId>org.hamcrest</groupId>
                <artifactId>hamcrest-library</artifactId>
                <version>2.2</version>
            </dependency>
            <dependency>
                <groupId>org.mockito</groupId>
                <artifactId>mockito-junit-jupiter</artifactId>
                <version>3.7.7</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>io.pvdnc</groupId>
                    <artifactId>providence-maven-plugin</artifactId>
                    <version>${project.version}</version>
                </plugin>

                <!-- Core Maven Plugins -->
                <plugin><artifactId>maven-assembly-plugin</artifactId><version>3.3.0</version></plugin>
                <plugin><artifactId>maven-clean-plugin</artifactId><version>3.1.0</version></plugin>
                <plugin><artifactId>maven-compiler-plugin</artifactId><version>3.8.1</version></plugin>
                <plugin><artifactId>maven-deploy-plugin</artifactId><version>3.0.0-M1</version></plugin>
                <plugin><artifactId>maven-enforcer-plugin</artifactId><version>3.0.0-M3</version></plugin>
                <plugin><artifactId>maven-failsafe-plugin</artifactId><version>3.0.0-M5</version></plugin>
                <plugin><artifactId>maven-gpg-plugin</artifactId><version>1.6</version></plugin>
                <plugin><artifactId>maven-install-plugin</artifactId><version>3.0.0-M1</version></plugin>
                <plugin><artifactId>maven-jar-plugin</artifactId><version>3.2.0</version></plugin>
                <plugin><artifactId>maven-javadoc-plugin</artifactId><version>3.2.0</version></plugin>
                <plugin><artifactId>maven-plugin-plugin</artifactId><version>3.6.0</version></plugin>
                <plugin><artifactId>maven-release-plugin</artifactId><version>3.0.0-M1</version></plugin>
                <plugin><artifactId>maven-resources-plugin</artifactId><version>3.2.0</version></plugin>
                <plugin><artifactId>maven-shade-plugin</artifactId><version>3.2.4</version></plugin>
                <plugin><artifactId>maven-site-plugin</artifactId><version>3.9.1</version></plugin>
                <plugin><artifactId>maven-source-plugin</artifactId><version>3.2.1</version></plugin>
                <plugin><artifactId>maven-surefire-plugin</artifactId><version>3.0.0-M5</version></plugin>

                <!-- additional plugins -->
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>rpm-maven-plugin</artifactId>
                    <version>2.2.0</version>
                </plugin>
                <plugin>
                    <groupId>org.vafer</groupId>
                    <artifactId>jdeb</artifactId>
                    <version>1.8</version>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>build-helper-maven-plugin</artifactId>
                    <version>3.2.0</version>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>versions-maven-plugin</artifactId>
                    <version>2.8.1</version>
                </plugin>
                <plugin>
                    <groupId>org.sonatype.plugins</groupId>
                    <artifactId>nexus-staging-maven-plugin</artifactId>
                    <version>1.6.8</version>
                </plugin>
                <plugin>
                    <groupId>org.jacoco</groupId>
                    <artifactId>jacoco-maven-plugin</artifactId>
                    <version>0.8.6</version>
                </plugin>
                <plugin>
                    <groupId>com.googlecode.maven-download-plugin</groupId>
                    <artifactId>download-maven-plugin</artifactId>
                    <version>1.6.1</version>
                </plugin>
                <plugin>
                    <groupId>com.github.spotbugs</groupId>
                    <artifactId>spotbugs-maven-plugin</artifactId>
                    <version>4.2.0</version>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>exec-maven-plugin</artifactId>
                    <version>3.0.0</version>
                </plugin>
                <plugin>
                    <groupId>com.github.os72</groupId>
                    <artifactId>protoc-jar-maven-plugin</artifactId>
                    <version>3.11.4</version>
                </plugin>
            </plugins>
        </pluginManagement>

        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
                <executions>
                    <execution>
                        <id>enforce-maven</id>
                        <goals>
                            <goal>enforce</goal>
                        </goals>
                        <configuration>
                            <rules>
                                <requireMavenVersion>
                                    <version>3.5.0</version>
                                </requireMavenVersion>
                            </rules>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>versions-maven-plugin</artifactId>
                <configuration>
                    <rulesUri>file:version-rules.xml</rulesUri>
                    <generateBackupPoms>false</generateBackupPoms>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <forceJavacCompilerUse>true</forceJavacCompilerUse>
                    <compilerArgs>
                        <arg>-XDcompilePolicy=byfile</arg>
                        <arg>-Xlint:all</arg>
                        <arg>-Xlint:-unchecked</arg>
                    </compilerArgs>
                    <useIncrementalCompilation>true</useIncrementalCompilation>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <phase>package</phase>
                        <goals>
                            <goal>jar-no-fork</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <configuration>
                    <encoding>${project.build.sourceEncoding}</encoding>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <configuration>
                    <failOnError>true</failOnError>
                    <doclint>all</doclint>
                    <notimestamp>true</notimestamp>
                    <nodeprecatedlist>true</nodeprecatedlist>
                    <quiet>true</quiet>
                    <sourceFileIncludes>io/pvdnc/**/*.java</sourceFileIncludes>
                    <excludes>
                        <!-- providence-core -->
                        <exclude>io/pvdnc/core/impl/Empty.java</exclude>
                        <exclude>io/pvdnc/core/impl/AnyBinary.java</exclude>
                        <exclude>io/pvdnc/core/impl/Providence_FileDescriptor.java</exclude>
                        <!-- providence-sio-thrift -->
                        <exclude>io/pvdnc/sio/thrift/rpc/ApplicationException.java</exclude>
                        <exclude>io/pvdnc/sio/thrift/rpc/ApplicationExceptionType.java</exclude>
                        <exclude>io/pvdnc/sio/thrift/rpc/ServiceCallType.java</exclude>
                        <exclude>io/pvdnc/sio/thrift/rpc/Thrift_FileDescriptor.java</exclude>
                        <!-- providence-sio-protobuf -->
                        <exclude>io/pvdnc/sio/protobuf/impl/AnyProtoBuf.java</exclude>
                        <exclude>io/pvdnc/sio/protobuf/impl/Protobuf_FileDescriptor.java</exclude>
                        <!-- providence-reflect-protobuf -->
                        <exclude>io/pvdnc/reflect/protobuf/Providence.java</exclude>
                    </excludes>
                    <sourcepath>:
                        src/main/java:
                        target/generated-sources/protobuf:</sourcepath>
                    <excludedocfilessubdir>:
                        src/test/*:
                        target/generated-test-sources/*:
                    </excludedocfilessubdir>
                    <additionalOptions>
                        <additionalOption>-html5</additionalOption>
                        <additionalOption>-quiet</additionalOption>
                    </additionalOptions>
                    <detectOfflineLinks>false</detectOfflineLinks>
                    <groups>
                        <group>
                            <title>Providence : Core</title>
                            <packages>io.pvdnc.core:io.pvdnc.core.*</packages>
                        </group>
                        <group>
                            <title>Providence : IDL</title>
                            <packages>io.pvdnc.idl.*</packages>
                        </group>
                        <group>
                            <title>Providence : RPC</title>
                            <packages>io.pvdnc.rpc.*</packages>
                        </group>
                        <group>
                            <title>Providence : Serialization &amp; I/O</title>
                            <packages>io.pvdnc.sio.*</packages>
                        </group>
                        <group>
                            <title>Providence : Reflection</title>
                            <packages>io.pvdnc.reflect.*</packages>
                        </group>
                    </groups>
                </configuration>
                <executions>
                    <execution>
                        <id>attach-javadoc</id>
                        <phase>package</phase>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <configuration>
                    <preparationGoals>clean install</preparationGoals>
                    <tagNameFormat>v@{project.version}</tagNameFormat>
                    <autoVersionSubmodules>true</autoVersionSubmodules>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-deploy-plugin</artifactId>
                <executions>
                    <execution>
                        <id>default-deploy</id>
                        <phase>none</phase>
                    </execution>
                </executions>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-site-plugin</artifactId>
                <executions>
                    <execution>
                        <id>default-site</id>
                        <phase>none</phase>
                    </execution>
                    <execution>
                        <id>default-deploy</id>
                        <phase>none</phase>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <!-- Sets the VM argument line used when unit tests are run. -->
                    <argLine>${surefireArgLine} ${maven.testing.args}</argLine>
                    <!-- Excludes integration tests when unit tests are run. -->
                    <systemPropertyVariables>
                        <org.slf4j.simpleLogger.defaultLogLevel>off</org.slf4j.simpleLogger.defaultLogLevel>
                    </systemPropertyVariables>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-failsafe-plugin</artifactId>
                <executions>
                    <execution>
                        <id>integration-tests</id>
                        <phase>none</phase>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>com.github.spotbugs</groupId>
                <artifactId>spotbugs-maven-plugin</artifactId>
                <dependencies>
                    <!-- overwrite dependency on spotbugs if you want to specify the version of spotbugs -->
                    <dependency>
                        <groupId>org.slf4j</groupId>
                        <artifactId>slf4j-api</artifactId>
                        <version>2.0.0-alpha1</version>
                    </dependency>
                    <dependency>
                        <groupId>org.slf4j</groupId>
                        <artifactId>slf4j-simple</artifactId>
                        <version>2.0.0-alpha1</version>
                    </dependency>
                    <dependency>
                        <groupId>com.github.spotbugs</groupId>
                        <artifactId>spotbugs</artifactId>
                        <version>4.2.0</version>
                    </dependency>
                </dependencies>
                <configuration>
                    <failOnError>true</failOnError>
                    <excludeFilterFile>${project.basedir}/spotbugs-exclude.xml</excludeFilterFile>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <configuration>
                    <includes>
                        <include>io/pvdnc/*</include>
                        <include>io/pvdnc/**/*</include>
                    </includes>
                    <excludes>
                        <!-- providence-core -->
                        <exclude>io/pvdnc/core/impl/Empty*</exclude>
                        <exclude>io/pvdnc/core/impl/AnyBinary*</exclude>
                        <exclude>io/pvdnc/core/impl/Providence_FileDescriptor*</exclude>
                        <!-- providence-sio-thrift -->
                        <exclude>io/pvdnc/sio/thrift/rpc/ApplicationException*</exclude>
                        <exclude>io/pvdnc/sio/thrift/rpc/ApplicationExceptionType*</exclude>
                        <exclude>io/pvdnc/sio/thrift/rpc/ServiceCallType*</exclude>
                        <exclude>io/pvdnc/sio/thrift/rpc/Thrift_FileDescriptor*</exclude>
                    </excludes>

                </configuration>
                <executions>
                    <!--
                        Prepares the property pointing to the JaCoCo runtime agent which
                        is passed as VM argument when Maven the Surefire plugin is executed.
                    -->
                    <execution>
                        <id>pre-unit-test</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                        <configuration>
                            <!-- Sets the path to the file which contains the execution data. -->
                            <destFile>${project.build.directory}/coverage-reports/jacoco-ut.exec</destFile>
                            <!--
                                Sets the name of the property containing the settings
                                for JaCoCo runtime agent.
                            -->
                            <propertyName>surefireArgLine</propertyName>
                        </configuration>
                    </execution>
                    <!--
                        Ensures that the code coverage report for unit tests is created after
                        unit tests have been run.
                    -->
                    <execution>
                        <id>post-unit-test</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                        <configuration>
                            <!-- Sets the path to the file which contains the execution data. -->
                            <dataFile>${project.build.directory}/coverage-reports/jacoco-ut.exec</dataFile>
                            <!-- Sets the output directory for the code coverage report. -->
                            <outputDirectory>${project.reporting.outputDirectory}/jacoco-ut</outputDirectory>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>build-helper-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <id>add-source</id>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>add-source</goal>
                        </goals>
                        <configuration>
                            <sources>
                                <source>${project.basedir}/src/main/java-gen</source>
                                <source>${project.build.directory}/generated-sources/protobuf</source>
                            </sources>
                        </configuration>
                    </execution>
                    <execution>
                        <id>add-test-source</id>
                        <phase>generate-test-sources</phase>
                        <goals>
                            <goal>add-test-source</goal>
                        </goals>
                        <configuration>
                            <sources>
                                <source>${project.basedir}/src/test/java-gen</source>
                                <source>${project.build.directory}/generated-test-sources/protobuf</source>
                            </sources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>reporting</id>
            <modules>
                <module>reporting</module>
            </modules>
        </profile>
        <!-- release process modifications -->
        <profile>
            <id>release-sign-artifacts</id>
            <activation>
                <property>
                    <name>performRelease</name>
                    <value>true</value>
                </property>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.sonatype.plugins</groupId>
                        <artifactId>nexus-staging-maven-plugin</artifactId>
                        <extensions>true</extensions>
                        <configuration>
                            <serverId>sonatype</serverId>
                            <nexusUrl>https://oss.sonatype.org/</nexusUrl>
                            <autoReleaseAfterClose>false</autoReleaseAfterClose>
                        </configuration>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <configuration>
                            <executable>${gpg.executable}</executable>
                            <useAgent>true</useAgent>
                        </configuration>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
