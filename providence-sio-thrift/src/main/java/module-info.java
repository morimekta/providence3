import io.pvdnc.core.io.PSerializer;
import io.pvdnc.sio.thrift.ThriftBinarySerializer;
import io.pvdnc.sio.thrift.ThriftGsonSerializer;
import io.pvdnc.sio.thrift.ThriftSerializer;

module io.pvdnc.sio.thrift {
    exports io.pvdnc.sio.thrift;
    exports io.pvdnc.sio.thrift.rpc;

    requires transitive io.pvdnc.core;
    requires io.pvdnc.sio.gson;
    requires com.google.gson;
    requires net.morimekta.collect;
    requires net.morimekta.strings;
    requires net.morimekta.io;

    requires java.net.http;

    uses ThriftSerializer;
    provides PSerializer with ThriftBinarySerializer;
    provides ThriftSerializer with ThriftBinarySerializer, ThriftGsonSerializer;
}