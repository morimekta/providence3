/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.thrift;

import io.pvdnc.core.io.PSerializer;
import io.pvdnc.core.types.PServiceDescriptor;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.ServiceLoader;

import static net.morimekta.collect.UnmodifiableList.asList;

public interface ThriftSerializer extends PSerializer {
    /**
     * Read a service call from stream.
     *
     * @param service The service to read call for.
     * @param in The input stream to read from.
     * @return The read service call.
     * @throws IOException If reading service call failed.
     */
    ThriftServiceCall readFrom(PServiceDescriptor service, InputStream in) throws IOException;

    /**
     * Write service call to stream.
     *
     * @param call The service call to write.
     * @param out The output stream to write to.
     * @throws IOException If writing service call failed.
     */
    void writeTo(ThriftServiceCall call, OutputStream out) throws IOException;

    /**
     * @return List of available serializers.
     */
    static List<ThriftSerializer> listSerializers() {
        return asList(kServiceLoader);
    }
    ServiceLoader<ThriftSerializer> kServiceLoader = ServiceLoader.load(ThriftSerializer.class);

}
