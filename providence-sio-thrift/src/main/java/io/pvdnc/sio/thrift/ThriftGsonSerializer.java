/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.thrift;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import io.pvdnc.core.PMessage;
import io.pvdnc.core.property.PJsonProperties;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PServiceDescriptor;
import io.pvdnc.core.types.PServiceMethod;
import io.pvdnc.sio.gson.GsonMessageReader;
import io.pvdnc.sio.gson.GsonMessageWriter;
import io.pvdnc.sio.gson.GsonSerializer;
import io.pvdnc.sio.thrift.rpc.ApplicationException;
import io.pvdnc.sio.thrift.rpc.ApplicationExceptionType;
import io.pvdnc.sio.thrift.rpc.ServiceCallType;
import net.morimekta.strings.io.Utf8StreamReader;
import net.morimekta.strings.io.Utf8StreamWriter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Map;

import static java.util.Locale.US;

public class ThriftGsonSerializer extends GsonSerializer implements ThriftSerializer {
    private final static Map<String, ServiceCallType> jsonNameToServiceCall = makeJsonNameToServiceCall();

    public ThriftGsonSerializer() {
    }

    protected ThriftGsonSerializer(boolean strict, boolean pretty) {
        super(strict, pretty);
    }

    @Override
    public ThriftServiceCall readFrom(PServiceDescriptor service, InputStream in) throws IOException {
        String          methodName;
        int             sequenceNo;
        ServiceCallType type;
        String          typeValue;

        try {
            JsonReader reader = new JsonReader(new Utf8StreamReader(in));
            reader.beginArray();
            methodName = reader.nextString();
            if (reader.peek() == JsonToken.STRING) {
                typeValue = reader.nextString();
                type = jsonNameToServiceCall.get(typeValue);
                if (type == null) {
                    type = ServiceCallType.findByName(typeValue);
                }
            } else {
                int tmp = reader.nextInt();
                typeValue = "" + tmp;
                type = ServiceCallType.findById(tmp);
            }
            sequenceNo = reader.nextInt();

            if (type == null) {
                throw new ApplicationException.$Impl("Unknown type: " + typeValue,
                                                     ApplicationExceptionType.INVALID_MESSAGE_TYPE);
            }
            if (sequenceNo < 0) {
                throw new ApplicationException.$Impl("Invalid sequence no " + sequenceNo,
                                                     ApplicationExceptionType.BAD_SEQUENCE_ID);
            }
            PServiceMethod method = service.findMethodForName(methodName);
            if (method == null) {
                throw new ApplicationException.$Impl("No such method '" + methodName + "' on " + service.getTypeName(),
                                                     ApplicationExceptionType.UNKNOWN_METHOD);
            }

            PMessageDescriptor<?> descriptor = method.getRequestType();
            if (type == ServiceCallType.REPLY) {
                descriptor = method.getResponseType();
            } else if (type == ServiceCallType.EXCEPTION) {
                descriptor = ApplicationException.kDescriptor;
            }

            PMessage message = new GsonMessageReader(reader, false).read(descriptor);
            return new ThriftServiceCall(method, type, sequenceNo, message);
        } catch (ApplicationException.$Impl e) {
            throw e;
        } catch (IOException | IllegalStateException | IllegalArgumentException e) {
            throw new ApplicationException.$Impl("Bad JSON content: " + e.getMessage(),
                                                 ApplicationExceptionType.PROTOCOL_ERROR)
                    .initCause(e);
        }
    }

    public void writeTo(ThriftServiceCall call, OutputStream out) throws IOException {
        Writer     writer = new Utf8StreamWriter(out);
        JsonWriter json   = new JsonWriter(writer);
        if (isPretty()) {
            json.setIndent("  ");
        }
        json.beginArray();
        json.value(call.getMethodName());
        json.value(call.getCallType().getName().toLowerCase(US));
        json.value(call.getSequenceNo());
        new GsonMessageWriter(json).write(call.getMessage());
        json.endArray();
        json.flush();
        if (isPretty()) {
            writer.write("\n");
        }
        writer.flush();
    }

    @Override
    public ThriftGsonSerializer strict() {
        if (isStrict()) return this;
        return new ThriftGsonSerializer(true, isPretty());
    }

    @Override
    public ThriftGsonSerializer pretty() {
        if (isPretty()) return this;
        return new ThriftGsonSerializer(isStrict(), true);
    }

    @SuppressWarnings("unchecked")
    private static Map<String, ServiceCallType> makeJsonNameToServiceCall() {
        return (Map<String, ServiceCallType>) (Map<String,?>) ServiceCallType.kDescriptor.getProperty(PJsonProperties.JSON_ENUM_MAP);
    }
}
