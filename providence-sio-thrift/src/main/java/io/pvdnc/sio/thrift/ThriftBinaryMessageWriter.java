/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.thrift;

import io.pvdnc.core.PEnum;
import io.pvdnc.core.PMessage;
import io.pvdnc.core.PUnion;
import io.pvdnc.core.PAny;
import io.pvdnc.core.io.PMessageWriter;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PFieldRequirement;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.sio.thrift.rpc.ApplicationException;
import io.pvdnc.sio.thrift.rpc.ApplicationExceptionType;
import net.morimekta.collect.util.Binary;
import net.morimekta.io.BigEndianBinaryOutputStream;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Map;

import static io.pvdnc.core.types.PFieldRequirement.OPTIONAL;
import static io.pvdnc.core.types.PFieldRequirement.REQUIRED;
import static io.pvdnc.core.types.PFieldRequirement.REQUIRED_OUT;
import static io.pvdnc.sio.thrift.ThriftType.forType;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.requireNonNull;

public class ThriftBinaryMessageWriter implements PMessageWriter {
    static final int VERSION_1    = 0x80010000;

    private final BigEndianBinaryOutputStream writer;
    private final boolean strict;
    private final boolean versioned;

    public ThriftBinaryMessageWriter(OutputStream out) {
        this(out, false, true);
    }

    public ThriftBinaryMessageWriter(OutputStream out, boolean strict, boolean versioned) {
        this(new BigEndianBinaryOutputStream(out), strict, versioned);
    }

    public ThriftBinaryMessageWriter(BigEndianBinaryOutputStream writer, boolean strict, boolean versioned) {
        this.writer = requireNonNull(writer, "writer == null");
        this.strict = strict;
        this.versioned = versioned;
    }

    @Override
    public void write(PMessage message) throws IOException {
        requireNonNull(message, "message == null");
        writeMessage(writer, message, strict);
        writer.flush();
    }

    public void write(ThriftServiceCall call) throws IOException {
        requireNonNull(call, "call == null");
        byte[] method = call.getMethodName().getBytes(UTF_8);
        if (versioned) {
            writer.writeInt(VERSION_1 | call.getCallType().getValue());
            writer.writeInt(method.length);
            writer.write(method);
        } else {
            writer.writeInt(method.length);
            writer.write(method);
            writer.writeByte((byte) call.getCallType().getValue());
        }
        writer.writeInt(call.getSequenceNo());
        write(call.getMessage());
    }

    /**
     * Write message to writer.
     *
     * @param writer The binary writer.
     * @param message The message to write.
     * @param strict If the message should be strictly verified when writing.
     * @throws IOException If write failed.
     */
    public static void writeMessage(BigEndianBinaryOutputStream writer, PMessage message, boolean strict)
            throws IOException {
        // if (message instanceof BinaryWriter) {
        //     ((BinaryWriter) message).writeBinary(writer);
        //     return;
        // }

        if (message instanceof PUnion) {
            if (((PUnion) message).unionFieldIsSet()) {
                PField field = ((PUnion) message).unionField();
                writeFieldSpec(writer, forType(field.getResolvedDescriptor().getType()), field.getId());
                writeFieldValue(writer,
                                message.get(field.getId()),
                                field.getResolvedDescriptor(),
                                strict);
            }
        } else {
            for (PField field : message.$descriptor().allFields()) {
                PFieldRequirement requirement = field.getRequirement();
                boolean           present     = message.has(field.getId());
                if (strict && !present && requirement == REQUIRED || requirement == REQUIRED_OUT) {
                    throw new ApplicationException.$Impl(
                            "Missing required field in " + message.$descriptor().getTypeName() + ": " + field.getName(),
                            ApplicationExceptionType.PROTOCOL_ERROR);
                } else if (present || requirement != OPTIONAL) {
                    Object value = message.get(field.getId());
                    if (value == null) {
                        if (strict) {
                            throw new ApplicationException.$Impl(
                                    String.format("Missing non-optional field value in %s: %s",
                                                  message.$descriptor().getTypeName(), field.getName()),
                                    ApplicationExceptionType.PROTOCOL_ERROR);
                        }
                        continue;
                    }
                    writeFieldSpec(writer, forType(field.getResolvedDescriptor().getType()), field.getId());
                    writeFieldValue(writer,
                                    value,
                                    field.getResolvedDescriptor(),
                                    strict);
                }
            }
        }
        writer.writeUInt8(ThriftType.STOP);
    }

    private static void writeFieldSpec(BigEndianBinaryOutputStream out, byte type, int key) throws IOException {
        out.writeByte(type);
        out.writeShort((short) key);
    }

    private static void writeFieldValue(BigEndianBinaryOutputStream out, Object value, PDescriptor descriptor, boolean strict) throws IOException {
        switch (descriptor.getType()) {
            case VOID:
                return;
            case BOOL:
                out.writeByte(((Boolean) value) ? (byte) 1 : (byte) 0);
                return;
            case BYTE:
                out.writeByte((Byte) value);
                return;
            case SHORT:
                out.writeShort((Short) value);
                return;
            case INT:
                out.writeInt((Integer) value);
                return;
            case LONG:
                out.writeLong((Long) value);
                return;
            case FLOAT:
                // since ThriftType for FLOAT is I32.
                out.writeInt(Float.floatToIntBits((Float) value));
                return;
            case DOUBLE:
                out.writeDouble((Double) value);
                return;
            case BINARY: {
                Binary binary = (Binary) value;
                out.writeUInt32(binary.length());
                binary.write(out);
                return;
            }
            case STRING: {
                byte[] bytes = value.toString().getBytes(StandardCharsets.UTF_8);
                out.writeUInt32(bytes.length);
                out.write(bytes);
                return;
            }
            case ENUM:
                out.writeInt(((PEnum) value).getValue());
                return;
            case MAP: {
                @SuppressWarnings("unchecked")
                Map<Object, Object> map = (Map<Object, Object>) value;
                PMapDescriptor<?, ?> pMap = (PMapDescriptor<?, ?>) descriptor;
                out.writeByte(forType(pMap.getKeyType().getType()));
                out.writeByte(forType(pMap.getValueType().getType()));
                out.writeUInt32(map.size());
                for (Map.Entry<Object, Object> entry : map.entrySet()) {
                    writeFieldValue(out, entry.getKey(), pMap.getKeyType(), strict);
                    writeFieldValue(out, entry.getValue(), pMap.getValueType(), strict);
                }
                return;
            }
            case SET: {
                @SuppressWarnings("unchecked")
                Collection<Object> coll = (Collection<Object>) value;
                PSetDescriptor<?> pSet = (PSetDescriptor<?>) descriptor;

                out.writeByte(forType(pSet.getItemType().getType()));
                out.writeUInt32(coll.size());

                for (Object item : coll) {
                    writeFieldValue(out, item, pSet.getItemType(), strict);
                }
                return;
            }
            case LIST: {
                @SuppressWarnings("unchecked")
                Collection<Object> coll = (Collection<Object>) value;
                PListDescriptor<?> pList = (PListDescriptor<?>) descriptor;

                out.writeByte(forType(pList.getItemType().getType()));
                out.writeUInt32(coll.size());

                for (Object item : coll) {
                    writeFieldValue(out, item, pList.getItemType(), strict);
                }
                return;
            }
            case MESSAGE: {
                writeMessage(out, (PMessage) value, strict);
                return;
            }
            case ANY: {
                writeMessage(out, ((PAny) value).pack(new ThriftBinarySerializer(strict, true)), strict);
                return;
            }
            default:
                throw new ApplicationException.$Impl("Unhandled field type: " + descriptor.getType(),
                                                     ApplicationExceptionType.PROTOCOL_ERROR);
        }
    }
}
