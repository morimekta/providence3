/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.thrift;

import io.pvdnc.core.PMessageBuilder;
import io.pvdnc.core.impl.AnyBinary;
import io.pvdnc.sio.thrift.rpc.ApplicationException;
import io.pvdnc.sio.thrift.rpc.ApplicationExceptionType;
import io.pvdnc.sio.thrift.rpc.ServiceCallType;
import net.morimekta.collect.ListBuilder;
import net.morimekta.collect.MapBuilder;
import net.morimekta.collect.SetBuilder;
import net.morimekta.collect.UnmodifiableList;
import net.morimekta.collect.UnmodifiableMap;
import net.morimekta.collect.UnmodifiableSet;
import net.morimekta.collect.util.Binary;
import net.morimekta.io.BigEndianBinaryInputStream;
import io.pvdnc.core.PMessage;
import io.pvdnc.core.io.PMessageReader;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PFieldRequirement;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PServiceDescriptor;
import io.pvdnc.core.types.PServiceMethod;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.types.PType;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.requireNonNull;
import static io.pvdnc.sio.thrift.ThriftType.asString;
import static io.pvdnc.sio.thrift.ThriftType.forType;

public class ThriftBinaryMessageReader implements PMessageReader {
    // 255 byte (ASCII char) length for a method name is exceptionally long.
    private static final int VERSION_MASK = 0xffff0000;
    private static final int MAX_METHOD_NAME_LEN = 255;

    private final BigEndianBinaryInputStream reader;
    private final boolean strict;
    private final boolean versioned;

    public ThriftBinaryMessageReader(InputStream in) {
        this(in, false, true);
    }

    public ThriftBinaryMessageReader(InputStream in, boolean strict, boolean versioned) {
        this(new BigEndianBinaryInputStream(in), strict, versioned);
    }

    public ThriftBinaryMessageReader(BigEndianBinaryInputStream reader, boolean strict, boolean versioned) {
        this.reader = reader;
        this.strict = strict;
        this.versioned = versioned;
    }

    @Override
    public <M extends PMessage> M read(PMessageDescriptor<M> descriptor) throws IOException {
        return readMessage(reader, descriptor, strict);
    }

    public ThriftServiceCall read(PServiceDescriptor descriptor) throws IOException {
        String          methodName;
        int             sequence;
        int             typeKey;
        ServiceCallType type;

        int methodNameLen = reader.expectInt();
        // Accept both "strict" read mode and non-strict.
        // versioned
        if (methodNameLen <= 0) {
            int version = methodNameLen & VERSION_MASK;
            if (version == ThriftBinaryMessageWriter.VERSION_1) {
                typeKey = methodNameLen & 0xFF;
                methodNameLen = reader.expectInt();
                if (methodNameLen > MAX_METHOD_NAME_LEN) {
                    throw new ApplicationException.$Impl(format("Exceptionally long method name of %s bytes", methodNameLen),
                                                        ApplicationExceptionType.PROTOCOL_ERROR);
                } if (methodNameLen < 1) {
                    throw new ApplicationException.$Impl(format("Exceptionally short method name of %s bytes", methodNameLen),
                                                        ApplicationExceptionType.PROTOCOL_ERROR);
                }
                methodName = new String(reader.expectBytes(methodNameLen), UTF_8);
            } else {
                throw new ApplicationException.$Impl(format("Bad protocol version: %08x", version >>> 16),
                                                    ApplicationExceptionType.INVALID_PROTOCOL);
            }
        } else {
            if (strict && versioned) {
                throw new ApplicationException.$Impl("Missing protocol version",
                                                    ApplicationExceptionType.INVALID_PROTOCOL);
            }

            if (methodNameLen > MAX_METHOD_NAME_LEN) {
                if (methodNameLen >>> 24 == '<') {
                    throw new ApplicationException.$Impl("Received HTML in service call",
                                                        ApplicationExceptionType.PROTOCOL_ERROR);
                }
                if (methodNameLen >>> 24 == '{' || methodNameLen >>> 24 == '[') {
                    throw new ApplicationException.$Impl("Received JSON in service call",
                                                        ApplicationExceptionType.PROTOCOL_ERROR);
                }

                throw new ApplicationException.$Impl(format("Exceptionally long method name of %s bytes", methodNameLen),
                                                    ApplicationExceptionType.PROTOCOL_ERROR);
            }
            methodName = new String(reader.expectBytes(methodNameLen), UTF_8);
            typeKey = reader.expectByte();
        }

        sequence = reader.expectInt();

        type = ServiceCallType.kDescriptor.findById(typeKey);
        PServiceMethod method = descriptor.findMethodForName(methodName);
        if (type == null) {
            throw new ApplicationException.$Impl(format("Invalid call type %d", typeKey),
                                                ApplicationExceptionType.PROTOCOL_ERROR);
        } else if (method == null) {
            throw new ApplicationException.$Impl(format("No such method %s on %s", methodName, descriptor.getTypeName()),
                    ApplicationExceptionType.UNKNOWN_METHOD);
        } else if (type == ServiceCallType.EXCEPTION) {
            ApplicationException exception = read(ApplicationException.kDescriptor);
            return new ThriftServiceCall(method, type, sequence, exception);
        }

        PMessageDescriptor<?> messageDescriptor =
                isRequestCallType(type)
                ? requireNonNull(method.getRequestType(), "requestType == null")
                : requireNonNull(method.getResponseType(), "responseType == null");

        PMessage message = read(messageDescriptor);

        return new ThriftServiceCall(method, type, sequence, message);
    }

    /**
     * Utility method to check if a service call is a request type.
     *
     * @param type The service call type.
     * @return If the service call is a request, and not a response.
     */
    protected boolean isRequestCallType(ServiceCallType type) {
        switch (type) {
            case CALL:
            case ONEWAY:
                return true;
        }
        return false;
    }


    /**
     * Read message from reader.
     *
     * @param input The input reader.
     * @param descriptor The message descriptor.
     * @param strict If the message should be read in strict mode.
     * @param <Message> The message type.
     * @return The read and parsed message.
     * @throws IOException If read failed.
     */
    @SuppressWarnings("unchecked")
    public static <Message extends PMessage>
    Message readMessage(BigEndianBinaryInputStream input,
                        PMessageDescriptor<Message> descriptor,
                        boolean strict) throws IOException {
        PMessageBuilder builder = descriptor.newBuilder();

        // if (builder instanceof BinaryReader) {
        //     ((BinaryReader) builder).readBinary(input, strict);
        // } else {

        ThriftFieldInfo fieldInfo = readFieldInfo(input);
        while (fieldInfo != null) {
            PField field = descriptor.findFieldById(fieldInfo.getId());
            if (field != null) {
                Object value = readFieldValue(input, fieldInfo, field.getResolvedDescriptor(), strict);
                builder.set(field.getId(), value);
            } else if (strict) {
                throw new ApplicationException.$Impl("No " + fieldInfo + " on " + descriptor.getTypeName(),
                                                    ApplicationExceptionType.PROTOCOL_ERROR);
            } else {
                readFieldValue(input, fieldInfo, null, false);
            }

            fieldInfo = readFieldInfo(input);
        }

        if (strict) {
            for (PField field : descriptor.allFields()) {
                if (field.getRequirement() == PFieldRequirement.REQUIRED) {
                    if (!builder.has(field.getId())) {
                        throw new ApplicationException.$Impl("Missing field for " + descriptor.getTypeName() + ": " + field.getName(),
                                                            ApplicationExceptionType.PROTOCOL_ERROR);
                    }
                }
            }
        }

        // }

        return (Message) builder.build();
    }

    /**
     * Consume a message from the stream without parsing the content into a message.
     *
     * @param in Stream to read message from.
     * @throws IOException On read failures.
     */
    public static void consumeMessage(BigEndianBinaryInputStream in) throws IOException {
        ThriftFieldInfo fieldInfo;
        while ((fieldInfo = readFieldInfo(in)) != null) {
            readFieldValue(in, fieldInfo, null, false);
        }
    }

    /**
     * Read field info from stream. If this is the last field (field ID 0)
     * return null.
     *
     * @return The field info or null.
     * @throws IOException If read failed.
     */
    private static ThriftFieldInfo readFieldInfo(BigEndianBinaryInputStream in) throws IOException {
        byte type = in.expectByte();
        if (type == ThriftType.STOP) {
            return null;
        }
        return new ThriftFieldInfo(in.expectShort(), type);
    }

    /**
     * Read a field value from stream.
     *
     * @param in        The stream to consume.
     * @param fieldInfo The field info about the content.
     * @param fieldType The type to generate content for.
     * @param strict    If the field should be read strictly.
     * @return The field value, or null if no type.
     * @throws IOException If unable to read from stream or invalid field type.
     * @param <T> Return type.
     */
    @SuppressWarnings("unchecked")
    public static <T> T readFieldValue(BigEndianBinaryInputStream in,
                                       ThriftFieldInfo fieldInfo,
                                       PDescriptor fieldType,
                                       boolean strict)
            throws IOException {
        if (fieldType != null && forType(fieldType.getType()) != fieldInfo.getType()) {
            throw new ApplicationException.$Impl(format("Wrong field type for id=%d: expected %s, got %s",
                                                       fieldInfo.getId(),
                                                       asString(forType(fieldType.getType())),
                                                       asString(fieldInfo.getType())),
                                                ApplicationExceptionType.PROTOCOL_ERROR);
        }

        switch (fieldInfo.getType()) {
            case ThriftType.VOID:
                return (T) Boolean.TRUE;
            case ThriftType.BOOL:
                return (T) (Object) (in.expectByte() != 0);
            case ThriftType.I8:
                return (T) (Object) in.expectByte();
            case ThriftType.I16:
                return (T) (Object) in.expectShort();
            case ThriftType.I32:
                int val = in.expectInt();
                if (fieldType instanceof PEnumDescriptor) {
                    return (T) ((PEnumDescriptor) fieldType).findById(val);
                } else if (fieldType == PType.FLOAT) {
                    // since ThriftType for FLOAT is I32.
                    return (T) (Object) Float.intBitsToFloat(val);
                } else {
                    return (T) (Object) val;
                }
            case ThriftType.I64:
                return (T) (Object) in.expectLong();
            case ThriftType.DOUBLE:
                if (fieldType == PType.FLOAT) {
                    return (T) (Object) ((Double) in.expectDouble()).floatValue();
                }
                return (T) (Object) in.expectDouble();
            case ThriftType.STRING:
                int len = in.expectUInt32();
                byte[] data = in.expectBytes(len);
                if (fieldType == PType.STRING) {
                    return (T) new String(data, StandardCharsets.UTF_8);
                } else {
                    return (T) Binary.wrap(data);
                }
            case ThriftType.STRUCT: {
                if (fieldType == null) {
                    consumeMessage(in);
                    return null;
                } else if (fieldType == PType.ANY) {
                    return (T) readMessage(in, AnyBinary.kDescriptor, strict);
                }
                return (T) readMessage(in, (PMessageDescriptor<?>) fieldType, strict);
            }
            case ThriftType.MAP: {
                final byte keyT = in.expectByte();
                final byte itemT = in.expectByte();
                final int size = in.expectUInt32();

                PDescriptor keyType = null;
                PDescriptor                valueType = null;
                MapBuilder<Object, Object> out;
                if (fieldType != null) {
                    @SuppressWarnings("unchecked")
                    PMapDescriptor<Object, Object> mapType = (PMapDescriptor<Object, Object>) fieldType;
                    keyType = mapType.getKeyType();
                    valueType = mapType.getValueType();

                    out = mapType.builder(size);
                } else {
                    out = UnmodifiableMap.newBuilder(size);
                }

                ThriftFieldInfo keyInfo  = new ThriftFieldInfo(1, keyT);
                ThriftFieldInfo itemInfo = new ThriftFieldInfo(2, itemT);
                for (int i = 0; i < size; ++i) {
                    Object key = readFieldValue(in, keyInfo, keyType, strict);
                    Object value = readFieldValue(in, itemInfo, valueType, strict);
                    if (key != null && value != null) {
                        out.put(key, value);
                    } else if (strict) {
                        if (key == null) {
                            throw new ApplicationException.$Impl("Null key in map", ApplicationExceptionType.PROTOCOL_ERROR);
                        } else {
                            throw new ApplicationException.$Impl("Null value in map", ApplicationExceptionType.PROTOCOL_ERROR);
                        }
                    }
                }
                return (T) out.build();
            }
            case ThriftType.SET: {
                final byte itemT = in.expectByte();
                final int size = in.expectUInt32();

                PDescriptor        entryType = null;
                SetBuilder<Object> out;
                if (fieldType != null) {
                    @SuppressWarnings("unchecked")
                    PSetDescriptor<Object> setType = (PSetDescriptor<Object>) fieldType;
                    entryType = setType.getItemType();
                    out = setType.builder(size);
                } else {
                    out = UnmodifiableSet.newBuilder(size);
                }

                ThriftFieldInfo itemInfo = new ThriftFieldInfo(0, itemT);
                for (int i = 0; i < size; ++i) {
                    Object value = readFieldValue(in, itemInfo, entryType, strict);
                    if (value != null) {
                        out.add(value);
                    } else if (strict) {
                        throw new ApplicationException.$Impl("Null value in set", ApplicationExceptionType.PROTOCOL_ERROR);
                    }
                }

                return (T) out.build();
            }
            case ThriftType.LIST: {
                final byte itemT = in.expectByte();
                final int size = in.expectUInt32();

                PDescriptor         entryType = null;
                ListBuilder<Object> out;
                if (fieldType != null) {
                    @SuppressWarnings("unchecked")
                    PListDescriptor<Object> listType = (PListDescriptor<Object>) fieldType;
                    entryType = listType.getItemType();
                    out = listType.builder(size);
                } else {
                    out = UnmodifiableList.newBuilder(size);
                }

                ThriftFieldInfo itemInfo = new ThriftFieldInfo(0, itemT);
                for (int i = 0; i < size; ++i) {
                    Object value = readFieldValue(in, itemInfo, entryType, strict);
                    if (value != null) {
                        out.add(value);
                    } else if (strict) {
                        throw new ApplicationException.$Impl("Null value in list", ApplicationExceptionType.PROTOCOL_ERROR);
                    }
                }

                return (T) out.build();
            }
            default:
                throw new ApplicationException.$Impl("unknown data type: " + asString(fieldInfo.getType()),
                                                    ApplicationExceptionType.PROTOCOL_ERROR);
        }
    }
}
