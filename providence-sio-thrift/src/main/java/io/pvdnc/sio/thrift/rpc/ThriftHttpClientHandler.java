/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.thrift.rpc;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.impl.Empty;
import io.pvdnc.core.rpc.PServiceCallHandler;
import io.pvdnc.core.types.PServiceMethod;
import io.pvdnc.sio.thrift.ThriftProperties;
import io.pvdnc.sio.thrift.ThriftSerializer;
import io.pvdnc.sio.thrift.ThriftSerializerProvider;
import io.pvdnc.sio.thrift.ThriftServiceCall;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.UncheckedIOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

import static java.net.http.HttpRequest.BodyPublishers.ofInputStream;
import static java.util.Objects.requireNonNull;

/**
 * A client handler using native Java HTTP library, connecting to
 * a URI based on a simple 1 path per method, default using {@code /methodName}.
 * The client does not size it's output, and assumes all non-200
 * status codes are general HTTP exceptions not part of the protocol.
 */
public class ThriftHttpClientHandler implements PServiceCallHandler, Closeable {
    private final HttpClient                    httpClient;
    private final Supplier<HttpRequest.Builder> httpRequestBuilderSupplier;
    private final ThriftSerializerProvider      provider;
    private final Supplier<URI>                 uriSupplier;
    private final ExecutorService               executorService;
    private final AtomicInteger                 nextSequenceNo;

    public ThriftHttpClientHandler(String baseURI) {
        this(HttpClient.newBuilder().build(),
             HttpRequest::newBuilder,
             new ThriftSerializerProvider().load(),
             () -> URI.create(baseURI),
             Executors.newFixedThreadPool(5, run -> {
                 Thread thread = new Thread(run);
                 thread.setDaemon(true);
                 thread.setName("thrift-http-writer@" + thread.getId());
                 return thread;
             }));
    }

    public ThriftHttpClientHandler(HttpClient httpClient,
                                   Supplier<HttpRequest.Builder> httpRequestBuilderSupplier,
                                   ThriftSerializerProvider serializerProvider,
                                   Supplier<URI> uriSupplier,
                                   ExecutorService executorService) {
        this.httpClient = requireNonNull(httpClient, "httpClient == null");
        this.httpRequestBuilderSupplier = requireNonNull(httpRequestBuilderSupplier, "requestBuilderSupplier == null");
        this.provider = requireNonNull(serializerProvider, "serializerProvider == null");
        this.uriSupplier = requireNonNull(uriSupplier, "uriSupplier == null");
        this.executorService = requireNonNull(executorService, "executorService == null");
        this.nextSequenceNo = new AtomicInteger(1);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <RQ extends PMessage, RS extends PMessage>
    RS handleCall(PServiceMethod method, RQ request)
            throws IOException {
        try {
            if (executorService.isShutdown()) {
                throw new ApplicationException.$Impl("Calling on closed client.", ApplicationExceptionType.UNKNOWN);
            }
            boolean oneway = method.getProperty(ThriftProperties.THRIFT_SERVICE_METHOD_ONEWAY);
            int sequence = nextSequenceNo.getAndUpdate(ThriftHttpClientHandler::nextSequence);
            ThriftServiceCall call = new ThriftServiceCall(
                    method,
                    oneway ? ServiceCallType.ONEWAY : ServiceCallType.CALL,
                    sequence,
                    request);

            ThriftSerializer serializer = provider.defaultSerializer();
            URI              uri        = uriSupplier.get();
            HttpResponse<InputStream> httpResponse = httpClient.send(
                    makeRequest(call, uri),
                    HttpResponse.BodyHandlers.ofInputStream());
            if (httpResponse.statusCode() == 200) {
                if (oneway) {
                    httpResponse.body().close();
                    return (RS) Empty.empty();
                }
                ThriftServiceCall response;
                ThriftSerializer outputSerializer = provider
                        .forMediaTypes(httpResponse.headers().allValues("Content-Type"))
                        .orElse(serializer);
                try (BufferedInputStream bin = new BufferedInputStream(httpResponse.body())) {
                    response = outputSerializer.readFrom(method.getOnServiceType(), bin);
                }
                if (response.getCallType() == ServiceCallType.EXCEPTION) {
                    throw (ApplicationException.$Impl) response.getMessage();
                } else if (response.getCallType() != ServiceCallType.REPLY) {
                    throw new ApplicationException.$Impl(
                            "Bad response type: " + response.getCallType(),
                            ApplicationExceptionType.INVALID_MESSAGE_TYPE);
                } else if (!response.getMethodName().equals(method.getName())) {
                    throw new ApplicationException.$Impl(
                            "Bad response method: " + response.getMethodName() + " != " + method.getName(),
                            ApplicationExceptionType.WRONG_METHOD_NAME);
                } else if (response.getSequenceNo() != sequence) {
                    throw new ApplicationException.$Impl(
                            "Bad response sequence: " + response.getSequenceNo() + " != " + sequence,
                            ApplicationExceptionType.BAD_SEQUENCE_ID);
                }
                return (RS) response.getMessage();
            } else if (httpResponse.statusCode() == 204) {
                if (oneway) {
                    httpResponse.body().close();
                    return (RS) Empty.empty();
                }
                throw new ApplicationException.$Impl("Empty body on non-oneway method", ApplicationExceptionType.MISSING_RESULT);
            } else {
                String content;
                try (BufferedInputStream bin = new BufferedInputStream(httpResponse.body())) {
                    content = new String(bin.readAllBytes(), StandardCharsets.UTF_8);
                } catch (IOException e) {
                    String responseMessage = String.format("Exception when reading error(%d) on %s", httpResponse.statusCode(), uri);
                    throw new ApplicationException.$Impl(responseMessage, ApplicationExceptionType.UNKNOWN).initCause(e);
                }
                String responseMessage = String.format("Exception when reading error(%d) on %s: %s", httpResponse.statusCode(), uri, content);
                throw new ApplicationException.$Impl(responseMessage, ApplicationExceptionType.UNKNOWN);
            }
        } catch (InterruptedException ie) {
            close();
            // Thread.currentThread().interrupt();
            throw new RuntimeException(ie);
        }
    }

    @Override
    public void close() {
        executorService.shutdownNow();
    }

    private HttpRequest makeRequest(ThriftServiceCall request, URI url) {
        ThriftSerializer serializer = provider.defaultSerializer();
        return httpRequestBuilderSupplier
                .get()
                .uri(url)
                .header("Content-Type", serializer.mediaTypes().get(0))
                .POST(ofInputStream(() -> pipedWriter(request, serializer)))
                .build();
    }

    private InputStream pipedWriter(ThriftServiceCall request, ThriftSerializer serializer) {
        try {
            PipedOutputStream out = new PipedOutputStream();
            PipedInputStream  in  = new PipedInputStream(out);
            // TODO: Keep futures locally and clean up on close / timeouts?
            executorService.submit(() -> pipedRunner(request, serializer, out));
            return in;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private void pipedRunner(ThriftServiceCall request, ThriftSerializer serializer, OutputStream out) {
        try (BufferedOutputStream bout = new BufferedOutputStream(out)) {
            serializer.writeTo(request, bout);
            bout.flush();
        } catch (IOException e) {
            try {
                out.close();
            } catch (IOException e2) {
                e.addSuppressed(e2);
            }
            throw new UncheckedIOException(e);
        }
    }

    private static int nextSequence(int i) {
        return Math.max(1, i + 1);
    }
}
