/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.thrift;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.types.PServiceMethod;
import io.pvdnc.sio.thrift.rpc.ServiceCallType;

import java.util.Objects;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

public class ThriftServiceCall {
    private final PServiceMethod  method;
    private final String          methodName;
    private final ServiceCallType callType;
    private final int             sequenceNo;
    private final PMessage        message;

    public ThriftServiceCall(PServiceMethod method,
                             ServiceCallType callType,
                             int sequenceNo,
                             PMessage message) {
        this.method = requireNonNull(method, "method == null");
        this.methodName = method.getName();
        this.callType = requireNonNull(callType, "callType == null");
        this.sequenceNo = sequenceNo;
        this.message = requireNonNull(message, "message == null");
    }

    public PServiceMethod getMethod() {
        return method;
    }

    public String getMethodName() {
        return methodName;
    }

    public ServiceCallType getCallType() {
        return callType;
    }

    public int getSequenceNo() {
        return sequenceNo;
    }

    public PMessage getMessage() {
        return message;
    }

    // ---- Object ----


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ThriftServiceCall that = (ThriftServiceCall) o;
        return sequenceNo == that.sequenceNo &&
               methodName.equals(that.methodName) &&
               callType == that.callType &&
               message.equals(that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(methodName, callType, sequenceNo, message);
    }

    @Override
    public String toString() {
        return "ThriftServiceCall{" +
               "method='" + methodName + '\'' +
               ", callType=" + callType +
               ", sequenceNo=" + sequenceNo +
               ", message=" + message +
               '}';
    }
}
