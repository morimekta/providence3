/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.thrift;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PServiceDescriptor;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import static net.morimekta.collect.UnmodifiableList.listOf;

public class ThriftBinarySerializer implements ThriftSerializer {
    public static final String MEDIA_TYPE = "application/vnd.apache.thrift.binary";
    public static final String MEDIA_TYPE_OLD = "application/x-thrift";

    private final boolean strict;
    private final boolean versioned;

    public ThriftBinarySerializer() {
        this(false, true);
    }

    public ThriftBinarySerializer(boolean strict, boolean versioned) {
        this.strict = strict;
        this.versioned = versioned;
    }

    public boolean isVersioned() {
        return versioned;
    }

    public ThriftBinarySerializer unVersioned() {
        if (!versioned) return this;
        return new ThriftBinarySerializer(strict, false);
    }

    // --- PSerializer

    @Override
    public List<String> mediaTypes() {
        return listOf(MEDIA_TYPE, MEDIA_TYPE_OLD);
    }

    @Override
    public <M extends PMessage> M readFrom(PMessageDescriptor<M> descriptor, InputStream in) throws IOException {
        return new ThriftBinaryMessageReader(in, strict, versioned).read(descriptor);
    }

    @Override
    public void writeTo(PMessage message, OutputStream out) throws IOException {
        new ThriftBinaryMessageWriter(out, strict, versioned).write(message);
    }

    @Override
    public boolean isStrict() {
        return strict;
    }

    @Override
    public ThriftBinarySerializer strict() {
        if (strict) return this;
        return new ThriftBinarySerializer(true, versioned);
    }

    // --- ThriftSerializer

    @Override
    public ThriftServiceCall readFrom(PServiceDescriptor service, InputStream in) throws IOException {
        return new ThriftBinaryMessageReader(in, strict, versioned).read(service);
    }

    @Override
    public void writeTo(ThriftServiceCall call, OutputStream out) throws IOException {
        new ThriftBinaryMessageWriter(out, strict, versioned).write(call);
    }
}
