/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.thrift;

import java.util.Locale;

public class ThriftFieldInfo {
    private final int id;
    private final byte type;

    /**
     * Create a field info instance.
     *
     * @param id The field ID or key.
     * @param type The field binary written type.
     */
    public ThriftFieldInfo(int id, byte type) {
        this.id = id;
        this.type = type;
    }

    @Override
    public String toString() {
        return String.format(Locale.US, "field(%d: %s)", id, ThriftType.asString(type));
    }

    /**
     * @return The field ID or key.
     */
    public int getId() {
        return id;
    }

    /**
     * @return The binary field type.
     */
    public byte getType() {
        return type;
    }
}
