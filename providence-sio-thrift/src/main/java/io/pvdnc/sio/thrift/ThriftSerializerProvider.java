/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.thrift;

import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

import static java.lang.System.Logger.Level.WARNING;
import static java.util.Optional.ofNullable;

public class ThriftSerializerProvider {
    private static final System.Logger LOGGER = System.getLogger(ThriftSerializerProvider.class.getName());

    private final Map<String, ThriftSerializer> serializerMap;
    private final AtomicReference<ThriftSerializer> defaultSerializer;

    public ThriftSerializerProvider() {
        serializerMap = new ConcurrentHashMap<>();
        defaultSerializer = new AtomicReference<>();
    }

    public ThriftSerializerProvider load() {
        for (ThriftSerializer serializer : ThriftSerializer.listSerializers()) {
            register(serializer);
        }
        setDefault(ThriftBinarySerializer.MEDIA_TYPE);
        return this;
    }

    public ThriftSerializerProvider setDefault(String mediaType) {
        ThriftSerializer serializer = forMediaType(mediaType)
                .orElseThrow(() -> new IllegalArgumentException("No serializer for media type: " + mediaType));
        defaultSerializer.set(serializer);
        return this;
    }

    public ThriftSerializerProvider registerDefault(ThriftSerializer serializer) {
        defaultSerializer.set(serializer);
        return register(serializer);
    }

    public ThriftSerializerProvider register(ThriftSerializer serializer) {
        // make the first serializer we encounter into the default.
        defaultSerializer.updateAndGet(s -> s != null ? s : serializer);
        for (String mediaType : serializer.mediaTypes()) {
            if (serializerMap.containsKey(mediaType)) {
                LOGGER.log(WARNING, "Existing media type: " + mediaType + " -> " + serializer.getClass().getSimpleName());
            }
            serializerMap.put(mediaType, serializer);
        }
        return this;
    }

    public Optional<ThriftSerializer> forMediaTypes(Collection<String> mediaTypes) {
        for (String mediaType : mediaTypes) {
            String normalized = mediaType.replaceAll(";.*", "").strip().toLowerCase(Locale.US);
            if (serializerMap.containsKey(normalized)) {
                return Optional.of(serializerMap.get(normalized));
            } else if (serializerMap.containsKey(mediaType)) {
                return Optional.of(serializerMap.get(mediaType));
            }
        }
        return Optional.empty();
    }

    public Optional<ThriftSerializer> forMediaType(String mediaType) {
        String normalized = mediaType.replaceAll(";.*", "").strip();
        return ofNullable(serializerMap.get(mediaType))
                .or(() -> ofNullable(serializerMap.get(normalized)));
    }

    public ThriftSerializer defaultSerializer() {
        return Optional.ofNullable(defaultSerializer.get())
                .orElseThrow(() -> new IllegalArgumentException("No default serializer"));
    }
}
