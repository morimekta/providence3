/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.thrift;

import io.pvdnc.core.property.PPersistedProperty;
import io.pvdnc.core.property.PProperty;

import static io.pvdnc.core.property.PPropertyTarget.SERVICE;
import static io.pvdnc.core.property.PPropertyTarget.SERVICE_METHOD;

public class ThriftProperties {
    /**
     * A thrift service method has the property that the response struct
     * is a union of a single '0: result success' field, and the rest
     * of the response fields are exceptions. Also each field in the
     * request object are used as parameters for the service method itself.
     * <p>
     * It is also likely that the
     * <p>
     * E.g.:
     *
     * <pre>{@code
     * service MyService {
     *     i32 myMethod(
     *         1: string foo
     *     ) throws (
     *         1: MyException me;
     *     )
     * }
     * }</pre>
     *
     * Will make an interface like:
     *
     * <pre>{@code
     * interface MyService {
     *     // This is the Stub equivalent of the thrift service
     *     MyService$MyMethod$Response myMethod(MyService$MyMethod$Request request) throws IOException;
     *
     *     interface Iface {
     *         int myMethod(String foo) throws IOException, MyException;
     *     }
     *
     *     static class IfaceWrapper implements MyService {
     *         private final Iface impl;
     *
     *         MyService$MyMethod$Response myMethod(MyService$MyMethod$Request request) throws IOException {
     *             try {
     *                 return MyService$MyMethod$Response.withSuccess(impl.myMethod(request.getFoo()));
     *             } catch (MyException e) (
     *                 return MyService$MyMethod$Response.withMe(e);
     *             }
     *         }
     *     }
     *
     *     // ... and the default service stub implementations.
     * }
     * }</pre>
     */
    public static final PProperty<Boolean> THRIFT_SERVICE;
    public static final PProperty<String>  THRIFT_SERVICE_METHODS_THROWS;
    public static final PProperty<Boolean> THRIFT_SERVICE_METHOD;

    /**
     * Marking on a service method to say it's oneway, and should not send a response
     * service call. And caller should not expect one.
     */
    public static final PProperty<Boolean> THRIFT_SERVICE_METHOD_ONEWAY;

    static {
        THRIFT_SERVICE = new PPersistedProperty<>(
                "thrift.service", Boolean.TYPE, Boolean.FALSE, SERVICE);
        THRIFT_SERVICE_METHODS_THROWS = new PPersistedProperty<>(
                "thrift.service.methods.throws", String.class, null, true, SERVICE, SERVICE_METHOD);
        THRIFT_SERVICE_METHOD = new PPersistedProperty<>(
                "thrift.service.method", Boolean.TYPE, Boolean.FALSE, SERVICE_METHOD);
        THRIFT_SERVICE_METHOD_ONEWAY = new PPersistedProperty<>(
                "thrift.service.method.oneway", Boolean.TYPE, Boolean.FALSE, SERVICE_METHOD);
        PProperty.registerProperties(THRIFT_SERVICE,
                                     THRIFT_SERVICE_METHODS_THROWS,
                                     THRIFT_SERVICE_METHOD,
                                     THRIFT_SERVICE_METHOD_ONEWAY);
    }

    private ThriftProperties() {}
}
