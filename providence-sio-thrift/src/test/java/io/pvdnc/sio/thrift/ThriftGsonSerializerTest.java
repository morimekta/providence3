package io.pvdnc.sio.thrift;

import io.pvdnc.core.types.PServiceDescriptor;
import io.pvdnc.core.types.PServiceMethod;
import io.pvdnc.sio.thrift.rpc.ApplicationException;
import io.pvdnc.sio.thrift.rpc.ApplicationExceptionType;
import io.pvdnc.sio.thrift.rpc.ServiceCallType;
import io.pvdnc.sio.thrift.test.PvdException;
import io.pvdnc.sio.thrift.test.PvdService;
import io.pvdnc.sio.thrift.test.PvdService_FullMethod_Request;
import io.pvdnc.sio.thrift.test.PvdService_FullMethod_Response;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.stream.Stream;

import static java.nio.charset.StandardCharsets.UTF_8;
import static net.morimekta.collect.UnmodifiableList.listOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class ThriftGsonSerializerTest extends TestSerializerBase {
    @Test
    public void testVariants() {
        ThriftGsonSerializer serializer = new ThriftGsonSerializer();
        assertThat(serializer.mediaTypes(), is(listOf("application/json")));
        assertThat(serializer.isStrict(), is(false));
        assertThat(serializer.isPretty(), is(false));

        ThriftGsonSerializer strict = serializer.strict();
        assertThat(strict.isPretty(), is(false));
        assertThat(strict.isStrict(), is(true));
        assertThat(strict.strict(), is(sameInstance(strict)));

        ThriftGsonSerializer pretty = serializer.pretty();
        assertThat(pretty.isPretty(), is(true));
        assertThat(pretty.isStrict(), is(false));
        assertThat(pretty.pretty(), is(sameInstance(pretty)));

        ThriftGsonSerializer both = strict.pretty();
        assertThat(both.isPretty(), is(true));
        assertThat(both.isStrict(), is(true));
        assertThat(both.pretty(), is(sameInstance(both)));
        assertThat(both.strict(), is(sameInstance(both)));
    }

    public static Stream<Arguments> testSerializeData() {
        ThriftGsonSerializer serializer = new ThriftGsonSerializer();
        ThriftGsonSerializer pretty = serializer.pretty();
        return Stream.of(arguments(serializer,
                                   new ThriftServiceCall(PvdService.$Descriptor.kMethodFullMethod,
                                                         ServiceCallType.CALL,
                                                         42,
                                                         PvdService_FullMethod_Request.newBuilder()
                                                                                      .setB(true)
                                                                                      .setI(42)
                                                                                      .build()),
                                   "[\"fullMethod\",\"call\",42,{\"b\":true,\"i\":42}]"),
                         arguments(pretty,
                                   new ThriftServiceCall(PvdService.$Descriptor.kMethodFullMethod,
                                                         ServiceCallType.REPLY,
                                                         42,
                                                         PvdService_FullMethod_Response.newBuilder()
                                                                                       .setEx(PvdException.newBuilder().setText("foo").build())
                                                                                       .build()),
                                   "[\n" +
                                   "  \"fullMethod\",\n" +
                                   "  \"reply\",\n" +
                                   "  42,\n" +
                                   "  {\n" +
                                   "    \"ex\": {\n" +
                                   "      \"text\": \"foo\"\n" +
                                   "    }\n" +
                                   "  }\n" +
                                   "]\n"),
                         arguments(serializer,
                                   new ThriftServiceCall(PvdService.$Descriptor.kMethodFullMethod,
                                                         ServiceCallType.EXCEPTION,
                                                         42,
                                                         new ApplicationException.$Impl("__test__", ApplicationExceptionType.PROTOCOL_ERROR)),
                                   "[\"fullMethod\",\"exception\",42,{\"message\":\"__test__\",\"type\":\"PROTOCOL_ERROR\"}]"));
    }

    @ParameterizedTest
    @MethodSource("testSerializeData")
    public void testSerialize(ThriftGsonSerializer serializer,
                              ThriftServiceCall call,
                              String expected) throws IOException {
        PServiceMethod        method = call.getMethod();
        ByteArrayOutputStream out    = new ByteArrayOutputStream();
        serializer.writeTo(call, out);
        assertThat(out.toString(UTF_8), is(expected));
        ByteArrayInputStream in     = new ByteArrayInputStream(out.toByteArray());
        ThriftServiceCall    parsed = serializer.readFrom(method.getOnServiceType(), in);
        assertThat(parsed, is(call));
        assertThat(parsed.getMethod(), is(method));
    }

    public static Stream<Arguments> testDeserializeData() {
        ThriftGsonSerializer serializer = new ThriftGsonSerializer();
        return Stream.of(arguments(serializer,
                                   "[\"fullMethod\",\"CALL\",42,{\"b\":true,\"i\":42}]",
                                   new ThriftServiceCall(PvdService.$Descriptor.kMethodFullMethod,
                                                         ServiceCallType.CALL,
                                                         42,
                                                         PvdService_FullMethod_Request.newBuilder()
                                                                                      .setB(true)
                                                                                      .setI(42)
                                                                                      .build())),
                         arguments(serializer,
                                   "[\"fullMethod\",1,42,{\"b\":true,\"i\":42}]",
                                   new ThriftServiceCall(PvdService.$Descriptor.kMethodFullMethod,
                                                         ServiceCallType.CALL,
                                                         42,
                                                         PvdService_FullMethod_Request.newBuilder()
                                                                                      .setB(true)
                                                                                      .setI(42)
                                                                                      .build())));
    }

    @ParameterizedTest
    @MethodSource("testDeserializeData")
    public void testDeserialize(ThriftGsonSerializer serializer,
                                String content,
                                ThriftServiceCall expected) throws IOException {
        PServiceMethod       method = expected.getMethod();
        ByteArrayInputStream in     = new ByteArrayInputStream(content.getBytes(UTF_8));
        ThriftServiceCall    parsed = serializer.readFrom(method.getOnServiceType(), in);
        assertThat(parsed, is(expected));
        assertThat(parsed.getMethod(), is(method));
    }

    public static Stream<Arguments> testDeserialize_FailsData() {
        ThriftGsonSerializer serializer = new ThriftGsonSerializer();
        return Stream.of(arguments(serializer,
                                   "[\"notAMethod\",\"CALL\",42,{\"b\":true,\"i\":42}]",
                                   PvdService.kDescriptor,
                                   "No such method 'notAMethod' on test.PvdService",
                                   ApplicationExceptionType.UNKNOWN_METHOD),
                         arguments(serializer,
                                   "[\"fullMethod\",\"BALL\",42,{\"b\":true,\"i\":42}]",
                                   PvdService.kDescriptor,
                                   "Unknown type: BALL",
                                   ApplicationExceptionType.INVALID_MESSAGE_TYPE),
                         arguments(serializer,
                                   "[\"fullMethod\",\"CALL\",-42,{\"b\":true,\"i\":42}]",
                                   PvdService.kDescriptor,
                                   "Invalid sequence no -42",
                                   ApplicationExceptionType.BAD_SEQUENCE_ID),
                         arguments(serializer,
                                   "[\"fullMethod\",\"CALL\",42]",
                                   PvdService.kDescriptor,
                                   "Bad JSON content: Expected BEGIN_OBJECT but was END_ARRAY at line 1 column 25 path $[3]",
                                   ApplicationExceptionType.PROTOCOL_ERROR));
    }

    @ParameterizedTest
    @MethodSource("testDeserialize_FailsData")
    public void testDeserialize_Fails(ThriftGsonSerializer serializer,
                                      String content,
                                      PServiceDescriptor service,
                                      String expectedMessage,
                                      ApplicationExceptionType expectedType) throws IOException {
        try {
            ByteArrayInputStream in     = new ByteArrayInputStream(content.getBytes(UTF_8));
            serializer.readFrom(service, in);
            fail("no exception: " + expectedMessage);
        } catch (ApplicationException.$Impl e) {
            assertThat(e.getMessage(), is(expectedMessage));
            assertThat(e.getType(), is(expectedType));
        }
    }
}
