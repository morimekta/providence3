package io.pvdnc.sio.thrift;

import io.pvdnc.core.types.PType;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ThriftTypeTest {
    @Test
    public void testAsString() {
        assertThat(ThriftType.asString(ThriftType.STOP), is("stop(0)"));
        assertThat(ThriftType.asString(ThriftType.VOID), is("void(1)"));
        assertThat(ThriftType.asString(ThriftType.BOOL), is("bool(2)"));
        assertThat(ThriftType.asString(ThriftType.I8), is("i8(3)"));
        assertThat(ThriftType.asString(ThriftType.DOUBLE), is("double(4)"));
        assertThat(ThriftType.asString((byte) 5), is("unknown(5)"));
        assertThat(ThriftType.asString(ThriftType.I16), is("i16(6)"));
        assertThat(ThriftType.asString((byte) 7), is("unknown(7)"));
        assertThat(ThriftType.asString(ThriftType.I32), is("i32(8)"));
        assertThat(ThriftType.asString((byte) 9), is("unknown(9)"));
        assertThat(ThriftType.asString(ThriftType.I64), is("i64(10)"));
        assertThat(ThriftType.asString(ThriftType.STRING), is("string(11)"));
        assertThat(ThriftType.asString(ThriftType.STRUCT), is("struct(12)"));
        assertThat(ThriftType.asString(ThriftType.MAP), is("map(13)"));
        assertThat(ThriftType.asString(ThriftType.SET), is("set(14)"));
        assertThat(ThriftType.asString(ThriftType.LIST), is("list(15)"));
    }
    @Test
    public void testForType() {
        assertThat(ThriftType.forType(PType.VOID), is(ThriftType.VOID));
        for (PType type : PType.values()) {
            // check that all types are exhausted.
            ThriftType.forType(type);
        }
    }
}
