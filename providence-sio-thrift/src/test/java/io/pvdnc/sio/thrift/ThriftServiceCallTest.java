package io.pvdnc.sio.thrift;

import io.pvdnc.sio.thrift.rpc.ServiceCallType;
import io.pvdnc.sio.thrift.test.PvdMessage;
import io.pvdnc.sio.thrift.test.PvdService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class ThriftServiceCallTest {
    @Test
    public void testConstructor() {
        ThriftServiceCall call = new ThriftServiceCall(
                PvdService.$Descriptor.kMethodPing, ServiceCallType.CALL, 42, PvdMessage.newBuilder().setI(42));
        assertThat(call.getCallType(), CoreMatchers.is(ServiceCallType.CALL));
        assertThat(call.getSequenceNo(), is(42));
        assertThat(call.getMethod(), is(PvdService.$Descriptor.kMethodPing));
        assertThat(call.getMethodName(), is("ping"));
        assertThat(call.getMessage(), is(PvdMessage.newBuilder().setI(42)));
        ThriftServiceCall other = new ThriftServiceCall(
                PvdService.$Descriptor.kMethodPing, ServiceCallType.CALL, 42, PvdMessage.newBuilder().setI(42));
        ThriftServiceCall diff = new ThriftServiceCall(
                PvdService.$Descriptor.kMethodPing, ServiceCallType.CALL, 42, PvdMessage.newBuilder().setI(33));

        assertThat(call, is(other));
        assertThat(call, is(not(diff)));
        assertThat(call, is(not(new Object())));
    }

    @Test
    public void testConstructor_method() {
        ThriftServiceCall call = new ThriftServiceCall(
                PvdService.$Descriptor.kMethodFullMethod, ServiceCallType.CALL, 42, PvdMessage.newBuilder().setI(42));
        assertThat(call.getCallType(), CoreMatchers.is(ServiceCallType.CALL));
        assertThat(call.getSequenceNo(), is(42));
        assertThat(call.getMethod(), is(PvdService.$Descriptor.kMethodFullMethod));
        assertThat(call.getMethodName(), is("fullMethod"));
        assertThat(call.getMessage(), is(PvdMessage.newBuilder().setI(42)));

        ThriftServiceCall other = new ThriftServiceCall(
                PvdService.$Descriptor.kMethodFullMethod, ServiceCallType.CALL, 42, PvdMessage.newBuilder().setI(42));
        ThriftServiceCall diff = new ThriftServiceCall(
                PvdService.$Descriptor.kMethodFullMethod, ServiceCallType.CALL, 42, PvdMessage.newBuilder().setI(33));
        assertThat(call, is(other));
        assertThat(call, is(not(diff)));
        assertThat(call, is(not(new Object())));
        assertThat(call.hashCode(), is(other.hashCode()));
        assertThat(call.hashCode(), is(not(diff.hashCode())));
    }
}
