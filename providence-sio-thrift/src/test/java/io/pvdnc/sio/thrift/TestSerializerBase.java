package io.pvdnc.sio.thrift;

import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.reflect.CEnumDescriptor;
import io.pvdnc.core.reflect.CMessageDescriptor;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.types.PType;
import org.junit.jupiter.api.BeforeEach;

import static io.pvdnc.core.property.PJsonProperties.JSON_COMPACT;
import static io.pvdnc.core.property.PJsonProperties.JSON_NAME;

public class TestSerializerBase {
    protected PEnumDescriptor       testEnum;
    protected PMessageDescriptor<?> testMessage;
    protected PMessageDescriptor<?> testWrapper;
    protected PMessageDescriptor<?> testCompact;

    @BeforeEach
    public void setUp() {
        testEnum = CEnumDescriptor.builder("test", "ContainedEnum")
                                  .value("FIRST")
                                  .value("SECOND", PPropertyMap.of(JSON_NAME, "second"))
                                  .value("THIRD").build();

        CMessageDescriptor.Builder messageB = CMessageDescriptor.builder("test", "ContainedMessage");
        messageB.field("b", PType.BOOL).id(1);
        messageB.field("bt", PType.BYTE).id(2);
        messageB.field("s", PType.SHORT).id(3);
        messageB.field("i", PType.INT).id(4);
        messageB.field("l", PType.LONG).id(5);
        messageB.field("d", PType.DOUBLE).id(7);
        messageB.field("str", PType.STRING).id(8);
        messageB.field("bin", PType.BINARY).id(9);
        messageB.field("e", testEnum).id(10);
        messageB.field("strings", PListDescriptor.provider(PType.STRING.provider())).id(11);
        messageB.field("doubles", PSetDescriptor.provider(PType.DOUBLE.provider())).id(12);
        messageB.field("is_map", PMapDescriptor.provider(PType.INT.provider(), PType.STRING.provider())).id(13);
        testMessage = messageB.build();

        CMessageDescriptor.Builder compactB = CMessageDescriptor.builder("test", "ContainedCompact");
        compactB.field("b", PType.BOOL).id(1);
        compactB.field("bt", PType.BYTE).id(2);
        compactB.field("s", PType.SHORT).id(3);
        compactB.field("i", PType.INT).id(4);
        compactB.field("l", PType.LONG).id(5);
        compactB.property(JSON_COMPACT, true);
        testCompact = compactB.build();

        CMessageDescriptor.Builder wrapperB = CMessageDescriptor.builder("test", "ContainedWrapper");
        wrapperB.field("msg", testCompact).id(1);
        wrapperB.field("msg_list", PListDescriptor.provider(() -> testMessage)).id(2);
        wrapperB.field("msg_set", PSetDescriptor.provider(() -> testCompact)).id(3);
        wrapperB.field("msg_map", PMapDescriptor.provider(PType.STRING.provider(), () -> testMessage)).id(4);
        testWrapper = wrapperB.build();
    }
}
