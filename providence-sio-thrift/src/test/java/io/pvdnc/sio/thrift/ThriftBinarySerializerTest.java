package io.pvdnc.sio.thrift;

import io.pvdnc.core.PMessage;
import io.pvdnc.sio.thrift.test.PvdCompact;
import io.pvdnc.sio.thrift.test.PvdEnum;
import io.pvdnc.sio.thrift.test.PvdMessage;
import io.pvdnc.sio.thrift.test.PvdUnion;
import io.pvdnc.sio.thrift.test.ThriftCompact;
import io.pvdnc.sio.thrift.test.ThriftEnum;
import io.pvdnc.sio.thrift.test.ThriftMessage;
import io.pvdnc.sio.thrift.test.ThriftUnion;
import org.apache.thrift.TBase;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TIOStreamTransport;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.stream.Stream;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static net.morimekta.collect.util.Binary.fromHexString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class ThriftBinarySerializerTest extends TestSerializerBase {
    @Test
    public void testBasic() {
        ThriftBinarySerializer serializer = new ThriftBinarySerializer();
        assertThat(serializer.mediaTypes(), is(listOf("application/vnd.apache.thrift.binary", "application/x-thrift")));
        assertThat(serializer.isStrict(), is(false));
        assertThat(serializer.isVersioned(), is(true));

        ThriftBinarySerializer strict = serializer.strict();
        assertThat(strict.isStrict(), is(true));
        assertThat(strict.isVersioned(), is(true));
        assertThat(strict.strict(), is(sameInstance(strict)));

        ThriftBinarySerializer unVersioned = serializer.unVersioned();
        assertThat(unVersioned.isStrict(), is(false));
        assertThat(unVersioned.isVersioned(), is(false));
        assertThat(unVersioned.unVersioned(), is(sameInstance(unVersioned)));

        ThriftBinarySerializer both = strict.unVersioned();
        assertThat(both.isStrict(), is(true));
        assertThat(both.isVersioned(), is(false));
        assertThat(both.strict(), is(sameInstance(both)));
        assertThat(both.unVersioned(), is(sameInstance(both)));
    }

    public static Stream<Arguments> testSerializationData() {
        return Stream.of(arguments(PvdCompact.newBuilder()
                                             .setB(true)
                                             .setBt((byte) 42)
                                             .setS((short) 4200)
                                             .setI(42000000)
                                             .setL(42000000000000L)
                                             .build(),
                                   new ThriftCompact()
                                           .setB(true)
                                           .setBt((byte) 42)
                                           .setS((short) 4200)
                                           .setI(42000000)
                                           .setL(42000000000000L)),
                         arguments(PvdMessage.newBuilder()
                                             .setD(42.424242)
                                             .setStr("foo")
                                             .setBin(fromHexString("deadbeef"))
                                             .setE(PvdEnum.FIRST)
                                             .setStrings(listOf("foo", "bar"))
                                             .setDoubles(setOf(42.42, 3.141592))
                                             .setIsMap(mapOf(42, "foo"))
                                             .build(),
                                   new ThriftMessage()
                                           .setD(42.424242)
                                           .setStr("foo")
                                           .setBin(fromHexString("deadbeef").get())
                                           .setE(ThriftEnum.FIRST)
                                           .setStrings(listOf("foo", "bar"))
                                           .setDoubles(setOf(42.42, 3.141592))
                                           .setIsMap(mapOf(42, "foo"))),
                         arguments(PvdUnion.withCompact(PvdCompact.newBuilder().setI(42)),
                                   ThriftUnion.compact(new ThriftCompact().setI(42))));
    }

    @ParameterizedTest
    @MethodSource("testSerializationData")
    public void testSerialization(PMessage providence, TBase<?,?> thrift)
            throws IOException, TException,
                   NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        ThriftBinarySerializer serializer = new ThriftBinarySerializer();
        ByteArrayOutputStream pvdOut = new ByteArrayOutputStream();
        serializer.writeTo(providence, pvdOut);

        ByteArrayInputStream thriftIn = new ByteArrayInputStream(pvdOut.toByteArray());
        ByteArrayOutputStream thriftOut = new ByteArrayOutputStream();
        TBase<?,?> parsedT = thrift.getClass().getConstructor().newInstance();
        TProtocol binary = new TBinaryProtocol(new TIOStreamTransport(thriftIn, thriftOut));
        parsedT.read(binary);

        assertThat(parsedT, is(thrift));

        thrift.write(binary);
        ByteArrayInputStream pvdIn = new ByteArrayInputStream(thriftOut.toByteArray());
        PMessage parsedP = serializer.readFrom(providence.$descriptor(), pvdIn);

        assertThat(parsedP, is(providence));
    }
}
