package io.pvdnc.sio.thrift.rpc;

import io.pvdnc.sio.thrift.test.PvdCompact;
import io.pvdnc.sio.thrift.test.PvdException;
import io.pvdnc.sio.thrift.test.PvdService;
import io.pvdnc.sio.thrift.test.PvdUnion;
import io.pvdnc.sio.thrift.test.ThriftCompact;
import io.pvdnc.sio.thrift.test.ThriftService;
import io.pvdnc.sio.thrift.test.ThriftUnion;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.TServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyByte;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyShort;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class ThriftHttpClientHandlerTest {
    private ThriftService.Iface impl;
    private Server                 server;
    private int port;

    @BeforeEach
    public void setUp() throws Exception {
        impl = mock(ThriftService.Iface.class);

        ServletContextHandler handler = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
        handler.addServlet(new ServletHolder(new TServlet(
                                   new ThriftService.Processor<>(impl),
                                   new TBinaryProtocol.Factory())),
                           "/thrift");

        server = new Server(0);
        server.setHandler(handler);
        server.start();
        port = ((ServerConnector) server.getConnectors()[0]).getLocalPort();
    }

    @AfterEach
    public void tearDown() throws Exception {
        if (server != null) {
            server.stop();
            server.destroy();
        }
    }

    @Test
    public void testHandler_OneWay() throws IOException, TException {
        ThriftHttpClientHandler handler = new ThriftHttpClientHandler("http://localhost:" + port + "/thrift");
        PvdService.IFace client = PvdService.newThriftClient(handler);
        client.ping();
        verify(impl).ping();
        verifyNoMoreInteractions(impl);
    }

    @Test
    public void testHandler_Void() throws IOException, TException {
        ThriftHttpClientHandler handler = new ThriftHttpClientHandler("http://localhost:" + port + "/thrift");
        PvdService.IFace client = PvdService.newThriftClient(handler);
        try {
            client.voidMethod(PvdUnion.withCompact(PvdCompact.newBuilder().setI(42)));
            // This is actually testing that the providence client fails when the
            // apache thrift servlet does NOT send a void success response. Which
            // it does not, and probably never will.
            fail("no exception");
        } catch (ApplicationException.$Impl e) {
            assertThat(e.getMessage(), is("Result field for test.PvdService.voidMethod() not set"));
        }
        verify(impl).voidMethod(ThriftUnion.compact(new ThriftCompact().setI(42)));
        verifyNoMoreInteractions(impl);
    }

    @Test
    public void testHandler_simple() throws IOException, PvdException.$Impl, TException {
        ThriftHttpClientHandler handler = new ThriftHttpClientHandler("http://localhost:" + port + "/thrift");
        PvdService.IFace client = PvdService.newThriftClient(handler);
        when(impl.fullMethod(anyBoolean(), anyByte(), anyShort(), anyInt(), anyLong()))
                .thenReturn(ThriftUnion.compact(new ThriftCompact().setI(42)));
        PvdUnion response = client.fullMethod(false, null, (short) 42, 42, null);
        assertThat(response, is(PvdUnion.withCompact(PvdCompact.newBuilder().setI(42))));
        verify(impl).fullMethod(false, (byte) 0, (short) 42, 42, 0L);
        verifyNoMoreInteractions(impl);
    }
}
