// generated by providence-idl-generator:{version} @ {date}
package io.pvdnc.sio.thrift.test;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.PMessageBuilder;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.types.PType;
import io.pvdnc.core.utils.BuilderUtil;
import io.pvdnc.core.utils.ValueUtil;
import net.morimekta.collect.UnmodifiableList;
import net.morimekta.collect.UnmodifiableMap;
import net.morimekta.collect.UnmodifiableSet;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

@SuppressWarnings("unused")
public interface PvdWrapper
        extends PMessage {
    // ---- Declared Fields ----

    /**
     * @return Optional msg value.
     */
    Optional<PvdCompact> optionalMsg();

    /**
     * @return The msg value or default.
     */
    PvdCompact getMsg();

    /**
     * @return If the msg value is set.
     */
    boolean hasMsg();

    /**
     * @return Optional msg_list value.
     */
    Optional<java.util.List<PvdMessage>> optionalMsgList();

    /**
     * @return The msg_list value or default.
     */
    java.util.List<PvdMessage> getMsgList();

    /**
     * @return If the msg_list value is set.
     */
    boolean hasMsgList();

    /**
     * @return Optional msg_set value.
     */
    Optional<java.util.Set<PvdCompact>> optionalMsgSet();

    /**
     * @return The msg_set value or default.
     */
    java.util.Set<PvdCompact> getMsgSet();

    /**
     * @return If the msg_set value is set.
     */
    boolean hasMsgSet();

    /**
     * @return Optional msg_map value.
     */
    Optional<java.util.Map<String,PvdMessage>> optionalMsgMap();

    /**
     * @return The msg_map value or default.
     */
    java.util.Map<String,PvdMessage> getMsgMap();

    /**
     * @return If the msg_map value is set.
     */
    boolean hasMsgMap();

    /**
     * @return Optional msg_union value.
     */
    Optional<PvdUnion> optionalMsgUnion();

    /**
     * @return The msg_union value or default.
     */
    PvdUnion getMsgUnion();

    /**
     * @return If the msg_union value is set.
     */
    boolean hasMsgUnion();

    // ---- PMessage ----

    /**
     * @return The test.PvdWrapper builder.
     */
    @Override
    PvdWrapper.Builder mutate();

    /**
     * @return The test.PvdWrapper descriptor.
     */
    @Override
    PMessageDescriptor<? extends PvdWrapper> $descriptor();

    // ---- Static ----

    $Descriptor kDescriptor = new $Descriptor();

    static PvdWrapper.Builder newBuilder() {
        return kDescriptor.newBuilder();
    }

    static PvdWrapper empty() {
        return $Impl.kEmpty;
    }

    static Supplier<$Descriptor> provider() {
        return () -> kDescriptor;
    }

    /**
     * Builder interface for creating test.PvdWrapper instances.
     */
    interface Builder
            extends PMessageBuilder,
                    PvdWrapper {
        // ---- Fields ----

        /**
         * @param value The new msg value
         * @return The msg builder.
         */
        PvdWrapper.Builder setMsg(PvdCompact value);

        /**
         * Get mutable value of the field. This will initialize the field to default
         * if not set, and change any immutable type that has a mutable counter part
         * to it&#39;s mutable type.
         *
         * @return The mutable msg value.
         */
        PvdCompact.Builder mutableMsg();

        /**
         * Clears the msg value.
         *
         * @return The msg builder.
         */
        PvdWrapper.Builder clearMsg();

        /**
         * @param value The new msg_list value
         * @return The msg_list builder.
         */
        PvdWrapper.Builder setMsgList(java.util.Collection<PvdMessage> value);

        /**
         * Get mutable value of the field. This will initialize the field to default
         * if not set, and change any immutable type that has a mutable counter part
         * to it&#39;s mutable type.
         *
         * @return The mutable msg_list value.
         */
        java.util.List<PvdMessage> mutableMsgList();

        /**
         * Clears the msg_list value.
         *
         * @return The msg_list builder.
         */
        PvdWrapper.Builder clearMsgList();

        /**
         * @param value The new msg_set value
         * @return The msg_set builder.
         */
        PvdWrapper.Builder setMsgSet(java.util.Collection<PvdCompact> value);

        /**
         * Get mutable value of the field. This will initialize the field to default
         * if not set, and change any immutable type that has a mutable counter part
         * to it&#39;s mutable type.
         *
         * @return The mutable msg_set value.
         */
        java.util.Set<PvdCompact> mutableMsgSet();

        /**
         * Clears the msg_set value.
         *
         * @return The msg_set builder.
         */
        PvdWrapper.Builder clearMsgSet();

        /**
         * @param value The new msg_map value
         * @return The msg_map builder.
         */
        PvdWrapper.Builder setMsgMap(java.util.Map<String,PvdMessage> value);

        /**
         * Get mutable value of the field. This will initialize the field to default
         * if not set, and change any immutable type that has a mutable counter part
         * to it&#39;s mutable type.
         *
         * @return The mutable msg_map value.
         */
        java.util.Map<String,PvdMessage> mutableMsgMap();

        /**
         * Clears the msg_map value.
         *
         * @return The msg_map builder.
         */
        PvdWrapper.Builder clearMsgMap();

        /**
         * @param value The new msg_union value
         * @return The msg_union builder.
         */
        PvdWrapper.Builder setMsgUnion(PvdUnion value);

        /**
         * Get mutable value of the field. This will initialize the field to default
         * if not set, and change any immutable type that has a mutable counter part
         * to it&#39;s mutable type.
         *
         * @return The mutable msg_union value.
         */
        PvdUnion.Builder mutableMsgUnion();

        /**
         * Clears the msg_union value.
         *
         * @return The msg_union builder.
         */
        PvdWrapper.Builder clearMsgUnion();

        // ---- PMessage.Builder ----

        /**
         * @return The built test.PvdWrapper instance.
         */
        @Override
        PvdWrapper build();
    }

    class $Descriptor extends PMessageDescriptor<PvdWrapper> {
        public static final PvdCompact kDefaultMsg = PvdCompact.newBuilder().build();
        public static final java.util.List<PvdMessage> kDefaultMsgList = net.morimekta.collect.UnmodifiableList.listOf();
        public static final java.util.Set<PvdCompact> kDefaultMsgSet = net.morimekta.collect.UnmodifiableSet.setOf();
        public static final java.util.Map<String,PvdMessage> kDefaultMsgMap = net.morimekta.collect.UnmodifiableMap.mapOf();
        public static final PvdUnion kDefaultMsgUnion = PvdUnion.newBuilder().build();

        public static final PField kFieldMsg = new PField(
                1,
                PvdCompact.provider(),
                "msg",
                () -> kDefaultMsg,
                PPropertyMap.of(
                        "field.requirement", "OPTIONAL"),
                null,
                () -> PvdWrapper.kDescriptor);
        public static final PField kFieldMsgList = new PField(
                2,
                PListDescriptor.provider(PvdMessage.provider()),
                "msg_list",
                () -> kDefaultMsgList,
                PPropertyMap.of(
                        "field.requirement", "OPTIONAL"),
                null,
                () -> PvdWrapper.kDescriptor);
        public static final PField kFieldMsgSet = new PField(
                3,
                PSetDescriptor.provider(PvdCompact.provider()),
                "msg_set",
                () -> kDefaultMsgSet,
                PPropertyMap.of(
                        "field.requirement", "OPTIONAL"),
                null,
                () -> PvdWrapper.kDescriptor);
        public static final PField kFieldMsgMap = new PField(
                4,
                PMapDescriptor.provider(PType.STRING.provider(),PvdMessage.provider()),
                "msg_map",
                () -> kDefaultMsgMap,
                PPropertyMap.of(
                        "field.requirement", "OPTIONAL"),
                null,
                () -> PvdWrapper.kDescriptor);
        public static final PField kFieldMsgUnion = new PField(
                5,
                PvdUnion.provider(),
                "msg_union",
                () -> kDefaultMsgUnion,
                PPropertyMap.of(
                        "field.requirement", "OPTIONAL"),
                null,
                () -> PvdWrapper.kDescriptor);

        private final List<PField> mAllFields;
        private final List<PField> mDeclaredFields;

        private $Descriptor() {
            super("test", "PvdWrapper", PPropertyMap.of(
                    "message.variant", "STRUCT"));
            this.mDeclaredFields = UnmodifiableList.listOf(
                    kFieldMsg,
                    kFieldMsgList,
                    kFieldMsgSet,
                    kFieldMsgMap,
                    kFieldMsgUnion);
            this.mAllFields = this.mDeclaredFields;
        }

        @Override
        public List<PField> declaredFields() {
            return mDeclaredFields;
        }

        @Override
        public List<PField> allFields() {
            return mAllFields;
        }

        @Override
        public PField findFieldById(int id) {
            switch (id) {
                case 1: return kFieldMsg;
                case 2: return kFieldMsgList;
                case 3: return kFieldMsgSet;
                case 4: return kFieldMsgMap;
                case 5: return kFieldMsgUnion;
            }
            return null;
        }

        @Override
        public PField findFieldByName(String name) {
            if (name == null) return null;
            switch (name) {
                case "msg": return kFieldMsg;
                case "msg_list": return kFieldMsgList;
                case "msg_set": return kFieldMsgSet;
                case "msg_map": return kFieldMsgMap;
                case "msg_union": return kFieldMsgUnion;
            }
            return null;
        }

        @Override
        public PvdWrapper.Builder newBuilder() {
            return new PvdWrapper.$BuilderImpl();
        }
    }

    class $Impl
            implements PvdWrapper {
        private final PvdCompact vMsg;
        private final java.util.List<PvdMessage> vMsgList;
        private final java.util.Set<PvdCompact> vMsgSet;
        private final java.util.Map<String,PvdMessage> vMsgMap;
        private final PvdUnion vMsgUnion;

        private transient Integer tHashCode;

        protected $Impl(PvdCompact pMsg,
                        java.util.List<PvdMessage> pMsgList,
                        java.util.Set<PvdCompact> pMsgSet,
                        java.util.Map<String,PvdMessage> pMsgMap,
                        PvdUnion pMsgUnion) {
            vMsg = BuilderUtil.toImmutableIfNotNull(pMsg);;
            vMsgList = BuilderUtil.toImmutableAll(pMsgList);
            vMsgSet = pMsgSet == null ? null : UnmodifiableSet.asSet(pMsgSet);
            vMsgMap = BuilderUtil.toImmutableValues(pMsgMap);
            vMsgUnion = BuilderUtil.toImmutableIfNotNull(pMsgUnion);;
        }

        @Override
        public PvdCompact getMsg() {
            return vMsg != null ? vMsg : PvdWrapper.$Descriptor.kDefaultMsg;
        }

        @Override
        public Optional<PvdCompact> optionalMsg() {
            return Optional.ofNullable(vMsg);
        }

        @Override
        public boolean hasMsg() {
            return vMsg != null;
        }

        @Override
        public java.util.List<PvdMessage> getMsgList() {
            return vMsgList != null ? vMsgList : PvdWrapper.$Descriptor.kDefaultMsgList;
        }

        @Override
        public Optional<java.util.List<PvdMessage>> optionalMsgList() {
            return Optional.ofNullable(vMsgList);
        }

        @Override
        public boolean hasMsgList() {
            return vMsgList != null;
        }

        @Override
        public java.util.Set<PvdCompact> getMsgSet() {
            return vMsgSet != null ? vMsgSet : PvdWrapper.$Descriptor.kDefaultMsgSet;
        }

        @Override
        public Optional<java.util.Set<PvdCompact>> optionalMsgSet() {
            return Optional.ofNullable(vMsgSet);
        }

        @Override
        public boolean hasMsgSet() {
            return vMsgSet != null;
        }

        @Override
        public java.util.Map<String,PvdMessage> getMsgMap() {
            return vMsgMap != null ? vMsgMap : PvdWrapper.$Descriptor.kDefaultMsgMap;
        }

        @Override
        public Optional<java.util.Map<String,PvdMessage>> optionalMsgMap() {
            return Optional.ofNullable(vMsgMap);
        }

        @Override
        public boolean hasMsgMap() {
            return vMsgMap != null;
        }

        @Override
        public PvdUnion getMsgUnion() {
            return vMsgUnion != null ? vMsgUnion : PvdWrapper.$Descriptor.kDefaultMsgUnion;
        }

        @Override
        public Optional<PvdUnion> optionalMsgUnion() {
            return Optional.ofNullable(vMsgUnion);
        }

        @Override
        public boolean hasMsgUnion() {
            return vMsgUnion != null;
        }

        @Override
        public boolean has(int id) {
            switch (id) {
                case 1: return vMsg != null;
                case 2: return vMsgList != null;
                case 3: return vMsgSet != null;
                case 4: return vMsgMap != null;
                case 5: return vMsgUnion != null;
                default: return false;
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T get(int id) {
            switch (id) {
                case 1: return (T) getMsg();
                case 2: return (T) getMsgList();
                case 3: return (T) getMsgSet();
                case 4: return (T) getMsgMap();
                case 5: return (T) getMsgUnion();
                default: throw new IllegalArgumentException("No field " + id + " on test.PvdWrapper");
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> Optional<T> optional(int id) {
            switch (id) {
                case 1: return Optional.ofNullable((T) vMsg);
                case 2: return Optional.ofNullable((T) vMsgList);
                case 3: return Optional.ofNullable((T) vMsgSet);
                case 4: return Optional.ofNullable((T) vMsgMap);
                case 5: return Optional.ofNullable((T) vMsgUnion);
                default: return Optional.empty();
            }
        }

        @Override
        public PvdWrapper.$BuilderImpl mutate() {
            return new PvdWrapper.$BuilderImpl(
                    vMsg,
                    vMsgList,
                    vMsgSet,
                    vMsgMap,
                    vMsgUnion);
        }

        @Override
        public PMessageDescriptor<PvdWrapper> $descriptor() {
            return PvdWrapper.kDescriptor;
        }

        // ---- Object ----

        @Override
        public String toString() {
            return "test.PvdWrapper" + ValueUtil.asString(this);
        }

        @Override
        public int hashCode() {
            if (tHashCode == null) {
                tHashCode = Objects.hash(
                        vMsg,
                        vMsgList,
                        vMsgSet,
                        vMsgMap,
                        vMsgUnion);
            }
            return tHashCode;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (o == null || !getClass().equals(o.getClass())) {
                return false;
            }
            PvdWrapper.$Impl other = (PvdWrapper.$Impl) o;
            return Objects.equals(vMsg, other.vMsg) &&
                   Objects.equals(vMsgList, other.vMsgList) &&
                   Objects.equals(vMsgSet, other.vMsgSet) &&
                   Objects.equals(vMsgMap, other.vMsgMap) &&
                   Objects.equals(vMsgUnion, other.vMsgUnion);
        }

        public static final PvdWrapper.$Impl kEmpty = new PvdWrapper.$Impl(null, null, null, null, null);
    }

    class $BuilderImpl
            implements PvdWrapper.Builder {
        private PvdCompact vMsg;
        private java.util.List<PvdMessage> vMsgList;
        private java.util.Set<PvdCompact> vMsgSet;
        private java.util.Map<String,PvdMessage> vMsgMap;
        private PvdUnion vMsgUnion;

        private boolean mMsg;
        private boolean mMsgList;
        private boolean mMsgSet;
        private boolean mMsgMap;
        private boolean mMsgUnion;

        protected $BuilderImpl(PvdCompact pMsg,
                               java.util.List<PvdMessage> pMsgList,
                               java.util.Set<PvdCompact> pMsgSet,
                               java.util.Map<String,PvdMessage> pMsgMap,
                               PvdUnion pMsgUnion) {
            vMsg = pMsg;
            vMsgList = pMsgList;
            vMsgSet = pMsgSet;
            vMsgMap = pMsgMap;
            vMsgUnion = pMsgUnion;
        }

        protected $BuilderImpl() {
        }

        @Override
        public PvdCompact getMsg() {
            return vMsg != null ? vMsg : PvdWrapper.$Descriptor.kDefaultMsg;
        }

        @Override
        public Optional<PvdCompact> optionalMsg() {
            return Optional.ofNullable(vMsg);
        }

        @Override
        public boolean hasMsg() {
            return vMsg != null;
        }

        @Override
        public java.util.List<PvdMessage> getMsgList() {
            return vMsgList != null ? vMsgList : PvdWrapper.$Descriptor.kDefaultMsgList;
        }

        @Override
        public Optional<java.util.List<PvdMessage>> optionalMsgList() {
            return Optional.ofNullable(vMsgList);
        }

        @Override
        public boolean hasMsgList() {
            return vMsgList != null;
        }

        @Override
        public java.util.Set<PvdCompact> getMsgSet() {
            return vMsgSet != null ? vMsgSet : PvdWrapper.$Descriptor.kDefaultMsgSet;
        }

        @Override
        public Optional<java.util.Set<PvdCompact>> optionalMsgSet() {
            return Optional.ofNullable(vMsgSet);
        }

        @Override
        public boolean hasMsgSet() {
            return vMsgSet != null;
        }

        @Override
        public java.util.Map<String,PvdMessage> getMsgMap() {
            return vMsgMap != null ? vMsgMap : PvdWrapper.$Descriptor.kDefaultMsgMap;
        }

        @Override
        public Optional<java.util.Map<String,PvdMessage>> optionalMsgMap() {
            return Optional.ofNullable(vMsgMap);
        }

        @Override
        public boolean hasMsgMap() {
            return vMsgMap != null;
        }

        @Override
        public PvdUnion getMsgUnion() {
            return vMsgUnion != null ? vMsgUnion : PvdWrapper.$Descriptor.kDefaultMsgUnion;
        }

        @Override
        public Optional<PvdUnion> optionalMsgUnion() {
            return Optional.ofNullable(vMsgUnion);
        }

        @Override
        public boolean hasMsgUnion() {
            return vMsgUnion != null;
        }

        @Override
        public PvdWrapper.$BuilderImpl setMsg(PvdCompact value) {
            if (value == null) {
                return clearMsg();
            }
            mMsg = true;
            vMsg = value;
            return this;
        }

        @Override
        public PvdCompact.Builder mutableMsg() {
            mMsg = true;
            if (vMsg == null) {
                vMsg = PvdWrapper.$Descriptor.kDefaultMsg;
            }
            if (!(vMsg instanceof PvdCompact.Builder)) {
                vMsg = vMsg.mutate();
            }
            return (PvdCompact.Builder) vMsg;
        }

        @Override
        public PvdWrapper.$BuilderImpl clearMsg() {
            mMsg |= vMsg != null;
            vMsg = null;
            return this;
        }

        @Override
        public PvdWrapper.$BuilderImpl setMsgList(java.util.Collection<PvdMessage> value) {
            if (value == null) {
                return clearMsgList();
            }
            mMsgList = true;
            vMsgList = UnmodifiableList.asList(value);
            return this;
        }

        @Override
        public java.util.List<PvdMessage> mutableMsgList() {
            mMsgList = true;
            if (vMsgList == null) {
                vMsgList = PvdWrapper.$Descriptor.kDefaultMsgList;
            }
            if (!(vMsgList instanceof java.util.ArrayList)) {
                vMsgList = new java.util.ArrayList<>(vMsgList);
            }
            return vMsgList;
        }

        @Override
        public PvdWrapper.$BuilderImpl clearMsgList() {
            mMsgList |= vMsgList != null;
            vMsgList = null;
            return this;
        }

        @Override
        public PvdWrapper.$BuilderImpl setMsgSet(java.util.Collection<PvdCompact> value) {
            if (value == null) {
                return clearMsgSet();
            }
            mMsgSet = true;
            vMsgSet = UnmodifiableSet.asSet(value);
            return this;
        }

        @Override
        public java.util.Set<PvdCompact> mutableMsgSet() {
            mMsgSet = true;
            if (vMsgSet == null) {
                vMsgSet = PvdWrapper.$Descriptor.kDefaultMsgSet;
            }
            if (!(vMsgSet instanceof java.util.HashSet)) {
                vMsgSet = new java.util.LinkedHashSet<>(vMsgSet);
            }
            return vMsgSet;
        }

        @Override
        public PvdWrapper.$BuilderImpl clearMsgSet() {
            mMsgSet |= vMsgSet != null;
            vMsgSet = null;
            return this;
        }

        @Override
        public PvdWrapper.$BuilderImpl setMsgMap(java.util.Map<String,PvdMessage> value) {
            if (value == null) {
                return clearMsgMap();
            }
            mMsgMap = true;
            vMsgMap = UnmodifiableMap.asMap(value);
            return this;
        }

        @Override
        public java.util.Map<String,PvdMessage> mutableMsgMap() {
            mMsgMap = true;
            if (vMsgMap == null) {
                vMsgMap = PvdWrapper.$Descriptor.kDefaultMsgMap;
            }
            if (!(vMsgMap instanceof java.util.HashMap)) {
                vMsgMap = new java.util.LinkedHashMap<>(vMsgMap);
            }
            return vMsgMap;
        }

        @Override
        public PvdWrapper.$BuilderImpl clearMsgMap() {
            mMsgMap |= vMsgMap != null;
            vMsgMap = null;
            return this;
        }

        @Override
        public PvdWrapper.$BuilderImpl setMsgUnion(PvdUnion value) {
            if (value == null) {
                return clearMsgUnion();
            }
            mMsgUnion = true;
            vMsgUnion = value;
            return this;
        }

        @Override
        public PvdUnion.Builder mutableMsgUnion() {
            mMsgUnion = true;
            if (vMsgUnion == null) {
                vMsgUnion = PvdWrapper.$Descriptor.kDefaultMsgUnion;
            }
            if (!(vMsgUnion instanceof PvdUnion.Builder)) {
                vMsgUnion = vMsgUnion.mutate();
            }
            return (PvdUnion.Builder) vMsgUnion;
        }

        @Override
        public PvdWrapper.$BuilderImpl clearMsgUnion() {
            mMsgUnion |= vMsgUnion != null;
            vMsgUnion = null;
            return this;
        }

        @Override
        public boolean has(int id) {
            switch (id) {
                case 1: return vMsg != null;
                case 2: return vMsgList != null;
                case 3: return vMsgSet != null;
                case 4: return vMsgMap != null;
                case 5: return vMsgUnion != null;
                default: return false;
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T get(int id) {
            switch (id) {
                case 1: return (T) getMsg();
                case 2: return (T) getMsgList();
                case 3: return (T) getMsgSet();
                case 4: return (T) getMsgMap();
                case 5: return (T) getMsgUnion();
                default: throw new IllegalArgumentException("No field " + id + " on test.PvdWrapper");
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> Optional<T> optional(int id) {
            switch (id) {
                case 1: return Optional.ofNullable((T) vMsg);
                case 2: return Optional.ofNullable((T) vMsgList);
                case 3: return Optional.ofNullable((T) vMsgSet);
                case 4: return Optional.ofNullable((T) vMsgMap);
                case 5: return Optional.ofNullable((T) vMsgUnion);
                default: return Optional.empty();
            }
        }

        @Override
        public PvdWrapper.$BuilderImpl mutate() {
            return new PvdWrapper.$BuilderImpl(
                    vMsg,
                    vMsgList,
                    vMsgSet,
                    vMsgMap,
                    vMsgUnion);
        }

        @Override
        public PMessageDescriptor<PvdWrapper> $descriptor() {
            return PvdWrapper.kDescriptor;
        }

        @Override
        @SuppressWarnings("unchecked")
        public void set(int id, Object value) {
            switch (id) {
                case 1: setMsg((PvdCompact) value); break;
                case 2: setMsgList((java.util.Collection<PvdMessage>) value); break;
                case 3: setMsgSet((java.util.Collection<PvdCompact>) value); break;
                case 4: setMsgMap((java.util.Map<String,PvdMessage>) value); break;
                case 5: setMsgUnion((PvdUnion) value); break;
                default: throw new IllegalArgumentException("Unknown field id " + id + " on test.PvdWrapper");
            }
        }

        @Override
        public void clear(int id) {
            switch (id) {
                case 1: clearMsg(); break;
                case 2: clearMsgList(); break;
                case 3: clearMsgSet(); break;
                case 4: clearMsgMap(); break;
                case 5: clearMsgUnion(); break;
                default: break;
            }
        }

        @Override
        public boolean modified(int id) {
            switch (id) {
                case 1: return mMsg;
                case 2: return mMsgList;
                case 3: return mMsgSet;
                case 4: return mMsgMap;
                case 5: return mMsgUnion;
                default: return false;
            }
        }

        @Override
        public PvdWrapper.$Impl build() {
            return new PvdWrapper.$Impl(
                    vMsg,
                    vMsgList,
                    vMsgSet,
                    vMsgMap,
                    vMsgUnion);
        }

        // ---- Object ----

        @Override
        public String toString() {
            return "test.PvdWrapper" + ValueUtil.asString(this);
        }

        @Override
        public int hashCode() {
            return Objects.hash(
                    vMsg,
                    vMsgList,
                    vMsgSet,
                    vMsgMap,
                    vMsgUnion);
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (o == null || !getClass().equals(o.getClass())) {
                return false;
            }
            PvdWrapper.$BuilderImpl other = (PvdWrapper.$BuilderImpl) o;
            return Objects.equals(vMsg, other.vMsg) &&
                   Objects.equals(vMsgList, other.vMsgList) &&
                   Objects.equals(vMsgSet, other.vMsgSet) &&
                   Objects.equals(vMsgMap, other.vMsgMap) &&
                   Objects.equals(vMsgUnion, other.vMsgUnion);
        }
    }
}
