namespace java io.pvdnc.sio.thrift.test

enum PvdEnum {
    FIRST = 1,
    SECOND = 2,
    THIRD = 3,
}

struct PvdMessage {
    1: optional bool b;
    2: optional i8 bt;
    3: optional i16 s;
    4: optional i32 i;
    5: optional i64 l;
    7: optional double d;
    8: optional string str;
    9: optional binary bin;
    10: optional PvdEnum e;
    11: optional list<string> strings;
    12: optional set<double> doubles;
    13: optional map<i32,string> is_map;
}

struct PvdCompact {
    1: optional bool b;
    2: optional i8 bt;
    3: optional i16 s;
    4: optional i32 i;
    5: optional i64 l;
}

union PvdUnion {
    1: optional PvdMessage message;
    2: optional PvdCompact compact;
    3: optional string text;
}

struct PvdWrapper {
    1: optional PvdCompact msg;
    2: optional list<PvdMessage> msg_list;
    3: optional set<PvdCompact> msg_set;
    4: optional map<string, PvdMessage> msg_map;
    5: optional PvdUnion msg_union;
}

exception PvdException {
    1: optional string text;
    2: optional i32 id;
}

service PvdService {
    oneway void ping();

    void voidMethod(
        1: optional PvdUnion param;
    );

    // params matching PvdCompact
    PvdUnion fullMethod(
        1: optional bool b;
        2: optional i8 bt;
        3: optional i16 s;
        4: optional i32 i;
        5: optional i64 l;
    ) throws (
        1: optional PvdException ex;
    );
}