namespace java io.pvdnc.sio.thrift.test

enum ThriftEnum {
    FIRST = 1,
    SECOND = 2,
    THIRD = 3,
}

struct ThriftMessage {
    1: optional bool b;
    2: optional i8 bt;
    3: optional i16 s;
    4: optional i32 i;
    5: optional i64 l;
    7: optional double d;
    8: optional string str;
    9: optional binary bin;
    10: optional ThriftEnum e;
    11: optional list<string> strings;
    12: optional set<double> doubles;
    13: optional map<i32,string> is_map;
}

struct ThriftCompact {
    1: optional bool b;
    2: optional i8 bt;
    3: optional i16 s;
    4: optional i32 i;
    5: optional i64 l;
}

union ThriftUnion {
    1: optional ThriftMessage message;
    2: optional ThriftCompact compact;
    3: optional string text;
}

struct ThriftWrapper {
    1: optional ThriftCompact msg;
    2: optional list<ThriftMessage> msg_list;
    3: optional set<ThriftCompact> msg_set;
    4: optional map<string, ThriftMessage> msg_map;
    5: optional ThriftUnion msg_union;
}

exception ThriftException {
    1: optional string text;
    2: optional i32 id;
}

service ThriftService {
    oneway void ping();

    void voidMethod(
        1: optional ThriftUnion param;
    );

    // params matching ThriftCompact
    ThriftUnion fullMethod(
        1: optional bool b;
        2: optional i8 bt;
        3: optional i16 s;
        4: optional i32 i;
        5: optional i64 l;
    ) throws (
        1: optional ThriftException ex;
    );
}