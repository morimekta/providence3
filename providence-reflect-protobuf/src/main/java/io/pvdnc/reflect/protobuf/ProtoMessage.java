/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.reflect.protobuf;

import com.google.protobuf.Message;
import io.pvdnc.core.PMessage;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class ProtoMessage extends ProtoMessageBase implements PMessage {
    private final Map<ProtoField, Object> valueCache;
    private final Map<ProtoField, Optional<Object>> optionalCache;

    public ProtoMessage(Message message, ProtoMessageDescriptor descriptor) {
        super(message, descriptor);
        this.valueCache = new ConcurrentHashMap<>();
        this.optionalCache = new ConcurrentHashMap<>();
    }

    public Message getProtoMessage() {
        return (Message) super.getProtoMessage();
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T get(int field) {
        ProtoField fieldDesc = getProtoDescriptor().findFieldById(field);
        if (fieldDesc != null) {
            return (T) valueCache.computeIfAbsent(fieldDesc, this::getInternal);
        }
        return null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> Optional<T> optional(int field) {
        ProtoField fieldDesc = getProtoDescriptor().findFieldById(field);
        if (fieldDesc != null) {
            return (Optional<T>) optionalCache.computeIfAbsent(fieldDesc, this::optionalInternal);
        }
        return Optional.empty();
    }

    @Override
    public ProtoMessageBuilder mutate() {
        return new ProtoMessageBuilder(getProtoMessage().toBuilder(), getProtoDescriptor());
    }
}
