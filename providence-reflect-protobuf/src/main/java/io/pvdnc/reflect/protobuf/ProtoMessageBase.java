/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.reflect.protobuf;

import com.google.protobuf.MessageOrBuilder;
import io.pvdnc.core.PMessage;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.utils.ValueUtil;

import java.util.Objects;
import java.util.Optional;

import static io.pvdnc.reflect.protobuf.util.ProtoValueUtil.getProvidenceValue;

public abstract class ProtoMessageBase implements PMessage {
    private final MessageOrBuilder       messageOrBuilder;
    private final ProtoMessageDescriptor descriptor;

    public ProtoMessageBase(MessageOrBuilder messageOrBuilder, ProtoMessageDescriptor descriptor) {
        this.messageOrBuilder = messageOrBuilder;
        this.descriptor = descriptor;
    }

    public MessageOrBuilder getProtoMessage() {
        return messageOrBuilder;
    }

    public ProtoMessageDescriptor getProtoDescriptor() {
        return descriptor;
    }

    @Override
    public boolean has(int field) {
        return hasInternal(descriptor.findFieldById(field));
    }

    @Override
    public PMessageDescriptor<?> $descriptor() {
        return descriptor;
    }

    // --- Object

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProtoMessageBase that = (ProtoMessageBase) o;
        return messageOrBuilder.equals(that.messageOrBuilder) && descriptor.equals(that.descriptor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(messageOrBuilder, descriptor);
    }

    @Override
    public String toString() {
        return $descriptor().getTypeName() + ValueUtil.asString(this);
    }

    // --- Private ---

    protected boolean hasInternal(ProtoField field) {
        if (field == null) return false;
        if (field.getProtoDescriptor().isRepeated()) {
            return messageOrBuilder.getRepeatedFieldCount(field.getProtoDescriptor()) > 0;
        } else {
            return messageOrBuilder.hasField(field.getProtoDescriptor());
        }
    }

    @SuppressWarnings("unchecked")
    protected <T> T getInternal(ProtoField field) {
        if (hasInternal(field)) {
            return (T) getProvidenceValue(
                    field.getResolvedDescriptor(),
                    messageOrBuilder.getField(field.getProtoDescriptor()));
        }
        return defaultValueInternal(field);
    }

    @SuppressWarnings("unchecked")
    protected <T> Optional<T> optionalInternal(ProtoField field) {
        if (hasInternal(field)) {
            return Optional.ofNullable((T) getProvidenceValue(
                    field.getResolvedDescriptor(),
                    messageOrBuilder.getField(field.getProtoDescriptor())));
        }
        return Optional.empty();
    }

    @SuppressWarnings("unchecked")
    protected <T> T defaultValueInternal(ProtoField field) {
        if (field == null) return null;
        PDescriptor descriptor = field.getResolvedDescriptor();
        return (T) field.getDefaultValue()
                .map(protoDefault -> getProvidenceValue(descriptor, protoDefault))
                .orElseGet(() -> descriptor
                        .getDefaultValue()
                        .orElse(null));
    }
}
