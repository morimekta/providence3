/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.reflect.protobuf.collect;

import com.google.protobuf.Message;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.reflect.protobuf.ProtoField;
import net.morimekta.collect.UnmodifiableList;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

import static io.pvdnc.reflect.protobuf.util.ProtoValueUtil.getProtoValue;
import static io.pvdnc.reflect.protobuf.util.ProtoValueUtil.getProvidenceValue;

public class ProtoFieldList<T>
        extends AbstractList<T>
        implements RandomAccess {
    private final Message.Builder    builder;
    private final ProtoField         field;
    private final PListDescriptor<T> type;

    @SuppressWarnings("unchecked")
    public ProtoFieldList(Message.Builder builder, ProtoField field) {
        this.builder = builder;
        this.field = field;
        this.type = (PListDescriptor<T>) field.getResolvedDescriptor();
    }

    @Override
    public int size() {
        return builder.getRepeatedFieldCount(field.getProtoDescriptor());
    }

    @Override
    public void clear() {
        builder.clearField(field.getProtoDescriptor());
    }

    @Override
    @SuppressWarnings("unchecked")
    public T get(int i) {
        return (T) getProvidenceValue(
                type.getItemType(),
                builder.getRepeatedField(field.getProtoDescriptor(), i));
    }

    @Override
    public T set(int index, T element) {
        if (index < 0) throw new IllegalArgumentException("index < 0");
        if (index > size()) throw new IllegalArgumentException("index > size(" + size() + ")");
        Object protoValue = getProtoValue(element);
        if (index == size()) {
            builder.addRepeatedField(field.getProtoDescriptor(), protoValue);
            return null;
        }
        T old = get(index);
        builder.setRepeatedField(field.getProtoDescriptor(), index, protoValue);
        return old;
    }

    @Override
    public void add(int index, T element) {
        if (index < 0) throw new IllegalArgumentException("index < 0");
        if (index > size()) throw new IllegalArgumentException("index > size(" + size() + ")");
        Object protoValue = getProtoValue(element);
        if (index == size()) {
            builder.addRepeatedField(field.getProtoDescriptor(), protoValue);
        } else {
            @SuppressWarnings("unchecked")
            List<Object> tmp = new ArrayList<>((List<Object>) builder.getField(field.getProtoDescriptor()));
            tmp.add(index, protoValue);
            builder.setField(field.getProtoDescriptor(), tmp);
        }
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        if (index < 0) throw new IllegalArgumentException("index(" + index + ") < 0");
        if (index > size()) throw new IllegalArgumentException("index(" + index + ") > size(" + size() + ")");
        if (c.isEmpty()) return false;

        @SuppressWarnings("unchecked")
        List<Object> protoValue = (List<Object>) getProtoValue(c);
        if (index == size()) {
            for (Object t : protoValue) {
                builder.addRepeatedField(field.getProtoDescriptor(), t);
            }
        } else {
            @SuppressWarnings("unchecked")
            List<Object> tmp = new ArrayList<>((List<Object>) builder.getField(field.getProtoDescriptor()));
            tmp.addAll(index, protoValue);
            builder.setField(field.getProtoDescriptor(), tmp);
        }
        return true;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T remove(int index) {
        if (index < 0) throw new IllegalArgumentException("index < 0");
        if (index >= size()) throw new IllegalArgumentException("index >= size(" + size() + ")");
        @SuppressWarnings("unchecked")
        List<Object> tmp = new ArrayList<>((List<Object>) builder.getField(field.getProtoDescriptor()));
        Object old = tmp.remove(index);
        builder.setField(field.getProtoDescriptor(), tmp);
        return (T) getProvidenceValue(type.getItemType(), old);
    }

    @Override
    public void sort(Comparator<? super T> c) {
        List<T> newList = UnmodifiableList.asList(this).sortedBy(c);
        builder.setField(field.getProtoDescriptor(), getProtoValue(newList));
    }

    @Override
    public Iterator<T> iterator() {
        return new FieldIterator(0);
    }

    @Override
    public ListIterator<T> listIterator() {
        return new FieldIterator(0);
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        if (index < 0) throw new IllegalArgumentException("index < 0");
        if (index > size()) throw new IllegalArgumentException("index > size(" + size() + ")");
        return new FieldIterator(index);
    }

    private class FieldIterator
            implements ListIterator<T> {
        // "this" item is removed, so cannot be accessed. Next will use same index.
        private int     currentIndex;
        private int     nextIndex;

        FieldIterator(int nextIndex) {
            this.currentIndex = -1;
            this.nextIndex = nextIndex;
        }

        @Override
        public boolean hasNext() {
            return nextIndex < size();
        }

        @Override
        public T next() {
            currentIndex = nextIndex;
            ++nextIndex;
            return get(currentIndex);
        }

        @Override
        public boolean hasPrevious() {
            return nextIndex > 0;
        }

        @Override
        public T previous() {
            --nextIndex;
            currentIndex = nextIndex;
            return get(currentIndex);
        }

        @Override
        public int nextIndex() {
            return nextIndex;
        }

        @Override
        public int previousIndex() {
            return nextIndex - 1;
        }

        @Override
        public void remove() {
            if (currentIndex < 0) {
                throw new IllegalStateException("No current item");
            }
            ProtoFieldList.this.remove(currentIndex);
            currentIndex = -1;
            --nextIndex;
        }

        @Override
        public void set(T t) {
            if (currentIndex < 0) {
                throw new IllegalStateException("No current item");
            }
            ProtoFieldList.this.set(currentIndex, t);
        }

        @Override
        public void add(T t) {
            if (currentIndex < 0) {
                throw new IllegalStateException("No current item");
            }
            ProtoFieldList.this.add(currentIndex, t);
            currentIndex = -1;
            ++nextIndex;
        }
    }
}
