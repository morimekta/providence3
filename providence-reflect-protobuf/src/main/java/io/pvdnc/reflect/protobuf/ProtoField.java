/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.reflect.protobuf;

import com.google.protobuf.Descriptors;
import io.pvdnc.core.property.PDefaultProperties;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PFieldRequirement;
import io.pvdnc.sio.protobuf.utils.ProtoBufProperties;
import io.pvdnc.sio.protobuf.utils.ProtoBufValueType;

import java.util.Locale;
import java.util.Objects;
import java.util.function.Supplier;

import static io.pvdnc.reflect.protobuf.util.ProtoTypeUtil.getFieldTypeDescriptor;
import static io.pvdnc.reflect.protobuf.util.ProtoValueUtil.getProvidenceValue;
import static net.morimekta.collect.util.LazyCachedSupplier.lazyCache;

public class ProtoField extends PField {
    private final Descriptors.FieldDescriptor protoDescriptor;

    public ProtoField(Descriptors.FieldDescriptor protoDescriptor,
                      ProtoMessageDescriptor onMessage) {
        this(protoDescriptor,
                () -> onMessage,
                lazyCache(() -> getFieldTypeDescriptor(protoDescriptor)));
    }

    private ProtoField(Descriptors.FieldDescriptor protoDescriptor,
                       Supplier<ProtoMessageDescriptor> onMessageSupplier,
                       Supplier<PDescriptor> typeDescriptorSupplier) {
        super(protoDescriptor.getNumber(),
                typeDescriptorSupplier,
                protoDescriptor.getName(),
                protoDescriptor.hasDefaultValue()
                        ? lazyCache(() -> getProvidenceValue(
                                typeDescriptorSupplier.get(),
                                protoDescriptor.getDefaultValue()))
                        : null,
                makePropertyMap(protoDescriptor),
                null,
                onMessageSupplier);
        this.protoDescriptor = protoDescriptor;
    }

    public Descriptors.FieldDescriptor getProtoDescriptor() {
        return protoDescriptor;
    }

    @Override
    public ProtoMessageDescriptor getOnMessageType() {
        return (ProtoMessageDescriptor) super.getOnMessageType();
    }

    // --- Object ---

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ProtoField that = (ProtoField) o;
        return protoDescriptor.equals(that.protoDescriptor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), protoDescriptor);
    }

    // --- Static ---

    private static PPropertyMap makePropertyMap(Descriptors.FieldDescriptor protoDescriptor) {
        PPropertyMap.Builder builder = PPropertyMap.newBuilder();
        if (protoDescriptor.isRequired()) {
            builder.put(PDefaultProperties.FIELD_REQUIREMENT, PFieldRequirement.REQUIRED_OUT);
        }
        if (protoDescriptor.getOptions().getDeprecated()) {
            builder.put(PDefaultProperties.DEPRECATED, "");
        }
        if (protoDescriptor.getOptions().getExtension(Providence.sorted)) {
            builder.put(PDefaultProperties.SORTED, true);
        }
        if (protoDescriptor.getContainingOneof() != null) {
            builder.put(PDefaultProperties.ONE_OF_GROUP, protoDescriptor.getContainingOneof().getName());
        }

        if (protoDescriptor.isMapField()) {
            Descriptors.Descriptor entryDesc = protoDescriptor.getMessageType();
            Descriptors.FieldDescriptor keyDesc = entryDesc.findFieldByNumber(1);
            ProtoBufValueType protoType = ProtoBufValueType.findByName(keyDesc.getType().name().toLowerCase(Locale.US));
            if (protoType != null) {
                builder.put(ProtoBufProperties.PROTO_KEY_TYPE, protoType);
            }
            Descriptors.FieldDescriptor valueDesc = entryDesc.findFieldByNumber(2);
            protoType = ProtoBufValueType.findByName(valueDesc.getType().name().toLowerCase(Locale.US));
            if (protoType != null) {
                builder.put(ProtoBufProperties.PROTO_TYPE, protoType);
            }
        } else {
            ProtoBufValueType protoType = ProtoBufValueType.findByName(protoDescriptor.getType().name().toLowerCase(Locale.US));
            if (protoType != null) {
                builder.put(ProtoBufProperties.PROTO_TYPE, protoType);
            }
        }
        if (protoDescriptor.getOptions().getPacked()) {
            builder.put(ProtoBufProperties.PROTO_PACKED, true);
        }
        return builder.build();
    }
}
