/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.reflect.protobuf;

import com.google.protobuf.Descriptors;
import com.google.protobuf.Message;
import io.pvdnc.core.property.PDefaultProperties;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.registry.PSimpleTypeRegistry;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.reflect.protobuf.util.ProtoTypeUtil;
import net.morimekta.collect.UnmodifiableList;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

import static io.pvdnc.reflect.protobuf.util.ProtoTypeUtil.getProtoNamespace;
import static java.util.Objects.requireNonNull;
import static net.morimekta.collect.UnmodifiableMap.toMap;
import static net.morimekta.collect.util.LazyCachedSupplier.lazyCache;

public class ProtoMessageDescriptor extends PMessageDescriptor<ProtoMessage> {
    private final Descriptors.Descriptor protoDescriptor;
    private final Supplier<Supplier<Message.Builder>> builderSupplier;
    private final Supplier<List<ProtoField>> protoFields;
    private final Supplier<Map<Integer, ProtoField>> idFields;
    private final Supplier<Map<String, ProtoField>> nameFields;

    ProtoMessageDescriptor(Class<? extends Message> messageClass, Descriptors.Descriptor protoDescriptor) {
        super(protoDescriptor.getName(), getProtoNamespace(protoDescriptor), makePropertyMap(protoDescriptor));
        this.protoDescriptor = protoDescriptor;
        this.builderSupplier = lazyCache(() -> makeBuilderSupplier(messageClass));
        this.protoFields = lazyCache(this::generateProtoFields);
        this.idFields = lazyCache(() -> protoFields.get().stream().collect(toMap(ProtoField::getId)));
        this.nameFields = lazyCache(() -> protoFields.get().stream().collect(toMap(ProtoField::getName)));
    }

    // --- PMessageDescriptor ---

    @Override
    @SuppressWarnings("unchecked")
    public List<PField> declaredFields() {
        return (List<PField>) (List<?>) protoFields.get();
    }

    @Override
    public List<PField> allFields() {
        return declaredFields();
    }

    @Override
    public ProtoField findFieldById(int id) {
        return idFields.get().get(id);
    }

    @Override
    public ProtoField findFieldByName(String name) {
        return nameFields.get().get(name);
    }

    @Override
    public ProtoField fieldForId(int id) {
        return (ProtoField) super.fieldForId(id);
    }

    @Override
    public ProtoField fieldForName(String name) {
        return (ProtoField) super.fieldForName(name);
    }

    @Override
    public ProtoMessageBuilder newBuilder() {
        return new ProtoMessageBuilder(builderSupplier.get().get(), this);
    }

    // --- Object ---

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ProtoMessageDescriptor that = (ProtoMessageDescriptor) o;
        return protoDescriptor.equals(that.protoDescriptor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), protoDescriptor);
    }

    // --- Static ---

    public static ProtoMessageDescriptor pvdMessageDescriptor(Message message) {
        requireNonNull(message, "message == null");
        return messageTypeForClass.computeIfAbsent(message.getClass(), c -> {
            ProtoMessageDescriptor descriptor = new ProtoMessageDescriptor(
                    message.getClass(), message.getDescriptorForType());
            messageTypeForDescriptor.put(message.getDescriptorForType(), descriptor);
            PSimpleTypeRegistry.getInstance().register(descriptor);
            return descriptor;
        });
    }

    public static ProtoMessageDescriptor pvdMessageDescriptor(Message.Builder builder) {
        requireNonNull(builder, "builder == null");
        return pvdMessageDescriptor(builder.build());
    }

    @SuppressWarnings("unchecked")
    public static ProtoMessageDescriptor pvdMessageDescriptor(Descriptors.Descriptor protoDescriptor) {
        requireNonNull(protoDescriptor, "descriptor == null");
        return messageTypeForDescriptor.computeIfAbsent(protoDescriptor, d -> {
            Class<?> messageClass = ProtoTypeUtil.getInstanceClass(protoDescriptor);
            ProtoMessageDescriptor descriptor = new ProtoMessageDescriptor((Class<? extends Message>) messageClass, protoDescriptor);
            messageTypeForClass.put(messageClass, descriptor);
            PSimpleTypeRegistry.getInstance().register(descriptor);
            return descriptor;
        });
    }

    @SuppressWarnings("unchecked")
    public static ProtoMessageDescriptor pvdMessageDescriptor(Class<?> type) {
        requireNonNull(type, "type == null");
        if (Message.class.isAssignableFrom(type)) {
            return messageTypeForClass.computeIfAbsent(type, t -> {
                try {
                    Method getDescriptor = type.getDeclaredMethod("getDescriptor");
                    Object protoDescriptor;
                    if (!getDescriptor.canAccess(null)) {
                        // TODO: This is needed to access message types in other modules. The message type
                        //       must be in an exported module at least.
                        getDescriptor.setAccessible(true);
                        protoDescriptor = getDescriptor.invoke(null);
                        getDescriptor.setAccessible(false);
                    } else {
                        protoDescriptor = getDescriptor.invoke(null);
                    }
                    if (protoDescriptor instanceof Descriptors.Descriptor) {
                        ProtoMessageDescriptor descriptor = new ProtoMessageDescriptor((Class<? extends Message>) type, (Descriptors.Descriptor) protoDescriptor);
                        messageTypeForDescriptor.put((Descriptors.Descriptor) protoDescriptor, descriptor);
                        PSimpleTypeRegistry.getInstance().register(descriptor);
                        return descriptor;
                    }
                    throw new IllegalArgumentException(
                            "Not a message descriptor getter: " + type.getSimpleName() + ".getDescriptor()");
                } catch (NoSuchMethodException | SecurityException | IllegalAccessException | InvocationTargetException e) {
                    throw new IllegalArgumentException("Not a typed message: " + type.getSimpleName(), e);
                }
            });
        }
        throw new IllegalArgumentException("Not a typed message: " + type.getSimpleName());
    }

    // --- Private ---

    private List<ProtoField> generateProtoFields() {
        ArrayList<ProtoField> out = new ArrayList<>();
        for (Descriptors.FieldDescriptor protoField : protoDescriptor.getFields()) {
            out.add(new ProtoField(protoField, this));
        }
        return UnmodifiableList.asList(out);
    }

    private static final Map<Class<?>, ProtoMessageDescriptor> messageTypeForClass = new ConcurrentHashMap<>();
    private static final Map<Descriptors.Descriptor, ProtoMessageDescriptor> messageTypeForDescriptor = new ConcurrentHashMap<>();

    private static Supplier<Message.Builder> makeBuilderSupplier(Class<? extends Message> messageClass) {
        try {
            Method method = messageClass.getDeclaredMethod("newBuilder");
            return () -> {
                try {
                    return (Message.Builder) method.invoke(null);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    throw new IllegalArgumentException(e);
                }
            };
        } catch (NoSuchMethodException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private static PPropertyMap makePropertyMap(Descriptors.Descriptor protoDescriptor) {
        PPropertyMap.Builder builder = PPropertyMap.newBuilder();
        if (protoDescriptor.getOptions().getDeprecated()) {
            builder.put(PDefaultProperties.DEPRECATED, "");
        }
        return builder.build();
    }
}
