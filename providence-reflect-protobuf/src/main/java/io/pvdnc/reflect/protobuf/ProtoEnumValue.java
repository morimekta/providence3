/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.reflect.protobuf;

import com.google.protobuf.ProtocolMessageEnum;
import io.pvdnc.core.PEnum;
import io.pvdnc.core.property.PDefaultProperties;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PEnumValue;

import java.util.Objects;

public class ProtoEnumValue extends PEnumValue implements PEnum {
    private final ProtocolMessageEnum protoValue;

    ProtoEnumValue(ProtoEnumDescriptor<?> descriptor, ProtocolMessageEnum value) {
        super(value.getValueDescriptor().getName(),
                value.getNumber(),
                makePropertyMap(value),
                () -> descriptor);
        this.protoValue = value;
    }

    public ProtocolMessageEnum getProtoValue() {
        return protoValue;
    }

    @Override
    public PEnumValue $meta() {
        return this;
    }

    @Override
    public PEnumDescriptor $descriptor() {
        return super.descriptor();
    }

    // --- Object

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProtoEnumValue that = (ProtoEnumValue) o;
        return protoValue.equals(that.protoValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), protoValue);
    }

    private static PPropertyMap makePropertyMap(ProtocolMessageEnum value) {
        PPropertyMap.Builder builder = PPropertyMap.newBuilder();
        if (value.getValueDescriptor().getOptions().getDeprecated()) {
            builder.put(PDefaultProperties.DEPRECATED, "");
        }
        return builder.build();
    }
}
