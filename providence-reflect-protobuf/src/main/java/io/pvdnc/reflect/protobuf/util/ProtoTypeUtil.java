/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.reflect.protobuf.util;

import com.google.protobuf.Any;
import com.google.protobuf.DescriptorProtos;
import com.google.protobuf.Descriptors;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.types.PType;
import io.pvdnc.reflect.protobuf.ProtoEnumDescriptor;
import io.pvdnc.reflect.protobuf.ProtoMessageDescriptor;
import io.pvdnc.reflect.protobuf.Providence;
import net.morimekta.strings.NamingUtil;

import java.util.Objects;

import static net.morimekta.collect.util.LazyCachedSupplier.lazyCache;

public class ProtoTypeUtil {
    public static String getProtoNamespace(Descriptors.GenericDescriptor descriptor) {
        return descriptor.getFullName().contains(".")
                ? descriptor.getFullName().substring(0, descriptor.getFullName().lastIndexOf("."))
                : "";
    }

    /**
     * Get the instance class name for the generic type.
     *
     * @param descriptor The generic descriptor to get the class name for. This inclused
     *                   the full package and enclosing classes.
     * @return The full java class name.
     */
    public static String getInstanceClassName(Descriptors.GenericDescriptor descriptor) {
        Objects.requireNonNull(descriptor, "descriptor == null");
        Descriptors.FileDescriptor fileDescriptor = descriptor.getFile();
        DescriptorProtos.FileOptions fileOptions = fileDescriptor.getOptions();
        StringBuilder nameBuilder = new StringBuilder();
        if (fileOptions.hasJavaPackage()) {
            nameBuilder.append(fileOptions.getJavaPackage()).append(".");
        }
        if (!fileOptions.getJavaMultipleFiles()) {
            if (fileOptions.hasJavaOuterClassname()) {
                nameBuilder.append(fileOptions.getJavaOuterClassname()).append(".");
            } else {
                nameBuilder.append(NamingUtil.format(fileDescriptor.getName(), NamingUtil.Format.PASCAL)).append(".");
            }
        }
        if (descriptor instanceof Descriptors.Descriptor) {
            Descriptors.Descriptor md = (Descriptors.Descriptor) descriptor;
            handleContainingType(md.getContainingType(), nameBuilder);
        } else if (descriptor instanceof Descriptors.EnumDescriptor) {
            Descriptors.EnumDescriptor ed = (Descriptors.EnumDescriptor) descriptor;
            handleContainingType(ed.getContainingType(), nameBuilder);
        }
        nameBuilder.append(NamingUtil.format(descriptor.getName(), NamingUtil.Format.PASCAL));
        return nameBuilder.toString();
    }

    /**
     * Get the class of the instance for the generic proto descriptor.
     *
     * @param descriptor The protobuf descriptor.
     * @return The instance class.
     */
    public static Class<?> getInstanceClass(Descriptors.GenericDescriptor descriptor) {
        if (descriptor instanceof Descriptors.Descriptor ||
            descriptor instanceof Descriptors.EnumDescriptor) {
            String className = getInstanceClassName(descriptor);
            try {
                return ProtoTypeUtil.class.getClassLoader().loadClass(className);
            } catch (ClassNotFoundException e) {
                System.err.println(className);
                System.err.println(descriptor);
                throw new IllegalArgumentException("No generated class for " + descriptor.getFullName());
            }
        } else {
            throw new IllegalArgumentException("Not a typed descriptor");
        }
    }

    public static PDescriptor getDescriptor(Descriptors.GenericDescriptor descriptor) {
        if (descriptor instanceof Descriptors.Descriptor) {
            Descriptors.Descriptor md = (Descriptors.Descriptor) descriptor;
            if (descriptor.equals(Any.getDescriptor())) {
                return PType.ANY;
            }
            return ProtoMessageDescriptor.pvdMessageDescriptor(md);
        } else if (descriptor instanceof Descriptors.EnumDescriptor) {
            return ProtoEnumDescriptor.pvdEnumDescriptor((Descriptors.EnumDescriptor) descriptor);
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public static PDescriptor getFieldTypeDescriptor(Descriptors.FieldDescriptor field) {
        if (field.isMapField()) {
            Descriptors.Descriptor entryDesc = field.getMessageType();
            return new PMapDescriptor<>(
                    lazyCache(() -> getFieldTypeDescriptor(entryDesc.findFieldByNumber(1))),
                    lazyCache(() -> getFieldTypeDescriptor(entryDesc.findFieldByNumber(2))),
                    field.getOptions().getExtension(Providence.sorted));
        } else if (field.isRepeated()) {
            PDescriptor it = valueType(field);
            if (field.getOptions().getExtension(Providence.unique)) {
                return new PSetDescriptor<>(
                        () -> it, field.getOptions().getExtension(Providence.sorted));
            } else {
                return new PListDescriptor<>(() -> it);
            }
        } else {
            return valueType(field);
        }
    }

    // --- Private ---

    private static PDescriptor valueType(Descriptors.FieldDescriptor field) {
        switch (field.getJavaType()) {
            case BOOLEAN: return PType.BOOL;
            case INT: return PType.INT;
            case LONG: return PType.LONG;
            case FLOAT: return PType.FLOAT;
            case DOUBLE: return PType.DOUBLE;
            case STRING: return PType.STRING;
            case BYTE_STRING: return PType.BINARY;
            case ENUM: return getDescriptor(field.getEnumType());
            case MESSAGE: return getDescriptor(field.getMessageType());
            default: throw new IllegalArgumentException("unknown java type: " + field.getJavaType());
        }
    }

    private static void handleContainingType(Descriptors.Descriptor containing, StringBuilder nameBuilder) {
        if (containing == null) {
            return;
        }
        handleContainingType(containing.getContainingType(), nameBuilder);
        nameBuilder.append(NamingUtil.format(containing.getName(), NamingUtil.Format.PASCAL)).append(".");
    }
}
