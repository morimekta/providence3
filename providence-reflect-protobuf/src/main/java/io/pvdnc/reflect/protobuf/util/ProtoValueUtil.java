/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.reflect.protobuf.util;

import com.google.protobuf.ByteString;
import com.google.protobuf.Descriptors;
import com.google.protobuf.MapEntry;
import com.google.protobuf.Message;
import com.google.protobuf.ProtocolMessageEnum;
import io.pvdnc.core.PAny;
import io.pvdnc.core.impl.AnyBinary;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.types.PType;
import io.pvdnc.reflect.protobuf.ProtoEnumValue;
import io.pvdnc.reflect.protobuf.ProtoMessage;
import io.pvdnc.reflect.protobuf.ProtoMessageBuilder;
import io.pvdnc.reflect.protobuf.ProtoMessageDescriptor;
import io.pvdnc.sio.protobuf.ProtoBufSerializer;
import net.morimekta.collect.util.Binary;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import static io.pvdnc.reflect.protobuf.ProtoMessageDescriptor.pvdMessageDescriptor;
import static net.morimekta.collect.UnmodifiableList.asList;
import static net.morimekta.collect.UnmodifiableList.toList;
import static net.morimekta.collect.UnmodifiableMap.toMap;
import static net.morimekta.collect.UnmodifiableSet.toSet;
import static net.morimekta.collect.UnmodifiableSortedMap.toSortedMap;
import static net.morimekta.collect.UnmodifiableSortedSet.toSortedSet;

public class ProtoValueUtil {
    public static Object getProtoValue(Object pvdValue) {
        if (pvdValue == null) {
            return null;
        } else if (pvdValue instanceof ProtoEnumValue) {
            return ((ProtoEnumValue) pvdValue).getProtoValue();
        } else if (pvdValue instanceof PAny) {
            AnyBinary binary = ((PAny) pvdValue).pack(new ProtoBufSerializer());
            return com.google.protobuf.Any.newBuilder()
                                          .setTypeUrl(binary.getTypeName())
                                          .setValue(ByteString.copyFrom(binary.getData().getByteBuffer()))
                                          .build();
        } else if (pvdValue instanceof ProtoMessage) {
            return ((ProtoMessage) pvdValue).getProtoMessage();
        } else if (pvdValue instanceof ProtoMessageBuilder) {
            return ((ProtoMessageBuilder) pvdValue).build().getProtoMessage();
        } else if (pvdValue instanceof Collection) {
            return ((Collection<?>) pvdValue).stream().map(ProtoValueUtil::getProtoValue).collect(toList());
        } else if (pvdValue instanceof SortedMap) {
            return ((Map<?, ?>) pvdValue).entrySet()
                                         .stream()
                                         .collect(toSortedMap(
                                                 e -> getProtoValue(e.getKey()),
                                                 e -> getProtoValue(e.getValue()),
                                                 ((SortedMap<?, ?>) pvdValue).comparator()));
        } else if (pvdValue instanceof Map) {
            return ((Map<?, ?>) pvdValue).entrySet()
                                         .stream()
                                         .collect(toMap(
                                                 e -> getProtoValue(e.getKey()),
                                                 e -> getProtoValue(e.getValue())));
        } else if (pvdValue instanceof Binary) {
            return ByteString.copyFrom(((Binary) pvdValue).getByteBuffer());
        } else if (pvdValue instanceof Long || pvdValue instanceof Float || pvdValue instanceof Double) {
            return pvdValue;
        } else if (pvdValue instanceof Number) {
            return ((Number) pvdValue).intValue();
        } else {
            return pvdValue;
        }
    }

    @SuppressWarnings("unchecked")
    public static Object getProvidenceValue(PDescriptor descriptor, Object protoValue) {
        if (protoValue == null) {
            return null;
        }
        switch (descriptor.getType()) {
            case VOID:
                return Boolean.TRUE;
            case BOOL:
                return Boolean.TRUE.equals(protoValue);
            case BYTE:
                return ((Number) protoValue).byteValue();
            case SHORT:
                return ((Number) protoValue).shortValue();
            case INT:
                return ((Number) protoValue).intValue();
            case LONG:
                return ((Number) protoValue).longValue();
            case FLOAT:
                return ((Number) protoValue).floatValue();
            case DOUBLE:
                return ((Number) protoValue).doubleValue();
            case STRING:
                return protoValue.toString();
            case BINARY:
                return Binary.wrap(((ByteString) protoValue).toByteArray());
            case ENUM: {
                if (protoValue instanceof ProtocolMessageEnum) {
                    return ((PEnumDescriptor) descriptor)
                            .findById(((ProtocolMessageEnum) protoValue).getNumber());
                } else {
                    return ((PEnumDescriptor) descriptor)
                            .findById(((Descriptors.EnumValueDescriptor) protoValue).getNumber());
                }
            }
            case LIST: {
                PListDescriptor<?> ld = (PListDescriptor<?>) descriptor;
                if (ld.getItemType().getType().isOneOf(
                        PType.BOOL, PType.INT, PType.LONG, PType.FLOAT, PType.DOUBLE, PType.STRING)) {
                    return asList((List<?>) protoValue);
                }
                return ((Collection<?>) protoValue).stream()
                                                   .map(item -> getProvidenceValue(ld.getItemType(), item))
                                                   .collect(toList());
            }
            case SET: {
                PSetDescriptor<?> sd = (PSetDescriptor<?>) descriptor;
                if (sd.isSorted()) {
                    return ((Collection<?>) protoValue).stream()
                                                       .map(item -> getProvidenceValue(sd.getItemType(), item))
                                                       .collect(toSortedSet());
                } else {
                    return ((Collection<?>) protoValue).stream()
                                                       .map(item -> getProvidenceValue(sd.getItemType(), item))
                                                       .collect(toSet());
                }
            }
            case MAP: {
                PMapDescriptor<?, ?> md = (PMapDescriptor<?, ?>) descriptor;
                if (protoValue instanceof Collection) {
                    if (md.isSorted()) {
                        return ((Collection<MapEntry<?, ?>>) protoValue)
                                .stream()
                                .collect(toSortedMap(
                                        e -> getProvidenceValue(md.getKeyType(), e.getKey()),
                                        e -> getProvidenceValue(md.getValueType(), e.getValue()),
                                        Comparator.naturalOrder()));
                    } else {
                        return ((Collection<MapEntry<?, ?>>) protoValue)
                                .stream()
                                .collect(toMap(
                                        e -> getProvidenceValue(md.getKeyType(), e.getKey()),
                                        e -> getProvidenceValue(md.getValueType(), e.getValue())));
                    }
                } else if (md.isSorted()) {
                    return ((Map<?, ?>) protoValue).entrySet()
                                                   .stream()
                                                   .collect(toSortedMap(
                                                           e -> getProvidenceValue(md.getKeyType(), e.getKey()),
                                                           e -> getProvidenceValue(md.getValueType(), e.getValue()),
                                                           ((SortedMap<?, ?>) protoValue).comparator()));
                } else {
                    return ((Map<?, ?>) protoValue).entrySet()
                                                   .stream()
                                                   .collect(toMap(
                                                           e -> getProvidenceValue(md.getKeyType(), e.getKey()),
                                                           e -> getProvidenceValue(md.getValueType(), e.getValue())));
                }
            }
            case MESSAGE: {
                ProtoMessageDescriptor protoMessageDescriptor;
                if (descriptor instanceof ProtoMessageDescriptor) {
                    protoMessageDescriptor = (ProtoMessageDescriptor) descriptor;
                } else {
                    System.err.println(protoValue);
                    System.err.println(descriptor);
                    protoMessageDescriptor = pvdMessageDescriptor(((Message) protoValue).getDescriptorForType());
                }
                return new ProtoMessage((Message) protoValue, protoMessageDescriptor);
            }
            case ANY: {
                com.google.protobuf.Any protoAny = (com.google.protobuf.Any) protoValue;
                return AnyBinary.newBuilder()
                                .setMediaType(ProtoBufSerializer.MEDIA_TYPE)
                                .setTypeName(protoAny.getTypeUrl())
                                .setData(Binary.wrap(protoAny.getValue().toByteArray()))
                                .build();
            }
            default: {
                throw new IllegalArgumentException("Unhandled proto type: " + descriptor);
            }
        }
    }
}
