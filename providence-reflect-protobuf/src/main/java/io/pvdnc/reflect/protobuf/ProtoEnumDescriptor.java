/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.reflect.protobuf;

import com.google.protobuf.Descriptors;
import com.google.protobuf.ProtocolMessageEnum;
import io.pvdnc.core.PEnum;
import io.pvdnc.core.property.PDefaultProperties;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.registry.PSimpleTypeRegistry;
import io.pvdnc.core.types.PEnumDescriptor;
import net.morimekta.collect.UnmodifiableMap;
import net.morimekta.collect.util.LazyCachedSupplier;

import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static io.pvdnc.reflect.protobuf.util.ProtoTypeUtil.getInstanceClass;
import static io.pvdnc.reflect.protobuf.util.ProtoTypeUtil.getProtoNamespace;
import static net.morimekta.collect.UnmodifiableList.toList;
import static net.morimekta.collect.util.LazyCachedSupplier.lazyCache;

public class ProtoEnumDescriptor<E extends Enum<E> & ProtocolMessageEnum> extends PEnumDescriptor {
    private final Descriptors.EnumDescriptor protoDescriptor;
    private final Class<? extends Enum<?>> enumClass;
    private final LazyCachedSupplier<List<E>> protoValues;
    private final LazyCachedSupplier<List<ProtoEnumValue>> values;
    private final LazyCachedSupplier<Map<String, ProtoEnumValue>> nameMap;
    private final LazyCachedSupplier<Map<Integer, ProtoEnumValue>> idMap;

    public ProtoEnumDescriptor(Class<E> enumClass, Descriptors.EnumDescriptor protoDescriptor) {
        super(protoDescriptor.getName(), getProtoNamespace(protoDescriptor), makePropertyMap(protoDescriptor));
        this.enumClass = enumClass;
        this.protoDescriptor = protoDescriptor;
        this.protoValues = lazyCache(
                () -> List.copyOf(EnumSet.allOf(enumClass)));
        this.values = lazyCache(() -> protoValues.get()
                .stream()
                .filter(pvd -> !pvd.name().equals("UNRECOGNIZED"))
                .map(pv -> new ProtoEnumValue(ProtoEnumDescriptor.this, pv))
                .collect(toList()));
        this.nameMap = lazyCache(() -> values.get().stream().collect(UnmodifiableMap.toMap(PEnum::getName)));
        this.idMap = lazyCache(() -> values.get().stream().collect(UnmodifiableMap.toMap(PEnum::getValue)));
    }

    @Override
    public List<? extends PEnum> allValues() {
        return values.get();
    }

    @Override
    public ProtoEnumValue findById(Integer id) {
        if (id == null) return null;
        return idMap.get().get(id);
    }

    @Override
    public ProtoEnumValue findByName(String name) {
        if (name == null) return null;
        return nameMap.get().get(name);
    }

    @Override
    public ProtoEnumValue valueForId(int id) {
        return (ProtoEnumValue) super.valueForId(id);
    }

    @Override
    public ProtoEnumValue valueForName(String name) {
        return (ProtoEnumValue) super.valueForName(name);
    }

    @Override
    public Optional<ProtoEnumValue> getDefaultValue() {
        return Optional.ofNullable(findById(0));
    }

    // --- Object ---

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProtoEnumDescriptor<?> that = (ProtoEnumDescriptor<?>) o;
        return protoDescriptor.equals(that.protoDescriptor) && enumClass.equals(that.enumClass);
    }

    @Override
    public int hashCode() {
        return Objects.hash(protoDescriptor, enumClass);
    }

    // --- Static ---

    @SuppressWarnings("unchecked")
    public static <E extends Enum<E> & ProtocolMessageEnum>
    ProtoEnumDescriptor<E> pvdEnumDescriptor(E instance) {
        instance.getValueDescriptor();
        return (ProtoEnumDescriptor<E>) enumTypeFromClass.computeIfAbsent(
                instance.getDeclaringClass(), t -> {
                    ProtoEnumDescriptor<?> descriptor = new ProtoEnumDescriptor<>(
                            instance.getDeclaringClass(), instance.getDescriptorForType());
                    enumTypeFromDescriptor.put(instance.getDescriptorForType(), descriptor);
                    PSimpleTypeRegistry.getInstance().register(descriptor);
                    return descriptor;
                });
    }

    @SuppressWarnings("unchecked")
    public static <E extends Enum<E> & ProtocolMessageEnum>
    ProtoEnumDescriptor<E> pvdEnumDescriptor(Descriptors.EnumDescriptor protoDescriptor) {
        return (ProtoEnumDescriptor<E>) enumTypeFromDescriptor.computeIfAbsent(
                protoDescriptor, d -> {
                    Class<?> type = getInstanceClass(protoDescriptor);
                    ProtoEnumDescriptor<?> descriptor = new ProtoEnumDescriptor<>((Class<E>) type, protoDescriptor);
                    enumTypeFromClass.put(type, descriptor);
                    PSimpleTypeRegistry.getInstance().register(descriptor);
                    return descriptor;
                });
    }

    @SuppressWarnings("unchecked")
    public static <E extends Enum<E> & ProtocolMessageEnum>
    ProtoEnumDescriptor<E> pvdEnumDescriptor(Class<?> type) {
        if (Enum.class.isAssignableFrom(type) && ProtocolMessageEnum.class.isAssignableFrom(type)) {
            return (ProtoEnumDescriptor<E>) enumTypeFromClass.computeIfAbsent(
                    type, t -> {
                        // proto enums MUST have a value. Just get hold of first.
                        E first = EnumSet.allOf((Class<E>) type)
                                .stream()
                                .filter(pvd -> !pvd.name().equals("UNRECOGNIZED"))
                                .findFirst()
                                .orElseThrow(() -> new IllegalArgumentException(
                                        "Not a generated proto enum type: " + type.getSimpleName()));
                        ProtoEnumDescriptor<?> descriptor = new ProtoEnumDescriptor<>((Class<E>) type, first.getDescriptorForType());
                        enumTypeFromDescriptor.put(first.getDescriptorForType(), descriptor);
                        PSimpleTypeRegistry.getInstance().register(descriptor);
                        return descriptor;
                    });
        }
        throw new IllegalArgumentException("Not a typed enum class: " + type.getName());
    }

    // --- Private ---

    private static PPropertyMap makePropertyMap(Descriptors.EnumDescriptor value) {
        PPropertyMap.Builder builder = PPropertyMap.newBuilder();
        if (value.getOptions().getDeprecated()) {
            builder.put(PDefaultProperties.DEPRECATED, "");
        }
        return builder.build();
    }

    private static final Map<Class<?>, ProtoEnumDescriptor<?>> enumTypeFromClass = new ConcurrentHashMap<>();
    private static final Map<Descriptors.EnumDescriptor, ProtoEnumDescriptor<?>> enumTypeFromDescriptor = new ConcurrentHashMap<>();
}
