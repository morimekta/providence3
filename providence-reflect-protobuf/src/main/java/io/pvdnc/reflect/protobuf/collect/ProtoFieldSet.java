/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.reflect.protobuf.collect;

import com.google.protobuf.Message;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.reflect.protobuf.ProtoField;

import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static io.pvdnc.reflect.protobuf.util.ProtoValueUtil.getProtoValue;
import static io.pvdnc.reflect.protobuf.util.ProtoValueUtil.getProvidenceValue;

public class ProtoFieldSet<T> extends AbstractSet<T> {
    private final Message.Builder   builder;
    private final ProtoField        field;
    private final PSetDescriptor<T> type;

    @SuppressWarnings("unchecked")
    public ProtoFieldSet(Message.Builder builder, ProtoField field) {
        this.builder = builder;
        this.field = field;
        this.type = (PSetDescriptor<T>) field.getResolvedDescriptor();
    }

    @Override
    public int size() {
        return builder.getRepeatedFieldCount(field.getProtoDescriptor());
    }

    @Override
    public boolean add(T t) {
        if (!contains(t)) {
            builder.addRepeatedField(field.getProtoDescriptor(), getProtoValue(t));
            return true;
        }
        return false;
    }

    @Override
    public void clear() {
        builder.clearField(field.getProtoDescriptor());
    }

    @Override
    public Iterator<T> iterator() {
        return new FieldIterator();
    }

    @SuppressWarnings("unchecked")
    private T getInternal(int index) {
        return (T) getProvidenceValue(type.getItemType(), builder.getRepeatedField(field.getProtoDescriptor(), index));
    }

    private void removeInternal(int index) {
        if (isEmpty() || index >= size()) {
            throw new IndexOutOfBoundsException(index);
        }
        if (size() == 1) {
            clear();
            return;
        }

        @SuppressWarnings("unchecked")
        List<Object> tmp = new ArrayList<>((List<Object>) builder.getField(field.getProtoDescriptor()));
        tmp.remove(index);
        builder.setField(field.getProtoDescriptor(), tmp);
    }

    private class FieldIterator implements Iterator<T> {
        private int currentIndex;
        private int nextIndex;

        FieldIterator() {
            this.currentIndex = -1;
            this.nextIndex = 0;
        }

        @Override
        public boolean hasNext() {
            return nextIndex < size();
        }

        @Override
        public T next() {
            currentIndex = nextIndex;
            ++nextIndex;
            return getInternal(currentIndex);
        }

        @Override
        public void remove() {
            if (currentIndex < 0) {
                throw new IllegalStateException("No current item");
            }
            removeInternal(currentIndex);
            --nextIndex;
            currentIndex = -1;
        }
    }
}
