/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.reflect.protobuf;

import com.google.protobuf.Message;
import io.pvdnc.core.PMessageBuilder;
import io.pvdnc.reflect.protobuf.collect.ProtoFieldList;
import io.pvdnc.reflect.protobuf.collect.ProtoFieldSet;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static io.pvdnc.reflect.protobuf.util.ProtoValueUtil.getProtoValue;

public class ProtoMessageBuilder extends ProtoMessageBase implements PMessageBuilder {
    private final Set<Integer> modifiedFields;

    public ProtoMessageBuilder(Message.Builder builder, ProtoMessageDescriptor descriptor) {
        super(builder, descriptor);
        this.modifiedFields = new HashSet<>();
    }

    @Override
    public Message.Builder getProtoMessage() {
        return (Message.Builder) super.getProtoMessage();
    }

    @Override
    public <T> T get(int field) {
        return getInternal(getProtoDescriptor().findFieldById(field));
    }

    @Override
    public <T> Optional<T> optional(int field) {
        return optionalInternal(getProtoDescriptor().findFieldById(field));
    }

    @Override
    public ProtoMessageBuilder mutate() {
        // Deep snapshot, not OK with clone().
        return new ProtoMessageBuilder(getProtoMessage().build().toBuilder(), getProtoDescriptor());
    }

    @Override
    public void set(int field, Object value) {
        ProtoField fieldDesc = getProtoDescriptor().findFieldById(field);
        if (fieldDesc != null) {
            getProtoMessage().setField(fieldDesc.getProtoDescriptor(), getProtoValue(value));
            modifiedFields.add(fieldDesc.getId());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T mutable(int field) {
        ProtoField fieldDesc = getProtoDescriptor().findFieldById(field);
        if (fieldDesc != null) {
            modifiedFields.add(field);
            switch (fieldDesc.getType()) {
                case MESSAGE:
                    Message.Builder mb = getProtoMessage().getFieldBuilder(fieldDesc.getProtoDescriptor());
                    return (T) new ProtoMessageBuilder(mb, (ProtoMessageDescriptor) fieldDesc.getResolvedDescriptor());
                case LIST:
                    return (T) new ProtoFieldList<>(getProtoMessage(), fieldDesc);
                case SET:
                    // TODO: Create sorted-set wrapper?
                    return (T) new ProtoFieldSet<>(getProtoMessage(), fieldDesc);
                case MAP:
                    // TODO: Create map wrappers?
                default:
                    return getInternal(fieldDesc);
            }
        }
        return null;
    }

    @Override
    public void clear(int field) {
        ProtoField fieldDesc = getProtoDescriptor().findFieldById(field);
        if (fieldDesc != null) {
            getProtoMessage().clearField(fieldDesc.getProtoDescriptor());
            modifiedFields.add(fieldDesc.getId());
        }
    }

    @Override
    public boolean modified(int field) {
        return modifiedFields.contains(field);
    }

    @Override
    public ProtoMessage build() {
        return new ProtoMessage(getProtoMessage().build(), getProtoDescriptor());
    }
}
