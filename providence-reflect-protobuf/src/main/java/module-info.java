module io.pvdnc.reflect.protobuf {
    exports io.pvdnc.reflect.protobuf;
    exports io.pvdnc.reflect.protobuf.util;

    requires io.pvdnc.core;
    requires io.pvdnc.sio.protobuf;

    requires net.morimekta.strings;
    requires net.morimekta.collect;
    requires com.google.protobuf;
}