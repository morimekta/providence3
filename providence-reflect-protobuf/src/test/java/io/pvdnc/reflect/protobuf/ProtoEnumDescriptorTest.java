package io.pvdnc.reflect.protobuf;

import com.google.protobuf.Descriptors;
import com.google.protobuf.ProtocolMessageEnum;
import org.junit.jupiter.api.Test;

import static io.pvdnc.reflect.protobuf.ProtoEnumDescriptor.pvdEnumDescriptor;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.fail;

public class ProtoEnumDescriptorTest {
    @Test
    public void testGetDescriptor_icd() {
        ProtoEnumDescriptor<TestEnum> det = pvdEnumDescriptor(TestEnum.SECOND);
        ProtoEnumDescriptor<TestEnum> det2 = pvdEnumDescriptor(TestEnum.class);
        ProtoEnumDescriptor<TestEnum> det3 = pvdEnumDescriptor(TestEnum.getDescriptor());
        ProtoEnumDescriptor<TestEnum> ped = new ProtoEnumDescriptor<>(TestEnum.class, TestEnum.getDescriptor());

        assertThat(det, is(sameInstance(det2)));
        assertThat(det, is(sameInstance(det3)));
        assertThat(det, is(ped));

        assertThat(det.valueForName("NO_VALUE").$descriptor(), is(sameInstance(det)));
        assertThat(det.getDefaultValue().orElse(null), is(sameInstance(det.findByName("NO_VALUE"))));

        assertThat(det.allValues(), hasSize(7));
        assertThat(det.findById(0), is(det.findById(0)));
        assertThat(det.findById(0), is(ped.findById(0)));
        assertThat(det.findById(0), is(not(det.findById(1))));
        assertThat(det.findById(0), is(not(ped.findById(1))));

        assertThat(det.hashCode(), is(ped.hashCode()));

        assertThat(det.findById(0).hashCode(), is(ped.findById(0).hashCode()));
        assertThat(det.findById(0).hashCode(), is(not(ped.findById(1).hashCode())));

        assertThat(det.findByName("SECOND"), is(ped.findByName("SECOND")));
        assertThat(det.findByName("SECOND"), is(sameInstance(det.findByName("SECOND"))));
        assertThat(det.findById(null), is(nullValue()));
        assertThat(det.findByName(null), is(nullValue()));

        assertThat(det.valueForId(0).descriptor(), is(sameInstance(det)));
        assertThat(det.valueForId(0).$meta(), is(sameInstance(det.valueForName("NO_VALUE"))));
        assertThat(det.valueForId(0).getProtoValue(), is(sameInstance(TestEnum.NO_VALUE)));
        assertThat(det.valueForId(0).getValue(), is(0));
        assertThat(det.valueForId(0).getName(), is("NO_VALUE"));
    }

    @Test
    public void testGetDescriptor_cdi() {
        ProtoEnumDescriptor<TestEnum2> det = pvdEnumDescriptor(TestEnum2.class);
        ProtoEnumDescriptor<TestEnum2> det2 = pvdEnumDescriptor(TestEnum2.getDescriptor());
        ProtoEnumDescriptor<TestEnum2> det3 = pvdEnumDescriptor(TestEnum2.NO_SECOND);
        ProtoEnumDescriptor<TestEnum2> ped = new ProtoEnumDescriptor<>(TestEnum2.class, TestEnum2.getDescriptor());

        assertThat(det, is(sameInstance(det2)));
        assertThat(det, is(sameInstance(det3)));
        assertThat(det, is(ped));

        assertThat(det.allValues(), hasSize(2));
    }

    @Test
    public void testGetDescriptor_dic() {
        ProtoEnumDescriptor<TestEnum3> det = pvdEnumDescriptor(TestEnum3.getDescriptor());
        ProtoEnumDescriptor<TestEnum3> det2 = pvdEnumDescriptor(TestEnum3.ALTERNATIVE);
        ProtoEnumDescriptor<TestEnum3> det3 = pvdEnumDescriptor(TestEnum3.class);
        ProtoEnumDescriptor<TestEnum3> ped = new ProtoEnumDescriptor<>(TestEnum3.class, TestEnum3.getDescriptor());

        assertThat(det, is(sameInstance(det2)));
        assertThat(det, is(sameInstance(det3)));
        assertThat(det, is(ped));

        assertThat(det.allValues(), hasSize(2));
    }

    @Test
    public void testGetDescriptor_BadArgs() {
        try {
            pvdEnumDescriptor(Enum.class);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a typed enum class: java.lang.Enum"));
        }
        try {
            pvdEnumDescriptor(BadEnum.class);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a generated proto enum type: BadEnum"));
        }
        try {
            pvdEnumDescriptor(TestEnum.UNRECOGNIZED);
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Can't get the descriptor of an unrecognized enum value."));
        }
        try {
            pvdEnumDescriptor(BadEnum.UNRECOGNIZED);
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is(nullValue()));
        }
    }

    private enum BadEnum implements ProtocolMessageEnum {
        UNRECOGNIZED;

        @Override
        public int getNumber() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Descriptors.EnumValueDescriptor getValueDescriptor() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Descriptors.EnumDescriptor getDescriptorForType() {
            throw new UnsupportedOperationException();
        }
    }
}
