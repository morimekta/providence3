package io.pvdnc.reflect.protobuf;

import com.google.protobuf.ByteString;
import io.pvdnc.reflect.protobuf.collect.ProtoFieldList;
import io.pvdnc.reflect.protobuf.collect.ProtoFieldSet;
import net.morimekta.collect.util.Binary;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;

import static io.pvdnc.reflect.protobuf.ProtoMessageDescriptor.pvdMessageDescriptor;
import static java.nio.charset.StandardCharsets.UTF_8;
import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static net.morimekta.collect.UnmodifiableSortedMap.sortedMapOf;
import static net.morimekta.collect.UnmodifiableSortedSet.sortedSetOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;

public class ProtoMessageTest {
    public static final ProtoEnumDescriptor<ProtoTestEnum> ENUM_DESCRIPTOR
            = ProtoEnumDescriptor.pvdEnumDescriptor(ProtoTestEnum.class);
    public static final ProtoMessageDescriptor             MESSAGE_DESCRIPTOR
            = pvdMessageDescriptor(ProtoTestMessage.class);

    @Test
    public void testMessage() {
        ProtoTestMessage message = ProtoTestMessage.newBuilder()
                                                   .setB(true)
                                                   .setI(2)
                                                   .setSi(-3)
                                                   .setFi(4)
                                                   .setL(5)
                                                   .setSl(-6)
                                                   .setFl(7)
                                                   .setF(8.0f)
                                                   .setD(9.0)
                                                   .setStr("10")
                                                   .setBin(ByteString.copyFrom("11", UTF_8))
                                                   .setE(ProtoTestEnum.FIFTH)
                                                   .setC(ProtoTestCompact.newBuilder()
                                                                         .setI(1)
                                                                         .setL(2)
                                                                         .build())
                                                   .addAllStrings(listOf("14", "15"))
                                                   .addAllIntegers(listOf(15, 16))
                                                   .putAllIsMap(mapOf(16, "17"))
                                                   .putAllIiMap(mapOf(17, 18))
                                                   .addAllLongs(listOf(18L, 19L))
                                                   .build();
        ProtoMessage wrapper = new ProtoMessage(message, MESSAGE_DESCRIPTOR);
        assertThat(wrapper.$descriptor(), is(MESSAGE_DESCRIPTOR));
        assertThat(wrapper.mutate().build(), is(wrapper));

        assertThat(wrapper.get(0), is(nullValue()));
        assertThat(wrapper.has(0), is(false));
        assertThat(wrapper.optional(0), is(Optional.empty()));

        assertThat(wrapper.get(1), is(true));
        assertThat(wrapper.get(2), is(2));
        assertThat(wrapper.get(3), is(-3));
        assertThat(wrapper.get(4), is(4));
        assertThat(wrapper.get(5), is(5L));
        assertThat(wrapper.get(6), is(-6L));
        assertThat(wrapper.get(7), is(7L));
        assertThat(wrapper.get(8), is(8.0f));
        assertThat(wrapper.get(9), is(9.0));
        assertThat(wrapper.get(10), is("10"));
        assertThat(wrapper.get(11), is(Binary.encodeFromString("11", UTF_8)));
        assertThat(wrapper.get(12), is(ENUM_DESCRIPTOR.valueForId(5)));
        assertThat(wrapper.get(13), is(compact(() -> ProtoTestCompact.newBuilder()
                                                                     .setI(1)
                                                                     .setL(2)
                                                                     .build())));
        assertThat(wrapper.get(14), is(listOf("14", "15")));
        assertThat(wrapper.get(15), is(setOf(15, 16)));
        assertThat(wrapper.get(16), is(sortedMapOf(16, "17")));
        assertThat(wrapper.get(17), is(mapOf(17, 18)));
        assertThat(wrapper.get(18), is(sortedSetOf(18L, 19L)));
    }

    private ProtoMessage compact(Supplier<ProtoTestCompact> function) {
        return new ProtoMessage(function.get(), pvdMessageDescriptor(ProtoTestCompact.class));
    }

    @Test
    public void testMessage_Empty() {
        ProtoTestMessage empty = ProtoTestMessage.newBuilder().build();
        ProtoMessage wrapper = new ProtoMessage(empty, MESSAGE_DESCRIPTOR);
        assertThat(wrapper.$descriptor(), is(MESSAGE_DESCRIPTOR));
        assertThat(wrapper.get(0), is(nullValue()));
        assertThat(wrapper.has(0), is(false));
        assertThat(wrapper.optional(0), is(Optional.empty()));

        for (int i = 1; i <= 18; ++i) {
            assertThat(wrapper.has(i), is(false));
            assertThat(wrapper.optional(i), is(Optional.empty()));
        }

        assertThat(wrapper.get(1), is(false));
        assertThat(wrapper.get(2), is(0));
        assertThat(wrapper.get(3), is(0));
        assertThat(wrapper.get(4), is(0));
        assertThat(wrapper.get(5), is(0L));
        assertThat(wrapper.get(6), is(0L));
        assertThat(wrapper.get(7), is(0L));
        assertThat(wrapper.get(8), is(0.0f));
        assertThat(wrapper.get(9), is(0.0));
        assertThat(wrapper.get(10), is(""));
        assertThat(wrapper.get(11), is(Binary.empty()));
        assertThat(wrapper.get(12), is(ENUM_DESCRIPTOR.valueForId(0)));
        assertThat(wrapper.get(13), is(pvdMessageDescriptor(ProtoTestCompact.class).newBuilder().build()));
        assertThat(wrapper.get(14), is(listOf()));
        assertThat(wrapper.get(15), is(setOf()));
        assertThat(wrapper.get(16), is(sortedMapOf()));
        assertThat(wrapper.get(17), is(mapOf()));
        assertThat(wrapper.get(18), is(sortedSetOf()));
    }

    @Test
    public void testBuilder() {
        ProtoTestMessage.Builder builder = ProtoTestMessage.newBuilder()
                                                           .setB(true)
                                                           .setI(2)
                                                           .setSi(-3)
                                                           .setFi(4)
                                                           .setL(5)
                                                           .setSl(-6)
                                                           .setFl(7)
                                                           .setF(8.0f)
                                                           .setD(9.0)
                                                           .setStr("10")
                                                           .setBin(ByteString.copyFrom("11", UTF_8))
                                                           .setE(ProtoTestEnum.FIFTH)
                                                           .setC(ProtoTestCompact.newBuilder()
                                                                                 .setI(1)
                                                                                 .setL(2)
                                                                                 .build())
                                                           .addAllStrings(listOf("14", "15"))
                                                           .addAllIntegers(listOf(15, 16))
                                                           .putAllIsMap(mapOf(16, "17"))
                                                           .putAllIiMap(mapOf(17, 18))
                                                           .addAllLongs(listOf(18L, 19L));
        ProtoMessageBuilder wrapper = new ProtoMessageBuilder(builder, MESSAGE_DESCRIPTOR);
        assertThat(wrapper.$descriptor(), is(MESSAGE_DESCRIPTOR));

        assertThat(wrapper.get(0), is(nullValue()));
        assertThat(wrapper.has(0), is(false));
        assertThat(wrapper.optional(0), is(Optional.empty()));

        assertThat(wrapper.get(1), is(true));
        assertThat(wrapper.get(2), is(2));
        assertThat(wrapper.get(3), is(-3));
        assertThat(wrapper.get(4), is(4));
        assertThat(wrapper.get(5), is(5L));
        assertThat(wrapper.get(6), is(-6L));
        assertThat(wrapper.get(7), is(7L));
        assertThat(wrapper.get(8), is(8.0f));
        assertThat(wrapper.get(9), is(9.0));
        assertThat(wrapper.get(10), is("10"));
        assertThat(wrapper.get(11), is(Binary.encodeFromString("11", UTF_8)));
        assertThat(wrapper.get(12), is(ENUM_DESCRIPTOR.valueForId(5)));
        assertThat(wrapper.get(13), is(compact(() -> ProtoTestCompact.newBuilder()
                                                                     .setI(1)
                                                                     .setL(2)
                                                                     .build())));
        assertThat(wrapper.get(14), is(listOf("14", "15")));
        assertThat(wrapper.get(15), is(setOf(15, 16)));
        assertThat(wrapper.get(16), is(sortedMapOf(16, "17")));
        assertThat(wrapper.get(17), is(mapOf(17, 18)));
        assertThat(wrapper.get(18), is(sortedSetOf(18L, 19L)));

        ProtoMessageBuilder other = wrapper.mutate();
        assertThat(other.getProtoMessage(), is(not(sameInstance(wrapper.getProtoMessage()))));

        // --- mutable MESSAGE

        assertThat(wrapper.modified(13), is(false));
        ProtoMessageBuilder cb = wrapper.mutable(13);
        assertThat(wrapper.modified(13), is(true));
        ProtoMessageBuilder cb2 = wrapper.mutable(13);
        assertThat(cb, is(not(sameInstance(cb2))));
        assertThat(cb2.get(2), is(1));
        assertThat(builder.getC().getI(), is(1));
        cb.set(2, 3);
        assertThat(cb2.get(2), is(3));
        assertThat(builder.getC().getI(), is(3));

        // --- mutable LIST

        assertThat(wrapper.modified(14), is(false));
        assertThat(wrapper.mutable(14), is(instanceOf(ProtoFieldList.class)));
        assertThat(wrapper.mutable(14), is(List.of("14", "15")));
        assertThat(wrapper.modified(14), is(true));

        // --- mutable SET

        assertThat(wrapper.modified(15), is(false));
        assertThat(wrapper.mutable(15), is(instanceOf(ProtoFieldSet.class)));
        assertThat(wrapper.mutable(15), is(Set.of(15, 16)));
        assertThat(wrapper.modified(15), is(true));

        // TODO: sorted.
        assertThat(wrapper.modified(18), is(false));
        assertThat(wrapper.mutable(18), is(instanceOf(ProtoFieldSet.class)));
        assertThat(wrapper.mutable(18), is(Set.of(18L, 19L)));
        assertThat(wrapper.modified(18), is(true));

        // --- mutable MAP

        // TODO: Implement.

        // --- mutable * (just set the value to default, which in this case just marks it as modified).

        assertThat(wrapper.modified(10), is(false));
        assertThat(wrapper.mutable(10), is("10"));
        assertThat(wrapper.modified(10), is(true));

    }

    @Test
    public void testBuilder_Empty() {
        ProtoTestMessage.Builder empty = ProtoTestMessage.newBuilder();
        ProtoMessageBuilder wrapper = new ProtoMessageBuilder(empty, MESSAGE_DESCRIPTOR);
        assertThat(wrapper.$descriptor(), is(MESSAGE_DESCRIPTOR));

        for (int i = 1; i <= 18; ++i) {
            assertThat(wrapper.has(i), is(false));
            assertThat(wrapper.optional(i), is(Optional.empty()));
        }

        assertThat(wrapper.get(1), is(false));
        assertThat(wrapper.get(2), is(0));
        assertThat(wrapper.get(3), is(0));
        assertThat(wrapper.get(4), is(0));
        assertThat(wrapper.get(5), is(0L));
        assertThat(wrapper.get(6), is(0L));
        assertThat(wrapper.get(7), is(0L));
        assertThat(wrapper.get(8), is(0.0f));
        assertThat(wrapper.get(9), is(0.0));
        assertThat(wrapper.get(10), is(""));
        assertThat(wrapper.get(11), is(Binary.empty()));
        assertThat(wrapper.get(12), is(ENUM_DESCRIPTOR.valueForId(0)));
        assertThat(wrapper.get(13), is(pvdMessageDescriptor(ProtoTestCompact.class).newBuilder().build()));
        assertThat(wrapper.get(14), is(listOf()));
        assertThat(wrapper.get(15), is(setOf()));
        assertThat(wrapper.get(16), is(sortedMapOf()));
        assertThat(wrapper.get(17), is(mapOf()));
        assertThat(wrapper.get(18), is(sortedSetOf()));
    }
}
