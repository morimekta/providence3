package io.pvdnc.reflect.protobuf;

import io.pvdnc.core.types.PField;
import org.junit.jupiter.api.Test;

import static io.pvdnc.reflect.protobuf.ProtoMessageDescriptor.pvdMessageDescriptor;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;

public class ProtoMessageDescriptorTest {
    @Test
    public void testGetDescriptor_icd() {
        ProtoMessageDescriptor def = pvdMessageDescriptor(TestMessage.newBuilder().build());
        ProtoMessageDescriptor def2 = pvdMessageDescriptor(TestMessage.newBuilder());
        ProtoMessageDescriptor def3 = pvdMessageDescriptor(TestMessage.class);
        ProtoMessageDescriptor def4 = pvdMessageDescriptor(TestMessage.getDescriptor());
        ProtoMessageDescriptor pmd = new ProtoMessageDescriptor(TestMessage.class, TestMessage.getDescriptor());

        assertThat(def, is(pmd));
        assertThat(def, is(sameInstance(def2)));
        assertThat(def, is(sameInstance(def3)));
        assertThat(def, is(sameInstance(def4)));

        for (PField field : def.declaredFields()) {
            assertThat(field, is(instanceOf(ProtoField.class)));
            assertThat(field.getResolvedDescriptor(), is(notNullValue()));
            assertThat(field.getDefaultValue(), is(notNullValue()));
        }

        assertThat(def.fieldForId(1), hasProperty("name", is("enumValue")));
        assertThat(def.fieldForName("enumValue"), hasProperty("id", is(1)));

        ProtoMessageBuilder builder = def.newBuilder();
        assertThat(builder, is(notNullValue()));
        assertThat(builder.$descriptor(), is(sameInstance(def)));
        ProtoMessage message = builder.build();
        assertThat(message, is(notNullValue()));
        assertThat(message.$descriptor(), is(sameInstance(def)));
    }

    @Test
    public void testGetDescriptor_cdi() {
        ProtoMessageDescriptor def = pvdMessageDescriptor(TestMessage2.class);
        ProtoMessageDescriptor def2 = pvdMessageDescriptor(TestMessage2.getDescriptor());
        ProtoMessageDescriptor def3 = pvdMessageDescriptor(TestMessage2.newBuilder());
        ProtoMessageDescriptor pmd = new ProtoMessageDescriptor(TestMessage2.class, TestMessage2.getDescriptor());

        assertThat(def, is(pmd));
        assertThat(def, is(sameInstance(def2)));
        assertThat(def, is(sameInstance(def3)));
    }

    @Test
    public void testGetDescriptor_dic() {
        ProtoMessageDescriptor def = pvdMessageDescriptor(TestMessage3.getDescriptor());
        ProtoMessageDescriptor def2 = pvdMessageDescriptor(TestMessage3.newBuilder());
        ProtoMessageDescriptor def3 = pvdMessageDescriptor(TestMessage3.class);
        ProtoMessageDescriptor pmd = new ProtoMessageDescriptor(TestMessage3.class, TestMessage3.getDescriptor());

        assertThat(def, is(pmd));
        assertThat(def, is(sameInstance(def2)));
        assertThat(def, is(sameInstance(def3)));
    }
}
