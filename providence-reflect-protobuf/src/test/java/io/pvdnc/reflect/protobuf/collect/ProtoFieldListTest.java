package io.pvdnc.reflect.protobuf.collect;

import io.pvdnc.reflect.protobuf.ProtoMessage;
import io.pvdnc.reflect.protobuf.ProtoMessageBuilder;
import io.pvdnc.reflect.protobuf.ProtoTestMessage;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;

import static io.pvdnc.reflect.protobuf.ProtoMessageTest.MESSAGE_DESCRIPTOR;
import static net.morimekta.collect.UnmodifiableList.listOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ProtoFieldListTest {
    @Test
    public void testList() {
        ProtoTestMessage.Builder builder = ProtoTestMessage.newBuilder()
                                                           .addAllStrings(listOf("foo", "bar"));
        ProtoMessageBuilder wrapper = new ProtoMessageBuilder(builder, MESSAGE_DESCRIPTOR);
        List<String> list = wrapper.mutable(14);

        list.add("foo");
        list.remove(0);
        list.addAll(1, listOf("bar", "baz"));
        list.add(0, "foo2");
        list.set(1, "baa");
        assertThat(wrapper.get(14), is(List.of("foo2", "baa", "bar", "baz", "foo")));
        assertThat(wrapper.mutable(14), is(instanceOf(ProtoFieldList.class)));

        list.clear();
        list.addAll(listOf("gob", "baz"));
        list.set(2, "bar");
        list.sort(Comparator.naturalOrder());
        assertThat(wrapper.get(14), is(List.of("bar", "baz", "gob")));

        ListIterator<String> li14 = list.listIterator();
        assertThat(li14.next(), is("bar"));
        li14.remove();
        assertThat(li14.next(), is("baz"));
        li14.set("foo");
        li14.add("foo2");
        assertThat(li14.nextIndex(), is(2));
        assertThat(li14.previousIndex(), is(1));

        assertThat(li14.next(), is("gob"));

        assertThat(list.lastIndexOf("foo"), is(1));
        assertThat(list.lastIndexOf("boo"), is(-1));
        assertThat(list, is(List.of("foo2", "foo", "gob")));

        list.clear();
        list.addAll(0, listOf("foo", "bar"));
        assertThat(list, is(List.of("foo", "bar")));

        ProtoMessage msg = wrapper.build();
        assertThat(msg.get(14), is((List<?>) wrapper.get(14)));
    }
}
