package io.pvdnc.reflect.protobuf.collect;

import io.pvdnc.reflect.protobuf.ProtoMessageBuilder;
import io.pvdnc.reflect.protobuf.ProtoTestMessage;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.Set;

import static io.pvdnc.reflect.protobuf.ProtoMessageTest.MESSAGE_DESCRIPTOR;
import static net.morimekta.collect.UnmodifiableList.listOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ProtoFieldSetTest {
    @Test
    public void testSet() {
        ProtoTestMessage.Builder builder = ProtoTestMessage.newBuilder()
                                                           .addAllIntegers(listOf(15, 16));
        ProtoMessageBuilder wrapper = new ProtoMessageBuilder(builder, MESSAGE_DESCRIPTOR);
        Set<Integer> set = wrapper.mutable(15);

        assertThat(set, is(Set.of(15, 16)));
        set.add(17);
        assertThat(set, is(Set.of(15, 16, 17)));
        set.add(16);
        assertThat(set, is(Set.of(15, 16, 17)));
        set.remove(15);
        assertThat(set, is(Set.of(16, 17)));
        set.clear();
        assertThat(set, is(Set.of()));
        set.add(5);
        assertThat(set, is(Set.of(5)));
        Iterator<Integer> i = set.iterator();
        assertThat(i.next(), is(5));
        i.remove();
        assertThat(i.hasNext(), is(false));
        assertThat(set, is(Set.of()));
    }
}
