// generated by providence-idl-generator:{version} @ {date}
package io.pvdnc.sio.protobuf.pvd;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.PMessageBuilder;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.types.PType;
import io.pvdnc.core.utils.ValueUtil;
import net.morimekta.collect.UnmodifiableList;
import net.morimekta.collect.UnmodifiableSet;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

@SuppressWarnings("unused")
public interface TestOnly
        extends PMessage {
    // ---- Declared Fields ----

    /**
     * @return If the vo value is set.
     */
    boolean hasVo();

    /**
     * @return Optional bt value.
     */
    Optional<Byte> optionalBt();

    /**
     * @return The bt value or default.
     */
    byte getBt();

    /**
     * @return If the bt value is set.
     */
    boolean hasBt();

    /**
     * @return Optional sh value.
     */
    Optional<Short> optionalSh();

    /**
     * @return The sh value or default.
     */
    short getSh();

    /**
     * @return If the sh value is set.
     */
    boolean hasSh();

    /**
     * @return Optional is value.
     */
    Optional<java.util.Set<Integer>> optionalIs();

    /**
     * @return The is value or default.
     */
    java.util.Set<Integer> getIs();

    /**
     * @return If the is value is set.
     */
    boolean hasIs();

    // ---- PMessage ----

    /**
     * @return The test.TestOnly builder.
     */
    @Override
    TestOnly.Builder mutate();

    /**
     * @return The test.TestOnly descriptor.
     */
    @Override
    PMessageDescriptor<? extends TestOnly> $descriptor();

    // ---- Static ----

    $Descriptor kDescriptor = new $Descriptor();

    static TestOnly.Builder newBuilder() {
        return kDescriptor.newBuilder();
    }

    static TestOnly empty() {
        return $Impl.kEmpty;
    }

    static Supplier<$Descriptor> provider() {
        return () -> kDescriptor;
    }

    /**
     * Builder interface for creating test.TestOnly instances.
     */
    interface Builder
            extends PMessageBuilder,
                    TestOnly {
        // ---- Fields ----

        /**
         * @return The vo builder.
         */
        TestOnly.Builder setVo();

        /**
         * Clears the vo value.
         *
         * @return The vo builder.
         */
        TestOnly.Builder clearVo();

        /**
         * @param value The new bt value
         * @return The bt builder.
         */
        TestOnly.Builder setBt(Byte value);

        /**
         * Get mutable value of the field. This will initialize the field to default
         * if not set, and change any immutable type that has a mutable counter part
         * to it&#39;s mutable type.
         *
         * @return The mutable bt value.
         */
        byte mutableBt();

        /**
         * Clears the bt value.
         *
         * @return The bt builder.
         */
        TestOnly.Builder clearBt();

        /**
         * @param value The new sh value
         * @return The sh builder.
         */
        TestOnly.Builder setSh(Short value);

        /**
         * Get mutable value of the field. This will initialize the field to default
         * if not set, and change any immutable type that has a mutable counter part
         * to it&#39;s mutable type.
         *
         * @return The mutable sh value.
         */
        short mutableSh();

        /**
         * Clears the sh value.
         *
         * @return The sh builder.
         */
        TestOnly.Builder clearSh();

        /**
         * @param value The new is value
         * @return The is builder.
         */
        TestOnly.Builder setIs(java.util.Collection<Integer> value);

        /**
         * Get mutable value of the field. This will initialize the field to default
         * if not set, and change any immutable type that has a mutable counter part
         * to it&#39;s mutable type.
         *
         * @return The mutable is value.
         */
        java.util.Set<Integer> mutableIs();

        /**
         * Clears the is value.
         *
         * @return The is builder.
         */
        TestOnly.Builder clearIs();

        // ---- PMessage.Builder ----

        /**
         * @return The built test.TestOnly instance.
         */
        @Override
        TestOnly build();
    }

    class $Descriptor extends PMessageDescriptor<TestOnly> {
        public static final byte kDefaultBt = (byte) 0;
        public static final short kDefaultSh = (short) 0;
        public static final java.util.Set<Integer> kDefaultIs = net.morimekta.collect.UnmodifiableSet.setOf();

        public static final PField kFieldVo = new PField(
                1,
                PType.VOID.provider(),
                "vo",
                null,
                PPropertyMap.of(),
                null,
                () -> TestOnly.kDescriptor);
        public static final PField kFieldBt = new PField(
                2,
                PType.BYTE.provider(),
                "bt",
                () -> kDefaultBt,
                PPropertyMap.of(),
                null,
                () -> TestOnly.kDescriptor);
        public static final PField kFieldSh = new PField(
                3,
                PType.SHORT.provider(),
                "sh",
                () -> kDefaultSh,
                PPropertyMap.of(),
                null,
                () -> TestOnly.kDescriptor);
        public static final PField kFieldIs = new PField(
                4,
                PSetDescriptor.provider(PType.INT.provider()),
                "is",
                () -> kDefaultIs,
                PPropertyMap.of(),
                null,
                () -> TestOnly.kDescriptor);

        private final List<PField> mAllFields;
        private final List<PField> mDeclaredFields;

        private $Descriptor() {
            super("test", "TestOnly", PPropertyMap.of(
                    "message.variant", "STRUCT"));
            this.mDeclaredFields = UnmodifiableList.listOf(
                    kFieldVo,
                    kFieldBt,
                    kFieldSh,
                    kFieldIs);
            this.mAllFields = this.mDeclaredFields;
        }

        @Override
        public List<PField> declaredFields() {
            return mDeclaredFields;
        }

        @Override
        public List<PField> allFields() {
            return mAllFields;
        }

        @Override
        public PField findFieldById(int id) {
            switch (id) {
                case 1: return kFieldVo;
                case 2: return kFieldBt;
                case 3: return kFieldSh;
                case 4: return kFieldIs;
            }
            return null;
        }

        @Override
        public PField findFieldByName(String name) {
            if (name == null) return null;
            switch (name) {
                case "vo": return kFieldVo;
                case "bt": return kFieldBt;
                case "sh": return kFieldSh;
                case "is": return kFieldIs;
            }
            return null;
        }

        @Override
        public TestOnly.Builder newBuilder() {
            return new TestOnly.$BuilderImpl();
        }
    }

    class $Impl
            implements TestOnly {
        private final Boolean vVo;
        private final Byte vBt;
        private final Short vSh;
        private final java.util.Set<Integer> vIs;

        private transient Integer tHashCode;

        protected $Impl(Boolean pVo,
                        Byte pBt,
                        Short pSh,
                        java.util.Set<Integer> pIs) {
            vVo = pVo;
            vBt = pBt;
            vSh = pSh;
            vIs = pIs == null ? null : UnmodifiableSet.asSet(pIs);
        }

        @Override
        public boolean hasVo() {
            return vVo != null;
        }

        @Override
        public byte getBt() {
            return vBt != null ? vBt : TestOnly.$Descriptor.kDefaultBt;
        }

        @Override
        public Optional<Byte> optionalBt() {
            return Optional.ofNullable(vBt);
        }

        @Override
        public boolean hasBt() {
            return vBt != null;
        }

        @Override
        public short getSh() {
            return vSh != null ? vSh : TestOnly.$Descriptor.kDefaultSh;
        }

        @Override
        public Optional<Short> optionalSh() {
            return Optional.ofNullable(vSh);
        }

        @Override
        public boolean hasSh() {
            return vSh != null;
        }

        @Override
        public java.util.Set<Integer> getIs() {
            return vIs != null ? vIs : TestOnly.$Descriptor.kDefaultIs;
        }

        @Override
        public Optional<java.util.Set<Integer>> optionalIs() {
            return Optional.ofNullable(vIs);
        }

        @Override
        public boolean hasIs() {
            return vIs != null;
        }

        @Override
        public boolean has(int id) {
            switch (id) {
                case 1: return vVo != null;
                case 2: return vBt != null;
                case 3: return vSh != null;
                case 4: return vIs != null;
                default: return false;
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T get(int id) {
            switch (id) {
                case 1: return (T) (Object) hasVo();
                case 2: return (T) (Object) getBt();
                case 3: return (T) (Object) getSh();
                case 4: return (T) getIs();
                default: throw new IllegalArgumentException("No field " + id + " on test.TestOnly");
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> Optional<T> optional(int id) {
            switch (id) {
                case 1: return Optional.ofNullable((T) vVo);
                case 2: return Optional.ofNullable((T) vBt);
                case 3: return Optional.ofNullable((T) vSh);
                case 4: return Optional.ofNullable((T) vIs);
                default: return Optional.empty();
            }
        }

        @Override
        public TestOnly.$BuilderImpl mutate() {
            return new TestOnly.$BuilderImpl(
                    vVo,
                    vBt,
                    vSh,
                    vIs);
        }

        @Override
        public PMessageDescriptor<TestOnly> $descriptor() {
            return TestOnly.kDescriptor;
        }

        // ---- Object ----

        @Override
        public String toString() {
            return "test.TestOnly" + ValueUtil.asString(this);
        }

        @Override
        public int hashCode() {
            if (tHashCode == null) {
                tHashCode = Objects.hash(
                        vVo,
                        vBt,
                        vSh,
                        vIs);
            }
            return tHashCode;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (o == null || !getClass().equals(o.getClass())) {
                return false;
            }
            TestOnly.$Impl other = (TestOnly.$Impl) o;
            return Objects.equals(vVo, other.vVo) &&
                   Objects.equals(vBt, other.vBt) &&
                   Objects.equals(vSh, other.vSh) &&
                   Objects.equals(vIs, other.vIs);
        }

        public static final TestOnly.$Impl kEmpty = new TestOnly.$Impl(null, null, null, null);
    }

    class $BuilderImpl
            implements TestOnly.Builder {
        private Boolean vVo;
        private Byte vBt;
        private Short vSh;
        private java.util.Set<Integer> vIs;

        private boolean mVo;
        private boolean mBt;
        private boolean mSh;
        private boolean mIs;

        protected $BuilderImpl(Boolean pVo,
                               Byte pBt,
                               Short pSh,
                               java.util.Set<Integer> pIs) {
            vVo = pVo;
            vBt = pBt;
            vSh = pSh;
            vIs = pIs;
        }

        protected $BuilderImpl() {
        }

        @Override
        public boolean hasVo() {
            return vVo != null;
        }

        @Override
        public byte getBt() {
            return vBt != null ? vBt : TestOnly.$Descriptor.kDefaultBt;
        }

        @Override
        public Optional<Byte> optionalBt() {
            return Optional.ofNullable(vBt);
        }

        @Override
        public boolean hasBt() {
            return vBt != null;
        }

        @Override
        public short getSh() {
            return vSh != null ? vSh : TestOnly.$Descriptor.kDefaultSh;
        }

        @Override
        public Optional<Short> optionalSh() {
            return Optional.ofNullable(vSh);
        }

        @Override
        public boolean hasSh() {
            return vSh != null;
        }

        @Override
        public java.util.Set<Integer> getIs() {
            return vIs != null ? vIs : TestOnly.$Descriptor.kDefaultIs;
        }

        @Override
        public Optional<java.util.Set<Integer>> optionalIs() {
            return Optional.ofNullable(vIs);
        }

        @Override
        public boolean hasIs() {
            return vIs != null;
        }

        @Override
        public TestOnly.$BuilderImpl setVo() {
            mVo |= vVo == null;
            vVo = true;
            return this;
        }

        @Override
        public TestOnly.$BuilderImpl clearVo() {
            mVo |= vVo != null;
            vVo = null;
            return this;
        }

        @Override
        public TestOnly.$BuilderImpl setBt(Byte value) {
            if (value == null) {
                return clearBt();
            }
            mBt |= !Objects.equals(vBt, value);
            vBt = value;
            return this;
        }

        @Override
        public byte mutableBt() {
            mBt = true;
            if (vBt == null) {
                vBt = TestOnly.$Descriptor.kDefaultBt;
            }
            return vBt;
        }

        @Override
        public TestOnly.$BuilderImpl clearBt() {
            mBt |= vBt != null;
            vBt = null;
            return this;
        }

        @Override
        public TestOnly.$BuilderImpl setSh(Short value) {
            if (value == null) {
                return clearSh();
            }
            mSh |= !Objects.equals(vSh, value);
            vSh = value;
            return this;
        }

        @Override
        public short mutableSh() {
            mSh = true;
            if (vSh == null) {
                vSh = TestOnly.$Descriptor.kDefaultSh;
            }
            return vSh;
        }

        @Override
        public TestOnly.$BuilderImpl clearSh() {
            mSh |= vSh != null;
            vSh = null;
            return this;
        }

        @Override
        public TestOnly.$BuilderImpl setIs(java.util.Collection<Integer> value) {
            if (value == null) {
                return clearIs();
            }
            mIs = true;
            vIs = UnmodifiableSet.asSet(value);
            return this;
        }

        @Override
        public java.util.Set<Integer> mutableIs() {
            mIs = true;
            if (vIs == null) {
                vIs = TestOnly.$Descriptor.kDefaultIs;
            }
            if (!(vIs instanceof java.util.HashSet)) {
                vIs = new java.util.LinkedHashSet<>(vIs);
            }
            return vIs;
        }

        @Override
        public TestOnly.$BuilderImpl clearIs() {
            mIs |= vIs != null;
            vIs = null;
            return this;
        }

        @Override
        public boolean has(int id) {
            switch (id) {
                case 1: return vVo != null;
                case 2: return vBt != null;
                case 3: return vSh != null;
                case 4: return vIs != null;
                default: return false;
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T get(int id) {
            switch (id) {
                case 1: return (T) (Object) hasVo();
                case 2: return (T) (Object) getBt();
                case 3: return (T) (Object) getSh();
                case 4: return (T) getIs();
                default: throw new IllegalArgumentException("No field " + id + " on test.TestOnly");
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> Optional<T> optional(int id) {
            switch (id) {
                case 1: return Optional.ofNullable((T) vVo);
                case 2: return Optional.ofNullable((T) vBt);
                case 3: return Optional.ofNullable((T) vSh);
                case 4: return Optional.ofNullable((T) vIs);
                default: return Optional.empty();
            }
        }

        @Override
        public TestOnly.$BuilderImpl mutate() {
            return new TestOnly.$BuilderImpl(
                    vVo,
                    vBt,
                    vSh,
                    vIs);
        }

        @Override
        public PMessageDescriptor<TestOnly> $descriptor() {
            return TestOnly.kDescriptor;
        }

        @Override
        @SuppressWarnings("unchecked")
        public void set(int id, Object value) {
            switch (id) {
                case 1: {
                    if (value == Boolean.TRUE) setVo();
                    else clearVo();
                    break;
                }
                case 2: setBt((Byte) value); break;
                case 3: setSh((Short) value); break;
                case 4: setIs((java.util.Collection<Integer>) value); break;
                default: throw new IllegalArgumentException("Unknown field id " + id + " on test.TestOnly");
            }
        }

        @Override
        public void clear(int id) {
            switch (id) {
                case 1: clearVo(); break;
                case 2: clearBt(); break;
                case 3: clearSh(); break;
                case 4: clearIs(); break;
                default: break;
            }
        }

        @Override
        public boolean modified(int id) {
            switch (id) {
                case 1: return mVo;
                case 2: return mBt;
                case 3: return mSh;
                case 4: return mIs;
                default: return false;
            }
        }

        @Override
        public TestOnly.$Impl build() {
            return new TestOnly.$Impl(
                    vVo,
                    vBt,
                    vSh,
                    vIs);
        }

        // ---- Object ----

        @Override
        public String toString() {
            return "test.TestOnly" + ValueUtil.asString(this);
        }

        @Override
        public int hashCode() {
            return Objects.hash(
                    vVo,
                    vBt,
                    vSh,
                    vIs);
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (o == null || !getClass().equals(o.getClass())) {
                return false;
            }
            TestOnly.$BuilderImpl other = (TestOnly.$BuilderImpl) o;
            return Objects.equals(vVo, other.vVo) &&
                   Objects.equals(vBt, other.vBt) &&
                   Objects.equals(vSh, other.vSh) &&
                   Objects.equals(vIs, other.vIs);
        }
    }
}
