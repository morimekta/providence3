package io.pvdnc.sio.protobuf;

import io.pvdnc.sio.protobuf.utils.ProtoBufProperties;
import io.pvdnc.sio.protobuf.utils.ProtoBufValueType;
import org.junit.jupiter.api.Test;

import static io.pvdnc.sio.protobuf.pvd.TestMessage.$Descriptor.kFieldAny;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ProtoBufPropertiesTest {
    @Test
    public void testProperties() {
        assertThat(kFieldAny.getProperty(ProtoBufProperties.PROTO_TYPE), is(ProtoBufValueType.BYTES));
    }
}
