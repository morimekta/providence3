package io.pvdnc.sio.protobuf;

import com.google.protobuf.Any;
import com.google.protobuf.ByteString;
import io.pvdnc.core.PAny;
import io.pvdnc.sio.protobuf.pb.TestCompact;
import io.pvdnc.sio.protobuf.pb.TestEnum;
import io.pvdnc.sio.protobuf.pb.TestMessage;
import net.morimekta.collect.util.Binary;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Random;

import static java.nio.charset.StandardCharsets.UTF_8;
import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ProtoBufSerializerTest {
    private final Random random = new Random();
    private final ProtoBufSerializer serializer = new ProtoBufSerializer();

    @Test
    public void testSerialize() throws IOException {
        var protoMessage = TestMessage.newBuilder()
                .setB(true)
                .setI(random.nextInt(10000))
                .setSi(random.nextInt(20000) - 10000)
                .setFi(random.nextInt())
                .setL(random.nextInt(100000))
                .setSl(random.nextInt(200000) - 100000)
                .setFl(random.nextLong())
                .setF((float) (random.nextDouble() * 1_000_000))
                .setD((random.nextDouble() * 1_000_000_000L))
                .setStr("string")
                .setBin(ByteString.copyFrom("data", UTF_8))
                .setE(TestEnum.FOURTH)
                .setC(TestCompact.newBuilder()
                        .setI(random.nextInt(100) + 1)
                        .setL(random.nextInt(20000) - 10000)
                        .build())
                .addAllStrings(listOf("first", "second"))
                .addAllIntegers(listOf(1, 1, 2, 3, 5, 8, 13))
                .putAllIsMap(mapOf(42, "bar"))
                .putAllIiMap(mapOf(12, 42, 13, 11))
                .setAny(Any.pack(TestCompact.newBuilder().setI(42).build()))
                .build();
        var pvdMessage = io.pvdnc.sio.protobuf.pvd.TestMessage.newBuilder()
                .setB(protoMessage.getB())
                .setI(protoMessage.getI())
                .setSi(protoMessage.getSi())
                .setFi(protoMessage.getFi())
                .setL(protoMessage.getL())
                .setSl(protoMessage.getSl())
                .setFl(protoMessage.getFl())
                .setF(protoMessage.getF())
                .setD(protoMessage.getD())
                .setStr(protoMessage.getStr())
                .setBin(Binary.wrap(protoMessage.getBin().toByteArray()))
                .setE(io.pvdnc.sio.protobuf.pvd.TestEnum.FOURTH)
                .setC(io.pvdnc.sio.protobuf.pvd.TestCompact.newBuilder()
                        .setI(protoMessage.getC().getI())
                        .setL(protoMessage.getC().getL())
                        .build())
                .setStrings(protoMessage.getStringsList())
                .setIntegers(protoMessage.getIntegersList())
                .setIsMap(protoMessage.getIsMapMap())
                .setIiMap(protoMessage.getIiMapMap())
                .setAny(PAny.wrap(
                        io.pvdnc.sio.protobuf.pvd.TestCompact.newBuilder().setI(42).build())
                            .pack(serializer))
                .build();

        ByteArrayOutputStream fromPvd = new ByteArrayOutputStream();
        serializer.writeTo(pvdMessage, fromPvd);
        var parsed1 = TestMessage.parseFrom(fromPvd.toByteArray());
        assertThat(parsed1, is(protoMessage));

        Binary fromProto = Binary.wrap(protoMessage.toByteArray());
        io.pvdnc.sio.protobuf.pvd.TestMessage parsedProtoToPvd = serializer.readFrom(
                io.pvdnc.sio.protobuf.pvd.TestMessage.kDescriptor, fromProto.getInputStream());
        assertThat(parsedProtoToPvd.getAny().getMessage(),
                is(io.pvdnc.sio.protobuf.pvd.TestCompact.newBuilder().setI(42).build()));
        assertThat(parsedProtoToPvd, is(pvdMessage));
    }
}
