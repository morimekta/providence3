// generated by providence-idl-generator:{version} @ {date}
package io.pvdnc.sio.protobuf.impl;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.registry.PSimpleTypeRegistry;
import io.pvdnc.core.types.PFileDescriptor;
import net.morimekta.collect.UnmodifiableList;
import net.morimekta.collect.UnmodifiableMap;

@SuppressWarnings("unused")
public final class Protobuf_FileDescriptor extends PFileDescriptor {
    public static final Protobuf_FileDescriptor kDescriptor = new Protobuf_FileDescriptor();

    private Protobuf_FileDescriptor() {
        super("protobuf",
                PPropertyMap.of(
                        "java.namespace", "io.pvdnc.sio.protobuf.impl"),
                UnmodifiableMap.mapOf(),
                UnmodifiableList.listOf(
                        AnyProtoBuf.kDescriptor));
        PSimpleTypeRegistry.getInstance().registerRecursively(this);
    }
}
