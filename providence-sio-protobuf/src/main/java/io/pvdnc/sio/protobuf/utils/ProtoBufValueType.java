/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.protobuf.utils;


import io.pvdnc.core.types.PType;

import static java.util.Objects.requireNonNull;

public enum ProtoBufValueType {
    DOUBLE("double", PType.DOUBLE, ProtoBufWireType._64BIT),
    FLOAT("float", PType.FLOAT, ProtoBufWireType._32BIT),
    /** Uses LEB128 encoding. */
    INT32("int32", PType.INT, ProtoBufWireType.VARINT),
    /** Uses LEB128 encoding. */
    INT64("int64", PType.LONG, ProtoBufWireType.VARINT),
    /** Uses LEB128 encoding. */
    UINT32("uint32", PType.INT, ProtoBufWireType.VARINT),
    /** Uses LEB128 encoding. */
    UINT64("uint64", PType.LONG, ProtoBufWireType.VARINT),
    /** Uses zigzag LEB128 encoding. */
    SINT32("sint32", PType.INT, ProtoBufWireType.VARINT),
    /** Uses zigzag encoding. */
    SINT64("sint64", PType.LONG, ProtoBufWireType.VARINT),
    FIXED32("fixed32", PType.INT, ProtoBufWireType._32BIT),
    FIXED64("fixed64", PType.LONG, ProtoBufWireType._64BIT),
    SFIXED32("sfixed32", PType.INT, ProtoBufWireType._32BIT),
    SFIXED64("sfixed64", PType.LONG, ProtoBufWireType._64BIT),
    BOOL("bool", PType.BOOL, ProtoBufWireType.VARINT),
    STRING("string", PType.STRING, ProtoBufWireType.BUFFER),
    BYTES("bytes", PType.BINARY, ProtoBufWireType.BUFFER),
    ;

    public final String name;
    public final PType type;
    public final byte wire;

    ProtoBufValueType(String name, PType type, byte wire) {
        this.name = name;
        this.type = type;
        this.wire = wire;
    }

    @Override
    public String toString() {
        return name;
    }

    public static ProtoBufValueType defaultValueType(PType type) {
        requireNonNull(type, "type == null");
        switch (type) {
            case VOID: // always with the value 'true'.
            case BOOL: // with '0' and '1' for false and true.
                return BOOL;
            case BYTE:
            case SHORT:
            case INT:
                return SINT32;
            case LONG:
                return SINT64;
            case FLOAT:
                return FLOAT;
            case DOUBLE:
                return DOUBLE;
            case STRING: // using utf-8 encoded string.
                return STRING;
            case ENUM:  // not zigzag encoded as only non-negative numbers.
                return INT32;
            case BINARY: // containing the raw data content.
            case MESSAGE: // containing the serialized message.
            case LIST: // defaults to using packed list.
            case SET: // defaults to using packed set.
            case MAP: // encoded as virtual message with {1:key,2:value}.
            case ANY: // encoded as a replacement message.
                return BYTES;
            default: throw new IllegalArgumentException("Unhandled type " + type);
        }
    }

    public static ProtoBufValueType forName(String name) {
        ProtoBufValueType ret = findByName(name);
        if (ret == null) {
            throw new IllegalArgumentException("No proto buf value type for '" + name + "'");
        }
        return ret;
    }

    public static ProtoBufValueType findByName(String name) {
        requireNonNull(name, "name == null");
        for (ProtoBufValueType type : values()) {
            if (type.name.equals(name)) {
                return type;
            }
        }
        return null;
    }
}
