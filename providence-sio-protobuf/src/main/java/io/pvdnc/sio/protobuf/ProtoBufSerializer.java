/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.protobuf;

import io.pvdnc.core.PAny;
import io.pvdnc.core.PEnum;
import io.pvdnc.core.PMessage;
import io.pvdnc.core.PMessageBuilder;
import io.pvdnc.core.impl.AnyBinary;
import io.pvdnc.core.io.PSerializer;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.types.PType;
import io.pvdnc.sio.protobuf.impl.AnyProtoBuf;
import io.pvdnc.sio.protobuf.utils.ProtoBufValueType;
import net.morimekta.collect.ListBuilder;
import net.morimekta.collect.MapBuilder;
import net.morimekta.collect.SetBuilder;
import net.morimekta.collect.UnmodifiableList;
import net.morimekta.collect.util.Binary;
import net.morimekta.io.LittleEndianBinaryInputStream;
import net.morimekta.io.LittleEndianBinaryOutputStream;
import net.morimekta.io.sub.FramedOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import static io.pvdnc.sio.protobuf.utils.ProtoBufProperties.PROTO_KEY_TYPE;
import static io.pvdnc.sio.protobuf.utils.ProtoBufProperties.PROTO_PACKED;
import static io.pvdnc.sio.protobuf.utils.ProtoBufProperties.PROTO_TYPE;
import static io.pvdnc.sio.protobuf.utils.ProtoBufUtils.getItemType;
import static io.pvdnc.sio.protobuf.utils.ProtoBufWireType.BUFFER;
import static io.pvdnc.sio.protobuf.utils.ProtoBufWireType.GROUP_END;
import static io.pvdnc.sio.protobuf.utils.ProtoBufWireType.GROUP_START;
import static io.pvdnc.sio.protobuf.utils.ProtoBufWireType.VARINT;
import static io.pvdnc.sio.protobuf.utils.ProtoBufWireType._32BIT;
import static io.pvdnc.sio.protobuf.utils.ProtoBufWireType._64BIT;
import static java.nio.charset.StandardCharsets.UTF_8;

public class ProtoBufSerializer implements PSerializer {
    public static final String MEDIA_TYPE = "application/x-protobuf";

    private final static int MAX_BUFFER_SIZE = 1 << 24;

    private final boolean strict;

    public ProtoBufSerializer() {
        this(false);
    }

    public ProtoBufSerializer(boolean strict) {
        this.strict = strict;
    }

    @Override
    public List<String> mediaTypes() {
        return List.of(MEDIA_TYPE);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <M extends PMessage> M readFrom(PMessageDescriptor<M> descriptor, InputStream in) throws IOException {
        return (M) readMessage(descriptor, new LittleEndianBinaryInputStream(in));
    }

    @Override
    public void writeTo(PMessage message, OutputStream out) throws IOException {
        writeMessage(message, new LittleEndianBinaryOutputStream(out));
        out.flush();
    }

    private PMessage readMessage(PMessageDescriptor<?> descriptor, LittleEndianBinaryInputStream in) throws IOException {
        PMessageBuilder builder = descriptor.newBuilder();
        while (in.available() > 0) {
            int tag = in.expectIntBase128();
            int fieldId = tag >> 3;
            byte fieldType = (byte) (tag & 0x7);

            PField field = descriptor.findFieldById(fieldId);
            if (field == null) {
                consumeField(fieldType, in);
            } else {
                readField(builder, field, fieldType, in);
            }
        }
        return builder.build();
    }

    private void readField(PMessageBuilder into, PField field, byte wireType, LittleEndianBinaryInputStream in) throws IOException {
        ProtoBufValueType valueType = field.getProperty(PROTO_TYPE);

        if (field.getType() == PType.MAP) {
            // repeated struct{1: keyType key, 2: valueType value}, never packed.
            @SuppressWarnings("unchecked")
            PMapDescriptor<Object, Object> mapDescriptor = (PMapDescriptor<Object, Object>) field.getResolvedDescriptor();
            Map<Object,Object> oldMap = into.get(field.getId());
            MapBuilder<Object,Object> map = mapDescriptor.builder(oldMap.size() + 1);
            map.putAll(oldMap);

            Binary buffer = readFieldBuffer(in);
            LittleEndianBinaryInputStream bin = new LittleEndianBinaryInputStream(buffer.getInputStream());

            ProtoBufValueType keyType = field.getProperty(PROTO_KEY_TYPE);
            bin.readIntBase128(); // 1 and wire-type. TODO: Validate with keyType.
            Object key = readFieldValue(mapDescriptor.getKeyType(), keyType, bin);

            bin.readIntBase128(); // 2 and wire-type. TODO: Validate with valueType.
            Object value = readFieldValue(mapDescriptor.getValueType(), valueType, bin);

            map.put(key, value);
            into.set(field.getId(), map.build());
            return;
        }

        boolean packed = wireType == BUFFER && field.getProperty(PROTO_PACKED);
        if (!packed && wireType != valueType.wire) {
            consumeField(wireType, in);
            return;
        }

        if (field.getType() == PType.LIST) {
            PListDescriptor<?> listDescriptor = (PListDescriptor<?>) field.getResolvedDescriptor();
            List<Object> oldList = into.get(field.getId());
            ListBuilder<Object> list = UnmodifiableList.newBuilder(oldList.size() + 1);
            list.addAll(oldList);

            if (packed) {
                // BUFFER with individual values using default value type.
                Binary buffer = readFieldBuffer(in);
                LittleEndianBinaryInputStream bin = new LittleEndianBinaryInputStream(buffer.getInputStream());
                while (bin.available() > 0) {
                    list.add(readFieldValue(listDescriptor.getItemType(), valueType, bin));
                }
            } else {
                // single added entry.
                list.add(readFieldValue(listDescriptor.getItemType(), valueType, in));
            }
            into.set(field.getId(), list.build());
        } else if (field.getType() == PType.SET) {
            @SuppressWarnings("unchecked")
            PSetDescriptor<Object> setDescriptor = (PSetDescriptor<Object>) field.getResolvedDescriptor();
            Set<Object> oldSet = into.get(field.getId());
            SetBuilder<Object> set = setDescriptor.builder(oldSet.size() + 1);
            set.addAll(oldSet);

            if (packed) {
                // BUFFER with individual values using default value type.
                Binary buffer = readFieldBuffer(in);
                LittleEndianBinaryInputStream bin = new LittleEndianBinaryInputStream(buffer.getInputStream());
                while (bin.available() > 0) {
                    set.add(readFieldValue(setDescriptor.getItemType(), valueType, bin));
                }
            } else {
                // single added entry.
                set.add(readFieldValue(setDescriptor.getItemType(), valueType, in));
            }
            into.set(field.getId(), set.build());
        } else {
            into.set(field.getId(), readFieldValue(field.getResolvedDescriptor(), valueType, in));
        }
    }

    private Binary readFieldBuffer(LittleEndianBinaryInputStream in) throws IOException {
        int len = in.expectIntBase128();
        if (len > MAX_BUFFER_SIZE) {
            throw new IOException("buffer with too many bytes, limit at " + MAX_BUFFER_SIZE + " (1 << 24).");
        }
        return Binary.read(in, len);
    }

    private Object readFieldValue(PDescriptor descriptor, ProtoBufValueType valueType, LittleEndianBinaryInputStream in) throws IOException {
        switch (descriptor.getType()) {
            case ANY: {
                // buffer.
                AnyProtoBuf any = (AnyProtoBuf) readFieldValue(AnyProtoBuf.kDescriptor, ProtoBufValueType.BYTES, in);
                return AnyBinary.newBuilder()
                        .setTypeName(any.getTypeName().replaceAll(".*type\\.googleapis\\.com/", ""))
                        .setMediaType(MEDIA_TYPE)
                        .setData(any.getData())
                        .build();
            }
            case VOID:
            case BOOL: {
                // varint
                return in.expectIntBase128() != 0;
            }
            case BYTE: {
                // sint32
                return (byte) in.expectIntZigzag();
            }
            case SHORT: {
                // sint32
                return (short) in.expectIntZigzag();
            }
            case INT: {
                switch (valueType) {
                    case INT32:
                    case UINT32: {
                        return in.expectIntBase128();
                    }
                    case SINT32: {
                        return in.expectIntZigzag();
                    }
                    case FIXED32:
                    case SFIXED32: {
                        return in.expectInt();
                    }
                }
                throw new IOException("Bad type for INT: " + valueType);
            }
            case LONG: {
                switch (valueType) {
                    case INT64:
                    case UINT64: {
                        return in.expectLongBase128();
                    }
                    case SINT64: {
                        return in.expectLongZigzag();
                    }
                    case FIXED64:
                    case SFIXED64: {
                        return in.expectLong();
                    }
                }
                throw new IOException("Bad type for LONG: " + valueType);
            }
            case FLOAT: {
                // fixed32
                return Float.intBitsToFloat(in.expectInt());
            }
            case DOUBLE: {
                // fixed64
                return Double.longBitsToDouble(in.expectLong());
            }
            case STRING: {
                // buffer
                return readFieldBuffer(in).decodeToString(UTF_8);
            }
            case BINARY: {
                return readFieldBuffer(in);
            }
            case ENUM: {
                PEnumDescriptor enumDescriptor = (PEnumDescriptor) descriptor;
                int number = in.expectIntBase128();
                if (strict) {
                    return enumDescriptor.valueForId(number);
                } else {
                    return enumDescriptor.findById(number);
                }
            }
            case MESSAGE: {
                Binary buffer = readFieldBuffer(in);
                LittleEndianBinaryInputStream bin = new LittleEndianBinaryInputStream(buffer.getInputStream());
                @SuppressWarnings("rawtypes")
                PMessageDescriptor messageDescriptor = (PMessageDescriptor) descriptor;
                return readMessage(messageDescriptor, bin);
            }
            default: {
                throw new IOException("Unsupported singular value type: " + descriptor);
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void consumeField(byte fieldType, LittleEndianBinaryInputStream in) throws IOException {
        switch (fieldType) {
            // same as v3, but if field is GROUP_START, will skipp all fields until GROUP_END.
            case _32BIT: {
                in.expectInt();
                break;
            }
            case _64BIT: {
                in.expectLong();
                break;
            }
            case VARINT: {
                in.expectLongBase128();
                break;
            }
            case BUFFER: {
                int len = in.expectIntBase128();
                in.expectBytes(len);
                break;
            }
            case GROUP_START: {
                for (;;) {
                    // skip all fields until GROUP_END.
                    int readFieldSpec = in.readIntBase128();
                    byte groupFieldType = (byte) (readFieldSpec % 0x7);
                    if (groupFieldType == GROUP_END) {
                        break;
                    }
                    consumeField(groupFieldType, in);
                }
                break;
            }
            default: {  // dangling group end
                // ignore, no content.
                break;
            }
        }
    }

    private void writeMessage(PMessage message, LittleEndianBinaryOutputStream out) throws IOException {
        for (PField field : message.$descriptor().allFields()) {
            if (message.has(field.getId())) {
                writeFieldValue(field, message.get(field.getId()), out);
            }
        }
    }

    private void writeFieldValue(PField field, Object value, LittleEndianBinaryOutputStream out) throws IOException {
        if (field.getId() < 1) {
            // Unable to write field to proto serialization with negative ID, ignored.
            return;
        }
        if (Objects.equals(field
                .getDefaultValue()
                .orElseGet(() -> field.getType()
                        .getDefaultValue()
                        .orElse(null)),
                value)) {
            // in proto the default value is not written, even if explicitly set.
            return;
        }
        ProtoBufValueType pbType = field.getProperty(PROTO_TYPE);
        switch (field.getType()) {
            case LIST:
            case SET: {
                PDescriptor itemType = getItemType(field.getResolvedDescriptor());
                Collection<?> collection = (Collection<?>) value;
                if (collection.isEmpty()) {
                    // do not write empty collections.
                    break;
                }
                if (field.getProperty(PROTO_PACKED)) {
                    // only numeric primitives may be packed!
                    out.writeBase128(field.getId() << 3 | BUFFER);
                    try (FramedOutputStream fos = new FramedOutputStream(out, this::writeBufferLength);
                         LittleEndianBinaryOutputStream os = new LittleEndianBinaryOutputStream(fos)) {
                        // The ACTUAL wire type is implicit, and just assumed to match the
                        // field annotation or type default.
                        for (Object item : collection) {
                            writeFieldValue(itemType, pbType, item, os);
                        }
                    }
                } else {
                    for (Object item : collection) {
                        out.writeBase128(field.getId() << 3 | pbType.wire);
                        writeFieldValue(itemType, pbType, item, out);
                    }
                }
                break;
            }
            case MAP: {
                Map<?, ?> map = ((Map<?, ?>) value);
                PDescriptor keyType = ((PMapDescriptor<?, ?>) field.getResolvedDescriptor()).getKeyType();
                PDescriptor valueType = ((PMapDescriptor<?, ?>) field.getResolvedDescriptor()).getValueType();
                ProtoBufValueType pbKeyType = field.getProperty(PROTO_KEY_TYPE);

                for (Map.Entry<?, ?> entry : map.entrySet()) {
                    out.writeBase128(field.getId() << 3 | BUFFER);
                    try (FramedOutputStream bos = new FramedOutputStream(out, this::writeBufferLength);
                         LittleEndianBinaryOutputStream os = new LittleEndianBinaryOutputStream(bos)) {
                        os.writeBase128(1 << 3 | pbKeyType.wire);
                        writeFieldValue(keyType, pbKeyType, entry.getKey(), os);
                        os.writeBase128(2 << 3 | pbType.wire);
                        writeFieldValue(valueType, pbType, entry.getValue(), os);
                    }
                }
                break;
            }
            case ANY: {
                PAny any = (PAny) value;
                AnyBinary anyBinary = any.pack(this);
                AnyProtoBuf anyProtoBuf = AnyProtoBuf.newBuilder()
                        .setTypeName("type.googleapis.com/" + anyBinary.getTypeName())
                        .setData(anyBinary.getData())
                        .build();

                out.writeBase128(field.getId() << 3 | pbType.wire);
                writeFieldValue(
                        AnyBinary.kDescriptor,
                        pbType,
                        anyProtoBuf,
                        out);
                break;
            }
            default: {
                out.writeBase128(field.getId() << 3 | pbType.wire);
                writeFieldValue(
                        field.getResolvedDescriptor(),
                        pbType,
                        value,
                        out);
                break;
            }
        }
    }

    private void writeBufferLength(OutputStream os, int size) throws IOException {
        new LittleEndianBinaryOutputStream(os).writeBase128(size);
    }

    private void writeFieldValue(PDescriptor pDescriptor, ProtoBufValueType pbType, Object value, LittleEndianBinaryOutputStream out) throws IOException {
        switch (pDescriptor.getType()) {
            case VOID: {
                if (pbType.wire != VARINT) {
                    throw new IOException("Unknown void encoding " + pbType);
                }
                out.writeBase128(1);
                break;
            }
            case BOOL: {
                if (pbType.wire != VARINT) {
                    throw new IOException("Unknown bool encoding " + pbType);
                }
                out.writeBase128((Boolean) value ? 1 : 0);
                break;
            }
            case BYTE:
            case SHORT:
            case INT: {
                int val = ((Number) value).intValue();
                switch (pbType) {
                    case INT32:
                    case UINT32:{
                        out.writeBase128(val);
                        break;
                    }
                    case SINT32: {
                        out.writeZigzag(val);
                        break;
                    }
                    case FIXED32:
                    case SFIXED32: {
                        out.writeInt(val);
                        break;
                    }
                    default:
                        throw new IOException("Unknown " + pDescriptor.getType() + " encoding " + pbType);
                }
                break;
            }
            case LONG: {
                long val = (Long) value;
                switch (pbType) {
                    case INT64:
                    case UINT64:{
                        out.writeBase128(val);
                        break;
                    }
                    case SINT64: {
                        out.writeZigzag(val);
                        break;
                    }
                    case FIXED64:
                    case SFIXED64: {
                        out.writeLong(val);
                        break;
                    }
                    default:
                        throw new IOException("Unknown long encoding " + pbType);
                }
                break;
            }
            case FLOAT: {
                if (pbType.wire != _32BIT) {
                    throw new IOException("Unknown float encoding " + pbType);
                }
                out.writeUInt32(Float.floatToRawIntBits((Float) value));
                break;
            }
            case DOUBLE: {
                if (pbType.wire != _64BIT) {
                    throw new IOException("Unknown double encoding " + pbType);
                }
                out.writeLong(Double.doubleToRawLongBits((Double) value));
                break;
            }
            case STRING: {
                // STRING or BYTES
                if (pbType.wire != BUFFER) {
                    throw new IOException("Unknown string encoding " + pbType);
                }
                byte[] data = value.toString().getBytes(UTF_8);
                out.writeBase128(data.length);
                out.write(data);
                break;
            }
            case BINARY: {
                // BYTES
                if (pbType.wire != BUFFER) {
                    throw new IOException("Unknown binary encoding " + pbType);
                }
                Binary bin = (Binary) value;
                out.write(bin.length());
                bin.write(out);
                break;
            }
            case ENUM: {
                if (pbType.wire == VARINT) {
                    out.writeBase128(((PEnum) value).getValue());
                } else {
                    throw new IOException("Unknown enum encoding " + pbType);
                }
                break;
            }
            case MESSAGE: {
                if (pbType.wire != BUFFER) {
                    throw new IOException("Unknown message encoding " + pbType);
                }
                try (FramedOutputStream fos = new FramedOutputStream(out, this::writeBufferLength)) {
                    writeTo((PMessage) value, fos);
                }
                break;
            }
            default: {
                throw new IOException("Unhandled value type " + pDescriptor.getTypeName());
            }
        }
    }

    @Override
    public boolean isStrict() {
        return strict;
    }

    @Override
    public ProtoBufSerializer strict() {
        return strict ? this : new ProtoBufSerializer(true);
    }
}
