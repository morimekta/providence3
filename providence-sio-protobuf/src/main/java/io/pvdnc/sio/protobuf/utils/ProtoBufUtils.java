/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.protobuf.utils;

import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PSetDescriptor;

public final class ProtoBufUtils {
    public static ProtoBufValueType defaultValueTypeFor(PDescriptor descriptor) {
        if (descriptor instanceof PListDescriptor) {
            return defaultValueTypeFor(((PListDescriptor<?>) descriptor).getItemType());
        } else if (descriptor instanceof PSetDescriptor) {
            return defaultValueTypeFor(((PSetDescriptor<?>) descriptor).getItemType());
        } else if (descriptor instanceof PMapDescriptor) {
            return defaultValueTypeFor(((PMapDescriptor<?,?>) descriptor).getValueType());
        }
        return ProtoBufValueType.defaultValueType(descriptor.getType());
    }

    public static ProtoBufValueType defaultKeyTypeFor(PDescriptor descriptor) {
        if (descriptor instanceof PMapDescriptor) {
            return defaultValueTypeFor(((PMapDescriptor<?,?>) descriptor).getKeyType());
        }
        return ProtoBufValueType.defaultValueType(descriptor.getType());
    }

    public static PDescriptor getItemType(PDescriptor descriptor) {
        if (descriptor instanceof PListDescriptor) {
            return ((PListDescriptor<?>) descriptor).getItemType();
        }
        if (descriptor instanceof PSetDescriptor) {
            return ((PSetDescriptor<?>) descriptor).getItemType();
        }
        throw new IllegalArgumentException("Not a collection type.");
    }

    private ProtoBufUtils() {}
}
