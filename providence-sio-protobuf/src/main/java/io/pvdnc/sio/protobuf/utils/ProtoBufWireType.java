/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.protobuf.utils;


public final class ProtoBufWireType {
    /** A varint / LEB128 encoded number. */
    public static final byte VARINT = 0;
    /** 64 bits (8 bytes) */
    public static final byte _64BIT = 1;
    /** A length delimited buffer. */
    public static final byte BUFFER = 2;
    @Deprecated(since = "proto3")
    public static final byte GROUP_START = 3;
    @Deprecated(since = "proto3")
    public static final byte GROUP_END = 4;
    /** 32 bits (4 bytes) */
    public static final byte _32BIT = 5;

    private ProtoBufWireType() {}
}
