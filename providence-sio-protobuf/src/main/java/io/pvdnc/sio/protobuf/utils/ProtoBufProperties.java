/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.sio.protobuf.utils;

import io.pvdnc.core.property.PPersistedProperty;
import io.pvdnc.core.property.PProperty;
import io.pvdnc.core.property.PPropertyHolder;
import io.pvdnc.core.property.PPropertyTarget;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PType;

public class ProtoBufProperties {
    /**
     * The proto type of a field. For list and set fields this is the type of the
     * individual value.
     */
    public static final PProperty<ProtoBufValueType> PROTO_TYPE;
    /**
     * The proto type of a map key. Will only have any effect on map fields. Otherwise
     * will have the same restriction as the 'proto.type' annotation.
     */
    public static final PProperty<ProtoBufValueType> PROTO_KEY_TYPE;
    /**
     * If a repeated (list or set) field should be packed. Only repeated
     * numeric values may be packed. Packing any length-delimited type will lead to
     * badly encoded content.
     */
    public static final PProperty<Boolean> PROTO_PACKED;

    static {
        PROTO_TYPE = new PPersistedProperty<>("proto.type", ProtoBufValueType.class, PPropertyTarget.MESSAGE_FIELD) {
            @Override
            public ProtoBufValueType parseString(String value) {
                return ProtoBufValueType.forName(value);
            }
            @Override
            public String stringify(ProtoBufValueType value) {
                return value.name;
            }
            @Override
            public ProtoBufValueType defaultValueFor(PPropertyHolder instance) {
                if (instance instanceof PField) {
                    return ProtoBufUtils.defaultValueTypeFor(((PField) instance).getResolvedDescriptor());
                }
                return super.defaultValueFor(instance);
            }
        };
        PROTO_KEY_TYPE = new PPersistedProperty<>("proto.key.type", ProtoBufValueType.class, PPropertyTarget.MESSAGE_FIELD) {
            @Override
            public ProtoBufValueType parseString(String value) {
                return ProtoBufValueType.forName(value);
            }
            @Override
            public String stringify(ProtoBufValueType value) {
                return value.name;
            }
            @Override
            public ProtoBufValueType defaultValueFor(PPropertyHolder instance) {
                if (instance instanceof PField) {
                    return ProtoBufUtils.defaultKeyTypeFor(((PField) instance).getResolvedDescriptor());
                }
                return super.defaultValueFor(instance);
            }
        };
        PROTO_PACKED = new PPersistedProperty<>("proto.packed", Boolean.class, Boolean.FALSE, PPropertyTarget.MESSAGE_FIELD) {
            @Override
            public Boolean defaultValueFor(PPropertyHolder instance) {
                if (instance instanceof PField) {
                    PField field = (PField) instance;
                    if (field.getType() != PType.LIST && field.getType() != PType.SET) {
                        return false;
                    }
                    PDescriptor itemType = ProtoBufUtils.getItemType(field.getResolvedDescriptor());
                    switch (itemType.getType()) {
                        case BOOL:
                        case BYTE:
                        case SHORT:
                        case INT:
                        case LONG:
                        case FLOAT:
                        case DOUBLE:
                        case ENUM:
                            return true;
                    }
                    return false;
                }
                return super.defaultValueFor(instance);
            }
        };
    }
}
