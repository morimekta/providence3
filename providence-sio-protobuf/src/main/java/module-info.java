import io.pvdnc.core.io.PSerializer;
import io.pvdnc.sio.protobuf.ProtoBufSerializer;

module io.pvdnc.sio.protobuf {
    exports io.pvdnc.sio.protobuf;
    exports io.pvdnc.sio.protobuf.utils;

    requires transitive io.pvdnc.core;
    requires net.morimekta.collect;
    requires net.morimekta.strings;
    requires net.morimekta.io;

    provides PSerializer with ProtoBufSerializer;
}