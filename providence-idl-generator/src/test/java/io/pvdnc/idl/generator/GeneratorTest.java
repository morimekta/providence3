package io.pvdnc.idl.generator;

import io.pvdnc.core.PMessageBuilder;
import io.pvdnc.idl.generator.util.JavaProperties;
import net.morimekta.collect.util.Binary;
import net.morimekta.file.FileUtil;
import io.pvdnc.core.impl.Empty;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.reflect.CEnumDescriptor;
import io.pvdnc.core.reflect.CMessageDescriptor;
import io.pvdnc.core.reflect.CServiceDescriptor;
import io.pvdnc.core.registry.PGlobalFileTypeRegistry;
import io.pvdnc.core.registry.PNamespaceFileTypeRegistry;
import io.pvdnc.core.types.PConstDescriptor;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PMessageVariant;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.types.PType;
import io.pvdnc.core.types.PTypeDefDescriptor;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableSortedMap.sortedMapOf;
import static net.morimekta.collect.UnmodifiableSortedSet.sortedSetOf;
import static io.pvdnc.core.property.PDefaultProperties.DOCUMENTATION;
import static io.pvdnc.core.property.PDefaultProperties.MESSAGE_VARIANT;
import static io.pvdnc.core.registry.PTypeReference.ref;

public class GeneratorTest {
    @TempDir
    public static Path source;

    public static Path outputDir;

    public static PGlobalFileTypeRegistry global;
    public static PNamespaceFileTypeRegistry otherRegistry;
    public static PNamespaceFileTypeRegistry testRegistry;

    public static CEnumDescriptor otherEnum;
    public static CMessageDescriptor otherInterface;
    public static CMessageDescriptor otherException;
    public static CMessageDescriptor otherMessage;
    public static CMessageDescriptor otherUnion;
    public static CMessageDescriptor testMessage;

    @BeforeAll
    public static void setUpAll() throws IOException {
        if (Files.isDirectory(Path.of("providence-idl-generator"))) {
            outputDir = FileUtil.readCanonicalPath(Path.of("providence-idl-generator/target/providence"));
        } else {
            outputDir = FileUtil.readCanonicalPath(Path.of("target/providence"));
        }
        FileUtil.deleteRecursively(outputDir);
        Files.createDirectories(outputDir);

        CMessageDescriptor.Builder message;

        global = new PGlobalFileTypeRegistry();
        otherRegistry = global.computeRegistryForPath(source.resolve("other.pvd"), "other");
        otherRegistry.setProperties(PPropertyMap.of(JavaProperties.JAVA_PACKAGE_NAME,
                                                    "io.pvdnc.idl.generator.test.other"));

        otherEnum = CEnumDescriptor.builder("other", "other_enum")
                                   .value("first", 1)
                                   .value("Second", 2)
                                   .value("THIRD", 3)
                                   .value("four_th", 5, PPropertyMap.of("json.name", "fourth"))
                                   .value("FiFth", 8, PPropertyMap.of(
                                           "documentation", "Keep away",
                                           "deprecated", "Yes!"))
                                   .property("documentation", "Documented!")
                                   .build();
        otherRegistry.register(otherEnum);

        message = CMessageDescriptor.builder("other", "OtherUnion");
        message.property(MESSAGE_VARIANT, PMessageVariant.UNION);
        message.field("foo", PType.STRING).id(1);
        message.field("bar", PType.DOUBLE.provider()).id(2);
        message.field("baz", PType.SHORT).id(3);
        message.field("baltazar", PType.BINARY).id(4).defaultValue(() -> Binary.fromBase64("baltazar"));
        otherUnion = message.build();
        otherRegistry.register(otherUnion);

        message = CMessageDescriptor.builder("other", "OtherInterface");
        message.property(MESSAGE_VARIANT, PMessageVariant.INTERFACE);
        message.field("foo", PListDescriptor.provider(PType.STRING.provider()));
        message.field("bar", PSetDescriptor.sortedProvider(PType.DOUBLE.provider()));
        message.field("fizz", PMapDescriptor.provider(PType.SHORT.provider(), PType.STRING.provider()));
        message.field("buzz", otherUnion);
        otherInterface = message.build();
        otherRegistry.register(otherInterface);

        message = CMessageDescriptor.builder("other", "OtherMessage");
        message.property(DOCUMENTATION,
                         "Also documented.\n" +
                         "\n" +
                         "This time with a multi-\n" +
                         "line, but short string.\n" +
                         "æøå");
        message.implementing(() -> otherInterface);
        // TODO: Properly handle void fields.
        message.field("v", PType.VOID.provider()).id(1);
        message.field("b", PType.BOOL.provider()).id(2);
        message.field("bt", PType.BYTE.provider()).id(3);
        message.field("s", PType.SHORT.provider()).id(4).property("one.of", "ints");
        message.field("i", PType.INT.provider()).id(5).property("one.of", "ints");
        message.field("l", PType.LONG.provider()).id(6).property("one.of", "ints");
        message.field("f", PType.FLOAT.provider()).id(7);
        message.field("d", PType.DOUBLE.provider()).id(8);
        message.field("e", otherEnum).id(9);
        message.field("str", PType.STRING.provider()).id(10);
        message.field("bin", PType.BINARY.provider()).id(11);
        message.field("foo", PListDescriptor.provider(PType.STRING.provider())).id(12);
        message.field("bar", PSetDescriptor.sortedProvider(PType.DOUBLE.provider())).id(13);
        message.field("fizz", PMapDescriptor.provider(PType.SHORT.provider(), PType.STRING.provider())).id(14);
        message.field("buzz", otherUnion).id(15);
        message.field("any", PType.ANY.provider()).id(16);

        otherMessage = message.build();
        otherRegistry.register(otherMessage);

        message = CMessageDescriptor.builder("other", "OtherException");
        message.property(MESSAGE_VARIANT, PMessageVariant.EXCEPTION);
        message.field("text", PType.STRING).id(1);
        otherException = message.build();
        otherRegistry.register(otherException);

        // --- Testing ---

        testRegistry = global.computeRegistryForPath(source.resolve("test.pvd"), "test");
        testRegistry.setProperties(PPropertyMap.of(JavaProperties.JAVA_PACKAGE_NAME,
                                                    "io.pvdnc.idl.generator.test"));
        testRegistry.putInclude("other", otherRegistry);
        testRegistry.register(new PTypeDefDescriptor("test", "OtherEnumReally",
                PPropertyMap.of("json.name", "Other"), () -> testRegistry.descriptor(ref("other", "other_enum"))));

        CServiceDescriptor.Builder service = CServiceDescriptor.newBuilder("test", "MyService");
        service.method("ping", Empty.kDescriptor, Empty.kDescriptor, PPropertyMap.of());
        testRegistry.register(service.build());

        testRegistry.register(new PConstDescriptor(
                "test", "kBool",
                PType.BOOL.provider(),
                () -> true));
        testRegistry.register(new PConstDescriptor(
                "test", "kByte",
                PType.BYTE.provider(),
                () -> (byte) 42));
        testRegistry.register(new PConstDescriptor(
                "test", "kShort",
                PType.SHORT.provider(),
                () -> (short) 4200));
        testRegistry.register(new PConstDescriptor(
                "test", "kInt",
                PType.INT.provider(),
                () -> 420000));
        testRegistry.register(new PConstDescriptor(
                "test", "kLong",
                PType.LONG.provider(),
                () -> 4200000000L));
        testRegistry.register(new PConstDescriptor(
                "test", "kFloat",
                PType.FLOAT.provider(),
                () -> 42.4242f));
        testRegistry.register(new PConstDescriptor(
                "test", "kDouble",
                PType.DOUBLE.provider(),
                () -> 42.42424242));
        testRegistry.register(new PConstDescriptor(
                "test", "kString",
                PType.STRING.provider(),
                () -> "fizzbuzz"));
        testRegistry.register(new PConstDescriptor(
                "test", "kBinary",
                PType.BINARY.provider(),
                () -> Binary.fromBase64("fizzbuzz")));
        testRegistry.register(new PConstDescriptor(
                "test", "kEnum",
                () -> otherEnum,
                () -> otherEnum.valueForId(1)));

        testRegistry.register(new PConstDescriptor(
                "test", "kList",
                PListDescriptor.provider(PType.STRING.provider()),
                () -> listOf("foo", "bar")));
        testRegistry.register(new PConstDescriptor(
                "test", "kSet",
                PSetDescriptor.sortedProvider(PType.STRING.provider()),
                () -> sortedSetOf("foo", "bar")));
        testRegistry.register(new PConstDescriptor(
                "test", "kSet",
                PMapDescriptor.sortedProvider(PType.STRING.provider(), PType.STRING.provider()),
                () -> sortedMapOf("foo", "bar", "fizz", "buzz")));

        PMessageBuilder builder = otherMessage.newBuilder();
        builder.set(1, true);
        builder.set(2, true);
        builder.set(10, "foo");
        testRegistry.register(new PConstDescriptor(
                "test", "kMessage",
                () -> otherMessage,
                builder::build));
    }

    @Test
    public void testSimple() throws IOException {
        GeneratorOptions options = new GeneratorOptions();
        GeneratorRunner runner = new GeneratorRunner(outputDir, global, options);
        runner.runGenerators(otherRegistry);
        runner.runGenerators(testRegistry);
    }
}
