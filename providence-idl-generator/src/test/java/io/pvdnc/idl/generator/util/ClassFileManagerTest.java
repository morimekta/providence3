package io.pvdnc.idl.generator.util;

import net.morimekta.file.PathUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.file.Path;

import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.io.FileMatchers.anExistingDirectory;
import static org.junit.jupiter.api.Assertions.fail;

public class ClassFileManagerTest {
    @TempDir
    public Path root;

    @Test
    public void testGetClassFile() throws IOException {
        ClassFileManager manager = new ClassFileManager(root);
        Path file = manager.getFileForClass("io.pvdnc.Foo");
        assertThat(file, is(notNullValue()));
        assertThat(PathUtil.getFileName(file), is("Foo.java"));
        Path parent = file.getParent();
        assertThat(parent, is(notNullValue()));
        assertThat(parent.toString(), startsWith(root.toString()));
        assertThat(parent.toString(), endsWith("/io/pvdnc"));
        assertThat(parent.toFile(), is(anExistingDirectory()));
        assertThat(manager.getRoot(), is(root));
    }

    @Test
    public void testBadArgs() throws IOException {
        ClassFileManager manager = new ClassFileManager(root);
        try {
            manager.getFileForClass(null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("instanceName == null"));
        }
        try {
            manager.getFileForClass("does..not..match");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Invalid package and class name: does..not..match"));
        }
    }
}
