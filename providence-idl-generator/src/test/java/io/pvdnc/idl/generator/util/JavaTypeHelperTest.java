package io.pvdnc.idl.generator.util;

import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.reflect.CEnumDescriptor;
import io.pvdnc.core.reflect.CMessageDescriptor;
import io.pvdnc.core.registry.PGlobalFileTypeRegistry;
import io.pvdnc.core.registry.PNamespaceFileTypeRegistry;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.types.PType;
import io.pvdnc.idl.generator.GeneratorOptions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Path;

import static io.pvdnc.core.registry.PTypeReference.ref;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;

public class JavaTypeHelperTest {
    @TempDir
    public Path temp;
    private PGlobalFileTypeRegistry global;
    private PNamespaceFileTypeRegistry registry;
    private PNamespaceFileTypeRegistry include;
    private GeneratorOptions options;
    private JavaTypeHelper   helper;

    @BeforeEach
    public void setUp() {
        global = new PGlobalFileTypeRegistry();
        registry = global.computeRegistryForPath(temp.resolve("foo.json"), "foo");
        registry.setProperties(PPropertyMap.of(JavaProperties.JAVA_PACKAGE_NAME, "net.morimekta.foo"));
        registry.register(CEnumDescriptor.builder("foo", "TestEnum").build());
        registry.register(CMessageDescriptor.builder("foo", "TestMessage").build());
        include = global.computeRegistryForPath(temp.resolve("bar.json"), "bar");
        include.setProperties(PPropertyMap.of(JavaProperties.JAVA_PACKAGE_NAME, "net.morimekta.bar"));
        include.register(CEnumDescriptor.builder("bar", "EnumTest").build());
        include.register(CMessageDescriptor.builder("bar", "MessageTest").build());
        registry.putInclude("bar", include);
        options = new GeneratorOptions();
        helper = new JavaTypeHelper(global, registry, options, "Foo");
    }

    @Test
    public void testProperties() {
        assertThat(helper.getJavaClassName(), is("Foo"));
        assertThat(helper.getJavaPackage(), is("net.morimekta.foo"));
        assertThat(helper.getOptions(), is(sameInstance(options)));
        assertThat(helper.getRegistry(), is(sameInstance(registry)));
    }

    @Test
    public void testGetValueType() {
        assertThat(helper.getValueType(PType.VOID), is("void"));
        assertThat(helper.getValueType(PType.BOOL), is("boolean"));
        assertThat(helper.getValueType(PType.BYTE), is("byte"));
        assertThat(helper.getValueType(PType.SHORT), is("short"));
        assertThat(helper.getValueType(PType.INT), is("int"));
        assertThat(helper.getValueType(PType.LONG), is("long"));
        assertThat(helper.getValueType(PType.FLOAT), is("float"));
        assertThat(helper.getValueType(PType.DOUBLE), is("double"));
        assertThat(helper.getValueType(PType.STRING), is("String"));
        assertThat(helper.getValueType(PType.BINARY), is("net.morimekta.collect.util.Binary"));
        assertThat(helper.getValueType(new PListDescriptor<>(PType.SHORT.provider())), is("java.util.List<Short>"));
        assertThat(helper.getValueType(new PSetDescriptor<>(PType.SHORT.provider(), true)), is("java.util.SortedSet<Short>"));
        assertThat(helper.getValueType(new PSetDescriptor<>(PType.SHORT.provider(), false)), is("java.util.Set<Short>"));
        assertThat(helper.getValueType(new PMapDescriptor<>(PType.SHORT.provider(), PType.LONG.provider(), true)), is("java.util.SortedMap<Short,Long>"));
        assertThat(helper.getValueType(new PMapDescriptor<>(PType.SHORT.provider(), PType.LONG.provider(), false)), is("java.util.Map<Short,Long>"));
        assertThat(helper.getValueType(registry.descriptor(ref("TestEnum"))), is("TestEnum"));
        assertThat(helper.getValueType(registry.descriptor(ref("bar", "EnumTest"))), is("net.morimekta.bar.EnumTest"));
        assertThat(helper.getValueType(registry.descriptor(ref("TestMessage"))), is("TestMessage"));
        assertThat(helper.getValueType(registry.descriptor(ref("bar", "MessageTest"))), is("net.morimekta.bar.MessageTest"));
    }

    @Test
    public void testGetValueClass() {
        assertThat(helper.getValueClass(PType.VOID), is("Boolean"));
        assertThat(helper.getValueClass(PType.BOOL), is("Boolean"));
        assertThat(helper.getValueClass(PType.BYTE), is("Byte"));
        assertThat(helper.getValueClass(PType.SHORT), is("Short"));
        assertThat(helper.getValueClass(PType.INT), is("Integer"));
        assertThat(helper.getValueClass(PType.LONG), is("Long"));
        assertThat(helper.getValueClass(PType.FLOAT), is("Float"));
        assertThat(helper.getValueClass(PType.DOUBLE), is("Double"));
        assertThat(helper.getValueClass(PType.STRING), is("String"));
        assertThat(helper.getValueClass(PType.BINARY), is("net.morimekta.collect.util.Binary"));
        assertThat(helper.getValueClass(new PListDescriptor<>(PType.SHORT.provider())), is("java.util.List<Short>"));
        assertThat(helper.getValueClass(new PSetDescriptor<>(PType.SHORT.provider(), true)), is("java.util.SortedSet<Short>"));
        assertThat(helper.getValueClass(new PSetDescriptor<>(PType.SHORT.provider(), false)), is("java.util.Set<Short>"));
        assertThat(helper.getValueClass(new PMapDescriptor<>(PType.SHORT.provider(), PType.LONG.provider(), true)), is("java.util.SortedMap<Short,Long>"));
        assertThat(helper.getValueClass(new PMapDescriptor<>(PType.SHORT.provider(), PType.LONG.provider(), false)), is("java.util.Map<Short,Long>"));
        assertThat(helper.getValueClass(registry.descriptor(ref("TestEnum"))), is("TestEnum"));
        assertThat(helper.getValueClass(registry.descriptor(ref("bar", "EnumTest"))), is("net.morimekta.bar.EnumTest"));
        assertThat(helper.getValueClass(registry.descriptor(ref("TestMessage"))), is("TestMessage"));
        assertThat(helper.getValueClass(registry.descriptor(ref("bar", "MessageTest"))), is("net.morimekta.bar.MessageTest"));
    }

    @Test
    public void testGetSetterParamType() {
        assertThat(helper.getSetterParamType(PType.VOID), is("Boolean"));
        assertThat(helper.getSetterParamType(PType.BOOL), is("Boolean"));
        assertThat(helper.getSetterParamType(PType.BYTE), is("Byte"));
        assertThat(helper.getSetterParamType(PType.SHORT), is("Short"));
        assertThat(helper.getSetterParamType(PType.INT), is("Integer"));
        assertThat(helper.getSetterParamType(PType.LONG), is("Long"));
        assertThat(helper.getSetterParamType(PType.FLOAT), is("Float"));
        assertThat(helper.getSetterParamType(PType.DOUBLE), is("Double"));
        assertThat(helper.getSetterParamType(PType.STRING), is("String"));
        assertThat(helper.getSetterParamType(PType.BINARY), is("net.morimekta.collect.util.Binary"));
        assertThat(helper.getSetterParamType(new PListDescriptor<>(PType.SHORT.provider())), is("java.util.Collection<Short>"));
        assertThat(helper.getSetterParamType(new PSetDescriptor<>(PType.SHORT.provider(), true)), is("java.util.Collection<Short>"));
        assertThat(helper.getSetterParamType(new PMapDescriptor<>(PType.SHORT.provider(), PType.LONG.provider(), true)), is("java.util.Map<Short,Long>"));
        assertThat(helper.getSetterParamType(registry.descriptor(ref("TestEnum"))), is("TestEnum"));
        assertThat(helper.getSetterParamType(registry.descriptor(ref("bar", "EnumTest"))), is("net.morimekta.bar.EnumTest"));
        assertThat(helper.getSetterParamType(registry.descriptor(ref("TestMessage"))), is("TestMessage"));
        assertThat(helper.getSetterParamType(registry.descriptor(ref("bar", "MessageTest"))), is("net.morimekta.bar.MessageTest"));
    }

    @Test
    public void testGetDescriptorProvider() {
        assertThat(helper.getDescriptorProvider(PType.VOID),
                   is("PType.VOID.provider()"));
        assertThat(helper.getDescriptorProvider(PType.BOOL),
                   is("PType.BOOL.provider()"));
        assertThat(helper.getDescriptorProvider(PType.BYTE),
                   is("PType.BYTE.provider()"));
        assertThat(helper.getDescriptorProvider(PType.SHORT),
                   is("PType.SHORT.provider()"));
        assertThat(helper.getDescriptorProvider(PType.INT),
                   is("PType.INT.provider()"));
        assertThat(helper.getDescriptorProvider(PType.LONG),
                   is("PType.LONG.provider()"));
        assertThat(helper.getDescriptorProvider(PType.FLOAT),
                   is("PType.FLOAT.provider()"));
        assertThat(helper.getDescriptorProvider(PType.DOUBLE),
                   is("PType.DOUBLE.provider()"));
        assertThat(helper.getDescriptorProvider(PType.STRING),
                   is("PType.STRING.provider()"));
        assertThat(helper.getDescriptorProvider(PType.BINARY),
                   is("PType.BINARY.provider()"));
        assertThat(helper.getDescriptorProvider(new PListDescriptor<>(PType.SHORT.provider())),
                   is("PListDescriptor.provider(PType.SHORT.provider())"));
        assertThat(helper.getDescriptorProvider(new PSetDescriptor<>(PType.SHORT.provider(), true)),
                   is("PSetDescriptor.sortedProvider(PType.SHORT.provider())"));
        assertThat(helper.getDescriptorProvider(new PMapDescriptor<>(PType.SHORT.provider(), PType.LONG.provider(), true)),
                   is("PMapDescriptor.sortedProvider(PType.SHORT.provider(),PType.LONG.provider())"));
        assertThat(helper.getDescriptorProvider(registry.descriptor(ref("TestEnum"))),
                   is("TestEnum.provider()"));
        assertThat(helper.getDescriptorProvider(registry.descriptor(ref("bar", "EnumTest"))),
                   is("net.morimekta.bar.EnumTest.provider()"));
        assertThat(helper.getDescriptorProvider(registry.descriptor(ref("TestMessage"))),
                   is("TestMessage.provider()"));
        assertThat(helper.getDescriptorProvider(registry.descriptor(ref("bar", "MessageTest"))),
                   is("net.morimekta.bar.MessageTest.provider()"));
    }
}
