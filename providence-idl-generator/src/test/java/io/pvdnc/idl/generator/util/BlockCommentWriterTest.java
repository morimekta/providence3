package io.pvdnc.idl.generator.util;

import net.morimekta.strings.io.IndentedPrintWriter;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringWriter;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class BlockCommentWriterTest {
    @Test
    public void testBlockCommenter_Empty() {
        StringWriter writer = new StringWriter();
        IndentedPrintWriter iw = new IndentedPrintWriter(writer);
        try (BlockCommentWriter ignore = new BlockCommentWriter(iw)) {
            // deliberately empty.
        }

        assertThat(writer.getBuffer().toString(), is(""));
    }

    @Test
    public void testBlockCommenter_Simple() {
        StringWriter writer = new StringWriter();
        IndentedPrintWriter iw = new IndentedPrintWriter(writer);

        try (BlockCommentWriter comment = new BlockCommentWriter(iw)) {
            comment.comment("foo <b>FOO</b>\n\nbar")
                   .newline()
                   .param_("name", "Comment")
                   .return_("When nice")
                   .throws_(IOException.class.getName(), "When in a bad mood")
                   .deprecated_("Help!");
        }

        assertThat(writer.getBuffer().toString(),
                   is("\n" +
                      "/**\n" +
                      " * foo &lt;b&gt;FOO&lt;/b&gt;\n" +
                      " * <p>\n" +
                      " * bar\n" +
                      " *\n" +
                      " * @param name Comment\n" +
                      " * @return When nice\n" +
                      " * @throws java.io.IOException When in a bad mood\n" +
                      " * @deprecated Help!\n" +
                      " */\n" +
                      "@Deprecated"));
    }

    @Test
    public void testBlockCommenter_RawSimple() {
        StringWriter writer = new StringWriter();
        IndentedPrintWriter iw = new IndentedPrintWriter(writer);

        try (BlockCommentWriter comment = new BlockCommentWriter(iw)) {
            comment.commentRaw("foo <b>FOO</b>\n\nbar")
                   .paragraph()
                   .throws_(IOException.class, "When in a bad mood")
                   .return_("When nice");
        }

        assertThat(writer.getBuffer().toString(),
                   is("\n" +
                      "/**\n" +
                      " * foo <b>FOO</b>\n" +
                      " * <p>\n" +
                      " * bar\n" +
                      " * <p>\n" +
                      " * @throws java.io.IOException When in a bad mood\n" +
                      " * @return When nice\n" +
                      " */"));
    }
}
