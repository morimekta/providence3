/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.util;

import io.pvdnc.idl.generator.GeneratorException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.Pattern;

import static java.util.Objects.requireNonNull;

public class ClassFileManager {
    private static final Pattern PACKAGE_CLASS = Pattern.compile("([a-zA-Z_][a-zA-Z0-9_]*[.])*([A-Z][a-zA-z0-9_$]*)");

    private final Path root;

    public ClassFileManager(Path root) {
        this.root = requireNonNull(root, "root == null");
    }

    public Path getRoot() {
        return root;
    }

    /**
     * @param instanceName The full java package and class name.
     * @return The file path to the
     * @throws IOException If unable to resolve file or create it's parent directory.
     */
    public Path getFileForClass(String instanceName) throws IOException {
        requireNonNull(instanceName, "instanceName == null");
        if (!PACKAGE_CLASS.matcher(instanceName).matches()) {
            throw new IllegalArgumentException("Invalid package and class name: " + instanceName);
        }
        String[] parts = instanceName.split("[.]");
        Path tmp = root;
        for (String part : parts) {
            tmp = tmp.resolve(part);
        }
        Path parent = tmp.getParent();
        if (parent == null) {
            // This is usually impossible to get to, as long as the root is valid.
            throw new GeneratorException("No parent on path: " + tmp);
        }
        Files.createDirectories(parent);
        return parent.resolve(tmp.getFileName() + ".java");
    }
}
