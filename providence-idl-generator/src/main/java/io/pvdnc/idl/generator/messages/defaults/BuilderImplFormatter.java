/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.messages.defaults;

import io.pvdnc.idl.generator.InnerClassGenerator;
import io.pvdnc.idl.generator.MessageTypeFormatter;
import io.pvdnc.idl.generator.util.JavaAnnotation;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;
import net.morimekta.collect.UnmodifiableList;
import net.morimekta.collect.UnmodifiableMap;
import net.morimekta.collect.UnmodifiableSet;
import net.morimekta.collect.UnmodifiableSortedMap;
import net.morimekta.collect.UnmodifiableSortedSet;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.types.PType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import static io.pvdnc.core.property.PDefaultProperties.MESSAGE_VARIANT;
import static io.pvdnc.core.property.PDefaultProperties.ONE_OF_GROUP;
import static io.pvdnc.core.types.PMessageVariant.UNION;
import static net.morimekta.strings.NamingUtil.Format.PASCAL;
import static net.morimekta.strings.NamingUtil.format;

public class BuilderImplFormatter implements MessageTypeFormatter {
    @Override
    public Set<String> getImportClasses(JavaTypeHelper helper, PMessageDescriptor<?> descriptor) {
        Set<String> imports = new HashSet<>();
        imports.add(Objects.class.getName());
        for (PField field : descriptor.allFields()) {
            switch (field.getType()) {
                case LIST: {
                    imports.add(UnmodifiableList.class.getName());
                    break;
                }
                case SET: {
                    PSetDescriptor<?> set = (PSetDescriptor<?>) field.getResolvedDescriptor();
                    if (set.isSorted()) {
                        imports.add(UnmodifiableSortedSet.class.getName());
                    } else {
                        imports.add(UnmodifiableSet.class.getName());
                    }
                    break;
                }
                case MAP: {
                    PMapDescriptor<?,?> map = (PMapDescriptor<?,?>) field.getResolvedDescriptor();
                    if (map.isSorted()) {
                        imports.add(UnmodifiableSortedMap.class.getName());
                    } else {
                        imports.add(UnmodifiableMap.class.getName());
                    }
                    break;
                }
                default: {
                    break;
                }
            }
        }
        return imports;
    }

    @Override
    public void appendMethods(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        JavaTypeHelper helper = writer.getHelper();
        String builder = helper.javaTypeReference(descriptor, InnerClassGenerator.BUILDER_IMPL);
        for (PField field : descriptor.allFields()) {
            String pascalName = format(field.getName(), PASCAL);
            if (field.getType() == PType.VOID) {
                writer.newline()
                      .appendln(JavaAnnotation.OVERRIDE)
                      .formatln("public %s set%s() {\n" +
                                "    m%s |= v%s == null;\n" +
                                "    v%s = true;\n" +
                                "    return this;\n" +
                                "}",
                                builder, pascalName,
                                pascalName, pascalName,
                                pascalName);
            } else {
                writer.newline()
                      .appendln(JavaAnnotation.OVERRIDE)
                      .formatln("public %s set%s(%s value) {",
                                builder,
                                pascalName,
                                helper.getSetterParamType(field.getResolvedDescriptor()))
                      .begin();

                writer.formatln("if (value == null) {\n" +
                                "    return clear%s();\n" +
                                "}", pascalName);
                appendUpdateOneOfProperty(writer, descriptor, field);
                if (field.getType().getBaseType().isPrimitive()) {
                    writer.formatln("m%s |= !Objects.equals(v%s, value);", pascalName, pascalName);
                } else {
                    writer.formatln("m%s = true;", pascalName);
                }
                switch (field.getType()) {
                    case LIST: {
                        writer.formatln("v%s = %s.asList(value);",
                                        pascalName, UnmodifiableList.class.getSimpleName());
                        break;
                    }
                    case SET: {
                        PSetDescriptor<?> set = (PSetDescriptor<?>) field.getResolvedDescriptor();
                        if (set.isSorted()) {
                            writer.formatln("v%s = %s.asSortedSet(value);",
                                            pascalName, UnmodifiableSortedSet.class.getSimpleName());
                        } else {
                            writer.formatln("v%s = %s.asSet(value);",
                                            pascalName, UnmodifiableSet.class.getSimpleName());
                        }
                        break;
                    }
                    case MAP: {
                        PMapDescriptor<?, ?> map = (PMapDescriptor<?, ?>) field.getResolvedDescriptor();
                        if (map.isSorted()) {
                            writer.formatln("v%s = %s.asSortedMap(value);",
                                            pascalName, UnmodifiableSortedMap.class.getSimpleName());
                        } else {
                            writer.formatln("v%s = %s.asMap(value);",
                                            pascalName, UnmodifiableMap.class.getSimpleName());
                        }
                        break;
                    }
                    default: {
                        writer.formatln("v%s = value;", pascalName);
                        break;
                    }
                }

                writer.appendln("return this;");
                writer.end()
                      .appendln("}");

                // -----------------------------
                // ----       MUTABLE       ----
                // -----------------------------

                boolean hasDefaultConst = field.getDefaultValue()
                        .or(() -> field.getResolvedDescriptor().getDefaultValue())
                        .isPresent();

                writer.newline()
                      .appendln(JavaAnnotation.OVERRIDE)
                      .formatln("public %s mutable%s() {",
                                helper.getMutableType(field.getResolvedDescriptor()),
                                pascalName)
                      .begin();

                writer.formatln("m%s = true;", pascalName);
                appendUpdateOneOfProperty(writer, descriptor, field);

                if (hasDefaultConst) {
                    writer.formatln("if (v%s == null) {\n" +
                                    "    v%s = %s.kDefault%s;\n" +
                                    "}",
                                    pascalName,
                                    pascalName, helper.javaTypeReference(field.getOnMessageType(), InnerClassGenerator.DESCRIPTOR), pascalName);
                } else if (field.getType() == PType.MESSAGE) {
                    writer.formatln("if (v%s == null) {\n" +
                                    "    v%s = %s.newBuilder();\n" +
                                    "}",
                                    pascalName,
                                    pascalName, helper.javaTypeReference((PMessageDescriptor<?>) field.getResolvedDescriptor()));
                }

                String cast = "";
                switch (field.getType()) {
                    case LIST: {
                        writer.formatln("if (!(v%s instanceof %s)) {\n" +
                                        "    v%s = new %s<>(v%s);\n" +
                                        "}",
                                        pascalName, ArrayList.class.getName(),
                                        pascalName, ArrayList.class.getName(), pascalName);
                        break;
                    }
                    case SET: {
                        PSetDescriptor<?> set = (PSetDescriptor<?>) field.getResolvedDescriptor();
                        if (set.isSorted()) {
                            writer.formatln("if (!(v%s instanceof %s)) {\n" +
                                            "    v%s = new %s<>(v%s);\n" +
                                            "}",
                                            pascalName, TreeSet.class.getName(),
                                            pascalName, TreeSet.class.getName(), pascalName);
                        } else {
                            writer.formatln("if (!(v%s instanceof %s)) {\n" +
                                            "    v%s = new %s<>(v%s);\n" +
                                            "}",
                                            pascalName, HashSet.class.getName(),
                                            pascalName, LinkedHashSet.class.getName(), pascalName);
                        }
                        break;
                    }
                    case MAP: {
                        PMapDescriptor<?,?> map = (PMapDescriptor<?,?>) field.getResolvedDescriptor();
                        if (map.isSorted()) {
                            writer.formatln("if (!(v%s instanceof %s)) {\n" +
                                            "    v%s = new %s<>(v%s);\n" +
                                            "}",
                                            pascalName, TreeMap.class.getName(),
                                            pascalName, TreeMap.class.getName(), pascalName);
                        } else {
                            writer.formatln("if (!(v%s instanceof %s)) {\n" +
                                            "    v%s = new %s<>(v%s);\n" +
                                            "}",
                                            pascalName, HashMap.class.getName(),
                                            pascalName, LinkedHashMap.class.getName(), pascalName);
                        }
                        break;
                    }
                    case MESSAGE: {
                        PMessageDescriptor<?> message = (PMessageDescriptor<?>) field.getResolvedDescriptor();
                        cast = String.format("(%s) ", helper.javaTypeReference(message, InnerClassGenerator.BUILDER));
                        writer.formatln("if (!(v%s instanceof %s)) {\n" +
                                        "    v%s = v%s.mutate();\n" +
                                        "}",
                                        pascalName, helper.javaTypeReference(message, InnerClassGenerator.BUILDER),
                                        pascalName, pascalName);
                        break;
                    }
                }

                writer.formatln("return %sv%s;", cast, pascalName);
                writer.end()
                      .appendln("}");
            }

            writer.newline()
                  .appendln(JavaAnnotation.OVERRIDE)
                  .formatln("public %s clear%s() {",
                            builder,
                            pascalName,
                            helper.getSetterParamType(field.getResolvedDescriptor()))
                  .begin();
            if (descriptor.getProperty(MESSAGE_VARIANT) == UNION) {
                writer.formatln("if (tUnionField == %d) {\n" +
                                "    tUnionField = Integer.MIN_VALUE;\n" +
                                "}", field.getId());
            } else if (field.hasProperty(ONE_OF_GROUP)) {
                String groupField = OneOfIFaceFormatter.oneOfField(field.getProperty(ONE_OF_GROUP));
                writer.formatln("if (%s == %d) {\n" +
                                "    %s = Integer.MIN_VALUE;\n" +
                                "}",
                                groupField, field.getId(),
                                groupField);
            }

            writer.formatln("m%s |= v%s != null;", pascalName, pascalName)
                  .formatln("v%s = null;", pascalName);

            writer.appendln("return this;");
            writer.end()
                  .appendln("}");
        }
    }

    private void appendUpdateOneOfProperty(JavaTypeWriter writer, PMessageDescriptor<?> descriptor, PField field) {
        if (descriptor.getProperty(MESSAGE_VARIANT) == UNION) {
            writer.formatln("if (tUnionField != %d) {\n" +
                            "    clear(tUnionField);\n" +
                            "}\n" +
                            "tUnionField = %d;", field.getId(), field.getId());
        } else if (field.hasProperty(ONE_OF_GROUP)) {
            String groupField = OneOfIFaceFormatter.oneOfField(field.getProperty(ONE_OF_GROUP));
            writer.formatln("if (%s != %d) {\n" +
                            "    clear(%s);\n" +
                            "}\n" +
                            "%s = %d;",
                            groupField, field.getId(),
                            groupField,
                            groupField, field.getId());
        }
    }
}
