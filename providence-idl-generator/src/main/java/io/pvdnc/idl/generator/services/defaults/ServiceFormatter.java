/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.services.defaults;

import io.pvdnc.core.rpc.PServiceCallHandler;
import io.pvdnc.core.types.PServiceDescriptor;
import io.pvdnc.core.types.PServiceMethod;
import io.pvdnc.idl.generator.util.BlockCommentWriter;
import io.pvdnc.idl.generator.util.JavaAnnotation;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;
import io.pvdnc.idl.generator.ServiceTypeFormatter;

import java.io.IOException;
import java.util.Set;

import static io.pvdnc.idl.generator.InnerClassGenerator.PROCESSOR;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static io.pvdnc.core.property.PDefaultProperties.DEPRECATED;
import static io.pvdnc.core.property.PDefaultProperties.DOCUMENTATION;
import static io.pvdnc.idl.generator.InnerClassGenerator.CLIENT;

public class ServiceFormatter implements ServiceTypeFormatter {
    @Override
    public void appendClassAnnotations(JavaTypeWriter writer, PServiceDescriptor descriptor) {
        writer.appendln(JavaAnnotation.SUPPRESS_WARNINGS_UNUSED);
    }

    @Override
    public Set<String> getImportClasses(JavaTypeHelper helper, PServiceDescriptor descriptor) {
        return setOf(PServiceCallHandler.class.getName());
    }

    @Override
    public void appendMethods(JavaTypeWriter writer, PServiceDescriptor descriptor) {
        JavaTypeHelper helper = writer.getHelper();

        for (PServiceMethod method : descriptor.declaredMethods()) {
            writer.newline();

            try (BlockCommentWriter comment = new BlockCommentWriter(writer)) {
                if (method.hasProperty(DOCUMENTATION)) {
                    comment.comment(method.getProperty(DOCUMENTATION));
                }
                comment.newline();
                comment.param_("request", "The service call request.");
                comment.return_("The service call response.");
                if (method.hasProperty(DEPRECATED)) {
                    comment.deprecated_(method.getProperty(DEPRECATED));
                }
            }
            writer.formatln("%s %s(%s request) throws %s;",
                            helper.javaTypeReference(method.getResponseType()),
                            method.getName(),
                            helper.javaTypeReference(method.getRequestType()),
                            IOException.class.getSimpleName());
        }
    }

    @Override
    public void appendConstructors(JavaTypeWriter writer, PServiceDescriptor descriptor) {
        JavaTypeHelper helper = writer.getHelper();
        writer.newline();
        try (BlockCommentWriter comment = new BlockCommentWriter(writer)) {
            comment.comment("Create a new client instance.");
            comment.newline();
            comment.param_("handler", "The client handler.");
            comment.return_("The client implementation");
        }
        writer.formatln("static %s newClient(%s handler) {\n" +
                        "    return new %s(handler);\n" +
                        "}",
                        helper.javaTypeReference(descriptor), PServiceCallHandler.class.getSimpleName(),
                        helper.javaTypeReference(descriptor, CLIENT));

        writer.newline()
                .formatln("static %s newProcessor(%s impl) {\n" +
                        "    return new %s(impl);\n" +
                        "}",
                helper.javaTypeReference(descriptor, PROCESSOR), writer.getJavaClassName(),
                helper.javaTypeReference(descriptor, PROCESSOR));
    }
}
