/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator;

import java.io.IOException;
import java.io.InputStream;
import java.time.Clock;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import java.util.TreeMap;
import java.util.TreeSet;

public class GeneratorOptions {
    private final TreeSet<Object>         enableGenerators;
    private final TreeMap<String, String> extraOptions;

    public GeneratorOptions() {
        this(getCurrentTime(Clock.systemDefaultZone()),
             getProvidenceVersion());
    }

    public GeneratorOptions(String generatedAt,
                            String generatorVersion) {
        this.generatedAt = generatedAt;
        this.generatorVersion = generatorVersion;
        this.enableGenerators = new TreeSet<>();
        this.extraOptions = new TreeMap<>();
    }

    public String getGeneratedAt() {
        return generatedAt;
    }

    public String getGeneratorVersion() {
        return generatorVersion;
    }

    public TreeSet<Object> getEnableGenerators() {
        return enableGenerators;
    }

    public TreeMap<String, String> getExtraOptions() {
        return extraOptions;
    }

    // --- Private ---

    private final String generatedAt;
    private final String generatorVersion;

    public static String getCurrentTime(Clock clock) {
        return DateTimeFormatter.RFC_1123_DATE_TIME.format(ZonedDateTime.ofInstant(clock.instant(), clock.getZone()).withNano(0));
    }

    public static String getProvidenceVersion() {
        try (InputStream in = GeneratorOptions.class.getResourceAsStream(
                "/io/pvdnc/idl/generator/data/version.properties")) {
            Properties properties = new Properties();
            properties.load(in);
            return properties.getProperty("providence.version");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
