/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.messages.defaults;

import io.pvdnc.core.types.PDeclaredDescriptor;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PType;
import io.pvdnc.idl.generator.GeneratorException;
import io.pvdnc.idl.generator.InnerClassGenerator;
import io.pvdnc.idl.generator.MessageTypeFormatter;
import io.pvdnc.idl.generator.util.JavaAnnotation;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import static net.morimekta.collect.UnmodifiableSet.setOf;
import static io.pvdnc.core.property.PDefaultProperties.MESSAGE_VARIANT;
import static io.pvdnc.core.types.PMessageVariant.UNION;
import static net.morimekta.strings.NamingUtil.Format.PASCAL;
import static net.morimekta.strings.NamingUtil.format;

public class ImplFormatter implements MessageTypeFormatter {
    private final String innerClass;

    public ImplFormatter(String innerClass) {
        this.innerClass = innerClass;
    }

    @Override
    public Set<String> getImportClasses(JavaTypeHelper helper, PMessageDescriptor<?> descriptor) {
        return setOf(Optional.class.getName());
    }

    @Override
    public void appendMethods(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) throws GeneratorException {
        JavaTypeHelper helper = writer.getHelper();
        for (PField field : descriptor.allFields()) {
            String pascalName = format(field.getName(), PASCAL);
            boolean hasDefaultValue = field.getDefaultValue()
                                           .or(() -> field.getDeclaredDescriptor().getDefaultValue())
                                           .isPresent();
            String defaultValueRef = hasDefaultValue
                                     ? helper.javaTypeReference(field.getOnMessageType(), InnerClassGenerator.DESCRIPTOR) + ".kDefault" + pascalName
                                     : field.getType() == PType.MESSAGE
                                       ? helper.javaTypeReference((PDeclaredDescriptor) field.getResolvedDescriptor()) + ".empty()"
                                       : null;
            if (field.getType() != PType.VOID) {
                writer.newline()
                      .appendln(JavaAnnotation.OVERRIDE)
                      .formatln("public %s get%s() {",
                                helper.getValueType(field.getResolvedDescriptor()),
                                pascalName)
                      .begin();
                if (defaultValueRef != null) {
                    writer.formatln("return v%s != null ? v%s : %s;",
                                    pascalName,
                                    pascalName,
                                    defaultValueRef);
                } else {
                    writer.formatln("return v%s;", pascalName);
                }
                writer.end()
                      .appendln("}");

                writer.newline()
                      .appendln(JavaAnnotation.OVERRIDE)
                      .formatln("public Optional<%s> optional%s() {\n" +
                                "    return Optional.ofNullable(v%s);\n" +
                                "}",
                                helper.getValueClass(field.getResolvedDescriptor()),
                                pascalName,
                                pascalName);
            }

            writer.newline()
                  .appendln(JavaAnnotation.OVERRIDE)
                  .formatln("public boolean has%s() {\n" +
                            "    return v%s != null;\n" +
                            "}",
                            pascalName,
                            pascalName);
        }
    }

    @Override
    public void appendConstants(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        if (innerClass.equals(InnerClassGenerator.IMPL)) {
            JavaTypeHelper helper = writer.getHelper();

            writer.newline()
                  .formatln("public static final %s kEmpty = new %s(",
                            helper.javaTypeReference(descriptor, InnerClassGenerator.IMPL),
                            helper.javaTypeReference(descriptor, InnerClassGenerator.IMPL));
            AtomicBoolean hasBefore = new AtomicBoolean();
            for (PField ignored : descriptor.allFields()) {
                if (hasBefore.getAndSet(true)) {
                    writer.append(", ");
                }
                writer.append("null");
            }
            if (descriptor.getProperty(MESSAGE_VARIANT) == UNION) {
                if (hasBefore.getAndSet(true)) {
                    writer.append(", ");
                }
                writer.append("Integer.MIN_VALUE");
            } else {
                for (String ignore : OneOfIFaceFormatter.oneOfGroups(descriptor)) {
                    writer.append(", ")
                          .append("Integer.MIN_VALUE");
                }
            }
            writer.append(");");
        }
    }
}
