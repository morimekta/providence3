/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.services.gen;

import io.pvdnc.idl.generator.InnerClassGenerator;
import io.pvdnc.idl.generator.util.JavaAnnotation;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;
import net.morimekta.collect.UnmodifiableList;
import io.pvdnc.core.PService;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.types.PServiceDescriptor;
import io.pvdnc.core.types.PServiceMethod;
import io.pvdnc.idl.generator.ServiceTypeFormatter;

import java.util.Map;
import java.util.Set;

import static net.morimekta.collect.UnmodifiableSet.setOf;
import static net.morimekta.strings.EscapeUtil.javaEscape;
import static net.morimekta.strings.NamingUtil.Format.PASCAL;
import static net.morimekta.strings.NamingUtil.format;

public class ServiceDescriptorGenerator extends InnerClassGenerator<PServiceDescriptor, ServiceTypeFormatter> {
    public ServiceDescriptorGenerator() {
    }

    @Override
    public String getInnerClassName() {
        return DESCRIPTOR;
    }

    @Override
    public Set<String> getClassImports(JavaTypeHelper helper, PServiceDescriptor descriptor) {
        return setOf(PService.class.getName(),
                     PServiceDescriptor.class.getName(),
                     PServiceMethod.class.getName(),
                     PPropertyMap.class.getName(),
                     UnmodifiableList.class.getName());
    }

    @Override
    public void appendClass(JavaTypeWriter writer, PServiceDescriptor descriptor) {
        JavaTypeHelper helper = writer.getHelper();

        forEachFormatter(f -> f.appendClassAnnotations(writer, descriptor));
        writer.appendln(JavaAnnotation.SUPPRESS_WARNINGS_UNUSED);
        writer.formatln("class %s\n" +
                        "        extends %s", DESCRIPTOR, PServiceDescriptor.class.getSimpleName())
              .begin().begin("            ");
        // TODO: Extra implements.
        writer.append(" {")
              .end();
        for (PServiceMethod method : descriptor.declaredMethods()) {
            writer.formatln("public static final %s kMethod%s = new %s(\n" +
                            "        \"%s\",\n" +
                            "        %s,\n" +
                            "        %s,\n" +
                            "        %s.of(",
                            PServiceMethod.class.getSimpleName(),
                            format(method.getName(), PASCAL),
                            PServiceMethod.class.getSimpleName(),
                            method.getName(),
                            helper.getDescriptorProvider(method.getResponseType()),
                            helper.getDescriptorProvider(method.getRequestType()),
                            PPropertyMap.class.getSimpleName());
            boolean firstProp = true;
            for (Map.Entry<String, String> prop : method.getUncheckedProperties().entrySet()) {
                if (firstProp) firstProp = false;
                else writer.append(",");
                writer.formatln("                \"%s\", \"%s\"",
                                javaEscape(prop.getKey()), javaEscape(prop.getValue()));
            }
            writer.append("),")
                  .formatln("        () -> %s.kDescriptor);", helper.javaTypeReference(descriptor));
        }

        forEachFormatter(f -> f.appendConstants(writer, descriptor));

        forEachFormatter(f -> f.appendFields(writer, descriptor));

        writer.newline()
              .formatln("private %s() {\n" +
                        "    super(\"%s\", \"%s\", %s.of(",
                        DESCRIPTOR,
                        descriptor.getNamespace(), descriptor.getSimpleName(), PPropertyMap.class.getSimpleName());
        boolean firstProp = true;
        for (Map.Entry<String, String> prop : descriptor.getUncheckedProperties().entrySet()) {
            if (firstProp) firstProp = false;
            else writer.append(",");
            writer.formatln("            \"%s\", \"%s\"",
                            javaEscape(prop.getKey()), javaEscape(prop.getValue()));
        }
        writer.format("), %s.listOf(", UnmodifiableList.class.getSimpleName());
        boolean firstExt = true;
        for (PServiceDescriptor ext : descriptor.extending()) {
            if (firstExt) firstExt = false;
            else writer.append(",");
            writer.formatln("            %s",
                            helper.getDescriptorProvider(ext));
        }
        writer.format("), %s.listOf(", UnmodifiableList.class.getSimpleName());
        boolean firstMethod = true;
        for (PServiceMethod method : descriptor.declaredMethods()) {
            if (firstMethod) {
                firstMethod = false;
            } else {
                writer.append(",");
            }
            writer.formatln("            kMethod%s",
                            format(method.getName(), PASCAL));
        }
        writer.format("));")
              .appendln('}');

        forEachFormatter(f -> f.appendConstructors(writer, descriptor));

        forEachFormatter(f -> f.appendMethods(writer, descriptor));

        writer.newline()
              .appendln("// ---- Static ----");

        forEachFormatter(f -> f.appendExtraProperties(writer, descriptor));

        writer.end()
              .formatln("}");
    }
}
