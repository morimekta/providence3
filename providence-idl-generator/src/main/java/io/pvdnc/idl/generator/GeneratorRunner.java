/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator;

import io.pvdnc.core.registry.PGlobalFileTypeRegistry;
import io.pvdnc.core.registry.PNamespaceFileTypeRegistry;
import io.pvdnc.core.types.PDeclaredDescriptor;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PServiceDescriptor;
import io.pvdnc.core.types.PTypeDefDescriptor;
import io.pvdnc.idl.generator.enums.EnumGenerator;
import io.pvdnc.idl.generator.type_defs.TypeDefGenerator;
import io.pvdnc.idl.generator.util.ClassFileManager;
import io.pvdnc.idl.generator.messages.MessageGenerator;
import io.pvdnc.idl.generator.namespaces.NamespaceGenerator;
import io.pvdnc.idl.generator.namespaces.gen.ConstantsGenerator;
import io.pvdnc.idl.generator.namespaces.gen.FileDescriptorGenerator;
import io.pvdnc.idl.generator.services.ServiceGenerator;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;

public class GeneratorRunner {
    public GeneratorRunner(Path generateToDirectory,
                           PGlobalFileTypeRegistry globalTypeRegistry,
                           GeneratorOptions options) {
        this.classFileManager = new ClassFileManager(generateToDirectory);
        this.globalTypeRegistry = globalTypeRegistry;

        enumGenerator = new EnumGenerator(classFileManager, globalTypeRegistry, options);
        messageGenerator = new MessageGenerator(classFileManager, globalTypeRegistry, options);
        serviceGenerator = new ServiceGenerator(classFileManager, globalTypeRegistry, options);
        typeDefGenerator = new TypeDefGenerator(classFileManager, globalTypeRegistry, options);
        namespaceGenerators = new HashMap<>();
        addNamespaceGenerator(new ConstantsGenerator(classFileManager, globalTypeRegistry, options));
        addNamespaceGenerator(new FileDescriptorGenerator(classFileManager, globalTypeRegistry, options));

        load(options);
    }

    public ClassFileManager getClassFileManager() {
        return classFileManager;
    }

    public PGlobalFileTypeRegistry getGlobalTypeRegistry() {
        return globalTypeRegistry;
    }

    public EnumGenerator getEnumGenerator() {
        return enumGenerator;
    }

    public MessageGenerator getMessageGenerator() {
        return messageGenerator;
    }

    public ServiceGenerator getServiceGenerator() {
        return serviceGenerator;
    }

    public TypeDefGenerator getTypeDefGenerator() {
        return typeDefGenerator;
    }

    public void addNamespaceGenerator(NamespaceGenerator generator) {
        if (namespaceGenerators.containsKey(generator.getPostFix())) {
            throw new IllegalArgumentException("Generator for postfix '" + generator.getPostFix() + " already exists.");
        }
        namespaceGenerators.put(generator.getPostFix(), generator);
    }

    public void runGenerators(PNamespaceFileTypeRegistry registry) throws IOException {
        for (PDeclaredDescriptor descriptor : registry.declaredDescriptors()) {
            if (descriptor instanceof PEnumDescriptor) {
                enumGenerator.generate(registry, (PEnumDescriptor) descriptor);
            } else if (descriptor instanceof PMessageDescriptor) {
                messageGenerator.generate(registry, (PMessageDescriptor<?>) descriptor);
            } else if (descriptor instanceof PServiceDescriptor) {
                serviceGenerator.generate(registry, (PServiceDescriptor) descriptor);
            } else if (descriptor instanceof PTypeDefDescriptor) {
                typeDefGenerator.generate(registry, (PTypeDefDescriptor) descriptor);
            }
        }
        for (NamespaceGenerator namespaceGenerator : namespaceGenerators.values()) {
            namespaceGenerator.generate(registry);
        }
    }

    private void load(GeneratorOptions options) {
        for (GeneratorProvider provider : serviceLoader) {
            if (options.getEnableGenerators().contains(provider.getName()) ||
                options.getEnableGenerators().contains("all")) {
                provider.configure(this, options);
            }
        }
    }

    private final ClassFileManager classFileManager;
    private final PGlobalFileTypeRegistry globalTypeRegistry;

    private final EnumGenerator enumGenerator;
    private final MessageGenerator messageGenerator;
    private final ServiceGenerator serviceGenerator;
    private final TypeDefGenerator typeDefGenerator;
    private final Map<String, NamespaceGenerator> namespaceGenerators;

    private static final ServiceLoader<GeneratorProvider> serviceLoader = ServiceLoader.load(GeneratorProvider.class);
}
