/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.namespaces.gen;

import io.pvdnc.core.property.PDefaultProperties;
import io.pvdnc.core.registry.PGlobalFileTypeRegistry;
import io.pvdnc.core.registry.PNamespaceFileTypeRegistry;
import io.pvdnc.core.types.PConstDescriptor;
import io.pvdnc.core.types.PDeclarationType;
import io.pvdnc.core.types.PDeclaredDescriptor;
import io.pvdnc.idl.generator.GeneratorOptions;
import io.pvdnc.idl.generator.util.BlockCommentWriter;
import io.pvdnc.idl.generator.util.ClassFileManager;
import io.pvdnc.idl.generator.util.JavaAnnotation;
import io.pvdnc.idl.generator.util.JavaConstWriter;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;
import io.pvdnc.idl.generator.namespaces.NamespaceGenerator;

import static io.pvdnc.core.property.PDefaultProperties.DEPRECATED;
import static io.pvdnc.core.property.PDefaultProperties.DOCUMENTATION;

public class ConstantsGenerator extends NamespaceGenerator {
    public ConstantsGenerator(ClassFileManager fileManager,
                              PGlobalFileTypeRegistry global,
                              GeneratorOptions options) {
        super(fileManager, global, options);
    }

    @Override
    public String getPostFix() {
        return CONSTANTS;
    }

    @Override
    public void appendClass(JavaTypeWriter writer, PNamespaceFileTypeRegistry registry) {
        try (BlockCommentWriter comment = new BlockCommentWriter(writer)) {
            if (registry.hasProperty(DOCUMENTATION)) {
                comment.comment(registry.getProperty(DOCUMENTATION));
            }
            comment.newline();
            if (registry.hasProperty(PDefaultProperties.DEPRECATED)) {
                comment.deprecated_(registry.getProperty(PDefaultProperties.DEPRECATED));
            }
        }
        writer.appendln(JavaAnnotation.SUPPRESS_WARNINGS_UNUSED)
              .formatln("public final class %s {", writer.getJavaClassName())
              .begin();

        JavaTypeHelper helper = writer.getHelper();
        JavaConstWriter constWriter = new JavaConstWriter(helper);

        boolean first = true;
        for (PDeclaredDescriptor descriptor : registry.declaredDescriptors(PDeclarationType.CONST)) {
            PConstDescriptor constant = (PConstDescriptor) descriptor;
            if (first) {
                first = false;
            } else {
                writer.newline();
            }
            // TODO: Complete when constant handling is reworked in registries.
            writer.formatln("// --- %s ---", constant.getTypeName());
            try (BlockCommentWriter comment = new BlockCommentWriter(writer)) {
                if (constant.hasProperty(DOCUMENTATION)) {
                    comment.comment(constant.getProperty(DOCUMENTATION));
                }
                if (constant.hasProperty(DEPRECATED)) {
                    comment.newline()
                           .deprecated_(constant.getProperty(DEPRECATED));
                }
            }
            writer.formatln("public static final %s %s = ",
                            helper.getValueType(constant.getDescriptor()),
                            constant.getSimpleName());
            constWriter.writeConst(writer, constant.getValue(), constant.getDescriptor());
            writer.append(";");
        }

        // And a private constructor.
        writer.newline()
              .formatln("private %s() {}", helper.getJavaClassName());

        writer.end()
              .appendln('}');
    }

    @Override
    public boolean shouldGenerate(PNamespaceFileTypeRegistry registry) {
        return !registry.declaredTypeReferences(PDeclarationType.CONST).isEmpty();
    }
}
