/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator;

import io.pvdnc.core.types.PDeclaredDescriptor;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;
import static net.morimekta.collect.UnmodifiableSet.setOf;

/**
 * Generator for a single class for a single type. There are often multiple
 * of these per definition type, e.g. one for the main class or interface, and
 * one for the definition class. There are often more.
 *
 * @param <DescriptorType> The declared descriptor type.
 * @param <FormatterType> The formatter type for extensibility.
 */
public abstract class InnerClassGenerator<
        DescriptorType extends PDeclaredDescriptor,
        FormatterType extends TypeFormatter<DescriptorType>> {
    /** Use the 'MAIN' or empty postfix for the main class. */
    public static final String MAIN = "";
    /** For the descriptor implementation for the type. */
    public static final String BUILDER = "Builder";
    /** For the descriptor implementation for the type. */
    public static final String DESCRIPTOR = "$Descriptor";
    /** For message immutable implementation. */
    public static final String IMPL = "$Impl";
    /** For special wrapper impl for Any. */
    public static final String WRAPPER = "$Wrapper";
    /** For message builder implementation. */
    public static final String BUILDER_IMPL = "$BuilderImpl";
    /** For service processor implementation. */
    public static final String PROCESSOR = "$Processor";
    /** For service client implementation. */
    public static final String CLIENT = "$Client";

    private final List<FormatterType> formatters;

    public InnerClassGenerator() {
        this.formatters = new ArrayList<>();
    }

    /**
     * @return The inner class name. Used to separate out extra classes for each type
     *         that can be generated on demand. Return empty string for 'default' / 'main'.
     */
    public abstract String getInnerClassName();

    /**
     * Append class content to the type writer.
     *
     * @param writer The type writer to use.
     * @param descriptor The type descriptor to generate from.
     */
    public abstract void appendClass(JavaTypeWriter writer, DescriptorType descriptor);

    /**
     * @param helper Type helper.
     * @param descriptor The type descriptor.
     * @return Get a set of imports to be done on behalf of the generator itself.
     */
    public Set<String> getClassImports(JavaTypeHelper helper, DescriptorType descriptor) {
        return setOf();
    }

    /**
     * @param descriptor The descriptor to check enabled for.
     * @return If the class should be generated for the descriptor.
     */
    public boolean isEnabled(DescriptorType descriptor) {
        return true;
    }

    // ---

    /**
     * Add a formatter that can add extra content to the class definition.
     *
     * @param formatter The formatter to be added.
     */
    public final void addFormatter(FormatterType formatter) {
        formatters.add(requireNonNull(formatter, "formatter == null"));
    }

    /**
     * @param consumer Run for each registered formatter.
     */
    protected final void forEachFormatter(Consumer<FormatterType> consumer) {
        formatters.forEach(requireNonNull(consumer, "consumer == null"));
    }
}
