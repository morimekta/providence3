/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.enums.defaults;

import io.pvdnc.idl.generator.EnumTypeFormatter;
import io.pvdnc.idl.generator.GeneratorException;
import io.pvdnc.idl.generator.util.JavaAnnotation;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;
import net.morimekta.collect.UnmodifiableList;
import io.pvdnc.core.types.PEnumDescriptor;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;

import static net.morimekta.collect.UnmodifiableSet.setOf;
import static net.morimekta.strings.NamingUtil.Format.SNAKE_UPPER;
import static net.morimekta.strings.NamingUtil.format;

public class PEnumDescriptorFormatter implements EnumTypeFormatter {
    @Override
    public Set<String> getImportClasses(JavaTypeHelper helper, PEnumDescriptor descriptor) {
        return setOf(List.class.getName(),
                     UnmodifiableList.class.getName(),
                     Optional.class.getName(),
                     Supplier.class.getName());
    }

    @Override
    public void appendMethods(JavaTypeWriter writer, PEnumDescriptor descriptor) throws GeneratorException {
        JavaTypeHelper helper = writer.getHelper();

        writer.newline()
              .appendln("// ---- PEnumDescriptor ----");

        writer.newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .formatln("public %s<%s> allValues() {\n" +
                        "    return %s.asList(%s.values());\n" +
                        "}",
                        List.class.getSimpleName(), helper.javaTypeReference(descriptor),
                        UnmodifiableList.class.getSimpleName(), helper.javaTypeReference(descriptor));

        writer.newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .formatln("public %s findById(Integer id) {\n" +
                        "    return %s.findById(id);\n" +
                        "}", helper.javaTypeReference(descriptor), helper.javaTypeReference(descriptor));

        writer.newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .formatln("public %s findByName(String name) {\n" +
                        "    return %s.findByName(name);\n" +
                        "}", helper.javaTypeReference(descriptor), helper.javaTypeReference(descriptor));

        writer.newline()
              .appendln("// ---- PDescriptor ----");

        writer.newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .formatln("public %s<%s> getDefaultValue() {",
                        Optional.class.getSimpleName(),
                        helper.javaTypeReference(descriptor));
        descriptor.getDefaultValue().ifPresentOrElse(
                def -> writer.formatln("    return %s.of(%s.%s);",
                                       Optional.class.getSimpleName(),
                                       helper.javaTypeReference(descriptor),
                                       format(def.getName(), SNAKE_UPPER)),
                () -> writer.formatln("    return %s.empty();", Optional.class.getSimpleName()));
        writer.appendln("}");
    }
}
