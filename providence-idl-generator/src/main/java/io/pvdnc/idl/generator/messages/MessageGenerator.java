/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.messages;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.registry.PGlobalFileTypeRegistry;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.idl.generator.GeneratorOptions;
import io.pvdnc.idl.generator.InnerClassGenerator;
import io.pvdnc.idl.generator.MessageTypeFormatter;
import io.pvdnc.idl.generator.TypeGenerator;
import io.pvdnc.idl.generator.messages.defaults.IFaceFormatter;
import io.pvdnc.idl.generator.messages.defaults.OneOfIFaceFormatter;
import io.pvdnc.idl.generator.messages.defaults.PUnionIFaceFormatter;
import io.pvdnc.idl.generator.messages.gen.MessageBuilderGenerator;
import io.pvdnc.idl.generator.messages.gen.MessageBuilderImplGenerator;
import io.pvdnc.idl.generator.messages.gen.MessageDescriptorGenerator;
import io.pvdnc.idl.generator.messages.gen.MessageImplGenerator;
import io.pvdnc.idl.generator.util.ClassFileManager;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;

import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import static net.morimekta.collect.UnmodifiableSet.setOf;

public class MessageGenerator extends TypeGenerator<PMessageDescriptor<?>, MessageTypeFormatter> {
    public MessageGenerator(ClassFileManager manager, PGlobalFileTypeRegistry global, GeneratorOptions options) {
        super(manager, global, options);
        addClassFormatter(InnerClassGenerator.MAIN, new IFaceFormatter());
        addClassFormatter(InnerClassGenerator.MAIN, new PUnionIFaceFormatter());
        addClassFormatter(InnerClassGenerator.MAIN, new OneOfIFaceFormatter());
        addClass(new MessageBuilderGenerator());
        addClass(new MessageDescriptorGenerator());
        addClass(new MessageImplGenerator());
        addClass(new MessageBuilderImplGenerator());
    }

    @Override
    public Set<String> getClassImports(JavaTypeHelper helper, PMessageDescriptor<?> descriptor) {
        return setOf(PMessage.class.getName(),
                     PMessageDescriptor.class.getName(),
                     Optional.class.getName());
    }

    @Override
    public void appendClassStart(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        JavaTypeHelper helper = writer.getHelper();

        writer.formatln("public interface %s\n" +
                        "        extends %s", helper.getJavaClassName(), PMessage.class.getSimpleName())
              .begin().begin("            ");
        Set<String> extraImplements = new TreeSet<>();
        forEachFormatter(f -> extraImplements.addAll(f.getExtraImplements(helper, descriptor)));
        for (String impl : extraImplements) {
            writer.append(",").appendln(impl);
        }
        descriptor.getExtending().ifPresent(
                ext -> writer.append(",").appendln(helper.javaTypeReference(ext)));
        for (PMessageDescriptor<?> iface : descriptor.getImplementing()) {
            writer.append(",").appendln(helper.javaTypeReference(iface));
        }
        // extra implements goes here.
        writer.append(" {")
              .end()
              .appendln("// ---- Declared Fields ----");

        for (PField field : descriptor.declaredFields()) {
            forEachFormatter(f -> f.appendFieldMethods(writer, descriptor, field));
        }

        forEachFormatter(f -> f.appendMethods(writer, descriptor));

        writer.newline()
              .appendln("// ---- Static ----");

        forEachFormatter(f -> f.appendConstructors(writer, descriptor));
        forEachFormatter(f -> f.appendConstants(writer, descriptor));
    }
}
