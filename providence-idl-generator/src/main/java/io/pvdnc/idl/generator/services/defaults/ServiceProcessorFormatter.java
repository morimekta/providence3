/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.services.defaults;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.rpc.PServiceCallHandler;
import io.pvdnc.core.types.PServiceDescriptor;
import io.pvdnc.core.types.PServiceMethod;
import io.pvdnc.idl.generator.InnerClassGenerator;
import io.pvdnc.idl.generator.ServiceTypeFormatter;
import io.pvdnc.idl.generator.util.JavaAnnotation;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;

import java.io.Closeable;
import java.io.IOException;
import java.util.Objects;
import java.util.Set;

import static net.morimekta.collect.UnmodifiableSet.setOf;
import static net.morimekta.strings.NamingUtil.Format.PASCAL;
import static net.morimekta.strings.NamingUtil.format;

public class ServiceProcessorFormatter implements ServiceTypeFormatter {
    @Override
    public Set<String> getImportClasses(JavaTypeHelper helper, PServiceDescriptor descriptor) {
        return setOf(
                PServiceCallHandler.class.getName(),
                Objects.class.getName(),
                IOException.class.getName(),
                PMessage.class.getName(),
                Closeable.class.getName());
    }

    @Override
    public void appendFields(JavaTypeWriter writer, PServiceDescriptor descriptor) {
        writer.formatln("public final %s impl;", writer.getJavaClassName());
    }

    @Override
    public void appendConstructors(JavaTypeWriter writer, PServiceDescriptor descriptor) {
        writer.newline()
                .formatln("public %s(%s impl) {\n" +
                          "    %s.requireNonNull(impl, \"impl == null\");\n" +
                          "    this.impl = impl;\n" +
                          "}",
                        InnerClassGenerator.PROCESSOR,
                        writer.getJavaClassName(),
                        Objects.class.getSimpleName());
    }

    @Override
    public void appendMethods(JavaTypeWriter writer, PServiceDescriptor descriptor) {
        JavaTypeHelper helper = writer.getHelper();

        // PServiceCallHandler

        writer.newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .formatln("public <RQ extends %s, RS extends %s>\n" +
                        "RS handleCall(%s method, RQ request) throws %s {\n" +
                        "    %s.requireNonNull(method, \"method == null\");\n" +
                        "    %s.requireNonNull(request, \"request == null\");\n" +
                        "    switch(method.getName()) {",
                      PMessage.class.getSimpleName(),
                      PMessage.class.getSimpleName(),
                      PServiceMethod.class.getSimpleName(),
                      IOException.class.getSimpleName(),
                      Objects.class.getSimpleName(),
                      Objects.class.getSimpleName())
                .begin()
                .begin();

        for (PServiceMethod method : descriptor.allMethods()) {
            writer.formatln(
                    "case \"%s\": {\n" +
                    "    return (RS) impl.%s((%s) request);\n" +
                    "}", method.getName(),
                    method.getName(),
                    helper.javaTypeReference(method.getRequestType()));
        }
        writer.formatln(
                "default: {\n" +
                "    throw new %s(\"Unhandled method \" + method.getName() + \" in %s\");\n" +
                "}",
                IOException.class.getSimpleName(),
                descriptor.getTypeName());

        writer.end()
                .appendln('}')
                .end()
                .appendln('}');

        // Closable

        writer.newline()
                .appendln(JavaAnnotation.OVERRIDE)
                .formatln("public void close() throws %s {\n" +
                                "    if (impl instanceof %s) {\n" +
                                "        ((%s) impl).close();\n" +
                                "    }\n" +
                                "}",
                        IOException.class.getSimpleName(),
                        Closeable.class.getSimpleName(),
                        Closeable.class.getSimpleName());
    }
}
