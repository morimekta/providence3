/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.messages.defaults;

import io.pvdnc.idl.generator.InnerClassGenerator;
import io.pvdnc.idl.generator.MessageTypeFormatter;
import io.pvdnc.idl.generator.util.JavaAnnotation;
import io.pvdnc.idl.generator.util.JavaConstWriter;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;
import net.morimekta.collect.UnmodifiableList;
import io.pvdnc.core.property.PDefaultProperties;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PMessageVariant;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

import static net.morimekta.collect.UnmodifiableSet.setOf;
import static net.morimekta.strings.EscapeUtil.javaEscape;
import static net.morimekta.strings.NamingUtil.Format.PASCAL;
import static net.morimekta.strings.NamingUtil.format;

public class PMessageDescriptorFormatter implements MessageTypeFormatter {
    @Override
    public Set<String> getImportClasses(JavaTypeHelper helper, PMessageDescriptor<?> descriptor) {
        Set<String> imports = new HashSet<>(setOf(List.class.getName(),
                     Supplier.class.getName(),
                     PPropertyMap.class.getName(),
                     PField.class.getName(),
                     UnmodifiableList.class.getName()));
        for (PField field : descriptor.declaredFields()) {
            imports.addAll(helper.getDescriptorImports(field.getDeclaredDescriptor(), descriptor));
        }
        return imports;
    }

    @Override
    public void appendConstants(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        JavaTypeHelper helper = writer.getHelper();
        JavaConstWriter constWriter = new JavaConstWriter(helper);
        boolean         hasDefault  = false;
        if (descriptor.allFields().size() > 0) {
            for (PField field : descriptor.allFields()) {
                Object defaultValue = field.getDefaultValue()
                                           .or(() -> field.getResolvedDescriptor().getDefaultValue())
                                           .orElse(null);
                if (defaultValue != null) {
                    writer.formatln("public static final %s kDefault%s = ",
                                    helper.getValueType(field.getResolvedDescriptor()),
                                    format(field.getName(), PASCAL)).begin().begin();
                    constWriter.writeConst(writer, defaultValue, field.getResolvedDescriptor());
                    writer.append(";")
                          .end().end();
                    hasDefault = true;
                }
            }
        }

        for (PField field : descriptor.declaredFields()) {
            String constProvider = "null";
            if (field.getDefaultValue()
                     .or(() -> field.getResolvedDescriptor().getDefaultValue()).isPresent()) {
                constProvider = String.format("() -> kDefault%s", format(field.getName(), PASCAL));
            }
            if (hasDefault) {
                writer.newline();
                hasDefault = false;
            }

            writer.formatln("public static final %s kField%s = new %s(\n" +
                            "        %d,\n" +
                            "        %s,\n" +
                            "        \"%s\",\n" +
                            "        %s,\n" +
                            "        %s.of(",
                            PField.class.getSimpleName(),
                            format(field.getName(), PASCAL),
                            PField.class.getSimpleName(),
                            field.getId(),
                            helper.getDescriptorProvider(field.getDeclaredDescriptor()),
                            javaEscape(field.getName()),
                            constProvider,
                            PPropertyMap.class.getSimpleName())
                  .begin("                ");
            boolean firstProp = true;
            for (Map.Entry<String, String> prop : field.getUncheckedProperties().entrySet()) {
                if (firstProp) {
                    firstProp = false;
                } else {
                    writer.append(",");
                }
                writer.formatln("\"%s\", \"%s\"", javaEscape(prop.getKey()), javaEscape(prop.getValue()));
            }
            writer.end()
                  .append("),");

            if (field.getArgumentsType() != null) {
                writer.formatln("        () -> %s.kDescriptor,", helper.getValueClass(field.getArgumentsType()));
            } else {
                writer.formatln("        null,");
            }
            // The 'onMessageType' param is always last.
            writer.formatln("        () -> %s.kDescriptor);", helper.getJavaClassName());
        }
    }

    @Override
    public void appendFields(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        writer.newline();
        writer.formatln("private final %s<%s> mAllFields;",
                        List.class.getSimpleName(), PField.class.getSimpleName())
              .formatln("private final %s<%s> mDeclaredFields;",
                        List.class.getSimpleName(), PField.class.getSimpleName());
    }

    @Override
    public void appendConstructors(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        JavaTypeHelper helper = writer.getHelper();
        writer.newline()
              .formatln("private %s() {", InnerClassGenerator.DESCRIPTOR)
              .begin()
              .formatln("super(\"%s\", \"%s\", %s.of(",
                        descriptor.getNamespace(), descriptor.getSimpleName(), PPropertyMap.class.getSimpleName())
              .begin("        ");

        boolean firstProp = true;
        for (Map.Entry<String, String> prop : descriptor.getUncheckedProperties().entrySet()) {
            if (firstProp) {
                firstProp = false;
            } else {
                writer.append(",");
            }
            writer.formatln("\"%s\", \"%s\"", javaEscape(prop.getKey()), javaEscape(prop.getValue()));
        }

        writer.end().append("));");

        writer.formatln("this.mDeclaredFields = %s.listOf(",
                        UnmodifiableList.class.getSimpleName())
              .begin().begin();
        boolean firstField = true;
        for (PField field : descriptor.declaredFields()) {
            if (firstField) firstField = false;
            else writer.append(",");
            writer.formatln("kField%s", format(field.getName(), PASCAL));
        }
        writer.append(");").end().end();

        descriptor.getExtending().ifPresentOrElse(ext -> {
            writer.formatln("this.mAllFields = %s.listOf(", UnmodifiableList.class.getSimpleName())
                  .begin().begin();
            boolean firstAllField = true;
            for (PField field : descriptor.allFields()) {
                if (firstAllField) firstAllField = false;
                else writer.append(",");
                if (field.getOnMessageType().equals(descriptor)) {
                    writer.formatln("kField%s", format(field.getName(), PASCAL));
                } else {
                    writer.formatln("%s.kField%s",
                                    helper.javaTypeReference(field.getOnMessageType(), InnerClassGenerator.DESCRIPTOR),
                                    format(field.getName(), PASCAL));
                }
            }
            writer.append(");").end().end();
        }, () -> {
            // the most common scenario: all fields = declared fields.
            writer.appendln("this.mAllFields = this.mDeclaredFields;");
        });

        writer.end()
              .appendln('}');
    }

    @Override
    public void appendMethods(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        JavaTypeHelper helper = writer.getHelper();
        writer.newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .formatln("public %s<%s> declaredFields() {\n" +
                        "    return mDeclaredFields;\n" +
                        "}",
                        List.class.getSimpleName(),
                        PField.class.getSimpleName());

        writer.newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .formatln("public %s<%s> allFields() {\n" +
                        "    return mAllFields;\n" +
                        "}",
                        List.class.getSimpleName(),
                        PField.class.getSimpleName());

        writer.newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .formatln("public %s findFieldById(int id) {\n" +
                        "    switch (id) {",
                        PField.class.getSimpleName());
        for (PField field : descriptor.allFields()) {
            if (field.getOnMessageType().equals(descriptor)) {
                writer.formatln("        case %d: return kField%s;",
                                field.getId(),
                                format(field.getName(), PASCAL));
            } else {
                writer.formatln("        case %d: return %s.kField%s;",
                                field.getId(),
                                helper.javaTypeReference(field.getOnMessageType(), InnerClassGenerator.DESCRIPTOR),
                                format(field.getName(), PASCAL));
            }
        }
        writer.appendln("    }\n" +
                        "    return null;\n" +
                        "}");

        writer.newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .formatln("public %s findFieldByName(String name) {\n" +
                        "    if (name == null) return null;\n" +
                        "    switch (name) {",
                        PField.class.getSimpleName());
        for (PField field : descriptor.allFields()) {
            if (field.getOnMessageType().equals(descriptor)) {
                writer.formatln("        case \"%s\": return kField%s;",
                                field.getName(),
                                format(field.getName(), PASCAL));
            } else {
                writer.formatln("        case \"%s\": return %s.kField%s;",
                                field.getName(),
                                helper.javaTypeReference(field.getOnMessageType(), InnerClassGenerator.DESCRIPTOR),
                                format(field.getName(), PASCAL));
            }
        }
        writer.appendln("    }\n" +
                        "    return null;\n" +
                        "}");

        if (descriptor.getProperty(PDefaultProperties.MESSAGE_VARIANT) == PMessageVariant.INTERFACE) {
            writer.newline()
                  .appendln(JavaAnnotation.OVERRIDE)
                  .formatln("public %s.Builder newBuilder() {\n" +
                            "    throw new UnsupportedOperationException(\"Builders unsupported on interface %s\");\n" +
                            "}",
                            helper.javaTypeReference(descriptor),
                            descriptor.getTypeName());
        } else {
            writer.newline()
                  .appendln(JavaAnnotation.OVERRIDE)
                  .formatln("public %s.Builder newBuilder() {\n" +
                            "    return new %s();\n" +
                            "}",
                            helper.javaTypeReference(descriptor),
                            helper.javaTypeReference(descriptor, InnerClassGenerator.BUILDER_IMPL));
        }
    }
}
