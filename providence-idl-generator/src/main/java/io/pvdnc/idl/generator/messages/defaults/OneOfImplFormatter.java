/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.messages.defaults;

import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.idl.generator.InnerClassGenerator;
import io.pvdnc.idl.generator.MessageTypeFormatter;
import io.pvdnc.idl.generator.util.JavaAnnotation;
import io.pvdnc.idl.generator.util.JavaTypeWriter;

import java.util.Set;

import static io.pvdnc.core.property.PDefaultProperties.MESSAGE_VARIANT;
import static io.pvdnc.core.types.PMessageVariant.UNION;

public class OneOfImplFormatter implements MessageTypeFormatter {
    private final String innerClass;

    public OneOfImplFormatter(String innerClass) {
        this.innerClass = innerClass;
    }

    @Override
    public void appendFields(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        Set<String> groups = OneOfIFaceFormatter.oneOfGroups(descriptor);
        if (groups.isEmpty() || descriptor.getProperty(MESSAGE_VARIANT) == UNION) {
            return;
        }
        writer.newline();
        for (String group : groups) {
            if (innerClass.equals(InnerClassGenerator.IMPL)) {
                writer.formatln("private final int %s;", OneOfIFaceFormatter.oneOfField(group));
            } else {
                writer.formatln("private int %s;", OneOfIFaceFormatter.oneOfField(group));
            }
        }
    }

    @Override
    public void appendMethods(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        Set<String> groups = OneOfIFaceFormatter.oneOfGroups(descriptor);
        if (groups.isEmpty() || descriptor.getProperty(MESSAGE_VARIANT) == UNION) {
            return;
        }
        for (String group : groups) {
            writer.newline()
                  .appendln(JavaAnnotation.OVERRIDE)
                  .formatln("public int %s() {\n" +
                            "    return %s;" +
                            "}",
                            OneOfIFaceFormatter.oneOfFieldIdMethod(group),
                            OneOfIFaceFormatter.oneOfField(group));
        }
    }
}
