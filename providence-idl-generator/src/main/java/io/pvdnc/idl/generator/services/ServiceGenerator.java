/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.services;

import io.pvdnc.core.PService;
import io.pvdnc.core.registry.PGlobalFileTypeRegistry;
import io.pvdnc.core.types.PServiceDescriptor;
import io.pvdnc.idl.generator.GeneratorOptions;
import io.pvdnc.idl.generator.InnerClassGenerator;
import io.pvdnc.idl.generator.ServiceTypeFormatter;
import io.pvdnc.idl.generator.TypeGenerator;
import io.pvdnc.idl.generator.services.defaults.PServiceDescriptorFormatter;
import io.pvdnc.idl.generator.services.defaults.ServiceFormatter;
import io.pvdnc.idl.generator.services.gen.ServiceClientGenerator;
import io.pvdnc.idl.generator.services.gen.ServiceDescriptorGenerator;
import io.pvdnc.idl.generator.services.gen.ServiceProcessorGenerator;
import io.pvdnc.idl.generator.util.ClassFileManager;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;

import java.io.IOException;
import java.util.Set;

import static net.morimekta.collect.UnmodifiableSet.setOf;

public class ServiceGenerator extends TypeGenerator<PServiceDescriptor, ServiceTypeFormatter> {
    public ServiceGenerator(ClassFileManager manager, PGlobalFileTypeRegistry global, GeneratorOptions options) {
        super(manager, global, options);
        addClassFormatter(InnerClassGenerator.MAIN, new ServiceFormatter());
        addClassFormatter(InnerClassGenerator.MAIN, new PServiceDescriptorFormatter());
        addClass(new ServiceDescriptorGenerator());
        addClass(new ServiceClientGenerator());
        addClass(new ServiceProcessorGenerator());
    }

    @Override
    public Set<String> getClassImports(JavaTypeHelper helper, PServiceDescriptor descriptor) {
        return setOf(PService.class.getName(),
                     IOException.class.getName());
    }

    @Override
    public void appendClassStart(JavaTypeWriter writer, PServiceDescriptor descriptor) {
        JavaTypeHelper helper = writer.getHelper();

        writer.formatln("public interface %s",
                        helper.getJavaClassName())
              .begin().begin();
        boolean firstExt = true;
        for (PServiceDescriptor extending : descriptor.extending()) {
            if (firstExt) {
                writer.appendln("extends ");
                firstExt = false;
            } else {
                writer.append(',')
                      .appendln("        ");
            }
            writer.append(helper.javaTypeReference(extending));
        }
        writer.append(" {")
              .end()
              .appendln("// ---- Service Methods ----");

        forEachFormatter(f -> f.appendMethods(writer, descriptor));

        writer.newline()
              .appendln("// ---- Service Constructors ----");

        forEachFormatter(f -> f.appendConstructors(writer, descriptor));

        writer.newline()
              .appendln("// ---- Static ----");

        forEachFormatter(f -> f.appendConstants(writer, descriptor));
    }
}
