/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator;

import io.pvdnc.core.types.PDeclaredDescriptor;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;

import java.util.Set;

import static net.morimekta.collect.UnmodifiableSet.setOf;

public interface TypeFormatter<DescriptorType extends PDeclaredDescriptor> {
    default Set<String> getImportClasses(JavaTypeHelper helper, DescriptorType descriptor) {
        return setOf();
    }

    default void appendClassAnnotations(JavaTypeWriter writer, DescriptorType descriptor) {}

    default void appendExtraProperties(JavaTypeWriter writer, DescriptorType descriptor) {}
}
