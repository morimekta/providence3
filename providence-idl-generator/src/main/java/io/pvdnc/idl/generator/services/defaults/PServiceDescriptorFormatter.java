/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.services.defaults;

import io.pvdnc.core.types.PServiceDescriptor;
import io.pvdnc.idl.generator.InnerClassGenerator;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;
import io.pvdnc.idl.generator.ServiceTypeFormatter;

import java.util.Set;
import java.util.function.Supplier;

import static net.morimekta.collect.UnmodifiableSet.setOf;

public class PServiceDescriptorFormatter implements ServiceTypeFormatter {
    @Override
    public Set<String> getImportClasses(JavaTypeHelper helper, PServiceDescriptor descriptor) {
        return setOf(Supplier.class.getName());
    }

    @Override
    public void appendExtraProperties(JavaTypeWriter writer, PServiceDescriptor descriptor) {
        JavaTypeHelper helper = writer.getHelper();
        writer.newline()
              .formatln("static %s<%s> provider() {\n" +
                        "    return () -> %s.kDescriptor;\n" +
                        "}",
                        Supplier.class.getSimpleName(), helper.javaTypeReference(descriptor, InnerClassGenerator.DESCRIPTOR),
                        helper.javaTypeReference(descriptor));

        writer.newline()
              .formatln("%s kDescriptor = new %s();",
                        helper.javaTypeReference(descriptor, InnerClassGenerator.DESCRIPTOR), helper.javaTypeReference(descriptor, InnerClassGenerator.DESCRIPTOR));
    }
}
