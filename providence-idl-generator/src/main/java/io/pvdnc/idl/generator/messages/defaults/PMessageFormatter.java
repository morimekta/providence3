/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.messages.defaults;

import io.pvdnc.core.property.PDefaultProperties;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PMessageVariant;
import io.pvdnc.core.types.PType;
import io.pvdnc.idl.generator.InnerClassGenerator;
import io.pvdnc.idl.generator.MessageTypeFormatter;
import io.pvdnc.idl.generator.util.JavaAnnotation;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;

import java.util.Optional;
import java.util.Set;

import static net.morimekta.collect.UnmodifiableSet.setOf;
import static net.morimekta.strings.NamingUtil.Format.PASCAL;
import static net.morimekta.strings.NamingUtil.format;

public class PMessageFormatter implements MessageTypeFormatter {
    private final String innerClass;

    public PMessageFormatter(String innerClass) {
        this.innerClass = innerClass;
    }

    @Override
    public Set<String> getImportClasses(JavaTypeHelper helper, PMessageDescriptor<?> descriptor) {
        return setOf(Optional.class.getName(),
                     PMessageDescriptor.class.getName());
    }

    @Override
    public void appendMethods(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        appendHas(writer, descriptor);
        appendGet(writer, descriptor);
        appendOptional(writer, descriptor);

        appendMutate(writer, descriptor);
        appendDescriptor(writer, descriptor);
    }

    public void appendHas(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        writer.newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .formatln("public boolean has(int id) {\n" +
                        "    switch (id) {")
              .begin().begin();

        for (PField field : descriptor.allFields()) {
            writer.formatln("case %d: return v%s != null;",
                            field.getId(), format(field.getName(), PASCAL));
        }

        writer.appendln("default: return false;")
              .end().appendln('}')
              .end().appendln('}');
    }


    public void appendGet(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        writer.newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .appendln(JavaAnnotation.SUPPRESS_WARNINGS_UNCHECKED)
              .formatln("public <T> T get(int id) {\n" +
                        "    switch (id) {")
              .begin().begin();

        for (PField field : descriptor.allFields()) {
            if (field.getType() == PType.VOID) {
                writer.formatln("case %d: return (T) (Object) has%s();",
                                field.getId(), format(field.getName(), PASCAL));
            } else if (field.getType().getBaseType().isPrimitive()) {
                writer.formatln("case %d: return (T) (Object) get%s();",
                                field.getId(), format(field.getName(), PASCAL));
            } else {
                writer.formatln("case %d: return (T) get%s();",
                                field.getId(), format(field.getName(), PASCAL));
            }
        }

        writer.formatln("default: throw new IllegalArgumentException(\"No field \" + id + \" on %s\");",
                        descriptor.getTypeName())
              .end().appendln('}')
              .end().appendln('}');
    }

    public void appendOptional(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        writer.newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .appendln(JavaAnnotation.SUPPRESS_WARNINGS_UNCHECKED)
              .formatln("public <T> Optional<T> optional(int id) {\n" +
                        "    switch (id) {")
              .begin().begin();

        for (PField field : descriptor.allFields()) {
            writer.formatln("case %d: return Optional.ofNullable((T) v%s);",
                            field.getId(), format(field.getName(), PASCAL));
        }

        writer.formatln("default: return Optional.empty();",
                        descriptor.getTypeName())
              .end().appendln('}')
              .end().appendln('}');
    }

    public void appendMutate(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        JavaTypeHelper helper = writer.getHelper();
        writer.newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .formatln("public %s mutate() {", helper.javaTypeReference(descriptor, InnerClassGenerator.BUILDER_IMPL))
              .begin();

        writer.formatln("return new %s(", helper.javaTypeReference(descriptor, InnerClassGenerator.BUILDER_IMPL))
              .begin().begin();

        boolean first = true;
        for (PField field : descriptor.allFields()) {
            if (first) first = false;
            else writer.append(',');
            writer.formatln("v%s", format(field.getName(), PASCAL));
        }
        if (descriptor.getProperty(PDefaultProperties.MESSAGE_VARIANT) == PMessageVariant.UNION) {
            if (!first) {
                writer.append(',');
            }
            writer.formatln("tUnionField");
        } else {
            for (String group : OneOfIFaceFormatter.oneOfGroups(descriptor)) {
                writer.append(',')
                      .appendln(OneOfIFaceFormatter.oneOfField(group));
            }
        }

        writer.end().end().append(");");

        writer.end()
              .appendln('}');
    }

    public void appendDescriptor(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        JavaTypeHelper helper = writer.getHelper();
        writer.newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .formatln("public PMessageDescriptor<%s> $descriptor() {\n" +
                        "    return %s.kDescriptor;\n" +
                        "}",
                        helper.javaTypeReference(descriptor),
                        helper.javaTypeReference(descriptor));
    }
}
