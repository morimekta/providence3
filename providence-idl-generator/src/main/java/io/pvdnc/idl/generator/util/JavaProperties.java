/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.util;

import io.pvdnc.core.property.PPersistedProperty;
import io.pvdnc.core.property.PProperty;

import static io.pvdnc.core.property.PProperty.registerProperties;
import static io.pvdnc.core.property.PPropertyTarget.FILE;
import static io.pvdnc.core.property.PPropertyTarget.MESSAGE;

/**
 * Any properties that only refer to generated code, and does not impact
 * serialization, live handling etc.
 */
public class JavaProperties {
    /**
     * The java package that classes for a given file is going to.
     */
    public static final PProperty<String> JAVA_PACKAGE_NAME;
    /**
     * Fully qualified class of java exception to use for base exception.
     * Only applies to exception messages.
     */
    public static final PProperty<String> JAVA_EXCEPTION_CLASS;
    /**
     * Comma-separated list of fully qualified interfaces to also be
     * implemented by the type. The base interface will extend all these
     * interfaces, and no extra methods will be implemented, so the
     * interface methods  must either refer back to declared field getters or
     * have default implementation.
     */
    public static final PProperty<String> JAVA_EXTRA_IMPLEMENTS;
    /**
     * If the implementation (immutable) type of the message should have a public
     * constructor. This only applies to struct and exception messages.
     */
    public static final PProperty<Boolean> JAVA_PUBLIC_CONSTRUCTOR;

    static {
        JAVA_PACKAGE_NAME = new PPersistedProperty<>("java.namespace", String.class, FILE);
        JAVA_EXCEPTION_CLASS = new PPersistedProperty<>("java.exception.class", String.class, "Exception", MESSAGE);
        JAVA_EXTRA_IMPLEMENTS = new PPersistedProperty<>("java.extra.implements", String.class, MESSAGE);
        JAVA_PUBLIC_CONSTRUCTOR = new PPersistedProperty<>("java.public.constructor", Boolean.class, Boolean.FALSE, MESSAGE);

        registerProperties(JAVA_PACKAGE_NAME,
                           JAVA_EXCEPTION_CLASS,
                           JAVA_EXTRA_IMPLEMENTS,
                           JAVA_PUBLIC_CONSTRUCTOR);
    }
}
