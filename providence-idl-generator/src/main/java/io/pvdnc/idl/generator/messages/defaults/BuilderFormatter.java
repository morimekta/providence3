/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.messages.defaults;

import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PType;
import io.pvdnc.idl.generator.MessageTypeFormatter;
import io.pvdnc.idl.generator.util.BlockCommentWriter;
import io.pvdnc.idl.generator.util.JavaAnnotation;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;

import static io.pvdnc.core.property.PDefaultProperties.DEPRECATED;
import static io.pvdnc.core.property.PDefaultProperties.DOCUMENTATION;
import static net.morimekta.strings.NamingUtil.Format.PASCAL;
import static net.morimekta.strings.NamingUtil.format;

public class BuilderFormatter implements MessageTypeFormatter {
    @Override
    public void appendFieldMethods(JavaTypeWriter writer, PMessageDescriptor<?> descriptor, PField field) {
        JavaTypeHelper helper = writer.getHelper();
        String pascalName = format(field.getName(), PASCAL);

        writer.newline();
        try (BlockCommentWriter comment = new BlockCommentWriter(writer)) {
            if (field.hasProperty(DOCUMENTATION)) {
                comment.comment(field.getProperty(DOCUMENTATION));
            }
            comment.newline();
            if (field.getType() != PType.VOID) {
                comment.param_("value", "The new " + field.getName() + " value");
            }
            comment.return_("The " + field.getName() + " builder.");
            if (field.hasProperty(DEPRECATED)) {
                comment.deprecated_(field.getProperty(DEPRECATED));
            }
        }
        if (!field.getOnMessageType().equals(descriptor)) {
            writer.appendln(JavaAnnotation.OVERRIDE);
        }
        if (field.getType() == PType.VOID) {
            writer.formatln("%s.Builder set%s();",
                            helper.javaTypeReference(descriptor),
                            pascalName);
        } else {
            writer.formatln("%s.Builder set%s(%s value);",
                            helper.javaTypeReference(descriptor),
                            pascalName,
                            helper.getSetterParamType(field.getResolvedDescriptor()));
        }

        if (field.getOnMessageType().equals(descriptor) && field.getType() != PType.VOID) {
            writer.newline();
            try (BlockCommentWriter comment = new BlockCommentWriter(writer)) {
                comment.comment("Get mutable value of the field. This will initialize the field to default\n" +
                                "if not set, and change any immutable type that has a mutable counter part\n" +
                                "to it's mutable type.");
                if (field.hasProperty(DOCUMENTATION)) {
                    comment.paragraph();
                    comment.comment(field.getProperty(DOCUMENTATION));
                }
                comment.newline();
                comment.return_("The mutable " + field.getName() + " value.");
                if (field.hasProperty(DEPRECATED)) {
                    comment.deprecated_(field.getProperty(DEPRECATED));
                }
            }
            writer.formatln("%s mutable%s();",
                            helper.getMutableType(field.getResolvedDescriptor()), pascalName);
        }

        writer.newline();
        try (BlockCommentWriter comment = new BlockCommentWriter(writer)) {
            comment.comment("Clears the " + field.getName() + " value.");
            if (field.hasProperty(DOCUMENTATION)) {
                comment.paragraph()
                       .comment(field.getProperty(DOCUMENTATION));
            }
            comment.newline()
                   .return_("The " + field.getName() + " builder.");
            if (field.hasProperty(DEPRECATED)) {
                comment.deprecated_(field.getProperty(DEPRECATED));
            }
        }
        if (!field.getOnMessageType().equals(descriptor)) {
            writer.appendln(JavaAnnotation.OVERRIDE);
        }
        writer.formatln("%s.Builder clear%s();",
                        helper.javaTypeReference(descriptor),
                        pascalName);
    }

    @Override
    public void appendMethods(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        JavaTypeHelper helper = writer.getHelper();

        writer.newline()
              .appendln("// ---- PMessage.Builder ----");

        writer.newline();
        try (BlockCommentWriter comment = new BlockCommentWriter(writer)) {
            comment.return_("The built " + descriptor.getTypeName() + " instance.");
        }
        writer.appendln(JavaAnnotation.OVERRIDE)
              .formatln("%s build();", helper.javaTypeReference(descriptor));
    }
}
