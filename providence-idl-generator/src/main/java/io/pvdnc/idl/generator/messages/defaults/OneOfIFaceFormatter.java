/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.messages.defaults;

import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.idl.generator.MessageTypeFormatter;
import io.pvdnc.idl.generator.util.JavaTypeWriter;

import java.util.Set;
import java.util.TreeSet;

import static io.pvdnc.core.property.PDefaultProperties.ONE_OF_GROUP;
import static net.morimekta.strings.NamingUtil.Format.CAMEL;
import static net.morimekta.strings.NamingUtil.Format.PASCAL;
import static net.morimekta.strings.NamingUtil.format;

public class OneOfIFaceFormatter implements MessageTypeFormatter {
    public static Set<String> oneOfGroups(PMessageDescriptor<?> descriptor) {
        Set<String> groups = new TreeSet<>();
        for (PField field : descriptor.allFields()) {
            if (field.hasProperty(ONE_OF_GROUP)) {
                groups.add(field.getProperty(ONE_OF_GROUP));
            }
        }
        return groups;
    }

    public static String oneOfFieldIdMethod(String group) {
        return format(group, CAMEL) + "FieldId";
    }

    public static String oneOfField(String group) {
        return 't' + format(group, PASCAL) + "Field";
    }

    public static String oneOfParam(String group) {
        return 'r' + format(group, PASCAL) + "Field";
    }

    @Override
    public void appendMethods(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        for (String group : oneOfGroups(descriptor)) {
            writer.newline()
                  .formatln("int %sFieldId();", format(group, CAMEL));
        }
    }
}
