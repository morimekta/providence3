/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.util;

import io.pvdnc.core.PEnum;
import io.pvdnc.core.PMessage;
import io.pvdnc.core.PAny;
import io.pvdnc.core.impl.AnyBinary;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.types.PType;
import net.morimekta.collect.UnmodifiableList;
import net.morimekta.collect.UnmodifiableMap;
import net.morimekta.collect.UnmodifiableSet;
import net.morimekta.collect.UnmodifiableSortedMap;
import net.morimekta.collect.UnmodifiableSortedSet;
import net.morimekta.collect.util.Binary;
import net.morimekta.strings.EscapeUtil;
import net.morimekta.strings.io.IndentedPrintWriter;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import static java.util.Objects.requireNonNull;
import static net.morimekta.strings.NamingUtil.Format.PASCAL;
import static net.morimekta.strings.NamingUtil.Format.SNAKE_UPPER;
import static net.morimekta.strings.NamingUtil.format;

public class JavaConstWriter {
    private final JavaTypeHelper helper;

    public JavaConstWriter(JavaTypeHelper helper) {
        this.helper = requireNonNull(helper, "helper == null");
    }

    public void writeConst(IndentedPrintWriter writer, Object value, PDescriptor descriptor) {
        requireNonNull(writer, "writer == null");
        requireNonNull(value, "value == null");
        requireNonNull(descriptor, "descriptor == null");
        switch (descriptor.getType()) {
            case BOOL:
            case INT:
            case DOUBLE: {
                writer.append(value.toString());
                break;
            }
            case BYTE: {
                writer.append("(byte) ").append(value.toString());
                break;
            }
            case SHORT: {
                writer.append("(short) ").append(value.toString());
                break;
            }
            case LONG: {
                writer.append(value.toString()).append("L");
                break;
            }
            case FLOAT: {
                writer.append(value.toString()).append("f");
                break;
            }
            case STRING:
                writer.format("\"%s\"", EscapeUtil.javaEscape(value.toString()));
                break;
            case BINARY:
                if (((Binary) value).length() == 0) {
                    writer.format("%s.empty()",
                                  Binary.class.getName());
                } else {
                    writer.format("%s.fromBase64(\"%s\")",
                                  Binary.class.getName(),
                                  ((Binary) value).toBase64());
                }
                break;
            case ANY:
                if (((PAny) value).isPresent()) {
                    PMessage message;
                    try {
                        message = ((PAny) value).getMessage();
                    } catch (Exception e) {
                        if (value instanceof AnyBinary) {
                            writeConst(writer, value, AnyBinary.kDescriptor);
                            break;
                        } else {
                            throw e;
                        }
                    }
                    writer.format("%s.wrap(", PAny.class.getName())
                            .begin("        ");
                    writeConst(writer, message, message.$descriptor());
                    writer.end().append(")");
                } else {
                    writer.format("%s.empty()", PAny.class.getName());
                }
                break;

            case LIST: {
                @SuppressWarnings("unchecked")
                PListDescriptor<Object> list = (PListDescriptor<Object>) descriptor;
                @SuppressWarnings("unchecked")
                Collection<Object> collection = (Collection<Object>) value;
                writer.format("%s.listOf(", UnmodifiableList.class.getName())
                      .begin(" ".repeat(8));
                boolean first = true;
                for (Object o : collection) {
                    if (first) {
                        first = false;
                    } else {
                        writer.append(",");
                    }
                    writer.appendln();
                    writeConst(writer, o, list.getItemType());
                }
                writer.append(")").end();
                break;
            }
            case SET: {
                @SuppressWarnings("unchecked")
                PSetDescriptor<Object> set = (PSetDescriptor<Object>) descriptor;
                @SuppressWarnings("unchecked")
                Collection<Object> collection = (Collection<Object>) value;
                writer.format("%s.%s(",
                              set.isSorted() ? UnmodifiableSortedSet.class.getName() : UnmodifiableSet.class.getName(),
                              set.isSorted() ? "sortedSetOf" : "setOf")
                      .begin(" ".repeat(8));
                boolean first = true;
                for (Object o : collection) {
                    if (first) {
                        first = false;
                    } else {
                        writer.append(",");
                    }
                    writer.appendln();
                    writeConst(writer, o, set.getItemType());
                }
                writer.append(")").end();
                break;
            }
            case MAP: {
                @SuppressWarnings("unchecked")
                PMapDescriptor<Object, Object> map = (PMapDescriptor<Object, Object>) descriptor;
                @SuppressWarnings("unchecked")
                Map<Object, Object> mv = (Map<Object, Object>) value;
                if (mv.isEmpty()) {
                    writer.format("%s.%s()",
                                  map.isSorted()
                                  ? UnmodifiableSortedMap.class.getName()
                                  : UnmodifiableMap.class.getName(),
                                  map.isSorted()
                                  ? "sortedMapOf"
                                  : "mapOf");
                } else {
                    writer.format("%s.<%s,%s>%s(%d)",
                                  map.isSorted()
                                  ? UnmodifiableSortedMap.class.getName()
                                  : UnmodifiableMap.class.getName(),
                                  helper.getValueClass(map.getKeyType()),
                                  helper.getValueClass(map.getValueType()),
                                  map.isSorted()
                                  ? "newBuilderNaturalOrder"
                                  : "newBuilder",
                                  mv.size())
                          .begin(" ".repeat(8));
                    for (Map.Entry<Object,Object> entry : mv.entrySet()) {
                        writer.appendln(".put(");
                        writeConst(writer, entry.getKey(), map.getKeyType());
                        writer.append(", ");
                        writeConst(writer, entry.getValue(), map.getValueType());
                        writer.append(")");
                    }
                    writer.appendln(".build()").end();
                }
                break;
            }
            case ENUM: {
                PEnum ev = (PEnum) value;
                writer.format("%s.%s",
                              helper.javaTypeReference((PEnumDescriptor) descriptor),
                              format(ev.getName(), SNAKE_UPPER));
                break;
            }
            case MESSAGE: {
                PMessageDescriptor<?> md = (PMessageDescriptor<?>) descriptor;
                PMessage message = (PMessage) value;
                writer.format("%s.newBuilder()", helper.javaTypeReference(md))
                      .begin(" ".repeat(8));
                boolean hasField = false;
                for (PField field : md.allFields()) {
                    if (field.getType() == PType.VOID) {
                        if (message.has(field.getId())) {
                            hasField = true;
                            writer.formatln(".set%s()", format(field.getName(), PASCAL));
                        }
                    } else {
                        Optional<?> fv = message.optional(field.getId());
                        if (fv.isPresent()) {
                            hasField = true;
                            writer.formatln(".set%s(", format(field.getName(), PASCAL));
                            writeConst(writer, fv.get(), field.getResolvedDescriptor());
                            writer.append(")");
                        }
                    }
                }
                if (hasField) {
                    writer.appendln();
                }
                writer.append(".build()")
                      .end();
                break;
            }
            case VOID:
            default:
                throw new IllegalArgumentException("Unhandled const type: " + descriptor.getType());
        }
    }
}
