/*
 * Copyright 2015-2016 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.util;

import net.morimekta.strings.io.IndentedPrintWriter;

import java.io.Closeable;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.util.Objects.requireNonNull;

/**
 * Builds a proper block javadoc-compatible comment. Usage is to
 * do a try with resource block with the comment writer. When closed it will
 * complete the comment and if not opened, will not write anything.
 *
 * <pre>{@code
 * try (BlockCommentWriter comment = new BlockCommentWriter(writer)) {
 *     boolean first = true;
 *     if (def.hasProperty(DOCUMENTATION)) {
 *         first = false;
 *         comment.comment(def.getProperty(DOCUMENTATION));
 *     }
 *     if (def.hasProperty(DEPRECATED)) {
 *         if (!first) {
 *             comment.newline();
 *         }
 *         first = false;
 *         comment.deprecated_(def.getProperty(DEPRECATED));
 *     }
 *     // ... etc.
 * }
 * }</pre>
 */
public class BlockCommentWriter implements Closeable {
    private final IndentedPrintWriter writer;
    private final AtomicBoolean opened;
    private final AtomicBoolean deprecated;

    public BlockCommentWriter(IndentedPrintWriter writer) {
        this.writer = writer;
        this.opened = new AtomicBoolean();
        this.deprecated = new AtomicBoolean();
    }

    public boolean isOpened() {
        return opened.get();
    }

    public BlockCommentWriter comment(String comment) {
        maybeOpen();
        String escaped = escapeHtml(comment);
        for (String line : escaped.trim().split("\r?\n", Short.MAX_VALUE)) {
            if (line.strip().length() == 0) {
                writer.appendln(" <p>");
            } else {
                writer.appendln(" " + line.replaceAll("[ ]*$", ""));
            }
        }
        return this;
    }

    public BlockCommentWriter commentRaw(String comment) {
        maybeOpen();
        for (String line : comment.trim().split("\r?\n", Short.MAX_VALUE)) {
            if (line.strip().length() == 0) {
                writer.appendln(" <p>");
            } else {
                writer.appendln(" " + line.replaceAll("[ ]*$", ""));
            }
        }
        return this;
    }

    public BlockCommentWriter newline() {
        if (isOpened()) {
            writer.appendln();
        }
        return this;
    }

    public BlockCommentWriter paragraph() {
        if (isOpened()) {
            writer.appendln(" <p>");
        }
        return this;
    }

    public BlockCommentWriter param_(String name, String comment) {
        requireNonNull(name, "name == null");
        requireNonNull(comment, "comment == null");
        maybeOpen();
        writer.formatln(" @param %s %s", name, escapeHtml(comment));
        return this;
    }

    public BlockCommentWriter return_(String comment) {
        requireNonNull(comment, "comment == null");
        maybeOpen();
        writer.formatln(" @return %s", comment);
        return this;
    }

    public BlockCommentWriter throws_(Class<?> klass, String comment) {
        requireNonNull(klass, "class == null");
        requireNonNull(comment, "comment == null");
        maybeOpen();
        return throws_(klass.getName().replaceAll("[$]", "."), comment);
    }

    public BlockCommentWriter throws_(String klass, String comment) {
        requireNonNull(klass, "class == null");
        requireNonNull(comment, "comment == null");
        maybeOpen();
        writer.formatln(" @throws %s %s",
                        klass,
                        comment);
        return this;
    }

    public BlockCommentWriter deprecated_(String reason) {
        requireNonNull(reason);
        maybeOpen();
        writer.formatln(" @deprecated %s", escapeHtml(reason));
        deprecated.set(true);
        return this;
    }

    // --- Closeable ---

    @Override
    public void close() {
        if (opened.get()) {
            writer.end()
                  .appendln(" */");
            if (deprecated.get()) {
                writer.appendln(JavaAnnotation.DEPRECATED);
            }
        }
    }

    // --- Private ---

    private void maybeOpen() {
        if (!opened.getAndSet(true)) {
            writer.appendln("/**")
                  .begin(" *");
        }
    }

    private String escapeHtml(String html) {
        return html.replaceAll("[&]", "&amp;")
                   .replaceAll("[\"]", "&quot;")
                   .replaceAll("[@]", "&#64;")
                   .replaceAll("[']", "&#39;")
                   .replaceAll(">", "&gt;")
                   .replaceAll("<", "&lt;");
    }
}
