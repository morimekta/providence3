/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.services.defaults;

import io.pvdnc.core.rpc.PServiceCallHandler;
import io.pvdnc.core.types.PServiceDescriptor;
import io.pvdnc.core.types.PServiceMethod;
import io.pvdnc.idl.generator.InnerClassGenerator;
import io.pvdnc.idl.generator.util.JavaAnnotation;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;
import io.pvdnc.idl.generator.ServiceTypeFormatter;

import java.io.IOException;
import java.util.Objects;
import java.util.Set;

import static net.morimekta.collect.UnmodifiableSet.setOf;
import static net.morimekta.strings.NamingUtil.Format.PASCAL;
import static net.morimekta.strings.NamingUtil.format;

public class ServiceClientFormatter implements ServiceTypeFormatter {
    @Override
    public Set<String> getImportClasses(JavaTypeHelper helper, PServiceDescriptor descriptor) {
        return setOf(PServiceCallHandler.class.getName(),
                     Objects.class.getName(),
                     IOException.class.getName());
    }

    @Override
    public void appendFields(JavaTypeWriter writer, PServiceDescriptor descriptor) {
        writer.formatln("public final %s handler;", PServiceCallHandler.class.getSimpleName());
    }

    @Override
    public void appendConstructors(JavaTypeWriter writer, PServiceDescriptor descriptor) {
        writer.newline()
              .formatln("public %s(%s handler) {\n" +
                        "    %s.requireNonNull(handler, \"handler == null\");\n" +
                        "    this.handler = handler;\n" +
                        "}",
                        InnerClassGenerator.CLIENT, PServiceCallHandler.class.getSimpleName(),
                        Objects.class.getSimpleName());
    }

    @Override
    public void appendMethods(JavaTypeWriter writer, PServiceDescriptor descriptor) {
        JavaTypeHelper helper = writer.getHelper();

        for (PServiceMethod method : descriptor.allMethods()) {
            writer.newline()
                  .appendln(JavaAnnotation.OVERRIDE)
                  .formatln("public %s %s(%s request) throws %s {\n" +
                            "    return handler.handleCall(%s.kMethod%s, request);\n" +
                            "}",
                            helper.javaTypeReference(method.getResponseType()),
                            method.getName(),
                            helper.javaTypeReference(method.getRequestType()),
                            IOException.class.getSimpleName(),
                            helper.javaTypeReference(method.getOnServiceType(), InnerClassGenerator.DESCRIPTOR),
                            format(method.getName(), PASCAL));
        }
    }
}
