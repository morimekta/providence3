/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.messages.gen;

import io.pvdnc.core.PMessageBuilder;
import io.pvdnc.core.PUnion;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PMessageVariant;
import io.pvdnc.idl.generator.InnerClassGenerator;
import io.pvdnc.idl.generator.MessageTypeFormatter;
import io.pvdnc.idl.generator.messages.defaults.BuilderFormatter;
import io.pvdnc.idl.generator.util.BlockCommentWriter;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;

import java.util.Set;

import static io.pvdnc.core.property.PDefaultProperties.MESSAGE_VARIANT;
import static net.morimekta.collect.UnmodifiableSet.setOf;

public class MessageBuilderGenerator extends InnerClassGenerator<PMessageDescriptor<?>, MessageTypeFormatter> {
    public MessageBuilderGenerator() {
        addFormatter(new BuilderFormatter());
    }

    @Override
    public String getInnerClassName() {
        return BUILDER;
    }

    @Override
    public Set<String> getClassImports(JavaTypeHelper helper, PMessageDescriptor<?> descriptor) {
        return setOf(PMessageBuilder.class.getName());
    }

    @Override
    public void appendClass(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        JavaTypeHelper helper = writer.getHelper();

        try (BlockCommentWriter comment = new BlockCommentWriter(writer)) {
            comment.comment("Builder interface for creating " + descriptor.getTypeName() + " instances.");
        }
        forEachFormatter(f -> f.appendClassAnnotations(writer, descriptor));
        writer.formatln("interface Builder\n" +
                        "        extends %s,\n" +
                        "                %s",
                        PMessageBuilder.class.getSimpleName(),
                        helper.javaTypeReference(descriptor))
              .begin().begin("            ");
        if (descriptor.getProperty(MESSAGE_VARIANT) == PMessageVariant.UNION) {
            writer.append(",").appendln(PUnion.class.getSimpleName());
        }
        descriptor.getExtending().ifPresent(
                ext -> writer.append(",").appendln(helper.javaTypeReference(ext, "Builder")));
        for (PMessageDescriptor<?> iface : descriptor.getImplementing()) {
            writer.append(",").appendln(helper.javaTypeReference(iface, "Builder"));
        }
        writer.end().append(" {")
              .appendln("// ---- Fields ----");

        for (PField field : descriptor.allFields()) {
            forEachFormatter(f -> f.appendFieldMethods(writer, descriptor, field));
        }
        forEachFormatter(f -> f.appendMethods(writer, descriptor));
        forEachFormatter(f -> f.appendConstructors(writer, descriptor));
        forEachFormatter(f -> f.appendExtraProperties(writer, descriptor));
        forEachFormatter(f -> f.appendConstants(writer, descriptor));

        writer.end()
              .appendln("}");
    }
}
