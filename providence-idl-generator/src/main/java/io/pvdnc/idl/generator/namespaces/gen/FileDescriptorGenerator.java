/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.namespaces.gen;

import io.pvdnc.idl.generator.GeneratorOptions;
import io.pvdnc.idl.generator.util.BlockCommentWriter;
import io.pvdnc.idl.generator.util.ClassFileManager;
import io.pvdnc.idl.generator.util.JavaAnnotation;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;
import net.morimekta.collect.UnmodifiableList;
import net.morimekta.collect.UnmodifiableMap;
import io.pvdnc.core.property.PDefaultProperties;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.registry.PGlobalFileTypeRegistry;
import io.pvdnc.core.registry.PNamespaceFileTypeRegistry;
import io.pvdnc.core.registry.PNamespaceTypeRegistry;
import io.pvdnc.core.registry.PSimpleTypeRegistry;
import io.pvdnc.core.types.PConstDescriptor;
import io.pvdnc.core.types.PDeclarationType;
import io.pvdnc.core.types.PDeclaredDescriptor;
import io.pvdnc.core.types.PFileDescriptor;
import io.pvdnc.idl.generator.namespaces.NamespaceGenerator;
import net.morimekta.strings.NamingUtil;

import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static net.morimekta.collect.UnmodifiableSortedSet.sortedSetOf;
import static net.morimekta.collect.util.SetOperations.union;
import static io.pvdnc.core.property.PDefaultProperties.DOCUMENTATION;
import static net.morimekta.strings.EscapeUtil.javaEscape;

public class FileDescriptorGenerator extends NamespaceGenerator {
    public FileDescriptorGenerator(ClassFileManager fileManager,
                                   PGlobalFileTypeRegistry global,
                                   GeneratorOptions options) {
        super(fileManager, global, options);
    }

    @Override
    public String getPostFix() {
        return FILE_DESCRIPTOR;
    }

    private Set<String> getConstTypeIncludes(JavaTypeHelper helper, PNamespaceFileTypeRegistry registry) {
        Set<String> includes = new TreeSet<>();
        for (PDeclaredDescriptor declaredDescriptor : registry.declaredDescriptors(PDeclarationType.CONST)) {
            PConstDescriptor pcc = (PConstDescriptor) declaredDescriptor;
            includes.addAll(helper.getTypeIncludes(pcc.getDescriptor()));
            includes.add(PConstDescriptor.class.getName());
        }
        for (PNamespaceTypeRegistry include : registry.getIncludes().values()) {
            String inc = helper.javaFileDescriptorReference(include);
            if (inc.contains(".")) {
                includes.add(inc);
            }
        }
        return includes;
    }

    @Override
    public void appendClass(JavaTypeWriter writer, PNamespaceFileTypeRegistry registry) {
        JavaTypeHelper helper = writer.getHelper();

        try {
            for (String classImport : union(
                    sortedSetOf(
                            PPropertyMap.class.getName(),
                            UnmodifiableMap.class.getName(),
                            UnmodifiableList.class.getName(),
                            PFileDescriptor.class.getName(),
                            PSimpleTypeRegistry.class.getName()),
                    getConstTypeIncludes(helper, registry))) {
                writer.formatln("import %s;", classImport);
            }
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            throw e;
        }
        writer.newline();

        try (BlockCommentWriter comment = new BlockCommentWriter(writer)) {
            if (registry.hasProperty(DOCUMENTATION)) {
                comment.comment(registry.getProperty(DOCUMENTATION));
            }
            comment.newline();
            if (registry.hasProperty(PDefaultProperties.DEPRECATED)) {
                comment.deprecated_(registry.getProperty(PDefaultProperties.DEPRECATED));
            }
        }
        boolean first;
        writer.appendln(JavaAnnotation.SUPPRESS_WARNINGS_UNUSED)
                .formatln("public final class %s extends PFileDescriptor {", writer.getJavaClassName())
                .begin()
                .formatln("public static final %s kDescriptor = new %s();",
                        writer.getJavaClassName(), writer.getJavaClassName())
                .newline()
                .formatln("private %s() {", writer.getJavaClassName())
                .begin()
                .formatln("super(\"%s\",", registry.getNamespace()).begin("        ")
                .formatln("%s.of(", PPropertyMap.class.getSimpleName()).begin("        ");
        first = true;
        for (Map.Entry<String, String> entry : registry.getUncheckedProperties().entrySet()) {
            if (first) first = false;
            else writer.append(',');
            writer.formatln("\"%s\", \"%s\"", entry.getKey(), javaEscape(entry.getValue()));
        }
        writer.end()
                .append("),")
                .formatln("%s.mapOf(", UnmodifiableMap.class.getSimpleName())
                .begin("        ");
        first = true;
        for (Map.Entry<String, PNamespaceTypeRegistry> include : registry.getIncludes().entrySet()) {
            if (first) first = false;
            else writer.append(',');
            String className = NamingUtil.format(include.getValue().getNamespace(), NamingUtil.Format.PASCAL) + FILE_DESCRIPTOR;
            writer.formatln("\"%s\", %s.kDescriptor", include.getKey(), className);
        }
        writer.end()
              .append("),")
              .formatln("%s.listOf(", UnmodifiableList.class.getSimpleName())
              .begin("        ");
        first = true;
        for (PDeclaredDescriptor descriptor : registry.declaredDescriptors()) {
            if (first) first = false;
            else writer.append(',');
            if (descriptor instanceof PConstDescriptor) {
                writer.formatln("new %s(\"%s\", \"%s\", %s, () -> %s%s.%s)",
                        PConstDescriptor.class.getSimpleName(),
                        registry.getNamespace(),
                        descriptor.getSimpleName(),
                        helper.getDescriptorProvider(((PConstDescriptor) descriptor).getDescriptor()),
                        NamingUtil.format(descriptor.getNamespace(), NamingUtil.Format.PASCAL),
                        CONSTANTS,
                        descriptor.getSimpleName());
            } else {
                writer.formatln("%s.kDescriptor", helper.javaTypeReference(descriptor));
            }
        }
        writer.end()
              .end()
              .append("));");
        writer.appendln("PSimpleTypeRegistry.getInstance().registerRecursively(this);");
        writer.end()
              .appendln("}");
        writer.end()
              .appendln('}');
    }

    @Override
    public boolean shouldGenerate(PNamespaceFileTypeRegistry registry) {
        return true;
    }
}
