/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.enums.defaults;

import io.pvdnc.core.PEnum;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PEnumValue;
import io.pvdnc.idl.generator.EnumTypeFormatter;
import io.pvdnc.idl.generator.GeneratorException;
import io.pvdnc.idl.generator.InnerClassGenerator;
import io.pvdnc.idl.generator.util.JavaAnnotation;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;

import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;

import static net.morimekta.collect.UnmodifiableSet.setOf;
import static net.morimekta.strings.EscapeUtil.javaEscape;
import static net.morimekta.strings.NamingUtil.Format.SNAKE_UPPER;
import static net.morimekta.strings.NamingUtil.format;

public class PEnumFormatter implements EnumTypeFormatter {
    @Override
    public Set<String> getImportClasses(JavaTypeHelper helper, PEnumDescriptor descriptor) {
        return setOf(PEnumValue.class.getName(),
                     Objects.class.getName(),
                     Supplier.class.getName());
    }

    @Override
    public void appendClassAnnotations(JavaTypeWriter writer, PEnumDescriptor type) throws GeneratorException {
        writer.appendln(JavaAnnotation.SUPPRESS_WARNINGS_UNUSED);
    }

    @Override
    public void appendMethods(JavaTypeWriter writer, PEnumDescriptor descriptor) throws GeneratorException {
        writer.newline()
              .appendln("// ---- PEnum ----");

        writer.newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .formatln("public %s $meta() {\n" +
                        "    return meta;\n" +
                        "}", PEnumValue.class.getSimpleName())
              .newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .formatln("public %s $descriptor() {\n" +
                        "    return kDescriptor;\n" +
                        "}",
                        InnerClassGenerator.DESCRIPTOR,
                        writer.getHelper().javaTypeReference(descriptor));
    }

    @Override
    public void appendExtraProperties(JavaTypeWriter writer, PEnumDescriptor descriptor) throws GeneratorException {
        String javaClassName = JavaTypeHelper.javaClassName(descriptor);

        writer.newline()
              .formatln("public static %s findById(Integer id) {\n" +
                        "    if (id == null) return null;\n" +
                        "    switch (id) {",
                        javaClassName);

        for (PEnum pe : descriptor.allValues()) {
            writer.formatln("        case %d: return %s;", pe.getValue(), format(pe.getName(), SNAKE_UPPER));
        }

        writer.appendln("        default: return null;\n" +
                        "    }\n" +
                        "}");

        writer.newline()
              .formatln("public static %s findByName(String name) {\n" +
                        "    if (name == null) return null;\n" +
                        "    switch (name) {",
                        javaClassName);

        for (PEnum pe : descriptor.allValues()) {
            writer.formatln("        case \"%s\": return %s;",
                            javaEscape(pe.getName()),
                            format(pe.getName(), SNAKE_UPPER));
        }

        writer.appendln("        default: return null;\n" +
                        "    }\n" +
                        "}");

        writer.newline()
              .formatln("public static %s valueForId(int id) {\n" +
                        "    %s value = findById(id);\n" +
                        "    if (value == null) {\n" +
                        "        throw new %s(\"No %s value for id \" + id);\n" +
                        "    }\n" +
                        "    return value;\n" +
                        "}",
                        javaClassName,
                        javaClassName,
                        IllegalArgumentException.class.getSimpleName(),
                        descriptor.getTypeName());

        writer.newline()
              .formatln("public static %s valueForName(String name) {\n" +
                        "    %s.requireNonNull(name, \"name == null\");\n" +
                        "    %s value = findByName(name);\n" +
                        "    if (value == null) {\n" +
                        "        throw new %s(\"No %s value for name '\" + name + \"'\");\n" +
                        "    }\n" +
                        "    return value;\n" +
                        "}",
                        javaClassName,
                        Objects.class.getSimpleName(),
                        javaClassName,
                        IllegalArgumentException.class.getSimpleName(),
                        descriptor.getTypeName());
    }

    @Override
    public void appendConstants(JavaTypeWriter writer, PEnumDescriptor descriptor) throws GeneratorException {
        writer.newline()
              .formatln("public static final %s kDescriptor = new %s();",
                        InnerClassGenerator.DESCRIPTOR, InnerClassGenerator.DESCRIPTOR);

        writer.newline();
        writer.formatln("public static %s<%s> provider() {\n" +
                        "    return () -> kDescriptor;\n" +
                        "}",
                        Supplier.class.getSimpleName(),
                        InnerClassGenerator.DESCRIPTOR,
                        JavaTypeHelper.javaClassName(descriptor));
    }
}
