/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.services.gen;

import io.pvdnc.core.rpc.PServiceProcessor;
import io.pvdnc.core.types.PServiceDescriptor;
import io.pvdnc.idl.generator.InnerClassGenerator;
import io.pvdnc.idl.generator.ServiceTypeFormatter;
import io.pvdnc.idl.generator.services.PServiceFormatter;
import io.pvdnc.idl.generator.services.defaults.ServiceProcessorFormatter;
import io.pvdnc.idl.generator.util.JavaAnnotation;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;

import java.io.IOException;
import java.util.Set;

import static net.morimekta.collect.UnmodifiableSet.setOf;

public class ServiceProcessorGenerator extends InnerClassGenerator<PServiceDescriptor, ServiceTypeFormatter> {
    public ServiceProcessorGenerator() {
        addFormatter(new ServiceProcessorFormatter());
        addFormatter(new PServiceFormatter());
    }

    @Override
    public String getInnerClassName() {
        return PROCESSOR;
    }

    @Override
    public Set<String> getClassImports(JavaTypeHelper helper, PServiceDescriptor descriptor) {
        return setOf(PServiceProcessor.class.getName(),
                     IOException.class.getName());
    }

    @Override
    public void appendClass(JavaTypeWriter writer, PServiceDescriptor descriptor) {
        forEachFormatter(f -> f.appendClassAnnotations(writer, descriptor));

        writer.appendln(JavaAnnotation.SUPPRESS_WARNINGS_UNUSED);
        writer.formatln("class %s implements %s {",
                        PROCESSOR,
                        PServiceProcessor.class.getSimpleName())
              .begin();

        forEachFormatter(f -> f.appendFields(writer, descriptor));

        forEachFormatter(f -> f.appendConstructors(writer, descriptor));

        forEachFormatter(f -> f.appendMethods(writer, descriptor));

        forEachFormatter(f -> f.appendExtraProperties(writer, descriptor));

        forEachFormatter(f -> f.appendConstants(writer, descriptor));

        writer.end()
              .formatln("}");
    }
}
