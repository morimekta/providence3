/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.messages.gen;

import io.pvdnc.idl.generator.InnerClassGenerator;
import io.pvdnc.idl.generator.MessageTypeFormatter;
import io.pvdnc.idl.generator.messages.defaults.ExceptionFormatter;
import io.pvdnc.idl.generator.messages.defaults.ImplFormatter;
import io.pvdnc.idl.generator.messages.defaults.ObjectFormatter;
import io.pvdnc.idl.generator.messages.defaults.OneOfIFaceFormatter;
import io.pvdnc.idl.generator.messages.defaults.OneOfImplFormatter;
import io.pvdnc.idl.generator.messages.defaults.PMessageFormatter;
import io.pvdnc.idl.generator.messages.defaults.PUnionFormatter;
import io.pvdnc.idl.generator.util.JavaProperties;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;
import net.morimekta.collect.UnmodifiableList;
import net.morimekta.collect.UnmodifiableMap;
import net.morimekta.collect.UnmodifiableSet;
import net.morimekta.collect.UnmodifiableSortedMap;
import net.morimekta.collect.UnmodifiableSortedSet;
import io.pvdnc.core.property.PDefaultProperties;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.types.PType;
import io.pvdnc.core.utils.BuilderUtil;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import static io.pvdnc.core.property.PDefaultProperties.MESSAGE_VARIANT;
import static io.pvdnc.core.types.PMessageVariant.EXCEPTION;
import static io.pvdnc.core.types.PMessageVariant.INTERFACE;
import static io.pvdnc.core.types.PMessageVariant.UNION;
import static net.morimekta.strings.NamingUtil.Format.PASCAL;
import static net.morimekta.strings.NamingUtil.format;

public class MessageImplGenerator extends InnerClassGenerator<PMessageDescriptor<?>, MessageTypeFormatter> {
    public MessageImplGenerator() {
        addFormatter(new ImplFormatter(IMPL));
        addFormatter(new PMessageFormatter(IMPL));
        addFormatter(new PUnionFormatter());
        addFormatter(new OneOfImplFormatter(IMPL));
        addFormatter(new ExceptionFormatter());
        addFormatter(new ObjectFormatter(IMPL));
    }

    @Override
    public String getInnerClassName() {
        return IMPL;
    }

    @Override
    public Set<String> getClassImports(JavaTypeHelper helper, PMessageDescriptor<?> descriptor) {
        Set<String> imports = new HashSet<>();
        for (PField field : descriptor.allFields()) {
            switch (field.getResolvedDescriptor().getType()) {
                case MESSAGE: {
                    imports.add(BuilderUtil.class.getName());
                    break;
                }
                case LIST: {
                    PListDescriptor<?> list = (PListDescriptor<?>) field.getResolvedDescriptor();
                    if (list.getItemType().getType() == PType.MESSAGE) {
                        imports.add(BuilderUtil.class.getName());
                    } else {
                        imports.add(UnmodifiableList.class.getName());
                    }
                    break;
                }
                case SET: {
                    PSetDescriptor<?> set = (PSetDescriptor<?>) field.getResolvedDescriptor();
                    if (set.isSorted()) {
                        imports.add(UnmodifiableSortedSet.class.getName());
                    } else {
                        imports.add(UnmodifiableSet.class.getName());
                    }
                    break;
                }
                case MAP: {
                    PMapDescriptor<?,?> map = (PMapDescriptor<?,?>) field.getResolvedDescriptor();
                    if (map.getValueType().getType() == PType.MESSAGE) {
                        imports.add(BuilderUtil.class.getName());
                    } else if (map.isSorted()) {
                        imports.add(UnmodifiableSortedMap.class.getName());
                    } else {
                        imports.add(UnmodifiableMap.class.getName());
                    }
                    break;
                }
                default:
                    break;
            }
        }
        return imports;
    }

    private String makeImmutable(PDescriptor descriptor, String param) {
        switch (descriptor.getType()) {
            case MESSAGE: {
                return BuilderUtil.class.getSimpleName() + ".toImmutableIfNotNull(" + param + ");";
            }
            case LIST: {
                PListDescriptor<?> list = (PListDescriptor<?>) descriptor;
                if (list.getItemType().getType() == PType.MESSAGE) {
                    return BuilderUtil.class.getSimpleName() + ".toImmutableAll(" + param + ")";
                }
                return param + " == null ? null : " + UnmodifiableList.class.getSimpleName() + ".asList(" + param + ")";
            }
            case SET: {
                PSetDescriptor<?> set = (PSetDescriptor<?>) descriptor;
                if (set.isSorted()) {
                    return param + " == null ? null : " + UnmodifiableSortedSet.class.getSimpleName() + ".asSortedSet(" + param + ")";
                }
                return param + " == null ? null : " + UnmodifiableSet.class.getSimpleName() + ".asSet(" + param + ")";
            }
            case MAP: {
                PMapDescriptor<?,?> map = (PMapDescriptor<?,?>) descriptor;
                if (map.getValueType().getType() == PType.MESSAGE) {
                    return BuilderUtil.class.getSimpleName() + ".toImmutableValues(" + param + ")";
                }
                if (map.isSorted()) {
                    return param + " == null ? null : " + UnmodifiableSortedMap.class.getSimpleName() + ".asSortedMap(" + param + ")";
                }
                return param + " == null ? null : " + UnmodifiableMap.class.getSimpleName() + ".asMap(" + param + ")";
            }
            default: {
                return param;
            }
        }
    }

    @Override
    public boolean isEnabled(PMessageDescriptor<?> descriptor) {
        return descriptor.getProperty(MESSAGE_VARIANT) != INTERFACE;
    }

    @Override
    public void appendClass(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        JavaTypeHelper helper = writer.getHelper();

        forEachFormatter(f -> f.appendClassAnnotations(writer, descriptor));
        writer.formatln("class %s", getInnerClassName());
        if (descriptor.getProperty(MESSAGE_VARIANT) == EXCEPTION) {
            // TODO: Handle exception class property.
            writer.formatln("        extends %s", descriptor.getProperty(JavaProperties.JAVA_EXCEPTION_CLASS));
        }
        writer.formatln("        implements %s", helper.javaTypeReference(descriptor))
              .begin().begin("               ");
        Set<String> extraImplements = new TreeSet<>();
        forEachFormatter(f -> extraImplements.addAll(f.getExtraImplements(helper, descriptor)));
        extraImplements.forEach(i -> writer.append(",").appendln(i));
        writer.append(" {").end();

        appendFields(writer, descriptor);

        forEachFormatter(f -> f.appendFields(writer, descriptor));

        appendConstructor(writer, descriptor);

        for (PField field : descriptor.allFields()) {
            forEachFormatter(f -> f.appendFieldMethods(writer, descriptor, field));
        }

        forEachFormatter(f -> f.appendMethods(writer, descriptor));
        forEachFormatter(f -> f.appendConstructors(writer, descriptor));
        forEachFormatter(f -> f.appendExtraProperties(writer, descriptor));
        forEachFormatter(f -> f.appendConstants(writer, descriptor));

        writer.end().appendln("}");
    }

    private void appendFields(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        JavaTypeHelper helper = writer.getHelper();
        for (PField field : descriptor.allFields()) {
            writer.formatln("private final %s v%s;",
                            helper.getValueClass(field.getResolvedDescriptor()),
                            format(field.getName(), PASCAL));
        }
        if (descriptor.getProperty(MESSAGE_VARIANT) == UNION) {
            writer.formatln("private final int tUnionField;");
        }
    }

    private void appendConstructor(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        boolean isUnion = descriptor.getProperty(MESSAGE_VARIANT) == UNION;
        boolean isException = descriptor.getProperty(MESSAGE_VARIANT) == EXCEPTION;

        JavaTypeHelper helper = writer.getHelper();
        writer.newline();
        if (descriptor.getProperty(JavaProperties.JAVA_PUBLIC_CONSTRUCTOR)) {
            writer.formatln("public %s(", IMPL)
                  .begin()
                  .begin(" ".repeat(IMPL.length() + 4));
        } else {
            writer.formatln("protected %s(", IMPL)
                  .begin()
                  .begin(" ".repeat(IMPL.length() + 7));
        }
        boolean first = true;
        for (PField field : descriptor.allFields()) {
            if (first) {
                first = false;
            } else {
                writer.append(",");
                writer.appendln();
            }
            writer.format("%s p%s",
                          helper.getValueClass(field.getResolvedDescriptor()),
                          format(field.getName(), PASCAL));
        }
        if (isUnion) {
            if (!first) {
                writer.append(",");
                writer.appendln();
            }
            writer.append("int uUnionField");
        } else {
            for (String group : OneOfIFaceFormatter.oneOfGroups(descriptor)) {
                writer.append(',')
                      .formatln("int %s", OneOfIFaceFormatter.oneOfParam(group));
            }
        }
        writer.append(") {").end();
        if (isException) {
            // TODO: Call to super with exception message field.
            String messageFieldName = descriptor.getProperty(PDefaultProperties.EXCEPTION_MESSAGE);
            PField messageField = descriptor.findFieldByName(messageFieldName);
            if (messageField != null) {
                writer.formatln("super(p%s);", format(messageField.getName(), PASCAL));
            }
        }
        for (PField field : descriptor.allFields()) {
            writer.formatln("v%s = %s;",
                            format(field.getName(), PASCAL),
                            makeImmutable(field.getResolvedDescriptor(), "p" + format(field.getName(), PASCAL)));
        }
        if (isUnion) {
            writer.formatln("tUnionField = uUnionField;");
        } else {
            for (String group : OneOfIFaceFormatter.oneOfGroups(descriptor)) {
                writer.formatln("%s = %s;", OneOfIFaceFormatter.oneOfField(group), OneOfIFaceFormatter.oneOfParam(group));
            }
        }
        writer.end()
              .appendln("}");
    }
}
