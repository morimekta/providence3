/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.messages.defaults;

import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PType;
import io.pvdnc.idl.generator.InnerClassGenerator;
import io.pvdnc.idl.generator.MessageTypeFormatter;
import io.pvdnc.idl.generator.util.JavaAnnotation;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;

import static io.pvdnc.core.property.PDefaultProperties.MESSAGE_VARIANT;
import static io.pvdnc.core.types.PMessageVariant.UNION;
import static io.pvdnc.idl.generator.messages.defaults.OneOfIFaceFormatter.oneOfField;
import static io.pvdnc.idl.generator.messages.defaults.OneOfIFaceFormatter.oneOfGroups;
import static net.morimekta.strings.NamingUtil.Format.PASCAL;
import static net.morimekta.strings.NamingUtil.format;

public class PMessageBuilderFormatter implements MessageTypeFormatter {
    @Override
    public void appendFields(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        writer.newline();
        for (PField field : descriptor.allFields()) {
            writer.formatln("private boolean m%s;", format(field.getName(), PASCAL));
        }
    }

    @Override
    public void appendMethods(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        JavaTypeHelper helper = writer.getHelper();

        // ---------------- SET ----------------

        writer.newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .appendln(JavaAnnotation.SUPPRESS_WARNINGS_UNCHECKED)
              .appendln("public void set(int id, Object value) {")
              .begin()
              .appendln("switch (id) {")
              .begin();

        for (PField field : descriptor.allFields()) {
            if (field.getType() == PType.VOID) {
                writer.formatln("case %d: {\n" +
                                "    if (value == Boolean.TRUE) set%s();\n" +
                                "    else clear%s();\n" +
                                "    break;\n" +
                                "}",
                                field.getId(),
                                format(field.getName(), PASCAL),
                                format(field.getName(), PASCAL));
            } else {
                writer.formatln("case %d: set%s((%s) value); break;",
                                field.getId(),
                                format(field.getName(), PASCAL),
                                writer.getHelper().getSetterParamType(field.getResolvedDescriptor()));
            }
        }

        writer.formatln("default: throw new IllegalArgumentException(\"Unknown field id \" + id + \" on %s\");",
                        descriptor.getTypeName())
              .end()
              .appendln('}')
              .end()
              .appendln('}');

        // ---------------- CLEAR ----------------

        writer.newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .appendln("public void clear(int id) {")
              .begin()
              .appendln("switch (id) {")
              .begin();

        for (PField field : descriptor.allFields()) {
            writer.formatln("case %d: clear%s(); break;",
                            field.getId(),
                            format(field.getName(), PASCAL),
                            writer.getHelper().getSetterParamType(field.getResolvedDescriptor()));
        }
        writer.appendln("default: break;");

        writer.end()
              .appendln('}')
              .end()
              .appendln('}');

        // ---------------- MODIFIED ----------------

        writer.newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .appendln("public boolean modified(int id) {")
              .begin()
              .appendln("switch (id) {")
              .begin();

        for (PField field : descriptor.allFields()) {
            writer.formatln("case %d: return m%s;",
                            field.getId(),
                            format(field.getName(), PASCAL),
                            writer.getHelper().getSetterParamType(field.getResolvedDescriptor()));
        }

        writer.appendln("default: return false;")
              .end()
              .appendln('}')
              .end()
              .appendln('}');

        // ---------------- BUILD ----------------

        writer.newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .formatln("public %s build() {", helper.javaTypeReference(descriptor, InnerClassGenerator.IMPL))
              .begin();

        writer.formatln("return new %s(", helper.javaTypeReference(descriptor, InnerClassGenerator.IMPL))
              .begin().begin();

        boolean first = true;
        for (PField field : descriptor.allFields()) {
            if (first) first = false;
            else writer.append(',');
            writer.formatln("v%s", format(field.getName(), PASCAL));
        }
        if (descriptor.getProperty(MESSAGE_VARIANT) == UNION) {
            if (!first) {
                writer.append(',');
            }
            writer.appendln("tUnionField");
        } else {
            for (String group : oneOfGroups(descriptor)) {
                writer.append(',')
                      .appendln(oneOfField(group));
            }
        }

        writer.end().end().append(");");

        writer.end()
              .appendln('}');
    }
}
