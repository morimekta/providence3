/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.messages.defaults;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PMessageVariant;
import io.pvdnc.core.types.PType;
import io.pvdnc.idl.generator.InnerClassGenerator;
import io.pvdnc.idl.generator.MessageTypeFormatter;
import io.pvdnc.idl.generator.util.BlockCommentWriter;
import io.pvdnc.idl.generator.util.JavaAnnotation;
import io.pvdnc.idl.generator.util.JavaProperties;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;

import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;

import static io.pvdnc.core.property.PDefaultProperties.DEPRECATED;
import static io.pvdnc.core.property.PDefaultProperties.DOCUMENTATION;
import static io.pvdnc.core.property.PDefaultProperties.MESSAGE_VARIANT;
import static net.morimekta.collect.UnmodifiableSet.asSet;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static net.morimekta.strings.NamingUtil.Format.PASCAL;
import static net.morimekta.strings.NamingUtil.format;

public class IFaceFormatter implements MessageTypeFormatter {
    @Override
    public Set<String> getImportClasses(JavaTypeHelper helper, PMessageDescriptor<?> descriptor) {
        return setOf(PMessage.class.getName(),
                     PMessageDescriptor.class.getName(),
                     Optional.class.getName());
    }

    @Override
    public Set<String> getExtraImplements(JavaTypeHelper helper, PMessageDescriptor<?> descriptor) {
        if (descriptor.hasProperty(JavaProperties.JAVA_EXTRA_IMPLEMENTS)) {
            String[] extra = descriptor.getProperty(JavaProperties.JAVA_EXTRA_IMPLEMENTS).split(",");
            return asSet(extra);
        }
        return setOf();
    }

    @Override
    public void appendClassAnnotations(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        writer.appendln(JavaAnnotation.SUPPRESS_WARNINGS_UNUSED);
    }

    @Override
    public void appendFieldMethods(JavaTypeWriter writer, PMessageDescriptor<?> descriptor, PField field) {
        if (!descriptor.equals(field.getOnMessageType())) {
            return;
        }

        JavaTypeHelper helper = writer.getHelper();

        if (field.getType() != PType.VOID) {
            writer.newline();
            try (BlockCommentWriter comment = new BlockCommentWriter(writer)) {
                if (field.hasProperty(DOCUMENTATION)) {
                    comment.comment(field.getProperty(DOCUMENTATION));
                }
                comment.newline()
                       .return_("Optional " + field.getName() + " value.");
                if (field.hasProperty(DEPRECATED)) {
                    comment.deprecated_(field.getProperty(DEPRECATED));
                }
            }
            writer.formatln("%s<%s> optional%s();",
                            Optional.class.getSimpleName(),
                            helper.getValueClass(field.getResolvedDescriptor()),
                            format(field.getName(), PASCAL));

            writer.newline();
            try (BlockCommentWriter comment = new BlockCommentWriter(writer)) {
                if (field.hasProperty(DOCUMENTATION)) {
                    comment.comment(field.getProperty(DOCUMENTATION));
                }
                comment.newline()
                       .return_("The " + field.getName() + " value or default.");
                if (field.hasProperty(DEPRECATED)) {
                    comment.deprecated_(field.getProperty(DEPRECATED));
                }
            }
            writer.formatln("%s get%s();",
                            helper.getValueType(field.getResolvedDescriptor()),
                            format(field.getName(), PASCAL));
        }

        writer.newline();
        try (BlockCommentWriter comment = new BlockCommentWriter(writer)) {
            if (field.hasProperty(DOCUMENTATION)) {
                comment.comment(field.getProperty(DOCUMENTATION));
            }
            comment.newline()
                   .return_("If the " + field.getName() + " value is set.");
            if (field.hasProperty(DEPRECATED)) {
                comment.deprecated_(field.getProperty(DEPRECATED));
            }
        }
        writer.formatln("boolean has%s();", format(field.getName(), PASCAL));
    }

    @Override
    public void appendMethods(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        JavaTypeHelper helper = writer.getHelper();

        writer.newline()
              .appendln("// ---- PMessage ----");

        writer.newline();
        try (BlockCommentWriter comment = new BlockCommentWriter(writer)) {
            comment.return_("The " + descriptor.getTypeName() + " builder.");
        }
        writer.appendln(JavaAnnotation.OVERRIDE)
              .formatln("%s.Builder mutate();", helper.javaTypeReference(descriptor));

        writer.newline();
        try (BlockCommentWriter comment = new BlockCommentWriter(writer)) {
            comment.return_("The " + descriptor.getTypeName() + " descriptor.");
        }
        writer.appendln(JavaAnnotation.OVERRIDE);
        writer.formatln("%s<? extends %s> $descriptor();",
                        PMessageDescriptor.class.getSimpleName(),
                        helper.javaTypeReference(descriptor));
    }

    @Override
    public void appendConstants(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        writer.newline()
              .formatln("%s kDescriptor = new %s();",
                        InnerClassGenerator.DESCRIPTOR,
                        InnerClassGenerator.DESCRIPTOR);

    }

    @Override
    public void appendExtraProperties(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        if (descriptor.getProperty(MESSAGE_VARIANT) != PMessageVariant.INTERFACE) {
            writer.newline()
                  .formatln("static %s.Builder newBuilder() {\n" +
                            "    return kDescriptor.newBuilder();\n" +
                            "}",
                            writer.getHelper().javaTypeReference(descriptor));

            writer.newline()
                  .formatln("static %s empty() {\n" +
                            "    return %s.kEmpty;\n" +
                            "}",
                            writer.getHelper().javaTypeReference(descriptor),
                            InnerClassGenerator.IMPL);
        }
        writer.newline()
              .formatln("static %s<%s> provider() {\n" +
                        "    return () -> kDescriptor;\n" +
                        "}",
                        Supplier.class.getSimpleName(),
                        InnerClassGenerator.DESCRIPTOR);
    }
}
