/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.type_defs;

import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.registry.PGlobalFileTypeRegistry;
import io.pvdnc.core.types.PTypeDefDescriptor;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;
import io.pvdnc.idl.generator.GeneratorOptions;
import io.pvdnc.idl.generator.TypeDefTypeFormatter;
import io.pvdnc.idl.generator.TypeGenerator;
import io.pvdnc.idl.generator.util.ClassFileManager;

import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

import static net.morimekta.collect.UnmodifiableSortedSet.sortedSetOf;
import static net.morimekta.collect.util.SetOperations.union;
import static net.morimekta.strings.EscapeUtil.javaEscape;

public class TypeDefGenerator extends TypeGenerator<PTypeDefDescriptor, TypeDefTypeFormatter> {
    public TypeDefGenerator(ClassFileManager manager, PGlobalFileTypeRegistry global, GeneratorOptions options) {
        super(manager, global, options);
    }

    @Override
    public Set<String> getClassImports(JavaTypeHelper helper, PTypeDefDescriptor descriptor) {
        return union(
                sortedSetOf(
                        PTypeDefDescriptor.class.getName(),
                        PPropertyMap.class.getName(),
                        Supplier.class.getName()),
                helper.getTypeIncludes(descriptor.getDescriptor()));
    }

    @Override
    public void appendClassStart(JavaTypeWriter writer, PTypeDefDescriptor descriptor) {
        JavaTypeHelper helper = writer.getHelper();

        writer.formatln("public final class %s extends %s {", writer.getJavaClassName(), PTypeDefDescriptor.class.getSimpleName())
                .begin();

        writer.formatln("public static final %s kDescriptor = new %s();",
                writer.getJavaClassName(), writer.getJavaClassName());
        writer.formatln("public static %s<%s> provider() {\n" +
                        "    return () -> kDescriptor;\n" +
                        "}", Supplier.class.getSimpleName(), writer.getJavaClassName());
        writer.newline();

        writer.formatln("private %s() {\n" +
                "    super(\"%s\", \"%s\", %s.of(",
                writer.getJavaClassName(),
                descriptor.getNamespace(),
                descriptor.getSimpleName(),
                PPropertyMap.class.getSimpleName())
        .begin("        ").begin("        ");
        boolean first = true;
        for (Map.Entry<String, String> entry : descriptor.getUncheckedProperties().entrySet()) {
            if (first) first = false;
            else writer.append(',');
            writer.formatln("\"%s\", \"%s\"", entry.getKey(), javaEscape(entry.getValue()));
        }
        writer.end()
                .format("), %s);", helper.getDescriptorProvider(descriptor.getDescriptor()))
                .end()
                .appendln("}");
    }
}
