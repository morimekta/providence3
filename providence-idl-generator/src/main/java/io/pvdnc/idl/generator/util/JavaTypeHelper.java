/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.util;

import io.pvdnc.core.PAny;
import net.morimekta.collect.util.Binary;
import io.pvdnc.core.impl.Providence_FileDescriptor;
import io.pvdnc.core.registry.PGlobalFileTypeRegistry;
import io.pvdnc.core.registry.PNamespaceFileTypeRegistry;
import io.pvdnc.core.registry.PNamespaceTypeRegistry;
import io.pvdnc.core.types.PDeclaredDescriptor;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PServiceDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.types.PType;
import io.pvdnc.core.types.PTypeDefDescriptor;
import io.pvdnc.idl.generator.GeneratorException;
import io.pvdnc.idl.generator.GeneratorOptions;
import net.morimekta.strings.NamingUtil;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static net.morimekta.collect.util.SetOperations.union;
import static io.pvdnc.idl.generator.InnerClassGenerator.MAIN;
import static io.pvdnc.idl.generator.namespaces.NamespaceGenerator.FILE_DESCRIPTOR;
import static io.pvdnc.idl.generator.util.JavaProperties.JAVA_PACKAGE_NAME;
import static net.morimekta.strings.StringUtil.isNullOrEmpty;

/**
 * A print writer for a single class, with utilities for getting common
 * info related to the class code location.
 */
public class JavaTypeHelper {
    private final PNamespaceTypeRegistry  registry;
    private final PGlobalFileTypeRegistry global;
    private final GeneratorOptions        options;
    private final String                  javaPackage;
    private final String                  javaClassName;
    private final HashSet<Object>         reservedNames;

    public JavaTypeHelper(PGlobalFileTypeRegistry global,
                          PNamespaceFileTypeRegistry registry,
                          GeneratorOptions options,
                          String javaClassName) {
        this.global = requireNonNull(global, "global == null");
        this.registry = requireNonNull(registry, "registry == null");
        this.options = requireNonNull(options, "options == null");

        this.javaPackage = registry.getProperty(JAVA_PACKAGE_NAME);
        if (this.javaPackage == null) {
            throw new GeneratorException("Not set up for java generation: " + registry.getPath());
        }
        this.javaClassName = javaClassName;
        this.reservedNames = new HashSet<>(kDefaultReservedNames);
    }

    public PNamespaceTypeRegistry getRegistry() {
        return registry;
    }

    public GeneratorOptions getOptions() {
        return options;
    }

    public String getJavaPackage() {
        return javaPackage;
    }

    public String getJavaClassName() {
        return javaClassName;
    }

    /**
     * The value type is the return type of the "get*" method, which is the most
     * non-nullable type for the descriptor. But also as specific as possible, e.g
     * with generics for collections.
     *
     * @param descriptor The type to get value type for.
     * @return The value type string.
     */
    public String getValueType(PDescriptor descriptor) {
        switch (descriptor.getType()) {
            case VOID:
                if (descriptor instanceof PTypeDefDescriptor) {
                    return getValueType(((PTypeDefDescriptor) descriptor).getDescriptor());
                }
                return Void.TYPE.getSimpleName();
            case BOOL:
                return Boolean.TYPE.getSimpleName();
            case BYTE:
                return Byte.TYPE.getSimpleName();
            case SHORT:
                return Short.TYPE.getSimpleName();
            case INT:
                return Integer.TYPE.getSimpleName();
            case LONG:
                return Long.TYPE.getSimpleName();
            case FLOAT:
                return Float.TYPE.getSimpleName();
            case DOUBLE:
                return Double.TYPE.getSimpleName();
            default:
                return getValueClass(descriptor);
        }
    }

    public Set<String> getDescriptorImports(PDescriptor descriptor, PDeclaredDescriptor from) {
        if (descriptor.equals(from)) {
            return setOf();
        }

        HashSet<String> out = new HashSet<>();
        if (descriptor instanceof PDeclaredDescriptor) {
            PNamespaceTypeRegistry ns = namespaceOfType((PDeclaredDescriptor) descriptor);
            String pkg = javaPackagePrefix(ns);
            if (!pkg.equals(javaPackagePrefix())) {
                out.add(pkg + javaClassName((PDeclaredDescriptor) descriptor));
            }
        } else if (descriptor instanceof PListDescriptor) {
            out.add(PListDescriptor.class.getName());
            PListDescriptor<?> list = (PListDescriptor<?>) descriptor;
            out.addAll(getDescriptorImports(list.getItemType(), from));
        } else if (descriptor instanceof PSetDescriptor) {
            out.add(PSetDescriptor.class.getName());
            PSetDescriptor<?> set = (PSetDescriptor<?>) descriptor;
            out.addAll(getDescriptorImports(set.getItemType(), from));
        } else if (descriptor instanceof PMapDescriptor) {
            out.add(PMapDescriptor.class.getName());
            PMapDescriptor<?,?> set = (PMapDescriptor<?,?>) descriptor;
            out.addAll(getDescriptorImports(set.getKeyType(), from));
            out.addAll(getDescriptorImports(set.getValueType(), from));
        } else {
            out.add(PType.class.getName());
        }
        return out;
    }


    /**
     * The value type is the return type of the "get*" method, which is the most
     * non-nullable type for the descriptor. But also as specific as possible, e.g
     * with generics for collections.
     *
     * @param descriptor The type to get value type for.
     * @return The value type string.
     */
    public String getMutableType(PDescriptor descriptor) {
        if (descriptor.getType() == PType.MESSAGE) {
            return javaTypeReference((PDeclaredDescriptor) descriptor) + ".Builder";
        }
        return getValueType(descriptor);
    }

    /**
     * The value type is the class type of the "optional*" method's value type. Also
     * used in setter methods and the value field types.
     *
     * @param descriptor The type to get value type for.
     * @return The value type string.
     */
    public String getValueClass(PDescriptor descriptor) {
        switch (descriptor.getType()) {
            case VOID:  // uses Boolean as return and generic setter type.
            case BOOL:
                return Boolean.class.getSimpleName();
            case BYTE:
                return Byte.class.getSimpleName();
            case SHORT:
                return Short.class.getSimpleName();
            case INT:
                return Integer.class.getSimpleName();
            case LONG:
                return Long.class.getSimpleName();
            case FLOAT:
                return Float.class.getSimpleName();
            case DOUBLE:
                return Double.class.getSimpleName();
            case STRING:
                return String.class.getSimpleName();
            case BINARY:
                return Binary.class.getName();
            case LIST:
                PListDescriptor<?> lType = (PListDescriptor<?>) descriptor;
                return String.format(Locale.US, "%s<%s>",
                                     List.class.getName(),
                                     getValueClass(lType.getItemType()));
            case SET:
                PSetDescriptor<?> sType = (PSetDescriptor<?>) descriptor;
                return String.format(Locale.US, "%s<%s>",
                                     sType.isSorted() ? SortedSet.class.getTypeName() : Set.class.getName(),
                                     getValueClass(sType.getItemType()));
            case MAP:
                PMapDescriptor<?, ?> mType = (PMapDescriptor<?, ?>) descriptor;
                return String.format(Locale.US, "%s<%s,%s>",
                                     mType.isSorted() ? SortedMap.class.getTypeName() : Map.class.getName(),
                                     getValueClass(mType.getKeyType()),
                                     getValueClass(mType.getValueType()));
            case ENUM:
            case MESSAGE:
                return javaTypeReference((PDeclaredDescriptor) descriptor);
            case ANY:
                return PAny.class.getName();
        }
        throw new GeneratorException("Unhandled type group" + descriptor.getType());
    }

    /**
     * Get the string making the type provider for fields etc.
     *
     * @param descriptor The type to get value type for.
     * @return The value type string.
     */
    public String getDescriptorProvider(PDescriptor descriptor) {
        switch (descriptor.getType()) {
            case VOID:
                if (descriptor instanceof PServiceDescriptor) {
                    return javaTypeReference((PDeclaredDescriptor) descriptor) + ".provider()";
                } else if (descriptor instanceof PTypeDefDescriptor) {
                    return javaTypeReference((PDeclaredDescriptor) descriptor) + ".provider()";
                }
                // uses Boolean as return and generic setter type.
            case BOOL:
            case BYTE:
            case SHORT:
            case INT:
            case LONG:
            case FLOAT:
            case DOUBLE:
            case STRING:
            case BINARY:
            case ANY:
                return PType.class.getSimpleName() + "." + descriptor.getType().name() + ".provider()";
            case LIST:
                PListDescriptor<?> lType = (PListDescriptor<?>) descriptor;
                return String.format(Locale.US, "%s.provider(%s)",
                                     PListDescriptor.class.getSimpleName(),
                                     getDescriptorProvider(lType.getItemType()));
            case SET: {
                PSetDescriptor<?> sType          = (PSetDescriptor<?>) descriptor;
                String            providerMethod = "provider";
                if (sType.isSorted()) {
                    providerMethod = "sortedProvider";
                }
                return String.format(Locale.US, "%s.%s(%s)",
                                     PSetDescriptor.class.getSimpleName(),
                                     providerMethod,
                                     getDescriptorProvider(sType.getItemType()));
            }
            case MAP: {
                PMapDescriptor<?, ?> mType          = (PMapDescriptor<?, ?>) descriptor;
                String               providerMethod = "provider";
                if (mType.isSorted()) {
                    providerMethod = "sortedProvider";
                }
                return String.format(Locale.US, "%s.%s(%s,%s)",
                                     PMapDescriptor.class.getSimpleName(),
                                     providerMethod,
                                     getDescriptorProvider(mType.getKeyType()),
                                     getDescriptorProvider(mType.getValueType()));
            }
            case ENUM:
            case MESSAGE:
                return javaTypeReference((PDeclaredDescriptor) descriptor) + ".provider()";
        }
        throw new GeneratorException("Unhandled type group" + descriptor.getType());
    }

    /**
     * The setter param type is the type of the param of the setter method. If is
     * mostly the same as the type class, but uses more generic types for lists
     * and sets.
     *
     * @param descriptor The type to get value type for.
     * @return The value type string.
     */
    public String getSetterParamType(PDescriptor descriptor) {
        switch (descriptor.getType()) {
            case LIST:
                PListDescriptor<?> lType = (PListDescriptor<?>) descriptor;
                return String.format(Locale.US, "%s<%s>",
                                     Collection.class.getName(),
                                     getValueClass(lType.getItemType()));
            case SET:
                PSetDescriptor<?> sType = (PSetDescriptor<?>) descriptor;
                return String.format(Locale.US, "%s<%s>",
                                     Collection.class.getName(),
                                     getValueClass(sType.getItemType()));
            case MAP:
                PMapDescriptor<?, ?> mType = (PMapDescriptor<?, ?>) descriptor;
                return String.format(Locale.US, "%s<%s,%s>",
                                     Map.class.getName(),
                                     getValueClass(mType.getKeyType()),
                                     getValueClass(mType.getValueType()));
            default:
                return getValueClass(descriptor);
        }
    }

    public Set<String> getTypeIncludes(PDescriptor descriptor) {
        switch (descriptor.getType()) {
            case VOID:
            case MESSAGE:
            case ENUM:
            case ANY:
                // always direct reference.
                return Set.of();
            case LIST: {
                PListDescriptor<?> ld = (PListDescriptor<?>) descriptor;
                return union(
                        Set.of(PListDescriptor.class.getName()),
                        getTypeIncludes(ld.getItemType()));
            }
            case SET: {
                PSetDescriptor<?> sd = (PSetDescriptor<?>) descriptor;
                return union(Set.of(PSetDescriptor.class.getName()),
                        getTypeIncludes(sd.getItemType()));
            }
            case MAP: {
                PMapDescriptor<?,?> sd = (PMapDescriptor<?,?>) descriptor;
                return union(Set.of(PMapDescriptor.class.getName()),
                        getTypeIncludes(sd.getKeyType()),
                        getTypeIncludes(sd.getValueType()));
            }
            default:
                return Set.of(PType.class.getName());
        }
    }

    public String javaFileDescriptorReference(PNamespaceTypeRegistry descriptor) {
        String myPackage = javaPackagePrefix(registry);
        String otherPackage = javaPackagePrefix(descriptor);
        if (myPackage.equals(otherPackage)) {
            return javaFileDescriptorClassName(descriptor);
        }
        return otherPackage + javaFileDescriptorClassName(descriptor);
    }

    public String javaFileDescriptorClassName(PNamespaceTypeRegistry descriptor) {
        return NamingUtil.format(descriptor.getNamespace(), NamingUtil.Format.PASCAL) + FILE_DESCRIPTOR;
    }

    public String javaTypeReference(PDeclaredDescriptor descriptor) {
        return javaTypeReference(descriptor, MAIN);
    }

    public String javaTypeReference(PDeclaredDescriptor descriptor, String innerClass) {
        PNamespaceTypeRegistry ns = namespaceOfType(descriptor);
        String pkg = javaPackagePrefix(ns);
        String className = javaClassName(descriptor);

        String innerClassRef = innerClass == null || innerClass.isEmpty() ? innerClass : "." + innerClass;
        if ((ns == registry || pkg.equals(javaPackagePrefix(registry))) &&
            !reservedNames.contains(className)) {
            return className + innerClassRef;
        }
        return pkg + className + innerClassRef;
    }

    public String javaPackagePrefix(PNamespaceTypeRegistry reg) {
        String pkg = reg.getProperty(JAVA_PACKAGE_NAME);
        if (isNullOrEmpty(pkg)) return "";
        return pkg + ".";
    }

    public String javaPackagePrefix() {
        if (isNullOrEmpty(javaPackage)) return "";
        return javaPackage + ".";
    }

    private static final Set<String> kDefaultReservedNames = setOf("Builder", "List", "Set", "Map");

    public static String javaClassName(PDeclaredDescriptor descriptor) {
        return javaClassName(descriptor, MAIN);
    }

    public static String javaClassName(PDeclaredDescriptor descriptor, String innerClass) {
        String[] parts = descriptor.getSimpleName().split("[.]");
        return Arrays.stream(parts)
                     .map(name -> NamingUtil.format(name, NamingUtil.Format.PASCAL))
                     .collect(Collectors.joining("_")) +
               (innerClass == null || innerClass.isEmpty() ? innerClass : "." + innerClass);
    }

    private PNamespaceTypeRegistry namespaceOfType(PDeclaredDescriptor descriptor) {
        if (descriptor.getNamespace().equals(registry.getNamespace())) {
            if (registry.declaredDescriptors().contains(descriptor)) {
                return registry;
            }
        }
        for (PNamespaceTypeRegistry included : global.getRegistriesForNamespace(descriptor.getNamespace())) {
            if (included.declaredDescriptors().contains(descriptor)) {
                return included;
            }
        }
        if (Providence_FileDescriptor.kDescriptor.declaredDescriptors().contains(descriptor)) {
            return Providence_FileDescriptor.kDescriptor;
        }
        throw new GeneratorException("No registry for type: " + descriptor.getTypeName());
    }

    public void addReservedNames(Set<String> names) {
        reservedNames.addAll(requireNonNull(names, "names == null"));
    }
}
