/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.messages.defaults;

import io.pvdnc.core.PUnion;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PMessageVariant;
import io.pvdnc.core.types.PType;
import io.pvdnc.idl.generator.GeneratorException;
import io.pvdnc.idl.generator.MessageTypeFormatter;
import io.pvdnc.idl.generator.util.BlockCommentWriter;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;

import java.util.Objects;
import java.util.Set;

import static net.morimekta.collect.UnmodifiableSet.setOf;
import static io.pvdnc.core.property.PDefaultProperties.DEPRECATED;
import static io.pvdnc.core.property.PDefaultProperties.DOCUMENTATION;
import static io.pvdnc.core.property.PDefaultProperties.MESSAGE_VARIANT;
import static net.morimekta.strings.NamingUtil.Format.PASCAL;
import static net.morimekta.strings.NamingUtil.format;

public class PUnionIFaceFormatter implements MessageTypeFormatter {
    @Override
    public Set<String> getImportClasses(JavaTypeHelper helper, PMessageDescriptor<?> descriptor) {
        if (notUnion(descriptor)) {
            return setOf();
        }
        return setOf(PUnion.class.getName());
    }

    @Override
    public Set<String> getExtraImplements(JavaTypeHelper helper, PMessageDescriptor<?> descriptor) {
        if (notUnion(descriptor)) {
            return setOf();
        }
        return setOf(PUnion.class.getSimpleName());
    }

    @Override
    public void appendConstructors(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) throws GeneratorException {
        if (notUnion(descriptor)) {
            return;
        }
        JavaTypeHelper helper = writer.getHelper();
        for (PField field : descriptor.allFields()) {
            String value = "value";
            String param = String.format("%s value", helper.getValueType(field.getResolvedDescriptor()));
            writer.newline();
            try (BlockCommentWriter comment = new BlockCommentWriter(writer)) {
                comment.comment("Get union with specific field.");
                if (field.hasProperty(DOCUMENTATION)) {
                    comment.paragraph()
                           .comment(field.getProperty(DOCUMENTATION));
                }
                if (field.getType() != PType.VOID) {
                    comment.param_("value", "The " + field.getName() + " value.");
                } else {
                    value = "";
                    param = "";
                }
                comment.return_("The union.");
                if (field.hasProperty(DEPRECATED)) {
                    comment.deprecated_(field.getProperty(DEPRECATED));
                }
            }
            String nullCheck = field.getType().getBaseType().isPrimitive()
                               ? ""
                               : String.format("    %s.requireNonNull(value, \"value == null\");%n",
                                               Objects.class.getName());
            writer.formatln("static %s with%s(%s) {\n" +
                            "%s" +
                            "    return newBuilder().set%s(%s).build();\n" +
                            "}",
                            helper.javaTypeReference(descriptor), format(field.getName(), PASCAL), param,
                            nullCheck,
                            format(field.getName(), PASCAL),
                            value);
        }
    }

    private boolean notUnion(PMessageDescriptor<?> descriptor) {
        return descriptor.getProperty(MESSAGE_VARIANT) != PMessageVariant.UNION;
    }
}
