/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator;

import io.pvdnc.idl.generator.util.BlockCommentWriter;
import io.pvdnc.idl.generator.util.ClassFileManager;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;
import net.morimekta.collect.UnmodifiableSortedSet;
import io.pvdnc.core.property.PDefaultProperties;
import io.pvdnc.core.registry.PGlobalFileTypeRegistry;
import io.pvdnc.core.registry.PNamespaceFileTypeRegistry;
import io.pvdnc.core.types.PDeclaredDescriptor;
import net.morimekta.strings.io.IndentedPrintWriter;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.requireNonNull;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static io.pvdnc.core.property.PDefaultProperties.DOCUMENTATION;

public abstract class TypeGenerator<
        DescriptorType extends PDeclaredDescriptor,
        Formatter extends TypeFormatter<DescriptorType>>
        implements Generator<DescriptorType> {
    private final Map<String, InnerClassGenerator<DescriptorType, Formatter>> postFixToGenerator;
    private final List<Formatter>                                             mainFormatters;

    protected final ClassFileManager fileManager;
    protected final PGlobalFileTypeRegistry global;
    protected final GeneratorOptions    options;

    public TypeGenerator(ClassFileManager fileManager,
                         PGlobalFileTypeRegistry global,
                         GeneratorOptions options) {
        this.fileManager = fileManager;
        this.global = global;
        this.options = options;
        this.postFixToGenerator = new LinkedHashMap<>();
        this.mainFormatters = new ArrayList<>();
    }

    public void addClass(InnerClassGenerator<DescriptorType, Formatter> generator) {
        if (generator.getInnerClassName().equals(InnerClassGenerator.MAIN)) {
            throw new IllegalArgumentException("Cannot add generator for postfix '' (empty / main)");
        }
        if (postFixToGenerator.containsKey(generator.getInnerClassName())) {
            throw new IllegalArgumentException("Already has generator for " + generator.getInnerClassName());
        }
        postFixToGenerator.put(generator.getInnerClassName(), generator);
    }

    public void addClassFormatter(String postFix, Formatter formatter) {
        if (InnerClassGenerator.MAIN.equals(postFix)) {
            mainFormatters.add(formatter);
        } else if (!postFixToGenerator.containsKey(postFix)) {
            throw new IllegalArgumentException("No generator for postfix '" + postFix + "'");
        } else {
            postFixToGenerator.get(postFix).addFormatter(formatter);
        }
    }

    /**
     * @param helper Type helper.
     * @param descriptor The type descriptor.
     * @return Get a set of imports to be done on behalf of the generator itself.
     */
    public Set<String> getClassImports(JavaTypeHelper helper, DescriptorType descriptor) {
        return setOf();
    }

    /**
     * Append class start to the type writer.
     *
     * @param writer The type writer to use.
     * @param descriptor The type descriptor to generate from.
     */
    public abstract void appendClassStart(JavaTypeWriter writer, DescriptorType descriptor);

    /**
     * Append class end to the type writer.
     *
     * @param writer The type writer to use.
     */
    public void appendClassEnd(JavaTypeWriter writer) {
        writer.end()
              .appendln("}");
    }

    /**
     * @param consumer Run for each registered formatter.
     */
    protected final void forEachFormatter(Consumer<Formatter> consumer) {
        mainFormatters.forEach(requireNonNull(consumer, "consumer == null"));
    }

    @Override
    public void generate(PNamespaceFileTypeRegistry registry, DescriptorType descriptor) throws IOException {
        String         javaClassName = JavaTypeHelper.javaClassName(descriptor);
        JavaTypeHelper helper        = new JavaTypeHelper(global, registry, options, javaClassName);
        Path file = fileManager.getFileForClass(
                helper.getJavaPackage() + '.' + javaClassName);

        try (OutputStream is = Files.newOutputStream(file,
                                                     StandardOpenOption.CREATE,
                                                     StandardOpenOption.TRUNCATE_EXISTING);
             IndentedPrintWriter iw = new IndentedPrintWriter(new BufferedWriter(new OutputStreamWriter(is, UTF_8)));
             JavaTypeWriter writer = new JavaTypeWriter(iw, helper)) {
            Set<String> imports = new TreeSet<>(getClassImports(helper, descriptor));
            forEachFormatter(f -> imports.addAll(f.getImportClasses(helper, descriptor)));
            for (InnerClassGenerator<DescriptorType, Formatter> generator : postFixToGenerator.values()) {
                if (generator.isEnabled(descriptor)) {
                    imports.addAll(generator.getClassImports(helper, descriptor));
                    generator.forEachFormatter(f -> imports.addAll(f.getImportClasses(helper, descriptor)));
                }
            }
            if (!imports.isEmpty()) {
                Set<String> globalImports = imports.stream()
                                                   .filter(s -> !s.startsWith("java"))
                                                   .collect(UnmodifiableSortedSet.toSortedSet());
                Set<String> javaImports = imports.stream()
                                                 .filter(s -> s.startsWith("java"))
                                                 .collect(UnmodifiableSortedSet.toSortedSet());
                if (!globalImports.isEmpty()) {
                    writer.newline();
                    globalImports.forEach(i -> writer.formatln("import %s;", i));
                }
                if (!javaImports.isEmpty()) {
                    writer.newline();
                    javaImports.forEach(i -> writer.formatln("import %s;", i));
                }
                helper.addReservedNames(imports.stream().map(s -> s.replaceAll(".*\\.", "")).collect(Collectors.toSet()));
            }
            writer.newline();

            try (BlockCommentWriter docs = new BlockCommentWriter(writer)) {
                if (descriptor.hasProperty(DOCUMENTATION)) {
                    docs.comment(descriptor.getProperty(DOCUMENTATION));
                }
                if (descriptor.hasProperty(PDefaultProperties.DEPRECATED)) {
                    docs.newline();
                    docs.deprecated_(descriptor.getProperty(PDefaultProperties.DEPRECATED));
                }
            }

            forEachFormatter(f -> f.appendClassAnnotations(writer, descriptor));

            appendClassStart(writer, descriptor);

            forEachFormatter(f -> f.appendExtraProperties(writer, descriptor));

            for (InnerClassGenerator<DescriptorType, Formatter> generator : postFixToGenerator.values()) {
                if (generator.getInnerClassName().equals(InnerClassGenerator.MAIN)) {
                    continue;
                }
                if (generator.isEnabled(descriptor)) {
                    writer.newline();
                    generator.appendClass(writer, descriptor);
                }
            }

            appendClassEnd(writer);
        }
    }
}
