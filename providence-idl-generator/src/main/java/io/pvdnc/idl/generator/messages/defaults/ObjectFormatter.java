/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.messages.defaults;

import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.utils.ValueUtil;
import io.pvdnc.idl.generator.InnerClassGenerator;
import io.pvdnc.idl.generator.MessageTypeFormatter;
import io.pvdnc.idl.generator.util.JavaAnnotation;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;

import java.util.Objects;
import java.util.Set;

import static net.morimekta.collect.UnmodifiableSet.setOf;
import static net.morimekta.strings.NamingUtil.Format.PASCAL;
import static net.morimekta.strings.NamingUtil.format;

public class ObjectFormatter implements MessageTypeFormatter {
    private final String innerClass;

    public ObjectFormatter(String innerClass) {this.innerClass = innerClass;}

    @Override
    public Set<String> getImportClasses(JavaTypeHelper helper, PMessageDescriptor<?> descriptor) {
        return setOf(Objects.class.getName(),
                     ValueUtil.class.getName());
    }

    @Override
    public void appendFields(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        if (innerClass.equals(InnerClassGenerator.IMPL)) {
            writer.newline()
                  .appendln("private transient Integer tHashCode;");
        }
    }

    @Override
    public void appendMethods(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        JavaTypeHelper helper = writer.getHelper();

        writer.newline()
              .appendln("// ---- Object ----");

        // ---- toString ----

        writer.newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .formatln("public String toString() {\n" +
                        "    return \"%s\" + %s.asString(this);\n" +
                        "}",
                        descriptor.getTypeName(),
                        ValueUtil.class.getSimpleName());

        // ---- hashCode ----
        writer.newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .appendln("public int hashCode() {")
              .begin();

        if (innerClass.equals(InnerClassGenerator.IMPL)) {
            writer.appendln("if (tHashCode == null) {")
                  .begin()
                  .appendln("tHashCode = ");
        } else {
            writer.appendln("return ");
        }

        writer.append("Objects.hash(")
              .begin("        ");

        boolean firstField = true;
        for (PField field : descriptor.allFields()) {
            if (firstField) firstField = false;
            else writer.append(',');
            writer.formatln("v%s", format(field.getName(), PASCAL));
        }

        writer.append(");")
              .end();

        if (innerClass.equals(InnerClassGenerator.IMPL)) {
            writer.end()
                  .appendln("}")
                  .appendln("return tHashCode;");
        }
        writer.end()
              .appendln("}");

        // ---- equals ----

        writer.newline()
              .appendln(JavaAnnotation.OVERRIDE)
              .formatln("public boolean equals(Object o) {\n" +
                        "    if (o == this) {\n" +
                        "        return true;\n" +
                        "    }\n" +
                        "    if (o == null || !getClass().equals(o.getClass())) {\n" +
                        "        return false;\n" +
                        "    }");
        if (descriptor.allFields().isEmpty()) {
            writer.formatln("    return true;");
        } else {
            writer.formatln("    %s other = (%s) o;\n" +
                            "    return ",
                            helper.javaTypeReference(descriptor, innerClass),
                            helper.javaTypeReference(descriptor, innerClass))
                  .begin("           ");

            firstField = true;
            for (PField field : descriptor.allFields()) {
                if (firstField) firstField = false;
                else writer.append(" &&").appendln();
                writer.format("Objects.equals(v%s, other.v%s)",
                              format(field.getName(), PASCAL), format(field.getName(), PASCAL));
            }
            writer.append(";").end();
        }
        writer.appendln("}");
    }
}
