/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.namespaces;

import io.pvdnc.core.registry.PGlobalFileTypeRegistry;
import io.pvdnc.core.registry.PNamespaceFileTypeRegistry;
import io.pvdnc.idl.generator.GeneratorOptions;
import io.pvdnc.idl.generator.util.ClassFileManager;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;
import net.morimekta.strings.io.IndentedPrintWriter;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import static java.nio.charset.StandardCharsets.UTF_8;
import static net.morimekta.strings.NamingUtil.Format.PASCAL;
import static net.morimekta.strings.NamingUtil.format;

/**
 * Generate something related to a whole namespace (usually same as a file).
 */
public abstract class NamespaceGenerator {
    public static final String CONSTANTS = "_Constants";
    public static final String FILE_DESCRIPTOR = "_FileDescriptor";

    protected final ClassFileManager fileManager;
    protected final PGlobalFileTypeRegistry global;
    protected final GeneratorOptions options;

    protected NamespaceGenerator(ClassFileManager fileManager,
                                 PGlobalFileTypeRegistry global,
                                 GeneratorOptions options) {
        this.fileManager = fileManager;
        this.global = global;
        this.options = options;
    }

    public abstract String getPostFix();

    public abstract void appendClass(JavaTypeWriter writer,
                                     PNamespaceFileTypeRegistry registry);

    protected abstract boolean shouldGenerate(PNamespaceFileTypeRegistry registry);

    /**
     * Generate the class for the namespace registry, if it should.
     * @param registry The namespace registry to generate class for.
     * @throws IOException If unable to write class.
     */
    public final void generate(PNamespaceFileTypeRegistry registry) throws IOException {
        if (!shouldGenerate(registry)) {
            return;
        }
        String javaClassName = format(registry.getNamespace(), PASCAL) + getPostFix();

        JavaTypeHelper helper = new JavaTypeHelper(global, registry, options, javaClassName);
        Path file = fileManager.getFileForClass(
                helper.getJavaPackage() + '.' + javaClassName);
        try (OutputStream is = Files.newOutputStream(file,
                                                     StandardOpenOption.CREATE,
                                                     StandardOpenOption.TRUNCATE_EXISTING);
             IndentedPrintWriter iw = new IndentedPrintWriter(new BufferedWriter(new OutputStreamWriter(is, UTF_8)));
             JavaTypeWriter writer = new JavaTypeWriter(iw, helper)) {
            appendClass(writer, registry);
        }
    }
}
