/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.enums;

import io.pvdnc.core.PEnum;
import io.pvdnc.core.property.PDefaultProperties;
import io.pvdnc.core.registry.PGlobalFileTypeRegistry;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PEnumValue;
import io.pvdnc.idl.generator.EnumTypeFormatter;
import io.pvdnc.idl.generator.GeneratorOptions;
import io.pvdnc.idl.generator.TypeGenerator;
import io.pvdnc.idl.generator.enums.defaults.PEnumDescriptorFormatter;
import io.pvdnc.idl.generator.enums.defaults.PEnumFormatter;
import io.pvdnc.idl.generator.enums.gen.EnumDescriptorGenerator;
import io.pvdnc.idl.generator.util.BlockCommentWriter;
import io.pvdnc.idl.generator.util.ClassFileManager;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;

import java.util.Set;
import java.util.TreeSet;

import static net.morimekta.collect.UnmodifiableSet.setOf;
import static io.pvdnc.core.property.PDefaultProperties.DOCUMENTATION;
import static io.pvdnc.idl.generator.InnerClassGenerator.DESCRIPTOR;
import static io.pvdnc.idl.generator.InnerClassGenerator.MAIN;
import static io.pvdnc.idl.generator.util.JavaTypeHelper.javaClassName;
import static net.morimekta.strings.NamingUtil.Format.PASCAL;
import static net.morimekta.strings.NamingUtil.Format.SNAKE_UPPER;
import static net.morimekta.strings.NamingUtil.format;

public class EnumGenerator extends TypeGenerator<PEnumDescriptor, EnumTypeFormatter> {
    public EnumGenerator(ClassFileManager manager, PGlobalFileTypeRegistry global, GeneratorOptions options) {
        super(manager, global, options);
        addClass(new EnumDescriptorGenerator());
        addClassFormatter(MAIN, new PEnumFormatter());
        addClassFormatter(DESCRIPTOR, new PEnumDescriptorFormatter());
    }

    @Override
    public Set<String> getClassImports(JavaTypeHelper helper, PEnumDescriptor descriptor) {
        return setOf(PEnum.class.getName(),
                     PEnumValue.class.getName());
    }

    @Override
    public void appendClassStart(JavaTypeWriter writer, PEnumDescriptor descriptor) {
        JavaTypeHelper helper = writer.getHelper();

        writer.formatln("public enum %s implements %s", javaClassName(descriptor), PEnum.class.getSimpleName())
              .begin().begin();
        Set<String> extraImplements = new TreeSet<>();
        forEachFormatter(f -> extraImplements.addAll(f.getExtraImplements(helper, descriptor)));
        for (String extra : extraImplements) {
            writer.format(",").appendln(extra);
        }
        writer.append(" {").end();

        for (PEnum value : descriptor.allValues()) {
            PEnumValue meta = value.$meta();
            try (BlockCommentWriter docs = new BlockCommentWriter(writer)) {
                if (meta.hasProperty(DOCUMENTATION)) {
                    docs.comment(meta.getProperty(DOCUMENTATION));
                }
                if (meta.hasProperty(PDefaultProperties.DEPRECATED)) {
                    docs.newline()
                        .deprecated_(meta.getProperty(PDefaultProperties.DEPRECATED));
                }
            }
            writer.formatln("// %s = %s", meta.getName(), meta.getValue());
            writer.formatln("%s(%s.kValue%s),",
                            format(meta.getName(), SNAKE_UPPER),
                            DESCRIPTOR,
                            format(meta.getName(), PASCAL));
        }
        writer.formatln(";");

        // ------------------

        writer.newline()
              .formatln("private final %s meta;", PEnumValue.class.getSimpleName())
              .newline()
              .formatln("%s(%s meta) {\n" +
                        "    this.meta = meta;\n" +
                        "}", javaClassName(descriptor), PEnumValue.class.getSimpleName());

        forEachFormatter(f -> f.appendMethods(writer, descriptor));

        // ------------------

        forEachFormatter(f -> f.appendConstants(writer, descriptor));
    }
}
