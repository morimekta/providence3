/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.messages.gen;

import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.idl.generator.InnerClassGenerator;
import io.pvdnc.idl.generator.MessageTypeFormatter;
import io.pvdnc.idl.generator.messages.defaults.BuilderImplFormatter;
import io.pvdnc.idl.generator.messages.defaults.ImplFormatter;
import io.pvdnc.idl.generator.messages.defaults.ObjectFormatter;
import io.pvdnc.idl.generator.messages.defaults.OneOfIFaceFormatter;
import io.pvdnc.idl.generator.messages.defaults.OneOfImplFormatter;
import io.pvdnc.idl.generator.messages.defaults.PMessageBuilderFormatter;
import io.pvdnc.idl.generator.messages.defaults.PMessageFormatter;
import io.pvdnc.idl.generator.messages.defaults.PUnionFormatter;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;

import java.util.Set;
import java.util.TreeSet;

import static io.pvdnc.core.property.PDefaultProperties.MESSAGE_VARIANT;
import static io.pvdnc.core.types.PMessageVariant.INTERFACE;
import static io.pvdnc.core.types.PMessageVariant.UNION;
import static net.morimekta.strings.NamingUtil.Format.PASCAL;
import static net.morimekta.strings.NamingUtil.format;

public class MessageBuilderImplGenerator extends InnerClassGenerator<PMessageDescriptor<?>, MessageTypeFormatter> {
    public MessageBuilderImplGenerator() {
        addFormatter(new ImplFormatter(BUILDER_IMPL));
        addFormatter(new BuilderImplFormatter());
        addFormatter(new PMessageFormatter(BUILDER_IMPL));
        addFormatter(new PMessageBuilderFormatter());
        addFormatter(new PUnionFormatter());
        addFormatter(new OneOfImplFormatter(BUILDER_IMPL));
        addFormatter(new ObjectFormatter(BUILDER_IMPL));
    }

    @Override
    public String getInnerClassName() {
        return BUILDER_IMPL;
    }

    @Override
    public boolean isEnabled(PMessageDescriptor<?> descriptor) {
        return descriptor.getProperty(MESSAGE_VARIANT) != INTERFACE;
    }

    @Override
    public void appendClass(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        JavaTypeHelper helper = writer.getHelper();

        forEachFormatter(f -> f.appendClassAnnotations(writer, descriptor));
        writer.formatln("class %s", getInnerClassName());
        writer.formatln("        implements %s.Builder", helper.javaTypeReference(descriptor))
              .begin().begin("               ");
        Set<String> extraImplements = new TreeSet<>();
        forEachFormatter(f -> extraImplements.addAll(f.getExtraImplements(helper, descriptor)));
        extraImplements.forEach(i -> writer.append(",").appendln(i));
        writer.append(" {").end();

        appendFields(writer, descriptor);

        forEachFormatter(f -> f.appendFields(writer, descriptor));

        appendConstructor(writer, descriptor);

        for (PField field : descriptor.allFields()) {
            forEachFormatter(f -> f.appendFieldMethods(writer, descriptor, field));
        }
        forEachFormatter(f -> f.appendMethods(writer, descriptor));
        forEachFormatter(f -> f.appendConstructors(writer, descriptor));
        forEachFormatter(f -> f.appendConstants(writer, descriptor));

        writer.end().appendln("}");
    }

    private void appendFields(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        JavaTypeHelper helper = writer.getHelper();
        for (PField field : descriptor.allFields()) {
            writer.formatln("private %s v%s;",
                            helper.getValueClass(field.getResolvedDescriptor()),
                            format(field.getName(), PASCAL));
        }
        if (descriptor.getProperty(MESSAGE_VARIANT) == UNION) {
            writer.formatln("private int tUnionField;");
        }
    }

    private void appendConstructor(JavaTypeWriter writer, PMessageDescriptor<?> descriptor) {
        boolean isUnion = descriptor.getProperty(MESSAGE_VARIANT) == UNION;

        boolean empty = true;
        JavaTypeHelper helper = writer.getHelper();
        writer.newline()
              .formatln("protected %s(", BUILDER_IMPL)
              .begin()
              .begin(" ".repeat(BUILDER_IMPL.length() + 7));
        boolean first = true;
        for (PField field : descriptor.allFields()) {
            if (first) {
                first = false;
            } else {
                writer.append(",");
                writer.appendln();
            }
            empty = false;
            writer.format("%s p%s",
                          helper.getValueClass(field.getResolvedDescriptor()),
                          format(field.getName(), PASCAL));
        }
        if (isUnion) {
            if (!first) {
                writer.append(",");
                writer.appendln();
            }
            empty = false;
            writer.append("int uUnionField");
        } else {
            for (String group : OneOfIFaceFormatter.oneOfGroups(descriptor)) {
                empty = false;
                writer.append(',')
                      .formatln("int %s", OneOfIFaceFormatter.oneOfParam(group));
            }
        }
        writer.append(") {").end();
        for (PField field : descriptor.allFields()) {
            writer.formatln("v%s = p%s;",
                            format(field.getName(), PASCAL),
                            format(field.getName(), PASCAL));
        }
        if (isUnion) {
            writer.appendln("tUnionField = uUnionField;");
        } else {
            for (String group : OneOfIFaceFormatter.oneOfGroups(descriptor)) {
                writer.formatln("%s = %s;", OneOfIFaceFormatter.oneOfField(group), OneOfIFaceFormatter.oneOfParam(group));
            }
        }
        writer.end()
              .appendln("}");

        // --- And an empty constructor.

        if (!empty) {
            writer.newline()
                  .formatln("protected %s() {", BUILDER_IMPL)
                  .begin();
            if (isUnion) {
                writer.appendln("tUnionField = Integer.MIN_VALUE;");
            } else {
                for (String group : OneOfIFaceFormatter.oneOfGroups(descriptor)) {
                    writer.formatln("%s = Integer.MIN_VALUE;", OneOfIFaceFormatter.oneOfField(group));
                }
            }
            writer.end()
                  .appendln('}');
        }
    }
}
