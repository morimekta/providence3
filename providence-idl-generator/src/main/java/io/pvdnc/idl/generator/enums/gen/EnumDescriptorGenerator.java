/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.generator.enums.gen;

import io.pvdnc.idl.generator.EnumTypeFormatter;
import io.pvdnc.idl.generator.InnerClassGenerator;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;
import net.morimekta.collect.UnmodifiableList;
import io.pvdnc.core.PEnum;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PEnumValue;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Supplier;

import static net.morimekta.collect.UnmodifiableSet.setOf;
import static net.morimekta.strings.EscapeUtil.javaEscape;
import static net.morimekta.strings.NamingUtil.Format.PASCAL;
import static net.morimekta.strings.NamingUtil.format;

public class EnumDescriptorGenerator extends InnerClassGenerator<PEnumDescriptor, EnumTypeFormatter> {
    public EnumDescriptorGenerator() {
    }

    @Override
    public String getInnerClassName() {
        return DESCRIPTOR;
    }

    @Override
    public Set<String> getClassImports(JavaTypeHelper helper, PEnumDescriptor descriptor) {
        return setOf(PEnumValue.class.getName(),
                     PEnumDescriptor.class.getName(),
                     PPropertyMap.class.getName(),
                     UnmodifiableList.class.getName(),
                     Optional.class.getName(),
                     Supplier.class.getName(),
                     List.class.getName());
    }

    @Override
    public void appendClass(JavaTypeWriter writer, PEnumDescriptor descriptor) {
        JavaTypeHelper helper = writer.getHelper();

        forEachFormatter(f -> f.appendClassAnnotations(writer, descriptor));
        writer.formatln("public static class %s extends %s",
                        getInnerClassName(),
                        PEnumDescriptor.class.getSimpleName())
              .begin().begin();
        Set<String> extraImplements = new TreeSet<>();
        forEachFormatter(f -> extraImplements.addAll(f.getExtraImplements(helper, descriptor)));
        if (!extraImplements.isEmpty()) {
            writer.appendln("implements ");
            boolean first = true;
            for (String extra : extraImplements) {
                if (first) {
                    first = false;
                } else {
                    writer.append(",");
                }
                writer.appendln(extra);
            }
        }
        writer.append(" {").end();

        for (PEnum value : descriptor.allValues()) {
            PEnumValue meta = value.$meta();
            writer.formatln("public static final %s kValue%s =\n" +
                            "        new %s(\"%s\", %d, %s.of(",
                            PEnumValue.class.getSimpleName(),
                            format(meta.getName(), PASCAL),
                            PEnumValue.class.getSimpleName(),
                            javaEscape(meta.getName()),
                            meta.getValue(),
                            PPropertyMap.class.getSimpleName())
                  .begin("                ");
            boolean first = true;
            for (Map.Entry<String, String> prop : meta.getUncheckedProperties().entrySet()) {
                if (first) {
                    first = false;
                } else {
                    writer.append(",");
                }
                writer.formatln("\"%s\", \"%s\"",
                                javaEscape(prop.getKey()), javaEscape(prop.getValue()));
            }
            writer.formatln("), () -> %s.kDescriptor);", helper.getJavaClassName())
                  .end();
        }

        writer.newline()
              .formatln("private %s() {\n" +
                        "    super(\"%s\", \"%s\",\n" +
                        "          %s.of(",
                        DESCRIPTOR,
                        javaEscape(descriptor.getNamespace()),
                        javaEscape(descriptor.getSimpleName()),
                        PPropertyMap.class.getSimpleName())
              .begin().begin("              ");
        boolean first = true;
        for (Map.Entry<String, String> prop : descriptor.getUncheckedProperties().entrySet()) {
            if (first) {
                first = false;
            } else {
                writer.append(",");
            }
            writer.formatln("\"%s\", \"%s\"", javaEscape(prop.getKey()), javaEscape(prop.getValue()));
        }
        writer.append("));")
              .end().end()
              .appendln("}");

        forEachFormatter(f -> f.appendMethods(writer, descriptor));

        writer.newline()
              .appendln("// ---- Static ----");

        forEachFormatter(f -> f.appendExtraProperties(writer, descriptor));
        forEachFormatter(f -> f.appendConstants(writer, descriptor));

        writer.end()
              .formatln("}");
    }
}
