import io.pvdnc.idl.generator.GeneratorProvider;

module io.pvdnc.idl.generator {
    exports io.pvdnc.idl.generator;
    exports io.pvdnc.idl.generator.util;

    exports io.pvdnc.idl.generator.enums;
    exports io.pvdnc.idl.generator.messages;
    exports io.pvdnc.idl.generator.services;
    exports io.pvdnc.idl.generator.namespaces;

    requires io.pvdnc.core;
    requires net.morimekta.collect;
    requires net.morimekta.strings;
    requires net.morimekta.file;

    uses GeneratorProvider;
}