import io.pvdnc.idl.parser.IDLParserProvider;

module io.pvdnc.idl.parser {
    exports io.pvdnc.idl.parser;

    requires transitive io.pvdnc.core;

    requires net.morimekta.collect;
    requires net.morimekta.strings;
    requires net.morimekta.file;
    requires net.morimekta.lexer;

    uses IDLParserProvider;
}