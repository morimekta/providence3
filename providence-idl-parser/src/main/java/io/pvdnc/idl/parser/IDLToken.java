/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.parser;

import net.morimekta.lexer.Token;

import java.util.regex.Pattern;

/**
 * General token used for most IDL parsing.
 */
public class IDLToken extends Token<IDLTokenType> {
    // Various commonly used symbols.
    public static final char kGenericStart  = '<';
    public static final char kGenericEnd    = '>';
    public static final char kMessageStart  = '{';
    public static final char kMessageEnd    = '}';
    public static final char kKeyValueSep   = ':';
    public static final char kFieldValueSep = '=';
    public static final char kParamsStart   = '(';
    public static final char kParamsEnd     = ')';
    public static final char kListStart     = '[';
    public static final char kListEnd       = ']';
    public static final char kEntrySep      = ',';
    public static final char kLineSep       = ';';

    /**
     * Create a slice instance. The slice is only meant to be internal state
     * immutable, and not representing an immutable byte content.
     *
     * @param fb      The buffer to wrap.
     * @param off     The start offset to wrap.
     * @param len     The length to represent.
     * @param type    The token type represented.
     * @param lineNo  The current line number.
     * @param linePos The current line position.
     */
    public IDLToken(char[] fb, int off, int len,
                    IDLTokenType type, int lineNo, int linePos) {
        super(fb, off, len, type, lineNo, linePos);
    }

    /**
     * Parse token content as documentation.
     *
     * @return The documentation string.
     */
    public String parseDocumentation() {
        String block = toString().strip();
        if (block.startsWith("//")) {
            return block.replaceFirst("// *", "");
        }
        String[] lines = block
                .replaceAll("^/\\*\\**", "")
                .replaceAll("\\**\\*/$", "")
                .strip()
                .split("\\r?\\n", Short.MAX_VALUE);
        StringBuilder builder = new StringBuilder();

        for (String line : lines) {
            builder.append(RE_BLOCK_LINE.matcher(line).replaceFirst(""));
            builder.append('\n');
        }
        return builder.toString().trim();
    }

    /**
     * Parse token as an integer. This handles normal (decimal) numbers,
     * octal (if having leading '0') and hexadecimal (with leading '0x').
     *
     * @return The parsed integer.
     */
    public int parseInteger() {
        if (isInteger()) {
            if (charAt(0) == '0') {
                if (len > 2 && charAt(1) == 'x') {
                    return Integer.parseInt(this.subSlice(2).toString(), 16);
                }
                return Integer.parseInt(this.toString(), 8);
            } else {
                return Integer.parseInt(this.toString());
            }
        }
        return -1;
    }

    /**
     * @return If the token is a positive ID integer. This matches only decimal
     *         numbers of 1 and higher.
     */
    public boolean isPositiveID() {
        return RE_POSITIVE_ID.matcher(this).matches();
    }

    /**
     * @return If the token is a non-negative decimal integer. This matches
     *         numbers of 0 and higher.
     */
    public boolean isNonNegativeID() {
        return RE_NON_NEGATIVE_ID.matcher(this).matches();
    }

    /**
     * @return If the token is an integer. This matches all decimal, octal and
     *         hexadecimal integers.
     */
    public boolean isInteger() {
        return RE_INTEGER.matcher(this).matches();
    }

    /**
     * @return If the token is a single identifier.
     */
    public boolean isIdentifier() {
        return RE_IDENTIFIER.matcher(this).matches();
    }

    /**
     * @return If the token is a qualified identifier. This matches if the token
     *         consist of two identifiers concatenated with '.'.
     */
    public boolean isQualifiedIdentifier() {
        return RE_QUALIFIED_IDENTIFIER.matcher(this).matches();
    }

    /**
     * @return If the token is a double-qualified identifier. This matches if the token
     *         consist of three identifiers concatenated with '.'.
     */
    public boolean isDoubleQualifiedIdentifier() {
        return RE_DOUBLE_QUALIFIED_IDENTIFIER.matcher(this).matches();
    }

    /**
     * @return If the token is a reference identifier. This matches if the token
     *         consist of any number of identifiers concatenated with '.'. At least one.
     */
    public boolean isReferenceIdentifier() {
        return RE_REFERENCE_IDENTIFIER.matcher(this).matches();
    }

    // ---- INTERNAL ----

    private static final Pattern RE_IDENTIFIER                  = Pattern.compile(
            "^[_a-zA-Z][_a-zA-Z0-9]*$");
    private static final Pattern RE_QUALIFIED_IDENTIFIER        = Pattern.compile(
            "^[_a-zA-Z][_a-zA-Z0-9]*[.][_a-zA-Z][_a-zA-Z0-9]*$");
    private static final Pattern RE_DOUBLE_QUALIFIED_IDENTIFIER = Pattern.compile(
            "^[_a-zA-Z][_a-zA-Z0-9]*[.][_a-zA-Z][_a-zA-Z0-9]*[.][_a-zA-Z][_a-zA-Z0-9]*$");
    private static final Pattern RE_REFERENCE_IDENTIFIER        = Pattern.compile(
            "^[_a-zA-Z][_a-zA-Z0-9]*([.][_a-zA-Z][_a-zA-Z0-9]*)*$");
    private final static Pattern RE_BLOCK_LINE                  = Pattern.compile(
            "^([\\s]*[*])?[\\s]?");
    private static final Pattern RE_POSITIVE_ID                 = Pattern.compile(
            "([1-9][0-9]*)");
    private static final Pattern RE_NON_NEGATIVE_ID             = Pattern.compile(
            "(0|[1-9][0-9]*)");
    private static final Pattern RE_INTEGER                     = Pattern.compile(
            "-?(0|[1-9][0-9]*|0[0-7]+|0x[0-9a-fA-F]+)");
}
