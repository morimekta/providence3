/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.parser;

import io.pvdnc.core.registry.PGlobalFileTypeRegistry;
import io.pvdnc.core.registry.PNamespaceFileTypeRegistry;

import java.io.IOException;
import java.nio.file.Path;

public interface IDLParser {
    /**
     * Parse a file, and return the type registry for the generated namespace.
     *
     * @param filePath       File path to the file to be parsed. This should be
     *                       an absolute path.
     * @param registry       The file type registry to get includes from, and to put
     *                       chain-loaded includes into.
     * @param warningHandler Handler for non-breaking IDL parse warnings.
     * @return The type registry for the parsed file and namespace. This should
     *         already have been validated by the handler when returned.
     * @throws IOException If unable to read or parse file.
     */
    PNamespaceFileTypeRegistry parseFile(Path filePath,
                                         PGlobalFileTypeRegistry registry,
                                         IDLWarningHandler warningHandler) throws IOException;
}
