/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.parser;

import io.pvdnc.core.property.PProperty;

import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.ServiceLoader;

import static net.morimekta.collect.UnmodifiableList.asList;

/**
 * Interface for providing IDL parsers. This is handled as a java
 * service and must be available to a {@link ServiceLoader}.
 */
public interface IDLParserProvider {
    /**
     * @param filePath File path to check.
     * @return If the handler will handle this file.
     */
    boolean willHandleFile(Path filePath);

    /**
     * Get list of allowed options. If the option ends in "=", it also
     * requires a string argument when passed, which will be parsed by the
     * parser for it's option.
     *
     * @return List of options.
     */
    List<PProperty<?>> getOptions();

    /**
     * Get the parser name. This should be the match the file suffix for
     * easy understandability.
     *
     * @return The parser name.
     */
    String getName();

    /**
     * Get an actual parser.
     *
     * @param options Parser options.
     * @return The parser to be used with provided arguments.
     * @throws IllegalArgumentException If passed invalid option.
     */
    IDLParser getParser(IDLParserOptions options);

    /**
     * @return A list of available parser-providers.
     */
    static List<IDLParserProvider> getParserProviders() {
        ServiceLoader<IDLParserProvider> loader = ServiceLoader.load(IDLParserProvider.class);
        loader.reload();
        return asList(loader);
    }

    /**
     * Get a file type handler for a given file. This will simply ask all known handlers if
     * they want to handle the given file, and returns the first that approves.
     *
     * @param filePath The file path.
     * @return The file type handler.
     */
    static IDLParserProvider getParserProviderFor(Path filePath) {
        return getParserProviderFor(filePath, getParserProviders());
    }

    /**
     * Get a file type handler for a given file. This will simply ask all known handlers if
     * they want to handle the given file, and returns the first that approves.
     *
     * @param filePath The file path.
     * @param providers List of available providers.
     * @return The file type handler.
     */
    static IDLParserProvider getParserProviderFor(Path filePath, List<IDLParserProvider> providers) {
        Objects.requireNonNull(filePath, "filePath == null");
        if (filePath.getFileName() == null) {
            throw new IllegalArgumentException("No fileName in filePath " + filePath);
        }

        for (IDLParserProvider handler : providers) {
            if (handler.willHandleFile(filePath)) {
                return handler;
            }
        }
        throw new IllegalArgumentException("No handler for " + filePath.getFileName());
    }
}
