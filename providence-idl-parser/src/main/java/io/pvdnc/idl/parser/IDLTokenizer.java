/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.parser;

import net.morimekta.lexer.TokenizerBase;

import java.io.IOException;
import java.io.Reader;

import static net.morimekta.strings.EscapeUtil.javaEscape;

/**
 * Specialization of the 'pretty' tokenizer to make it handle some
 * special cases only applicable when parsing thrift files, but not
 * allowed in pretty format or config files.
 */
public class IDLTokenizer extends TokenizerBase<IDLTokenType, IDLToken> {
    /**
     * Create a pre-loaded tokenizer. This is the one used by most parsers.
     *
     * @param reader The reader to get content from. The whole reader will be
     *               consumed.
     */
    public IDLTokenizer(Reader reader) {
        this(reader, 4096, true);
    }

    /**
     * Create a tokenizer instance. Parameters passed to superclass.
     * @param reader The reader to get content from.
     * @param bufferSize Buffer-size when reading from reader.
     * @param preloadAll If all content of the reader should be pre-loaded.
     */
    public IDLTokenizer(Reader reader, int bufferSize, boolean preloadAll) {
        super(reader, bufferSize, preloadAll);
    }

    /**
     * @return Return true if the tokenizer should parse java-style comments. Otherwise
     *         only comments using '#' will be parsed. All single-line comments will be
     *         ignored, but nulti-line '/*... *{@literal/}' will be passed as
     *         {@link IDLTokenType#DOCUMENTATION}.
     */
    protected boolean parseJavaStyleComments() {
        return false;
    }

    // --- TokenizerBase

    @Override
    public IDLException failure(IDLToken token, String message, Object... params) {
        return new IDLException(token, message, params);
    }

    @Override
    protected IDLException eofFailure(String message, Object... params) {
        return new IDLException(message, params);
    }

    @Override
    public IDLToken parseNextToken() throws IOException {
        IDLToken token = super.parseNextToken();
        while (token != null && token.type() == IDLTokenType.COMMENT) {
            token = super.parseNextToken();
        }
        return token;
    }

    @Override
    protected IDLToken identifierToken(char[] buffer, int offset, int len, int lineNo, int linePos) {
        return new IDLToken(buffer, offset, len, IDLTokenType.IDENTIFIER, lineNo, linePos);
    }

    @Override
    protected IDLToken stringToken(char[] buffer, int offset, int len, int lineNo, int linePos) {
        return new IDLToken(buffer, offset, len, IDLTokenType.STRING, lineNo, linePos);
    }

    @Override
    protected IDLToken numberToken(char[] buffer, int offset, int len, int lineNo, int linePos) {
        return new IDLToken(buffer, offset, len, IDLTokenType.NUMBER, lineNo, linePos);
    }

    @Override
    protected IDLToken symbolToken(char[] buffer, int offset, int len, int lineNo, int linePos) {
        return new IDLToken(buffer, offset, len, IDLTokenType.SYMBOL, lineNo, linePos);
    }

    @Override
    protected IDLToken genericToken(char[] buffer,
                                    int offset,
                                    int len,
                                    IDLTokenType type,
                                    int lineNo,
                                    int linePos) {
        return new IDLToken(buffer, offset, len, type, lineNo, linePos);
    }

    @Override
    protected IDLToken nextSymbol() throws IOException {
        if (parseJavaStyleComments() && lastChar == '/') {
            int startOffset = bufferOffset;
            int startLinePos = linePos;

            if (!readNextChar()) {
                throw eofFailure("Expected java-style comment, got end of file");
            }
            if (lastChar == '/') {
                lastChar = 0;
                IDLToken token = readUntil("\n", IDLTokenType.COMMENT, true);
                if (token == null) {
                    // empty comment;
                    return genericToken("".toCharArray(), 0, 0, IDLTokenType.COMMENT, lineNo, startLinePos + 2);
                }
                return token;
            }
            if (lastChar == '*') {
                lastChar = 0;
                IDLToken token = readUntil("*/", IDLTokenType.DOCUMENTATION, false);
                if (token == null) {
                    // empty comment;
                    return genericToken("".toCharArray(), 0, 0, IDLTokenType.COMMENT, lineNo, startLinePos + 2);
                }
                return token;
            }

            IDLToken token = symbolToken(buffer, startOffset, 2, lineNo, startLinePos);
            throw failure(token, "Expected java-style comment, got '%s' after '/'",
                          javaEscape((char) lastChar));
        }
        return super.nextSymbol();
    }
}
