/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.parser;

import net.morimekta.lexer.LexerException;

import java.util.Locale;

/**
 * Token specialization for the thrift parser and tokenizer.
 */
public class IDLException extends LexerException {
    private String file;

    /**
     * Create IDL exception instance.
     *
     * @param message The message to display.
     * @param params Params to format message.
     */
    public IDLException(String message, Object... params) {
        super(params.length == 0 ? message : String.format(Locale.US, message, params));
    }

    /**
     * Create IDL exception instance.
     *
     * @param cause Exception causing the error.
     * @param message The message to display.
     * @param params Params to format message.
     */
    public IDLException(Throwable cause, String message, Object... params) {
        super(cause, params.length == 0 ? message : String.format(Locale.US, message, params));
    }

    /**
     * Create IDL exception instance.
     *
     * @param token With problem.
     * @param message The message to display.
     * @param params Params to format message.
     */
    public IDLException(IDLToken token, String message, Object... params) {
        super(token, params.length == 0 ? message : String.format(Locale.US, message, params));
    }

    /**
     * Create IDL exception instance.
     *
     * @param line The line string the exception is on.
     * @param lineNo The line number.
     * @param linePos The line position.
     * @param length The length of the error.
     * @param message The message to display.
     * @param params Params to format message.
     */
    public IDLException(CharSequence line, int lineNo, int linePos, int length, String message, Object... params) {
        super(line, lineNo, linePos, length, params.length == 0 ? message : String.format(Locale.US, message, params));
    }

    /**
     * Get the name of the file where the exception happened.
     * @return The file name.
     */
    public String getFile() {
        return file;
    }

    /**
     * Set the file the exception is related to.
     *
     * @param file The file name.
     * @return The IDL exception.
     */
    public IDLException setFile(String file) {
        this.file = file;
        return this;
    }

    // --- LexerException

    @Override
    protected String getError() {
        if (getFile() != null) {
            return "Error in " + getFile();
        }
        return "Error";
    }

    // --- Throwable

    @Override
    public IDLException initCause(Throwable cause) {
        super.initCause(cause);
        return this;
    }

    // --- Object

    @Override
    public String toString() {
        return getError() + ": " + getMessage();
    }
}
