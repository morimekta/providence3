/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.parser;

/**
 * Handler for IDL parse warnings. It will be handled exceptions
 * pointing to issues that are non-breaking, or using deprecated
 * or unadvised features etc.
 */
@FunctionalInterface
public interface IDLWarningHandler {
    /**
     * Handle a warning exception.
     * @param e The warning exception.
     * @throws IDLException If the exception should stop parsing and be
     *                      handled as error instead.
     */
    void handleWarning(IDLException e) throws IDLException;
}
