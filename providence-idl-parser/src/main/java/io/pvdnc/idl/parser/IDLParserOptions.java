/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.parser;

import io.pvdnc.core.property.PPropertyMap;

import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.requireNonNull;
import static net.morimekta.strings.EscapeUtil.javaEscape;

/**
 * Handle parser options.
 */
public class IDLParserOptions {
    private final Map<String, PPropertyMap> parserOptions;

    public IDLParserOptions() {
        parserOptions = new HashMap<>();
    }

    /**
     * Parse an options string and update the parser options map.
     * The options string must be of the format:
     * <pre>{@code
     * '{name}:({option}(={value})?)(,{option}(={value})?)*'
     * }</pre>
     *
     * @param options The options string to parse.
     * @return The options instance.
     */
    public IDLParserOptions parseOptions(String options) {
        requireNonNull(options, "options == null");
        String[] parts = options.split(":", 2);
        if (parts.length == 1) {
            throw new IllegalArgumentException(
                    "Options string must be of format '{name}:{options}*', was: '" + javaEscape(options) + "'");
        }
        return parseOptions(parts[0], parts[1]);
    }

    /**
     * Parse and apply options for a specific parser.
     *
     * The options string must be of the format:
     * <pre>{@code
     * '({option}(={value})?)(,{option}(={value})?)*'
     * }</pre>
     *
     * @param parser The parser to apply options on.
     * @param options The options string to parse.
     * @return The options instance.
     */
    public IDLParserOptions parseOptions(String parser, String options) {
        PPropertyMap map = parserOptions.computeIfAbsent(parser, name -> PPropertyMap.empty());
        PPropertyMap.Builder builder = PPropertyMap.newBuilder();
        builder.putAll(map);
        for (String opt : options.split(",")) {
            String[] pair = opt.split("=", 2);
            if (pair.length == 1) {
                builder.putUnsafe(pair[0], "");
            } else {
                builder.putUnsafe(pair[0], pair[1]);
            }
        }
        parserOptions.put(parser, builder.build());
        return this;
    }

    /**
     * Get the parser options for a specific parser.
     *
     * @param name The parser name.
     * @return The parse options for the parser.
     */
    public PPropertyMap getParserOptions(String name) {
        return parserOptions.getOrDefault(name, PPropertyMap.empty());
    }
}
