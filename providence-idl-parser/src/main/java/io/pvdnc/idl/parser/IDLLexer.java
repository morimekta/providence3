/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.parser;

import net.morimekta.lexer.Lexer;
import net.morimekta.lexer.LexerException;
import net.morimekta.lexer.Tokenizer;

import java.io.IOException;

/**
 * Lexer for IDL parsing.
 */
public class IDLLexer extends Lexer<IDLTokenType, IDLToken> {
    private IDLToken lastToken;

    /**
     * Make a lexer for IDL parsing.
     *
     * @param tokenizer The tokenizer to use.
     */
    public IDLLexer(Tokenizer<IDLTokenType, IDLToken> tokenizer) {
        super(tokenizer);
    }

    /**
     * @return The last token returned from the lexer.
     */
    public IDLToken last() {
        return lastToken;
    }

    @Override
    public LexerException failure(IDLToken token, String message, Object... args) {
        return new IDLException(token, message, args);
    }

    @Override
    protected LexerException eofFailure(CharSequence line, int lineNo, int linePos, String message, Object... args) {
        return new IDLException(line, lineNo, linePos, 0, message, args);
    }

    @Override
    public IDLToken next() throws IOException {
        IDLToken result = super.next();
        if (result != null) {
            // Do not forget the last token at end of stream.
            lastToken = result;
        }
        return result;
    }
}
