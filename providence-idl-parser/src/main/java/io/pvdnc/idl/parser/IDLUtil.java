/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.parser;

import java.util.List;

/**
 * Utilities for helping IDL parsing and token handling.
 */
public final class IDLUtil {
    /**
     * Turn a list of tokens into a single token, mostly for showing error
     * location in {@link IDLException}. Will use all the tokens on the first
     * (same) line of the file in the list. Assuming the tokens come in correct
     * order, and points to the same file.
     *
     * @param typeTokens List of tokens to make into a single token.
     * @return The one token representing the list.
     */
    public static IDLToken toSingleToken(List<IDLToken> typeTokens) {
        IDLToken type1 = typeTokens.get(0);
        if (typeTokens.size() == 1) {
            return type1;
        }
        for (int i = typeTokens.size() - 1; i > 0; --i) {
            IDLToken last = typeTokens.get(i);
            if (last.lineNo() == type1.lineNo()) {
                CharSequence line = type1.line();
                return new IDLToken(line.toString().toCharArray(),
                                    type1.offset(),
                                    last.offset() + last.length() - type1.offset(),
                                    type1.type(),
                                    type1.lineNo(),
                                    type1.linePos());
            }
        }
        return type1;
    }

    private IDLUtil() {}
}
