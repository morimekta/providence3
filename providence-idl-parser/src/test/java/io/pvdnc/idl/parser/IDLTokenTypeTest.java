package io.pvdnc.idl.parser;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class IDLTokenTypeTest {
    @Test
    public void testToString() {
        assertThat(IDLTokenType.NUMBER.toString(), is("<number>"));
    }
}
