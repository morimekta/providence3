package io.pvdnc.idl.parser;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class IDLUtilTest {
    @Test
    public void testToSingleToken_multiLine() throws IOException {
        StringReader reader = new StringReader("skip foo with\n" +
                                               "in not");
        IDLTokenizer tokenizer = new IDLTokenizer(reader);
        tokenizer.parseNextToken();
        List<IDLToken> tokens = new ArrayList<>();
        tokens.add(tokenizer.parseNextToken());
        tokens.add(tokenizer.parseNextToken());
        tokens.add(tokenizer.parseNextToken());
        tokenizer.parseNextToken();
        assertThat(tokenizer.parseNextToken(), is(CoreMatchers.nullValue()));
        IDLException ex = new IDLException(IDLUtil.toSingleToken(tokens), "__MESSAGE__");
        assertThat(ex.displayString(),
                   is("Error on line 1 row 6-13: __MESSAGE__\n" +
                      "skip foo with\n" +
                      "-----^^^^^^^^"));

        tokens.remove(0);
        ex = new IDLException(IDLUtil.toSingleToken(tokens), "__MESSAGE__");
        assertThat(ex.displayString(),
                   is("Error on line 1 row 10-13: __MESSAGE__\n" +
                      "skip foo with\n" +
                      "---------^^^^"));
        tokens.remove(0);
        ex = new IDLException(IDLUtil.toSingleToken(tokens), "__MESSAGE__");
        assertThat(ex.displayString(),
                   is("Error on line 2 row 1-2: __MESSAGE__\n" +
                      "in not\n" +
                      "^^"));
    }
}
