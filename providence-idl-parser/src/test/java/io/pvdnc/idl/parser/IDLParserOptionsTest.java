package io.pvdnc.idl.parser;

import io.pvdnc.core.property.PPropertyMap;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class IDLParserOptionsTest {
    @Test
    public void testOptions() {
        IDLParserOptions options = new IDLParserOptions();
        assertThat(options.parseOptions("test:foo,bar=baz"), is(sameInstance(options)));
        assertThat(options.getParserOptions("foo"), is(PPropertyMap.empty()));
        assertThat(options.getParserOptions("test"), is(PPropertyMap.of("foo", "", "bar", "baz")));

        try {
            options.parseOptions("foo");
            fail("no exception");
        } catch (IllegalArgumentException ex) {
            assertThat(ex.getMessage(), is("Options string must be of format '{name}:{options}*', was: 'foo'"));
        }
    }
}
