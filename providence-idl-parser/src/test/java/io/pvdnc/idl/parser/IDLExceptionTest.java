package io.pvdnc.idl.parser;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class IDLExceptionTest {
    public static Stream<Arguments> testConstructorsData() {
        String string = "123456";
        char[] fb = string.toCharArray();
        IDLToken token = new IDLToken(fb, 0, 1, IDLTokenType.NUMBER, 2, 1);
        IDLToken token2 = new IDLToken(fb, 0, 6, IDLTokenType.NUMBER, 2, 1);
        IOException ex = new IOException("foo");

        return Stream.of(
                arguments(new IDLException(token, "__%s__", "ARG"),
                          "Error on line 2 row 1: __ARG__\n" +
                          "123456\n" +
                          "^"),
                arguments(new IDLException(token, "__REASON__"),
                          "Error on line 2 row 1: __REASON__\n" +
                          "123456\n" +
                          "^"),
                arguments(new IDLException(token, "__REASON__").initCause(ex),
                          "Error on line 2 row 1: __REASON__\n" +
                          "123456\n" +
                          "^"),
                arguments(new IDLException(token2, "__%s__", "ARG"),
                          "Error on line 2 row 1-6: __ARG__\n" +
                          "123456\n" +
                          "^^^^^^"),
                arguments(new IDLException(token2, "__REASON__"),
                          "Error on line 2 row 1-6: __REASON__\n" +
                          "123456\n" +
                          "^^^^^^"),
                arguments(new IDLException(token2, "__REASON__").initCause(ex),
                          "Error on line 2 row 1-6: __REASON__\n" +
                          "123456\n" +
                          "^^^^^^"),
                arguments(new IDLException(ex, "__REASON__"),
                          "Error: __REASON__"),
                arguments(new IDLException(ex, "__%s__", "ARG"),
                          "Error: __ARG__"),
                arguments(new IDLException("__REASON__"),
                          "Error: __REASON__"),
                arguments(new IDLException("__%s__", "ARG"),
                          "Error: __ARG__"),
                arguments(new IDLException(ex, "__REASON__").setFile("foo.json"),
                          "Error in foo.json: __REASON__"),
                arguments(new IDLException(ex, "__%s__", "ARG").setFile("foo.json"),
                          "Error in foo.json: __ARG__"));
    }

    @ParameterizedTest
    @MethodSource("testConstructorsData")
    public void testConstructors(IDLException ex, String display) {
        assertThat(ex.displayString(), is(display));
    }
}
