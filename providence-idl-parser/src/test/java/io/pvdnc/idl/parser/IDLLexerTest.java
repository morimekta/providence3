package io.pvdnc.idl.parser;

import net.morimekta.lexer.TokenizerRepeater;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class IDLLexerTest {
    @Test
    public void testFailures() throws IOException {
        String content = "foo 1";
        char[] fb = content.toCharArray();
        TokenizerRepeater<IDLTokenType, IDLToken> repeater = new TokenizerRepeater<>(List.of(
                new IDLToken(fb, 0, 3, IDLTokenType.IDENTIFIER, 1, 1),
                new IDLToken(fb, 4, 1, IDLTokenType.NUMBER, 1, 5)));
        IDLLexer lexer = new IDLLexer(repeater);
        try {
            lexer.expect("__REASON__", IDLTokenType.NUMBER);
            fail("No exception");
        } catch (IDLException e) {
            assertThat(e.displayString(),
                       is("Error on line 1 row 1-3: Expected __REASON__, but got 'foo'\n" +
                          "foo 1\n" +
                          "^^^"));
        }
        lexer.next();
        lexer.next();
        try {
            lexer.expect("__REASON__", IDLTokenType.NUMBER);
            fail("No exception");
        } catch (IDLException e) {
            assertThat(e.displayString(),
                       is("Error on line 1 row 6: Expected __REASON__, but got end of file\n" +
                          "foo 1\n" +
                          "-----^"));
        }
    }
}
