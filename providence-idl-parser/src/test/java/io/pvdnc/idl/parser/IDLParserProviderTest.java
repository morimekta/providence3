package io.pvdnc.idl.parser;

import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Path;
import java.util.List;

import static io.pvdnc.idl.parser.IDLParserProvider.getParserProviderFor;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class IDLParserProviderTest {
    @TempDir
    public Path tempDir;

    @Test
    public void testProvider() {
        Path foo = tempDir.resolve("foo.json");
        try {
            IDLParserProvider.getParserProviderFor(foo);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No handler for foo.json"));
        }
        try {
            IDLParserProvider.getParserProviderFor(tempDir.resolve("/"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No fileName in filePath /"));
        }

        IDLParserProvider provider = mock(IDLParserProvider.class);
        when(provider.willHandleFile(foo)).thenReturn(true);
        IDLParserProvider other = mock(IDLParserProvider.class);
        when(other.willHandleFile(foo)).thenReturn(false);
        MatcherAssert.assertThat(IDLParserProvider.getParserProviderFor(foo, List.of(other, provider)), is(sameInstance(provider)));
    }
}
