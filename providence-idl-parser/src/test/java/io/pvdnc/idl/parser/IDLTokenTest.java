package io.pvdnc.idl.parser;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class IDLTokenTest {
    @Test
    public void testToken_numbers() {
        assertThat(token("1", IDLTokenType.NUMBER).isSymbol('{'), is(false));
        assertThat(token("1", IDLTokenType.NUMBER).isIdentifier(), is(false));
        assertThat(token("1", IDLTokenType.NUMBER).isQualifiedIdentifier(), is(false));
        assertThat(token("1", IDLTokenType.NUMBER).isDoubleQualifiedIdentifier(), is(false));
        assertThat(token("1", IDLTokenType.NUMBER).isReferenceIdentifier(), is(false));

        assertThat(token("-1", IDLTokenType.NUMBER).isInteger(), is(true));
        assertThat(token("0", IDLTokenType.NUMBER).isInteger(), is(true));
        assertThat(token("1", IDLTokenType.NUMBER).isInteger(), is(true));

        assertThat(token("-1", IDLTokenType.NUMBER).isNonNegativeID(), is(false));
        assertThat(token("0", IDLTokenType.NUMBER).isNonNegativeID(), is(true));
        assertThat(token("1", IDLTokenType.NUMBER).isNonNegativeID(), is(true));

        assertThat(token("-1", IDLTokenType.NUMBER).isPositiveID(), is(false));
        assertThat(token("0", IDLTokenType.NUMBER).isPositiveID(), is(false));
        assertThat(token("1", IDLTokenType.NUMBER).isPositiveID(), is(true));

        assertThat(token("1", IDLTokenType.NUMBER).isSymbol('{'), is(false));
        assertThat(token("1", IDLTokenType.NUMBER).isIdentifier(), is(false));
        assertThat(token("1", IDLTokenType.NUMBER).isQualifiedIdentifier(), is(false));
        assertThat(token("1", IDLTokenType.NUMBER).isDoubleQualifiedIdentifier(), is(false));
        assertThat(token("1", IDLTokenType.NUMBER).isReferenceIdentifier(), is(false));

        assertThat(token("1234", IDLTokenType.NUMBER).parseInteger(), is(1234));
        assertThat(token("0x1234", IDLTokenType.NUMBER).parseInteger(), is(0x1234));
        assertThat(token("01234", IDLTokenType.NUMBER).parseInteger(), is(01234));
        assertThat(token("foo", IDLTokenType.NUMBER).parseInteger(), is(-1));
    }

    @Test
    public void testToken_identifiers() {
        assertThat(token("foo", IDLTokenType.IDENTIFIER).isSymbol('f'), is(false));
        assertThat(token("foo", IDLTokenType.IDENTIFIER).isInteger(), is(false));

        assertThat(token("foo", IDLTokenType.IDENTIFIER).isIdentifier(), is(true));
        assertThat(token("foo", IDLTokenType.IDENTIFIER).isQualifiedIdentifier(), is(false));
        assertThat(token("foo", IDLTokenType.IDENTIFIER).isDoubleQualifiedIdentifier(), is(false));
        assertThat(token("foo", IDLTokenType.IDENTIFIER).isReferenceIdentifier(), is(true));

        assertThat(token("foo.bar", IDLTokenType.IDENTIFIER).isIdentifier(), is(false));
        assertThat(token("foo.bar", IDLTokenType.IDENTIFIER).isQualifiedIdentifier(), is(true));
        assertThat(token("foo.bar", IDLTokenType.IDENTIFIER).isDoubleQualifiedIdentifier(), is(false));
        assertThat(token("foo.bar", IDLTokenType.IDENTIFIER).isReferenceIdentifier(), is(true));

        assertThat(token("foo.bar.baz", IDLTokenType.IDENTIFIER).isIdentifier(), is(false));
        assertThat(token("foo.bar.baz", IDLTokenType.IDENTIFIER).isQualifiedIdentifier(), is(false));
        assertThat(token("foo.bar.baz", IDLTokenType.IDENTIFIER).isDoubleQualifiedIdentifier(), is(true));
        assertThat(token("foo.bar.baz", IDLTokenType.IDENTIFIER).isReferenceIdentifier(), is(true));

        assertThat(token("foo.bar.baz.faz", IDLTokenType.IDENTIFIER).isIdentifier(), is(false));
        assertThat(token("foo.bar.baz.faz", IDLTokenType.IDENTIFIER).isQualifiedIdentifier(), is(false));
        assertThat(token("foo.bar.baz.faz", IDLTokenType.IDENTIFIER).isDoubleQualifiedIdentifier(), is(false));
        assertThat(token("foo.bar.baz.faz", IDLTokenType.IDENTIFIER).isReferenceIdentifier(), is(true));
    }

    @Test
    public void testToken_documentation() {
        assertThat(token("// foo bar", IDLTokenType.DOCUMENTATION).parseDocumentation(),
                   is("foo bar"));
        assertThat(token("/**\n" +
                         " * foo bar\n" +
                         " *\n" +
                         " * bar bar\n" +
                         " */", IDLTokenType.DOCUMENTATION).parseDocumentation(),
                   is("foo bar\n" +
                      "\n" +
                      "bar bar"));
        assertThat(token("\n" +
                         "       * foo bar\n" +
                         "       *\n" +
                         "       * bar bar\n" +
                         "       ", IDLTokenType.DOCUMENTATION).parseDocumentation(),
                   is("foo bar\n" +
                      "\n" +
                      "bar bar"));
    }

    private IDLToken token(String text, IDLTokenType type) {
        return new IDLToken(text.toCharArray(), 0, text.length(), type, 1, 1);
    }
}
