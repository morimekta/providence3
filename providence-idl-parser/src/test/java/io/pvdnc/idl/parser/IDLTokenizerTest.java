package io.pvdnc.idl.parser;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringReader;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class IDLTokenizerTest {
    @Test
    public void testTokenizer() throws IOException {
        StringReader reader = new StringReader("# ignored\n" +
                                               "foo, 1234 \"bar\"");
        IDLTokenizer tokenizer = new IDLTokenizer(reader);
        IDLToken token;
        token = tokenizer.parseNextToken();
        assertThat(token, is(notNullValue()));
        assertThat(token.type(), CoreMatchers.is(IDLTokenType.IDENTIFIER));
        assertThat(token.toString(), is("foo"));
        token = tokenizer.parseNextToken();
        assertThat(token, is(notNullValue()));
        assertThat(token.type(), CoreMatchers.is(IDLTokenType.SYMBOL));
        assertThat(token.isSymbol(','), is(true));
        token = tokenizer.parseNextToken();
        assertThat(token, is(notNullValue()));
        assertThat(token.type(), CoreMatchers.is(IDLTokenType.NUMBER));
        assertThat(token.parseInteger(), is(1234));
        token = tokenizer.parseNextToken();
        assertThat(token, is(notNullValue()));
        assertThat(token.type(), CoreMatchers.is(IDLTokenType.STRING));
        assertThat(token.toString(), is("\"bar\""));
        assertThat(token.decodeString(true), is("bar"));

        assertThat(tokenizer.parseNextToken(), is(nullValue()));
    }

    @Test
    public void testFailures_eof() throws IOException {
        StringReader reader = new StringReader("// ignored1\n" +
                                               "/**/\n" +
                                               "\"foo");
        IDLTokenizer tokenizer = new IDLTokenizer(reader) {
            @Override
            protected boolean parseJavaStyleComments() {
                return true;
            }
        };
        try {
            tokenizer.parseNextToken();
            fail("no exception");
        } catch (IDLException e) {
            assertThat(e.displayString(),
                       is("Error: Unexpected end of stream in string"));
        }
    }

    @Test
    public void testFailures_parse() throws IOException {
        StringReader reader = new StringReader("\"foo\nbar\"");
        IDLTokenizer tokenizer = new IDLTokenizer(reader);
        try {
            tokenizer.parseNextToken();
            fail("no exception");
        } catch (IDLException e) {
            assertThat(e.displayString(),
                       is("Error on line 1 row 5: Unexpected newline in string\n" +
                          "\"foo\n" +
                          "----^"));
        }
    }

    @Test
    public void testTokenizer_javaComment() throws IOException {
        StringReader reader = new StringReader("//\n" +
                                               "foo /**\n" +
                                               "     * foo\n" +
                                               "     *\n" +
                                               "     * bar\n" +
                                               "     */");
        IDLTokenizer tokenizer = new IDLTokenizer(reader) {
            @Override
            protected boolean parseJavaStyleComments() {
                return true;
            }
        };
        IDLToken token;

        token = tokenizer.parseNextToken();
        assertThat(token, is(notNullValue()));
        assertThat(token.isIdentifier(), is(true));
        assertThat(token.toString(), is("foo"));
        token = tokenizer.parseNextToken();
        assertThat(token, is(notNullValue()));
        assertThat(token.type(), CoreMatchers.is(IDLTokenType.DOCUMENTATION));
        assertThat(token.type(), CoreMatchers.is(IDLTokenType.DOCUMENTATION));
    }

    @Test
    public void testFailures_javaComment_eof() throws IOException {
        StringReader reader = new StringReader("/");
        IDLTokenizer tokenizer = new IDLTokenizer(reader) {
            @Override
            protected boolean parseJavaStyleComments() {
                return true;
            }
        };
        try {
            tokenizer.parseNextToken();
            fail("no exception");
        } catch (IDLException e) {
            assertThat(e.displayString(),
                       is("Error: Expected java-style comment, got end of file"));
        }
    }

    @Test
    public void testFailures_javaComment_bad() throws IOException {
        StringReader reader = new StringReader("/-");
        IDLTokenizer tokenizer = new IDLTokenizer(reader) {
            @Override
            protected boolean parseJavaStyleComments() {
                return true;
            }
        };
        try {
            tokenizer.parseNextToken();
            fail("no exception");
        } catch (IDLException e) {
            assertThat(e.displayString(),
                       is("Error on line 1 row 1-2: Expected java-style comment, got '-' after '/'\n" +
                          "/-\n" +
                          "^^"));
        }
    }
}
