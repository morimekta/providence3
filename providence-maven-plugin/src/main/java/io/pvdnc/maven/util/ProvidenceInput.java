/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.maven.util;

import net.morimekta.file.FileUtil;
import io.pvdnc.idl.generator.GeneratorOptions;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.codehaus.plexus.components.io.fileselectors.IncludeExcludeFileSelector;
import org.codehaus.plexus.util.DirectoryScanner;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import static net.morimekta.file.FileUtil.readCanonicalPath;

/**
 * Input utility for providence maven plugins.
 */
public class ProvidenceInput {
    /**
     * Get the set of input files.
     * @param baseDir The maven project base dir.
     * @param inputSelector The input-exclude selector.
     * @param defaultInputInclude The default input include (if not specified).
     * @param print_debug Print debug info to maven log.
     * @param log Maven logger instance.
     * @throws MojoExecutionException If parsing or checking input files failed.
     * @return The set of input files.
     */
    public static Set<Path> getInputFiles(@Nonnull Path baseDir,
                                          IncludeExcludeFileSelector inputSelector,
                                          @Nonnull String defaultInputInclude,
                                          boolean print_debug,
                                          @Nonnull Log log) throws MojoExecutionException {
        try {
            baseDir = readCanonicalPath(baseDir.normalize()).toAbsolutePath();
            TreeSet<Path> inputs = new TreeSet<>();

            DirectoryScanner inputScanner = new DirectoryScanner();
            if (inputSelector != null) {
                if (inputSelector.getIncludes() != null &&
                        inputSelector.getIncludes().length > 0) {
                    if (print_debug) {
                        log.info("Specified includes:");
                        for (String include : inputSelector.getIncludes()) {
                            log.info("    -I " + include);
                        }
                    }
                    inputScanner.setIncludes(inputSelector.getIncludes());
                } else {
                    if (print_debug) {
                        log.info("Default includes: " + defaultInputInclude);
                    }
                    inputScanner.setIncludes(defaultInputInclude.split(","));
                }

                if (inputSelector.getExcludes() != null &&
                        inputSelector.getExcludes().length > 0) {
                    if (print_debug) {
                        log.info("Specified excludes:");
                        for (String exclude : inputSelector.getExcludes()) {
                            log.info("    -E " + exclude);
                        }
                    }
                    inputScanner.setExcludes(inputSelector.getExcludes());
                }
            } else {
                if (print_debug) {
                    log.info("Default input: " + defaultInputInclude);
                }
                inputScanner.setIncludes(defaultInputInclude.split(","));
            }

            inputScanner.setBasedir(baseDir.toFile());
            inputScanner.scan();

            GeneratorOptions options = new GeneratorOptions();
            options.getEnableGenerators().add("all");

            // Include all files included specifically.
            for (String includedFilePath : inputScanner.getIncludedFiles()) {
                Path path = Paths.get(includedFilePath);
            }

            // Include all thrift files in included directories.
            for (String name : inputScanner.getIncludedDirectories()) {
                Path dir = baseDir.resolve(name).normalize();
                for (Path file : FileUtil.list(dir)) {
                    file = dir.resolve(file);
                    if (Files.isHidden(file)) {
                        continue;
                    }

                    if (Files.isReadable(file)) {
                        inputs.add(file);
                    }
                }
            }
            // exclude all files excluded specifically.
            for (String file : inputScanner.getExcludedFiles()) {
                inputs.remove(baseDir.resolve(file).normalize());
            }
            // Exclude all files in excluded directories (and subdirectories).
            for (String dir : inputScanner.getExcludedDirectories()) {
                String path = baseDir.resolve(dir).normalize() + File.separator;
                inputs.removeIf(f -> f.toString().startsWith(path));
            }

            return inputs;
        } catch (IOException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }

    public static String format(Duration duration) {
        long h = duration.toHours();
        long m = duration.minusHours(h).toMinutes();
        if (h > 0) {
            return String.format(Locale.US, "%d:%02d H", h, m);
        }
        long s = duration.minusHours(h).minusMinutes(m).getSeconds();
        if (m > 0) {
            return String.format(Locale.US, "%d:%02d min", m, s);
        }
        long ms = duration.minusHours(h).minusMinutes(m).minusSeconds(s).toMillis();
        return String.format(Locale.US, "%d.%02d s", s, ms / 10);
    }
}
