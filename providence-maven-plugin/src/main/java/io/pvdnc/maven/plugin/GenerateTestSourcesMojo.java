/*
 * Copyright 2016 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.maven.plugin;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.InstantiationStrategy;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.components.io.fileselectors.IncludeExcludeFileSelector;

import java.io.File;
import java.time.Clock;
import java.time.Duration;

import static io.pvdnc.maven.util.ProvidenceInput.format;

/**
 * Generate providence test sources from thrift definitions.
 */
@Mojo(name = "testGenerate",
      defaultPhase = LifecyclePhase.GENERATE_TEST_SOURCES,
      instantiationStrategy = InstantiationStrategy.PER_LOOKUP,
      threadSafe = true)
@SuppressWarnings("unused")
public class GenerateTestSourcesMojo extends BaseGenerateSourcesMojo {
    /**
     * Skip the providence test compile step for this module.
     */
    @Parameter(alias = "skip",
               property = "providence.skip",
               defaultValue = "false")
    protected boolean skipTestGenerate = false;

    /**
     * Location of the output java source.
     */
    @Parameter(defaultValue = "${project.build.directory}/generated-test-sources/providence",
               property = "providence.test.output",
               alias = "testOutputDir")
    private File testOutput = null;

    /**
     * Files to compile. By default will select all '.thrift' files in
     * 'src/test/providence/' and subdirectories. Simple includes can be
     * specified by property <code>providence.test.input</code>.
     */
    @Parameter
    protected IncludeExcludeFileSelector testInput;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        if (skipTestGenerate) {
            getLog().info("Skipping providence:testGenerate");
            return;
        }

        long start = Clock.systemUTC().millis();

        String defaultInputIncludes = System.getProperties()
                                            .getProperty("providence.test.input",
                                                         "src/test/providence/**");
        if (executeInternal(testOutput, testInput, defaultInputIncludes, true)) {
            project.addTestCompileSourceRoot(testOutput.getPath());

            if (printDebug) {
                Duration duration = Duration.ofMillis(Clock.systemUTC().millis() - start);
                getLog().info("Duration: " + format(duration));
            }
        }
    }
}
