/*
 * Copyright 2016 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.maven.plugin;

import net.morimekta.lexer.LexerException;
import io.pvdnc.core.registry.PGlobalFileTypeRegistry;
import io.pvdnc.core.registry.PNamespaceFileTypeRegistry;
import io.pvdnc.idl.generator.GeneratorException;
import io.pvdnc.idl.generator.GeneratorOptions;
import io.pvdnc.idl.generator.GeneratorRunner;
import io.pvdnc.idl.parser.IDLException;
import io.pvdnc.idl.parser.IDLParser;
import io.pvdnc.idl.parser.IDLParserOptions;
import io.pvdnc.idl.parser.IDLParserProvider;
import io.pvdnc.idl.parser.IDLWarningHandler;
import io.pvdnc.maven.util.ProvidenceInput;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.components.io.fileselectors.IncludeExcludeFileSelector;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.time.Clock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.nio.file.Files.createDirectories;
import static io.pvdnc.idl.parser.IDLParserProvider.getParserProviderFor;

/**
 * mvn io.pvdnc:providence-maven-plugin:1.3.1:help -Ddetail=true -Dgoal=generate
 */
public abstract class BaseGenerateSourcesMojo extends AbstractMojo {
    // -----------    PARSER OPTIONS    ----------- //

    @Parameter
    protected Map<String, String> generatorOptions = new HashMap<>();
    @Parameter
    protected Map<String, String> parserOptions = new HashMap<>();

    @Parameter(defaultValue = "all")
    protected List<String> enableGenerators = new ArrayList<>(List.of("all"));

    // -----------    GENERATE OPTIONS    ----------- //
    /**
     * If true add version to generated annotations.
     */
    @Parameter(defaultValue = "true",
               property = "providence.gen.write_generator_version")
    protected boolean writeGeneratorVersion;

    /**
     * If true add date to generated annotations.
     */
    @Parameter(defaultValue = "true",
               property = "providence.gen.write_generated_date")
    protected boolean writeGeneratedDate;

    // -----------    PLUGIN OPTIONS    ----------- //

    /**
     * Print extra debug info to the maven log.
     */
    @Parameter(defaultValue = "true",
               property = "providence.print_debug")
    protected boolean printDebug;

    /**
     * If true will add the generated sources to be compiled.
     */
    @Parameter(defaultValue = "true")
    protected boolean compileOutput;

    // --- After here are internals, components and maven-set params.

    /**
     * Location of the output artifact.
     */
    @Parameter(defaultValue = "${project.build.directory}", readonly = true, required = true)
    protected File buildDir = null;

    @Parameter(defaultValue = "${project}", readonly = true, required = true)
    protected MavenProject project = null;

    boolean executeInternal(File outputDir,
                            IncludeExcludeFileSelector inputSelector,
                            String defaultInputIncludes,
                            boolean testCompile) throws MojoExecutionException, MojoFailureException {
        GeneratorOptions options = new GeneratorOptions(
                writeGeneratedDate ? GeneratorOptions.getCurrentTime(Clock.systemUTC()) : "{date}",
                writeGeneratorVersion ? GeneratorOptions.getProvidenceVersion() : "{version}"
        );
        options.getEnableGenerators().addAll(enableGenerators);
        options.getExtraOptions().putAll(this.generatorOptions);

        Set<Path> inputFiles = ProvidenceInput.getInputFiles(
                project.getBasedir().toPath(), inputSelector, defaultInputIncludes, printDebug, getLog());
        if (inputFiles.isEmpty()) {
            getLog().info("No input");
            return false;
        }

        if (!outputDir.exists()) {
            if (!outputDir.mkdirs()) {
                throw new MojoExecutionException("Unable to create target directory " + outputDir);
            }
        }

        Path workingDir = buildDir.toPath().resolve(testCompile ? "providence-test" : "providence");
        try {
            createDirectories(workingDir);
        } catch (IOException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }

        if (printDebug) {
            inputFiles.forEach(f -> getLog().info("Compiling: " + f));
        }

        PGlobalFileTypeRegistry global       = new PGlobalFileTypeRegistry();
        GeneratorRunner     runner       = new GeneratorRunner(outputDir.toPath(), global, options);
        IDLParserOptions    parseOptions = new IDLParserOptions();
        for (Map.Entry<String,String> option : parserOptions.entrySet()) {
            parseOptions.parseOptions(option.getKey(), option.getValue());
        }

        IDLWarningHandler warningHandler = e -> {
            getLog().warn("    ============ >> PROVIDENCE << ============");
            getLog().warn("");
            Arrays.stream(e.displayString()
                           .split("\r?\n", Short.MAX_VALUE))
                  .forEach(l -> getLog().warn(l));
            getLog().warn("");
            getLog().warn("    ============ << PROVIDENCE >> ============");
        };

        for (Path file : inputFiles) {
            try {
                IDLParserProvider provider = getParserProviderFor(file);
                IDLParser         parser   = provider.getParser(parseOptions);
                PNamespaceFileTypeRegistry registry = parser.parseFile(file, global, warningHandler);
                runner.runGenerators(registry);
            } catch (LexerException e) {
                getLog().error("    ============ >> PROVIDENCE << ============");
                getLog().error("");
                Arrays.stream(e.displayString()
                               .split("\r?\n", Short.MAX_VALUE))
                      .forEach(l -> getLog().error(l));
                getLog().error("");
                getLog().error("    ============ << PROVIDENCE >> ============");
                if (e instanceof IDLException) {
                    throw new MojoFailureException("Failed to parse thrift file: " + ((IDLException) e).getFile(), e);
                }
                throw new MojoFailureException("Failed to parse file: " + e.getMessage(), e);
            } catch (GeneratorException | IOException | IllegalArgumentException e) {
                e.printStackTrace();
                throw new MojoFailureException(e.getMessage(), e);
            }
        }

        return compileOutput;
    }
}
