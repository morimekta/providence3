module io.pvdnc.rpc.http.client {
    exports io.pvdnc.rpc.http.client;

    requires transitive io.pvdnc.core;
    requires net.morimekta.collect;
    requires net.morimekta.io;
    requires java.net.http;
}