/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.rpc.http.client;

import io.pvdnc.core.PMessage;
import io.pvdnc.core.io.PSerializer;
import io.pvdnc.core.io.PSerializerProvider;
import io.pvdnc.core.rpc.PServiceCallHandler;
import io.pvdnc.core.types.PServiceMethod;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.UncheckedIOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.function.Supplier;

import static java.net.http.HttpRequest.BodyPublishers.ofInputStream;
import static java.util.Objects.requireNonNull;

/**
 * A client handler using native Java HTTP library, connecting to
 * a URI based on a simple 1 path per method, default using {@code /methodName}.
 * The client does not size it's output, and assumes all non-200
 * status codes are general HTTP exceptions not part of the protocol.
 */
public class HttpClientHandler implements PServiceCallHandler, Closeable {
    private final HttpClient                    httpClient;
    private final Supplier<HttpRequest.Builder> httpRequestBuilderSupplier;
    private final PSerializerProvider           provider;
    private final Function<PServiceMethod, URI> uriForMethod;
    private final ExecutorService               executorService;

    public HttpClientHandler(String baseURI) {
        this(HttpClient.newBuilder().build(),
             HttpRequest::newBuilder,
             new PSerializerProvider().load(),
             method -> URI.create(baseURI + "/" + method.getName()),
             Executors.newFixedThreadPool(5, run -> {
                 Thread thread = new Thread(run);
                 thread.setDaemon(true);
                 thread.setName("providence-http-writer@" + thread.getId());
                 return thread;
             }));
    }

    public HttpClientHandler(HttpClient httpClient,
                             Supplier<HttpRequest.Builder> httpRequestBuilderSupplier,
                             PSerializerProvider serializerProvider,
                             Function<PServiceMethod, URI> uriForMethod,
                             ExecutorService executorService) {
        this.httpClient = requireNonNull(httpClient, "httpClient == null");
        this.httpRequestBuilderSupplier = requireNonNull(httpRequestBuilderSupplier, "requestBuilderSupplier == null");
        this.provider = requireNonNull(serializerProvider, "serializerProvider == null");
        this.uriForMethod = requireNonNull(uriForMethod, "uriForMethod == null");
        this.executorService = requireNonNull(executorService, "executorService == null");
    }

    @Override
    public <RQ extends PMessage, RS extends PMessage>
    RS handleCall(PServiceMethod method, RQ request)
            throws IOException {
        try {
            if (executorService.isShutdown()) {
                throw new IOException("Calling on closed client");
            }
            PSerializer serializer = provider.defaultSerializer();
            URI         uri        = uriForMethod.apply(method);
            HttpResponse<InputStream> httpResponse = httpClient.send(
                    makeRequest(request, uri),
                    HttpResponse.BodyHandlers.ofInputStream());
            if (httpResponse.statusCode() == 200  /* OK */) {
                PSerializer outputSerializer = provider.forMediaTypes(httpResponse.headers().allValues("Content-Type"))
                                                       .orElse(serializer);
                try (BufferedInputStream bin = new BufferedInputStream(httpResponse.body())) {
                    return outputSerializer.readFrom(method.getResponseType(), bin);
                }
            } else if (httpResponse.statusCode() == 204  /* NO CONTENT */) {
                httpResponse.body().close();
                if (method.getResponseType() == null) {
                    return null;
                }
                throw new HttpResponseException(uri, httpResponse.statusCode(), null);
            } else {
                String content;
                try (BufferedInputStream bin = new BufferedInputStream(httpResponse.body())) {
                    content = new String(bin.readAllBytes(), StandardCharsets.UTF_8);
                } catch (IOException e) {
                    throw new HttpResponseException(uri, httpResponse.statusCode(), null, e);
                }
                throw new HttpResponseException(uri, httpResponse.statusCode(), content);
            }
        } catch (InterruptedException ie) {
            close();
            // Thread.currentThread().interrupt();
            throw new RuntimeException(ie);
        }
    }

    protected HttpRequest makeRequest(PMessage request, URI url) {
        PSerializer serializer = provider.defaultSerializer();
        return httpRequestBuilderSupplier
                .get()
                .uri(url)
                .header("Content-Type", serializer.mediaTypes().get(0))
                .header("Accept", "*/*")
                .POST(ofInputStream(() -> pipedWriter(request, serializer)))
                .build();
    }

    private InputStream pipedWriter(PMessage request, PSerializer serializer) {
        try {
            PipedOutputStream out = new PipedOutputStream();
            PipedInputStream  in  = new PipedInputStream(out);
            executorService.submit(() -> pipedRunner(request, serializer, out));
            return in;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private void pipedRunner(PMessage request, PSerializer serializer, OutputStream out) {
        try (BufferedOutputStream bout = new BufferedOutputStream(out)) {
            serializer.writeTo(request, bout);
            bout.flush();
        } catch (IOException e) {
            try {
                out.close();
            } catch (IOException e2) {
                e.addSuppressed(e2);
            }
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public void close() {
        executorService.shutdownNow();
    }
}
