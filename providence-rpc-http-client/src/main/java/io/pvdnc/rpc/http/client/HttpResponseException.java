/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.rpc.http.client;

import java.io.IOException;
import java.net.URI;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * An IO exception for HTTP response exceptions. Normalization between
 * client library exceptions, or lack thereof.
 *
 * @since 2.6
 */
public class HttpResponseException extends IOException {
    private static final long serialVersionUID = 0xa013ef97976d90b7L;

    private final URI    url;
    private final int    statusCode;
    private final String content;

    /**
     * @param url The URL tried to connect to.
     * @param statusCode The response code.
     * @param content The response body, if any.
     */
    public HttpResponseException(
            URI url,
            int statusCode,
            String content) {
        super("Http response not OK: " + statusCode);
        this.url = requireNonNull(url, "url == null");
        this.statusCode = statusCode;
        this.content = content;
    }

    /**
     * @param url The URL tried to connect to.
     * @param statusCode The response code.
     * @param content The response body, if any.
     * @param cause The cause of the exception, if any.
     */
    public HttpResponseException(
            URI url,
            int statusCode,
            String content,
            IOException cause) {
        super("Http response not OK: " + statusCode, cause);
        this.url = requireNonNull(url, "url == null");
        this.statusCode = statusCode;
        this.content = content;
    }

    public URI getUrl() {
        return url;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getContent() {
        return content;
    }

    // ---

    @Override
    public HttpResponseException initCause(Throwable cause) {
        super.initCause(cause);
        return this;
    }

    // --- Object

    @Override
    public String toString() {
        return "PHttpResponseException{" +
               "url=" + url +
               ", statusCode=" + statusCode +
               ", content='" + content + '\'' +
               '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HttpResponseException that = (HttpResponseException) o;
        return statusCode == that.statusCode &&
               url.equals(that.url) &&
               Objects.equals(content, that.content) &&
               Objects.equals(getCause(), that.getCause());
    }

    @Override
    public int hashCode() {
        return Objects.hash(url, statusCode, content, getCause());
    }
}
