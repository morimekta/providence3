package io.pvdnc.rpc.http.client;

import io.pvdnc.core.io.PSerializer;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.types.PServiceMethod;
import io.pvdnc.rpc.client.test.PvdMessage;
import io.pvdnc.rpc.client.test.PvdService;
import io.pvdnc.sio.gson.GsonSerializer;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class HttpClientHandlerTest {
    private Server server;
    private int    port;

    private PvdMessage actualRequest;
    private String     actualRequestPath;
    private int         responseCode;
    private PvdMessage response;
    private PSerializer serializer;

    @BeforeEach
    public void setUp() throws Exception {
        serializer = new GsonSerializer();
        actualRequest = null;
        actualRequestPath = null;
        responseCode = 200;
        response = null;

        ServletContextHandler handler = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);

        HttpServlet servlet = new HttpServlet() {
            @Override
            protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
                actualRequestPath = req.getPathInfo();
                actualRequest = serializer.readFrom(PvdMessage.kDescriptor, req.getInputStream());
                if (responseCode == 200) {
                    resp.setContentType(serializer.mediaTypes().get(0));
                    serializer.writeTo(response, resp.getOutputStream());
                } else {
                    resp.sendError(responseCode);
                    resp.flushBuffer();
                }
            }
        };
        handler.addServlet(new ServletHolder(servlet), "/foo/*");

        server = new Server(0);
        server.setHandler(handler);
        server.start();
        port = ((ServerConnector) server.getConnectors()[0]).getLocalPort();
    }

    @AfterEach
    public void tearDown() throws Exception {
        if (server != null) {
            server.stop();
            server.destroy();
        }
    }

    @Test
    public void testHttpClient() throws IOException {
        PServiceMethod method = new PServiceMethod(
                "bar",
                PvdMessage.kDescriptor,
                PvdMessage.kDescriptor,
                PPropertyMap.empty(),
                () -> PvdService.kDescriptor);
        HttpClientHandler clientHandler = new HttpClientHandler("http://localhost:" + port + "/foo");

        response = PvdMessage.newBuilder().setI(43).build();
        PvdMessage request = PvdMessage.newBuilder().setI(42).build();

        PvdMessage actualResponse = clientHandler.handleCall(method, request);

        assertThat(actualResponse, is(response));
        assertThat(actualRequest, is(request));
        assertThat(actualRequestPath, is("/bar"));
    }

    @Test
    public void testHttpClient_Error() throws IOException {
        PServiceMethod method = new PServiceMethod(
                "bar",
                PvdMessage.kDescriptor,
                PvdMessage.kDescriptor,
                PPropertyMap.empty(),
                () -> PvdService.kDescriptor);
        HttpClientHandler clientHandler = new HttpClientHandler("http://localhost:" + port + "/foo");

        responseCode = 401;
        PvdMessage request = PvdMessage.newBuilder().setI(42).build();

        try {
            clientHandler.handleCall(method, request);
            fail("no exception");
        } catch (HttpResponseException e) {
            assertThat(e.getMessage(), is("Http response not OK: 401"));
            assertThat(e.getUrl(), is(notNullValue()));
            assertThat(e.getUrl().toString(), is("http://localhost:" + port + "/foo/bar"));
            assertThat(e.getContent(), containsString("Error 401 Unauthorized"));
            assertThat(e.getStatusCode(), is(401));
            assertThat(e.getCause(), is(nullValue()));

            assertThat(actualRequest, is(request));
            assertThat(actualRequestPath, is("/bar"));
        }

        clientHandler.close();
    }
}
