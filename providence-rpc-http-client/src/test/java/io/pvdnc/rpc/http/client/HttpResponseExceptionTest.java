package io.pvdnc.rpc.http.client;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URI;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class HttpResponseExceptionTest {
    @Test
    public void testException() {
        URI                   uri = URI.create("http://localhost:8080/foo");
        HttpResponseException ex1 = new HttpResponseException(uri, 401, "null");
        assertThat(ex1.getUrl(), is(uri));
        assertThat(ex1.getStatusCode(), is(401));
        assertThat(ex1.getContent(), is("null"));
        assertThat(ex1.getCause(), is(nullValue()));

        IOException           foo = new IOException("foo");
        HttpResponseException ex2 = new HttpResponseException(uri, 401, "null", foo);
        assertThat(ex2.getUrl(), is(uri));
        assertThat(ex2.getStatusCode(), is(401));
        assertThat(ex2.getContent(), is("null"));
        assertThat(ex2.getCause(), is(foo));

        assertThat(ex1.hashCode(), is(ex1.hashCode()));
        assertThat(ex1.hashCode(), is(not(ex2.hashCode())));

        assertThat(ex1, CoreMatchers.is(ex1));
        assertThat(ex2, is(CoreMatchers.not(ex1)));
        assertThat(ex1, is(not(new Object())));

        assertThat(ex1.toString(), is("PHttpResponseException{url=http://localhost:8080/foo, statusCode=401, content='null'}"));

        ex1 = ex1.initCause(foo);
        assertThat(ex1, CoreMatchers.is(ex2));
        assertThat(ex1.hashCode(), is(ex2.hashCode()));
    }
}
