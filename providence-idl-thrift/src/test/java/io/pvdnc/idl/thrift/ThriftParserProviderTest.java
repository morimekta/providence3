package io.pvdnc.idl.thrift;

import io.pvdnc.idl.parser.IDLParser;
import io.pvdnc.idl.parser.IDLParserOptions;
import io.pvdnc.idl.parser.IDLParserProvider;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Path;
import java.util.List;

import static io.pvdnc.idl.thrift.ThriftParserProperties.ALLOW_LANGUAGE_RESERVED_NAMES;
import static io.pvdnc.idl.thrift.ThriftParserProperties.REQUIRE_ENUM_VALUE;
import static io.pvdnc.idl.thrift.ThriftParserProperties.REQUIRE_FIELD_ID;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;

public class ThriftParserProviderTest {
    @TempDir
    public Path tempDir;

    @Test
    public void testProvider() {
        ThriftParserProvider provider = new ThriftParserProvider();
        assertThat(provider.willHandleFile(tempDir.resolve("foo.thrift")), is(true));
        assertThat(provider.willHandleFile(tempDir.resolve("bar.json")), is(false));

        assertThat(provider.getOptions(), containsInAnyOrder(REQUIRE_FIELD_ID,
                                                             REQUIRE_ENUM_VALUE,
                                                             ALLOW_LANGUAGE_RESERVED_NAMES));

        IDLParser parser = provider.getParser(new IDLParserOptions());
        assertThat(parser, is(notNullValue()));

        List<IDLParserProvider> providers = IDLParserProvider.getParserProviders();
        assertThat(providers, hasItem(instanceOf(ThriftParserProvider.class)));
    }
}
