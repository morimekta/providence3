package io.pvdnc.idl.thrift;

import io.pvdnc.core.registry.PGlobalFileTypeRegistry;
import io.pvdnc.core.registry.PNamespaceFileTypeRegistry;
import io.pvdnc.idl.parser.IDLException;
import io.pvdnc.idl.parser.IDLWarningHandler;
import io.pvdnc.idl.thrift.test_util.ResourceUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class ThriftParserTest {
    @TempDir
    public File tempDir;
    private Path temp;

    @BeforeEach
    public void setUp() throws IOException {
        temp = tempDir.getAbsoluteFile().getCanonicalFile().toPath();
    }

    public static Stream<Arguments> testParse_ParseFailure() {
        return Stream.of(arguments("conflicting_field_name.thrift",
                                   "Error in conflicting_field_name.thrift on line 5 row 10-22: Field with conflicting name separated_name already declared on line 4.\n" +
                                   "  2: i32 separatedName;\n" +
                                   "---------^^^^^^^^^^^^^"),
                         arguments("duplicate_field_id.thrift",
                                   "Error in duplicate_field_id.thrift on line 6 row 3: Field with id 1 already declared on line 5.\n" +
                                   "  1: i32 second;\n" +
                                   "--^"),
                         arguments("duplicate_field_name.thrift",
                                   "Error in duplicate_field_name.thrift on line 5 row 10-14: Field with name first already declared on line 4.\n" +
                                   "  2: i32 first;\n" +
                                   "---------^^^^^"),
                         arguments("if_not_allowed.thrift",
                                   "Error in if_not_allowed.thrift on line 4 row 1-9: Unexpected token 'interface'\n" +
                                   "interface ToImplement {\n" +
                                   "^^^^^^^^^"),
                         arguments("invalid_include.thrift",
                                   "Error in invalid_include.thrift on line 8 row 1-7: Unexpected token 'include', expected type declaration\n" +
                                   "include \"valid_reference.thrift\"\n" +
                                   "^^^^^^^"),
                         arguments("invalid_namespace.thrift",
                                   "Error in invalid_namespace.thrift on line 1 row 16-27: Identifier with double '.'\n" +
                                   "namespace java org.apache..test.failure\n" +
                                   "---------------^^^^^^^^^^^^"),
                         arguments("proto_stub_in_thrift.thrift",
                                   "Error in proto_stub_in_thrift.thrift on line 15 row 26-32: Field id required, but missing\n" +
                                   "    TextArgs protoMethod(Request);\n" +
                                   "-------------------------^^^^^^^"),
                         arguments("reserved_field_name.thrift",
                                   "Error in reserved_field_name.thrift on line 4 row 10-15: Field with reserved name: global\n" +
                                   "  1: i32 global;\n" +
                                   "---------^^^^^^"),
                         arguments("unknown_program.thrift",
                                   "Error in unknown_program.thrift on line 4 row 6-28: No such include valid_reference for type valid_reference.Message\n" +
                                   "  1: valid_reference.Message message;\n" +
                                   "-----^^^^^^^^^^^^^^^^^^^^^^^"));
    }

    @MethodSource("testParse_ParseFailure")
    @ParameterizedTest
    public void testParse_ParseFailure(String name,
                                       String displayString) throws IOException {
        fromResource("failure", "valid_reference.thrift");
        try {
            ThriftParser        parser         = new ThriftParser(true, true, false);
            PGlobalFileTypeRegistry registry       = new PGlobalFileTypeRegistry();
            IDLWarningHandler   warningHandler = ex -> {};
            parser.parseFile(fromResource("failure", name), registry, warningHandler);
            fail("no exception");
        } catch (IDLException e) {
            assertThat(e.displayString(),
                       is(displayString));
        }
    }

    public static Stream<Arguments> testParse_ConvertFailure() {
        return Stream.of(arguments("unknown_include.thrift",
                                   "Error in unknown_include.thrift on line 3 row 9-29: No such file to be included\n" +
                                   "include \"no_such_file.thrift\"\n" +
                                   "--------^^^^^^^^^^^^^^^^^^^^^"),
                         arguments("unknown_include_type.thrift",
                                   "Error on line 6 row 6-28: No such type 'Unknown' in valid_reference.thrift\n" +
                                   "  1: valid_reference.Unknown message;\n" +
                                   "-----^^^^^^^^^^^^^^^^^^^^^^^"),
                         arguments("unknown_type.thrift",
                                   "Error on line 4 row 6-14: Unknown type name OtherType\n" +
                                   "  1: OtherType message;\n" +
                                   "-----^^^^^^^^^"));
    }

    @MethodSource("testParse_ConvertFailure")
    @ParameterizedTest
    public void testParse_ConvertFailure(String name,
                                         String displayString) throws IOException {
        fromResource("failure_convert", "valid_reference.thrift");
        try {
            ThriftParser        parser         = new ThriftParser(true, true, false);
            PGlobalFileTypeRegistry registry       = new PGlobalFileTypeRegistry();
            IDLWarningHandler   warningHandler = ex -> {};
            parser.parseFile(fromResource("failure_convert", name), registry, warningHandler);
            fail("no exception");
        } catch (IDLException e) {
            try {
                assertThat(e.displayString(),
                           is(displayString));
            } catch (AssertionError a) {
                e.printStackTrace();
                throw a;
            }
        }
    }
    public static Stream<Arguments> testParse_Regression() {
        return Stream.of(arguments(listOf("calculator/calculator.thrift",
                                          "calculator/number.thrift"), false),
                         arguments(listOf("calculator/calculator_strict.thrift",
                                          "calculator/number.thrift"), true),
                         arguments(listOf("math/calculator.thrift",
                                          "calculator/number.thrift"), false),
                         arguments(listOf("tests/annotations.thrift"), false),
                         arguments(listOf("tests/autoid.thrift"), false),
                         arguments(listOf("tests/autovalue.thrift"), false),
                         arguments(listOf("tests/naming.thrift"), false),
                         arguments(listOf("tests/test.thrift"), true),
                         arguments(listOf("common_fileserver.thrift"), true),
                         arguments(listOf("model.thrift"), false));
    }

    @MethodSource("testParse_Regression")
    @ParameterizedTest
    public void testParse_Regression(List<String> resources, boolean strict)
            throws IOException {
        List<Path> files = resources.stream()
                                    .map(name -> fromResource("regression", name))
                                    .collect(Collectors.toList());
        ThriftParser           parser         = new ThriftParser(strict, strict, !strict);
        PGlobalFileTypeRegistry registry       = new PGlobalFileTypeRegistry();
        IDLWarningHandler      warningHandler = ex -> {};
        PNamespaceFileTypeRegistry rtr            = parser.parseFile(files.get(0), registry, warningHandler);
        assertThat(rtr, is(notNullValue()));
    }


    private Path fromResource(String group, String name) {
        return ResourceUtil.fromResource(temp.resolve(name),
                                  "/io/pvdnc/idl/thrift/parser/" + group + "/" + name);
    }
}
