package io.pvdnc.idl.thrift.test_util;

import io.pvdnc.idl.thrift.ThriftParserTest;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import static java.util.Objects.requireNonNull;

public class ResourceUtil {
    public static Path fromResource(Path target, String resource) {
        try {
            Files.createDirectories(target.getParent());
            try (InputStream in = requireNonNull(ThriftParserTest.class.getResourceAsStream(resource),
                                                 "no resource " + resource);
                 BufferedInputStream bin = new BufferedInputStream(in)) {
                Files.write(target, bin.readAllBytes(), StandardOpenOption.CREATE_NEW);
                return target;
            }
        } catch (IOException e) {
            throw new AssertionError(e.getMessage());
        }
    }

}
