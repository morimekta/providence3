package io.pvdnc.idl.thrift;

import net.morimekta.file.FileUtil;
import io.pvdnc.core.registry.PGlobalFileTypeRegistry;
import io.pvdnc.core.registry.PNamespaceFileTypeRegistry;
import io.pvdnc.idl.generator.GeneratorOptions;
import io.pvdnc.idl.generator.GeneratorRunner;
import io.pvdnc.idl.parser.IDLWarningHandler;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static net.morimekta.collect.UnmodifiableList.listOf;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class ThriftGeneratorTest {
    @TempDir
    public static Path source;

    public static Path outputDir;

    @BeforeAll
    public static void setUpAll() throws IOException {
        if (Files.isDirectory(Path.of("providence-idl-thrift"))) {
            outputDir = FileUtil.readCanonicalPath(Path.of("providence-idl-thrift/target/providence"));
        } else {
            outputDir = FileUtil.readCanonicalPath(Path.of("target/providence"));
        }
        FileUtil.deleteRecursively(outputDir);
        Files.createDirectories(outputDir);
    }

    public static Stream<Arguments> testGeneratorData() {
        return Stream.of(arguments(listOf("calculator/calculator.thrift",
                                          "calculator/number.thrift"), false));
    }

    @ParameterizedTest
    @MethodSource("testGeneratorData")
    public void testGenerator(List<String> files, boolean strict) throws IOException {
        GeneratorOptions options = new GeneratorOptions();
        options.getEnableGenerators().add("all");
        ThriftParser parser = new ThriftParser(strict, strict, !strict);

        PGlobalFileTypeRegistry global = new PGlobalFileTypeRegistry();
        GeneratorRunner     runner = new GeneratorRunner(outputDir, global, options);

        for (String file : files) {
            Path sourceFile = source.resolve(file);
            Files.createDirectories(sourceFile.getParent());
            try (InputStream in = ThriftGeneratorTest.class.getResourceAsStream(
                    "/io/pvdnc/idl/thrift/parser/regression/" + file);
                 BufferedInputStream bin = new BufferedInputStream(in)) {
                Files.write(sourceFile, bin.readAllBytes(), CREATE, TRUNCATE_EXISTING);
            }
        }

        for (String file : files) {
            Path                   sourceFile     = source.resolve(file);
            IDLWarningHandler      warningHandler = ex -> {};
            PNamespaceFileTypeRegistry registry       = parser.parseFile(sourceFile, global, warningHandler);
            runner.runGenerators(registry);
        }
    }
}
