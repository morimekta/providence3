package io.pvdnc.idl.thrift.parser;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.io.InputStream;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class ThriftProgramParserTest {
    public static Stream<Arguments> testParse_Regression() {
        return Stream.of(arguments("calculator/calculator.thrift", false, 
                                   "namespace java net.morimekta.test.calculator\n" +
                                   "include \"number.thrift\"\n" +
                                   "enum Operator {\n" +
                                   "    IDENTITY = 1;\n" +
                                   "    ADD;\n" +
                                   "    SUBTRACT;\n" +
                                   "    MULTIPLY;\n" +
                                   "    DIVIDE;\n" +
                                   "}\n" +
                                   "union Operand {\n" +
                                   "    1: Operation operation;\n" +
                                   "    2: double number;\n" +
                                   "    3: number.Imaginary imaginary;\n" +
                                   "}\n" +
                                   "struct Operation {\n" +
                                   "    1: Operator operator;\n" +
                                   "    2: list<Operand> operands;\n" +
                                   "}\n" +
                                   "exception CalculateException {\n" +
                                   "    1: string message;\n" +
                                   "    2: Operation operation;\n" +
                                   "}\n" +
                                   "service BaseCalculator {\n" +
                                   "    oneway void iamalive(\n" +
                                   "    )\n" +
                                   "    i32 numCalls(\n" +
                                   "        1: i32 minus;\n" +
                                   "        2: i32 inAdditionTo;\n" +
                                   "    )\n" +
                                   "}\n" +
                                   "service Calculator extends BaseCalculator {\n" +
                                   "    Operand calculate(\n" +
                                   "        1: Operation op;\n" +
                                   "    ) throws (\n" +
                                   "        1: CalculateException ce;\n" +
                                   "    )\n" +
                                   "}\n" +
                                   "const Operand PI = {\"number\":3.141592};\n" +
                                   "const set<Operator> kComplexOperands = [Operator.MULTIPLY,Operator.DIVIDE];\n"),
                         arguments("calculator/calculator_strict.thrift", true,
                                   "namespace java net.morimekta.test.calculator\n" +
                                   "include \"number.thrift\"\n" +
                                   "enum Operator {\n" +
                                   "    IDENTITY = 1;\n" +
                                   "    ADD = 2;\n" +
                                   "    SUBTRACT = 3;\n" +
                                   "    MULTIPLY = 4;\n" +
                                   "    DIVIDE = 5;\n" +
                                   "}\n" +
                                   "union Operand {\n" +
                                   "    1: Operation operation;\n" +
                                   "    2: double number;\n" +
                                   "    3: number.Imaginary imaginary;\n" +
                                   "}\n" +
                                   "struct Operation {\n" +
                                   "    1: Operator operator;\n" +
                                   "    2: list<Operand> operands;\n" +
                                   "}\n" +
                                   "exception CalculateException {\n" +
                                   "    1: string message;\n" +
                                   "    2: Operation operation;\n" +
                                   "}\n" +
                                   "service Calculator {\n" +
                                   "    Operand calculate(\n" +
                                   "        1: Operation op;\n" +
                                   "    ) throws (\n" +
                                   "        1: CalculateException ce;\n" +
                                   "    )\n" +
                                   "    oneway void iamalive(\n" +
                                   "    )\n" +
                                   "}\n" +
                                   "const Operand PI = {\"number\":3.141592};\n" +
                                   "const set<Operator> kComplexOperands = [Operator.MULTIPLY,Operator.DIVIDE];\n"),
                         arguments("calculator/number.thrift", true,
                                   "namespace java net.morimekta.test.number\n" +
                                   "typedef real double;\n" +
                                   "struct Imaginary {\n" +
                                   "    1: real v;\n" +
                                   "    2: double i;\n" +
                                   "}\n" +
                                   "typedef I Imaginary;\n" +
                                   "const Imaginary kSqrtMinusOne = {\"v\":0.0,\"i\":-1.0};\n"),
                         arguments("math/calculator.thrift", false,
                                   "namespace java net.morimekta.test.providence.testing.math\n" +
                                   "include \"../calculator/number.thrift\"\n" +
                                   "struct OtherCalculator {\n" +
                                   "    1: number.Imaginary math;\n" +
                                   "}\n" +
                                   "const string kValue = \"constant value\";\n" +
                                   "const map<string,i32> kMap = {kValue:12};\n" +
                                   "enum Numbers {\n" +
                                   "    E;\n" +
                                   "    PI;\n" +
                                   "}\n" +
                                   "const map<i32,number.Imaginary> imaginaries = {Numbers.E:{'v':3.141592;'i':-2.71828;},};\n"),
                         arguments("tests/annotations.thrift", false,
                                   "namespace java net.morimekta.test.annotations\n" +
                                   "enum E {\n" +
                                   "    VAL;\n" +
                                   "}\n" +
                                   "exception S {\n" +
                                   "    1: bool val;\n" +
                                   "}\n" +
                                   "service Srv {\n" +
                                   "    void method(\n" +
                                   "        1: i32 param;\n" +
                                   "    )\n" +
                                   "    void method2(\n" +
                                   "        1: i32 param;\n" +
                                   "    ) throws (\n" +
                                   "        1: S e;\n" +
                                   "    )\n" +
                                   "}\n" +
                                   ""),
                         arguments("tests/autoid.thrift", false,
                                   "namespace java net.morimekta.test.autoid\n" +
                                   "exception AutoId {\n" +
                                   "    string message;\n" +
                                   "    i32 second;\n" +
                                   "}\n" +
                                   "service AutoParam {\n" +
                                   "    i32 method(\n" +
                                   "        i32 a;\n" +
                                   "        i32 b;\n" +
                                   "    ) throws (\n" +
                                   "        AutoId auto1;\n" +
                                   "    )\n" +
                                   "}\n"),
                         arguments("tests/autovalue.thrift", false,
                                   "namespace java net.morimekta.test.autoid\n" +
                                   "enum AutoValue {\n" +
                                   "    FIRST;\n" +
                                   "    SECOND;\n" +
                                   "    THIRD;\n" +
                                   "}\n" +
                                   "enum PartialAutoValue {\n" +
                                   "    FIRST = 5;\n" +
                                   "    SECOND;\n" +
                                   "    THIRD;\n" +
                                   "}\n"),
                         arguments("tests/naming.thrift", false,
                                   "namespace java net.morimekta.test.naming\n" +
                                   "struct FieldNames {\n" +
                                   "    1: i32 lowercase;\n" +
                                   "    2: i32 camelCase;\n" +
                                   "    3: i32 PascalCase;\n" +
                                   "    4: i32 c_case;\n" +
                                   "    5: i32 UPPER_CASE;\n" +
                                   "    6: i32 Mixed_Case;\n" +
                                   "}\n" +
                                   "enum EnumNames {\n" +
                                   "    lowercase;\n" +
                                   "    camelCase;\n" +
                                   "    PascalCase;\n" +
                                   "    c_case;\n" +
                                   "    UPPER_CASE;\n" +
                                   "    Mixed_Case;\n" +
                                   "}\n" +
                                   "enum Fields {\n" +
                                   "    sDescriptor;\n" +
                                   "    mName;\n" +
                                   "    mValue;\n" +
                                   "    Field;\n" +
                                   "}\n" +
                                   "exception Builder {\n" +
                                   "    1: Builder Builder;\n" +
                                   "}\n" +
                                   "union Factory {\n" +
                                   "    1: Provider Factory;\n" +
                                   "}\n" +
                                   "struct Provider {\n" +
                                   "    1: Provider Provider;\n" +
                                   "    2: Factory Factory;\n" +
                                   "    3: Builder Builder;\n" +
                                   "    4: Fields Fields;\n" +
                                   "    5: i32 descriptor;\n" +
                                   "    6: i32 kDescriptor;\n" +
                                   "}\n"),
                         arguments("tests/test.thrift", true,
                                   "namespace java net.morimekta.test.providence\n" +
                                   "enum Value {\n" +
                                   "    FIRST = 1;\n" +
                                   "    SECOND = 2;\n" +
                                   "    THIRD = 3;\n" +
                                   "    FOURTH = 5;\n" +
                                   "    FIFTH = 8;\n" +
                                   "    SIXTH = 13;\n" +
                                   "    SEVENTH = 21;\n" +
                                   "    EIGHTH = 34;\n" +
                                   "    NINTH = 55;\n" +
                                   "    TENTH = 89;\n" +
                                   "    ELEVENTH = 144;\n" +
                                   "    TWELWETH = 233;\n" +
                                   "    THIRTEENTH = 377;\n" +
                                   "    FOURTEENTH = 610;\n" +
                                   "    FIFTEENTH = 987;\n" +
                                   "    SIXTEENTH = 1597;\n" +
                                   "    SEVENTEENTH = 2584;\n" +
                                   "    EIGHTEENTH = 4181;\n" +
                                   "    NINTEENTH = 6765;\n" +
                                   "    TWENTIETH = 10946;\n" +
                                   "}\n" +
                                   "struct CompactFields {\n" +
                                   "    1: string name;\n" +
                                   "    2: i32 id;\n" +
                                   "    3: string label;\n" +
                                   "}\n" +
                                   "const list<CompactFields> kDefaultCompactFields = [{\"name\":\"Tut-Ankh-Amon\",\"id\":1333,\"label\":\"dead\"},{\"name\":\"Ramses II\",\"id\":1279}];\n" +
                                   "const string kString = \"string\";\n" +
                                   "const i32 kInt = 1234;\n" +
                                   "struct OptionalFields {\n" +
                                   "    1: bool booleanValue;\n" +
                                   "    2: byte byteValue;\n" +
                                   "    3: i16 shortValue;\n" +
                                   "    4: i32 integerValue;\n" +
                                   "    5: i64 longValue;\n" +
                                   "    6: double doubleValue;\n" +
                                   "    7: string stringValue;\n" +
                                   "    8: binary binaryValue;\n" +
                                   "    9: Value enumValue;\n" +
                                   "    10: CompactFields compactValue;\n" +
                                   "}\n" +
                                   "struct RequiredFields {\n" +
                                   "    1: bool booleanValue;\n" +
                                   "    2: byte byteValue;\n" +
                                   "    3: i16 shortValue;\n" +
                                   "    4: i32 integerValue;\n" +
                                   "    5: i64 longValue;\n" +
                                   "    6: double doubleValue;\n" +
                                   "    7: string stringValue;\n" +
                                   "    8: binary binaryValue;\n" +
                                   "    9: Value enumValue;\n" +
                                   "    10: CompactFields compactValue;\n" +
                                   "}\n" +
                                   "struct DefaultFields {\n" +
                                   "    1: bool booleanValue;\n" +
                                   "    2: byte byteValue;\n" +
                                   "    3: i16 shortValue;\n" +
                                   "    4: i32 integerValue;\n" +
                                   "    5: i64 longValue;\n" +
                                   "    6: double doubleValue;\n" +
                                   "    7: string stringValue;\n" +
                                   "    8: binary binaryValue;\n" +
                                   "    9: Value enumValue;\n" +
                                   "    10: CompactFields compactValue;\n" +
                                   "}\n" +
                                   "union UnionFields {\n" +
                                   "    1: bool booleanValue;\n" +
                                   "    2: byte byteValue;\n" +
                                   "    3: i16 shortValue;\n" +
                                   "    4: i32 integerValue;\n" +
                                   "    5: i64 longValue;\n" +
                                   "    6: double doubleValue;\n" +
                                   "    7: string stringValue;\n" +
                                   "    8: binary binaryValue;\n" +
                                   "    9: Value enumValue;\n" +
                                   "    10: CompactFields compactValue;\n" +
                                   "}\nexception ExceptionFields {\n" +
                                   "    1: bool booleanValue;\n" +
                                   "    2: byte byteValue;\n" +
                                   "    3: i16 shortValue;\n" +
                                   "    4: i32 integerValue;\n" +
                                   "    5: i64 longValue;\n" +
                                   "    6: double doubleValue;\n" +
                                   "    7: string stringValue;\n" +
                                   "    8: binary binaryValue;\n" +
                                   "    9: Value enumValue;\n" +
                                   "    10: CompactFields compactValue;\n" +
                                   "}\n" +
                                   "typedef real double;\n" +
                                   "struct DefaultValues {\n" +
                                   "    1: bool booleanValue;\n" +
                                   "    2: byte byteValue;\n" +
                                   "    3: i16 shortValue;\n" +
                                   "    4: i32 integerValue;\n" +
                                   "    5: i64 longValue;\n" +
                                   "    6: real doubleValue;\n" +
                                   "    7: string stringValue;\n" +
                                   "    8: binary binaryValue;\n" +
                                   "    9: Value enumValue;\n" +
                                   "    10: CompactFields compactValue;\n" +
                                   "}\n" +
                                   "struct Containers {\n" +
                                   "    1: list<bool> booleanList;\n" +
                                   "    2: list<byte> byteList;\n" +
                                   "    3: list<i16> shortList;\n" +
                                   "    4: list<i32> integerList;\n" +
                                   "    5: list<i64> longList;\n" +
                                   "    6: list<double> doubleList;\n" +
                                   "    7: list<string> stringList;\n" +
                                   "    8: list<binary> binaryList;\n" +
                                   "    11: set<bool> booleanSet;\n" +
                                   "    12: set<byte> byteSet;\n" +
                                   "    13: set<i16> shortSet;\n" +
                                   "    14: set<i32> integerSet;\n" +
                                   "    15: set<i64> longSet;\n" +
                                   "    16: set<double> doubleSet;\n" +
                                   "    17: set<string> stringSet;\n" +
                                   "    18: set<binary> binarySet;\n" +
                                   "    21: map<bool,bool> booleanMap;\n" +
                                   "    22: map<byte,byte> byteMap;\n" +
                                   "    23: map<i16,i16> shortMap;\n" +
                                   "    24: map<i32,i32> integerMap;\n" +
                                   "    25: map<i64,i64> longMap;\n" +
                                   "    26: map<double,double> doubleMap;\n" +
                                   "    27: map<string,string> stringMap;\n" +
                                   "    28: map<binary,binary> binaryMap;\n" +
                                   "    31: list<Value> enumList;\n" +
                                   "    32: set<Value> enumSet;\n" +
                                   "    33: map<Value,Value> enumMap;\n" +
                                   "    41: list<DefaultFields> messageList;\n" +
                                   "    42: set<DefaultFields> messageSet;\n" +
                                   "    43: map<string,DefaultFields> messageMap;\n" +
                                   "    51: RequiredFields requiredFields;\n" +
                                   "    52: DefaultFields defaultFields;\n" +
                                   "    53: OptionalFields optionalFields;\n" +
                                   "    54: UnionFields unionFields;\n" +
                                   "    55: ExceptionFields exceptionFields;\n" +
                                   "    56: DefaultValues defaultValues;\n" +
                                   "}\n"),
                         arguments("common_fileserver.thrift", true,
                                   "struct FileServerConfig {\n" +
                                   "    3: i32 download_url;\n" +
                                   "}\n"),
                         arguments("model.thrift", false,
                                   "namespace java io.pvdnc.model\n" +
                                   "namespace js morimekta.providence\n" +
                                   "struct EnumValue {\n" +
                                   "    1: string documentation;\n" +
                                   "    2: string name;\n" +
                                   "    3: i32 id;\n" +
                                   "    4: map<string,string> annotations;\n" +
                                   "}\n" +
                                   "struct EnumType {\n" +
                                   "    1: string documentation;\n" +
                                   "    2: string name;\n" +
                                   "    3: list<EnumValue> values;\n" +
                                   "    4: map<string,string> annotations;\n" +
                                   "}\n" +
                                   "struct TypedefType {\n" +
                                   "    1: string documentation;\n" +
                                   "    2: string type;\n" +
                                   "    3: string name;\n" +
                                   "}\n" +
                                   "enum MessageVariant {\n" +
                                   "    STRUCT = 1;\n" +
                                   "    UNION = 2;\n" +
                                   "    EXCEPTION = 3;\n" +
                                   "}\n" +
                                   "enum FieldRequirement {\n" +
                                   "    DEFAULT = 0;\n" +
                                   "    OPTIONAL = 1;\n" +
                                   "    REQUIRED = 2;\n" +
                                   "}\n" +
                                   "struct FieldType {\n" +
                                   "    1: string documentation;\n" +
                                   "    2: i32 id;\n" +
                                   "    3: FieldRequirement requirement;\n" +
                                   "    4: string type;\n" +
                                   "    5: string name;\n" +
                                   "    6: string default_value;\n" +
                                   "    7: map<string,string> annotations;\n" +
                                   "    10: i32 start_line_no;\n" +
                                   "    11: i32 start_line_pos;\n" +
                                   "}\n" +
                                   "struct MessageType {\n" +
                                   "    1: string documentation;\n" +
                                   "    2: MessageVariant variant;\n" +
                                   "    3: string name;\n" +
                                   "    4: list<FieldType> fields;\n" +
                                   "    5: map<string,string> annotations;\n" +
                                   "}\n" +
                                   "struct FunctionType {\n" +
                                   "    1: string documentation;\n" +
                                   "    2: bool one_way;\n" +
                                   "    3: string return_type;\n" +
                                   "    4: string name;\n" +
                                   "    5: list<FieldType> params;\n" +
                                   "    6: list<FieldType> exceptions;\n" +
                                   "    7: map<string,string> annotations;\n" +
                                   "}\n" +
                                   "struct ServiceType {\n" +
                                   "    1: string documentation;\n" +
                                   "    2: string name;\n" +
                                   "    3: string extend;\n" +
                                   "    4: list<FunctionType> methods;\n" +
                                   "    5: map<string,string> annotations;\n" +
                                   "}\n" +
                                   "struct ConstType {\n" +
                                   "    1: string documentation;\n" +
                                   "    4: string type;\n" +
                                   "    5: string name;\n" +
                                   "    6: string value;\n" +
                                   "    7: map<string,string> annotations;\n" +
                                   "    10: i32 start_line_no;\n" +
                                   "    11: i32 start_line_pos;\n" +
                                   "}\n" +
                                   "union Declaration {\n" +
                                   "    1: EnumType decl_enum;\n" +
                                   "    2: TypedefType decl_typedef;\n" +
                                   "    3: MessageType decl_struct;\n" +
                                   "    4: ServiceType decl_service;\n" +
                                   "    5: ConstType decl_const;\n" +
                                   "}\n" +
                                   "struct ProgramType {\n" +
                                   "    1: string documentation;\n" +
                                   "    2: string program_name;\n" +
                                   "    3: list<string> includes;\n" +
                                   "    4: map<string,string> namespaces;\n" +
                                   "    5: list<Declaration> decl;\n" +
                                   "}\n" +
                                   "const set<string> kThriftKeywords = [\"include\",\"namespace\",];\n" +
                                   "const set<string> kReservedWords = [\"abstract\"\"alias\"\"and\"\"xor\"\"yield\"];\n"));
    }

    @MethodSource("testParse_Regression")
    @ParameterizedTest
    public void testParse_Regression(String resource, boolean strict, String out)
            throws IOException {
        ThriftProgramParser parser = new ThriftProgramParser(strict, strict, !strict);
        try (InputStream in = getClass().getResourceAsStream(
                "/io/pvdnc/idl/thrift/parser/regression/" + resource)) {
            assertThat(parser.parse(in, resource).displayString(), is(out));
        }
    }
}
