/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.thrift.model;

import io.pvdnc.idl.parser.IDLToken;
import io.pvdnc.idl.thrift.util.DeclarationUtil;
import net.morimekta.collect.UnmodifiableList;

import java.util.List;

import static java.util.Objects.requireNonNull;
import static net.morimekta.collect.UnmodifiableList.asList;
import static net.morimekta.collect.UnmodifiableList.listOf;

/**
 * <pre>{@code
 * function ::= 'oneway'? {type} {name} '(' {field}* ')' ('throws' '(' {field}* ')')? {annotations}?
 * }</pre>
 */
public class MethodDeclaration extends Declaration {
    private final IDLToken               oneway;
    private final List<IDLToken>         returnTypeTokens;
    private final List<FieldDeclaration> params;
    private final List<FieldDeclaration> throwing;

    public MethodDeclaration(String documentation,
                             IDLToken oneway,
                             List<IDLToken> returnTypeTokens,
                             IDLToken name,
                             List<FieldDeclaration> params,
                             List<FieldDeclaration> throwing,
                             List<AnnotationDeclaration> annotations) {
        super(documentation, name, annotations);
        requireNonNull(returnTypeTokens, "returnType == null");
        requireNonNull(params, "params == null");

        this.oneway = oneway;
        this.returnTypeTokens = asList(returnTypeTokens);
        this.params = UnmodifiableList.asList(params);
        this.throwing = throwing == null || throwing.isEmpty() ? listOf() : UnmodifiableList.asList(throwing);
    }

    public boolean isOneWay() {
        return getOneWayToken() != null;
    }

    public IDLToken getOneWayToken() {
        return oneway;
    }

    public String getReturnType() {
        return DeclarationUtil.toTypeString(getReturnTypeTokens());
    }

    public List<IDLToken> getReturnTypeTokens() {
        return returnTypeTokens;
    }

    public List<FieldDeclaration> getParams() {
        return params;
    }

    public List<FieldDeclaration> getThrowing() {
        return throwing;
    }

    // --- Displayable

    @Override
    public String displayString() {
        StringBuilder builder = new StringBuilder();
        if (isOneWay()) {
            builder.append("oneway ");
        }
        builder.append(getReturnType())
               .append(" ")
               .append(getNameToken())
               .append("(\n");
        for (FieldDeclaration param : getParams()) {
            builder.append("    ").append(param.displayString());
        }
        if (!getThrowing().isEmpty()) {
            builder.append(") throws (\n");
            for (FieldDeclaration throwing : getThrowing()) {
                builder.append("    ").append(throwing.displayString());
            }
        }
        builder.append(")\n");
        return builder.toString();
    }

    // --- Object

    @Override
    public String toString() {
        return "Method{" + getName() + "}";
    }
}
