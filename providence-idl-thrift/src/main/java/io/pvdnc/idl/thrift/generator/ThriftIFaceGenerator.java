/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.thrift.generator;

import io.pvdnc.core.PService;
import io.pvdnc.core.types.PServiceDescriptor;
import io.pvdnc.idl.generator.InnerClassGenerator;
import io.pvdnc.idl.generator.ServiceTypeFormatter;
import io.pvdnc.idl.generator.util.BlockCommentWriter;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;
import io.pvdnc.sio.thrift.ThriftProperties;

import java.util.Set;

import static net.morimekta.collect.UnmodifiableSet.setOf;
import static io.pvdnc.core.property.PDefaultProperties.DEPRECATED;
import static io.pvdnc.core.property.PDefaultProperties.DOCUMENTATION;

public class ThriftIFaceGenerator extends InnerClassGenerator<PServiceDescriptor, ServiceTypeFormatter> {
    public static final String INNER_CLASS = "IFace";

    public ThriftIFaceGenerator() {
        addFormatter(new ThriftIFaceFormatter());
    }

    @Override
    public String getInnerClassName() {
        return INNER_CLASS;
    }

    @Override
    public boolean isEnabled(PServiceDescriptor descriptor) {
        return descriptor.getProperty(ThriftProperties.THRIFT_SERVICE);
    }

    @Override
    public Set<String> getClassImports(JavaTypeHelper helper, PServiceDescriptor descriptor) {
        return setOf(PService.class.getName());
    }

    @Override
    public void appendClass(JavaTypeWriter writer, PServiceDescriptor descriptor) {
        JavaTypeHelper helper = writer.getHelper();

        writer.newline();
        try (BlockCommentWriter comment = new BlockCommentWriter(writer)) {
            comment.comment("A thrift service interface.");
            if (descriptor.hasProperty(DOCUMENTATION)) {
                comment.paragraph();
                comment.comment(descriptor.getProperty(DOCUMENTATION));
            }
            if (descriptor.hasProperty(DEPRECATED)) {
                comment.newline();
                comment.deprecated_(descriptor.getProperty(DEPRECATED));
            }
        }

        forEachFormatter(f -> f.appendClassAnnotations(writer, descriptor));

        writer.formatln("interface IFace",
                        PService.class.getSimpleName())
              .begin().begin("            ");
        boolean firstExt = true;
        for (PServiceDescriptor ext : descriptor.extending()) {
            if (firstExt) {
                writer.append(" extends ");
                firstExt = false;
            } else {
                writer.append(',')
                      .appendln("        ");
            }
            if (ext.getProperty(ThriftProperties.THRIFT_SERVICE)) {
                writer.format("%s.IFace", helper.javaTypeReference(ext));
            } else {
                // There is a theoretical possibility of extending non-thrift, though
                // that cannot be declared in a thrift file. Properties can be declared
                // ad-hoc too.
                writer.append(helper.javaTypeReference(ext));
            }
        }
        writer.end().append(" {");

        forEachFormatter(f -> f.appendFields(writer, descriptor));

        forEachFormatter(f -> f.appendConstructors(writer, descriptor));

        forEachFormatter(f -> f.appendMethods(writer, descriptor));

        forEachFormatter(f -> f.appendExtraProperties(writer, descriptor));

        forEachFormatter(f -> f.appendConstants(writer, descriptor));

        writer.end()
              .formatln("}");
    }
}
