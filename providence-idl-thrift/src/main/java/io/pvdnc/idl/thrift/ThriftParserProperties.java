/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.thrift;

import io.pvdnc.core.property.PPersistedProperty;
import io.pvdnc.core.property.PProperty;
import io.pvdnc.core.property.PPropertyTarget;

public class ThriftParserProperties {
    public static final PProperty<Boolean> REQUIRE_ENUM_VALUE;
    public static final PProperty<Boolean> REQUIRE_FIELD_ID;
    public static final PProperty<Boolean> ALLOW_LANGUAGE_RESERVED_NAMES;

    static {
        REQUIRE_ENUM_VALUE = new PPersistedProperty<>("requireEnumValue",
                                                      Boolean.class,
                                                      false,
                                                      PPropertyTarget.FILE);
        REQUIRE_FIELD_ID = new PPersistedProperty<>("requireFieldId",
                                                    Boolean.class,
                                                    false,
                                                    PPropertyTarget.FILE);
        ALLOW_LANGUAGE_RESERVED_NAMES = new PPersistedProperty<>("allowLanguageReservedNames",
                                                                 Boolean.class,
                                                                 false,
                                                                 PPropertyTarget.FILE);
    }

    private ThriftParserProperties() { }
}
