/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.thrift.model;

import io.pvdnc.idl.parser.IDLToken;
import net.morimekta.strings.Displayable;

import static java.util.Objects.requireNonNull;

public class NamespaceDeclaration implements Displayable {
    private final IDLToken namespaceToken;
    private final IDLToken language;
    private final IDLToken namespace;

    public NamespaceDeclaration(IDLToken namespaceToken,
                                IDLToken language,
                                IDLToken namespace) {
        this.namespaceToken = requireNonNull(namespaceToken, "namespaceToken == null");
        this.language = requireNonNull(language, "language == null");
        this.namespace = requireNonNull(namespace, "namespace == null");
    }

    public String getLanguage() {
        return language.toString();
    }

    public String getNamespace() {
        return namespace.toString();
    }

    // --- Displayable

    @Override
    public String displayString() {
        return "namespace " + getLanguage() + " " + getNamespace() + "\n";
    }
}
