/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.thrift.util;

import static java.util.Objects.requireNonNull;
import static net.morimekta.file.PathUtil.getFileBaseName;
import static net.morimekta.file.PathUtil.getFileSuffix;

public final class ThriftFileUtil {
    public static boolean isApacheThriftFileName(String file) {
        return isThriftFileSuffix(getFileSuffix(file));
    }

    public static String programNameFromFileName(String fileName) {
        requireNonNull(fileName, "fileName == null");
        return getFileBaseName(fileName).replaceAll("[^a-zA-Z0-9_]", "_");
    }

    private static boolean isThriftFileSuffix(String suffix) {
        requireNonNull(suffix);
        return suffix.equalsIgnoreCase("thrift") ||
               suffix.equalsIgnoreCase("thr");
    }
}
