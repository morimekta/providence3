/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.thrift.util;

import net.morimekta.collect.UnmodifiableSet;

import java.util.Set;

public final class ThriftConstants {
    private ThriftConstants() {}

    public static final Set<String> kThriftKeywords = UnmodifiableSet.<String>newBuilder()
            .add("include")
            .add("namespace")
            .add("bool")
            .add("byte")
            .add("i8")
            .add("i16")
            .add("i32")
            .add("i64")
            .add("double")
            .add("string")
            .add("binary")
            .add("list")
            .add("set")
            .add("map")
            .add("enum")
            .add("struct")
            .add("union")
            .add("exception")
            .add("const")
            .add("typedef")
            .add("service")
            .add("required")
            .add("optional")
            .add("extends")
            .add("throws")
            .add("oneway")
            .add("void")
            .build();

    public static final Set<String> kReservedWords = UnmodifiableSet.<String>newBuilder()
            .add("abstract")
            .add("alias")
            .add("and")
            .add("args")
            .add("as")
            .add("assert")
            .add("begin")
            .add("break")
            .add("case")
            .add("catch")
            .add("class")
            .add("clone")
            .add("continue")
            .add("declare")
            .add("def")
            .add("default")
            .add("del")
            .add("delete")
            .add("do")
            .add("dynamic")
            .add("elif")
            .add("else")
            .add("elseif")
            .add("elsif")
            .add("end")
            .add("enddeclare")
            .add("endfor")
            .add("endforeach")
            .add("endif")
            .add("endswitch")
            .add("endwhile")
            .add("ensure")
            .add("except")
            .add("exec")
            .add("finally")
            .add("float")
            .add("for")
            .add("foreach")
            .add("from")
            .add("function")
            .add("global")
            .add("goto")
            .add("if")
            .add("implements")
            .add("import")
            .add("in")
            .add("inline")
            .add("instanceof")
            .add("interface")
            .add("is")
            .add("int")
            .add("lambda")
            .add("long")
            .add("module")
            .add("native")
            .add("new")
            .add("next")
            .add("nil")
            .add("not")
            .add("or")
            .add("package")
            .add("pass")
            .add("public")
            .add("print")
            .add("private")
            .add("protected")
            .add("raise")
            .add("redo")
            .add("rescue")
            .add("retry")
            .add("register")
            .add("return")
            .add("self")
            .add("short")
            .add("sizeof")
            .add("static")
            .add("super")
            .add("switch")
            .add("synchronized")
            .add("then")
            .add("this")
            .add("throw")
            .add("transient")
            .add("try")
            .add("undef")
            .add("unless")
            .add("unsigned")
            .add("until")
            .add("use")
            .add("var")
            .add("virtual")
            .add("volatile")
            .add("when")
            .add("while")
            .add("with")
            .add("xor")
            .add("yield")
            .build();
}
