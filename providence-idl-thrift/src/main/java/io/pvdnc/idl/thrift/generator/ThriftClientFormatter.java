/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.thrift.generator;

import io.pvdnc.core.property.PDefaultProperties;
import io.pvdnc.core.rpc.PServiceCallHandler;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PMessageVariant;
import io.pvdnc.core.types.PServiceDescriptor;
import io.pvdnc.core.types.PServiceMethod;
import io.pvdnc.core.types.PType;
import io.pvdnc.idl.generator.GeneratorException;
import io.pvdnc.idl.generator.ServiceTypeFormatter;
import io.pvdnc.idl.generator.util.JavaAnnotation;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;
import io.pvdnc.sio.thrift.ThriftProperties;
import io.pvdnc.sio.thrift.rpc.ApplicationException;
import io.pvdnc.sio.thrift.rpc.ApplicationExceptionType;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static net.morimekta.collect.UnmodifiableSet.setOf;
import static io.pvdnc.idl.generator.InnerClassGenerator.DESCRIPTOR;
import static io.pvdnc.idl.generator.InnerClassGenerator.IMPL;
import static io.pvdnc.sio.thrift.ThriftProperties.THRIFT_SERVICE_METHOD_ONEWAY;
import static net.morimekta.strings.NamingUtil.Format.PASCAL;
import static net.morimekta.strings.NamingUtil.format;

public class ThriftClientFormatter implements ServiceTypeFormatter {
    @Override
    public Set<String> getImportClasses(JavaTypeHelper helper, PServiceDescriptor descriptor) {
        return setOf(Objects.class.getName(),
                     IOException.class.getName(),
                     PServiceCallHandler.class.getName(),
                     ApplicationException.class.getName(),
                     ApplicationExceptionType.class.getName());
    }

    @Override
    public void appendFields(JavaTypeWriter writer, PServiceDescriptor descriptor) {
        writer.formatln("private final %s handler;",
                        PServiceCallHandler.class.getSimpleName());
    }

    @Override
    public void appendConstructors(JavaTypeWriter writer, PServiceDescriptor descriptor) {
        writer.newline();
        writer.formatln("public %s(%s handler) {\n" +
                        "    %s.requireNonNull(handler, \"handler == null\");\n" +
                        "    this.handler = handler;\n" +
                        "}",
                        ThriftClientGenerator.INNER_CLASS, PServiceCallHandler.class.getSimpleName(),
                        Objects.class.getSimpleName());
    }

    @Override
    public void appendMethods(JavaTypeWriter writer, PServiceDescriptor descriptor) {
        JavaTypeHelper helper = writer.getHelper();

        for (PServiceMethod method : descriptor.allMethods()) {
            PMessageDescriptor<?> request  = method.getRequestType();
            PMessageDescriptor<?> response = method.getResponseType();

            if (method.getProperty(ThriftProperties.THRIFT_SERVICE_METHOD)) {
                PField success = response.findFieldById(0);
                String returnType = Optional.ofNullable(success)
                                            .map(f -> helper.getValueType(f.getResolvedDescriptor()))
                                            .orElse("void");

                writer.newline()
                      .appendln(JavaAnnotation.OVERRIDE)
                      .formatln("public %s %s(",
                                returnType,
                                method.getName())
                      .begin().begin(" ".repeat(returnType.length() + method.getName().length() + 5));
                boolean firstParam = true;
                for (PField param : request.allFields()) {
                    if (firstParam) {
                        firstParam = false;
                    } else {
                        writer.append(",").appendln();
                    }
                    writer.format("%s p%s",
                                  helper.getValueClass(param.getResolvedDescriptor()),
                                  format(param.getName(), PASCAL));
                }
                writer.append(")").end().begin()
                      .formatln("throws %s", IOException.class.getSimpleName())
                      .begin("       ");
                for (PField ex : response.allFields()) {
                    if (ex.getId() == 0) continue;
                    if (ex.getType() != PType.MESSAGE) {
                        throw new GeneratorException("Not a message field for thrift service exception: " +
                                                     ex.toString() + " for " + descriptor.getTypeName() + "." +
                                                     method.getName());
                    }
                    if (((PMessageDescriptor<?>) ex.getResolvedDescriptor()).getProperty(PDefaultProperties.MESSAGE_VARIANT) !=
                        PMessageVariant.EXCEPTION) {
                        throw new GeneratorException(
                                "Not an exception message field for thrift service exception: " +
                                ex.toString() + " for " + descriptor.getTypeName() + "." + method.getName());
                    }
                    writer.append(',')
                          .appendln(helper.javaTypeReference((PMessageDescriptor<?>) ex.getResolvedDescriptor(), IMPL));
                }

                writer.end().end()
                      .append(" {")
                      .formatln("%s rq = %s.newBuilder()",
                                helper.javaTypeReference(request),
                                helper.javaTypeReference(request)).begin("         ");
                for (PField param : request.allFields()) {
                    writer.format(".set%s(p%s)",
                                  format(param.getName(), PASCAL), format(param.getName(), PASCAL));
                }
                writer.appendln(".build();")
                      .end()
                      .newline();
                if (method.getProperty(THRIFT_SERVICE_METHOD_ONEWAY)) {
                    writer.formatln("handler.handleCall(%s.kMethod%s, rq);",
                                    helper.javaTypeReference(method.getOnServiceType(), DESCRIPTOR),
                                    format(method.getName(), PASCAL));
                } else {
                    writer.formatln("%s resp = handler.handleCall(%s.kMethod%s, rq);",
                                    helper.javaTypeReference(response),
                                    helper.javaTypeReference(method.getOnServiceType(), DESCRIPTOR),
                                    format(method.getName(), PASCAL));
                    writer.appendln("if (resp.unionFieldIsSet()) {\n" +
                                    "    switch (resp.unionFieldId()) {")
                          .begin("        ");

                    for (PField ex : response.allFields()) {
                        if (ex.getId() == 0) {
                            if (ex.getType() == PType.VOID) {
                                writer.formatln("case 0: return;");
                            } else {
                                writer.formatln("case 0: return resp.get%s();", format(ex.getName(), PASCAL));
                            }
                        } else {
                            writer.formatln("case %d: throw (%s) resp.get%s();",
                                            ex.getId(),
                                            helper.javaTypeReference((PMessageDescriptor<?>) ex.getResolvedDescriptor(), IMPL),
                                            format(ex.getName(), PASCAL));
                        }
                    }

                    writer.end()
                          .appendln("    }\n" +
                                    "}")
                          .newline().formatln("throw new %s.%s(\n" +
                                              "        \"Result field for %s.%s() not set\",\n" +
                                              "        %s.MISSING_RESULT);",
                                              ApplicationException.class.getSimpleName(), IMPL,
                                              descriptor.getTypeName(), method.getName(),
                                              ApplicationExceptionType.class.getSimpleName());
                }

                writer.end().appendln("}");
            } else {
                writer.newline()
                      .appendln(JavaAnnotation.OVERRIDE)
                      .formatln("public %s %s(%s request) throws %s {\n" +
                                "    return handler.handleCall(%s.kMethod%s, request);\n" +
                                "}",
                                helper.javaTypeReference(method.getResponseType()),
                                method.getName(),
                                helper.javaTypeReference(method.getRequestType()),
                                IOException.class.getSimpleName(),
                                helper.javaTypeReference(method.getOnServiceType(), DESCRIPTOR),
                                format(method.getName(), PASCAL))
                      .begin();
            }
        }
    }
}
