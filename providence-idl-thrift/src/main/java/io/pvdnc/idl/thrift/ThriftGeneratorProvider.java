/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.thrift;

import io.pvdnc.idl.generator.GeneratorOptions;
import io.pvdnc.idl.generator.GeneratorProvider;
import io.pvdnc.idl.generator.GeneratorRunner;
import io.pvdnc.idl.thrift.generator.ThriftClientGenerator;
import io.pvdnc.idl.thrift.generator.ThriftIFaceGenerator;
import io.pvdnc.idl.thrift.generator.ThriftMainFormatter;

import static io.pvdnc.idl.generator.InnerClassGenerator.MAIN;

public class ThriftGeneratorProvider implements GeneratorProvider {
    @Override
    public String getName() {
        return "thrift";
    }

    @Override
    public void configure(GeneratorRunner generator, GeneratorOptions options) {
        generator.getServiceGenerator().addClassFormatter(MAIN, new ThriftMainFormatter());
        generator.getServiceGenerator().addClass(new ThriftIFaceGenerator());
        generator.getServiceGenerator().addClass(new ThriftClientGenerator());
    }
}
