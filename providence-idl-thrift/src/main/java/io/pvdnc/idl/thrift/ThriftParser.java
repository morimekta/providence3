/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.thrift;

import io.pvdnc.idl.thrift.util.ThriftFileUtil;
import net.morimekta.file.FileUtil;
import net.morimekta.file.PathUtil;
import io.pvdnc.core.registry.PGlobalFileTypeRegistry;
import io.pvdnc.core.registry.PNamespaceFileTypeRegistry;
import io.pvdnc.idl.parser.IDLException;
import io.pvdnc.idl.parser.IDLParser;
import io.pvdnc.idl.parser.IDLWarningHandler;
import io.pvdnc.idl.thrift.model.IncludeDeclaration;
import io.pvdnc.idl.thrift.model.ProgramDeclaration;
import io.pvdnc.idl.thrift.parser.ThriftModelConverter;
import io.pvdnc.idl.thrift.parser.ThriftProgramParser;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import static net.morimekta.file.PathUtil.getFileName;

public class ThriftParser implements IDLParser {
    private final ThriftProgramParser programParser;
    private final ThriftModelConverter modelConverter;

    public ThriftParser(boolean requireFieldId,
                        boolean requireEnumValue,
                        boolean allowLanguageReservedNames) {
        this.programParser = new ThriftProgramParser(
                requireFieldId,
                requireEnumValue,
                allowLanguageReservedNames);
        this.modelConverter = new ThriftModelConverter();
    }

    @Override
    public PNamespaceFileTypeRegistry parseFile(Path filePath, PGlobalFileTypeRegistry registry, IDLWarningHandler warningHandler) throws IOException {
        IDLWarningHandler fileWarningHandler = ex -> {
            ex.setFile(getFileName(filePath));
            warningHandler.handleWarning(ex);
        };

        Path parent = filePath.getParent();
        if (parent == null) {
            throw new IllegalStateException("Invalid file path: " + filePath);
        }

        PNamespaceFileTypeRegistry programRegistry = registry.getRegistryForPath(filePath).orElse(null);
        if (programRegistry != null) {
            return programRegistry;
        }

        ProgramDeclaration  program;
        try (InputStream in = Files.newInputStream(filePath)) {
            program = programParser.parse(in, PathUtil.getFileName(filePath));
        }

        programRegistry = registry.computeRegistryForPath(filePath, ThriftFileUtil.programNameFromFileName(getFileName(filePath)));
        for (IncludeDeclaration include : program.getIncludes()) {
            Path includePath = FileUtil.readCanonicalPath(parent.resolve(include.getFilePath()));
            if (!Files.exists(includePath)) {
                throw new IDLException(include.getFilePathToken(), "No such file to be included")
                        .setFile(getFileName(filePath));
            }

            PNamespaceFileTypeRegistry reg = registry.getRegistryForPath(includePath).orElse(null);
            if (reg == null) {
                reg = parseFile(includePath, registry, warningHandler);
            }
            if (reg.isBuilding()) {
                throw new IDLException(include.getFilePathToken(), "Circular dependency between " +
                                                                   getFileName(filePath) + " and " + getFileName(reg.getPath()))
                        .setFile(getFileName(filePath));
            }
            programRegistry.putInclude(include.getProgramName(), reg);
        }
        programRegistry.complete();
        modelConverter.convert(program, programRegistry, fileWarningHandler);
        return programRegistry;
    }
}
