/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.thrift.model;

import io.pvdnc.idl.parser.IDLToken;
import net.morimekta.strings.io.IndentedPrintWriter;

import java.io.StringWriter;
import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * <pre>{@code
 * service ::= 'service' {name} ('extends' {extending})? '{' {function}* '}' {annotations}?
 * }</pre>
 */
public class ServiceDeclaration extends Declaration {
    private final IDLToken serviceToken;
    private final IDLToken extending;
    private final List<MethodDeclaration> methods;

    public ServiceDeclaration(String documentation,
                              IDLToken serviceToken,
                              IDLToken name,
                              IDLToken extending,
                              List<MethodDeclaration> methods,
                              List<AnnotationDeclaration> annotations) {
        super(documentation, name, annotations);
        this.serviceToken = requireNonNull(serviceToken, "serviceToken == null");
        this.extending = extending;
        this.methods = requireNonNull(methods, "methods == null");
    }

    public IDLToken getServiceToken() {
        return serviceToken;
    }

    public String getExtending() {
        if (extending != null) {
            return extending.toString();
        }
        return null;
    }

    public IDLToken getExtendingToken() {
        return extending;
    }

    public List<MethodDeclaration> getMethods() {
        return methods;
    }

    // --- Displayable

    @Override
    public String displayString() {
        StringWriter out = new StringWriter();
        IndentedPrintWriter builder = new IndentedPrintWriter(out);
        builder.append("service ")
               .append(getNameToken())
               .append(" ");
        if (getExtending() != null) {
            builder.append("extends ")
                   .append(getExtending())
                   .append(" ");
        }
        builder.append("{")
               .begin("    ");
        for (MethodDeclaration method : getMethods()) {
            builder.appendln(method.displayString());
        }
        builder.end()
               .appendln("}")
               .newline();
        return out.getBuffer().toString();
    }
}
