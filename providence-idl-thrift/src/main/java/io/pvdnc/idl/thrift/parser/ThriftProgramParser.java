/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.thrift.parser;

import io.pvdnc.idl.thrift.model.AnnotationDeclaration;
import io.pvdnc.idl.thrift.model.Declaration;
import io.pvdnc.idl.thrift.util.ThriftConstants;
import io.pvdnc.idl.thrift.util.ThriftFileUtil;
import net.morimekta.lexer.LexerException;
import io.pvdnc.idl.parser.IDLException;
import io.pvdnc.idl.parser.IDLLexer;
import io.pvdnc.idl.parser.IDLToken;
import io.pvdnc.idl.parser.IDLTokenType;
import io.pvdnc.idl.thrift.model.ConstDeclaration;
import io.pvdnc.idl.thrift.model.EnumDeclaration;
import io.pvdnc.idl.thrift.model.EnumValueDeclaration;
import io.pvdnc.idl.thrift.model.FieldDeclaration;
import io.pvdnc.idl.thrift.model.IncludeDeclaration;
import io.pvdnc.idl.thrift.model.MessageDeclaration;
import io.pvdnc.idl.thrift.model.MethodDeclaration;
import io.pvdnc.idl.thrift.model.NamespaceDeclaration;
import io.pvdnc.idl.thrift.model.ProgramDeclaration;
import io.pvdnc.idl.thrift.model.ServiceDeclaration;
import io.pvdnc.idl.thrift.model.TypedefDeclaration;
import net.morimekta.strings.NamingUtil;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

import static java.util.Objects.requireNonNull;
import static net.morimekta.file.PathUtil.getFileName;
import static io.pvdnc.idl.parser.IDLToken.kEntrySep;
import static io.pvdnc.idl.parser.IDLToken.kKeyValueSep;
import static io.pvdnc.idl.parser.IDLToken.kLineSep;
import static io.pvdnc.idl.parser.IDLToken.kListEnd;
import static io.pvdnc.idl.parser.IDLToken.kListStart;
import static io.pvdnc.idl.parser.IDLToken.kMessageEnd;
import static io.pvdnc.idl.parser.IDLToken.kMessageStart;
import static io.pvdnc.idl.parser.IDLToken.kParamsEnd;
import static io.pvdnc.idl.parser.IDLToken.kParamsStart;
import static io.pvdnc.idl.thrift.parser.ThriftTokenizer.kConst;
import static io.pvdnc.idl.thrift.parser.ThriftTokenizer.kEnum;
import static io.pvdnc.idl.thrift.parser.ThriftTokenizer.kException;
import static io.pvdnc.idl.thrift.parser.ThriftTokenizer.kExtends;
import static io.pvdnc.idl.thrift.parser.ThriftTokenizer.kInclude;
import static io.pvdnc.idl.thrift.parser.ThriftTokenizer.kNamespace;
import static io.pvdnc.idl.thrift.parser.ThriftTokenizer.kOneway;
import static io.pvdnc.idl.thrift.parser.ThriftTokenizer.kOptional;
import static io.pvdnc.idl.thrift.parser.ThriftTokenizer.kRequired;
import static io.pvdnc.idl.thrift.parser.ThriftTokenizer.kService;
import static io.pvdnc.idl.thrift.parser.ThriftTokenizer.kStruct;
import static io.pvdnc.idl.thrift.parser.ThriftTokenizer.kThrows;
import static io.pvdnc.idl.thrift.parser.ThriftTokenizer.kTypedef;
import static io.pvdnc.idl.thrift.parser.ThriftTokenizer.kUnion;
import static net.morimekta.strings.EscapeUtil.javaEscape;
import static net.morimekta.strings.NamingUtil.Format.CAMEL;

public class ThriftProgramParser {
    private final boolean requireFieldId;
    private final boolean requireEnumValue;
    private final boolean allowLanguageReservedNames;

    public ThriftProgramParser(boolean requireFieldId,
                               boolean requireEnumValue,
                               boolean allowLanguageReservedNames) {
        this.requireFieldId = requireFieldId;
        this.requireEnumValue = requireEnumValue;
        this.allowLanguageReservedNames = allowLanguageReservedNames;
    }

    public ProgramDeclaration parse(InputStream in, String fileName) throws IOException {
        requireNonNull(in, "in == null");
        requireNonNull(fileName, "fileName == null");
        try {
            return parseInternal(in, fileName);
        } catch (IDLException e) {
            if (e.getFile() == null) {
                e.setFile(fileName);
            }
            throw e;
        } catch (LexerException e) {
            throw new IDLException(e, e.getMessage())
                    .setFile(fileName);
        }
    }

    private ProgramDeclaration parseInternal(InputStream in, String fileName) throws IOException {
        String                     documentation = null;
        List<IncludeDeclaration>   includes     = new ArrayList<>();
        List<NamespaceDeclaration> namespaces   = new ArrayList<>();
        List<Declaration>          declarations = new ArrayList<>();

        String programName = ThriftFileUtil.programNameFromFileName(fileName);
        Set<String> includedPrograms = new HashSet<>();
        IDLLexer    lexer            = new IDLLexer(new ThriftTokenizer(in));

        boolean has_header = false;
        boolean hasDeclaration = false;

        String   doc_string = null;
        IDLToken token;
        while ((token = lexer.next()) != null) {
            if (token.type() == IDLTokenType.DOCUMENTATION) {
                if (doc_string != null && !has_header) {
                    documentation = doc_string;
                }
                doc_string = token.parseDocumentation();
                continue;
            }

            String keyword = token.toString();
            if (!(ThriftConstants.kThriftKeywords.contains(keyword))) {
                throw lexer.failure(token, "Unexpected token '%s'", token);
            }
            switch (keyword) {
                case kNamespace:
                    if (hasDeclaration) {
                        throw lexer.failure(token, "Unexpected token 'namespace', expected type declaration");
                    }
                    if (doc_string != null && !has_header) {
                        documentation = doc_string;
                    }
                    doc_string = null;
                    has_header = true;
                    parseNamespace(lexer, token, namespaces);
                    break;
                case kInclude:
                    if (hasDeclaration) {
                        throw lexer.failure(token, "Unexpected token 'include', expected type declaration");
                    }
                    if (doc_string != null && !has_header) {
                        documentation = doc_string;
                    }
                    doc_string = null;
                    has_header = true;
                    includes.add(parseInclude(lexer, token, includedPrograms));
                    break;
                case kTypedef:
                    has_header = true;
                    hasDeclaration = true;
                    declarations.add(parseTypedef(includedPrograms, lexer, token, doc_string));
                    doc_string = null;
                    break;
                case kEnum:
                    has_header = true;
                    hasDeclaration = true;
                    declarations.add(parseEnum(lexer, doc_string));
                    doc_string = null;
                    break;
                case kStruct:
                case kUnion:
                case kException:
                    has_header = true;
                    hasDeclaration = true;
                    declarations.add(parseMessage(includedPrograms, lexer, token, doc_string));
                    doc_string = null;
                    break;
                case kService:
                    has_header = true;
                    hasDeclaration = true;
                    declarations.add(parseService(includedPrograms, lexer, token, doc_string));
                    doc_string = null;
                    break;
                case kConst:
                    has_header = true;
                    hasDeclaration = true;
                    declarations.add(parseConst(includedPrograms, lexer, token, doc_string));
                    doc_string = null;
                    break;
                default:
                    throw lexer.failure(token, "Unexpected token '%s'", javaEscape(token.toString()));
            }
        }

        return new ProgramDeclaration(
                documentation,
                programName,
                includes,
                namespaces,
                declarations);
    }

    /**
     * <pre>{@code
     * service ::= 'service' {name} ('extends' {extending})? '{' {method}* '}' {annotations}?
     * }</pre>
     *
     *
     * @param includedPrograms
     * @param tokenizer The tokenizer.
     * @param serviceToken The 'service' token.
     * @param documentation The service documentation string.
     * @return The service declaration.
     * @throws IOException If unable to parse the service.
     */
    private ServiceDeclaration parseService(
            Set<String> includedPrograms, IDLLexer tokenizer,
            IDLToken serviceToken,
            String documentation) throws IOException {
        IDLToken name = tokenizer.expect("service name", IDLToken::isIdentifier);
        if (forbiddenNameIdentifier(name.toString())) {
            throw tokenizer.failure(name, "Service with reserved name: " + name);
        }

        IDLToken extending = null;
        IDLToken next      = tokenizer.expect("service start or extends");
        if (next.toString().equals(kExtends)) {
            extending = tokenizer.expect("extending type", IDLToken::isReferenceIdentifier);
            tokenizer.expectSymbol("service start", kMessageStart);
        }

        String                  doc_string = null;
        List<MethodDeclaration> functions  = new ArrayList<>();
        next = tokenizer.expect("function or service end");
        while (!next.isSymbol(kMessageEnd)) {
            if (next.type() == IDLTokenType.DOCUMENTATION) {
                doc_string = next.parseDocumentation();
                next = tokenizer.expect("function or service end");
                continue;
            }

            functions.add(parseMethod(includedPrograms, tokenizer, next, doc_string));
            next = tokenizer.expect("function or service end");
        }

        List<AnnotationDeclaration> annotations = null;
        if (tokenizer.hasNext() &&
            tokenizer.peek("next").isSymbol(kParamsStart)) {
            tokenizer.next();
            annotations = parseAnnotations(tokenizer);
        }

        if (tokenizer.hasNext() &&
            tokenizer.peek("next").isSymbol(kLineSep)) {
            tokenizer.next();
        }

        return new ServiceDeclaration(documentation, serviceToken, name, extending, functions, annotations);
    }

    /**
     * <pre>{@code
     * method ::= 'oneway'? {type} {name} '(' {param}* ')' ('throws' '(' {throwing}* ')')? {annotations}?
     * }</pre>
     *
     * @param lexer The tokenizer.
     * @param next The first token of the method declaration.
     * @param documentation The method documentation.
     * @return The method declaration.
     * @throws IOException If unable to parse the method.
     */
    private MethodDeclaration parseMethod(Set<String> includedPrograms,
                                          IDLLexer lexer,
                                          IDLToken next,
                                          String documentation)
            throws IOException {
        IDLToken oneway = null;
        if (next.toString().equals(kOneway)) {
            oneway = next;
            next = lexer.expect("method return type");
        }
        List<IDLToken>      returnType = parseType(includedPrograms, lexer, next);
        IDLToken            name       = lexer.expect("method name", IDLToken::isIdentifier);
        List<FieldDeclaration> params     = new ArrayList<>();
        lexer.expectSymbol("params start", kParamsStart);
        next = lexer.expect("param or params end");
        String doc_string = null;
        AtomicInteger nextParamId = new AtomicInteger(-1);
        while (!next.isSymbol(kParamsEnd)) {
            if (next.type() == IDLTokenType.DOCUMENTATION) {
                doc_string = next.parseDocumentation();
                next = lexer.expect("param or params end");
                continue;
            }

            params.add(parseField(includedPrograms, lexer, next, nextParamId, doc_string));
            doc_string = null;
            next = lexer.expect("param or params end");
        }

        List<FieldDeclaration> throwing = new ArrayList<>();
        if (lexer.peek("throws or service end").toString().equals(kThrows)) {
            lexer.next();
            lexer.expectSymbol("params start", kParamsStart);
            next = lexer.expect("param or params end");
            nextParamId.set(-1);
            doc_string = null;
            while (!next.isSymbol(kParamsEnd)) {
                if (next.type() == IDLTokenType.DOCUMENTATION) {
                    doc_string = next.parseDocumentation();
                    next = lexer.expect("param or params end");
                    continue;
                }

                throwing.add(parseField(includedPrograms, lexer, next, nextParamId, doc_string));
                doc_string = null;
                next = lexer.expect("param or params end");
            }
        }

        List<AnnotationDeclaration> annotations = null;
        if (lexer.peek("annotation or service end").isSymbol(kParamsStart)) {
            lexer.next();
            annotations = parseAnnotations(lexer);
        }

        if (lexer.peek("function or service end").isSymbol(kLineSep)) {
            lexer.next();
        }

        return new MethodDeclaration(documentation, oneway, returnType, name, params,  throwing, annotations);
    }

    /**
     * <pre>{@code
     * namespace ::= 'namespace' {language} {namespace}
     * }</pre>
     *
     * @param lexer The tokenizer.
     * @param namespaceToken The namespace token.
     * @param namespaces List of already declared namespaces.
     * @throws IOException If unable to parse namespace.
     */
    private void parseNamespace(IDLLexer lexer,
                                IDLToken namespaceToken,
                                List<NamespaceDeclaration> namespaces)
            throws IOException {
        IDLToken language = lexer.expect("namespace language",
                                                IDLToken::isReferenceIdentifier);
        if (namespaces.stream().anyMatch(ns -> ns.getLanguage().equals(language.toString()))) {
            throw lexer.failure(language,
                                    "Namespace for %s already defined.",
                                    language.toString());
        }

        IDLToken namespace = lexer.expect(
                "namespace",
                t -> VALID_NAMESPACE.matcher(t).matches());

        if (lexer.hasNext() &&
            lexer.peek("next").isSymbol(kLineSep)) {
            lexer.next();
        }

        namespaces.add(new NamespaceDeclaration(namespaceToken, language, namespace));
    }

    /**
     * <pre>{@code
     * include ::= 'include' {file}
     * }</pre>
     *
     * @param lexer The tokenizer.
     * @param includeToken The include token.
     * @param includedPrograms Set of included program names.
     * @return The include declaration.
     * @throws IOException If unable to parse the declaration.
     */
    private IncludeDeclaration parseInclude(IDLLexer lexer,
                                            IDLToken includeToken,
                                            Set<String> includedPrograms) throws IOException {
        IDLToken include  = lexer.expect("include file", IDLTokenType.STRING);
        String      fileName = getFileName(Path.of(include.decodeString(true)));
        if (!ThriftFileUtil.isApacheThriftFileName(fileName)) {
            throw lexer.failure(include, "Include not valid for thrift files " + fileName);
        }

        String includedProgram = ThriftFileUtil.programNameFromFileName(fileName);
        if (includedPrograms.contains(includedProgram)) {
            throw lexer.failure(include, "thrift program '" + includedProgram + "' already included");
        }
        includedPrograms.add(includedProgram);

        if (lexer.hasNext() &&
            lexer.peek("next").isSymbol(kLineSep)) {
            lexer.next();
        }

        return new IncludeDeclaration(includeToken, include);
    }

    /**
     * <pre>{@code
     * typedef ::= 'typedef' {name} {type}
     * }</pre>
     *
     *
     * @param includedPrograms
     * @param tokenizer The tokenizer.
     * @param documentation The documentation.
     * @return The typedef declaration.
     * @throws IOException If unable to parse typedef.
     */
    private TypedefDeclaration parseTypedef(Set<String> includedPrograms, IDLLexer tokenizer,
                                            IDLToken typedefToken,
                                            String documentation) throws IOException {
        List<IDLToken> type = parseType(includedPrograms, tokenizer, tokenizer.expect("typename"));
        IDLToken       name = tokenizer.expect("typedef identifier", IDLToken::isIdentifier);
        if (forbiddenNameIdentifier(name.toString())) {
            throw tokenizer.failure(name, "Typedef with reserved name: " + name);
        }

        if (tokenizer.hasNext() &&
            tokenizer.peek("next").isSymbol(kLineSep)) {
            tokenizer.next();
        }

        return new TypedefDeclaration(documentation, typedefToken, name, type);
    }

    /**
     * <pre>{@code
     * enum ::= 'enum' {name} '{' {enum_value}* '}'
     * }</pre>
     *
     * @param lexer Tokenizer to parse the enum.
     * @param documentation The enum documentation.
     * @return The enum declaration.
     * @throws IOException If unable to parse the enum.
     */
    private EnumDeclaration parseEnum(IDLLexer lexer,
                                      String documentation) throws IOException {
        IDLToken name = lexer.expect("enum name", IDLToken::isIdentifier);
        if (forbiddenNameIdentifier(name.toString())) {
            throw lexer.failure(name, "Enum with reserved name: %s", name);
        }
        lexer.expectSymbol("enum start", kMessageStart);

        List<EnumValueDeclaration> values = new ArrayList<>();

        int         nextAutoId = 1;
        String      doc_string = null;
        IDLToken next       = lexer.expect("enum value or end");
        while (!next.isSymbol(kMessageEnd)) {
            if (next.type() == IDLTokenType.DOCUMENTATION) {
                doc_string = next.parseDocumentation();
                next = lexer.expect("enum value or end");
                continue;
            }

            IDLToken valueName = next;
            IDLToken valueId   = null;
            next = lexer.expect("enum value sep or end");
            if (next.isSymbol(IDLToken.kFieldValueSep)) {
                valueId = lexer.expect("enum value id", IDLToken::isNonNegativeID);
                next = lexer.expect("enum value or end");
            }
            if (requireEnumValue && valueId == null) {
                throw lexer.failure(valueName, "Explicit enum value required, but missing");
            }

            List<AnnotationDeclaration> annotations = null;
            if (next.isSymbol(kParamsStart)) {
                annotations = parseAnnotations(lexer);
                next = lexer.expect("enum value or end");
            }

            if (next.isSymbol(kEntrySep) ||
                next.isSymbol(kLineSep)) {
                next = lexer.expect("enum value or end");
            }
            int id;
            if (valueId != null) {
                id = (int) valueId.parseInteger();
                nextAutoId = Math.max(id + 1, nextAutoId);
            } else {
                id = nextAutoId;
                ++nextAutoId;
            }

            values.add(new EnumValueDeclaration(doc_string, valueName, valueId, id, annotations));
            doc_string = null;
        }

        List<AnnotationDeclaration> annotations = null;
        if (lexer.hasNext() &&
            lexer.peek("next").isSymbol(kParamsStart)) {
            lexer.next();
            annotations = parseAnnotations(lexer);
        }

        if (lexer.hasNext() &&
            lexer.peek("next").isSymbol(kLineSep)) {
            lexer.next();
        }

        return new EnumDeclaration(documentation, name, values, annotations);
    }

    /**
     * <pre>{@code
     * message ::= {variant} {name} (('of' | 'implements') {implementing})? '{' {field}* '}'
     * }</pre>
     *
     *
     * @param includedPrograms
     * @param lexer Tokenizer.
     * @param variant The message variant.
     * @param documentation The message documentation.
     * @return The parsed message declaration.
     * @throws IOException If unable to parse the message.
     */
    private MessageDeclaration parseMessage(Set<String> includedPrograms, IDLLexer lexer,
                                            IDLToken variant,
                                            String documentation)
            throws IOException {
        List<FieldDeclaration> fields       = new ArrayList<>();

        IDLToken name = lexer.expect("message name identifier", IDLToken::isIdentifier);
        if (forbiddenNameIdentifier(name.toString())) {
            throw lexer.failure(name, "Message with reserved name: %s", name);
        }

        Map<String, IDLToken> normalizedNames = new HashMap<>();
        Map<String, IDLToken> exactNames = new HashMap<>();
        Map<Integer, IDLToken> fieldIds = new HashMap<>();

        lexer.expectSymbol("message start", kMessageStart);

        AtomicInteger nextAutoId = new AtomicInteger(-1);
        String doc_string = null;
        IDLToken next = lexer.expect("field def or message end");
        while (!next.isSymbol(kMessageEnd)) {
            if (next.type() == IDLTokenType.DOCUMENTATION) {
                doc_string = next.parseDocumentation();
                next = lexer.expect("field def or message end");
                continue;
            }

            FieldDeclaration fieldDeclaration = parseField(includedPrograms, lexer, next, nextAutoId, doc_string);

            if (fieldIds.containsKey(fieldDeclaration.getId())) {
                IDLToken other = fieldIds.get(fieldDeclaration.getId());
                throw new IDLException(fieldDeclaration.getIdToken(), "Field with id " + fieldDeclaration.getId() + " already declared on line %d.", other.lineNo());
            }
            fieldIds.put(fieldDeclaration.getId(), fieldDeclaration.getIdToken());

            if (exactNames.containsKey(fieldDeclaration.getName())) {
                IDLToken other = exactNames.get(fieldDeclaration.getName());
                throw new IDLException(fieldDeclaration.getNameToken(), "Field with name " + fieldDeclaration.getName() + " already declared on line %d.", other.lineNo());
            }
            exactNames.put(fieldDeclaration.getName(), fieldDeclaration.getNameToken());

            String normalizedName = NamingUtil.format(fieldDeclaration.getName(), CAMEL);
            if (normalizedNames.containsKey(normalizedName)) {
                IDLToken other = normalizedNames.get(normalizedName);
                throw new IDLException(fieldDeclaration.getNameToken(), "Field with conflicting name " + other.toString() + " already declared on line %d.", other.lineNo());
            }
            normalizedNames.put(normalizedName, fieldDeclaration.getNameToken());

            fields.add(fieldDeclaration);

            doc_string = null;
            next = lexer.expect("field def or message end");
        }

        List<AnnotationDeclaration> annotations = null;
        if (lexer.hasNext() &&
            lexer.peek("next").isSymbol(kParamsStart)) {
            lexer.next();
            annotations = parseAnnotations(lexer);
        }
        if (lexer.hasNext() &&
            lexer.peek("next").isSymbol(kLineSep)) {
            lexer.next();
        }

        return new MessageDeclaration(documentation, variant, name, fields, annotations);
    }

    /**
     * <pre>{@code
     * field ::= ({id} ':')? {requirement}? {type} {name} ('=' {defaultValue})? {annotations}?
     * }</pre>
     *
     *
     * @param includedPrograms
     * @param lexer The tokenizer.
     * @param documentation The field documentation.
     * @return The field declaration.
     * @throws IOException If unable to read the declaration.
     */
    private FieldDeclaration parseField(Set<String> includedPrograms, IDLLexer lexer,
                                        IDLToken next,
                                        AtomicInteger nextAutoId,
                                        String documentation) throws IOException {
        int fieldId;
        IDLToken idToken = null;
        if (next.isPositiveID()) {
            idToken = next;
            fieldId = (int) idToken.parseInteger();
            if (fieldId < 1) {
                throw lexer.failure(next, "Non-positive ");
            }
            lexer.expectSymbol("field id sep", kKeyValueSep);
            next = lexer.expect("field type");
        } else if (requireFieldId) {
            throw lexer.failure(next, "Field id required, but missing");
        } else {
            fieldId = nextAutoId.getAndDecrement();
        }

        IDLToken requirement = null;
        if (next.toString().equals(kOptional) ||
            next.toString().equals(kRequired)) {
            requirement = next;
            next = lexer.expect("field type");
        }

        List<IDLToken>           type         = parseType(includedPrograms, lexer, next);
        IDLToken                 name         = lexer.expect("field name", IDLToken::isIdentifier);
        List<IDLToken>           defaultValue = null;
        List<AnnotationDeclaration> annotations  = null;

        if (forbiddenNameIdentifier(name.toString())) {
            throw lexer.failure(name, "Field with reserved name: " + name.toString());
        }

        if (lexer.peek("field value").isSymbol(IDLToken.kFieldValueSep)) {
            lexer.next();
            defaultValue = parseValue(lexer);
        }
        if (lexer.peek("field annotation").isSymbol(kParamsStart)) {
            lexer.next();
            annotations = parseAnnotations(lexer);
        }

        if (lexer.peek("field sep").isSymbol(kEntrySep) ||
            lexer.peek("field sep").isSymbol(kLineSep)) {
            lexer.next();
        }

        return new FieldDeclaration(documentation, idToken, fieldId, requirement, name, type, defaultValue, annotations);
    }

    /**
     * <pre>{@code
     * const type kName = value;
     * }</pre>
     *
     *
     * @param includedPrograms
     * @param lexer The tokenizer.
     * @return The parsed const declaration.
     * @throws IOException If parsing failed.
     */
    private ConstDeclaration parseConst(Set<String> includedPrograms, IDLLexer lexer,
                                        IDLToken constToken,
                                        String documentation) throws IOException {
        requireNonNull(lexer);
        requireNonNull(constToken);

        List<IDLToken> type = parseType(includedPrograms, lexer, lexer.expect("const type"));
        IDLToken       name = lexer.expect("const name", IDLToken::isIdentifier);
        if (forbiddenNameIdentifier(name.toString())) {
            throw lexer.failure(name, "Const with reserved name: " + name.toString());
        }

        lexer.expect("const value sep", t -> t.isSymbol(IDLToken.kFieldValueSep));
        List<IDLToken> value = parseValue(lexer);

        List<AnnotationDeclaration> annotations = null;
        if (lexer.hasNext() &&
            lexer.peek("next").isSymbol(kLineSep)) {
            lexer.next();
        }

        return new ConstDeclaration(documentation, constToken, name, type, value, annotations);
    }

    /**
     * Parse annotations, not including the initial '('.
     *
     * <pre>{@code
     * annotation  ::= key ('=' value)?
     * annotations ::= '(' annotation (',' annotation)* ')'
     * }</pre>
     *
     * @param tokenizer The tokenizer to get tokens from.
     * @return The list of annotations.
     */
    private List<AnnotationDeclaration> parseAnnotations(IDLLexer tokenizer)
            throws IOException {
        requireNonNull(tokenizer, "tokenizer == null");

        List<AnnotationDeclaration> annotations = new ArrayList<>();
        IDLToken                 next        = tokenizer.expect("annotation key");
        while (!next.isSymbol(kParamsEnd)) {
            if (!next.isReferenceIdentifier()) {
                throw tokenizer.failure(next, "annotation key must be identifier-like");
            }
            IDLToken key   = next;
            IDLToken value = null;
            next = tokenizer.expect("annotation end or sep");
            if (next.isSymbol(IDLToken.kFieldValueSep)) {
                value = tokenizer.expect("annotation value", IDLTokenType.STRING);
                next = tokenizer.expect("annotation end or sep");
            }
            annotations.add(new AnnotationDeclaration(key, value));
            if (next.isSymbol(kEntrySep)) {
                next = tokenizer.expect("annotation key", t -> !t.isSymbol(kParamsEnd));
            }
        }
        return annotations;
    }

    /**
     * @param lexer Tokenizer to parse value.
     * @return The list of tokens part of the value.
     * @throws IOException If unable to parse the type.
     */
    private List<IDLToken> parseValue(IDLLexer lexer) throws IOException {
        return parseValue(lexer, lexer.expect("value"));
    }

    /**
     * @param lexer Tokenizer to parse value.
     * @param token First token of the value.
     * @return The list of tokens part of the value.
     * @throws IOException If unable to parse the type.
     */
    private List<IDLToken> parseValue(IDLLexer lexer,
                                         IDLToken token) throws IOException {
        // Ignore these comments.
        while (token.type() == IDLTokenType.DOCUMENTATION) {
            token = lexer.expect("value");
        }

        List<IDLToken> tokens = new ArrayList<>();
        tokens.add(token);

        if (token.isSymbol(kListStart)) {
            IDLToken next = lexer.expect("list value");
            while (!next.isSymbol(kListEnd)) {
                tokens.addAll(parseValue(lexer, next));
                next = lexer.expect("list value, sep or end");
                if (next.isSymbol(kEntrySep) || next.isSymbol(kLineSep)) {
                    tokens.add(next);
                    next = lexer.expect("list value or end");
                }
            }
            tokens.add(next);
        } else if (token.isSymbol(kMessageStart)) {
            IDLToken next = lexer.expect("map key");
            while (!next.isSymbol(kMessageEnd)) {
                tokens.addAll(parseValue(lexer, next));
                tokens.add(lexer.expectSymbol("key value sep", kKeyValueSep));
                tokens.addAll(parseValue(lexer, lexer.expect("map value")));

                next = lexer.expect("map sep or end");
                if (next.isSymbol(kEntrySep) || next.isSymbol(kLineSep)) {
                    tokens.add(next);
                    next = lexer.expect("map sep or end");
                }
            }
            tokens.add(next);
        } else if (!token.isReferenceIdentifier() &&
                   token.type() != IDLTokenType.NUMBER &&
                   token.type() != IDLTokenType.STRING) {
            throw lexer.failure(token, "not a value type: '%s'", token.toString());
        }
        return tokens;
    }

    /**
     * @param lexer The tokenizer.
     * @param token The first token of the type.
     * @return List of tokens part of the type string.
     * @throws IOException If unable to parse the type.
     */
    private List<IDLToken> parseType(Set<String> includedPrograms,
                                        IDLLexer lexer,
                                        IDLToken token) throws IOException {
        if (!token.isQualifiedIdentifier() &&
            !token.isIdentifier()) {
            throw lexer.failure(token, "Expected type identifier but found " + token);
        }
        if (token.isQualifiedIdentifier()) {
            String[] parts = token.toString().split("[.]");
            if (!includedPrograms.contains(parts[0])) {
                throw new IDLException(token, "No such include " + parts[0] + " for type " + token);
            }
        }

        List<IDLToken> tokens = new ArrayList<>();
        tokens.add(token);

        String type = token.toString();
        switch (type) {
            case "list":
            case "set": {
                tokens.add(lexer.expect(type + " generic start", t -> t.isSymbol(IDLToken.kGenericStart)));
                tokens.addAll(parseType(includedPrograms, lexer, lexer.expect(type + " item type")));
                tokens.add(lexer.expect(type + " generic end", t -> t.isSymbol(IDLToken.kGenericEnd)));
                break;
            }
            case "map": {
                tokens.add(lexer.expect(type + " generic start", t -> t.isSymbol(IDLToken.kGenericStart)));
                tokens.addAll(parseType(includedPrograms, lexer, lexer.expect(type + " key type")));
                tokens.add(lexer.expect(type + " generic sep", t -> t.isSymbol(kEntrySep)));
                tokens.addAll(parseType(includedPrograms, lexer, lexer.expect(type + " item type")));
                tokens.add(lexer.expect(type + " generic end", t -> t.isSymbol(IDLToken.kGenericEnd)));
                break;
            }
            default: {
                break;
            }
        }
        return tokens;
    }

    private boolean forbiddenNameIdentifier(String name) {
        if (ThriftConstants.kThriftKeywords.contains(name)) {
            return true;
        } else return !allowLanguageReservedNames && ThriftConstants.kReservedWords.contains(name);
    }

    private static final Pattern VALID_NAMESPACE  = Pattern.compile(
            "([_a-zA-Z][_a-zA-Z0-9]*[.])*[_a-zA-Z][_a-zA-Z0-9]*");
}
