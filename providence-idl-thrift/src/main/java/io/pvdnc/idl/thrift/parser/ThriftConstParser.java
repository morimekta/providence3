/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.thrift.parser;

import io.pvdnc.core.PMessageBuilder;
import io.pvdnc.idl.thrift.util.DeclarationUtil;
import net.morimekta.collect.ListBuilder;
import net.morimekta.collect.MapBuilder;
import net.morimekta.collect.SetBuilder;
import net.morimekta.collect.util.Binary;
import net.morimekta.lexer.TokenizerRepeater;
import net.morimekta.lexer.UncheckedLexerException;
import io.pvdnc.core.PEnum;
import io.pvdnc.core.registry.PNamespaceFileTypeRegistry;
import io.pvdnc.core.registry.PTypeReference;
import io.pvdnc.core.types.PDeclarationType;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PListDescriptor;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.types.PType;
import io.pvdnc.idl.parser.IDLException;
import io.pvdnc.idl.parser.IDLLexer;
import io.pvdnc.idl.parser.IDLToken;
import io.pvdnc.idl.parser.IDLTokenType;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

import static io.pvdnc.core.registry.PTypeReference.ref;
import static io.pvdnc.core.utils.ConversionUtil.coerceStrict;

public class ThriftConstParser implements Supplier<Object> {
    private final AtomicReference<Object>         value;
    private final Supplier<? extends PDescriptor> provider;
    private final List<IDLToken>               valueTokens;
    private final PNamespaceFileTypeRegistry registry;

    public ThriftConstParser(Supplier<? extends PDescriptor> provider,
                             PNamespaceFileTypeRegistry registry,
                             List<IDLToken> valueTokens) {
        this.provider = provider;
        this.valueTokens = valueTokens;
        this.registry = registry;
        this.value = new AtomicReference<>();
    }

    @Override
    public Object get() {
        return value.updateAndGet(value -> {
            if (value == null) {
                value = parse();
            }
            return value;
        });
    }

    private Object parse() {
        PDescriptor                               descriptor = provider.get();
        TokenizerRepeater<IDLTokenType, IDLToken> repeater   = new TokenizerRepeater<>(valueTokens);
        IDLLexer                               lexer      = new IDLLexer(repeater);
        try {
            Object value = parseValue(descriptor, lexer);
            if (lexer.hasNext()) {
                throw new IDLException(lexer.next(), "Garbage token at end of const value");
            }
            return value;
        } catch (IDLException e) {
            throw new UncheckedLexerException(e);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private Object parseValue(PDescriptor descriptor, IDLLexer lexer) throws IOException {
        IDLToken next = lexer.expect("value");
        if (next.isIdentifier() || (descriptor.getType() != PType.ENUM && next.isQualifiedIdentifier())) {
            PTypeReference ref = DeclarationUtil.parseType(next.toString());
            if (registry.declaredTypeReferences(PDeclarationType.CONST).contains(ref) ||
                registry.getIncludes().containsKey(ref.getNamespace())) {
                try {
                    Object value = registry.constant(DeclarationUtil.parseType(next.toString()));
                    if (value != null) {
                        return coerceStrict(descriptor, value).orElse(null);
                    }
                } catch (IllegalArgumentException e) {
                    throw new IDLException(next, "Unable to cast const to " + descriptor.getTypeName()).initCause(e);
                }
            }
        }
        switch (descriptor.getType()) {
            case BOOL: {
                switch (next.toString().toLowerCase(Locale.US)) {
                    case "true":
                        return true;
                    case "false":
                        return false;
                    default:
                        throw new IDLException(next, "Unknown boolean value");
                }
            }
            case BYTE:
                if (next.isDoubleQualifiedIdentifier() || next.isQualifiedIdentifier()) {
                    // try to resolve enum and use value.
                    return (byte) resolveEnumValue(next, Byte.MIN_VALUE, Byte.MAX_VALUE, "byte");
                }
                try {
                    return Byte.parseByte(next.toString());
                } catch (NumberFormatException e) {
                    throw new IDLException(next, "Invalid Byte: " + e.getMessage());
                }
            case SHORT:
                if (next.isDoubleQualifiedIdentifier() || next.isQualifiedIdentifier()) {
                    // try to resolve enum and use value.
                    return (short) resolveEnumValue(next, Short.MIN_VALUE, Short.MAX_VALUE, "short");
                }
                try {
                    return Short.parseShort(next.toString());
                } catch (NumberFormatException e) {
                    throw new IDLException(next, "Invalid Short: " + e.getMessage()).initCause(e);
                }
            case INT:
                if (next.isDoubleQualifiedIdentifier() || next.isQualifiedIdentifier()) {
                    // try to resolve enum and use value.
                    return resolveEnumValue(next, Integer.MIN_VALUE, Integer.MAX_VALUE, "int");
                }
                try {
                    return Integer.parseInt(next.toString());
                } catch (NumberFormatException e) {
                    throw new IDLException(next, "Invalid Integer: " + e.getMessage()).initCause(e);
                }
            case LONG:
                if (next.isDoubleQualifiedIdentifier() || next.isQualifiedIdentifier()) {
                    // try to resolve enum and use value.
                    return (long) resolveEnumValue(next, Integer.MIN_VALUE, Integer.MAX_VALUE, "long");
                }
                try {
                    return Long.parseLong(next.toString());
                } catch (NumberFormatException e) {
                    throw new IDLException(next, "Invalid Long: " + e.getMessage()).initCause(e);
                }
            case DOUBLE:
                try {
                    return Double.parseDouble(next.toString());
                } catch (NumberFormatException e) {
                    throw new IDLException(next, "Invalid Double: " + e.getMessage()).initCause(e);
                }
            case STRING:
                if (next.type() != IDLTokenType.STRING) {
                    throw new IDLException(next, "Not a string value: " + next);
                }
                try {
                    return next.decodeString(false);
                } catch (IllegalStateException | IllegalArgumentException e) {
                    throw new IDLException(next, "Bad string escaping: " + e.getMessage()).initCause(e);
                }
            case BINARY:
                if (next.type() != IDLTokenType.STRING) {
                    throw new IDLException(next, "Not a binary value: " + next);
                }
                try {
                    return Binary.fromBase64(next.decodeString(false));
                } catch (IllegalArgumentException | IllegalStateException e) {
                    throw new IDLException(next, "Bad binary encoding: " + e.getMessage()).initCause(e);
                }
            case ENUM: {
                PEnumDescriptor ed = (PEnumDescriptor) descriptor;
                PEnum value;
                if (next.isInteger()) {
                    value = ed.findById(Integer.parseInt(next.toString()));
                } else if (next.isQualifiedIdentifier()) {
                    String[] parts = next.toString().split("[.]");
                    try {
                        PDescriptor type = registry.resolve(ref(parts[0]));
                        if (type.equals(ed)) {
                            value = ed.findByName(parts[1]);
                        } else {
                            throw new IDLException(next, "Not referencing an enum type: " + parts[0]);
                        }
                    } catch (IllegalArgumentException e) {
                        throw new IDLException(next, "Unknown type reference: " + next).initCause(e);
                    }
                } else if (next.isDoubleQualifiedIdentifier()) {
                    String[] parts = next.toString().split("[.]");
                    try {
                        PDescriptor type = registry.resolve(ref(parts[0], parts[1]));
                        if (type.equals(ed)) {
                            value = ed.findByName(parts[2]);
                        } else {
                            throw new IDLException(next, "Not referencing an enum type: " + parts[0]);
                        }
                    } catch (IllegalArgumentException e) {
                        throw new IDLException(next, "Unknown type reference: " + next).initCause(e);
                    }
                } else {
                    throw new IDLException(next, "Unknown enum value format: " + next);
                }
                if (value == null) {
                    throw new IDLException(next, "Unknown " + ed.getTypeName() + " value " + next);
                }
                return value;
            }
            case MESSAGE: {
                PMessageDescriptor<?> md = (PMessageDescriptor<?>) descriptor;
                PMessageBuilder builder = md.newBuilder();

                if (!next.isSymbol('{')) {
                    throw new IDLException(next, "Invalid message start " + next);
                }
                next = lexer.expect("field or end");
                while (!next.isSymbol('}')) {
                    if (next.type() != IDLTokenType.STRING) {
                        throw new IDLException(next, "Invalid field name " + next);
                    }
                    PField field = md.findFieldByName(next.decodeString(false));
                    if (field == null) {
                        throw new IDLException(next, "No field on " + md.getTypeName() + " named '" + next.decodeString(false) + "'");
                    }
                    lexer.expectSymbol("field value sep", ':');
                    Object value = parseValue(field.getResolvedDescriptor(), lexer);
                    builder.set(field.getId(), value);

                    if (lexer.peek("sep").isSymbol(',') ||
                        lexer.peek("sep").isSymbol(';')) {
                        lexer.next();
                    }
                    next = lexer.expect("field or end");
                }
                return builder.build();
            }
            case LIST: {
                @SuppressWarnings("unchecked")
                PListDescriptor<Object> ld = (PListDescriptor<Object>) descriptor;
                ListBuilder<Object> builder = ld.builder(4);
                if (!next.isSymbol('[')) {
                    throw new IDLException(next, "Invalid list start " + next);
                }
                while (!lexer.peek("end").isSymbol(']')) {
                    builder.add(parseValue(ld.getItemType(), lexer));
                    if (lexer.peek("sep").isSymbol(',') ||
                        lexer.peek("sep").isSymbol(';')) {
                        lexer.next();
                    }
                }
                lexer.next();  // skip the ending ']'
                return builder.build();
            }
            case SET: {
                @SuppressWarnings("unchecked")
                PSetDescriptor<Object> ld      = (PSetDescriptor<Object>) descriptor;
                SetBuilder<Object>     builder = ld.builder(4);
                if (!next.isSymbol('[')) {
                    throw new IDLException(next, "Invalid set start " + next);
                }
                while (!lexer.peek("end").isSymbol(']')) {
                    builder.add(parseValue(ld.getItemType(), lexer));
                    if (lexer.peek("sep").isSymbol(',') ||
                        lexer.peek("sep").isSymbol(';')) {
                        lexer.next();
                    }
                }
                lexer.next();  // skip the ending ']'
                return builder.build();
            }
            case MAP: {
                @SuppressWarnings("unchecked")
                PMapDescriptor<Object, Object> ld      = (PMapDescriptor<Object, Object>) descriptor;
                MapBuilder<Object, Object>     builder = ld.builder(4);
                if (!next.isSymbol('{')) {
                    throw new IDLException(next, "Invalid map start " + next);
                }
                while (!lexer.peek("end").isSymbol('}')) {
                    Object key = parseValue(ld.getKeyType(), lexer);
                    lexer.expectSymbol("key value sep", ':');
                    Object value = parseValue(ld.getValueType(), lexer);
                    builder.put(key, value);

                    if (lexer.peek("sep").isSymbol(',') ||
                        lexer.peek("sep").isSymbol(';')) {
                        lexer.next();
                    }
                }
                lexer.next();  // skip the ending ']'
                return builder.build();
            }
        }
        throw new IDLException(next, descriptor.getTypeName() + " type not allowed in constants");
    }

    private int resolveEnumValue(IDLToken referenceToken, int minValue, int maxValue, String typeName) throws IDLException {
        String enumReference = referenceToken.toString();
        int lastDot = enumReference.lastIndexOf('.');
        PTypeReference reference = DeclarationUtil.parseType(enumReference.substring(0, lastDot));
        PDescriptor descriptor;
        try {
            descriptor = registry.resolve(reference);
        } catch (IllegalArgumentException e) {
            throw new IDLException(referenceToken, "Unknown type " + e.getMessage()).initCause(e);
        }
        if (!(descriptor instanceof PEnumDescriptor)) {
            throw new IDLException(referenceToken, "Not an enum type: " + reference);
        }
        String valueName = enumReference.substring(lastDot + 1);
        PEnum value = ((PEnumDescriptor) descriptor).findByName(valueName);
        if (value == null) {
            throw new IDLException(referenceToken, "Not an value of " + descriptor.getTypeName() + ": " + valueName);
        }
        if (value.getValue() < minValue || value.getValue() > maxValue) {
            throw new IDLException(referenceToken, "Value outside " + typeName + " range: " + value.getValue());
        }
        return value.getValue();
    }
}
