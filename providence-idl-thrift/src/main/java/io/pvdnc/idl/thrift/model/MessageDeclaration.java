/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.thrift.model;

import io.pvdnc.core.types.PMessageVariant;
import io.pvdnc.idl.parser.IDLToken;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * <pre>{@code
 * variant ::= 'struct' | 'union' | 'exception' | 'interface'
 * message ::= {variant} {name} (('implements' | 'of') {implementing})? '{' {field}* '}' {annotations}?
 * }</pre>
 */
public class MessageDeclaration extends Declaration {
    private final IDLToken               variant;
    private final List<FieldDeclaration> fields;

    public MessageDeclaration(String documentation,
                              IDLToken variant,
                              IDLToken name,
                              List<FieldDeclaration> fields,
                              List<AnnotationDeclaration> annotations) {
        super(documentation, name, annotations);
        this.variant = requireNonNull(variant, "variant == null");
        this.fields = requireNonNull(fields);
    }

    public List<FieldDeclaration> getFields() {
        return fields;
    }

    public PMessageVariant getVariant() {
        switch (variant.toString()) {
            case "union": return PMessageVariant.UNION;
            case "exception": return PMessageVariant.EXCEPTION;
        }
        return PMessageVariant.STRUCT;
    }

    public IDLToken getVariantToken() {
        return variant;
    }

    // --- Displayable

    @Override
    public String displayString() {
        StringBuilder builder = new StringBuilder(getVariantToken() + " " + getName() + " {\n");
        for (FieldDeclaration field : fields) {
            builder.append("    ").append(field.displayString());
        }
        builder.append("}\n");
        return builder.toString();
    }

    // --- Object

    @Override
    public String toString() {
        return "Message{" + fields.stream().map(Declaration::getName).collect(Collectors.joining(",")) + "}";
    }
}
