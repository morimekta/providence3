/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.thrift.generator;

import io.pvdnc.core.PService;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.rpc.PServiceCallHandler;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PServiceDescriptor;
import io.pvdnc.core.types.PServiceMethod;
import io.pvdnc.core.types.PType;
import io.pvdnc.idl.generator.GeneratorException;
import io.pvdnc.idl.generator.ServiceTypeFormatter;
import io.pvdnc.idl.generator.util.BlockCommentWriter;
import io.pvdnc.idl.generator.util.JavaAnnotation;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;
import io.pvdnc.sio.thrift.ThriftProperties;

import java.io.IOException;
import java.util.Optional;
import java.util.Set;

import static net.morimekta.collect.UnmodifiableSet.setOf;
import static io.pvdnc.core.property.PDefaultProperties.DEPRECATED;
import static io.pvdnc.core.property.PDefaultProperties.DOCUMENTATION;
import static io.pvdnc.core.property.PDefaultProperties.MESSAGE_VARIANT;
import static io.pvdnc.core.types.PMessageVariant.EXCEPTION;
import static io.pvdnc.idl.generator.InnerClassGenerator.IMPL;
import static io.pvdnc.sio.thrift.ThriftProperties.THRIFT_SERVICE_METHOD;
import static io.pvdnc.sio.thrift.ThriftProperties.THRIFT_SERVICE_METHODS_THROWS;
import static io.pvdnc.sio.thrift.ThriftProperties.THRIFT_SERVICE_METHOD_ONEWAY;
import static net.morimekta.strings.NamingUtil.Format.PASCAL;
import static net.morimekta.strings.NamingUtil.format;

public class ThriftIFaceFormatter implements ServiceTypeFormatter {
    @Override
    public Set<String> getImportClasses(JavaTypeHelper helper, PServiceDescriptor descriptor) {
        return setOf(PService.class.getName(),
                     PServiceCallHandler.class.getName(),
                     IOException.class.getName());
    }

    @Override
    public void appendClassAnnotations(JavaTypeWriter writer, PServiceDescriptor descriptor) {
        writer.appendln(JavaAnnotation.SUPPRESS_WARNINGS_UNUSED);
    }

    @Override
    public void appendMethods(JavaTypeWriter writer, PServiceDescriptor descriptor) {
        if (!descriptor.getProperty(ThriftProperties.THRIFT_SERVICE)) {
            return;
        }

        JavaTypeHelper helper = writer.getHelper();
        boolean firstMethod = true;
        for (PServiceMethod method : descriptor.declaredMethods()) {
            if (firstMethod) {
                firstMethod = false;
            } else {
                writer.newline();
            }

            PMessageDescriptor<?> request = method.getRequestType();
            PMessageDescriptor<?> response = method.getResponseType();

            try (BlockCommentWriter comment = new BlockCommentWriter(writer)) {
                if (method.hasProperty(DOCUMENTATION)) {
                    comment.comment(method.getProperty(DOCUMENTATION));
                }
                if (method.getProperty(THRIFT_SERVICE_METHOD)) {
                    for (PField field : request.allFields()) {
                        comment.param_('p' + format(field.getName(), PASCAL),
                                       Optional.ofNullable(field.getProperty(DOCUMENTATION))
                                               .orElse("The " + field.getName() + " value."));
                    }
                    PField success;
                    if (method.getProperty(THRIFT_SERVICE_METHOD_ONEWAY)) {
                        success = new PField(0, PType.VOID.provider(), "success", null,
                                             PPropertyMap.of(),
                                             null,
                                             () -> response);
                    } else {
                        success = response.fieldForId(0);
                    }
                    if (success.getType() != PType.VOID) {
                        comment.return_("The success response.");
                    }
                } else {
                    comment.param_("request", "The call request.");
                    comment.return_("The call response message.");
                }
                if (method.hasProperty(DEPRECATED)) {
                    comment.deprecated_(method.getProperty(DEPRECATED));
                }
            }
            if (method.getProperty(THRIFT_SERVICE_METHOD)) {
                PField success;
                if (method.getProperty(THRIFT_SERVICE_METHOD_ONEWAY)) {
                    success = new PField(0, PType.VOID.provider(), "success", null,
                                         PPropertyMap.of(),
                                         null,
                                         () -> response);
                } else {
                    success = response.fieldForId(0);
                }
                writer.formatln("%s %s(",
                                helper.getValueType(success.getResolvedDescriptor()),
                                method.getName())
                      .begin(" ".repeat(helper.getValueType(success.getResolvedDescriptor()).length() +
                                        method.getName().length() + 2));
                boolean firstField = true;
                for (PField field : request.allFields()) {
                    if (firstField) {
                        firstField = false;
                    } else {
                        writer.append(',').appendln();
                    }
                    writer.format("%s p%s",
                                  helper.getValueClass(field.getResolvedDescriptor()),
                                  format(field.getName(), PASCAL));
                }
                writer.format(")")
                      .end()
                      .formatln("        throws %s", IOException.class.getSimpleName())
                      .begin(   "               ");
                if (method.hasProperty(THRIFT_SERVICE_METHODS_THROWS)) {
                    writer.appendln(method.getProperty(THRIFT_SERVICE_METHODS_THROWS));
                } else {
                    for (PField field : response.allFields()) {
                        if (field.getId() == 0) continue;
                        if (field.getType() != PType.MESSAGE) {
                            throw new GeneratorException(
                                    "Not an exception type for throws on " + field.getName() +
                                    " on " + descriptor.getTypeName() + "." + method.getName());
                        }
                        PMessageDescriptor<?> ex = (PMessageDescriptor<?>) field.getResolvedDescriptor();
                        if (ex.getProperty(MESSAGE_VARIANT) != EXCEPTION) {
                            throw new GeneratorException(
                                    "Not an exception type for throws on " + field.getName() +
                                    " on " + descriptor.getTypeName() + "." + method.getName());
                        }
                        writer.append(',')
                              .appendln(helper.javaTypeReference(ex, IMPL));
                    }
                }
                writer.end()
                      .append(";");
            } else {
                // same as comment above.
                writer.formatln("%s %s(%s request) throws %s;",
                                helper.javaTypeReference(response),
                                method.getName(),
                                helper.javaTypeReference(request),
                                IOException.class.getSimpleName());
            }
        }
    }
}
