/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.thrift;

import io.pvdnc.core.property.PProperty;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.idl.parser.IDLParser;
import io.pvdnc.idl.parser.IDLParserOptions;
import io.pvdnc.idl.parser.IDLParserProvider;
import io.pvdnc.idl.thrift.util.ThriftFileUtil;

import java.nio.file.Path;
import java.util.List;

import static java.util.Objects.requireNonNull;
import static net.morimekta.file.PathUtil.getFileName;
import static io.pvdnc.idl.thrift.ThriftParserProperties.ALLOW_LANGUAGE_RESERVED_NAMES;
import static io.pvdnc.idl.thrift.ThriftParserProperties.REQUIRE_ENUM_VALUE;
import static io.pvdnc.idl.thrift.ThriftParserProperties.REQUIRE_FIELD_ID;

public class ThriftParserProvider implements IDLParserProvider {
    @Override
    public boolean willHandleFile(Path filePath) {
        return ThriftFileUtil.isApacheThriftFileName(getFileName(filePath));
    }

    @Override
    public String getName() {
        return "thrift";
    }

    @Override
    public List<PProperty<?>> getOptions() {
        return List.of(REQUIRE_ENUM_VALUE,
                       REQUIRE_FIELD_ID,
                       ALLOW_LANGUAGE_RESERVED_NAMES);
    }

    @Override
    public IDLParser getParser(IDLParserOptions options) {
        PPropertyMap properties = options.getParserOptions(getName());
        requireNonNull(properties, "properties == null");
        return new ThriftParser(properties.getPropertyFor(REQUIRE_FIELD_ID, properties),
                                properties.getPropertyFor(REQUIRE_ENUM_VALUE, properties),
                                properties.getPropertyFor(ALLOW_LANGUAGE_RESERVED_NAMES, properties));
    }
}
