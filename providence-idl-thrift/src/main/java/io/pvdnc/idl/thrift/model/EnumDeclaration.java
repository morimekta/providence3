/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.thrift.model;

import io.pvdnc.idl.parser.IDLToken;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * <pre>{@code
 * enum ::= 'enum' {name} '{' {enum_value}* '}' {annotations}?
 * }</pre>
 */
public class EnumDeclaration extends Declaration {
    private final List<EnumValueDeclaration> values;

    public EnumDeclaration(String documentation,
                           IDLToken name,
                           List<EnumValueDeclaration> values,
                           List<AnnotationDeclaration> annotations) {
        super(documentation, name, annotations);
        this.values = requireNonNull(values, "values == null");
    }

    public List<EnumValueDeclaration> getValues() {
        return values;
    }

    // --- Displayable

    @Override
    public String displayString() {
        StringBuilder builder = new StringBuilder("enum " + getName() + " {\n");
        for (EnumValueDeclaration v : values) {
            builder.append("    ").append(v.displayString());
        }
        builder.append("}\n");
        return builder.toString();
    }

    @Override
    public String toString() {
        return "Enum{" + values.stream().map(EnumValueDeclaration::getName).collect(Collectors.joining(",")) + "}";
    }
}
