/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.thrift.generator;

import io.pvdnc.core.PService;
import io.pvdnc.core.types.PServiceDescriptor;
import io.pvdnc.idl.generator.InnerClassGenerator;
import io.pvdnc.idl.generator.ServiceTypeFormatter;
import io.pvdnc.idl.generator.services.PServiceFormatter;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;
import io.pvdnc.sio.thrift.ThriftProperties;

import java.util.Set;

import static net.morimekta.collect.UnmodifiableSet.setOf;

public class ThriftClientGenerator extends InnerClassGenerator<PServiceDescriptor, ServiceTypeFormatter> {
    public static final String INNER_CLASS = "$ThriftClient";

    public ThriftClientGenerator() {
        addFormatter(new ThriftClientFormatter());
        addFormatter(new PServiceFormatter());
    }

    @Override
    public String getInnerClassName() {
        return INNER_CLASS;
    }

    @Override
    public boolean isEnabled(PServiceDescriptor descriptor) {
        return descriptor.getProperty(ThriftProperties.THRIFT_SERVICE);
    }

    @Override
    public Set<String> getClassImports(JavaTypeHelper helper, PServiceDescriptor descriptor) {
        return setOf(PService.class.getName());
    }

    @Override
    public void appendClass(JavaTypeWriter writer, PServiceDescriptor descriptor) {
        JavaTypeHelper helper = writer.getHelper();

        writer.formatln("class %s\n" +
                        "        implements %s, %s.IFace {",
                        INNER_CLASS,
                        PService.class.getSimpleName(),
                        helper.javaTypeReference(descriptor))
              .begin();

        forEachFormatter(f -> f.appendFields(writer, descriptor));

        forEachFormatter(f -> f.appendConstructors(writer, descriptor));

        forEachFormatter(f -> f.appendMethods(writer, descriptor));

        forEachFormatter(f -> f.appendConstants(writer, descriptor));

        writer.end()
              .formatln("}");

    }
}
