/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.thrift.model;


import io.pvdnc.idl.parser.IDLToken;
import io.pvdnc.idl.thrift.util.DeclarationUtil;

import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * <pre>{@code
 * typedef ::= 'typedef' {name} '=' {type}
 * }</pre>
 */
public class TypedefDeclaration extends Declaration {
    private final IDLToken       typedefToken;
    private final List<IDLToken> type;

    public TypedefDeclaration(String documentation,
                              IDLToken typedefToken,
                              IDLToken name,
                              List<IDLToken> type) {
        super(documentation, name, null);
        this.typedefToken = requireNonNull(typedefToken, "typedef == null");
        this.type = requireNonNull(type, "type == null");
    }

    public IDLToken getTypedefToken() {
        return typedefToken;
    }

    public List<IDLToken> getTypeTokens() {
        return type;
    }

    public String getType() {
        return DeclarationUtil.toTypeString(getTypeTokens());
    }

    // --- Displayable

    @Override
    public String displayString() {
        return "typedef " + getName() + " " + getType() + ";\n";
    }
}
