/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.thrift.generator;

import io.pvdnc.core.rpc.PServiceCallHandler;
import io.pvdnc.core.types.PServiceDescriptor;
import io.pvdnc.idl.generator.ServiceTypeFormatter;
import io.pvdnc.idl.generator.util.BlockCommentWriter;
import io.pvdnc.idl.generator.util.JavaTypeHelper;
import io.pvdnc.idl.generator.util.JavaTypeWriter;
import io.pvdnc.sio.thrift.ThriftProperties;

import java.util.Set;

import static net.morimekta.collect.UnmodifiableSet.setOf;

public class ThriftMainFormatter implements ServiceTypeFormatter {
    @Override
    public Set<String> getImportClasses(JavaTypeHelper helper, PServiceDescriptor descriptor) {
        return setOf(PServiceCallHandler.class.getName());
    }

    @Override
    public void appendConstructors(JavaTypeWriter writer, PServiceDescriptor descriptor) {
        if (!descriptor.getProperty(ThriftProperties.THRIFT_SERVICE)) {
            return;
        }

        JavaTypeHelper helper = writer.getHelper();
        writer.newline();
        try (BlockCommentWriter comment = new BlockCommentWriter(writer)) {
            comment.comment("Create a new thrift client instance.");
            comment.newline();
            comment.param_("handler", "The client handler.");
            comment.return_("The thrift client implementation");
        }
        writer.formatln("static %s.IFace newThriftClient(%s handler) {\n" +
                        "    return new %s(handler);\n" +
                        "}",
                        helper.javaTypeReference(descriptor), PServiceCallHandler.class.getSimpleName(),
                        helper.javaTypeReference(descriptor, ThriftClientGenerator.INNER_CLASS));
        writer.newline()
              .formatln("// ---- newThriftProcessor(IFace impl) ----");
    }
}
