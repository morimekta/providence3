/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.thrift.parser;

import io.pvdnc.idl.thrift.model.AnnotationDeclaration;
import io.pvdnc.idl.thrift.model.Declaration;
import io.pvdnc.idl.thrift.util.DeclarationUtil;
import net.morimekta.lexer.LexerException;
import net.morimekta.lexer.UncheckedLexerException;
import io.pvdnc.core.impl.Empty;
import io.pvdnc.core.property.PDefaultProperties;
import io.pvdnc.core.property.PPropertyMap;
import io.pvdnc.core.reflect.CEnumDescriptor;
import io.pvdnc.core.reflect.CMessageDescriptor;
import io.pvdnc.core.reflect.CServiceDescriptor;
import io.pvdnc.core.registry.PNamespaceFileTypeRegistry;
import io.pvdnc.core.registry.PTypeReference;
import io.pvdnc.core.types.PConstDescriptor;
import io.pvdnc.core.types.PDeclarationType;
import io.pvdnc.core.types.PDescriptor;
import io.pvdnc.core.types.PEnumDescriptor;
import io.pvdnc.core.types.PField;
import io.pvdnc.core.types.PMapDescriptor;
import io.pvdnc.core.types.PMessageDescriptor;
import io.pvdnc.core.types.PMessageVariant;
import io.pvdnc.core.types.PServiceDescriptor;
import io.pvdnc.core.types.PServiceMethod;
import io.pvdnc.core.types.PSetDescriptor;
import io.pvdnc.core.types.PTypeDefDescriptor;
import io.pvdnc.idl.parser.IDLException;
import io.pvdnc.idl.parser.IDLToken;
import io.pvdnc.idl.parser.IDLTokenType;
import io.pvdnc.idl.parser.IDLWarningHandler;
import io.pvdnc.idl.thrift.model.ConstDeclaration;
import io.pvdnc.idl.thrift.model.EnumDeclaration;
import io.pvdnc.idl.thrift.model.EnumValueDeclaration;
import io.pvdnc.idl.thrift.model.FieldDeclaration;
import io.pvdnc.idl.thrift.model.MessageDeclaration;
import io.pvdnc.idl.thrift.model.MethodDeclaration;
import io.pvdnc.idl.thrift.model.NamespaceDeclaration;
import io.pvdnc.idl.thrift.model.ProgramDeclaration;
import io.pvdnc.idl.thrift.model.ServiceDeclaration;
import io.pvdnc.idl.thrift.model.TypedefDeclaration;
import io.pvdnc.sio.thrift.ThriftProperties;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;
import static io.pvdnc.core.property.PDefaultProperties.DOCUMENTATION;
import static io.pvdnc.core.property.PDefaultProperties.FIELD_REQUIREMENT;
import static io.pvdnc.core.property.PDefaultProperties.MESSAGE_VARIANT;
import static io.pvdnc.core.property.PDefaultProperties.REF_ARGUMENTS;
import static io.pvdnc.core.property.PDefaultProperties.REF_ENUM;
import static io.pvdnc.core.property.PDefaultProperties.SORTED;
import static io.pvdnc.core.registry.PTypeReference.ref;
import static io.pvdnc.sio.thrift.ThriftProperties.THRIFT_SERVICE;

public class ThriftModelConverter {
    public void convert(ProgramDeclaration program,
                        PNamespaceFileTypeRegistry registry,
                        IDLWarningHandler warningHandler) throws IDLException {
        PPropertyMap.Builder properties = PPropertyMap.newBuilder();
        for (NamespaceDeclaration namespace : program.getNamespaces()) {
            properties.putUnsafe(namespace.getLanguage() + ".namespace", namespace.getNamespace());
        }
        if (program.getDocumentation() != null) {
            properties.put(DOCUMENTATION, program.getDocumentation());
        }
        registry.setProperties(properties.build());

        // 1: list types that may be referenced
        Map<String, PDeclarationType> localTypes = new HashMap<>();
        for (Declaration declaration : program.getDeclarationList()) {
            if (declaration instanceof EnumDeclaration) {
                localTypes.put(declaration.getName(), PDeclarationType.ENUM);
            } else if (declaration instanceof MessageDeclaration) {
                localTypes.put(declaration.getName(), PDeclarationType.MESSAGE);
            } else if (declaration instanceof TypedefDeclaration) {
                localTypes.put(declaration.getName(), PDeclarationType.TYPEDEF);
            } else if (declaration instanceof ServiceDeclaration) {
                localTypes.put(declaration.getName(), PDeclarationType.SERVICE);
            } else if (declaration instanceof ConstDeclaration) {
                localTypes.put(declaration.getName(), PDeclarationType.CONST);
            }
        }

        // 2: register everything
        for (Declaration declaration : program.getDeclarationList()) {
            if (declaration instanceof EnumDeclaration) {
                registerEnum(program, (EnumDeclaration) declaration, registry);
            } else if (declaration instanceof MessageDeclaration) {
                registerMessage(localTypes, program, (MessageDeclaration) declaration, registry, warningHandler);
            } else if (declaration instanceof TypedefDeclaration) {
                registerTypedef(localTypes, program, (TypedefDeclaration) declaration, registry, warningHandler);
            } else if (declaration instanceof ServiceDeclaration) {
                registerService(localTypes, program, (ServiceDeclaration) declaration, registry, warningHandler);
            } else if (declaration instanceof ConstDeclaration) {
                registerConst(localTypes, (ConstDeclaration) declaration, registry, warningHandler);
            }
        }

        // 3: validate everything
        for (Declaration declaration : program.getDeclarationList()) {
            if (declaration instanceof EnumDeclaration) {
                validateEnum(program, (EnumDeclaration) declaration, registry);
            } else if (declaration instanceof MessageDeclaration) {
                validateMessage((MessageDeclaration) declaration, registry);
            } else if (declaration instanceof TypedefDeclaration) {
                validateTypedef((TypedefDeclaration) declaration, registry);
            } else if (declaration instanceof ServiceDeclaration) {
                validateService((ServiceDeclaration) declaration, registry);
            } else if (declaration instanceof ConstDeclaration) {
                validateConst((ConstDeclaration) declaration, registry);
            }
        }
    }

    private static void registerEnum(ProgramDeclaration program,
                                     EnumDeclaration declaration,
                                     PNamespaceFileTypeRegistry registry) {
        CEnumDescriptor.Builder builder = CEnumDescriptor.builder(
                program.getProgramName(), declaration.getName());
        for (EnumValueDeclaration valueDeclaration : declaration.getValues()) {
            PPropertyMap ppm = makeProperties(valueDeclaration.getAnnotations(), valueDeclaration.getDocumentation());
            builder.value(valueDeclaration.getName(), valueDeclaration.getId(), ppm);
        }
        builder.properties(makeProperties(declaration.getAnnotations(), declaration.getDocumentation()));
        registry.register(builder.build());
    }

    private static void validateEnum(ProgramDeclaration program,
                                     EnumDeclaration declaration,
                                     PNamespaceFileTypeRegistry registry) {
        // Nothing to do.
    }

    private static void registerMessage(Map<String, PDeclarationType> localTypes,
                                        ProgramDeclaration program,
                                        MessageDeclaration declaration,
                                        PNamespaceFileTypeRegistry registry,
                                        IDLWarningHandler warningHandler) throws IDLException {
        PPropertyMap messageProps = makeProperties(declaration.getAnnotations(), PPropertyMap.of(
                MESSAGE_VARIANT, declaration.getVariant()), declaration.getDocumentation());
        CMessageDescriptor.Builder builder = CMessageDescriptor.builder(program.getProgramName(),
                                                                        declaration.getName())
                                                               .properties(messageProps);
        for (FieldDeclaration field : declaration.getFields()) {
            PPropertyMap fieldProps = makeProperties(field.getAnnotations(),
                                                     PPropertyMap.of(FIELD_REQUIREMENT,
                                                                     field.getRequirement()),
                                                     field.getDocumentation());
            Supplier<? extends PDescriptor> provider = DeclarationUtil.makeTypeProvider(
                    localTypes, registry, field.getTypeTokens(), fieldProps, warningHandler);
            builder.field(field.getName(), provider)
                   .id(field.getId())
                   .properties(fieldProps);
        }
        registry.register(builder.build());
    }

    private static void validateMessage(MessageDeclaration declaration,
                                        PNamespaceFileTypeRegistry registry) throws IDLException {
        PTypeReference reference  = ref(declaration.getName());
        PDescriptor    descriptor = registry.resolve(reference);
        if (!(descriptor instanceof PMessageDescriptor)) {
            throw new IDLException(declaration.getVariantToken(), "Unable to parse message");
        }
        PMessageDescriptor<?> messageDescriptor = (PMessageDescriptor<?>) descriptor;
        for (PField field : messageDescriptor.allFields()) {
            FieldDeclaration fieldDeclaration = findFieldByName(declaration.getFields(), field.getName());
            validateField(fieldDeclaration, field, registry);
        }
    }

    private static void validateField(FieldDeclaration declaration,
                                      PField descriptor,
                                      PNamespaceFileTypeRegistry registry) throws IDLException {
        try {
            PDescriptor type = requireNonNull(descriptor.getResolvedDescriptor(), "field type == null");
            if (descriptor.hasProperty(SORTED) && descriptor.getProperty(SORTED)) {
                if (!(type instanceof PSetDescriptor) &&
                    !(type instanceof PMapDescriptor)) {
                    AnnotationDeclaration annotation = findAnnotationByName(declaration.getAnnotations(), SORTED.getName());
                    throw new IDLException(annotation.getValueToken(), "Field type not applicable for sorted annotation: " + type.getTypeName());
                }
            }
        } catch (IllegalStateException | IllegalArgumentException e) {
            throw new IDLException(toSingleToken(declaration.getTypeTokens()), "Invalid field type: " + e.getMessage())
                    .initCause(e);
        }
        try {
            descriptor.getDefaultValue();
        } catch (IllegalArgumentException | IllegalStateException e) {
            throw new IDLException(toSingleToken(declaration.getDefaultValueTokens()), "Invalid default value: " + e.getMessage())
                    .initCause(e);
        }
        if (descriptor.hasProperty(REF_ENUM)) {
            String                refEnum    = descriptor.getProperty(REF_ENUM);
            AnnotationDeclaration annotation = findAnnotationByName(declaration.getAnnotations(), REF_ENUM.getName());
            try {
                PTypeReference reference         = DeclarationUtil.parseType(refEnum);
                PDescriptor    refEnumDescriptor = registry.resolve(reference);
                if (!(refEnumDescriptor instanceof PEnumDescriptor)) {
                    throw new IDLException(annotation.getValueToken(), "Not an enum type for ref.enum: " + refEnum);
                }
            } catch (IllegalArgumentException e) {
                throw new IDLException(annotation.getValueToken(), "Invalid ref.enum type: " + refEnum)
                        .initCause(e);
            }
        }
        if (descriptor.hasProperty(REF_ARGUMENTS)) {
            String refArguments = descriptor.getProperty(REF_ARGUMENTS);
            AnnotationDeclaration annotation = findAnnotationByName(declaration.getAnnotations(), REF_ARGUMENTS.getName());

            try {
                PTypeReference reference = DeclarationUtil.parseType(refArguments);
                PDescriptor refArgsDescriptor = registry.resolve(reference);
                if (!(refArgsDescriptor instanceof PMessageDescriptor)) {
                    throw new IDLException(annotation.getValueToken(), "Not an message type for ref.arguments: " + refArguments);
                }
            } catch (IllegalArgumentException e) {
                throw new IDLException(annotation.getValueToken(), "Invalid ref.arguments type: " + refArguments)
                        .initCause(e);
            }
        }
    }

    private static void registerTypedef(
            Map<String, PDeclarationType> localTypes,
            ProgramDeclaration program,
            TypedefDeclaration declaration,
            PNamespaceFileTypeRegistry registry,
            IDLWarningHandler warningHandler) throws IDLException {
        Supplier<? extends PDescriptor> provider =
                DeclarationUtil.makeTypeProvider(localTypes, registry, declaration.getTypeTokens(), PPropertyMap.empty(), warningHandler);
        registry.register(new PTypeDefDescriptor(
                program.getProgramName(), declaration.getName(), PPropertyMap.empty(), provider));
    }

    private static void validateTypedef(TypedefDeclaration declaration,
                                        PNamespaceFileTypeRegistry registry) throws IDLException {
        PTypeReference reference  = ref(declaration.getName());
        PDescriptor    descriptor = registry.descriptor(reference);
        if (!(descriptor instanceof PTypeDefDescriptor)) {
            throw new IDLException(declaration.getTypedefToken(), "Unable to parse typedef");
        }
        PTypeDefDescriptor typeDefinition = (PTypeDefDescriptor) descriptor;
        try {
            typeDefinition.getDescriptor();
        } catch (IllegalStateException | IllegalArgumentException e) {
            throw new IDLException(toSingleToken(declaration.getTypeTokens()),
                                      "Invalid field type: " + e.getMessage());
        }
    }

    private static void registerService(Map<String, PDeclarationType> localTypes,
                                        ProgramDeclaration program,
                                        ServiceDeclaration declaration,
                                        PNamespaceFileTypeRegistry registry,
                                        IDLWarningHandler warningHandler) throws IDLException {
        CServiceDescriptor.Builder builder = CServiceDescriptor.newBuilder(program.getProgramName(), declaration.getName());
        PPropertyMap serviceProps = makeProperties(
                declaration.getAnnotations(),
                PPropertyMap.of(
                        THRIFT_SERVICE, true),
                declaration.getDocumentation());
        builder.properties(serviceProps);
        if (declaration.getExtendingToken() != null) {
            builder.extending(DeclarationUtil.makeServiceProvider(localTypes, declaration.getExtendingToken(), registry));
        }
        for (MethodDeclaration method : declaration.getMethods()) {
            PMessageDescriptor<?> responseType = Empty.kDescriptor;
            PPropertyMap.Builder methodProps = PPropertyMap.newBuilder();
            for (AnnotationDeclaration anno : declaration.getAnnotations()) {
                methodProps.putUnsafe(anno.getTag(), anno.getValue());
            }

            if (method.getOneWayToken() == null) {
                PPropertyMap responseProps = makeProperties(
                        method.getAnnotations(),
                        PPropertyMap.of(MESSAGE_VARIANT, PMessageVariant.UNION), null);

                CMessageDescriptor.Builder responseBuilder = CMessageDescriptor.builder(
                    program.getProgramName(),
                    declaration.getName() + "." + method.getName() + ".Response")
                        .properties(responseProps);
                Supplier<? extends PDescriptor> response = DeclarationUtil.makeTypeProvider(localTypes,
                                                                            registry,
                                                                            method.getReturnTypeTokens(),
                                                                            methodProps.build(),
                                                                            warningHandler);
                responseBuilder.field("success", response).id(0);

                for (FieldDeclaration field : method.getThrowing()) {
                    PPropertyMap exProp = makeProperties(field.getAnnotations(), field.getDocumentation());
                    Supplier<? extends PDescriptor>  ex = DeclarationUtil.makeTypeProvider(localTypes,
                                                                           registry,
                                                                           field.getTypeTokens(),
                                                                           exProp,
                                                                           warningHandler);
                    responseBuilder.field(field.getName(), ex)
                                   .properties(exProp)
                                   .id(field.getId());
                }

                responseType = responseBuilder.build();
            } else {
                methodProps.put(ThriftProperties.THRIFT_SERVICE_METHOD_ONEWAY, true);
            }
            methodProps.put(ThriftProperties.THRIFT_SERVICE_METHOD, true);

            CMessageDescriptor.Builder requestBuilder = CMessageDescriptor.builder(
                    program.getProgramName(),
                    declaration.getName() + "." + method.getName() + ".Request");
            for (FieldDeclaration field : method.getParams()) {
                PPropertyMap paramProps = makeProperties(field.getAnnotations(),
                                                         PPropertyMap.of(FIELD_REQUIREMENT, field.getRequirement()),
                                                         field.getDocumentation());
                Supplier<? extends PDescriptor> ex = DeclarationUtil.makeTypeProvider(localTypes,
                                                                      registry,
                                                                      field.getTypeTokens(),
                                                                      paramProps,
                                                                      warningHandler);
                requestBuilder.field(field.getName(), ex)
                              .id(field.getId())
                              .properties(paramProps);
            }
            requestBuilder.property(MESSAGE_VARIANT, PMessageVariant.STRUCT);
            CMessageDescriptor requestType = requestBuilder.build();

            builder.method(method.getName(), responseType, requestType, methodProps.build());
            if (!Empty.kDescriptor.equals(responseType)) {
                registry.register(responseType);
            }
            registry.register(requestType);
        }
        builder.properties(PPropertyMap.of(THRIFT_SERVICE, true));
        registry.register(builder.build());
    }

    private static void validateService(ServiceDeclaration declaration,
                                        PNamespaceFileTypeRegistry registry) throws IDLException {

        PDescriptor descriptor;
        try {
            descriptor = registry.descriptor(ref(declaration.getName()));
        } catch (IllegalArgumentException e) {
            throw new IDLException(declaration.getServiceToken(), "Unable to parse service");
        }
        if (!(descriptor instanceof PServiceDescriptor)) {
            throw new IDLException(declaration.getServiceToken(), "Unable to parse service");
        }
        PServiceDescriptor service = (PServiceDescriptor) descriptor;
        try {
            service.extending();
        } catch (IllegalArgumentException e) {
            throw new IDLException(declaration.getExtendingToken(), "Unknown extending service");
        }

        Set<String> extendedMethods = service.allMethods()
                                         .stream()
                                         .filter(m -> !m.getOnServiceType().equals(service))
                                         .map(PServiceMethod::getName)
                                         .collect(Collectors.toSet());
        for (PServiceMethod method : service.declaredMethods()) {
            MethodDeclaration md = findMethodByName(declaration.getMethods(), method.getName());
            if (extendedMethods.contains(method.getName())) {
                throw new IDLException(md.getNameToken(), "Method already declared in " + declaration.getExtendingToken());
            }
            PMessageDescriptor<?> request = method.getRequestType();
            for (PField field : request.allFields()) {
                FieldDeclaration fd = findFieldByName(md.getParams(), field.getName());
                try {
                    field.getResolvedDescriptor();
                } catch (IllegalArgumentException e) {
                    throw new IDLException(toSingleToken(fd.getTypeTokens()), e.getMessage());
                }
                try {
                    field.getDefaultValue();
                } catch (UncheckedLexerException e) {
                    if (e.getCause() instanceof IDLException) {
                        throw (IDLException) e.getCause();
                    }
                    throw new IDLException(toSingleToken(fd.getDefaultValueTokens()), e.getCause().getMessage()).initCause(e.getCause());
                } catch (IllegalArgumentException e) {
                    throw new IDLException(toSingleToken(fd.getDefaultValueTokens()), e.getMessage()).initCause(e);
                }
            }
            if (!method.getProperty(ThriftProperties.THRIFT_SERVICE_METHOD_ONEWAY)) {
                PMessageDescriptor<?> response = method.getResponseType();
                for (PField field : response.allFields()) {
                    if (field.getId() == 0) {
                        try {
                            field.getResolvedDescriptor();
                        } catch (IllegalArgumentException e) {
                            throw new IDLException(toSingleToken(md.getReturnTypeTokens()), e.getMessage());
                        }
                        continue;
                    }
                    FieldDeclaration fd = findFieldByName(md.getThrowing(), field.getName());
                    try {
                        PDescriptor type = field.getResolvedDescriptor();
                        if (!(type instanceof PMessageDescriptor)) {
                            throw new IDLException(toSingleToken(fd.getTypeTokens()), "Not an exception type in throws declaration");
                        }
                        PMessageDescriptor<?> ex = (PMessageDescriptor<?>) type;
                        if (ex.getProperty(MESSAGE_VARIANT) != PMessageVariant.EXCEPTION) {
                            throw new IDLException(toSingleToken(fd.getTypeTokens()), "Not an exception type in throws declaration");
                        }
                    } catch (IllegalArgumentException e) {
                        throw new IDLException(toSingleToken(fd.getTypeTokens()), e.getMessage());
                    }
                }
            }
        }
    }


    private static void registerConst(Map<String, PDeclarationType> localTypes,
                                      ConstDeclaration declaration,
                                      PNamespaceFileTypeRegistry registry,
                                      IDLWarningHandler warningHandler) throws IDLException {
        Supplier<? extends PDescriptor> provider = DeclarationUtil.makeTypeProvider(
                localTypes,
                registry,
                declaration.getTypeTokens(),
                PPropertyMap.empty(),
                warningHandler);
        Supplier<Object> constParser = new ThriftConstParser(provider, registry, declaration.getValueTokens());
        registry.register(new PConstDescriptor(registry.getNamespace(), declaration.getName(), provider, constParser));
    }

    private static void validateConst(ConstDeclaration declaration,
                                      PNamespaceFileTypeRegistry registry) throws IDLException {
        PDescriptor descriptor;
        try {
            descriptor = registry.descriptor(ref(declaration.getName()));
        } catch (IllegalArgumentException e) {
            throw new IDLException(declaration.getConstToken(), "Unable to parse const");
        }
        if (!(descriptor instanceof PConstDescriptor)) {
            throw new IDLException(declaration.getConstToken(), "Unable to parse const");
        }
        PConstDescriptor constDescriptor = (PConstDescriptor) descriptor;
        try {
            constDescriptor.getDescriptor();
            Object value = constDescriptor.getValue();
            if (value == null) {
                throw new IDLException(toSingleToken(declaration.getValueTokens()), "Null const value");
            }
        } catch (IllegalArgumentException e) {
            throw new IDLException(toSingleToken(declaration.getTypeTokens()), e.getMessage());
        } catch (UncheckedLexerException e) {
            if (e.getCause() instanceof IDLException) {
                throw (IDLException) e.getCause();
            }
            // NOTE: Should not be possible?
            LexerException cause = e.getCause();
            throw new IDLException(cause.getLine(),
                                      cause.getLineNo(),
                                      cause.getLinePos(),
                                      cause.getLength(),
                                      cause.getMessage());
        }
    }

    private static AnnotationDeclaration findAnnotationByName(List<AnnotationDeclaration> annotations, String name) {
        for (AnnotationDeclaration annotation : annotations) {
            if (annotation.getTag().equals(name)) {
                return annotation;
            }
        }
        throw new IllegalArgumentException("No such annotation: " + name);

    }

    private static FieldDeclaration findFieldByName(List<FieldDeclaration> fieldList, String fieldName) {
        for (FieldDeclaration field : fieldList) {
            if (field.getName().equals(fieldName)) {
                return field;
            }
        }
        throw new IllegalArgumentException("No such field: " + fieldName);
    }

    private static MethodDeclaration findMethodByName(List<MethodDeclaration> methodList, String fieldName) {
        for (MethodDeclaration field : methodList) {
            if (field.getName().equals(fieldName)) {
                return field;
            }
        }
        throw new IllegalArgumentException("No such method: " + fieldName);
    }

    public static IDLToken toSingleToken(List<IDLToken> typeTokens) {
        IDLToken type1 = typeTokens.get(0);
        if (typeTokens.size() == 1) {
            return type1;
        }
        for (int i = typeTokens.size() - 1; i > 0; --i) {
            IDLToken last = typeTokens.get(i);
            if (last.lineNo() == type1.lineNo()) {
                CharSequence line = type1.line();
                return new IDLToken(line.toString().toCharArray(),
                                    type1.linePos() - 1,
                                    last.lineNo() == type1.lineNo() ?
                                    last.linePos() + last.length() - type1.linePos() :
                                    line.length() - type1.linePos(),
                                    IDLTokenType.IDENTIFIER,
                                    type1.lineNo(),
                                    type1.linePos());
            }
        }
        return type1;
    }
    private static PPropertyMap makeProperties(Collection<AnnotationDeclaration> annotations, String documentation) {
        return makeProperties(annotations, PPropertyMap.empty(), documentation);
    }

    private static PPropertyMap makeProperties(Collection<AnnotationDeclaration> annotations, PPropertyMap overrides,
                                               String documentation) {
        PPropertyMap.Builder builder = PPropertyMap.newBuilder();
        for (AnnotationDeclaration annotation : annotations) {
            builder.putUnsafe(annotation.getTag(), annotation.getValue());
        }
        if (documentation != null) {
            builder.put(PDefaultProperties.DOCUMENTATION, documentation);
        }
        builder.putAll(overrides);
        return builder.build();
    }
}
