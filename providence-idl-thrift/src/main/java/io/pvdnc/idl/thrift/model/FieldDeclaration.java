/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.thrift.model;

import io.pvdnc.core.types.PFieldRequirement;
import io.pvdnc.idl.parser.IDLToken;
import io.pvdnc.idl.thrift.util.DeclarationUtil;

import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * Represents the declaration of a single field param or thrown exception.
 *
 * <pre>{@code
 * field ::= ({id} ':')? {optionality}? {type} {name} ('=' {defaultValue}) {annotations}?
 * }</pre>
 */
public class FieldDeclaration extends Declaration {
    private final IDLToken       id;
    private final int            fieldId;
    private final IDLToken       requirement;
    private final List<IDLToken> type;
    private final List<IDLToken> defaultValue;

    public FieldDeclaration(String documentation,
                            IDLToken id,
                            int fieldId,
                            IDLToken requirement,
                            IDLToken name,
                            List<IDLToken> type,
                            List<IDLToken> defaultValue,
                            List<AnnotationDeclaration> annotations) {
        super(documentation, name, annotations);
        this.id = id;
        this.fieldId = fieldId;
        this.requirement = requirement;
        this.type = requireNonNull(type);
        this.defaultValue = defaultValue;
    }

    public int getId() {
        return fieldId;
    }

    public IDLToken getIdToken() {
        return id;
    }

    public PFieldRequirement getRequirement() {
        if (requirement != null) {
            switch (requirement.toString()) {
                case "required": return PFieldRequirement.REQUIRED;
                case "optional": return PFieldRequirement.OPTIONAL;
            }
        }
        return PFieldRequirement.DEFAULT_OUT;
    }

    public List<IDLToken> getDefaultValueTokens() {
        return defaultValue;
    }

    public String getType() {
        return DeclarationUtil.toTypeString(type);
    }

    public List<IDLToken> getTypeTokens() {
        return type;
    }

    // --- Displayable

    @Override
    public String displayString() {
        StringBuilder builder = new StringBuilder();
        if (getIdToken() != null) {
            builder.append(getId()).append(": ");
        }
        builder.append(DeclarationUtil.toTypeString(getTypeTokens()))
               .append(" ")
               .append(getName())
               .append(";\n");
        return builder.toString();
    }

    // --- Object

    @Override
    public String toString() {
        return "Field{" + getName() + "}";
    }
}
