/*
 * Copyright 2020 Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.pvdnc.idl.thrift.model;

import net.morimekta.strings.Displayable;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * <pre>{@code
 * declaration ::= {documentation} ({typedef} | {enum} | {message} | {service} | {const})
 * program     ::= {namespace|include}* {declaration}*
 * }</pre>
 */
public class ProgramDeclaration implements Displayable {
    private final String documentation;
    private final String programName;
    private final List<IncludeDeclaration> includes;
    private final List<NamespaceDeclaration> namespaces;
    private final List<Declaration> declarationList;

    public ProgramDeclaration(String documentation,
                              String programName,
                              List<IncludeDeclaration> includes,
                              List<NamespaceDeclaration> namespaces,
                              List<Declaration> declarationList) {
        this.documentation = documentation;
        this.programName = requireNonNull(programName, "programName == null");
        this.includes = requireNonNull(includes, "includes == null");
        this.namespaces = requireNonNull(namespaces, "namespaces == null");
        this.declarationList = requireNonNull(declarationList, "declarationList == null");
    }

    public String getDocumentation() {
        return documentation;
    }

    public String getProgramName() {
        return programName;
    }

    public List<IncludeDeclaration> getIncludes() {
        return includes;
    }

    public List<NamespaceDeclaration> getNamespaces() {
        return namespaces;
    }

    public List<Declaration> getDeclarationList() {
        return declarationList;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof ProgramDeclaration)) return false;
        ProgramDeclaration other = (ProgramDeclaration) obj;
        return Objects.equals(documentation, other.documentation) &&
               Objects.equals(programName, other.programName) &&
               Objects.equals(includes, other.includes) &&
               Objects.equals(namespaces, other.namespaces) &&
               Objects.equals(declarationList, other.declarationList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(documentation, programName, includes, namespaces, declarationList);
    }

    // --- Displayable

    @Override
    public String displayString() {
        return namespaces.stream().map(NamespaceDeclaration::displayString).collect(Collectors.joining()) +
               includes.stream().map(IncludeDeclaration::displayString).collect(Collectors.joining()) +
               declarationList.stream().map(Declaration::displayString).collect(Collectors.joining());
    }

    // --- Object

    @Override
    public String toString() {
        return "ProgramDeclaration{" + programName + ".{" +
               declarationList.stream().map(Declaration::getName).collect(Collectors.joining(",")) +
               "}}";
    }
}
