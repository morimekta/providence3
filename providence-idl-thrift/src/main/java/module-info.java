import io.pvdnc.idl.generator.GeneratorProvider;
import io.pvdnc.idl.parser.IDLParserProvider;
import io.pvdnc.idl.thrift.ThriftGeneratorProvider;
import io.pvdnc.idl.thrift.ThriftParserProvider;

module io.pvdnc.idl.thrift {
    exports io.pvdnc.idl.thrift;

    requires transitive io.pvdnc.core;
    requires transitive io.pvdnc.idl.generator;
    requires transitive io.pvdnc.idl.parser;

    requires io.pvdnc.sio.thrift;

    requires net.morimekta.collect;
    requires net.morimekta.strings;
    requires net.morimekta.lexer;
    requires net.morimekta.file;

    provides IDLParserProvider with ThriftParserProvider;
    provides GeneratorProvider with ThriftGeneratorProvider;
}